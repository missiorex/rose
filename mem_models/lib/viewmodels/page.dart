import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FacingPage {
  FacingPage(
      {this.label,
      this.fabLabel,
      this.colors,
      this.tabIconData,
      this.fabIconData,
      this.popUpElement,
      this.isAddTitle,
      this.isEditingTitle,
      this.fabImgUrl,
      this.unReadCount});

  final String label;
  final String fabLabel;
  final MaterialColor colors;
  final IconData tabIconData;
  final IconData fabIconData;
  final bool popUpElement;
  final String isAddTitle;
  final String isEditingTitle;
  final String fabImgUrl;
  String unReadCount;

  Color get labelColor =>
      colors != null ? colors.shade300 : Colors.grey.shade300;
  bool get fabDefined => colors != null && tabIconData != null;
  Color get fabColor => colors.shade400;
  Icon get fabIcon => new Icon(fabIconData);
  Icon get tabIcon => new Icon(tabIconData);
  ImageIcon get FabImgIcon => new ImageIcon(AssetImage(fabImgUrl));
  Key get fabKey => new ValueKey<Color>(fabColor);
}
