import 'package:flutter/material.dart';

class ChatUser {
  ChatUser({this.name, this.imageUrl, this.id});
  final String name;
  final String imageUrl;
  final String id;
}

class ChatMessage {
  ChatMessage(
      {this.id,
      this.sender,
      this.text,
      this.imageUrl,
      this.time,
      this.textOverlay,
      this.animationController});
  final String id;
  final ChatUser sender;
  final String text;
  final time;
  final String imageUrl;
  final String textOverlay;
  final AnimationController animationController;
}
