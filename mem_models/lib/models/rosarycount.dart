import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';
import 'enumerations.dart';

class RosaryCount {
  String id;
  int rosaryCount;
  DateTime time;
  String author;
  String uid;
  int totalCount;

  RosaryCount({
    String id,
    this.rosaryCount = 0,
    this.time,
    this.author,
    this.uid,
    this.totalCount,
  }) : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      id.hashCode ^
      rosaryCount.hashCode ^
      uid.hashCode ^
      totalCount.hashCode ^
      time.hashCode ^
      author.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RosaryCount &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          rosaryCount == other.rosaryCount &&
          totalCount == other.totalCount &&
          uid == other.uid &&
          time == other.time &&
          author == other.author;

  @override
  String toString() {
    return 'Message{id: $id, rosaryCount : $rosaryCount,totalCount:$totalCount, uid:$uid,time:$time,author:$author}';
  }

  RosaryCountEntity toEntity() {
    return RosaryCountEntity(
        id, rosaryCount, totalCount, uid, time.millisecondsSinceEpoch, author);
  }

  static RosaryCount fromEntity(RosaryCountEntity entity) {
    return RosaryCount(
        id: entity.id ?? Uuid().generateV4(),
        rosaryCount: entity.rosaryCount,
        totalCount: entity.totalCount,
        uid: entity.uid ?? false,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        author: entity.author);
  }
}
