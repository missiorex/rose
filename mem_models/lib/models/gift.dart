import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';

class Gift {
  final String id;
  final String title;
  final String uid;
  final DateTime time;
  final String giftType;
  final String giftVerse;
  final String giftImageUrl;
  final String giftDetails;

  Gift(
      {String id,
      this.title,
      this.uid,
      this.time,
      this.giftVerse,
      this.giftType,
      this.giftImageUrl,
      this.giftDetails})
      : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      giftType.hashCode ^
      giftVerse.hashCode ^
      giftImageUrl.hashCode ^
      giftDetails.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Gift &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          uid == other.uid &&
          time == other.time &&
          giftType == other.giftType &&
          giftVerse == other.giftVerse &&
          giftImageUrl == other.giftImageUrl &&
          giftDetails == other.giftDetails;

  @override
  String toString() {
    return 'BadgeEntity{ id: $id, badgeDoc:$title,uid:$uid,time:$time,giftType:$giftType,giftVerse:$giftVerse,giftImageUrl:$giftImageUrl,giftDetails:$giftDetails}';
  }

  GiftEntity toEntity() {
    return GiftEntity(id, title, uid, time.millisecondsSinceEpoch, giftType,
        giftVerse, giftImageUrl, giftDetails);
  }

  static Gift fromEntity(GiftEntity entity) {
    return Gift(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        uid: entity.uid,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        giftType: entity.giftType,
        giftVerse: entity.giftVerse,
        giftImageUrl: entity.giftImageUrl,
        giftDetails: entity.giftDetails);
  }
}
