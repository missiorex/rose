import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';

class StarsChannel {
  final String id;
  final String title;
  final String uid;
  final DateTime time;
  final String action;
  final int count;
  final int level;

  StarsChannel(
      {this.id,
      this.title,
      this.uid,
      this.time,
      this.action,
      this.count,
      this.level});

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      level.hashCode ^
      action.hashCode ^
      count.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StarsChannel &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          uid == other.uid &&
          time == other.time &&
          level == other.level &&
          action == other.action &&
          count == other.count;

  @override
  String toString() {
    return 'Badge{ id: $id, title:$title,uid:$uid,time:$time,level:$level,action:$action,count:$count}';
  }

  StarsChannelEntity toEntity() {
    return StarsChannelEntity(
        id, title, uid, time.millisecondsSinceEpoch, action, count, level);
  }

  static StarsChannel fromEntity(StarsChannelEntity entity) {
    return StarsChannel(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        uid: entity.uid,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        level: entity.level,
        action: entity.action,
        count: entity.count);
  }
}
