import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';
import 'enumerations.dart';

class VideoTestimony {
  String id;
  String title;
  String body;
  DateTime time;
  String author;
  String authorTitle;
  String youtubeUrl;
  String videoImageUrl;

  VideoTestimony(
      {String id,
      this.title,
      this.body = '',
      this.time,
      this.author,
      this.authorTitle,
      this.youtubeUrl,
      this.videoImageUrl})
      : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      title.hashCode ^
      body.hashCode ^
      id.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorTitle.hashCode ^
      youtubeUrl.hashCode ^
      videoImageUrl.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VideoTestimony &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          body == other.body &&
          id == other.id &&
          time == other.time &&
          author == other.author &&
          authorTitle == other.authorTitle &&
          youtubeUrl == other.youtubeUrl &&
          videoImageUrl == other.videoImageUrl;

  @override
  String toString() {
    return 'Message{ title: $title, body: $body, id: $id, time:$time,author:$author,authorTitle:$authorTitle,videoImageUrl:$videoImageUrl}';
  }

  VideoTestimonyEntity toEntity() {
    return VideoTestimonyEntity(id, title, body, time.millisecondsSinceEpoch,
        author, authorTitle, youtubeUrl, videoImageUrl);
  }

  static VideoTestimony fromEntity(entity) {
    return VideoTestimony(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        body: entity.body,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        author: entity.author,
        authorTitle: entity.authorTitle,
        youtubeUrl: entity.youtubeUrl,
        videoImageUrl: entity.videoImageUrl);
  }
}
