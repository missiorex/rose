import 'package:firebase_auth/firebase_auth.dart';
import 'package:mem_models/mem_models.dart';

class AppState {
  bool isLoading;
  bool rosaryListLoading;
  User user;
  MemUser userDetails;
  List<MEMmessage> messages;
  List<MEMmessage> rosaryMessages;
  List<MEMmessage> requests;
  List<MEMmessage> testimonies;
  List<MEMmessage> guidanceMessages;
  List<RosaryCount> rosaryList;
  List<MEMmessage> regionalMessages;
  List<MEMmessage> localeMessages;
  List<String> regions;
  List<String> locales;
  List<String> videotypes;
  List<MemUser> members;
  List<Badge> badges;
  List<GospelCard> gospelCards;
  Acl userAcl;
  List<FCMNotification> newUserNotifications;
  List<FCMNotification> rosaryLimitNotifications;
  List<BishopsMessage> bishopsMessages;
  List<Benefactor> benefactorMessages;
  List<VideoTestimony> videoTestimonies;
  int unreadGuidanceReqCount;
  int unreadNewUserReqCount;
  int unreadRosaryLimitsCount;
  int unreadChatMsgCount;
  int unreadAdminChatMsgCount;
  int totalRosaryCount;
  bool hideGiftVerse;
  String latestGospelCard;

  AppState(
      {this.isLoading = false,
      this.rosaryListLoading = true,
      this.user,
      this.userDetails,
      this.messages = const [],
      this.rosaryMessages = const [],
      this.requests = const [],
      this.testimonies = const [],
      this.guidanceMessages = const [],
      this.rosaryList = const [],
      this.regionalMessages = const [],
      this.localeMessages = const [],
      this.regions = const [],
      this.locales = const [],
      this.videotypes = const [],
      this.members = const [],
      this.badges = const [],
      this.gospelCards = const [],
      this.userAcl,
      this.bishopsMessages = const [],
      this.benefactorMessages = const [],
      this.videoTestimonies = const [],
      this.newUserNotifications = const [],
      this.rosaryLimitNotifications = const [],
      this.unreadNewUserReqCount = 0,
      this.unreadGuidanceReqCount = 0,
      this.unreadRosaryLimitsCount = 0,
      this.unreadChatMsgCount = 0,
      this.unreadAdminChatMsgCount = 0,
      this.hideGiftVerse = false,
      this.latestGospelCard = ""});

  factory AppState.loading() => AppState(isLoading: true);

  @override
  int get hashCode => isLoading.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState &&
          runtimeType == other.runtimeType &&
          isLoading == other.isLoading;

  @override
  String toString() {
    return 'AppState{isLoading: $isLoading,user: ${user?.displayName ?? 'null'}}';
  }

//  List<Message> filteredMessages(VisibilityFilter activeFilter, String uid) =>
//      regionalMessages.where((message) {
//        //debugPrint("Regional Message in Appstate" + message.toString());
//
//        switch (activeFilter) {
//          case VisibilityFilter.allExpectRosaries:
//            return message.type != "rosary";
//            break;
//          case VisibilityFilter.rosaryAddAndDelete:
//            return message.type == "rosary" || message.type == "rosary_archive";
//            break;
//          case VisibilityFilter.all:
//            return true;
//            break;
//          case VisibilityFilter.adminMsgsOnly:
//            return message.type == "admin";
//            break;
//          case VisibilityFilter.requestsOnly:
//            return message.type == "request";
//            break;
//          case VisibilityFilter.thanksOnly:
//            return message.type == "testimony";
//            break;
//          case VisibilityFilter.rosaryOnly:
//            return message.type == "rosary";
//            break;
//          case VisibilityFilter.guidanceOnly:
//            return message.type == "guidance";
//            break;
//          case VisibilityFilter.myAdminMsgsOnly:
//            return message.authorID == uid && message.type == "admin";
//            break;
//
//          default:
//            return true;
//        }
//      }).toList();

  List<MEMmessage> filteredMessages(VisibilityFilter activeFilter, String uid) {
    List<MEMmessage> _regionalMessages = regionalMessages.where((message) {
      //debugPrint("Regional Message in Appstate" + message.toString());

      switch (activeFilter) {
        case VisibilityFilter.allExpectRosaries:
          return message.type != "rosary";
          break;
        case VisibilityFilter.rosaryAddAndDelete:
          return message.type == "rosary" || message.type == "rosary_archive";
          break;
        case VisibilityFilter.requestAndThanks:
          return message.type == "request" || message.type == "testimony";
          break;
        case VisibilityFilter.all:
          return true;
          break;
        case VisibilityFilter.adminMsgsOnly:
          return message.type == "admin";
          break;
        case VisibilityFilter.requestsOnly:
          return message.type == "request";
          break;
        case VisibilityFilter.thanksOnly:
          return message.type == "testimony";
          break;
        case VisibilityFilter.rosaryOnly:
          return message.type == "rosary";
          break;
        case VisibilityFilter.guidanceOnly:
          return message.type == "guidance";
          break;
        case VisibilityFilter.myAdminMsgsOnly:
          return message.authorID == uid && message.type == "admin";
          break;

        default:
          return true;
      }
    }).toList();

    List<MEMmessage> _localeMessages = localeMessages;

    List<MEMmessage> _adminMessages = List.from(_regionalMessages)
      ..addAll(_localeMessages);

    _adminMessages.sort((a, b) => b.time.compareTo(a.time));

    return _adminMessages;
  }

  List<MEMmessage> filteredGlobalMessages(VisibilityFilter activeFilter) =>
      messages.where((message) {
        //debugPrint("Regional Message in Appstate" + message.toString());

        switch (activeFilter) {
          case VisibilityFilter.allExpectRosaries:
            return message.type != "rosary";
            break;
          case VisibilityFilter.rosaryAddAndDelete:
            return message.type == "rosary" || message.type == "rosary_archive";
            break;
          case VisibilityFilter.all:
            return true;
            break;
          case VisibilityFilter.adminMsgsOnly:
            return message.type == "admin";
            break;
          case VisibilityFilter.requestsOnly:
            return message.type == "request";
            break;
          case VisibilityFilter.thanksOnly:
            return message.type == "testimony";
            break;
          case VisibilityFilter.rosaryOnly:
            return message.type == "rosary";
            break;
          case VisibilityFilter.guidanceOnly:
            return message.type == "guidance";
            break;

          default:
            return true;
        }
      }).toList();

//  List<MEMmessage> filteredMessageList(String msgType) =>
//      messages.where((message) {
//        //debugPrint("Regional Message in Appstate" + message.toString());
//
//        switch (msgType) {
//          case "rosary":
//            return message.type == "rosary";
//            break;
//
//          case "request":
//            return message.type == "request";;
//            break;
//          case "testimony":
//            return message.type == "testimony";
//            break;
//          case "admin":
//            return message.type == "admin";
//            break;
//
//          default:
//            return true;
//        }
//      }).toList();

  int getLatestMessageCount(String msgType, DateTime lastacess) {
    int counter = 0;
    //debugPrint("Regional Message in Appstate" + message.toString());

//    SharedPreferences.getInstance().then((prefs) {
//      DateTime last_open_date = DateTime.fromMillisecondsSinceEpoch(
//          prefs.getInt(msgType + "_last_open"));
//
//      messages.forEach((msg) {
//        if (msg.time.isBefore(DateTime.now())) {
//          counter++;
//        }
//      });
//    });

    messages.forEach((msg) {
      if (msg.time.isAfter(lastacess ?? DateTime.now())) {
        counter++;
      }
    });

    return counter;
  }
}
