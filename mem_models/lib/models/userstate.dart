import 'package:firebase_auth/firebase_auth.dart';
import 'package:mem_models/mem_models.dart';
import 'package:flutter/foundation.dart';

class UserState {
  bool isAuthenticated;
  bool isActive;
  User user;

  UserState({
    this.isActive = false,
    this.isAuthenticated = false,
    this.user,
  });

  factory UserState.loading() => UserState(isActive: false);

  @override
  int get hashCode => isActive.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserState &&
          runtimeType == other.runtimeType &&
          isActive == other.isActive;

  @override
  String toString() {
    return 'UserState{isLoading: $isActive}}';
  }
}
