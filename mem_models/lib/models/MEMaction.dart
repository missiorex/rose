import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';
import 'enumerations.dart';

class MEMAction {
  String id;
  String title;
  String body;
  String topic;
  String token;
  int time;
  String author;
  String authorID;
  String priority;
  bool notified;
  String notificationType;
  String summary;
  List<String> actionItems;
  bool weekly;
  bool daily;
  int repeatHour;
  int repeatMinute;
  int repeatSecond;
  int day;

  MEMAction(
      {String id,
      this.title,
      this.body = '',
      this.topic,
      this.token,
      this.time,
      this.author,
      this.authorID,
      this.priority,
      this.notified,
      this.notificationType,
      this.summary,
      this.actionItems,
      this.weekly,
      this.daily,
      this.repeatHour,
      this.repeatMinute,
      this.repeatSecond,
      this.day})
      : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      title.hashCode ^
      body.hashCode ^
      topic.hashCode ^
      token.hashCode ^
      id.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorID.hashCode ^
      priority.hashCode ^
      notified.hashCode ^
      notificationType.hashCode ^
      summary.hashCode ^
      actionItems.hashCode ^
      weekly.hashCode ^
      daily.hashCode ^
      repeatHour.hashCode ^
      repeatMinute.hashCode ^
      repeatSecond.hashCode ^
      day.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MEMAction &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          body == other.body &&
          topic == other.topic &&
          token == other.token &&
          id == other.id &&
          time == other.time &&
          author == other.author &&
          authorID == other.authorID &&
          priority == other.priority &&
          notified == other.notified &&
          notificationType == other.notificationType &&
          summary == other.summary &&
          actionItems == other.actionItems &&
          weekly == other.weekly &&
          daily == other.daily &&
          repeatHour == other.repeatHour &&
          repeatMinute == other.repeatMinute &&
          repeatSecond == other.repeatSecond;

  @override
  String toString() {
    return 'Action{ title: $title, body: $body,topic:$topic,token:$token, id: $id, time:$time,author:$author,authorID:$authorID,authorTitle:$priority}';
  }

  ActionEntity toEntity() {
    return ActionEntity(
        id,
        title,
        body,
        topic,
        token,
        time,
        author,
        authorID,
        priority,
        notified,
        notificationType,
        summary,
        actionItems,
        weekly,
        daily,
        repeatHour,
        repeatMinute,
        repeatSecond,
        day);
  }

  static MEMAction fromEntity(entity) {
    return MEMAction(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        body: entity.body,
        topic: entity.topic,
        token: entity.token,
        time: entity.time,
        author: entity.author,
        authorID: entity.authorID,
        priority: entity.priority,
        notified: entity.notified,
        notificationType: entity.notificationType,
        summary: entity.summary,
        actionItems: entity.actionItems,
        weekly: entity.weekly,
        daily: entity.daily,
        repeatHour: entity.repeatHour,
        repeatMinute: entity.repeatMinute,
        repeatSecond: entity.repeatSecond);
  }
}
