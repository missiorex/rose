import 'package:mem_entities/entities/countsnapshot_entity.dart';
import 'package:base_helpers/uuid.dart';

class CountSnapShot {
  final String msgId;
  final int rosarySelfCount;
  final int rosaryExternalCount;
  final int totalCount;
  final DateTime time;
  final String updatedBy;
  final String updatedByID;
  final String details;
  final bool isExternal;

  Map toMap() {
    Map map = {
      "msgId": msgId,
      "rosarySelfCount": rosarySelfCount,
      "totalCount": totalCount,
      "rosaryExternalCount": rosaryExternalCount,
      "time": time,
      "author": updatedBy,
      "updatedByID": updatedByID,
      "details": details,
      "isExternal": isExternal
    };
//    if (id != null) {
//      map[columnId] = id;
//    }
    return map;
  }

  CountSnapShot(
      {this.msgId,
      this.rosarySelfCount,
      this.rosaryExternalCount,
      this.totalCount,
      this.time,
      this.updatedBy,
      this.updatedByID,
      this.details,
      this.isExternal});

  @override
  String toString() {
    return 'CountSnapShot{msgId:$msgId, totalCount: $totalCount, totalByUsers: $rosarySelfCount, totalExternal: $rosaryExternalCount, time : $time}';
  }

  CountSnapShotEntity toEntity() {
    return CountSnapShotEntity(
        msgId,
        rosarySelfCount,
        rosaryExternalCount,
        totalCount,
        time.millisecondsSinceEpoch,
        updatedBy,
        updatedByID,
        details,
        isExternal);
  }

  static CountSnapShot fromEntity(CountSnapShotEntity entity) {
    return CountSnapShot(
        msgId: entity.msgId ?? Uuid().generateV4(),
        rosarySelfCount: entity.rosarySelfCount,
        rosaryExternalCount: entity.rosaryExternalCount,
        totalCount: entity.totalCount,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        updatedBy: entity.updatedBy,
        updatedByID: entity.updatedByID,
        details: entity.details,
        isExternal: entity.isExternal);
  }

  @override
  int get hashCode =>
      details.hashCode ^ isExternal.hashCode ^ totalCount.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CountSnapShot &&
          runtimeType == other.runtimeType &&
          totalCount == other.totalCount &&
          rosarySelfCount == other.rosarySelfCount &&
          rosaryExternalCount == other.rosaryExternalCount;
}
