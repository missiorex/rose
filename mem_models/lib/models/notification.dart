import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';
import 'enumerations.dart';

class FCMNotification {
  String id;
  String title;
  String body;
  String topic;
  String token;
  DateTime time;
  String author;
  String authorID;
  String priority;

  FCMNotification({
    String id,
    this.title,
    this.body = '',
    this.topic,
    this.token,
    this.time,
    this.author,
    this.authorID,
    this.priority,
  }) : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      title.hashCode ^
      body.hashCode ^
      topic.hashCode ^
      token.hashCode ^
      id.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorID.hashCode ^
      priority.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FCMNotification &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          body == other.body &&
          topic == other.topic &&
          token == other.token &&
          id == other.id &&
          time == other.time &&
          author == other.author &&
          authorID == other.authorID &&
          priority == other.priority;

  @override
  String toString() {
    return 'Message{ title: $title, body: $body,topic:$topic,token:$token, id: $id, time:$time,author:$author,authorID:$authorID,authorTitle:$priority}';
  }

  NotificationEntity toEntity() {
    return NotificationEntity(id, title, body, topic, token,
        time.millisecondsSinceEpoch, author, authorID, priority);
  }

  static FCMNotification fromEntity(entity) {
    return FCMNotification(
      id: entity.id ?? Uuid().generateV4(),
      title: entity.title,
      body: entity.body,
      topic: entity.topic,
      token: entity.token,
      time: DateTime.fromMillisecondsSinceEpoch(entity.time),
      author: entity.author,
      authorID: entity.authorID,
      priority: entity.priority,
    );
  }
}
