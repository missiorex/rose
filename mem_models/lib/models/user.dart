import 'package:base_helpers/uuid.dart';
import 'package:mem_entities/mem_entities.dart';

class MemUser {
  String id;
  String emailId;
  String password;
  String title;
  String name;
  String displayName;
  String country;
  String region;
  String gender;
  String countryCode;
  String phoneNumber;
  String profilePhotoUrl;
  int rosariesOffered;
  int rosariesCollected;
  List<String> groupMemberOf = [];
  DateTime userAddedOn;
  DateTime rosaryLastAddedOn;
  DateTime birthday;
  String pushNotificationToken;
  String locale;
  bool status;
  bool isSuperAdmin;
  String role;
  bool isBlocked;
  int registrationCalls;

  MemUser(
      {this.id,
      this.emailId,
      this.password = '',
      this.title,
      this.name,
      this.displayName,
      this.country,
      this.region,
      this.gender,
      this.countryCode,
      this.phoneNumber,
      this.profilePhotoUrl,
      this.rosariesOffered,
      this.rosariesCollected,
      this.groupMemberOf,
      this.birthday,
      this.userAddedOn,
      this.rosaryLastAddedOn,
      this.pushNotificationToken,
      this.locale,
      this.status,
      this.isSuperAdmin,
      this.role,
      this.isBlocked,
      this.registrationCalls});

  @override
  int get hashCode =>
      emailId.hashCode ^
      password.hashCode ^
      title.hashCode ^
      name.hashCode ^
      displayName.hashCode ^
      id.hashCode ^
      country.hashCode ^
      countryCode.hashCode ^
      region.hashCode ^
      gender.hashCode ^
      phoneNumber.hashCode ^
      profilePhotoUrl.hashCode ^
      rosariesOffered.hashCode ^
      rosariesCollected.hashCode ^
      groupMemberOf.hashCode ^
      birthday.hashCode ^
      userAddedOn.hashCode ^
      rosaryLastAddedOn.hashCode ^
      isSuperAdmin.hashCode ^
      pushNotificationToken.hashCode ^
      locale.hashCode ^
      status.hashCode ^
      role.hashCode ^
      isBlocked.hashCode ^
      registrationCalls.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MemUser &&
          runtimeType == other.runtimeType &&
          emailId == other.emailId &&
          password == other.password &&
          title == other.title &&
          name == other.name &&
          displayName == other.displayName &&
          country == other.country &&
          countryCode == other.countryCode &&
          region == other.region &&
          gender == other.gender &&
          phoneNumber == other.phoneNumber &&
          profilePhotoUrl == other.profilePhotoUrl &&
          rosariesOffered == other.rosariesOffered &&
          rosariesCollected == other.rosariesCollected &&
          groupMemberOf == other.groupMemberOf &&
          birthday == other.birthday &&
          userAddedOn == other.userAddedOn &&
          rosaryLastAddedOn == other.rosaryLastAddedOn &&
          isSuperAdmin == other.isSuperAdmin &&
          pushNotificationToken == other.pushNotificationToken &&
          locale == other.locale &&
          status == other.status &&
          role == other.role &&
          isBlocked == other.isBlocked &&
          registrationCalls == other.registrationCalls;

  @override
  String toString() {
    return 'User{id:$id, name: $name}';
  }

  UserEntity toEntity() {
    return UserEntity(
        id: id,
        displayName: displayName,
        emailId: emailId,
        password: password,
        title: title,
        name: name,
        country: country,
        region: region,
        gender: gender,
        countryCode: countryCode,
        phoneNumber: phoneNumber,
        profilePhotoUrl: profilePhotoUrl,
        rosariesOffered: rosariesOffered,
        rosariesCollected: rosariesCollected,
        birthday: birthday.millisecondsSinceEpoch,
        userAddedOn: userAddedOn.millisecondsSinceEpoch,
        rosaryLastAddedOn: rosaryLastAddedOn.millisecondsSinceEpoch,
        isSuperAdmin: isSuperAdmin,
        status: status,
        pushNotificationToken: pushNotificationToken,
        locale: locale,
        role: role,
        isBlocked: isBlocked,
        registrationCalls: registrationCalls);
  }

  static MemUser fromEntity(UserEntity entity) {
    return MemUser(
        id: entity.id,
        displayName: entity.displayName,
        //photoUrl: entity.photoUrl,
        emailId: entity.emailId,
        password: entity.password,
        title: entity.title,
        name: entity.name,
        country: entity.country,
        region: entity.region,
        gender: entity.gender,
        countryCode: entity.countryCode,
        phoneNumber: entity.phoneNumber,
        profilePhotoUrl: entity.profilePhotoUrl,
        rosariesOffered: entity.rosariesOffered,
        rosariesCollected: entity.rosariesCollected,
        //groupMemberOf: json["groupMemberof"] as String,
        birthday: DateTime.fromMillisecondsSinceEpoch(entity.birthday),
        userAddedOn: DateTime.fromMillisecondsSinceEpoch(entity.userAddedOn),
        rosaryLastAddedOn:
            DateTime.fromMillisecondsSinceEpoch(entity.rosaryLastAddedOn),
        isSuperAdmin: entity.isSuperAdmin,
        pushNotificationToken: entity.pushNotificationToken,
        locale: entity.locale,
        status: entity.status,
        role: entity.role,
        isBlocked: entity.isBlocked,
        registrationCalls: entity.registrationCalls);
  }
}
