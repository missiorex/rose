import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';

class GospelCard {
  String id;
  String title;
  String uid;
  DateTime time;
  String cardType;
  String gospelVerse;
  String cardImageUrl;
  String cardDetails;
  String contentUrl;
  bool isComplete;
  bool startUpCard;
  bool sliderCard;
  String locale;

  GospelCard(
      {String id,
      this.title,
      this.uid,
      this.time,
      this.gospelVerse,
      this.cardType,
      this.cardImageUrl,
      this.cardDetails,
      this.contentUrl,
      this.isComplete,
      this.startUpCard,
      this.sliderCard,
      this.locale})
      : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      cardType.hashCode ^
      gospelVerse.hashCode ^
      cardImageUrl.hashCode ^
      cardDetails.hashCode ^
      contentUrl.hashCode ^
      isComplete.hashCode ^
      startUpCard.hashCode ^
      sliderCard.hashCode ^
      locale.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GospelCard &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          uid == other.uid &&
          time == other.time &&
          cardType == other.cardType &&
          gospelVerse == other.gospelVerse &&
          cardImageUrl == other.cardImageUrl &&
          cardDetails == other.cardDetails &&
          contentUrl == other.contentUrl &&
          isComplete == other.isComplete &&
          startUpCard == other.startUpCard &&
          locale == other.locale &&
          sliderCard == other.sliderCard;

  @override
  String toString() {
    return 'GospelCardEntity{ id: $id, title:$title,uid:$uid,time:$time,giftType:$cardType,giftVerse:$gospelVerse,giftImageUrl:$cardImageUrl,giftDetails:$cardDetails,contentUrl:$contentUrl,isComplete:$isComplete,startUpCard:$startUpCard,sliderCard:$sliderCard,,locale:$locale}';
  }

  GospelCardEntity toEntity() {
    return GospelCardEntity(
        id,
        title,
        uid,
        time.millisecondsSinceEpoch,
        cardType,
        gospelVerse,
        cardImageUrl,
        cardDetails,
        contentUrl,
        isComplete,
        startUpCard,
        sliderCard,
        locale);
  }

  static GospelCard fromEntity(GospelCardEntity entity) {
    return GospelCard(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        uid: entity.uid,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        cardType: entity.cardType,
        gospelVerse: entity.gospelVerse,
        cardImageUrl: entity.cardImageUrl,
        cardDetails: entity.cardDetails,
        contentUrl: entity.contentUrl,
        isComplete: entity.isComplete,
        startUpCard: entity.startUpCard,
        sliderCard: entity.sliderCard,
        locale: entity.locale);
  }
}
