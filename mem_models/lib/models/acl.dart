import 'package:base_helpers/uuid.dart';
import 'package:mem_entities/mem_entities.dart';

class Acl {
  String id;
  String name;
  bool grantAdminRole;
  bool revokeAdminRole;
  bool grantSuperAdminRole;
  bool revokeSuperAdminRole;
  bool activateUser;
  bool deactivateUser;
  bool blockUser;
  bool unBlockUser;
  bool deleteUser;
  bool regionalAdmin;
  bool localeAdmin;
  bool viewCounselling;
  bool rosaryDelete;
  bool messageDelete;
  bool addVideoTestimony;
  bool addBishopsMessage;
  bool editAboutUsContent;

  Acl(
      {this.id,
      this.name,
      this.grantAdminRole,
      this.revokeAdminRole,
      this.grantSuperAdminRole,
      this.revokeSuperAdminRole,
      this.activateUser,
      this.deactivateUser,
      this.blockUser,
      this.unBlockUser,
      this.deleteUser,
      this.regionalAdmin,
      this.localeAdmin,
      this.viewCounselling,
      this.rosaryDelete,
      this.messageDelete,
      this.addVideoTestimony,
      this.addBishopsMessage,
      this.editAboutUsContent});

  @override
  int get hashCode => grantAdminRole.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Acl &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          grantAdminRole == other.grantAdminRole &&
          revokeAdminRole == other.revokeAdminRole &&
          grantSuperAdminRole == other.grantSuperAdminRole &&
          revokeSuperAdminRole == other.revokeSuperAdminRole &&
          activateUser == other.activateUser &&
          deactivateUser == other.deactivateUser &&
          blockUser == other.blockUser &&
          unBlockUser == other.unBlockUser &&
          deleteUser == other.deleteUser &&
          regionalAdmin == other.regionalAdmin &&
          localeAdmin == other.localeAdmin &&
          viewCounselling == other.viewCounselling &&
          rosaryDelete == other.rosaryDelete &&
          messageDelete == other.messageDelete &&
          addVideoTestimony == other.addVideoTestimony &&
          addBishopsMessage == other.addBishopsMessage &&
          editAboutUsContent == other.editAboutUsContent;

  @override
  String toString() {
    return 'ACL{id: $id,name: $name, grantAdminRole: $grantAdminRole,revokeAdminRole: $revokeAdminRole,grantSuperAdminRole: $grantSuperAdminRole,revokeSuperAdminRole: $revokeSuperAdminRole,activateUser: $activateUser,deactivateUser: $deactivateUser,blockUser: $blockUser,unBlockUser: $unBlockUser,deleteUser: $deleteUser,regionalAdmin: $regionalAdmin,localeAdmin: $localeAdmin,viewCounselling: $viewCounselling,rosaryDelete:$rosaryDelete,messageDelete:$messageDelete,"addVideoTestimony":$addVideoTestimony,addBishopsMessage:$addBishopsMessage,editAboutUsContent:$editAboutUsContent}';
  }

  AclEntity toEntity() {
    return AclEntity(
        id: id,
        name: name,
        grantAdminRole: grantAdminRole,
        revokeAdminRole: revokeAdminRole,
        grantSuperAdminRole: grantSuperAdminRole,
        revokeSuperAdminRole: revokeSuperAdminRole,
        activateUser: activateUser,
        deactivateUser: deactivateUser,
        blockUser: blockUser,
        unBlockUser: unBlockUser,
        deleteUser: deleteUser,
        regionalAdmin: regionalAdmin,
        localeAdmin: localeAdmin,
        viewCounselling: viewCounselling,
        rosaryDelete: rosaryDelete,
        messageDelete: messageDelete,
        addVideoTestimony: addVideoTestimony,
        addBishopsMessage: addBishopsMessage,
        editAboutUsContent: editAboutUsContent);
  }

  static Acl fromEntity(AclEntity entity) {
    return Acl(
        id: entity.id,
        name: entity.name,
        grantAdminRole: entity.grantAdminRole,
        revokeAdminRole: entity.revokeAdminRole,
        grantSuperAdminRole: entity.grantSuperAdminRole,
        revokeSuperAdminRole: entity.revokeSuperAdminRole,
        activateUser: entity.activateUser,
        deactivateUser: entity.deactivateUser,
        blockUser: entity.blockUser,
        unBlockUser: entity.unBlockUser,
        deleteUser: entity.deleteUser,
        regionalAdmin: entity.regionalAdmin,
        localeAdmin: entity.localeAdmin,
        viewCounselling: entity.viewCounselling,
        rosaryDelete: entity.rosaryDelete,
        messageDelete: entity.messageDelete,
        addVideoTestimony: entity.addVideoTestimony,
        addBishopsMessage: entity.addBishopsMessage,
        editAboutUsContent: entity.editAboutUsContent);
  }
}
