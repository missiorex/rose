import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';
import 'enumerations.dart';

class BishopsMessage {
  String id;
  String title;
  String body;
  DateTime time;
  String author;
  String authorID;
  String authorTitle;
  String youtubeUrl;
  String videoImageUrl;

  BishopsMessage(
      {String id,
      this.title,
      this.body = '',
      this.time,
      this.author,
      this.authorID,
      this.authorTitle,
      this.youtubeUrl,
      this.videoImageUrl})
      : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      title.hashCode ^
      body.hashCode ^
      id.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorID.hashCode ^
      authorTitle.hashCode ^
      youtubeUrl.hashCode ^
      videoImageUrl.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BishopsMessage &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          body == other.body &&
          id == other.id &&
          time == other.time &&
          author == other.author &&
          authorID == other.authorID &&
          authorTitle == other.authorTitle &&
          youtubeUrl == other.youtubeUrl &&
          videoImageUrl == other.videoImageUrl;

  @override
  String toString() {
    return 'Message{ title: $title, body: $body, id: $id, time:$time,author:$author,authorID:$authorID,authorTitle:$authorTitle,videoImageUrl:$videoImageUrl}';
  }

  BishopsMessageEntity toEntity() {
    return BishopsMessageEntity(id, title, body, time.millisecondsSinceEpoch,
        author, authorID, authorTitle, youtubeUrl, videoImageUrl);
  }

  static BishopsMessage fromEntity(entity) {
    return BishopsMessage(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        body: entity.body,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        author: entity.author,
        authorID: entity.authorID,
        authorTitle: entity.authorTitle,
        youtubeUrl: entity.youtubeUrl,
        videoImageUrl: entity.videoImageUrl);
  }
}
