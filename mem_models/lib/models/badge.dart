import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';

class Badge {
  final String id;
  final String title;
  final String uid;
  final DateTime time;
  final int level;
  final String action;
  final int starsValue;
  final bool isComplete;
  final String assetName;

  Badge(
      {String id,
      this.title,
      this.uid,
      this.time,
      this.level,
      this.action,
      this.starsValue,
      this.isComplete,
      this.assetName})
      : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      level.hashCode ^
      action.hashCode ^
      isComplete.hashCode ^
      starsValue.hashCode ^
      assetName.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Badge &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          uid == other.uid &&
          time == other.time &&
          level == other.level &&
          action == other.action &&
          starsValue == other.starsValue &&
          isComplete == other.isComplete &&
          assetName == other.assetName;

  @override
  String toString() {
    return 'Badge{ id: $id, badgeDoc:$title,uid:$uid,time:$time,level:$level,action:$action,starsValue:$starsValue,isComplete:$isComplete,assetName:$assetName}';
  }

  BadgeEntity toEntity() {
    return BadgeEntity(id, title, uid, time.millisecondsSinceEpoch, level,
        action, starsValue, isComplete, assetName);
  }

  static Badge fromEntity(BadgeEntity entity) {
    return Badge(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        uid: entity.uid,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        level: entity.level,
        action: entity.action,
        starsValue: entity.starsValue,
        isComplete: entity.isComplete,
        assetName: entity.assetName);
  }
}
