import 'package:mem_entities/entities/message_entity.dart';
import 'package:base_helpers/uuid.dart';
import 'enumerations.dart';

class MEMmessage {
  bool complete;
  String id;
  String title;
  String body;
  int rosaryCount;
  int rosaryByOthers;
  String type;
  DateTime time;
  String author;
  String authorID;

  MEMmessage(
      {String id,
      this.title,
      this.body = '',
      this.complete = false,
      this.type = "request",
      this.rosaryCount = 0,
      this.rosaryByOthers = 0,
      this.time,
      this.author,
      this.authorID})
      : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      complete.hashCode ^
      title.hashCode ^
      body.hashCode ^
      id.hashCode ^
      rosaryCount.hashCode ^
      rosaryByOthers.hashCode ^
      type.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorID.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MEMmessage &&
          runtimeType == other.runtimeType &&
          complete == other.complete &&
          title == other.title &&
          body == other.body &&
          id == other.id &&
          rosaryCount == other.rosaryCount &&
          rosaryByOthers == other.rosaryByOthers &&
          type == other.type &&
          time == other.time &&
          author == other.author &&
          authorID == other.authorID;

  @override
  String toString() {
    return 'Message{complete: $complete, title: $title, body: $body, id: $id, rosaryCount : $rosaryCount,rosaryByOthers:$rosaryByOthers, type:$type,time:$time,author:$author,authorID:$authorID}';
  }

  MessageEntity toEntity() {
    return MessageEntity(id, title, body, complete, type, rosaryCount,
        rosaryByOthers, time.millisecondsSinceEpoch, author, authorID);
  }

  static MEMmessage fromEntity(MessageEntity entity) {
    return MEMmessage(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        body: entity.body,
        complete: entity.complete ?? false,
        type: entity.msgType,
        rosaryCount: entity.rosaryCount,
        rosaryByOthers: entity.rosaryByOthers,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time),
        author: entity.author,
        authorID: entity.authorID);
  }
}
