enum SignState { SIGNIN, SIGNOUT, SIGNUP }
enum MessageType { admin, prayerRequest, thanks, rosary }
enum VisibilityFilter {
  all,
  requestsOnly,
  adminMsgsOnly,
  thanksOnly,
  allExpectRosaries,
  rosaryOnly,
  guidanceOnly,
  myAdminMsgsOnly,
  rosaryAddAndDelete,
  requestAndThanks
}
