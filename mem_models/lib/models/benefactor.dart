import 'package:mem_entities/mem_entities.dart';
import 'package:base_helpers/uuid.dart';
import 'enumerations.dart';

class Benefactor {
  String id;
  String title;
  String name;
  String message;
  bool isPriest;
  String profilePhotoUrl;

  Benefactor({
    String id,
    this.title,
    this.name,
    this.message,
    this.isPriest,
    this.profilePhotoUrl,
  }) : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      title.hashCode ^
      name.hashCode ^
      message.hashCode ^
      id.hashCode ^
      isPriest.hashCode ^
      profilePhotoUrl.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Benefactor &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          name == other.name &&
          message == other.message &&
          id == other.id &&
          isPriest == other.isPriest &&
          profilePhotoUrl == other.profilePhotoUrl;

  @override
  String toString() {
    return 'Message{ title: $title, name: $name, id: $id,message:$message, isPriest:$isPriest,profilePhotoUrl:$profilePhotoUrl}';
  }

  BenefactorEntity toEntity() {
    return BenefactorEntity(
        id, title, name, message, isPriest, profilePhotoUrl);
  }

  static Benefactor fromEntity(entity) {
    return Benefactor(
        id: entity.id ?? Uuid().generateV4(),
        title: entity.title,
        name: entity.name,
        message: entity.message,
        isPriest: entity.isPriest,
        profilePhotoUrl: entity.profilePhotoUrl);
  }
}
