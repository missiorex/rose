import 'package:mem_entities/entities/rosary_entity.dart';
import 'package:base_helpers/uuid.dart';

final String tableRosary = "rosary";
final String colTime = "time";
final String columnTotalCount = "total_count";
final String colTotalByUsers = "total_by_users";
final String colTotalExternal = "total_external";

class Rosary {
  String id;
  int totalByUsers;
  int totalCount;
  int totalExternal;
  DateTime time;

  Map toMap() {
    Map map = {
      columnTotalCount: totalCount,
      colTime: time,
      colTotalByUsers: totalByUsers,
      colTotalExternal: totalExternal,
    };
//    if (id != null) {
//      map[columnId] = id;
//    }
    return map;
  }

  Rosary(
      {String id,
      this.totalByUsers,
      this.totalCount,
      this.totalExternal,
      this.time})
      : this.id = id ?? Uuid().generateV4();

  Rosary.fromMap(Map map) {
    this.totalCount = map[columnTotalCount];
    this.totalByUsers = map[colTotalByUsers];
    this.totalExternal = map[colTotalExternal];
    this.time = map[colTime];
  }

  @override
  String toString() {
    return 'Rosary{id:$id, totalCount: $totalCount, totalByUsers: $totalByUsers, totalExternal: $totalExternal, time : $time}';
  }

  RosaryEntity toEntity() {
    return RosaryEntity(id, totalByUsers, totalCount, totalExternal,
        time.millisecondsSinceEpoch);
  }

  static Rosary fromEntity(RosaryEntity entity) {
    return Rosary(
        id: entity.id ?? Uuid().generateV4(),
        totalByUsers: entity.totalByUsers,
        totalCount: entity.totalCount,
        totalExternal: entity.totalExternal,
        time: DateTime.fromMillisecondsSinceEpoch(entity.time));
  }

  @override
  int get hashCode =>
      totalByUsers.hashCode ^ totalExternal.hashCode ^ totalCount.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Rosary &&
          runtimeType == other.runtimeType &&
          totalCount == other.totalCount &&
          totalExternal == other.totalExternal &&
          totalByUsers == other.totalByUsers;
}
