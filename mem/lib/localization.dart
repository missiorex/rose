// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';

import 'package:flutter/material.dart';

class MemAppLocalizations {
  static MemAppLocalizations of(BuildContext context) {
    return Localizations.of<MemAppLocalizations>(context, MemAppLocalizations);
  }

  String get appTitle => "Marian Eucharistic Ministry";
}

class MemAppLocalizationsDelegate
    extends LocalizationsDelegate<MemAppLocalizations> {
  @override
  Future<MemAppLocalizations> load(Locale locale) =>
      Future(() => MemAppLocalizations());

  @override
  bool shouldReload(MemAppLocalizationsDelegate old) => false;

  @override
  bool isSupported(Locale locale) =>
      locale.languageCode.toLowerCase().contains("en");
}
