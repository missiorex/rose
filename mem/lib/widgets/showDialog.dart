import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

typedef void ActionCallBack();

class ShowDialog extends StatelessWidget {
  final String title;
  final String content;
  final String okayText;
  final String cancelText;
  final ActionCallBack cancelCallBack;
  final ActionCallBack okayCallBack;

  ShowDialog(
      {Key key,
      this.title,
      this.content,
      this.okayText,
      this.cancelText,
      this.cancelCallBack,
      this.okayCallBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: <Widget>[
        cancelCallBack != null
            ? TextButton(
                onPressed: () => cancelCallBack,
                child: Text(cancelText),
              )
            : Container(),
        okayCallBack != null
            ? TextButton(
                onPressed: () => okayCallBack,
                child: Text(okayText),
              )
            : Container(),
      ],
    );
  }
}
