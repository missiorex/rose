import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/widgets/rosary_item.dart';
import 'package:base_helpers/basehelpers.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:memapp/app_state_container.dart';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class RosaryList extends StatefulWidget {
  final List<MEMmessage> filteredMessages;
  final bool loading = false;
  final String bgImageUrl;
  final bool connectionStatus;
  final MessageBloc messageBloc;
  final UserBloc userBloc;
  final String msgType;
  static const _loadingSpace = 2;

  RosaryList(
      {@required this.filteredMessages,
      @required this.bgImageUrl,
      this.connectionStatus,
      this.messageBloc,
      this.userBloc,
      this.msgType})
      : super(key: MemKeys.rosaryList);

  @override
  State createState() => new RosaryListState();
}

class RosaryListState extends State<RosaryList> {
  RosaryItem msgItem;
  int litanyIndex = 0;
  Random random = Random();
  String lastId = "";
  int lastMsgTime = 0;
  var messages = [];
  static const String path = 'mem';
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  bool isPerformingRequest = false;
  double _scrollThreshold = 200;
  var lastVisible;
  var end;
  List<StreamSubscription> listeners = []; // list of listeners
  StreamSubscription currentListener;
  Stream currentStream;

  void changeIndex() {
    litanyIndex = random.nextInt(57);
  }

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    changeIndex();
    getFirstMessages();
    //messages = widget.filteredMessages;
    _scrollController.addListener((_onScroll));
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (!isPerformingRequest) {
//        setState(() => isPerformingRequest = true);
        getNextMessages();
//        setState(() {
//          isPerformingRequest = false;
//        });
      }
      debugPrint("Scroll: " + (maxScroll - currentScroll).toString());
    }
  }

  void getFirstMessages() async {
    var firstRegional, first;
    first = firestore
        .collection(path)
        .doc("rosary")
        .collection('messages')
        .where("msgtype", isEqualTo: widget.msgType)
        .orderBy('time', descending: true)
        .limit(4);

    if (widget.msgType == "admin") {
      firstRegional = firestore
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(widget.userBloc.userDetails.region)
          .collection('messages')
          .orderBy('time', descending: true)
          .limit(1);
    }

    setState(() {
      widget.msgType != "admin"
          ? currentStream = first.snapshots()
          : currentStream = firstRegional.snapshots();
    });

    currentListener = currentStream.listen((documentSnapshots) {
      // Get the last visible document
      if (documentSnapshots.docs.length > 0)
        lastVisible = documentSnapshots.docs[documentSnapshots.docs.length - 1]
            .data()["time"];
      debugPrint("last" + lastVisible.toString());
      setState(() {
        messages = documentSnapshots.docs.map((doc) {
          return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data()));
        }).toList();
      });
    });
    listeners.add(currentListener);
  }

  void getNextMessages() async {
    // Construct a new query starting at this document,
    var nextRegional, next;
    next = firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .where("msgtype", isEqualTo: widget.msgType)
        .orderBy('time', descending: true);

    if (widget.msgType == "admin") {
      nextRegional = firestore
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(widget.userBloc.userDetails.region)
          .collection('messages')
          .orderBy('time', descending: true);
    }

    setState(() {
      widget.msgType != "admin"
          ? currentStream = next.startAfter([lastVisible]).limit(1).snapshots()
          : currentStream =
              nextRegional.startAfter([lastVisible]).limit(1).snapshots();
    });

    currentListener = currentStream.listen((documentSnapshots) {
      // Get the last visible document

      setState(() {
        // previous starting boundary becomes new ending boundary
        if (documentSnapshots.documents.length > 0)
          lastVisible = documentSnapshots
              .docs[documentSnapshots.docs.length - 1]
              .data()["time"];

        //List<Message> filteredMsgs = messages.where((i) => i.id != ).toList();

        messages.addAll(documentSnapshots.docs.map((doc) {
          return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
        }).toList());
      });

      debugPrint("last" + lastVisible.toString());
    });
    listeners.add(currentListener);
  }

  void dispose() {
    _scrollController.dispose();
    listeners.forEach((listener) {
      listener.cancel();
    });
    super.dispose();
  }

  List colors = [
    Color.fromRGBO(226, 87, 55, 1.0),
    Color.fromRGBO(27, 162, 181, 1.0),
    Color.fromRGBO(65, 123, 34, 1.0),
    Color.fromRGBO(128, 85, 140, 1.0),
    Color.fromRGBO(53, 170, 187, 1.0),
    Color.fromRGBO(62, 121, 167, 1.0),
    Color.fromRGBO(64, 162, 136, 1.0),
    Color.fromRGBO(201, 138, 50, 1.0),
    Color.fromRGBO(70, 155, 196, 1.0),
    Color.fromRGBO(169, 66, 125, 1.0),
    Color.fromRGBO(76, 165, 148, 1.0),
    Color.fromRGBO(179, 110, 92, 1.0),
    Color.fromRGBO(117, 195, 195, 1.0),
    Color.fromRGBO(200, 171, 101, 1.0),
    Color.fromRGBO(158, 172, 197, 1.0),
    Color.fromRGBO(51, 142, 187, 1.0),
    Color.fromRGBO(51, 92, 113, 1.0),
    Color.fromRGBO(180, 204, 152, 1.0),
    Color.fromRGBO(149, 181, 116, 1.0),
    Color.fromRGBO(162, 171, 188, 1.0),
    Color.fromRGBO(56, 155, 187, 1.0),
    Color.fromRGBO(28, 124, 172, 1.0),
  ];

  List litany = [
    "Lord, have mercy on us.",
    "Christ, have mercy on us.",
    "Lord, have mercy on us.",
    "Christ, hear us.",
    "Christ, graciously hear us.",
    "God the Father of Heaven, Have mercy on us.",
    "God the Son,Redeemer of the World, Have mercy on us.",
    "God the Holy Ghost, Have mercy on us.",
    "Holy Trinity, one God, Have mercy on us.",
    "Holy Mary, pray for us.",
    "Holy Mother of God, pray for us.",
    "Holy Virgin of virgins, pray for us.",
    "Mother of Christ, pray for us.",
    "Mother of divine grace, pray for us.",
    "Mother most pure, pray for us.",
    "Mother most chaste, pray for us.",
    "Mother inviolate, pray for us.",
    "Mother undefiled, pray for us.",
    "Mother most amiable, pray for us.",
    "Mother most admirable, pray for us.",
    "Mother of good counsel, pray for us.",
    "Mother of our Creator, pray for us.",
    "Mother of our Savior, pray for us.",
    "Virgin most prudent, pray for us.",
    "Virgin most venerable, pray for us.",
    "Virgin most renowned, pray for us.",
    "Virgin most powerful, pray for us.",
    "Virgin most merciful, pray for us.",
    "Virgin most faithful, pray for us.",
    "Mirror of justice, pray for us.",
    "Seat of wisdom, pray for us.",
    "Cause of our joy, pray for us.",
    "Spiritual vessel, pray for us.",
    "Vessel of honor, pray for us.",
    "Singular vessel of devotion, pray for us.",
    "Mystical rose, pray for us.",
    "Tower of David, pray for us.",
    "Tower of ivory, pray for us.",
    "House of gold, pray for us.",
    "Ark of the Covenant, pray for us.",
    "Gate of Heaven, pray for us.",
    "Morning star, pray for us.",
    "Health of the sick, pray for us.",
    "Refuge of sinners, pray for us.",
    "Comforter of the afflicted, pray for us.",
    "Help of Christians, pray for us.",
    "Queen of angels, pray for us.",
    "Queen of patriarchs, pray for us.",
    "Queen of prophets, pray for us.",
    "Queen of apostles, pray for us.",
    "Queen of martyrs, pray for us.",
    "Queen of confessors, pray for us.",
    "Queen of virgins, pray for us.",
    "Queen of all saints, pray for us.",
    "Queen conceived without Original Sin, pray for us.",
    "Queen assumed into Heaven, pray for us.",
    "Queen of the most holy Rosary, pray for us.",
    "Queen of peace, pray for us."
  ];

  List bibleVerses = [
    "അവിടുന്ന് തന്റെ മാർഗങ്ങൾ നമ്മെ പഠിപ്പിക്കും (മിക്ക 4:2)",
    "സകല തിന്മകളിൽ നിന്നും കർത്താവ്‌ നിന്നെ കാത്തുകൊള്ളും (സങ്കീ 121:7)",
    "അനോഷിച്ചറിയാതെ കുറ്റം ആരോപിക്കരുത് (പ്രഭാ 11:7)",
    "കർത്താവു വാഗ്ധാനങ്ങളിൽ വിശോസ്ഥനും പ്രവർത്തികളിൽ കാരുണ്യവാനുമാണ് (സങ്കീ 145:13)",
    "എനിക്ക് ജീവിതം ക്രിസ്തുവും മരണം നേട്ടവുമാകുന്നു.  (ഫിലി 1:20)",
    "ദൈവത്തിന്റെ വചനം സജീവവും ഊർജസ്വലവും ആണ് (ഹെബ്രാ 4:12)",
    "അവൻ നമ്മുടെ പാപ പരിഹാര ബലിയാണ് (1യോഹന്നാൻ 2:2)",
    "നിങ്ങളുടെ പിതാവ് കരുണയുള്ളവനായിരിക്കുന്നതുപോലെ നിങ്ങളും കരുണ ഉള്ളവരായിരിക്കുവിൻ. (ലുക്കാ 6:36)",
  ];

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isPerformingRequest ? 1.0 : 0.0,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Random colorRandom = Random(9);
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);
    _scrollThreshold = MediaQuery.of(context).size.height * 0.25;

    return Container(
      height: MediaQuery.of(context).size.height * 0.14,
      decoration: new BoxDecoration(
        color: Colors.white,
        image: new DecorationImage(
            image: new AssetImage(widget.bgImageUrl),
            fit: BoxFit.fitWidth,
            repeat: ImageRepeat.repeatX),
      ),
      child: Container(
        margin: EdgeInsets.only(
            top: MediaQuery.of(context).size.width * 0.01,
            bottom: MediaQuery.of(context).size.width * 0.01),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            reverse: false,
            itemCount: messages.length,
            controller: _scrollController,
            itemBuilder: (BuildContext context, int index) {
              if (messages.length == 0) {
                return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                        child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                Colors.deepOrange))));
              } else {
                final message = messages[index];
                final List<MemUser> authorUsers = AppStateContainer.of(context)
                        .state
                        .members
                        .where((i) => i.id == message.authorID)
                        .toList() ??
                    [];

                if (index == messages.length) {
                  return _buildProgressIndicator();
                } else {
                  return RosaryItem(
                    visible: true,
                    message: message,
//                          headerColor: colors[message.rosaryCount % 9],
                    headerColor: colors[colorRandom.nextInt(colors.length)],

                    litanyElement: MemLists().litanyEN,
//                          litanyElement: litany][randon.nextInt(litany.length)],
                    //bibleVerse: bibleVerses[random.nextInt(7)],
                    bibleVerse: MemLists().correctingVerseML,
                    userBloc: userBloc,
                    authorUser:
                        authorUsers.length > 0 ? authorUsers[0] : MemUser(),
                    appState: AppStateContainer.of(context).state,
                    onDismissed: (direction) {
                      _removeMessage(context, message);
                    },
                    onCheckboxChanged: (complete) {
                      //updateMessage(message, complete: !message.complete);
                    },
                    connectionStatus: widget.connectionStatus,
                  );
//                    FutureBuilder(
//                      future: userBloc.authorUser(message.authorID),
//                      builder: (context, snapshot) {
//                        if (snapshot.connectionState == ConnectionState.done) {
//                          return RosaryItem(
//                            visible: true,
//                            message: message,
////                          headerColor: colors[message.rosaryCount % 9],
//                            headerColor:
//                                colors[colorRandom.nextInt(colors.length)],
//
//                            litanyElement: MemLists().litanyEN,
////                          litanyElement: litany][randon.nextInt(litany.length)],
//                            //bibleVerse: bibleVerses[random.nextInt(7)],
//                            bibleVerse: MemLists().correctingVerseML,
//                            authorUser: snapshot.data,
//
//                            appState: AppStateContainer.of(context).state,
//                            onDismissed: (direction) {
//                              _removeMessage(context, message);
//                            },
//                            onTap: () {
//                              Navigator.of(context).push(
//                                MaterialPageRoute(
//                                  builder: (_) {
//                                    return DetailScreen(
//                                      message: message,
//                                      onDelete: () =>
//                                          _removeMessage(context, message),
//                                    );
//                                  },
//                                ),
//                              );
//                            },
//                            onCheckboxChanged: (complete) {
//                              //updateMessage(message, complete: !message.complete);
//                            },
//                            connectionStatus: widget.connectionStatus,
//                          );
//                        } else
//                          return Center(
//                              widthFactor: 2.0,
//                              child: CircularProgressIndicator(
//                                  valueColor: AlwaysStoppedAnimation<Color>(
//                                      Colors.black26)));
//                      });
                }
              }
            }),
      ),

//            Stack(children: <Widget>[
//              Container(
//                  child: ListView.builder(
//                padding:
//                    new EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
//                key: MemKeys.messageList,
//                reverse: false,
//                shrinkWrap: true,
//                controller: _scrollController,
////                      itemCount: widget.filteredMessages.length,
//                itemCount: messages.length,
//                itemBuilder: (BuildContext context, int index) {
//                  //final message = widget.filteredMessages[index];
//
//                  if (messages.length == 0) {
//                    return Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: Center(
//                            child: CircularProgressIndicator(
//                                valueColor: AlwaysStoppedAnimation<Color>(
//                                    Colors.deepOrange))));
//                  } else {
//                    final message = messages[index];
//                    final List<User> authorUsers = AppStateContainer.of(context)
//                            .state
//                            .members
//                            .where((i) => i.id == message.authorID)
//                            .toList() ??
//                        [];
//
//                    if (index == messages.length) {
//                      return _buildProgressIndicator();
//                    } else {
//                      return msgItem = MessageItem(
//                        visible: true,
//                        message: message,
////                          headerColor: colors[message.rosaryCount % 9],
//                        headerColor: colors[colorRandom.nextInt(colors.length)],
//
//                        litanyElement: MemLists().litanyEN,
////                          litanyElement: litany][randon.nextInt(litany.length)],
//                        //bibleVerse: bibleVerses[random.nextInt(7)],
//                        bibleVerse: MemLists().correctingVerseML,
//                        authorUser:
//                            authorUsers.length > 0 ? authorUsers[0] : User(),
//                        appState: AppStateContainer.of(context).state,
//                        onDismissed: (direction) {
//                          _removeMessage(context, message);
//                        },
//                        onTap: () {
//                          Navigator.of(context).push(
//                            MaterialPageRoute(
//                              builder: (_) {
//                                return DetailScreen(
//                                  message: message,
//                                  onDelete: () =>
//                                      _removeMessage(context, message),
//                                );
//                              },
//                            ),
//                          );
//                        },
//                        onCheckboxChanged: (complete) {
//                          //updateMessage(message, complete: !message.complete);
//                        },
//                        connectionStatus: widget.connectionStatus,
//                      );
//                    }
//                  }
//                },
//              )),
//              Positioned(
//                right: 0.0,
//                top: -10.0,
//                child: FlatButton(
//                  child: Icon(Icons.keyboard_arrow_up),
//                  onPressed: () {
//                    try {
//                      _scrollController.animateTo(
//                        10.0,
//                        curve: Curves.easeOut,
//                        duration: const Duration(milliseconds: 300),
//                      );
//                    } catch (e) {
//                      debugPrint("Caught Exception");
//                    }
//                  },
//                ),
//              ),
//            ])
    );
  }

  String getLitany() {
    return "";
  }

  void _removeMessage(BuildContext context, MEMmessage message) {
    final messageBloc = MessageProvider.of(context);

    var container = AppStateContainer.of(context);
    String _deleteMessage;
    String _undoButtonMessage;
    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
        .messageDeleted(message.title);
    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;

    bool _isSuperAdmin = container.state.userDetails.isSuperAdmin;
    bool _eligibleToDelete =
        _isSuperAdmin || message.authorID == container.state.userDetails.id;
    debugPrint("Eligible to delete : " + _eligibleToDelete.toString());
    debugPrint("Author ID: " + message.authorID);
    debugPrint("User ID: " +
        container.state.userDetails.id +
        "Username" +
        container.state.userDetails.name);

    if (_eligibleToDelete) {
      switch (message.type) {
        case "rosary":
          messageBloc.removeRosaryMessage(message, container.state.userDetails);
          _deleteMessage =
              "Rosary Count reduced by" + message.rosaryCount.toString();
          _undoButtonMessage = "";
          break;

        case "request":
        case "thanks":
          messageBloc.removeMessage(message);
          _deleteMessage = "Requets/ Testimony Deleted";
          _undoButtonMessage = "";
          break;

        case "guidance":
          messageBloc.removeMessage(message);
          _deleteMessage = "Counselling Request Deleted";
          _undoButtonMessage = "";
          break;
        case "admin":
          messageBloc.removeRegionalMessage(
              message, container.state.userDetails);
          _deleteMessage = "Admin Message Delete";
          _undoButtonMessage = "";
          break;
      }
    } else {
      _deleteMessage = "Sorry, You cannot delete this message";
      _undoButtonMessage = "";
    }

    Scaffold.of(context).showSnackBar(
      SnackBar(
        key: MemKeys.snackbar,
        duration: Duration(seconds: 3),
        backgroundColor: Theme.of(context).errorColor,
        content: Text(
          _deleteMessage,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.white),
        ),
        action: SnackBarAction(
          label: _undoButtonMessage,
          onPressed: () {
            messageBloc.messageRepository.addRegionalMessage(
                message.toEntity(), container.state.userDetails.region);
            //msgItem.visible = true;
            //addMessage(message);
          },
        ),
      ),
    );
  }
}

//class MessageList extends StatelessWidget {
//  final List<Message> filteredMessages;
//  final bool loading = false;
//  final String bgImageUrl;
//  static const _loadingSpace = 2;
//  static ScrollController _controller = ScrollController();
//
//  MessageItem msgItem;
//
//  MessageList({@required this.filteredMessages, @required this.bgImageUrl})
//      : super(key: MemKeys.messageList);
//
//  List colors = [
//    Colors.pinkAccent,
//    Colors.amberAccent,
//    Colors.green,
//    Colors.orangeAccent
//  ];
//  Random random = new Random();
//
//  int index = 0;
//
//  void changeIndex() {
//    index = random.nextInt(4);
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Expanded(
//        child: Container(
//            decoration: new BoxDecoration(
//              color: Colors.white,
//              image: new DecorationImage(
//                  image: new AssetImage(bgImageUrl),
//                  fit: BoxFit.none,
//                  repeat: ImageRepeat.repeat),
//            ),
//            child: loading
//                ? Center(
//                child: CircularProgressIndicator(
//                  key: MemKeys.messagesLoading,
//                ))
//                : Stack(children: <Widget>[
//              Container(
//                  child: ListView.builder(
//                    padding: new EdgeInsets.all(20.0),
//                    key: MemKeys.messageList,
//                    reverse: false,
//                    shrinkWrap: true,
//                    controller: _controller,
//                    itemCount: filteredMessages.length,
//                    itemBuilder: (BuildContext context, int index) {
//                      final message = filteredMessages[index];
//                      final List<User> authorUsers =
//                      AppStateContainer
//                          .of(context)
//                          .state
//                          .members
//                          .where((i) => i.name == message.author)
//                          .toList();
//
//                      return msgItem = MessageItem(
//                        visible: true,
//                        message: message,
//                        headerColor: colors[random.nextInt(4)],
//                        authorUser: authorUsers[0],
//                        appState: AppStateContainer
//                            .of(context)
//                            .state,
//                        onDismissed: (direction) {
//                          _removeMessage(context, message);
//                        },
//                        onTap: () {
//                          Navigator.of(context).push(
//                            MaterialPageRoute(
//                              builder: (_) {
//                                return DetailScreen(
//                                  message: message,
//                                  onDelete: () =>
//                                      _removeMessage(context, message),
//
//                                );
//                              },
//                            ),
//                          );
//                        },
//                        onCheckboxChanged: (complete) {
//                          //updateMessage(message, complete: !message.complete);
//                        },
//                      );
//                    },
//                  )),
//              Positioned(
//                right: 0.0,
//                top: -10.0,
//                child: FlatButton(
//                  child: Icon(Icons.keyboard_arrow_up),
//                  onPressed: () {
//                    _controller.animateTo(
//                      10.0,
//                      curve: Curves.easeOut,
//                      duration: const Duration(milliseconds: 300),
//                    );
//                  },
//                ),
//              ),
//
//            ])));
//  }
//
//
//  void _removeMessage(BuildContext context, Message message) {
//    final messageBloc = MessageProvider.of(context);
//
//    var container = AppStateContainer.of(context);
//    String _deleteMessage;
//    String _undoButtonMessage;
//    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
//        .messageDeleted(message.title);
//    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;
//
//    bool _isAdmin = container.state.userDetails.role == "admin";
//    bool _isSuperAdmin = container.state.userDetails.isSuperAdmin;
//    bool _eligibleToDelete =
//        _isAdmin || message.authorID == container.state.userDetails.id;
//    debugPrint("Eligible to delete : " + _eligibleToDelete.toString());
//    debugPrint("Author ID: " + message.authorID);
//    debugPrint("User ID: " +
//        container.state.userDetails.id +
//        "Username" +
//        container.state.userDetails.name);
//
//    if (_eligibleToDelete) {
//      switch (message.type) {
//        case "rosary":
//          messageBloc.removeRosaryMessage(message, container.state.userDetails);
//          _deleteMessage =
//              "Rosary Count reduced by" + message.rosaryCount.toString();
//          _undoButtonMessage = "";
//          break;
//
//        case "request":
//        case "thanks":
//          messageBloc.removeMessage(message);
//          _deleteMessage = "Requets/ Testimony Deleted";
//          _undoButtonMessage = "";
//          break;
//        case "admin":
//          messageBloc.removeRegionalMessage(
//              message, container.state.userDetails);
//          _deleteMessage = "Sorry, You cannot delete this message";
//          _undoButtonMessage = "";
//          break;
//      }
//    } else {
//      _deleteMessage = "Sorry, You cannot delete this message";
//      _undoButtonMessage = "";
//    }
//
//
//    Scaffold.of(context).showSnackBar(
//      SnackBar(
//        key: MemKeys.snackbar,
//        duration: Duration(seconds: 3),
//        backgroundColor: Theme
//            .of(context)
//            .errorColor,
//        content: Text(
//          _deleteMessage,
//          maxLines: 1,
//          overflow: TextOverflow.ellipsis,
//          style: TextStyle(color: Colors.white),
//        ),
//        action: SnackBarAction(
//          label: _undoButtonMessage,
//          onPressed: () {
//            messageBloc.messageRepository.addRegionalMessage(
//                message.toEntity(), container.state.userDetails.region);
//            //msgItem.visible = true;
//            //addMessage(message);
//          },
//        ),
//      ),
//    );
//  }
//}
///todo: Message Slice to be tried later in iteration -2
/// Builds a square of the product on a given [index] in the catalog.
/// Also sends the [index] to the [catalogBloc] so it can potentially load
/// more data.
//  Widget _createMessage(
//      int index, MessageSlice slice, MessageBloc messageBloc) {
//    // Notify catalog BLoC of the latest index that the framework is trying
//    // to build.
//    messageBloc.index.add(index);
//
//    // Get data.
//    final message = slice.elementAt(index);
//
//    // Display spinner if waiting for data.
//    if (message == null) {
//      return Center(child: CircularProgressIndicator());
//    }
//
//    // Display data.
//    return MessageItem(
//      key: Key(message.id.toString()),
//      message: message,
//      onDismissed: (direction) {
//        //_removeMessage(context, message);
//      },
//      onTap: () {},
//      onCheckboxChanged: (complete) {
//        //updateMessage(message, complete: !message.complete);
//      },
//    );
//  }

///todo: Message Slice to be tried later in iteration -2
