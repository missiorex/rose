// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/widgets/notification_item.dart';
import 'package:utility/utility.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:memapp/screens/notification_details_screen.dart';

class NotificationList extends StatelessWidget {
  final List<FCMNotification> filteredNotifications;
  final bool loading = false;
  final String bgImageUrl;
  static const _loadingSpace = 2;
  static ScrollController _controller = ScrollController();

  NotificationItem notificationItem;

  NotificationList(
      {@required this.filteredNotifications, @required this.bgImageUrl})
      : super(key: MemKeys.messageList);

  @override
  Widget build(BuildContext context) {
//    debugPrint(
//        "Filtered messages from Message List :" + filteredMessages.toString());

    return Expanded(
        child: Container(
            decoration: new BoxDecoration(
              color: Colors.white,
              image: new DecorationImage(
                  image: new AssetImage(bgImageUrl),
                  fit: BoxFit.none,
                  repeat: ImageRepeat.repeat),
            ),
            child: loading
                ? Center(
                    child: CircularProgressIndicator(
                    key: MemKeys.messagesLoading,
                  ))
                : Column(children: <Widget>[
                    Expanded(
                        child: ListView.builder(
                      padding: new EdgeInsets.all(20.0),
                      key: MemKeys.notificationList,
                      reverse: false,
                      shrinkWrap: true,
                      controller: _controller,
                      itemCount: filteredNotifications.length,
                      itemBuilder: (BuildContext context, int index) {
                        final message = filteredNotifications[index];

                        return notificationItem = NotificationItem(
                          visible: true,
                          message: message,
                          onDismissed: (direction) {
                            _removeNotification(context, message);
                          },
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (_) {
                                  return NotificationDetailScreen(
                                    message: message,
                                    onDelete: () =>
                                        _removeNotification(context, message),

//                      addMessage: addMessage,
//                      updateMessage: updateMessage,
                                  );
                                },
                              ),
                            );
                          },
                          onCheckboxChanged: (complete) {
                            //updateMessage(message, complete: !message.complete);
                          },
                        );
                      },
                    )),
                    FlatButton(
                      child: Icon(Icons.keyboard_arrow_up),
                      onPressed: () {
                        _controller.animateTo(
                          10.0,
                          curve: Curves.easeOut,
                          duration: const Duration(milliseconds: 300),
                        );
                      },
                    ),
                  ])));
  }

//  void _removeMessage(BuildContext context, Message message) {
//    // removeMessage(message);
//
//    Scaffold.of(context).showSnackBar(
//          SnackBar(
//            key: MemKeys.snackbar,
//            duration: Duration(seconds: 2),
//            backgroundColor: Theme.of(context).backgroundColor,
//            content: Text(
//              MemLocalizations(Localizations.localeOf(context))
//                  .messageDeleted(message.title),
//              maxLines: 1,
//              overflow: TextOverflow.ellipsis,
//            ),
//            action: SnackBarAction(
//              label: MemLocalizations(Localizations.localeOf(context)).undo,
//              onPressed: () {
//                //addMessage(message);
//              },
//            ),
//          ),
//        );
//  }

  ///todo: Message Slice to be tried later in iteration -2

  void _removeNotification(BuildContext context, FCMNotification notification) {
    final messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);
    String _deleteMessage;
    String _undoButtonMessage;
    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
        .messageDeleted(notification.title);
    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;

    messageBloc.removeNotification(notification);

//    if (message.topic == "new_user") {
//      messageBloc.removeRosaryMessage(message, container.state.userDetails);
//    } else if (message.type == "admin"
//        ? messageBloc.removeRegionalMessage(
//            message, container.state.userDetails)
//        : messageBloc.removeMessage(message)) {
//      msgItem.visible = false;
//    } else {
//      _deleteMessage = "You cannot delete this message";
//      _undoButtonMessage = "";
//    }

    Scaffold.of(context).showSnackBar(
      SnackBar(
        key: MemKeys.snackbar,
        duration: Duration(seconds: 3),
        backgroundColor: Theme.of(context).errorColor,
        content: Text(
          _deleteMessage,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.white),
        ),
        action: SnackBarAction(
          label: _undoButtonMessage,
          onPressed: () {
            messageBloc.messageRepository
                .addNotification(notification.toEntity());
            notificationItem.visible = true;
            //addMessage(message);
          },
        ),
      ),
    );
  }

  ///todo: Message Slice to be tried later in iteration -2
  /// Builds a square of the product on a given [index] in the catalog.
  /// Also sends the [index] to the [catalogBloc] so it can potentially load
  /// more data.
//  Widget _createMessage(
//      int index, MessageSlice slice, MessageBloc messageBloc) {
//    // Notify catalog BLoC of the latest index that the framework is trying
//    // to build.
//    messageBloc.index.add(index);
//
//    // Get data.
//    final message = slice.elementAt(index);
//
//    // Display spinner if waiting for data.
//    if (message == null) {
//      return Center(child: CircularProgressIndicator());
//    }
//
//    // Display data.
//    return MessageItem(
//      key: Key(message.id.toString()),
//      message: message,
//      onDismissed: (direction) {
//        //_removeMessage(context, message);
//      },
//      onTap: () {},
//      onCheckboxChanged: (complete) {
//        //updateMessage(message, complete: !message.complete);
//      },
//    );
//  }
}
