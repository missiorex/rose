// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:intl/intl.dart';
import 'package:memapp/app_state_container.dart';
import 'package:mem_models/mem_models.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:mem_entities/mem_entities.dart';
import 'dart:async';
import 'dart:convert';
import 'package:share/share.dart';

class NotificationItem extends StatelessWidget {
  final DismissDirectionCallback onDismissed;
  final GestureTapCallback onTap;
  final ValueChanged<bool> onCheckboxChanged;
  final FCMNotification message;
  AppState appState;
  MemUser _user;
  bool visible = true;
  double height = 0.0;

  //final List<Message> _messages;

  NotificationItem(
      {Key key,
      @required this.onDismissed,
      @required this.onTap,
      @required this.onCheckboxChanged,
      @required this.message,
      @required visible})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var container = AppStateContainer.of(context);
    appState = container.state;

    return AnimatedOpacity(
        // If the Widget should be visible, animate to 1.0 (fully visible). If
        // the Widget should be hidden, animate to 0.0 (invisible).
        opacity: visible ? 1.0 : 0.0,
        duration: Duration(milliseconds: 500),
        child: Container(
            margin: const EdgeInsets.all(4.0),
            padding: new EdgeInsets.all(8.0),
            decoration: new BoxDecoration(
              color: message.topic != "new_user"
                  ? Color.fromARGB(255, 255, 255, 255)
                  : Color.fromARGB(230, 236, 234, 217),
//                  : Color.fromARGB(255, 225, 242, 184),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black26,
                    offset: new Offset(1.0, 1.0),
                    blurRadius: 0.3,
                    spreadRadius: 0.3)
              ],
              border: new Border.all(color: Color.fromRGBO(228, 228, 228, 1.0)),
              borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
            ),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                      child: Container(
                          margin:
                              const EdgeInsets.only(left: 17.0, right: 25.0),
                          child: Text(
                            message.topic != "new_user"
                                ? message.author ?? ""
                                : "",
                            style: TextStyle(
                                color: Colors.red[500],
                                fontWeight: FontWeight.bold,
                                fontSize: 15.0),
                          ))),
                  Container(
                      child: Text(
                        new DateFormat.yMMMd().format(message.time),
                        style: TextStyle(color: Colors.grey, fontSize: 13.0),
                      ),

                      ///todo: write a stream for formatted date & time.
                      margin: const EdgeInsets.only(right: 40.0)),
                  Text(
                    new DateFormat.jm().format(message.time),
                    style: TextStyle(color: Colors.grey, fontSize: 13.0),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 12.0),
                key: MemKeys.messageItem(message.id),
                child: ListTile(
                  onTap: onTap,
//              leading: Checkbox(
//                key: MemKeys.messageItemCheckbox(message.id),
//                value: message.complete,
//                onChanged: onCheckboxChanged,
//              ),
//              leading: CircleAvatar(
//                  backgroundColor: const Color(0x33FFFFFF),
//                  child: Text(message.rosaryCount.toString(),
//                      style: Theme.of(context).textTheme.title.copyWith(
//                          fontWeight: FontWeight.bold, fontSize: 18.0))),
                  title: Theme.of(context).platform == TargetPlatform.iOS
                      ? Text(
                          isMal(message.title)
                              ? formatMalContent(message.title)
                              : message.title,
                          key: MemKeys.messageItemTitle(message.id),
                          textAlign: TextAlign.left,
                          style: isMal(message.title)
                              ? Theme.of(context).textTheme.title.copyWith(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.0,
                                  fontFamily: 'Karthika')
                              : Theme.of(context).textTheme.title.copyWith(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.0,
                                  fontFamily: 'Rachana'),
                        )
                      : Text(
                          message.title,
                          key: MemKeys.messageItemTitle(message.id),
                          textAlign: TextAlign.left,
                          style: Theme.of(context).textTheme.title.copyWith(
                                fontWeight: FontWeight.bold,
                                fontSize: 15.0,
                              ),
                        ),
                  subtitle: Theme.of(context).platform == TargetPlatform.iOS
                      ? Text(
                          isMal(message.body)
                              ? formatMalContent(message.body)
                              : message.body,
                          key: MemKeys.messageItemBody(message.id),
                          maxLines: 100,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: isMal(message.body)
                              ? Theme.of(context).textTheme.subhead.copyWith(
                                  fontSize: 14.0, fontFamily: 'Karthika')
                              : Theme.of(context).textTheme.subhead.copyWith(
                                  fontSize: 14.0, fontFamily: 'Rachana'),
                        )
                      : Text(
                          message.body,
                          key: MemKeys.messageItemBody(message.id),
                          maxLines: 100,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .subhead
                              .copyWith(fontSize: 14.0, fontFamily: 'Nila'),
                        ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                      child: IconButton(
                          color: Colors.grey,
                          icon: Icon(Icons.share),
                          onPressed: message.body.isEmpty
                              ? null
                              : (() {
                                  final RenderBox box =
                                      context.findRenderObject();
                                  Share.share(message.body,
                                      sharePositionOrigin:
                                          box.localToGlobal(Offset.zero) &
                                              box.size);
                                })))
                ],
              )
            ])));
  }

//  Future<User> getUser(BuildContext context) async {
//    User _user;
//    UserBloc userBloc = UserProvider.of(context);
//    await userBloc.getUser(appState.user.uid).then((user) {
//      _user = User.fromEntity(user);
//    });
//
//    return _user;
//  }

  String formatMalContent(String text) {
    List<String> convertedList = List<String>();

    List<String> list = text.split(" ");

    list.forEach((word) {
      if (!word.contains(RegExp(r'[A-Za-z ]'))) {
        word = convertUnicode(word);
      }

      convertedList.add(word);
    });

    return convertedList.join(" ");
  }

  bool isMal(String text) {
    bool _isMal = true;
    int count = 0;
    List<String> convertedList = List<String>();
    List<String> list = text.split(" ");

    list.forEach((word) {
      if (word.contains(RegExp(r'[A-Za-z]'))) {
        count++;
      }
    });

    count >= 1 ? _isMal = false : _isMal = true;

    return _isMal;
  }

  String convertUnicode(String text) {
    return text
        .toLowerCase()
        .replaceAll("അ", "A")
        .replaceAll("ആ", "B")
        .replaceAll("ഇ", "C")
        .replaceAll("ഈ", "Cu")
        .replaceAll("ഉ", "D")
        .replaceAll("ഊ", "Du")
        .replaceAll("ഋ", "E")
        .replaceAll("എ", "F")
        .replaceAll("ഏ", "G")
        .replaceAll("ഐ", "sF")
        .replaceAll("ഒ", "H")
        .replaceAll("ഓ", "Hm")
        .replaceAll("ഔ", "Hu")
        .replaceAll("ക്തേ", "tà")
        .replaceAll("ക്തെ", "sà")
        .replaceAll("ക്ത്യ", "ày")
        .replaceAll("ക്തൈ", "ssà")
        .replaceAll("ക്ത", "à")
        .replaceAll("ക്ഷ്യ", "£y")
        .replaceAll("ക്ഷെ", "s£")
        .replaceAll("ക്ഷേ", "t£")
        .replaceAll("ക്ഷൈ", "ss£")
        .replaceAll("ക്ഷ", "£")
        .replaceAll("ക്ടെ", "sÎ")
        .replaceAll("ക്ടേ", "tÎ")
        .replaceAll("ക്ട", "Î")
        .replaceAll("ങ്കെ", "s¦")
        .replaceAll("ങ്കേ", "t¦")
        .replaceAll("ങ്കൈ", "ss¦")
        .replaceAll("ങ്ക", "¦")
        .replaceAll("ക്ലെ", "s¢")
        .replaceAll("ക്ലേ", "t¢")
        .replaceAll("ക്ലൈ", "ss¢")
        .replaceAll("ക്ല", "¢")
        .replaceAll("ക്കേ", "t¡")
        .replaceAll("ക്കോ", "t¡m")
        .replaceAll("ക്കെ", "s¡")
        .replaceAll("കൊ", "sIm")
        .replaceAll("കെ", "sI")
        .replaceAll("കോ", "tIm")
        .replaceAll("കേ", "tI")
        .replaceAll("ക്വ", "Iz")
        .replaceAll("ക്രൈ", "ss{I")
        .replaceAll("ക്രോ", "t{Im")
        .replaceAll("ക്രേ", "t{I")
        .replaceAll("ക്രൊ", "s{Im")
        .replaceAll("ക്രെ", "s{I")
        .replaceAll("ക്ര", "{I")
        .replaceAll("ക്യ", "Iy")
        .replaceAll("ക്കൈ", "ss¡")
        .replaceAll("ക്ക", "¡")
        .replaceAll("കൈ", "ssI")
        .replaceAll("ക", "I")
        .replaceAll("ഖേ", "tJ")
        .replaceAll("ഖൈ", "ssJ")
        .replaceAll("ഖെ", "sJ")
        .replaceAll("ഖ്വ", "Jz")
        .replaceAll("ഖ്യ", "Jy")
        .replaceAll("ഖ", "J")
        .replaceAll("ഗ്ലെ", "s¥")
        .replaceAll("ഗ്ലേ", "t¥")
        .replaceAll("ഗ്ലൈ", "ss¥")
        .replaceAll("ഗ്ല", "¥")
        .replaceAll("ഗ്ഗേ", "t¤")
        .replaceAll("ഗ്ഗൈ", "ss¤")
        .replaceAll("ഗ്ഗെ", "s¤")
        .replaceAll("ഗൈ", "ssK")
        .replaceAll("ഗൊ", "sKm")
        .replaceAll("ഗെ", "sK")
        .replaceAll("ഗ്വ", "Kz")
        .replaceAll("ഗ്യ", "Ky")
        .replaceAll("ഗൃ", "Kr")
        .replaceAll("ഗ്രൈ", "ss{K")
        .replaceAll("ഗ്ര", "{K")
        .replaceAll("ഗ്ഗ", "¤")
        .replaceAll("ഗോ", "tKm")
        .replaceAll("ഗേ", "tK")
        .replaceAll("ഗ", "K")
        .replaceAll("ഘൈ", "ssL")
        .replaceAll("ഘൊ", "sLm")
        .replaceAll("ഘെ", "sL")
        .replaceAll("ഘോ", "tLm")
        .replaceAll("ഘേ", "tL")
        .replaceAll("ഘ", "L")
        .replaceAll("ങ്ങൈ", "ss§")
        .replaceAll("ങ്ങെ", "s§")
        .replaceAll("ങ്ങേ", "t§")
        .replaceAll("ങ്ങ്യ", "§y")
        .replaceAll("ങ്ങ", "§")
        .replaceAll("ങേ", "tM")
        .replaceAll("ങൈ", "ssM")
        .replaceAll("ങെ", "sM")
        .replaceAll("ങ", "M")
        .replaceAll("ഞ്ചൈ", "ss©")
        .replaceAll("ഞ്ചെ", "s©")
        .replaceAll("ഞ്ചേ", "t©")
        .replaceAll("ഞ്ച്യ", "©y")
        .replaceAll("ഞ്ച", "©")
        .replaceAll("ച്ചേ", "t¨")
        .replaceAll("ച്ചൈ", "ss¨")
        .replaceAll("ച്ചെ", "s¨")
        .replaceAll("ചോ", "tNm")
        .replaceAll("ചേ", "tN")
        .replaceAll("ചൈ", "ssN")
        .replaceAll("ചൊ", "sNm")
        .replaceAll("ചെ", "sN")
        .replaceAll("ച്ര", "{N")
        .replaceAll("ച്യ", "Ny")
        .replaceAll("ച്ച", "¨")
        .replaceAll("ച", "N")
        .replaceAll("ഛൈ", "ssO")
        .replaceAll("ഛെ", "sO")
        .replaceAll("ഛേ", "tO")
        .replaceAll("ഛ", "O")
        .replaceAll("ജ്ഞൈ", "ssÚ")
        .replaceAll("ജ്ഞെ", "sÚ")
        .replaceAll("ജ്ഞേ", "tÚ")
        .replaceAll("ജ്ഞ", "Ú")
        .replaceAll("ജ്ജേ", "tÖ")
        .replaceAll("ജ്ജൈ", "ssÖ")
        .replaceAll("ജ്ജെ", "sÖ")
        .replaceAll("ജ്ജ", "Ö")
        .replaceAll("ജൈ", "ssP")
        .replaceAll("ജൊ", "sPm")
        .replaceAll("ജെ", "sP")
        .replaceAll("ജ്വ", "Pz")
        .replaceAll("ജ്യ", "Py")
        .replaceAll("ജ്ര", "{P")
        .replaceAll("ജോ", "tPm")
        .replaceAll("ജേ", "tP")
        .replaceAll("ജ", "P")
        .replaceAll("ഝൈ", "ssQ")
        .replaceAll("ഝെ", "sQ")
        .replaceAll("ഝേ", "tQ")
        .replaceAll("ത്സ്യ", "Qy")
        .replaceAll("ഝ", "Q")
        .replaceAll("ഞ്ഞൈ", "ssª")
        .replaceAll("ഞ്ഞെ", "sª")
        .replaceAll("ഞ്ഞേ", "tª")
        .replaceAll("ഞ്ഞ", "ª")
        .replaceAll("ഞൈ", "ssR")
        .replaceAll("ഞെ", "sR")
        .replaceAll("ഞേ", "tR")
        .replaceAll("ഞ", "R")
        .replaceAll("ന്റെ", "sâ")
        .replaceAll("ന്റേ", "tâ")
        .replaceAll("ന്റ", "â")
        .replaceAll("ണ്ടൈ", "ssî")
        .replaceAll("ണ്ടെ", "sî")
        .replaceAll("ണ്ടേ", "tî")
        .replaceAll("ണ്ട്യ", "îy")
        .replaceAll("ണ്ട", "î")
        .replaceAll("ട്ട്യ", "«y")
        .replaceAll("ട്ടൈ", "ss«")
        .replaceAll("ട്ടെ", "s«")
        .replaceAll("ട്രൈ", "ss{S")
        .replaceAll("ട്രോ", "t{Sm")
        .replaceAll("ട്രേ", "t{S")
        .replaceAll("ട്രെ", "s{S")
        .replaceAll("ടൈ", "ssS")
        .replaceAll("ടൊ", "sSm")
        .replaceAll("ടെ", "sS")
        .replaceAll("ട്ര", "{S")
        .replaceAll("ട്വ", "Sz")
        .replaceAll("ട്യ", "Sy")
        .replaceAll("ട്ടോ", "t«m")
        .replaceAll("ട്ടേ", "t«")
        .replaceAll("ട്ട", "«")
        .replaceAll("ടോ", "tSm")
        .replaceAll("ടേ", "tS")
        .replaceAll("ട", "S")
        .replaceAll("ഠൈ", "ssT")
        .replaceAll("ഠെ", "sT")
        .replaceAll("ഠേ", "tT")
        .replaceAll("ഠ്യ", "Ty")
        .replaceAll("ഠ", "T")
        .replaceAll("ഡ്രൈ", "ss{U")
        .replaceAll("ഡ്രേ", "t{U")
        .replaceAll("ഡ്രെ", "s{U")
        .replaceAll("ഡൈ", "ssU")
        .replaceAll("ഡെ", "sU")
        .replaceAll("ഡ്ര", "{U")
        .replaceAll("ഡേ", "tU")
        .replaceAll("ഡോ", "tUm")
        .replaceAll("ഡ", "U")
        .replaceAll("ഢൈ", "ssV")
        .replaceAll("ഢെ", "sV")
        .replaceAll("ഢേ", "tV")
        .replaceAll("ഢ", "V")
        .replaceAll("ണ്മൈ", "ss×")
        .replaceAll("ണ്മെ", "s×")
        .replaceAll("ണ്മേ", "t×")
        .replaceAll("ണ്മ", "×")
        .replaceAll("ണ്ണൈ", "ss®")
        .replaceAll("ണ്ണെ", "s®")
        .replaceAll("ണൈ", "ssW")
        .replaceAll("ണെ", "sW")
        .replaceAll("ണ്‍", "¬")
        .replaceAll("ണ്ണേ", "t®")
        .replaceAll("ണ്ണ", "®")
        .replaceAll("ണേ", "tW")
        .replaceAll("ണ", "W")
        .replaceAll("ത്ഥൈ", "ss°")
        .replaceAll("ത്ഥെ", "s°")
        .replaceAll("ത്ഥേ", "t°")
        .replaceAll("ത്ഥ്യ", "°y")
        .replaceAll("ത്ഥ", "°")
        .replaceAll("ത്മൈ", "ssß")
        .replaceAll("ത്മെ", "sß")
        .replaceAll("ത്മേ", "tß")
        .replaceAll("ത്മ", "ß")
        .replaceAll("ത്തൈ", "ss¯")
        .replaceAll("ത്തെ", "s¯")
        .replaceAll("ന്തൈ", "ss´")
        .replaceAll("ന്തെ", "s´")
        .replaceAll("തൈ", "ssX")
        .replaceAll("തൊ", "sXm")
        .replaceAll("തെ", "sX")
        .replaceAll("ത്വ", "Xz")
        .replaceAll("ന്ത്ര", "{´")
        .replaceAll("ന്ത്യ", "´y")
        .replaceAll("ന്തേ", "t´")
        .replaceAll("ന്തോ", "t´m")
        .replaceAll("ന്ത", "´")
        .replaceAll("ത്യേ", "tXy")
        .replaceAll("ത്യ", "Xy")
        .replaceAll("തൃ", "Xr")
        .replaceAll("ത്രൈ", "ss{X")
        .replaceAll("ത്രോ", "t{Xm")
        .replaceAll("ത്രൊ", "t{Xm")
        .replaceAll("ത്രേ", "t{X")
        .replaceAll("ത്രെ", "s{X")
        .replaceAll("ത്ര", "{X")
        .replaceAll("ത്തേ", "t¯")
        .replaceAll("ത്ത", "¯")
        .replaceAll("തോ", "tXm")
        .replaceAll("തേ", "tX")
        .replaceAll("ത", "X")
        .replaceAll("ഥൈ", "ssY")
        .replaceAll("ഥെ", "sY")
        .replaceAll("ഥേ", "tY")
        .replaceAll("ഥ", "Y")
        .replaceAll("ദ്ധേ", "t²")
        .replaceAll("ദ്ധൈ", "ss²")
        .replaceAll("ദ്ധെ", "s²")
        .replaceAll("ദ്ധ", "²")
        .replaceAll("ബ്ദൈ", "ssÐ")
        .replaceAll("ബ്ദെ", "sÐ")
        .replaceAll("ബ്ദേ", "tÐ")
        .replaceAll("ബ്ദ", "Ð")
        .replaceAll("ന്ദൈ", "ssµ")
        .replaceAll("ന്ദെ", "sµ")
        .replaceAll("ന്ദേ", "tµ")
        .replaceAll("ന്ദ്രേ", "t{µ")
        .replaceAll("ന്ദ്രൈ", "ss{µ")
        .replaceAll("ന്ദ്രെ", "s{µ")
        .replaceAll("ന്ദ്ര", "{µ")
        .replaceAll("ന്ദ", "µ")
        .replaceAll("ദ്ദൈ", "ss±")
        .replaceAll("ദ്ദെ", "s±")
        .replaceAll("ദ്ദേ", "t±")
        .replaceAll("ദ്ദ", "±")
        .replaceAll("ദൈ", "ssZ")
        .replaceAll("ദെ", "sZ")
        .replaceAll("ദ്വൈ", "ssZz")
        .replaceAll("ദ്വേ", "tZz")
        .replaceAll("ദ്വ", "Zz")
        .replaceAll("ദ്യെ", "sZy")
        .replaceAll("ദ്യേ", "tZy")
        .replaceAll("ദ്യ", "Zy")
        .replaceAll("ദൃ", "Zr")
        .replaceAll("ദേ", "tZ")
        .replaceAll("ദ", "Z")
        .replaceAll("ബ്ധെ", "sÏ")
        .replaceAll("ബ്ധേ", "tÏ")
        .replaceAll("ബ്ധ", "Ï")
        .replaceAll("ന്ധ്രെ", "s{Ô")
        .replaceAll("ന്ധ്യ", "Ôy")
        .replaceAll("ന്ധ്ര", "{Ô")
        .replaceAll("ന്ധെ", "sÔ")
        .replaceAll("ന്ധേ", "tÔ")
        .replaceAll("ന്ധ", "Ô")
        .replaceAll("ധൈ", "ss[")
        .replaceAll("ധെ", "s[")
        .replaceAll("ധ്യ", "[y")
        .replaceAll("ധൃ", "[r")
        .replaceAll("ധേ", "t[")
        .replaceAll("ധ", "[")
        .replaceAll("ന്&zwj;െറ", "sâ")
        .replaceAll("ന്&zwj;േറ", "tâ")
        .replaceAll("ന്റെ", "sâ")
        .replaceAll("ന്റേ", "tâ")
        .replaceAll("ന്&zwj;റ", "â")
        .replaceAll("ന്റ", "â")
        .replaceAll("ന്&zwj;", "³")
        .replaceAll("ന്‍", "³")
        .replaceAll("ന്നൈ", "ss¶")
        .replaceAll("ന്നെ", "s¶")
        .replaceAll("ന്നേ", "t¶")
        .replaceAll("ന്ന", "¶")
        .replaceAll("ന്മൈ", "ss∙")
        .replaceAll("ന്മെ", "s∙")
        .replaceAll("ന്മേ", "t∙")
        .replaceAll("ന്മ", "∙")
        .replaceAll("നൈ", "ss\\")
        .replaceAll("നൊ", "s\\m")
        .replaceAll("നെ", "s\\")
        .replaceAll("ന്വേ", "t\\z")
        .replaceAll("ന്വെ", "s\\z")
        .replaceAll("ന്വ", "\\z")
        .replaceAll("ന്യ", "\\y")
        .replaceAll("നോ", "t\\m")
        .replaceAll("നേ", "t\\")
        .replaceAll("ന", "\\")
        .replaceAll("മ്പൈ", "ss¼")
        .replaceAll("മ്പെ", "s¼")
        .replaceAll("മ്പ്യ", "¼y")
        .replaceAll("മ്പേ", "t¼")
        .replaceAll("മ്പ", "¼")
        .replaceAll("ല്പെ", "så")
        .replaceAll("ല്പേ", "tå")
        .replaceAll("ല്പ", "å")
        .replaceAll("പ്ലേ", "ts¹")
        .replaceAll("പ്ലൈ", "ss¹")
        .replaceAll("പ്ലെ", "s¹")
        .replaceAll("പ്ല", "¹")
        .replaceAll("പ്പേ", "t¸")
        .replaceAll("പ്പൈ", "ss¸")
        .replaceAll("പ്പെ", "s¸")
        .replaceAll("പൈ", "ss]")
        .replaceAll("പൊ", "s]m")
        .replaceAll("പെ", "s]")
        .replaceAll("പ്യ", "]y")
        .replaceAll("പ്രേ", "t{]")
        .replaceAll("പ്രൈ", "ss{]")
        .replaceAll("പ്രെ", "s{]")
        .replaceAll("പ്ര", "{]")
        .replaceAll("പ്പേ", "t¸")
        .replaceAll("പ്പ", "¸")
        .replaceAll("പോ", "t]m")
        .replaceAll("പേ", "t]")
        .replaceAll("പ", "]")
        .replaceAll("ഫ്ലൈ", "^vssf")
        .replaceAll("ഫ്ലെ", "^vsf")
        .replaceAll("ഫ്ലേ", "^vtf")
        .replaceAll("ഫ്ല", "^vf")
        .replaceAll("ഫൈ", "ss^")
        .replaceAll("ഫൊ", "s^m")
        .replaceAll("ഫെ", "s^")
        .replaceAll("ഫോ", "t^m")
        .replaceAll("ഫേ", "t^")
        .replaceAll("ഫ്ര", "{^")
        .replaceAll("ഫ", "^")
        .replaceAll("ബ്ലൈ", "ss»")
        .replaceAll("ബ്ലെ", "s»")
        .replaceAll("ബ്ലേ", "t»")
        .replaceAll("ബ്ല", "»")
        .replaceAll("ബ്ബൈ", "ssº")
        .replaceAll("ബ്ബെ", "sº")
        .replaceAll("ബൈ", "ss_")
        .replaceAll("ബൊ", "s_m")
        .replaceAll("ബെ", "s_")
        .replaceAll("ബ്രേ", "t{_")
        .replaceAll("ബ്രൈ", "ss{_")
        .replaceAll("ബ്രെ", "s{_")
        .replaceAll("ബ്ര", "{_")
        .replaceAll("ബ്ബേ", "tº")
        .replaceAll("ബ്ബ", "º")
        .replaceAll("ബോ", "t_m")
        .replaceAll("ബേ", "t_")
        .replaceAll("ബ", "_")
        .replaceAll("ഭൈ", "ss`")
        .replaceAll("ഭെ", "s`")
        .replaceAll("ഭ്വ", "`z")
        .replaceAll("ഭ്ര", "{`")
        .replaceAll("ഭ്യ", "`y")
        .replaceAll("ഭൃ", "`r")
        .replaceAll("ഭേ", "t`")
        .replaceAll("ഭ", "`")
        .replaceAll("മ്മൈ", "ss½")
        .replaceAll("മ്മെ", "s½")
        .replaceAll("മൈ", "ssa")
        .replaceAll("മൊ", "sam")
        .replaceAll("മെ", "sa")
        .replaceAll("മ്യ", "ay")
        .replaceAll("മൃ", "ar")
        .replaceAll("മ്മേ", "t½")
        .replaceAll("മ്മ", "½")
        .replaceAll("മോ", "tam")
        .replaceAll("മേ", "ta")
        .replaceAll("മ", "a")
        .replaceAll("ല്യ", "ey")
        .replaceAll("ര്യ", "cy")
        .replaceAll("ഷ്യ", "jy")
        .replaceAll("ഹ്യ", "ly")
        .replaceAll("സ്യേ", "tky")
        .replaceAll("സ്യ", "ky")
        .replaceAll("ശ്യ", "iy")
        .replaceAll("വ്യ", "hy")
        .replaceAll("യ്യൈ", "ss¿")
        .replaceAll("യ്യെ", "s¿")
        .replaceAll("യോ", "tbm")
        .replaceAll("യൈ", "ssb")
        .replaceAll("യെ", "sb")
        .replaceAll("യ്യേ", "t¿")
        .replaceAll("യ്യ", "¿")
        .replaceAll("യേ", "tb")
        .replaceAll("യ", "b")
        .replaceAll("ഹ്ര", "{l")
        .replaceAll("സ്രേ", "t{k")
        .replaceAll("സ്ര", "{k")
        .replaceAll("വ്ര", "{h")
        .replaceAll("ശ്രേ", "t{i")
        .replaceAll("ശ്ര", "{i")
        .replaceAll("രൈ", "ssc")
        .replaceAll("രൊ", "scm")
        .replaceAll("രെ", "sc")
        .replaceAll("ര്വ", "cz")
        .replaceAll("ര്&zwj;", "À")
        .replaceAll("ര്‍", "À")
        .replaceAll("ര്യ", "cy")
        .replaceAll("രോ", "tcm")
        .replaceAll("രേ", "tc")
        .replaceAll("ര", "c")
        .replaceAll("റ്റൈ", "ssä")
        .replaceAll("റ്റെ", "sä")
        .replaceAll("റ്റേ", "tä")
        .replaceAll("റ്റോ", "täm")
        .replaceAll("റ്റ", "ä")
        .replaceAll("റൈ", "ssd")
        .replaceAll("റെ", "sd")
        .replaceAll("റേ", "td")
        .replaceAll("റ", "d")
        .replaceAll("ഹ്ലെ", "sË")
        .replaceAll("ഹ്ലേ", "tË")
        .replaceAll("ഹ്ല", "Ë")
        .replaceAll("ശ്ലെ", "sÇ")
        .replaceAll("ശ്ലേ", "tÇ")
        .replaceAll("ശ്ല", "Ç")
        .replaceAll("ല്ലൈ", "ssÃ")
        .replaceAll("ല്ലൊ", "sÃm")
        .replaceAll("ല്ലെ", "sÃ")
        .replaceAll("ലൈ", "sse")
        .replaceAll("ലൊ", "sem")
        .replaceAll("ലെ", "se")
        .replaceAll("ല്ല്വ", "Ãz")
        .replaceAll("ല്ല", "Ã")
        .replaceAll("ല്&zwj;", "Â")
        .replaceAll("ല്‍", "Â")
        .replaceAll("ല്ല്യ", "Ãy")
        .replaceAll("ല്ലോ", "tÃm")
        .replaceAll("ല്ലേ", "tÃ")
        .replaceAll("ല്ല", "Ã")
        .replaceAll("ലോ", "tem")
        .replaceAll("ലേ", "te")
        .replaceAll("ല", "e")
        .replaceAll("ള്ള", "Å")
        .replaceAll("ള്&zwj;", "Ä")
        .replaceAll("ള്‍", "Ä")
        .replaceAll("ള്ളൈ", "ssÅ")
        .replaceAll("ള്ളെ", "sÅ")
        .replaceAll("ളൈ", "ssf")
        .replaceAll("ളെ", "sf")
        .replaceAll("ള്ള", "Å")
        .replaceAll("ള്ളേ", "tÅ")
        .replaceAll("ളേ", "tf")
        .replaceAll("ള", "f")
        .replaceAll("ഴൈ", "ssg")
        .replaceAll("ഴെ", "sg")
        .replaceAll("ഴേ", "tg")
        .replaceAll("ഴ", "g")
        .replaceAll("ഷ്വൈ", "ssjz")
        .replaceAll("ഷ്വെ", "sjz")
        .replaceAll("ഷ്വ", "jz")
        .replaceAll("ഹ്വ", "lz")
        .replaceAll("സ്വേ", "tkz")
        .replaceAll("സ്വൈ", "sskz")
        .replaceAll("സ്വെ", "skz")
        .replaceAll("സ്വ", "kz")
        .replaceAll("വ്വൈ", "ssÆ")
        .replaceAll("വ്വെ", "sÆ")
        .replaceAll("വൈ", "ssh")
        .replaceAll("വെ", "sh")
        .replaceAll("ശ്വേ", "tiz")
        .replaceAll("ശ്വ", "iz")
        .replaceAll("വ്വേ", "tÆ")
        .replaceAll("വ്വ", "Æ")
        .replaceAll("വേ", "th")
        .replaceAll("വോ", "thm")
        .replaceAll("വ", "h")
        .replaceAll("ശ്ശൈ", "ssÈ")
        .replaceAll("ശ്ശെ", "sÈ")
        .replaceAll("ശൈ", "ssi")
        .replaceAll("ശൊ", "sim")
        .replaceAll("ശെ", "si")
        .replaceAll("ശ്രേ", "t{i")
        .replaceAll("ശ്ര", "{i")
        .replaceAll("ശ്ശേ", "tÈ")
        .replaceAll("ശ്ശ", "È")
        .replaceAll("ശോ", "tim")
        .replaceAll("ശേ", "ti")
        .replaceAll("ശ", "i")
        .replaceAll("ഷൈ", "ssj")
        .replaceAll("ഷെ", "sj")
        .replaceAll("ഷേ", "tj")
        .replaceAll("ഷ", "j")
        .replaceAll("സ്സൈ", "ssÊ")
        .replaceAll("സ്സെ", "sÊ")
        .replaceAll("സൈ", "ssk")
        .replaceAll("സെ", "sk")
        .replaceAll("സ്സ", "Ê")
        .replaceAll("സ്സേ", "tÊ")
        .replaceAll("സേ", "tk")
        .replaceAll("സോ", "tkm")
        .replaceAll("സ", "k")
        .replaceAll("ഹൈ", "ssl")
        .replaceAll("ഹെ", "sl")
        .replaceAll("ഹേ", "tl")
        .replaceAll("ഹ", "l")
        .replaceAll("ാ", "m")
        .replaceAll("ി", "n")
        .replaceAll("ീ", "o")
        .replaceAll("ു", "p")
        .replaceAll("ൂ", "q")
        .replaceAll("ഃ", "x")
        .replaceAll("്", "v")
        .replaceAll("ം", "w")
        .replaceAll("ൗ", "u")
        .replaceAll("ൃ", "r");
  }
}
