import 'package:flutter/material.dart';

class MemHeader extends StatelessWidget {
  final bool homeScreen;

  MemHeader({this.homeScreen});

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Container(
        height: screenSize.height * 0.25,
//        decoration: BoxDecoration(
//          gradient: LinearGradient(
//            begin: Alignment(0, -1.0),
//            end: Alignment(
//                0.0, 1.0), // 10% of the width, so there are ten blinds.
//            colors: [
//              Color(0xFFb60503),
//              Theme.of(context).backgroundColor
//            ], // whitish to gray
//            tileMode: TileMode.repeated, // repeats the gradient over the canvas
//          ),
//        ),
        child: Container(
            margin: EdgeInsets.symmetric(
                vertical: homeScreen
                    ? screenSize.height * 0.05
                    : screenSize.height * 0.05,
                horizontal: screenSize.width * 0.2),
            child: Image.asset(
              "assets/logo_drawer.png",
              fit: BoxFit.contain,
            )));
  }
}
