import 'package:flutter/material.dart';

class AnimatedLogo extends AnimatedWidget {
  // Make the Tweens static because they don't change.

  AnimatedLogo({Key key, @required Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    final _opacityTween = Tween<double>(begin: 1, end: 0.1);
    final _colorTween =
        ColorTween(begin: Colors.white, end: Theme.of(context).backgroundColor);
    final _sizeTween = Tween<double>(begin: 120, end: 50);

    return Container(
        decoration: BoxDecoration(
          color: _colorTween.evaluate(animation),
        ),
        child: Center(
          child: Opacity(
            opacity: _opacityTween.evaluate(animation),
            child: Container(
              decoration: new BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/logo_drawer.png"),
                    fit: BoxFit.cover,
                    repeat: ImageRepeat.noRepeat),
              ),
              margin: EdgeInsets.symmetric(vertical: 10),
              height: _sizeTween.evaluate(animation),
              width: _sizeTween.evaluate(animation),
              //child: LogoWidget(),
            ),
          ),
        ));
  }
}

// #docregion LogoWidget
class LogoWidget extends StatelessWidget {
  // Leave out the height and width so it fills the animating parent
  Widget build(BuildContext context) => Image(
        image: AssetImage('assets/logo_drawer.png'),
      );
}
