// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:intl/intl.dart';
import 'package:memapp/app_state_container.dart';
import 'package:mem_models/mem_models.dart';
import 'dart:math';
import 'package:share/share.dart';
import 'package:recase/recase.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:memapp/screens/profile_screen.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:utility/src/dialogshower.dart' as DialogShower;
import 'package:cloud_firestore/cloud_firestore.dart';

class RosaryItem extends StatefulWidget {
  final DismissDirectionCallback onDismissed;
  final GestureTapCallback onTap;
  final ValueChanged<bool> onCheckboxChanged;
  final MEMmessage message;
  final AppState appState;
  final UserBloc userBloc;
  MemUser _user;
  Color headerColor;
  String litanyElement;
  String bibleVerse;
  final authorUser;
  bool visible = true;
  bool connectionStatus;

  RosaryItem(
      {Key key,
      this.onDismissed,
      this.onTap,
      this.onCheckboxChanged,
      this.message,
      this.appState,
      this.userBloc,
      visible,
      this.headerColor,
      this.litanyElement,
      this.bibleVerse,
      this.authorUser,
      this.connectionStatus})
      : super(key: MemKeys.messageItemWidget);

  @override
  State createState() => RosaryItemState();
}

class RosaryItemState extends State<RosaryItem> with TickerProviderStateMixin {
  //MessageItem({Key key}) : super(key: MemKeys.messageItemWidget);
  final _messageHeightLimit = 400;
  int _messageLength;
  Color _bibleMsgColor = Colors.transparent;
  double _bibleMsgHeight = 0.0;
  double _bibleMsgWidth = 0.0;
  AnimationController _controller;
  Animation<double> _frontScale;
  Animation<double> _backScale;
  MalTranslator malTranslator = MalTranslator();
  Animation<double> litanyAnimation;
  AnimationController litanyController;
  //Future<User> author;
//  final Connectivity _connectivity = Connectivity();
//  StreamSubscription<ConnectivityResult> _connectivitySubscription;
//  bool _connectionStatus;

  @override
  void initState() {
    super.initState();

    //author = widget.userBloc.authorUser(widget.message.authorID);

    _messageLength = _messageHeightLimit;
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );
    _frontScale = Tween(
      begin: 1.0,
      end: 0.0,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(0.0, 0.5, curve: Curves.easeIn),
    ));
    _backScale = CurvedAnimation(
      parent: _controller,
      curve: Interval(0.5, 1.0, curve: Curves.easeOut),
    );

    litanyController =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    litanyAnimation =
        CurvedAnimation(parent: litanyController, curve: Curves.easeIn)
          ..addStatusListener((status) {});
    litanyController.forward();
  }

  @override
  void dispose() {
    litanyController.dispose();
    _controller.dispose();
    super.dispose();
  }

  bool isColorDark(Color color) {
    double darkness = 1 -
        (0.299 * color.red + 0.587 * color.green + 0.114 * color.blue) / 255;
    if (darkness < 0.3) {
      return false; // It's a light color
    } else {
      return true; // It's a dark color
    }
  }

  @override
  Widget build(BuildContext context) {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    bool _isRosaryAdmin = container.state.userAcl != null
        ? container.state.userDetails.isSuperAdmin &&
            container.state.userAcl.rosaryDelete
        : false;
    bool _isMessageAdmin = container.state.userAcl != null
        ? container.state.userDetails.isSuperAdmin &&
            container.state.userAcl.messageDelete
        : false;

    bool _eligibleToDelete = false;

    _eligibleToDelete = _isRosaryAdmin && widget.message.complete ||
        widget.message.authorID == container.state.userDetails.id &&
            widget.message.complete;

    return Container(
        width: MediaQuery.of(context).size.width * 0.6,
        height: MediaQuery.of(context).size.height * 0.11,
        margin: EdgeInsets.all(MediaQuery.of(context).size.height * 0.005),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(100.0),
                bottomLeft: Radius.circular(100.0),
                topRight: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0)),

            //color: colors[colorRandom.nextInt(colors.length)],
            color: Color.fromARGB(242, 242, 242, 242),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black26,
                  offset: Offset(1.0, 1.0),
                  blurRadius: 0.3,
                  spreadRadius: 0.3)
            ]),
        child: Stack(
          children: <Widget>[
            Align(
                alignment: Alignment.centerLeft,
                child: Container(
                    margin: EdgeInsets.all(
                        MediaQuery.of(context).size.width * 0.01),
                    child: FutureBuilder(
                        future: userBloc.authorUser(widget.message.authorID),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            return CircleAvatar(
                              radius: MediaQuery.of(context).size.height * 0.05,
                              backgroundImage: CachedNetworkImageProvider(snapshot
                                          .data.profilePhotoUrl ??
                                      "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a") ??
                                  NetworkImage(
                                      snapshot.data.profilePhotoUrl ?? "") ??
                                  AssetImage("assets/placeholder-face.png"),
                            );
                          } else
                            return CircleAvatar(
                              radius: MediaQuery.of(context).size.height * 0.04,
                              backgroundImage:
                                  AssetImage("assets/placeholder-face.png"),
                            );
                        })
//                    child: CircleAvatar(
//                      radius: MediaQuery.of(context).size.width * 0.09,
//                      backgroundImage: CachedNetworkImageProvider(widget
//                                  .authorUser.profilePhotoUrl ??
//                              "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a") ??
//                          NetworkImage(
//                              widget.authorUser.profilePhotoUrl ?? "") ??
//                          AssetImage("assets/placeholder-face.png"),
//                    )

                    )),
            Positioned(
                top: 5.0,
                left: MediaQuery.of(context).size.width * 0.26,
                child: Container(
                  child: Text(
                    DateFormat.yMMMd().format(widget.message.time),
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 8.0),
                  ),
                )),
            Positioned(
                top: 5.0,
                left: MediaQuery.of(context).size.width * 0.48,
                child: Container(
                  child: Text(
                    new DateFormat.jm().format(widget.message.time),
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 8.0),
                  ),
                )),
            Positioned(
                bottom: MediaQuery.of(context).size.width * 0.015,
                right: MediaQuery.of(context).size.width * 0.10,
                child: StreamBuilder(
                      stream: FirebaseFirestore.instance
                          .collection('mem')
                          .doc('rosary')
                          .collection('users')
                          .doc(widget.message.authorID)
                          .collection('badges')
                          .where('action', isEqualTo: 'ADD ROSARY')
                          .snapshots(),
                      builder: (BuildContext context, snapshot) {
                        if (snapshot.hasError) return Container();
                        if (!snapshot.hasData || snapshot.data == null) {
                          return Container();
                        } else {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return SizedBox(
                                  width: 20.0,
                                  height: 20.0,
                                  child: CircularProgressIndicator(
                                      strokeWidth: 1.0,
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.black26)));

                            default:
                              try {
                                return Stack(children: <Widget>[
                                  Image.asset(
                                    "assets/crown_" +
                                        snapshot.data.documents.last["level"]
                                            .toString() +
                                        ".png",
                                    height: MediaQuery.of(context).size.height *
                                        0.03,
                                  ),
//                                                        Positioned(
//                                                            top: 6.0,
//                                                            left: 5.0,
//                                                            child: Icon(
//                                                                Icons
//                                                                    .brightness_1,
//                                                                size: 10.0,
//                                                                color: Colors
//                                                                    .redAccent)),
//                                                        Positioned(
//                                                          top: 7.0,
//                                                          left: 8.0,
//                                                          child: Text(
//                                                            snapshot
//                                                                .data
//                                                                .documents
//                                                                .last["level"]
//                                                                .toString(),
//                                                            style: TextStyle(
//                                                                fontSize: 8.0,
//                                                                color: Colors
//                                                                    .white),
//                                                          ),
//                                                        )
                                ]);
                              } catch (e) {
                                return Container();
                              }
                          }
                        }
                      },
                    ) ??
                    Container()),
            Positioned(
                bottom: MediaQuery.of(context).size.width * 0.018,
                right: MediaQuery.of(context).size.width * 0.04,
                child: StreamBuilder(
                      stream: FirebaseFirestore.instance
                          .collection('mem')
                          .doc('rosary')
                          .collection('users')
                          .doc(widget.message.authorID)
                          .collection('badges')
                          .where('action', isEqualTo: 'COLLECT ROSARY')
                          .snapshots(),
                      builder: (BuildContext context, snapshot) {
                        if (snapshot.hasError) return Container();
                        if (!snapshot.hasData || snapshot.data == null) {
                          return Container();
                        } else {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return Center(
                                  child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.transparent)));

                            default:
                              try {
                                return Stack(children: <Widget>[
                                  Image.asset(
                                    "assets/star_" +
                                        snapshot.data.documents.last["level"]
                                            .toString() +
                                        ".png",
                                    height: MediaQuery.of(context).size.height *
                                        0.026,
                                  ),
//                                                        Positioned(
//                                                            top: 6.0,
//                                                            left: 5.0,
//                                                            child: Icon(
//                                                                Icons
//                                                                    .brightness_1,
//                                                                size: 10.0,
//                                                                color: Colors
//                                                                    .redAccent)),
//                                                        Positioned(
//                                                          top: 7.0,
//                                                          left: 8.0,
//                                                          child: Text(
//                                                            snapshot
//                                                                .data
//                                                                .documents
//                                                                .last["level"]
//                                                                .toString(),
//                                                            style: TextStyle(
//                                                                fontSize: 8.0,
//                                                                color: Colors
//                                                                    .white),
//                                                          ),
//                                                        )
                                ]);
                              } catch (e) {
                                return Container();
                              }
                          }
                        }
                      },
                    ) ??
                    Container()),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.06,
              left: MediaQuery.of(context).size.width * 0.25,
              child: widget.message.rosaryCount == 0
                  ? Text(
                      widget.message.title.length > 20
                          ? widget.message.title.substring(0, 19) + "..."
                          : widget.message.title,
                      style: TextStyle(
                          fontSize: 10.0,
                          color: Color.fromRGBO(66, 65, 63, 1.0)),
                    )
                  : Container(),
            ),
            Positioned(
                top: widget.message.rosaryCount == 0
                    ? MediaQuery.of(context).size.height * 0.08
                    : MediaQuery.of(context).size.height * 0.06,
                left: MediaQuery.of(context).size.width * 0.25,
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Text(
                      widget.message.author.length > 15
                          ? widget.message.author.substring(0, 14) + "..."
                          : widget.message.author,
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Color.fromRGBO(66, 65, 63, 1.0)),
                    ))),
            Positioned(
              // draw a red marble
              top: MediaQuery.of(context).size.height * 0.04,
              right: MediaQuery.of(context).size.width * 0.10,
              child: widget.message.complete
                  ? Container()
                  : Icon(Icons.brightness_1,
                      size: 8.0, color: Colors.redAccent),
            ),
            Positioned(
                top: MediaQuery.of(context).size.height * 0.04,
                left: MediaQuery.of(context).size.width * 0.25,
                child: Column(
                  children: <Widget>[
                    Container(
//                    margin: EdgeInsets.only(
//                        top: MediaQuery.of(context).size.height * 0.05,
//                        left: MediaQuery.of(context).size.width * 0.25),
                        child: widget.message.type == "rosary"
                            ? widget.message.rosaryCount == 1 ||
                                    widget.message.rosaryByOthers == 1
                                ? Text(
                                    widget.message.rosaryCount == 0
                                        ? widget.message.rosaryByOthers
                                                .toString() +
                                            " ROSARY"
                                        : widget.message.rosaryCount
                                                .toString() +
                                            " ROSARY",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 12.0,
                                        color: Color.fromRGBO(66, 65, 63, 1.0)),
                                  )
                                : Text(
                                    widget.message.rosaryCount == 0
                                        ? widget.message.rosaryByOthers
                                                .toString() +
                                            " ROSARIES"
                                        : widget.message.rosaryCount
                                                .toString() +
                                            " ROSARIES",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 12.0,
                                        color: Color.fromRGBO(66, 65, 63, 1.0)),
                                  )
                            : Container())
                  ],
                )),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.02,
              right: 2.0,
              child: _eligibleToDelete
                  ? IconButton(
                      iconSize: 18.0,
                      color: Colors.grey,
                      key: MemKeys.deleteMessageButton,
                      tooltip: MemLocalizations(Localizations.localeOf(context))
                          .deleteMessage,
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        if (widget.connectionStatus) {
                          confirmDelete(
                            errorMsg: MemError(
                                title:
                                    "Are you sure ? Please confirm to delete",
                                body:
                                    "You are deleting a message. Please press Delete to confirm delete."),
                          );
                        } else {
                          showError(
                            errorMsg: MemError(
                                title: "Not Connected",
                                body:
                                    "Please check your internet connection. You can delete a message only when you are online."),
                          );
                        }

                        //Navigator.pop(context, message);
                      },
                    )
                  : Container(),
            )
          ],
        ));
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
        title: errorMsg.title,
        message: errorMsg.body,
        confirm: "Okay",
        confirmFn: () {
          Navigator.of(context, rootNavigator: true).pop();
        });
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void confirmDelete({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
        title: errorMsg.title,
        message: errorMsg.body,
        cancel: "cancel",
        confirm: "Delete",
        confirmFn: () {
          _removeMessage(context, widget.message);
          Navigator.of(context, rootNavigator: true).pop();
        });
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void _removeMessage(BuildContext context, MEMmessage message) {
    final messageBloc = MessageProvider.of(context);

    var container = AppStateContainer.of(context);
    String _deleteMessage;
    String _undoButtonMessage;
    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
        .messageDeleted(message.title);
    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;

    bool _isAdmin = container.state.userDetails.role == "admin";
    bool _isRosaryAdmin = container.state.userDetails.isSuperAdmin &&
        container.state.userAcl.rosaryDelete;
    bool _eligibleToDelete =
        _isRosaryAdmin || message.authorID == container.state.userDetails.id;
    debugPrint("Eligible to delete : " + _eligibleToDelete.toString());
//    debugPrint("Author ID: " + message.authorID);
//    debugPrint("User ID: " +
//        container.state.userDetails.id +
//        "Username" +
//        container.state.userDetails.name);

    if (_eligibleToDelete) {
      switch (message.type) {
        case "rosary":
          int deletedCount = 0;
          message.rosaryCount != 0
              ? deletedCount = message.rosaryCount
              : deletedCount = message.rosaryByOthers;
          messageBloc.removeRosaryMessage(message, container.state.userDetails);
          _deleteMessage = "Rosary Count reduced by " + deletedCount.toString();
          _undoButtonMessage = "";
          break;

        case "request":
          messageBloc.removeMessage(message);
          _deleteMessage = "Prayer Request Deleted";
          _undoButtonMessage = "";
          break;
        case "testimony":
          debugPrint("Deleted Testimony :" + message.id + message.author);
          messageBloc.removeMessage(message);
          _deleteMessage = "Testimony Deleted";
          _undoButtonMessage = "";
          break;
        case "guidance":
          debugPrint("Deleted Gudance :" + message.id + message.author);
          messageBloc.removeMessage(message);
          _deleteMessage = "Counselling Request Deleted";
          _undoButtonMessage = "";
          break;
        case "admin":
          messageBloc.removeRegionalMessage(
              message, container.state.userDetails);
          _deleteMessage = "Sorry, You cannot delete this message";
          _undoButtonMessage = "";
          break;
      }
    } else {
      _deleteMessage = "Sorry, You cannot delete this message";
      _undoButtonMessage = "";
    }

    Scaffold.of(context).showSnackBar(
      SnackBar(
        key: MemKeys.snackbar,
        duration: Duration(seconds: 3),
        backgroundColor: Theme.of(context).errorColor,
        content: Text(
          _deleteMessage,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.white),
        ),
        action: SnackBarAction(
          label: _undoButtonMessage,
          onPressed: () {
            messageBloc.messageRepository.addRegionalMessage(
                message.toEntity(), container.state.userDetails.region);
            //msgItem.visible = true;
            //addMessage(message);
          },
        ),
      ),
    );
  }
}

class AnimatedTextElement extends StatelessWidget {
  // Make the Tweens static because they don't change.
  static final _opacityTween = Tween<double>(begin: 0, end: 1);
  final String text = MemLists().litanyEN;

//  AnimatedTextElement({Key key, Animation<double> animation})
//      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    //final Animation<double> animation = listenable;
    return Center(
      child: Opacity(
        opacity: 1.0,
        //opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
//          child: Text(text.length > 40 ? text.substring(0, 40) + "..." : text,
//              textAlign: TextAlign.center,
//              maxLines: 2,
//              style: TextStyle(
//                fontSize: 11.0,
//              )),
          child: LitanyElement(text: MemLists().litanyEN),
        ),
      ),
    );
  }
}

class LitanyElement extends StatelessWidget {
  final String text;

  LitanyElement({this.text}) : super();
//  AnimatedTextElement({Key key, Animation<double> animation})
//      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    //final Animation<double> animation = listenable;
    return Text(text.length > 40 ? text.substring(0, 40) + "..." : text,
        textAlign: TextAlign.center,
        maxLines: 2,
        style: TextStyle(
          fontSize: 11.0,
        ));
  }
}
