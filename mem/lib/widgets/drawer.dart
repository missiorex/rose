import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:base_helpers/basehelpers.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memapp/app_state_container.dart';
import 'package:memapp/screens/about_us.dart';
import 'package:memapp/widgets/RestartWidget.dart';
import 'package:memapp/screens/bishops_message_screen.dart';
import 'package:memapp/screens/videos.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:recase/recase.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:memapp/screens/profile_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:memapp/screens/consecration.dart';
import 'memLabel.dart';
import 'memHeader.dart';

class MemDrawer extends StatefulWidget {
  final FirebaseStorage storage;
  final bool connectionStatus;
  final RouteObserver<PageRoute> routeObserver;

  MemDrawer({Key key, this.storage, this.connectionStatus, this.routeObserver})
      : super(key: MemKeys.drawer);

  @override
  MemDrawerState createState() => MemDrawerState();
}

class MemDrawerState extends State<MemDrawer> {
  UserBloc userBloc;

  @override
  Widget build(BuildContext context) {
    var container = AppStateContainer.of(context);
    AppState appState = container.state;

    final MessageBloc messageBloc = MessageProvider.of(context);
    userBloc = UserProvider.of(context);
    List<VideoTestimony> videoTestimonies = container.state.videoTestimonies;
    List<BishopsMessage> bishopsMessages = container.state.bishopsMessages;
    List<Benefactor> benefactors = container.state.benefactorMessages;
    BishopsMessage mainMessage;

    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    //Drawer Dimensions
    var drawerWidth = MediaQuery.of(context).size.width * 0.8;
    var drawerHeaderHeight = MediaQuery.of(context).size.height * 0.35;
    var userNameFontSize = MediaQuery.of(context).size.width * 0.04;
    var drawerIconWidth = MediaQuery.of(context).size.width * 0.08;
    var drawerIconFontSize = MediaQuery.of(context).size.width * 0.036;
    var drawerElementPadding = isTablet
        ? MediaQuery.of(context).size.height * 0.012
        : MediaQuery.of(context).size.height * 0.0006;
    var drawerIconMargin = isTablet
        ? MediaQuery.of(context).size.width * 0.02
        : MediaQuery.of(context).size.width * 0.02;
    var appTitleMainFontSize = MediaQuery.of(context).size.height * 0.035;
    var appTitleSubFontSize = MediaQuery.of(context).size.height * 0.029;
    var appTitleTopPosition = MediaQuery.of(context).size.height * 0.05;
    var appTitleLeftPosition = drawerWidth * 0.25;
    var appSubTitleLeftPosition = drawerWidth * 0.2;
    var appSubTitleTopPosition = appTitleTopPosition * 0.8;
    var drawerTitleStyle = Theme.of(context).textTheme.headline3.copyWith(
        fontSize: drawerIconFontSize, color: Theme.of(context).backgroundColor);
    var drawerIconColor = Theme.of(context).backgroundColor;
    var drawerItemBorderColor = Theme.of(context).primaryIconTheme.color;

    messageBloc.messageRepository.getBishopsMessage().then((message) {
      mainMessage = BishopsMessage.fromEntity(message);
    });

    return SizedBox(
        width: drawerWidth,
        child: Drawer(
          // Add a ListView to the drawer. This ensures the user can scroll
          // through the options in the Drawer if there isn't enough vertical
          // space to fit everything.
          child: Container(
              color: Theme.of(context).primaryColorLight,
//              decoration: BoxDecoration(
//                gradient: LinearGradient(
//                  begin: Alignment(0, -1.0),
//                  end: Alignment(
//                      0.0, 1.0), // 10% of the width, so there are ten blinds.
//                  colors: [
//                    Theme.of(context).backgroundColor,
////                    Color(0xFFb60503),
//                    Theme.of(context).primaryColorLight
//                  ], // whitish to gray
//                  tileMode:
//                      TileMode.repeated, // repeats the gradient over the canvas
//                ),
//              ),
              child: ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment(0, -1.0),
                          end: Alignment(0.0,
                              1.0), // 10% of the width, so there are ten blinds.
                          colors: [
                            Color(0xFFb60503),
                            Theme.of(context).backgroundColor,
                          ], // whitish to gray
                          tileMode: TileMode
                              .repeated, // repeats the gradient over the canvas
                        ),
                      ),
                      height: drawerHeaderHeight,
                      child: DrawerHeader(
                        margin: EdgeInsets.only(bottom: 0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            MemHeader(
                              homeScreen: false,
                            ),
                            Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.10,
                                padding: EdgeInsets.only(top: 3.0, bottom: 0.0),
                                color: Theme.of(context).primaryColor,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                          Container(
                                              child: Text(
                                            ReCase(userBloc.userDetails
                                                        .displayName ??
                                                    userBloc.userDetails.name)
                                                .titleCase,
                                            style: TextStyle(
                                                fontFamily: "Bahnschrift",
                                                fontWeight: FontWeight.bold,
                                                color: Theme.of(context)
                                                    .backgroundColor,
                                                fontSize: userNameFontSize),
                                          )),
                                          Container(
                                            child: userBloc.userDetails.role !=
                                                    "guest"
                                                ? Text(
                                                    " | " +
                                                        ReCase(userBloc
                                                                .userDetails
                                                                .region)
                                                            .titleCase,
                                                    style: TextStyle(
                                                        fontFamily:
                                                            "Bahnschrift",
                                                        fontSize:
                                                            userNameFontSize *
                                                                0.8,
                                                        color: Theme.of(context)
                                                            .backgroundColor),
                                                  )
                                                : Container(),
                                            //padding: EdgeInsets.only(right: 2.0),
                                          ),
                                        ])),
                                    Container(
                                        padding: EdgeInsets.only(left: 2.0),
                                        child: FlatButton(
                                          child: userBloc.userDetails.role !=
                                                  "guest"
                                              ? profilePhoto()
                                              : Container(),
                                          onPressed: () async {
                                            MemUser user =
                                                await userBloc.userProfile(
                                                    userBloc.userDetails.id);

                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      ProfileScreen(
                                                    key: MemKeys.profileScreen,
                                                    member: user,
                                                    storage: widget.storage,
                                                    avatarTag: 1,
                                                  ),
                                                ));
                                          },
                                        )),
                                  ],
                                ))
                          ],
                        ),
                        padding: EdgeInsets.only(left: 0.0),
                      )),
                  Container(
                      child: userBloc.userDetails.role != "guest"
                          ? Column(children: <Widget>[
                              Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: drawerItemBorderColor,
                                              width: 1.0,
                                              style: BorderStyle.solid))),
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.all(drawerElementPadding),
                                    leading: Container(
                                        padding: EdgeInsets.only(
                                            left: 5.0, top: 5.0, bottom: 5.0),
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                left: drawerIconMargin),
                                            child: Image(
                                                image: AssetImage(
                                                    "assets/icons/about-us.png"),
                                                width: drawerIconWidth,
                                                height: drawerIconWidth,
                                                color: drawerIconColor))),
                                    enabled: true,
                                    title: Text('About Us',
                                        style: drawerTitleStyle),
                                    onTap: () {
                                      // Update the state of the app
                                      // ...
                                      // Then close the drawer
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => AboutScreen(
                                              key: MemKeys.aboutScreen,
                                              benefactors: benefactors,
                                              storage: widget.storage,
                                            ),
                                          ));
//                        Navigator.pop(context);
                                    },
                                  )),
                              Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: drawerItemBorderColor,
                                              width: 1.0,
                                              style: BorderStyle.solid))),
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.all(drawerElementPadding),
                                    enabled: true,
                                    leading: Container(
                                        padding: EdgeInsets.only(
                                            left: 5.0, top: 5.0, bottom: 5.0),
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                left: drawerIconMargin),
                                            child: Image(
                                                image: AssetImage(
                                                    "assets/icons/bishop.png"),
                                                width: drawerIconWidth,
                                                height: drawerIconWidth,
                                                color: drawerIconColor))),
                                    title: Container(
                                        child: Text('Bishop\'s Message',
                                            style: drawerTitleStyle)),
                                    onTap: () {
                                      debugPrint(
                                        "tapped bishop's Message",
                                      );
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                BishopMessageScreen(
                                              key: MemKeys.bishopsMessageScreen,
                                              patronsMessages: bishopsMessages,
                                              mainMessage: mainMessage,
                                              storage: widget.storage,
                                            ),
                                          ));

                                      // Update the state of the app
                                      // ...
                                      // Then close the drawer
                                      //Navigator.pop(context);
                                    },
                                  )),
                              Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: drawerItemBorderColor,
                                              width: 1.0,
                                              style: BorderStyle.solid))),
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.all(drawerElementPadding),
                                    enabled: true,
                                    leading: Container(
                                      padding: EdgeInsets.only(
                                          left: 5.0, top: 5.0, bottom: 5.0),
                                      child: Container(
                                          margin: EdgeInsets.only(
                                              left: drawerIconMargin),
                                          child: ImageIcon(
                                              AssetImage(
                                                  'assets/icons/video.png'),
                                              size: drawerIconWidth,
                                              color: drawerIconColor)),
                                    ),
                                    title:
                                        Text("Videos", style: drawerTitleStyle),
                                    onTap: () {
                                      debugPrint("Tapped Videos");
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => VideoScreen(),
                                          ));
                                    },
                                  )),
                              Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: drawerItemBorderColor,
                                              width: 1.0,
                                              style: BorderStyle.solid))),
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.all(drawerElementPadding),
                                    leading: Container(
                                        padding: EdgeInsets.only(
                                            left: 5.0, top: 5.0, bottom: 5.0),
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                left: drawerIconMargin),
                                            child: ImageIcon(
                                                AssetImage(
                                                    "assets/icons/consecration.png"),
                                                size: drawerIconWidth,
                                                color: drawerIconColor))),
                                    enabled: true,
                                    title: Text(
                                      userBloc.userDetails.locale == "Malayalam"
                                          ? "വിമലഹൃദയപ്രതിഷ്ഠ"
                                          : "Marian Consecration",
                                      style: drawerTitleStyle,
                                    ),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                ConsecrationScreen(),
                                          ));
                                    },
                                  )),
                              Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: drawerItemBorderColor,
                                              width: 1.0,
                                              style: BorderStyle.solid))),
                                  child: ListTile(
                                    contentPadding:
                                        EdgeInsets.all(drawerElementPadding),
                                    leading: Container(
                                        padding: EdgeInsets.only(
                                            left: 5.0, top: 5.0, bottom: 5.0),
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                left: drawerIconMargin),
                                            child: ImageIcon(
                                              AssetImage(
                                                  "assets/icons/sign-out.png"),
                                              size: drawerIconWidth,
                                              color: drawerIconColor,
                                            ))),
                                    title: appState.user != null
                                        ? Text('Sign Out',
                                            style: drawerTitleStyle)
                                        : Text('Sign In',
                                            style: drawerTitleStyle),
                                    //selected: true,
                                    onTap: () async {
                                      _signOut(context);
                                    },
                                  )),
                              SizedBox(
                                  height: MediaQuery.of(context).size.height *
                                      0.18),
                              MemLabel(
                                labelColor: Theme.of(context).primaryColorDark,
                                fontSize: userNameFontSize * 0.9,
                              ),
                            ])
                          : Column(
                              children: <Widget>[
                                Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: drawerItemBorderColor,
                                                width: 1.0,
                                                style: BorderStyle.solid))),
                                    child: ListTile(
                                      leading: Container(
                                          padding: EdgeInsets.only(
                                              left: 5.0, top: 5.0, bottom: 5.0),
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  left: drawerIconMargin),
                                              child: ImageIcon(
                                                AssetImage(
                                                    "assets/cross-icon.png"),
                                                color: drawerIconColor,
                                              ))),
                                      title: Text("Signout and Register",
                                          style: drawerTitleStyle),
                                      //selected: true,
                                      onTap: () async {
                                        _signOut(context);
                                      },
                                    )),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.5),
                                MemLabel(
                                  labelColor:
                                      Theme.of(context).primaryColorDark,
                                  fontSize: userNameFontSize * 0.9,
                                ),
                              ],
                            )),
                ],
              )),
        ));
  }

  Widget profilePhoto() {
    var profilePhotoRadius = MediaQuery.of(context).size.width * 0.07;

    return FutureBuilder<ImageProvider>(
        future: _loadImage(),
        builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.active:
            case ConnectionState.none:
              return CircularProgressIndicator(
                  strokeWidth: 3.0,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black));
              break;
            case ConnectionState.done:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return CircleAvatar(
                  backgroundImage: snapshot.data,
                  radius: profilePhotoRadius,
                );
              break;
            default:
              return Container();
          }
        });
  }

  Future<ImageProvider> _loadImage() async {
    ImageProvider profilePhoto = CachedNetworkImageProvider(
            userBloc.userDetails.profilePhotoUrl ?? "") ??
        NetworkImage(userBloc.userDetails.profilePhotoUrl ?? "") ??
        AssetImage("assets/placeholder-face.png");

    return profilePhoto;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  _signOut(BuildContext context) {
    var container = AppStateContainer.of(context);
    var userBloc = UserProvider.of(context);
    var dialog = buildDialog(
        title: "Leaving?",
        message: "Are you sure you want to log out?",
        confirm: "Yes",
        confirmFn: () {
          if (userBloc.userDetails != null) {
            FirebaseAuth.instance.signOut();
            Navigator.of(context, rootNavigator: true).pop(context);

            //Restart the App, clearing the state.
            RestartWidget.restartApp(context);
          }
        },
        cancel: "No",
        cancelFn: () {
          Navigator.of(context, rootNavigator: true).pop();
          Navigator.pop(context);
        });

    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  undoSignOut(BuildContext context) async {
    final userBloc = UserProvider.of(context);

    //Initialise user state if user already once logged in.
    await userBloc.authRepository.initUser().then((user) async {
      if (user != null) {
        await userBloc.userRepository
            .userExists(user.uid)
            .then((userExists) async {
          if (userExists) {
            await userBloc.userActive(user.uid).then((isActive) {
              if (isActive) {
                AppStateContainer.of(context).state.user = user;
                debugPrint("User after undo signout : " + user.toString());
              }
            });
          }
        });
      }
    });
  }
}
