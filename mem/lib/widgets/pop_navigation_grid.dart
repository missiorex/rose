import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:memapp/widgets/logo_widget.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:memapp/widgets/memLabel.dart';
import 'package:utility/utility.dart';

import 'package:rive/rive.dart' as rive;
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

typedef NavigateToScreen();

class PopNavigationGrid extends StatelessWidget {
  final NavigateToScreen openBottomAddRosarySheet;
  final NavigateToScreen collectRosary;
  final NavigateToScreen addPrayerRequest;
  final NavigateToScreen addTestimony;
  final NavigateToScreen communityChat;
  final NavigateToScreen guidanceChat;
  final NavigateToScreen consecration;
  final NavigateToScreen videos;
  final NavigateToScreen home;
  final bool navigationReady;
  final rive.Artboard loaderArtboard;
  final Widget rosaryCount;

  final rive.SMIInput<bool> isLoading;

  PopNavigationGrid(
      {this.openBottomAddRosarySheet,
      this.collectRosary,
      this.rosaryCount,
      this.addPrayerRequest,
      this.addTestimony,
      this.communityChat,
      this.guidanceChat,
      this.consecration,
      this.videos,
      this.home,
      this.navigationReady,
      this.loaderArtboard,
      this.isLoading});

  SliverPersistentHeader logoHeader(Color color) {
    return SliverPersistentHeader(
      pinned: true,
      delegate: _SliverAppBarDelegate(
        minHeight: 100.0,
        maxHeight: 120.0,
        child: Container(color: color, child: Center(child: _logoWidget)),
      ),
    );
  }

  SliverPersistentHeader rosaryCountWidget(Color labelColor, Color bgColor) {
    return SliverPersistentHeader(
      pinned: true,
      delegate: _SliverAppBarDelegate(
        minHeight: 200.0,
        maxHeight: 200.0,
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          _rosaryCountLabel(labelColor),
          _rosaryCount(labelColor, bgColor),
          navigationReady
              ? IconButton(
                  tooltip: "Go to Home Screen",
                  icon: Icon(Icons.arrow_forward, color: Colors.white),
                  onPressed: () => home())
              : MemLabel(labelColor: labelColor)
        ]),
      ),
    );
  }

  Widget get _logoWidget {
    return Container(
      margin: EdgeInsets.only(top: 0, bottom: 10.0),
      height: 100.0,
      width: 100.0,
      child: LogoWidget(),
    );
  }

  Widget _rosaryCount(Color labelColor, Color bgColor) {
    return Container(
      margin: EdgeInsets.only(top: 5.0, bottom: 10.0),
      child: OutlineGradientButton(
        child: rosaryCount,
        gradient: LinearGradient(stops: [
          0,
          0.5,
          0.5,
          1
        ], colors: [
          bgColor,
          labelColor,
          labelColor,
          bgColor,
        ]),
        strokeWidth: 2,
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        backgroundColor: bgColor,
        elevation: 1,
        inkWell: true,
      ),
    );
  }

  Widget _rosaryCountLabel(Color labelColor) {
    return Container(
      margin: EdgeInsets.only(top: 30.0, bottom: 4.0),
      child: Text("Total Rosaries Offered",
          style: TextStyle(color: labelColor, fontSize: 14.0)),
    );
  }

  @override
  Widget build(BuildContext context) {
    double strokeWidth = 1.0;

    return Container(
        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.1),
        child: CustomScrollView(
          slivers: <Widget>[
            logoHeader(Theme.of(context).backgroundColor),
            SliverGrid.count(
              crossAxisSpacing: 0,
              mainAxisSpacing: 0,
              crossAxisCount: 3,
              children: <Widget>[
//
                GestureDetector(
                    onTap: () => Navigator.of(context)
                        .pushReplacementNamed(MemRoutes.campaignHome),
                    child: memIconButton(
                      image: AssetImage("assets/icons/virtual-chapel.png"),
                      label: "Virtual Chapel",
                      iconColor: Theme.of(context).backgroundColor,
                      labelColor: Theme.of(context).backgroundColor,
                      includeLoader: false,
                    )),
                RotatedBox(
                    quarterTurns: 3,
                    child: OutlineGradientButton(
                      child: RotatedBox(
                        quarterTurns: -3,
                        child: memIconButton(
                          image: AssetImage("assets/icons/prayer-request.png"),
                          label: "Prayer Request",
                          iconColor: Theme.of(context).backgroundColor,
                          labelColor: Theme.of(context).backgroundColor,
                          includeLoader: true,
                        ),
                      ),
                      gradient: LinearGradient(stops: [
                        0,
                        0.7,
                        0.7,
                        0.7,
                        1
                      ], colors: [
                        Theme.of(context).primaryColor,
                        Theme.of(context).backgroundColor,
                        Theme.of(context).backgroundColor,
                        Theme.of(context).backgroundColor,
                        Theme.of(context).backgroundColor,
                      ]),
                      strokeWidth: strokeWidth,
                      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                      backgroundColor: Theme.of(context).backgroundColor,
                      elevation: 4,
                      inkWell: true,
                      onTap: () => addPrayerRequest(),

//                      onTap: () => showSnack('onTap'),
//                      onDoubleTap: () => showSnack('onDoubleTap'),
//                      onLongPress: () => showSnack('onLongPress'),
                    )),
                GestureDetector(
                    onTap: () => addTestimony(),
                    child: memIconButton(
                      image: AssetImage("assets/icons/thanksgiving.png"),
                      label: "Thanksgiving",
                      iconColor: Theme.of(context).backgroundColor,
                      labelColor: Theme.of(context).backgroundColor,
                      includeLoader: true,
                    )),

                OutlineGradientButton(
                  child: memIconButton(
                    image: AssetImage("assets/icons/community.png"),
                    label: "Community",
                    iconColor: Theme.of(context).backgroundColor,
                    labelColor: Theme.of(context).backgroundColor,
                    includeLoader: true,
                  ),
                  gradient: LinearGradient(stops: [
                    0,
                    0.3,
                    0.3,
                    0.3,
                    1
                  ], colors: [
                    Theme.of(context).backgroundColor,
                    Theme.of(context).backgroundColor,
                    Theme.of(context).backgroundColor,
                    Theme.of(context).backgroundColor,
                    Theme.of(context).primaryColor,
                  ]),
                  strokeWidth: strokeWidth,
                  backgroundColor: Theme.of(context).backgroundColor,
                  elevation: 0,
                  padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                  inkWell: true,
                  onTap: () => communityChat(),
//                      onTap: () => showSnack('onTap'),
//                      onDoubleTap: () => showSnack('onDoubleTap'),
//                      onLongPress: () => showSnack('onLongPress'),
                ),
                GestureDetector(
                    onTap: () => openBottomAddRosarySheet(),
                    child: memIconButton(
                      image: AssetImage("assets/icons/offer-rosary.png"),
                      label: "Offer Rosary",
                      iconColor: Theme.of(context).accentColor,
                      labelColor: Theme.of(context).accentColor,
                      includeLoader: true,
                    )),

                OutlineGradientButton(
                  child: memIconButton(
                    image: AssetImage("assets/icons/collect-rosary.png"),
                    label: "Collect Rosary",
                    iconColor: Theme.of(context).backgroundColor,
                    labelColor: Theme.of(context).backgroundColor,
                    includeLoader: true,
                  ),
                  gradient: LinearGradient(stops: [
                    0,
                    0.7,
                    0.7,
                    0.7,
                    1
                  ], colors: [
                    Theme.of(context).primaryColor,
                    Theme.of(context).backgroundColor,
                    Theme.of(context).backgroundColor,
                    Theme.of(context).backgroundColor,
                    Theme.of(context).backgroundColor,
                  ]),
                  strokeWidth: strokeWidth,
                  padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                  backgroundColor: Color(0xFF7c0a03),
                  elevation: 4,
                  inkWell: true,
                  onTap: () => collectRosary(),
//                      onTap: () => showSnack('onTap'),
//                      onDoubleTap: () => showSnack('onDoubleTap'),
//                      onLongPress: () => showSnack('onLongPress'),
                ),
                GestureDetector(
                    onTap: () => videos(),
                    child: memIconButton(
                        image: AssetImage("assets/icons/video.png"),
                        label: "Videos",
                        iconColor: Theme.of(context).backgroundColor,
                        labelColor: Theme.of(context).backgroundColor,
                        includeLoader: true)),
                RotatedBox(
                    quarterTurns: 5,
                    child: OutlineGradientButton(
                      child: RotatedBox(
                        quarterTurns: -5,
                        child: memIconButton(
                          image: AssetImage("assets/icons/consecration2.png"),
                          label: "Consecration",
                          iconColor: Theme.of(context).backgroundColor,
                          labelColor: Theme.of(context).backgroundColor,
                          includeLoader: true,
                        ),
                      ),
                      gradient: LinearGradient(stops: [
                        0,
                        0.7,
                        0.7,
                        0.7,
                        1
                      ], colors: [
                        Theme.of(context).primaryColor,
                        Theme.of(context).backgroundColor,
                        Theme.of(context).backgroundColor,
                        Theme.of(context).backgroundColor,
                        Theme.of(context).backgroundColor,
                      ]),
                      strokeWidth: strokeWidth,
                      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                      backgroundColor: Color(0xFF7c0a03),
                      elevation: 0,
                      inkWell: true,
                      onTap: () => consecration(),
//                      onTap: () => showSnack('onTap'),
//                      onDoubleTap: () => showSnack('onDoubleTap'),
//                      onLongPress: () => showSnack('onLongPress'),
                    )),
                GestureDetector(
                    onTap: () {
                      if (!navigationReady) isLoading.value = true;
                      guidanceChat();
                    },
                    child: memIconButton(
                      image: AssetImage("assets/icons/admin.png"),
                      label: "Guidance",
                      iconColor: Theme.of(context).backgroundColor,
                      labelColor: Theme.of(context).backgroundColor,
                      includeLoader: true,
                    )),
              ],
            ),
            rosaryCountWidget(Theme.of(context).primaryColor,
                Theme.of(context).backgroundColor),
          ],
        ));
  }

  Widget memIconButton(
      {AssetImage image,
      String label,
      Color labelColor,
      Color iconColor,
      bool includeLoader}) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(2),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        padding: EdgeInsets.all(1),
        child: Center(
            child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
              Padding(
                  padding: EdgeInsets.all(2.0),
                  child: ImageIcon(image, color: iconColor, size: 35.0)),
              Text(label, style: TextStyle(fontSize: 10.0, color: labelColor)),
              loaderArtboard == null || !includeLoader || navigationReady
                  ? Container(height: 3.0)
                  : Container(
                      height: 3.0, child: rive.Rive(artboard: loaderArtboard))
            ])),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });
  final double minHeight;
  final double maxHeight;
  final Widget child;
  @override
  double get minExtent => minHeight;
  @override
  double get maxExtent => math.max(maxHeight, minHeight);
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}
