import 'package:flutter/material.dart';
import 'package:memapp/widgets/diagonal_coloured_image.dart';
import 'package:mem_models/mem_models.dart';
import 'package:intl/intl.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:utility/utility.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProfileDetailHeader extends StatelessWidget {
  static const BACKGROUND_IMAGE = 'assets/aboutus-banner.jpg';

  ProfileDetailHeader(this.member, this.avatarTag, this.profileUploadButton,
      this.editProfileButton, this._previewImage, this.profileImgLoading);

  MemUser member;
  MalTranslator malTranslator = MalTranslator();
  String bibleVerse =
      "\"നിങ്ങളുടെ സമ്പത്തു വിറ്റ് ദാനം ചെയ്യുവിന്‍.പഴകിപ്പോകാത്ത പണസഞ്ചികള്‍ കരുതിവയ്ക്കുവിന്‍. ഒടുങ്ങാത്തനിക്ഷേപം സ്വര്‍ഗത്തില്‍ സംഭരിച്ചുവയ്ക്കുവിന്‍. \nഅവിടെ കള്ളന്‍മാര്‍ കടന്നുവരുകയോ ചിതല്‍ നശിപ്പിക്കുകയോ ഇല്ല.\"        \nലൂക്കാ 12:33";
  String bibleVerseEN =
      "\"Sell your belongings and give alms. Provide money bags for yourselves  that do not wear out, an inexhaustible treasure in heaven  \nthat no thief can reach nor moth destroy.\"      \nLuke 12:33";

  final Object avatarTag;
  final Widget profileUploadButton;
  final Widget editProfileButton;
  final Widget _previewImage;
  final bool profileImgLoading;
  //final File profileImage;

  Widget _buildDiagonalImageBackground(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    return new DiagonallyCutColoredImage(
      new Image.asset(
        BACKGROUND_IMAGE,
        width: screenWidth,
        height: 190.0,
        fit: BoxFit.cover,
      ),
      color: Color.fromRGBO(11, 129, 140, 1.0),
    );
  }

  Widget _buildAvatar() {
//    return new Hero(tag: avatarTag, child: profilePhoto());
    return _previewImage;

    //return Hero(tag: avatarTag, child: _previewImage);
//        CircleAvatar(
//              backgroundImage: FileImage(profileImage),
//              radius: 50.0,
//            ) ??
//            CircleAvatar(
//          backgroundImage:
//              CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
//                  NetworkImage(member.profilePhotoUrl ?? "") ??
//                  AssetImage("assets/placeholder-face.png"),
//
  }

//  Widget profilePhoto() {
//    return FutureBuilder<ImageProvider>(
//        future: _loadImage(),
//        builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
//          if (profileImgLoading)
//            return SizedBox(
//                width: 80.0,
//                height: 80.0,
//                child: CircularProgressIndicator(
//                    strokeWidth: 1.0,
//                    valueColor: AlwaysStoppedAnimation<Color>(Colors.black26)));
//
//          switch (snapshot.connectionState) {
//            case ConnectionState.waiting:
//            case ConnectionState.active:
//            case ConnectionState.none:
//              return CircularProgressIndicator(
//                  backgroundColor: Colors.white70, strokeWidth: 1.5);
//            case ConnectionState.done:
//              if (snapshot.hasError)
//                return new Text('Error: ${snapshot.error}');
//              else
//                return CircleAvatar(
//                  backgroundImage: snapshot.data,
//                  radius: 50.0,
//                );
//          }
//        });
//  }
//
//  Future<ImageProvider> _loadImage() async {
//    ImageProvider profilePhoto =
//        CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
//            NetworkImage(member.profilePhotoUrl ?? "") ??
//            AssetImage("assets/placeholder-face.png");
//
//    return profilePhoto;
//  }

  Widget _buildBadgeDetails(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.4,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('mem')
                      .doc('rosary')
                      .collection('users')
                      .doc(member.id)
                      .collection('badges')
                      .where('action', isEqualTo: 'ADD ROSARY')
                      .snapshots(),
                  builder: (BuildContext context, snapshot) {
                    if (snapshot.hasError) return Container();
                    if (!snapshot.hasData || snapshot.data == null) {
                      return Container();
                    } else {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                          return SizedBox(
                              width: 20.0,
                              height: 20.0,
                              child: CircularProgressIndicator(
                                  strokeWidth: 1.0,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.black26)));

                        default:
                          try {
                            return Stack(children: <Widget>[
                              Image.asset(
                                "assets/crown_" +
                                    snapshot.data.docs.last["level"]
                                        .toString() +
                                    ".png",
                                height: 40.0,
                              ),
                              Positioned(
                                  top: 15.0,
                                  left: 14.0,
                                  child: Icon(Icons.brightness_1,
                                      size: 14.0, color: Colors.redAccent)),
                              Positioned(
                                top: 16.0,
                                left: 18.0,
                                child: Text(
                                  snapshot.data.docs.last["level"].toString(),
                                  style: TextStyle(
                                      fontSize: 10.0, color: Colors.white),
                                ),
                              )
                            ]);
                          } catch (e) {
                            return Container();
                          }
                      }
                    }
                  },
                ) ??
                Container(color: Colors.yellow, height: 30.0),
            StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('mem')
                      .doc('rosary')
                      .collection('users')
                      .doc(member.id)
                      .collection('badges')
                      .where('action', isEqualTo: 'ADD ROSARY')
                      .snapshots(),
                  builder: (BuildContext context, snapshot) {
                    if (snapshot.hasError)
                      return Container(height: 40.0, color: Colors.orange);
                    if (!snapshot.hasData || snapshot.data == null) {
                      return Container(height: 40.0, color: Colors.green);
                    } else {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                          return Center(
                              child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.green)));

                        default:
                          try {
                            return Stack(children: <Widget>[
                              Image.asset(
                                "assets/star_" +
                                    snapshot.data.docs.last["level"]
                                        .toString() +
                                    ".png",
                                height: 32.0,
                              ),
                              Positioned(
                                  top: 11.0,
                                  left: 10.0,
                                  child: Icon(Icons.brightness_1,
                                      size: 14.0, color: Colors.redAccent)),
                              Positioned(
                                top: 12.0,
                                left: 14.0,
                                child: Text(
                                  snapshot.data.docs.last["level"].toString(),
                                  style: TextStyle(
                                      fontSize: 10.0, color: Colors.white),
                                ),
                              )
                            ]);
                          } catch (e) {
                            return Container(height: 40.0, color: Colors.cyan);
                          }
                      }
                    }
                  },
                ) ??
                Container(height: 40.0, color: Colors.grey),
          ],
        ));
  }

  Widget _buildRosaryInfo(TextTheme textTheme) {
    var rosaryStyle =
        textTheme.subtitle2.copyWith(color: Color(0xFF7b7458), fontSize: 12.0);

    return Padding(
      padding: const EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Rosaries Offered: ' + member.rosariesOffered.toString(),
              style: rosaryStyle),
          Text(
            ' | ',
            style: rosaryStyle.copyWith(
                fontSize: 16.0, fontWeight: FontWeight.normal),
          ),
          Text(' Rosaries Collected: ' + member.rosariesCollected.toString(),
              style: rosaryStyle),
        ],
      ),
    );
  }

  Widget _buildTreasureVerse(TextTheme textTheme, BuildContext context) {
    var treasureVerseStyle = textTheme.subtitle2
        .copyWith(color: const Color(0xFF7b7458), fontSize: 14.0);

    UserBloc userBloc = UserProvider.of(context);

    return Padding(
      padding: const EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Theme.of(context).platform == TargetPlatform.iOS
              ? Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Text(
                    userBloc.userDetails.locale == "English"
                        ? bibleVerseEN
                        : malTranslator.formatMalContent(bibleVerse),
                    maxLines: 100,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: userBloc.userDetails.locale == "Malayalam"
                        ? treasureVerseStyle.copyWith(
                            fontSize: 12.0, fontFamily: 'Karthika')
                        : treasureVerseStyle.copyWith(fontSize: 14.0),
                  ))
              : Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Text(
                    userBloc.userDetails.locale == "English"
                        ? bibleVerseEN
                        : bibleVerse,
                    textAlign: TextAlign.center,
                    style: treasureVerseStyle,
                  )),
        ],
      ),
    );
  }

  Widget _buildTitle(
      {TextTheme textTheme, Color nameColor, Color regionColor}) {
    var nameStyle = textTheme.headline3.copyWith(
        color: nameColor, fontSize: 20.0, fontWeight: FontWeight.w900);
    var regionStyle = textTheme.headline4.copyWith(
        color: regionColor, fontSize: 16.0, fontWeight: FontWeight.w600);

    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          member.name,
          style: nameStyle,
        ),
        Text(
          ' | ',
          style: TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.w900, color: regionColor),
        ),
        Text(
          member.region,
          style: regionStyle,
        )
      ],
    ));
  }

  Widget _buildActionButtons(ThemeData theme, BuildContext context) {
    UserBloc userBloc = UserProvider.of(context);
    MemUser currentUser = userBloc.userDetails;

    return new Padding(
      padding: const EdgeInsets.only(
        top: 16.0,
        left: 20.0,
        right: 20.0,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          currentUser.id == member.id ? profileUploadButton : Container(),
          currentUser.id == member.id ? editProfileButton : Container(),
//          new DecoratedBox(
//            decoration: new BoxDecoration(
//              border: new Border.all(color: Colors.white30),
//              borderRadius: new BorderRadius.circular(30.0),
//            ),
//            child: _createPillButton(
//              'FOLLOW',
//              textColor: Colors.white70,
//            ),
//          ),
        ],
      ),
    );
  }

  Widget _buildProfileDetails(TextTheme textTheme, BuildContext context) {
    UserBloc userBloc = UserProvider.of(context);
    MemUser currentUser = userBloc?.userDetails;
    var titleStyle = textTheme.headline3.copyWith(
        color: Theme.of(context).backgroundColor,
        fontSize: 20.0,
        fontWeight: FontWeight.w900);
    var profileDetailsStyle = textTheme.subtitle2
        .copyWith(color: const Color(0xFF7b7458), fontSize: 14.0);

    return new Padding(
      padding: const EdgeInsets.only(
        top: 16.0,
        left: 20.0,
        right: 20.0,
      ),
      child: currentUser.id == member?.id
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Text(
                      "Profile Details",
                      style: titleStyle,
                    )),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Text(
                      "Name : ${member?.title} . ${member?.name} ",
                      style: profileDetailsStyle,
                    )),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Text(
                      "Display Name : ${member?.displayName} ",
                      style: profileDetailsStyle,
                    )),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Text(
                      "Locale : ${member?.locale} ",
                      style: profileDetailsStyle,
                    )),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Text(
                      "Phone Number : ${member?.phoneNumber} ",
                      style: profileDetailsStyle,
                    )),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Text(
                      "Member Since : ${DateFormat('dd-MM-yyyy').format(member?.userAddedOn)} ",
                      style: profileDetailsStyle,
                    )),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Text(
                      "Region : ${member?.region} ",
                      style: profileDetailsStyle,
                    )),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Text(
                      "Country : ${member?.country} ",
                      style: profileDetailsStyle,
                    )),
                SizedBox(
                  height: 5.0,
                ),
                member?.gender != null || member?.gender != ""
                    ? Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: Text(
                          "Gender : ${member?.gender == "M" ? "Male" : "Female"} ",
                          style: profileDetailsStyle,
                        ))
                    : Container(),

//          ),
              ],
            )
          : Container(),
    );
  }

  Widget _createPillButton(
    String text, {
    Color backgroundColor = Colors.transparent,
    Color textColor = Colors.white70,
  }) {
    return new ClipRRect(
      borderRadius: new BorderRadius.circular(30.0),
      child: new MaterialButton(
        minWidth: 140.0,
        color: backgroundColor,
        textColor: textColor,
        onPressed: () {},
        child: new Text(text),
      ),
    );
  }

  Widget _rosaryCountFromMessages(BuildContext context) {
//    return FutureBuilder<int>(
////      future: messageBloc.messageRepository.getTotalRosaryCount(), // async work
//      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
//        switch (snapshot.connectionState) {
//          case ConnectionState.waiting:
//            return new Text('Loading....');
//          default:
//            if (snapshot.hasError)
//              return new Text('Error: ${snapshot.error}');
//            else
//              return new Text('Total Rosary Count: ${snapshot.data}');
//        }
//      },
//    );
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return new Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * 0.15,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, 0),
                  end: Alignment(
                      1.0, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    Theme.of(context).primaryColorDark,
                    Theme.of(context).backgroundColor
                  ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
            _buildTitle(
                textTheme: textTheme,
                nameColor: Theme.of(context).backgroundColor,
                regionColor: Colors.grey),
            _buildRosaryInfo(textTheme),
            _buildBadgeDetails(context),
            _buildTreasureVerse(textTheme, context),
            _buildProfileDetails(textTheme, context),
          ],
        ),
        Positioned(
            top: MediaQuery.of(context).size.height * 0.06,
            left: (MediaQuery.of(context).size.width / 2 -
                MediaQuery.of(context).size.width * 0.1),
            child: _buildAvatar()),
        Positioned(
            top: MediaQuery.of(context).size.height * 0.06 +
                MediaQuery.of(context).size.width * 0.15,
            left: (MediaQuery.of(context).size.width / 2 -
                    MediaQuery.of(context).size.width * 0.1) +
                MediaQuery.of(context).size.width * 0.15,
            child: profileUploadButton)
      ],
    );
  }
}
