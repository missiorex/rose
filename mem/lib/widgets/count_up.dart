import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import "package:intl/intl.dart";
import 'bordered_text.dart';

// Animated Total Rosary Count up on splash screen load.
class Countup extends AnimatedWidget {
  Countup({Key key, this.animation}) : super(key: key, listenable: animation);
  Animation<int> animation;

  @override
  build(BuildContext context) {
    return BorderedText(
        strokeCap: StrokeCap.round,
        strokeColor: Theme.of(context).primaryColor,
        strokeWidth: 0.5,
        child: Text(
          NumberFormat("##,##,##,##,###").format(animation.value).toString(),
          textAlign: TextAlign.center,
          style: new TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.055,
              color: Colors.white,
              fontWeight: FontWeight.bold),
        ));
  }
}
