import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:utility/utility.dart';
import 'package:memapp/blocs/blocs.dart';
import 'package:memapp/models/models.dart';
import 'package:campaign_repository/campaign_repository.dart';

class ExtraActions extends StatelessWidget {
  ExtraActions({Key key}) : super(key: MemKeys.extraActionsButton);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CampaignsBloc, CampaignsState>(
      builder: (context, state) {
        if (state is CampaignsLoadSuccess) {
          bool allComplete = (BlocProvider.of<CampaignsBloc>(context).state
                  as CampaignsLoadSuccess)
              .campaigns
              .every((campaign) => campaign.complete);
          return PopupMenuButton<ExtraAction>(
            key: MemKeys.extraActionsPopupMenuButton,
            onSelected: (action) {
              switch (action) {
                case ExtraAction.clearCompleted:
                  BlocProvider.of<CampaignsBloc>(context).add(ClearCompleted());
                  break;
                case ExtraAction.toggleAllComplete:
                  BlocProvider.of<CampaignsBloc>(context).add(ToggleAll());
                  break;
              }
            },
            itemBuilder: (BuildContext context) => <PopupMenuItem<ExtraAction>>[
              PopupMenuItem<ExtraAction>(
                key: MemKeys.toggleAll,
                value: ExtraAction.toggleAllComplete,
                child: Text(
                  allComplete
                      ? MemLocalizations.of(context).markAllIncomplete
                      : MemLocalizations.of(context).markAllComplete,
                ),
              ),
              PopupMenuItem<ExtraAction>(
                key: MemKeys.clearCompleted,
                value: ExtraAction.clearCompleted,
                child: Text(
                  MemLocalizations.of(context).clearCompleted,
                ),
              ),
            ],
          );
        }
        return Container(key: MemKeys.extraActionsEmptyContainer);
      },
    );
  }
}
