import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class AdorationLive extends StatefulWidget {
  @override
  _AdorationLiveState createState() => _AdorationLiveState();
}

class _AdorationLiveState extends State<AdorationLive> {
  YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: 'Dd7YbWyPcQo',
      params: const YoutubePlayerParams(
        playlist: ['rz5gektkF0o', 'BHEdmcci_zg', 'jPUCmOZcUkU'],
        startAt: const Duration(minutes: 1, seconds: 36),
        showControls: true,
        showFullscreenButton: true,
        desktopMode: true,
        privacyEnhanced: true,
        useHybridComposition: true,
        autoPlay: true,
      ),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      log('Exited Fullscreen');
    };
  }

  @override
  Widget build(BuildContext context) {
    const player = YoutubePlayerIFrame();
    return YoutubePlayerControllerProvider(
      // Passing controller to widgets below.
      controller: _controller,
      child: LayoutBuilder(
        builder: (context, constraints) {
          if (kIsWeb && constraints.maxWidth > 800) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Expanded(child: player),
//                const SizedBox(
//                  width: 500,
//                  child: SingleChildScrollView(
//                    child: Controls(),
//                  ),
//                ),
              ],
            );
          }
          return ListView(
            children: [
              player,
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}

///
class Controls extends StatelessWidget {
  ///
  const Controls();

  @override
  Widget build(BuildContext context) {
//    return Padding(
//      padding: const EdgeInsets.all(16),
//      child: Column(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: [
////          _space,
////          MetaDataSection(),
////          _space,
////          SourceInputSection(),
////          _space,
////          PlayPauseButtonBar(),
////          _space,
////          VolumeSlider(),
////          _space,
////          PlayerStateSection(),
//        ],
//      ),
//    );

    return Container();
  }

  Widget get _space => const SizedBox(height: 10);
}
