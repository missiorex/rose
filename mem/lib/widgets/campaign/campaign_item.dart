import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:memapp/models/models.dart';
import 'package:campaign_repository/campaign_repository.dart';
import 'package:memapp/services/dynamic_link_service.dart';
import 'package:http/http.dart';
import 'dart:typed_data';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:wc_flutter_share/wc_flutter_share.dart';

class CampaignItem extends StatelessWidget {
  final DismissDirectionCallback onDismissed;
  final GestureTapCallback onTap;
  final ValueChanged<bool> onCheckboxChanged;
  final GestureTapCallback onRosaryIncrement;
  final Campaign campaign;
  DynamicLinkService _service = DynamicLinkService();

  CampaignItem({
    Key key,
    @required this.onDismissed,
    @required this.onTap,
    @required this.onCheckboxChanged,
    @required this.campaign,
    @required this.onRosaryIncrement,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: MemKeys.campaignItem(campaign.id),
      onDismissed: onDismissed,
      child: ListTile(
        onTap: onTap,
        leading: CircleAvatar(
          backgroundImage: NetworkImage(campaign.imageUrl),
        ),
        title: Hero(
          tag: '${campaign.id}__heroTag',
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Text(
              campaign.title,
              key: MemKeys.campaignItemTitle(campaign.id),
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
        ),
        subtitle: Column(
          children: [
            campaign.description.isNotEmpty
                ? Text(
                    campaign.description,
                    key: MemKeys.campaignItemDescription(campaign.id),
                    maxLines: 6,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.bodyText1,
                  )
                : null,
            Row(children: [
              Container(
                //padding: EdgeInsets.all(10.0),
                child: Stack(children: <Widget>[
                  IconButton(
                    iconSize: 30.0,
                    color: Colors.grey,
                    icon: Icon(Icons.settings),
                    onPressed: onRosaryIncrement,
                  ),
                  Positioned(
                    // draw a red marble
                    top: 0.0,
                    right: 0.0,
                    child: campaign.rosaryCount > 0
                        ? campaign.rosaryCount < 100
                            ? Icon(Icons.brightness_1,
                                size: 16.0, color: Colors.redAccent)
                            : Container(
                                width: 20.0,
                                height: 10.0,
                                decoration: new BoxDecoration(
                                    color: Colors.redAccent,
                                    borderRadius: BorderRadius.all(
                                        const Radius.circular(5.0))),
                              )
                        : Container(),
                  ),
                  campaign.rosaryCount > 0 && campaign.rosaryCount < 100
                      ? Positioned(
                          // draw a red marble
                          top: 3.0,
                          right: 3.0,
                          child: Text(
                            campaign.rosaryCount.toString(),
                            style: TextStyle(
                                fontSize: 8.0,
                                fontWeight: FontWeight.w800,
                                color: Colors.white),
                            textAlign: TextAlign.center,
                          ))
                      : Positioned(
                          // draw a red marble
                          top: 1.0,
                          right: 3.0,
                          child: campaign.rosaryCount >= 100
                              ? Text(
                                  "99+",
                                  style: TextStyle(
                                      fontSize: 8.0,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white),
                                  textAlign: TextAlign.center,
                                )
                              : Container(),
                        )
                ]),
              ),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: IconButton(
                    color: Colors.grey,
                    icon: Icon(Icons.share),
                    onPressed: (() async {
                      String shareCampaignImgUrl =
                          "https://firebasestorage.googleapis.com/v0/b/nehemiah-b1bea.appspot.com/o/default%2Fprayer_default.png?alt=media&token=55222858-52d4-4847-900d-47f98f1775b8";

                      Map parameters = {'id': campaign.id};

                      String campaignLink =
                          await _service.createLink(parameters, "campaign");

                      var request = await HttpClient()
                          .getUrl(Uri.parse(shareCampaignImgUrl));
                      var httpResponse = await request.close();
                      Uint8List bytes =
                          await consolidateHttpClientResponseBytes(
                              httpResponse);
                      WcFlutterShare.share(
                              sharePopupTitle: 'Share the love of Jesus',
                              subject: 'Please do join this prayer campaign',
                              text: campaignLink,
                              fileName: 'share.png',
                              mimeType: 'image/png',
                              bytesOfFile: bytes.buffer.asUint8List())
                          .then((value) {});
                    }),
                  )),
              Checkbox(
                key: MemKeys.campaignItemCheckbox(campaign.id),
                value: campaign.complete,
                onChanged: onCheckboxChanged,
              ),
            ])
          ],
        ),
      ),
    );
  }
}
