import 'package:flutter/material.dart';

class SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  SliverAppBarDelegate(this._tabBar, this._bgImage_url);

  final TabBar _tabBar;
  final String _bgImage_url;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: MediaQuery.of(context).size.height,

      //color: Color.fromRGBO(255, 188, 0, 1.0),
//      margin: EdgeInsets.symmetric(
//        vertical: MediaQuery.of(context).size.height * 0.01,
//        horizontal: MediaQuery.of(context).size.width * 0.01,
//      ),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              offset: new Offset(1.0, 1.0),
              blurRadius: 0.9,
              spreadRadius: 0.9)
        ],
//        gradient: LinearGradient(
//          begin: Alignment.topLeft,
//          end:
//              Alignment(1.1, 0.0), // 10% of the width, so there are ten blinds.
//          colors: [
//            Color.fromRGBO(24, 113, 170, 1.0),
//            Color.fromRGBO(242, 150, 13, 1.0)
//          ], // whitish to gray
//          tileMode: TileMode.repeated, // repeats the gradient over the canvas
//        ),
      ),
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
