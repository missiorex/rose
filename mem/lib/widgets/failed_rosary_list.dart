// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

// == Message List with Stream Builder == //

//import 'package:flutter/foundation.dart';
//import 'package:flutter/material.dart';
//import 'package:mem_models/mem_models.dart';
//import 'package:memapp/widgets/message_item.dart';
//import 'package:memapp/widgets/typedefs.dart';
//import 'package:base_helpers/basehelpers.dart';
//import 'package:mem_blocs/mem_blocs.dart';
//import 'package:mem_entities/mem_entities.dart';
//import 'package:indexed_list_view/indexed_list_view.dart';
//import 'package:memapp/app_state_container.dart';
//import 'package:indexed_list_view/indexed_list_view.dart';
//import 'package:mem_models/mem_models.dart';
//import 'dart:math';
//import 'package:cloud_firestore/cloud_firestore.dart';
//import 'dart:async';
//import 'package:shimmer/shimmer.dart';
//
//class MessageList extends StatefulWidget {
//  final List<MEMmessage> filteredMessages;
//  var container;
//  final bool loading = false;
//  final String bgImageUrl;
//  final bool connectionStatus;
//  final MessageBloc messageBloc;
//  final UserBloc userBloc;
//  final String msgType;
//  final RouteObserver<PageRoute> routeObserver;
//  static const _loadingSpace = 2;
//
//  MessageList(
//      {this.filteredMessages,
//      this.container,
//      @required this.bgImageUrl,
//      this.connectionStatus,
//      this.messageBloc,
//      this.userBloc,
//      this.msgType,
//      this.routeObserver})
//      : super(key: MemKeys.messageList);
//
//  @override
//  State createState() => new MessageListState();
//}
//
//class MessageListState extends State<MessageList> {
//  MessageItem msgItem;
//  int litanyIndex = 0;
//  Random random = Random();
//  String lastId = "";
//  int lastMsgTime = 0;
//  var messages = [];
//  static const String path = 'mem';
//  static Firestore firestore = Firestore.instance;
//  bool isPerformingRequest = false;
//  double _scrollThreshold = 200;
//  var lastVisible = "";
//  var end;
//  List<StreamSubscription> listeners = []; // list of listeners
//  StreamSubscription currentListener;
//  Stream currentStream;
//
//  void changeIndex() {
//    litanyIndex = random.nextInt(57);
//  }
//
//  ScrollController _scrollController = ScrollController();
//
//  @override
//  void initState() {
//    super.initState();
//    changeIndex();
//    getFirstMessages();
//    //messages = widget.filteredMessages;
//    _scrollController.addListener((_onScroll));
//  }
//
//  void _onScroll() {
//    final maxScroll = _scrollController.position.maxScrollExtent;
//    final currentScroll = _scrollController.position.pixels;
//    if (_scrollController.position.pixels ==
//        _scrollController.position.maxScrollExtent) {
//      if (!isPerformingRequest) {
//        setState(() => isPerformingRequest = true);
//        getNextMessages();
//        setState(() {
//          isPerformingRequest = false;
//        });
//      }
//      debugPrint("Scroll: " + (maxScroll - currentScroll).toString());
//    }
//  }
//
//  void getFirstMessages() {
//    var firstRegional, first;
//
//    first = firestore
//        .collection(path)
//        .document("rosary")
//        .collection('messages')
//        .where("msgtype", isEqualTo: widget.msgType)
//        .orderBy('time', descending: true)
//        .limit(10);
//
//    if (widget.msgType == "admin") {
//      firstRegional = firestore
//          .collection(path)
//          .document('groups')
//          .collection('regions')
//          .document(widget.userBloc.userDetails.region)
//          .collection('locales')
//          .document(widget.userBloc.userDetails.locale)
//          .collection('messages')
//          .orderBy('time', descending: true)
//          .limit(10);
//    }
//
//    setState(() {
//      widget.msgType != "admin"
//          ? currentStream = first.snapshots()
//          : currentStream = firstRegional.snapshots();
//    });
//
////    currentListener = currentStream.listen((documentSnapshots) {
////      // Get the last visible document
////      if (documentSnapshots.documents.length > 0)
////        lastVisible = documentSnapshots
////            .documents[documentSnapshots.documents.length - 1].data["time"];
////      debugPrint("last of first 10: " + lastVisible.toString());
////
//////      var source =
//////          documentSnapshots.metadata.fromCache ? "local cache" : "server";
//////      debugPrint("Data came from " + source);
////      setState(() {
////        messages = documentSnapshots.documents.map((doc) {
////          return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
////        }).toList();
////      });
////    });
////    listeners.add(currentListener);
//  }
//
//  void getNextMessages() {
//    // Construct a new query starting at this document,
//    var nextRegional, next;
//    next = firestore
//        .collection(path)
//        .document('rosary')
//        .collection('messages')
//        .where("msgtype", isEqualTo: widget.msgType)
//        .orderBy('time', descending: true);
//
//    if (widget.msgType == "admin") {
//      nextRegional = firestore
//          .collection(path)
//          .document('groups')
//          .collection('regions')
//          .document(widget.userBloc.userDetails.region)
//          .collection('locales')
//          .document(widget.userBloc.userDetails.locale)
//          .collection('messages')
//          .orderBy('time', descending: true);
//    }
//
//    setState(() {
//      widget.msgType != "admin"
//          ? currentStream = next.startAfter([lastVisible]).limit(10).snapshots()
//          : currentStream =
//              nextRegional.startAfter([lastVisible]).limit(10).snapshots();
//    });
//
////    currentListener = currentStream.listen((documentSnapshots) {
////      // Get the last visible document
////
////      setState(() {
////        // previous starting boundary becomes new ending boundary
////        if (documentSnapshots.documents.length > 0)
////          lastVisible = documentSnapshots
////              .documents[documentSnapshots.documents.length - 1].data["time"];
////
////        //List<Message> filteredMsgs = messages.where((i) => i.id != ).toList();
////        messages.addAll(documentSnapshots.documents.map((doc) {
////          return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
////        }).toList());
////      });
////
////      debugPrint("last" + lastVisible.toString());
////    });
////    listeners.add(currentListener);
//  }
//
//  void dispose() {
//    _scrollController.dispose();
////    listeners.forEach((listener) {
////      listener.cancel();
////    });
//    super.dispose();
//  }
//
//  List colors = [
//    Color.fromRGBO(226, 87, 55, 1.0),
//    Color.fromRGBO(27, 162, 181, 1.0),
//    Color.fromRGBO(65, 123, 34, 1.0),
//    Color.fromRGBO(128, 85, 140, 1.0),
//    Color.fromRGBO(53, 170, 187, 1.0),
//    Color.fromRGBO(62, 121, 167, 1.0),
//    Color.fromRGBO(64, 162, 136, 1.0),
//    Color.fromRGBO(201, 138, 50, 1.0),
//    Color.fromRGBO(70, 155, 196, 1.0),
//    Color.fromRGBO(169, 66, 125, 1.0),
//    Color.fromRGBO(76, 165, 148, 1.0),
//    Color.fromRGBO(179, 110, 92, 1.0),
//    Color.fromRGBO(117, 195, 195, 1.0),
//    Color.fromRGBO(200, 171, 101, 1.0),
//    Color.fromRGBO(158, 172, 197, 1.0),
//    Color.fromRGBO(51, 142, 187, 1.0),
//    Color.fromRGBO(51, 92, 113, 1.0),
//    Color.fromRGBO(180, 204, 152, 1.0),
//    Color.fromRGBO(149, 181, 116, 1.0),
//    Color.fromRGBO(162, 171, 188, 1.0),
//    Color.fromRGBO(56, 155, 187, 1.0),
//    Color.fromRGBO(28, 124, 172, 1.0),
//  ];
//
//  List litany = [
//    "Lord, have mercy on us.",
//    "Christ, have mercy on us.",
//    "Lord, have mercy on us.",
//    "Christ, hear us.",
//    "Christ, graciously hear us.",
//    "God the Father of Heaven, Have mercy on us.",
//    "God the Son,Redeemer of the World, Have mercy on us.",
//    "God the Holy Ghost, Have mercy on us.",
//    "Holy Trinity, one God, Have mercy on us.",
//    "Holy Mary, pray for us.",
//    "Holy Mother of God, pray for us.",
//    "Holy Virgin of virgins, pray for us.",
//    "Mother of Christ, pray for us.",
//    "Mother of divine grace, pray for us.",
//    "Mother most pure, pray for us.",
//    "Mother most chaste, pray for us.",
//    "Mother inviolate, pray for us.",
//    "Mother undefiled, pray for us.",
//    "Mother most amiable, pray for us.",
//    "Mother most admirable, pray for us.",
//    "Mother of good counsel, pray for us.",
//    "Mother of our Creator, pray for us.",
//    "Mother of our Savior, pray for us.",
//    "Virgin most prudent, pray for us.",
//    "Virgin most venerable, pray for us.",
//    "Virgin most renowned, pray for us.",
//    "Virgin most powerful, pray for us.",
//    "Virgin most merciful, pray for us.",
//    "Virgin most faithful, pray for us.",
//    "Mirror of justice, pray for us.",
//    "Seat of wisdom, pray for us.",
//    "Cause of our joy, pray for us.",
//    "Spiritual vessel, pray for us.",
//    "Vessel of honor, pray for us.",
//    "Singular vessel of devotion, pray for us.",
//    "Mystical rose, pray for us.",
//    "Tower of David, pray for us.",
//    "Tower of ivory, pray for us.",
//    "House of gold, pray for us.",
//    "Ark of the Covenant, pray for us.",
//    "Gate of Heaven, pray for us.",
//    "Morning star, pray for us.",
//    "Health of the sick, pray for us.",
//    "Refuge of sinners, pray for us.",
//    "Comforter of the afflicted, pray for us.",
//    "Help of Christians, pray for us.",
//    "Queen of angels, pray for us.",
//    "Queen of patriarchs, pray for us.",
//    "Queen of prophets, pray for us.",
//    "Queen of apostles, pray for us.",
//    "Queen of martyrs, pray for us.",
//    "Queen of confessors, pray for us.",
//    "Queen of virgins, pray for us.",
//    "Queen of all saints, pray for us.",
//    "Queen conceived without Original Sin, pray for us.",
//    "Queen assumed into Heaven, pray for us.",
//    "Queen of the most holy Rosary, pray for us.",
//    "Queen of peace, pray for us."
//  ];
//
//  List bibleVerses = [
//    "അവിടുന്ന് തന്റെ മാർഗങ്ങൾ നമ്മെ പഠിപ്പിക്കും (മിക്ക 4:2)",
//    "സകല തിന്മകളിൽ നിന്നും കർത്താവ്‌ നിന്നെ കാത്തുകൊള്ളും (സങ്കീ 121:7)",
//    "അനോഷിച്ചറിയാതെ കുറ്റം ആരോപിക്കരുത് (പ്രഭാ 11:7)",
//    "കർത്താവു വാഗ്ധാനങ്ങളിൽ വിശോസ്ഥനും പ്രവർത്തികളിൽ കാരുണ്യവാനുമാണ് (സങ്കീ 145:13)",
//    "എനിക്ക് ജീവിതം ക്രിസ്തുവും മരണം നേട്ടവുമാകുന്നു.  (ഫിലി 1:20)",
//    "ദൈവത്തിന്റെ വചനം സജീവവും ഊർജസ്വലവും ആണ് (ഹെബ്രാ 4:12)",
//    "അവൻ നമ്മുടെ പാപ പരിഹാര ബലിയാണ് (1യോഹന്നാൻ 2:2)",
//    "നിങ്ങളുടെ പിതാവ് കരുണയുള്ളവനായിരിക്കുന്നതുപോലെ നിങ്ങളും കരുണ ഉള്ളവരായിരിക്കുവിൻ. (ലുക്കാ 6:36)",
//  ];
//
//  Widget _buildProgressIndicator() {
//    return new Padding(
//      padding: const EdgeInsets.all(8.0),
//      child: new Center(
//        child: new Opacity(
//          opacity: isPerformingRequest ? 0.0 : 1.0,
//          child: new CircularProgressIndicator(
//              valueColor: AlwaysStoppedAnimation<Color>(Colors.black)),
//        ),
//      ),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    Random colorRandom = Random(9);
//    var container = AppStateContainer.of(context);
//    UserBloc userBloc = UserProvider.of(context);
//    _scrollThreshold = MediaQuery.of(context).size.height * 0.25;
//    var appTitleMainFontSize = MediaQuery.of(context).size.height * 0.040;
//
//    return StreamBuilder(
//        stream: currentStream,
//        builder: (context, snapshot) {
//          if (snapshot.data != null) {
//            lastVisible = snapshot
//                .data.documents[snapshot.data.documents.length - 1]["time"];
//
//            if (lastVisible == "") {
//              messages = snapshot.data.documents.map((doc) {
//                return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
//              }).toList();
//            } else {
//              messages.addAll(snapshot.data.documents.map((doc) {
//                return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
//              }).toList());
//            }
//          }
//
//          if (snapshot.data == null || !snapshot.hasData) {
//            return Expanded(
//                child: Container(
//                    decoration: new BoxDecoration(
//                      color: Colors.white,
//                      image: new DecorationImage(
//                          image: new AssetImage(widget.bgImageUrl),
//                          fit: BoxFit.fitWidth,
//                          repeat: ImageRepeat.repeatX),
//                    ),
//                    child: Center(
//                        child: CircularProgressIndicator(
//                            valueColor: AlwaysStoppedAnimation<Color>(
//                                Colors.white70)))));
//          } else {
//            return Expanded(
//                child: Container(
//                    decoration: new BoxDecoration(
//                      color: Colors.white,
//                      image: new DecorationImage(
//                          image: new AssetImage(widget.bgImageUrl),
//                          fit: BoxFit.fitWidth,
//                          repeat: ImageRepeat.repeatX),
//                    ),
//                    child: Stack(children: <Widget>[
//                      Container(
//                          child: ListView.builder(
//                        padding: new EdgeInsets.only(
//                            left: 10.0, right: 10.0, top: 20.0),
//                        key: MemKeys.messageList,
//                        reverse: false,
//                        shrinkWrap: true,
//                        controller: _scrollController,
////                      itemCount: widget.filteredMessages.length,
//                        itemCount: messages.length,
//                        itemBuilder: (BuildContext context, int index) {
//                          //final message = widget.filteredMessages[index];
//
//                          final message = messages[index];
////                    final List<User> authorUsers = AppStateContainer.of(context)
////                            .state
////                            .members
////                            .where((i) => i.id == message.authorID)
////                            .toList() ??
////                        [];
//
//                          if (index == messages.length) {
//                            return _buildProgressIndicator();
//                          } else {
////                      return FutureBuilder(
////                          future: userBloc.authorUser(message.authorID),
////                          builder: (context, snapshot) {
////                            if (snapshot.connectionState ==
////                                ConnectionState.done) {
////                              return msgItem = MessageItem(
////                                visible: true,
////                                message: message,
//////                          headerColor: colors[message.rosaryCount % 9],
////                                headerColor:
////                                    colors[colorRandom.nextInt(colors.length)],
////                                userBloc: userBloc,
////                                litanyElement: MemLists().litanyEN,
//////                          litanyElement: litany][randon.nextInt(litany.length)],
////                                //bibleVerse: bibleVerses[random.nextInt(7)],
////                                bibleVerse: MemLists().correctingVerseML,
//////                                authorUser: snapshot.data,
////                                appState: AppStateContainer.of(context).state,
////                                onDismissed: (direction) {
////                                  _removeMessage(context, message);
////                                },
////                                onTap: () {
////                                  Navigator.of(context).push(
////                                    MaterialPageRoute(
////                                      builder: (_) {
////                                        return DetailScreen(
////                                          message: message,
////                                          onDelete: () =>
////                                              _removeMessage(context, message),
////                                        );
////                                      },
////                                    ),
////                                  );
////                                },
////                                onCheckboxChanged: (complete) {
////                                  //updateMessage(message, complete: !message.complete);
////                                },
////                                connectionStatus: widget.connectionStatus,
////                              );
////                            } else
////                              return Center(
////                                  widthFactor: 2.0,
////                                  heightFactor: 2.0,
////                                  child: CircularProgressIndicator(
////                                      valueColor: AlwaysStoppedAnimation<Color>(
////                                          Colors.black26)));
////                          });
//
//                            return msgItem = MessageItem(
//                              visible: true,
//                              message: message,
////                          headerColor: colors[message.rosaryCount % 9],
//                              headerColor:
//                                  colors[colorRandom.nextInt(colors.length)],
//                              userBloc: userBloc,
//                              litanyElement: MemLists().getRandomListElement(
//                                  listType: "litany",
//                                  locale: userBloc.userDetails.locale),
////                          litanyElement: litany][randon.nextInt(litany.length)],
//                              //bibleVerse: bibleVerses[random.nextInt(7)],
//                              bibleVerse: MemLists().getRandomListElement(
//                                  listType: "bible_verse",
//                                  locale: userBloc.userDetails.locale),
////                        authorUser:
////                            authorUsers.length > 0 ? authorUsers[0] : User(),
//                              appState: AppStateContainer.of(context).state,
//                              onDismissed: (direction) {
//                                _removeMessage(context, message);
//                              },
//                              onTap: () {
//                                Navigator.of(context).push(
//                                  MaterialPageRoute(
//                                    builder: (_) {
//                                      return DetailScreen(
//                                        message: message,
//                                        onDelete: () =>
//                                            _removeMessage(context, message),
//                                      );
//                                    },
//                                  ),
//                                );
//                              },
//                              onCheckboxChanged: (complete) {
//                                //updateMessage(message, complete: !message.complete);
//                              },
//                              connectionStatus: widget.connectionStatus,
//                              routeObserver: widget.routeObserver,
//                            );
//                          }
//                        },
//                      )),
//                      Positioned(
//                        right: 0.0,
//                        top: -10.0,
//                        child: FlatButton(
//                          child: Icon(Icons.keyboard_arrow_up),
//                          onPressed: () {
//                            try {
//                              _scrollController.animateTo(
//                                10.0,
//                                curve: Curves.easeOut,
//                                duration: const Duration(milliseconds: 300),
//                              );
//                            } catch (e) {
//                              debugPrint("Caught Exception");
//                            }
//                          },
//                        ),
//                      ),
//                    ])));
//          }
//        });
//  }
//
//  String getLitany() {
//    return "";
//  }
//
//  void _removeMessage(BuildContext context, MEMmessage message) {
//    final messageBloc = MessageProvider.of(context);
//
//    var container = AppStateContainer.of(context);
//    String _deleteMessage;
//    String _undoButtonMessage;
//    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
//        .messageDeleted(message.title);
//    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;
//
//    bool _isSuperAdmin = container.state.userDetails.isSuperAdmin;
//    bool _eligibleToDelete =
//        _isSuperAdmin || message.authorID == container.state.userDetails.id;
//    debugPrint("Eligible to delete : " + _eligibleToDelete.toString());
//    debugPrint("Author ID: " + message.authorID);
//    debugPrint("User ID: " +
//        container.state.userDetails.id +
//        "Username" +
//        container.state.userDetails.name);
//
//    if (_eligibleToDelete) {
//      switch (message.type) {
//        case "rosary":
//          messageBloc.removeRosaryMessage(message, container.state.userDetails);
//          _deleteMessage =
//              "Rosary Count reduced by" + message.rosaryCount.toString();
//          _undoButtonMessage = "";
//          break;
//
//        case "request":
//        case "thanks":
//          messageBloc.removeMessage(message);
//          _deleteMessage = "Requets/ Testimony Deleted";
//          _undoButtonMessage = "";
//          break;
//
//        case "guidance":
//          messageBloc.removeMessage(message);
//          _deleteMessage = "Counselling Request Deleted";
//          _undoButtonMessage = "";
//          break;
//        case "admin":
//          messageBloc.removeRegionalMessage(
//              message, container.state.userDetails);
//          _deleteMessage = "Admin Message Delete";
//          _undoButtonMessage = "";
//          break;
//      }
//    } else {
//      _deleteMessage = "Sorry, You cannot delete this message";
//      _undoButtonMessage = "";
//    }
//
//    Scaffold.of(context).showSnackBar(
//      SnackBar(
//        key: MemKeys.snackbar,
//        duration: Duration(seconds: 3),
//        backgroundColor: Theme.of(context).errorColor,
//        content: Text(
//          _deleteMessage,
//          maxLines: 1,
//          overflow: TextOverflow.ellipsis,
//          style: TextStyle(color: Colors.white),
//        ),
//        action: SnackBarAction(
//          label: _undoButtonMessage,
//          onPressed: () {
//            messageBloc.messageRepository.addRegionalMessage(
//                message.toEntity(), container.state.userDetails.region);
//            //msgItem.visible = true;
//            //addMessage(message);
//          },
//        ),
//      ),
//    );
//  }
//}

// == MessageList with StreamBuilder == //

// -- Message List with queries --- ///

//import 'package:flutter/foundation.dart';
//import 'package:flutter/material.dart';
//import 'package:mem_models/mem_models.dart';
//import 'package:memapp/widgets/message_item.dart';
//import 'package:memapp/widgets/typedefs.dart';
//import 'package:base_helpers/basehelpers.dart';
//import 'package:mem_blocs/mem_blocs.dart';
//import 'package:mem_entities/mem_entities.dart';
//import 'package:indexed_list_view/indexed_list_view.dart';
//import 'package:memapp/app_state_container.dart';
//import 'package:indexed_list_view/indexed_list_view.dart';
//import 'package:mem_models/mem_models.dart';
//import 'dart:math';
//import 'package:cloud_firestore/cloud_firestore.dart';
//import 'dart:async';
//import 'package:shimmer/shimmer.dart';
//
//class MessageList extends StatefulWidget {
//  final List<MEMmessage> filteredMessages;
//  var container;
//  final bool loading = false;
//  final String bgImageUrl;
//  final bool connectionStatus;
//  final MessageBloc messageBloc;
//  final UserBloc userBloc;
//  final String msgType;
//  final RouteObserver<PageRoute> routeObserver;
//  static const _loadingSpace = 2;
//
//  MessageList(
//      {this.filteredMessages,
//      this.container,
//      @required this.bgImageUrl,
//      this.connectionStatus,
//      this.messageBloc,
//      this.userBloc,
//      this.msgType,
//      this.routeObserver})
//      : super(key: MemKeys.messageList);
//
//  @override
//  State createState() => new MessageListState();
//}
//
//class MessageListState extends State<MessageList> {
//  MessageItem msgItem;
//  int litanyIndex = 0;
//  Random random = Random();
//  var messages = [];
//  static const String path = 'mem';
//  static Firestore firestore = Firestore.instance;
//
//  DocumentSnapshot _lastVisible;
//  bool _isLoading;
//  List<DocumentSnapshot> _data = new List<DocumentSnapshot>();
//
//  void changeIndex() {
//    litanyIndex = random.nextInt(57);
//  }
//
//  ScrollController _scrollController = ScrollController();
//
//  @override
//  void initState() {
//    _scrollController.addListener((_onScroll));
//    super.initState();
//    _isLoading = true;
//    _getData();
//  }
//
//  void _onScroll() {
//    final maxScroll = _scrollController.position.maxScrollExtent;
//    final currentScroll = _scrollController.position.pixels;
//    if (_scrollController.position.pixels ==
//        _scrollController.position.maxScrollExtent) {
//      if (!_isLoading) {
//        setState(() => _isLoading = true);
//        _getData();
//      }
//      debugPrint("Scroll: " + (maxScroll - currentScroll).toString());
//    }
//  }
//
//  Future<Null> _getData() async {
////    await new Future.delayed(new Duration(seconds: 5));
//    var regionalQuery, globalQuery;
//
//    globalQuery = firestore
//        .collection(path)
//        .document("rosary")
//        .collection('messages')
//        .where("msgtype", isEqualTo: widget.msgType)
//        .orderBy('time', descending: true);
//
//    if (widget.msgType == "admin") {
//      regionalQuery = firestore
//          .collection(path)
//          .document('groups')
//          .collection('regions')
//          .document(widget.userBloc.userDetails.region)
//          .collection('locales')
//          .document(widget.userBloc.userDetails.locale)
//          .collection('messages')
//          .orderBy('time', descending: true);
//    }
//
//    QuerySnapshot data;
//    if (_lastVisible == null) {
//      if (widget.msgType == "admin")
//        data = await regionalQuery.limit(10).getDocuments();
//      else
//        data = await globalQuery.limit(10).getDocuments();
//      if (data.documents.length > 0) {
//        if (mounted) {
//          setState(() {
//            messages = data.documents.map((doc) {
//              return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
//            }).toList();
//          });
//        }
//      }
//    } else {
//      if (widget.msgType == "admin")
//        data = await regionalQuery
//            .startAfter([_lastVisible["time"]])
//            .limit(10)
//            .getDocuments();
//      else
//        data = await globalQuery
//            .startAfter([_lastVisible["time"]])
//            .limit(10)
//            .getDocuments();
//
//      if (data.documents.length > 0) {
//        if (mounted) {
//          setState(() {
//            messages.addAll(data.documents.map((doc) {
//              return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
//            }).toList());
//          });
//        }
//      }
//    }
//
//    if (data != null && data.documents.length > 0) {
//      if (mounted) {
//        setState(() {
//          _lastVisible = data.documents[data.documents.length - 1];
//          _isLoading = false;
//          _data.addAll(data.documents);
//        });
//      }
//    } else {
//      setState(() => _isLoading = false);
//      Scaffold.of(context).showSnackBar(
//        SnackBar(
//          key: MemKeys.snackbar,
//          duration: Duration(seconds: 3),
//          backgroundColor: Theme.of(context).errorColor,
//          content: Text(
//            "No More Messages",
//            maxLines: 1,
//            overflow: TextOverflow.ellipsis,
//            style: TextStyle(color: Colors.white),
//          ),
//        ),
//      );
//    }
//    return null;
//  }
//
//  void dispose() {
//    _scrollController.dispose();
//
//    super.dispose();
//  }
//
//  List colors = [
//    Color.fromRGBO(226, 87, 55, 1.0),
//    Color.fromRGBO(27, 162, 181, 1.0),
//    Color.fromRGBO(65, 123, 34, 1.0),
//    Color.fromRGBO(128, 85, 140, 1.0),
//    Color.fromRGBO(53, 170, 187, 1.0),
//    Color.fromRGBO(62, 121, 167, 1.0),
//    Color.fromRGBO(64, 162, 136, 1.0),
//    Color.fromRGBO(201, 138, 50, 1.0),
//    Color.fromRGBO(70, 155, 196, 1.0),
//    Color.fromRGBO(169, 66, 125, 1.0),
//    Color.fromRGBO(76, 165, 148, 1.0),
//    Color.fromRGBO(179, 110, 92, 1.0),
//    Color.fromRGBO(117, 195, 195, 1.0),
//    Color.fromRGBO(200, 171, 101, 1.0),
//    Color.fromRGBO(158, 172, 197, 1.0),
//    Color.fromRGBO(51, 142, 187, 1.0),
//    Color.fromRGBO(51, 92, 113, 1.0),
//    Color.fromRGBO(180, 204, 152, 1.0),
//    Color.fromRGBO(149, 181, 116, 1.0),
//    Color.fromRGBO(162, 171, 188, 1.0),
//    Color.fromRGBO(56, 155, 187, 1.0),
//    Color.fromRGBO(28, 124, 172, 1.0),
//  ];
//
//  List litany = [
//    "Lord, have mercy on us.",
//    "Christ, have mercy on us.",
//    "Lord, have mercy on us.",
//    "Christ, hear us.",
//    "Christ, graciously hear us.",
//    "God the Father of Heaven, Have mercy on us.",
//    "God the Son,Redeemer of the World, Have mercy on us.",
//    "God the Holy Ghost, Have mercy on us.",
//    "Holy Trinity, one God, Have mercy on us.",
//    "Holy Mary, pray for us.",
//    "Holy Mother of God, pray for us.",
//    "Holy Virgin of virgins, pray for us.",
//    "Mother of Christ, pray for us.",
//    "Mother of divine grace, pray for us.",
//    "Mother most pure, pray for us.",
//    "Mother most chaste, pray for us.",
//    "Mother inviolate, pray for us.",
//    "Mother undefiled, pray for us.",
//    "Mother most amiable, pray for us.",
//    "Mother most admirable, pray for us.",
//    "Mother of good counsel, pray for us.",
//    "Mother of our Creator, pray for us.",
//    "Mother of our Savior, pray for us.",
//    "Virgin most prudent, pray for us.",
//    "Virgin most venerable, pray for us.",
//    "Virgin most renowned, pray for us.",
//    "Virgin most powerful, pray for us.",
//    "Virgin most merciful, pray for us.",
//    "Virgin most faithful, pray for us.",
//    "Mirror of justice, pray for us.",
//    "Seat of wisdom, pray for us.",
//    "Cause of our joy, pray for us.",
//    "Spiritual vessel, pray for us.",
//    "Vessel of honor, pray for us.",
//    "Singular vessel of devotion, pray for us.",
//    "Mystical rose, pray for us.",
//    "Tower of David, pray for us.",
//    "Tower of ivory, pray for us.",
//    "House of gold, pray for us.",
//    "Ark of the Covenant, pray for us.",
//    "Gate of Heaven, pray for us.",
//    "Morning star, pray for us.",
//    "Health of the sick, pray for us.",
//    "Refuge of sinners, pray for us.",
//    "Comforter of the afflicted, pray for us.",
//    "Help of Christians, pray for us.",
//    "Queen of angels, pray for us.",
//    "Queen of patriarchs, pray for us.",
//    "Queen of prophets, pray for us.",
//    "Queen of apostles, pray for us.",
//    "Queen of martyrs, pray for us.",
//    "Queen of confessors, pray for us.",
//    "Queen of virgins, pray for us.",
//    "Queen of all saints, pray for us.",
//    "Queen conceived without Original Sin, pray for us.",
//    "Queen assumed into Heaven, pray for us.",
//    "Queen of the most holy Rosary, pray for us.",
//    "Queen of peace, pray for us."
//  ];
//
//  List bibleVerses = [
//    "അവിടുന്ന് തന്റെ മാർഗങ്ങൾ നമ്മെ പഠിപ്പിക്കും (മിക്ക 4:2)",
//    "സകല തിന്മകളിൽ നിന്നും കർത്താവ്‌ നിന്നെ കാത്തുകൊള്ളും (സങ്കീ 121:7)",
//    "അനോഷിച്ചറിയാതെ കുറ്റം ആരോപിക്കരുത് (പ്രഭാ 11:7)",
//    "കർത്താവു വാഗ്ധാനങ്ങളിൽ വിശോസ്ഥനും പ്രവർത്തികളിൽ കാരുണ്യവാനുമാണ് (സങ്കീ 145:13)",
//    "എനിക്ക് ജീവിതം ക്രിസ്തുവും മരണം നേട്ടവുമാകുന്നു.  (ഫിലി 1:20)",
//    "ദൈവത്തിന്റെ വചനം സജീവവും ഊർജസ്വലവും ആണ് (ഹെബ്രാ 4:12)",
//    "അവൻ നമ്മുടെ പാപ പരിഹാര ബലിയാണ് (1യോഹന്നാൻ 2:2)",
//    "നിങ്ങളുടെ പിതാവ് കരുണയുള്ളവനായിരിക്കുന്നതുപോലെ നിങ്ങളും കരുണ ഉള്ളവരായിരിക്കുവിൻ. (ലുക്കാ 6:36)",
//  ];
//
//  Widget _buildProgressIndicator() {
//    return new Padding(
//      padding: const EdgeInsets.all(8.0),
//      child: new Center(
//        child: new Opacity(
//          opacity: _isLoading ? 0.0 : 1.0,
//          child: new CircularProgressIndicator(
//              valueColor: AlwaysStoppedAnimation<Color>(Colors.black)),
//        ),
//      ),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    Random colorRandom = Random(9);
//    var container = AppStateContainer.of(context);
//    UserBloc userBloc = UserProvider.of(context);
////    _scrollThreshold = MediaQuery.of(context).size.height * 0.25;
//    var appTitleMainFontSize = MediaQuery.of(context).size.height * 0.040;
//
//    if (messages.length == 0) {
//      return Expanded(
//          child: Container(
//              decoration: new BoxDecoration(
//                color: Colors.white,
//                image: new DecorationImage(
//                    image: new AssetImage(widget.bgImageUrl),
//                    fit: BoxFit.fitWidth,
//                    repeat: ImageRepeat.repeatX),
//              ),
//              child: Center(
//                  child: CircularProgressIndicator(
//                      valueColor:
//                          AlwaysStoppedAnimation<Color>(Colors.white70)))));
//    } else {
//      return Expanded(
//          child: Container(
//              decoration: new BoxDecoration(
//                color: Colors.white,
//                image: new DecorationImage(
//                    image: new AssetImage(widget.bgImageUrl),
//                    fit: BoxFit.fitWidth,
//                    repeat: ImageRepeat.repeatX),
//              ),
//              child: Stack(children: <Widget>[
//                Container(
//                    child: ListView.builder(
//                  padding:
//                      new EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
//                  key: MemKeys.messageList,
//                  reverse: false,
//                  shrinkWrap: true,
//                  controller: _scrollController,
////                      itemCount: widget.filteredMessages.length,
//                  itemCount: messages.length,
//                  itemBuilder: (BuildContext context, int index) {
//                    //final message = widget.filteredMessages[index];
//
//                    final message = messages[index];
////                    final List<User> authorUsers = AppStateContainer.of(context)
////                            .state
////                            .members
////                            .where((i) => i.id == message.authorID)
////                            .toList() ??
////                        [];
//
//                    if (index == messages.length) {
//                      return _buildProgressIndicator();
//                    } else {
////                      return FutureBuilder(
////                          future: userBloc.authorUser(message.authorID),
////                          builder: (context, snapshot) {
////                            if (snapshot.connectionState ==
////                                ConnectionState.done) {
////                              return msgItem = MessageItem(
////                                visible: true,
////                                message: message,
//////                          headerColor: colors[message.rosaryCount % 9],
////                                headerColor:
////                                    colors[colorRandom.nextInt(colors.length)],
////                                userBloc: userBloc,
////                                litanyElement: MemLists().litanyEN,
//////                          litanyElement: litany][randon.nextInt(litany.length)],
////                                //bibleVerse: bibleVerses[random.nextInt(7)],
////                                bibleVerse: MemLists().correctingVerseML,
//////                                authorUser: snapshot.data,
////                                appState: AppStateContainer.of(context).state,
////                                onDismissed: (direction) {
////                                  _removeMessage(context, message);
////                                },
////                                onTap: () {
////                                  Navigator.of(context).push(
////                                    MaterialPageRoute(
////                                      builder: (_) {
////                                        return DetailScreen(
////                                          message: message,
////                                          onDelete: () =>
////                                              _removeMessage(context, message),
////                                        );
////                                      },
////                                    ),
////                                  );
////                                },
////                                onCheckboxChanged: (complete) {
////                                  //updateMessage(message, complete: !message.complete);
////                                },
////                                connectionStatus: widget.connectionStatus,
////                              );
////                            } else
////                              return Center(
////                                  widthFactor: 2.0,
////                                  heightFactor: 2.0,
////                                  child: CircularProgressIndicator(
////                                      valueColor: AlwaysStoppedAnimation<Color>(
////                                          Colors.black26)));
////                          });
//
//                      return msgItem = MessageItem(
//                        visible: true,
//                        message: message,
////                          headerColor: colors[message.rosaryCount % 9],
//                        headerColor: colors[colorRandom.nextInt(colors.length)],
//                        userBloc: userBloc,
//                        litanyElement: MemLists().getRandomListElement(
//                            listType: "litany",
//                            locale: userBloc.userDetails.locale),
////                          litanyElement: litany][randon.nextInt(litany.length)],
//                        //bibleVerse: bibleVerses[random.nextInt(7)],
//                        bibleVerse: MemLists().getRandomListElement(
//                            listType: "bible_verse",
//                            locale: userBloc.userDetails.locale),
////                        authorUser:
////                            authorUsers.length > 0 ? authorUsers[0] : User(),
//                        appState: AppStateContainer.of(context).state,
//                        onDismissed: (direction) {
//                          _removeMessage(context, message);
//                        },
//                        onTap: () {
//                          Navigator.of(context).push(
//                            MaterialPageRoute(
//                              builder: (_) {
//                                return DetailScreen(
//                                  message: message,
//                                  onDelete: () =>
//                                      _removeMessage(context, message),
//                                );
//                              },
//                            ),
//                          );
//                        },
//                        onCheckboxChanged: (complete) {
//                          //updateMessage(message, complete: !message.complete);
//                        },
//                        connectionStatus: widget.connectionStatus,
//                        routeObserver: widget.routeObserver,
//                      );
//                    }
//                  },
//                )),
//                Positioned(
//                  right: 0.0,
//                  top: -10.0,
//                  child: FlatButton(
//                    child: Icon(Icons.keyboard_arrow_up),
//                    onPressed: () {
//                      try {
//                        _scrollController.animateTo(
//                          10.0,
//                          curve: Curves.easeOut,
//                          duration: const Duration(milliseconds: 300),
//                        );
//                      } catch (e) {
//                        debugPrint("Caught Exception");
//                      }
//                    },
//                  ),
//                ),
//              ])));
//    }
//  }
//
//  String getLitany() {
//    return "";
//  }
//
//  void _removeMessage(BuildContext context, MEMmessage message) {
//    final messageBloc = MessageProvider.of(context);
//
//    var container = AppStateContainer.of(context);
//    String _deleteMessage;
//    String _undoButtonMessage;
//    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
//        .messageDeleted(message.title);
//    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;
//
//    bool _isSuperAdmin = container.state.userDetails.isSuperAdmin;
//    bool _eligibleToDelete =
//        _isSuperAdmin || message.authorID == container.state.userDetails.id;
//    debugPrint("Eligible to delete : " + _eligibleToDelete.toString());
//    debugPrint("Author ID: " + message.authorID);
//    debugPrint("User ID: " +
//        container.state.userDetails.id +
//        "Username" +
//        container.state.userDetails.name);
//
//    if (_eligibleToDelete) {
//      switch (message.type) {
//        case "rosary":
//          messageBloc.removeRosaryMessage(message, container.state.userDetails);
//          _deleteMessage =
//              "Rosary Count reduced by" + message.rosaryCount.toString();
//          _undoButtonMessage = "";
//          break;
//
//        case "request":
//        case "thanks":
//          messageBloc.removeMessage(message);
//          _deleteMessage = "Requets/ Testimony Deleted";
//          _undoButtonMessage = "";
//          break;
//
//        case "guidance":
//          messageBloc.removeMessage(message);
//          _deleteMessage = "Counselling Request Deleted";
//          _undoButtonMessage = "";
//          break;
//        case "admin":
//          messageBloc.removeRegionalMessage(
//              message, container.state.userDetails);
//          _deleteMessage = "Admin Message Delete";
//          _undoButtonMessage = "";
//          break;
//      }
//    } else {
//      _deleteMessage = "Sorry, You cannot delete this message";
//      _undoButtonMessage = "";
//    }
//
//    Scaffold.of(context).showSnackBar(
//      SnackBar(
//        key: MemKeys.snackbar,
//        duration: Duration(seconds: 3),
//        backgroundColor: Theme.of(context).errorColor,
//        content: Text(
//          _deleteMessage,
//          maxLines: 1,
//          overflow: TextOverflow.ellipsis,
//          style: TextStyle(color: Colors.white),
//        ),
//        action: SnackBarAction(
//          label: _undoButtonMessage,
//          onPressed: () {
//            messageBloc.messageRepository.addRegionalMessage(
//                message.toEntity(), container.state.userDetails.region);
//            //msgItem.visible = true;
//            //addMessage(message);
//          },
//        ),
//      ),
//    );
//  }
//}

// -- Message List with queries --- ///

// -----   Message List with listeneres -----//

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/widgets/message_item.dart';
import 'package:base_helpers/basehelpers.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:memapp/app_state_container.dart';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class FailedRosaryList extends StatefulWidget {
  final List<MEMmessage> filteredMessages;
  var container;
  final bool loading = false;
  final String bgImageUrl;
  final bool connectionStatus;
  final MessageBloc messageBloc;
  final UserBloc userBloc;
  final String msgType;
  final RouteObserver<PageRoute> routeObserver;
  static const _loadingSpace = 2;

  FailedRosaryList(
      {this.filteredMessages,
      this.container,
      @required this.bgImageUrl,
      this.connectionStatus,
      this.messageBloc,
      this.userBloc,
      this.msgType,
      this.routeObserver})
      : super(key: MemKeys.messageList);

  @override
  State createState() => new FailedRosaryListState();
}

class FailedRosaryListState extends State<FailedRosaryList> {
  MessageItem msgItem;
  int litanyIndex = 0;
  Random random = Random();
  String lastId = "";
  int lastMsgTime = 0;
  var messages = [];
  static const String path = 'mem';
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  bool isPerformingRequest = false;
  double _scrollThreshold = 200;
  var lastVisible;
  var end;
  List<StreamSubscription> listeners = []; // list of listeners
  StreamSubscription currentListener;
  Stream currentStream;

  void changeIndex() {
    litanyIndex = random.nextInt(57);
  }

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    changeIndex();
    getFirstMessages();

    //messages = widget.filteredMessages;
    //_scrollController.addListener((_onScroll));
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (!isPerformingRequest) {
        setState(() => isPerformingRequest = true);
        getNextMessages();
        setState(() {
          isPerformingRequest = false;
        });
      }
      debugPrint("Scroll: " + (maxScroll - currentScroll).toString());
    } else if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      if (!isPerformingRequest) {
        setState(() => isPerformingRequest = true);
        getFirstMessages();
        setState(() {
          isPerformingRequest = false;
        });
      }
    }
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    if (notification is ScrollEndNotification) {
      if (_scrollController.position.extentAfter == 0) {
        if (!isPerformingRequest) {
          setState(() => isPerformingRequest = true);
          getNextMessages();
          setState(() {
            isPerformingRequest = false;
          });
        }
      }
    } else if (notification is ScrollStartNotification) {
      if (_scrollController.offset <=
              _scrollController.position.minScrollExtent &&
          !_scrollController.position.outOfRange) {
        if (!isPerformingRequest) {
          setState(() => isPerformingRequest = true);
          getFirstMessages();
          setState(() {
            isPerformingRequest = false;
          });
        }
      }
    }
    return false;
  }

  void getFirstMessages() {
    var firstRegional, first;

    var currentDate = DateTime.now();
    var startDate =
        DateTime(currentDate.year, currentDate.month - 3, currentDate.day);

    if (widget.msgType == "pendingRosaries") {
      first = firestore
          .collection(path)
          .doc("rosary")
          .collection('messages')
          .where("msgtype", isEqualTo: "rosary")
          .where("complete", isEqualTo: false)
          .where("time",
              isGreaterThanOrEqualTo:
                  startDate.millisecondsSinceEpoch.toString())
          .orderBy('time', descending: true)
          .limit(1000);
    } else {
      first = firestore
          .collection(path)
          .doc("rosary")
          .collection('messages')
          .where("msgtype", isEqualTo: widget.msgType)
          .orderBy('time', descending: true)
          .limit(4);
    }

    if (widget.msgType == "admin") {
      firstRegional = firestore
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(widget.userBloc.userDetails.region)
          .collection('locales')
          .doc(widget.userBloc.userDetails.locale)
          .collection('messages')
          .orderBy('time', descending: true)
          .limit(4);
    }

    setState(() {
      widget.msgType != "admin"
          ? currentStream = first.snapshots()
          : currentStream = firstRegional.snapshots();
    });

    currentListener = currentStream.listen((documentSnapshots) {
      // Get the last visible document
      if (documentSnapshots.documents.length > 0)
        debugPrint("last of first 10: " + lastVisible.toString());

//      var source =
//          documentSnapshots.metadata.fromCache ? "local cache" : "server";
//      debugPrint("Data came from " + source);
      setState(() {
        lastVisible = documentSnapshots
            .documents[documentSnapshots.documents.length - 1].data["time"];
        if (messages.isEmpty) {
          messages = documentSnapshots.documents.map((doc) {
                return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
              }).toList() ??
              widget.filteredMessages;
        } else {
          messages = documentSnapshots.documents.map((doc) {
            return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
          }).toList();
        }
      });
    });
    listeners.add(currentListener);
  }

  void getNextMessages() {
    // Construct a new query starting at this document,
    var nextRegional, next;
    var currentDate = DateTime.now();
    var startDate =
        DateTime(currentDate.year, currentDate.month - 3, currentDate.day);

    if (widget.msgType == "pendingRosaries") {
      next = firestore
          .collection(path)
          .doc("rosary")
          .collection('messages')
          .where("msgtype", isEqualTo: "rosary")
          .where("complete", isEqualTo: false)
          .where("time",
              isGreaterThanOrEqualTo:
                  startDate.millisecondsSinceEpoch.toString())
          .orderBy('time', descending: true)
          .orderBy('time', descending: true)
          .limit(1000);
    } else {
      next = firestore
          .collection(path)
          .doc('rosary')
          .collection('messages')
          .where("msgtype", isEqualTo: widget.msgType)
          .orderBy('time', descending: true);
    }

    if (widget.msgType == "admin") {
      nextRegional = firestore
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(widget.userBloc.userDetails.region)
          .collection('locales')
          .doc(widget.userBloc.userDetails.locale)
          .collection('messages')
          .orderBy('time', descending: true);
    }

    setState(() {
      widget.msgType != "admin"
          ? currentStream = next.startAfter([lastVisible]).limit(4).snapshots()
          : currentStream =
              nextRegional.startAfter([lastVisible]).limit(4).snapshots();
    });

    currentListener = currentStream.listen((documentSnapshots) {
      // Get the last visible document

      setState(() {
        // previous starting boundary becomes new ending boundary
        if (documentSnapshots.documents.length > 0 &&
            documentSnapshots.documents[documentSnapshots.documents.length - 1]
                    .data["time"] !=
                lastVisible) {
          lastVisible = documentSnapshots
              .documents[documentSnapshots.documents.length - 1].data["time"];

          //List<Message> filteredMsgs = messages.where((i) => i.id != ).toList();
          messages.addAll(documentSnapshots.documents.map((doc) {
            return MEMmessage.fromEntity(MessageEntity.fromJson(doc.data));
          }).toList());
        }
      });

      debugPrint("last" + lastVisible.toString());
    });
    listeners.add(currentListener);
  }

  void dispose() {
    _scrollController.dispose();
    listeners.forEach((listener) {
      listener.cancel();
    });
    super.dispose();
  }

  List colors = [
    Color.fromRGBO(226, 87, 55, 1.0),
    Color.fromRGBO(27, 162, 181, 1.0),
    Color.fromRGBO(65, 123, 34, 1.0),
    Color.fromRGBO(128, 85, 140, 1.0),
    Color.fromRGBO(53, 170, 187, 1.0),
    Color.fromRGBO(62, 121, 167, 1.0),
    Color.fromRGBO(64, 162, 136, 1.0),
    Color.fromRGBO(201, 138, 50, 1.0),
    Color.fromRGBO(70, 155, 196, 1.0),
    Color.fromRGBO(169, 66, 125, 1.0),
    Color.fromRGBO(76, 165, 148, 1.0),
    Color.fromRGBO(179, 110, 92, 1.0),
    Color.fromRGBO(117, 195, 195, 1.0),
    Color.fromRGBO(200, 171, 101, 1.0),
    Color.fromRGBO(158, 172, 197, 1.0),
    Color.fromRGBO(51, 142, 187, 1.0),
    Color.fromRGBO(51, 92, 113, 1.0),
    Color.fromRGBO(180, 204, 152, 1.0),
    Color.fromRGBO(149, 181, 116, 1.0),
    Color.fromRGBO(162, 171, 188, 1.0),
    Color.fromRGBO(56, 155, 187, 1.0),
    Color.fromRGBO(28, 124, 172, 1.0),
  ];

  List litany = [
    "Lord, have mercy on us.",
    "Christ, have mercy on us.",
    "Lord, have mercy on us.",
    "Christ, hear us.",
    "Christ, graciously hear us.",
    "God the Father of Heaven, Have mercy on us.",
    "God the Son,Redeemer of the World, Have mercy on us.",
    "God the Holy Ghost, Have mercy on us.",
    "Holy Trinity, one God, Have mercy on us.",
    "Holy Mary, pray for us.",
    "Holy Mother of God, pray for us.",
    "Holy Virgin of virgins, pray for us.",
    "Mother of Christ, pray for us.",
    "Mother of divine grace, pray for us.",
    "Mother most pure, pray for us.",
    "Mother most chaste, pray for us.",
    "Mother inviolate, pray for us.",
    "Mother undefiled, pray for us.",
    "Mother most amiable, pray for us.",
    "Mother most admirable, pray for us.",
    "Mother of good counsel, pray for us.",
    "Mother of our Creator, pray for us.",
    "Mother of our Savior, pray for us.",
    "Virgin most prudent, pray for us.",
    "Virgin most venerable, pray for us.",
    "Virgin most renowned, pray for us.",
    "Virgin most powerful, pray for us.",
    "Virgin most merciful, pray for us.",
    "Virgin most faithful, pray for us.",
    "Mirror of justice, pray for us.",
    "Seat of wisdom, pray for us.",
    "Cause of our joy, pray for us.",
    "Spiritual vessel, pray for us.",
    "Vessel of honor, pray for us.",
    "Singular vessel of devotion, pray for us.",
    "Mystical rose, pray for us.",
    "Tower of David, pray for us.",
    "Tower of ivory, pray for us.",
    "House of gold, pray for us.",
    "Ark of the Covenant, pray for us.",
    "Gate of Heaven, pray for us.",
    "Morning star, pray for us.",
    "Health of the sick, pray for us.",
    "Refuge of sinners, pray for us.",
    "Comforter of the afflicted, pray for us.",
    "Help of Christians, pray for us.",
    "Queen of angels, pray for us.",
    "Queen of patriarchs, pray for us.",
    "Queen of prophets, pray for us.",
    "Queen of apostles, pray for us.",
    "Queen of martyrs, pray for us.",
    "Queen of confessors, pray for us.",
    "Queen of virgins, pray for us.",
    "Queen of all saints, pray for us.",
    "Queen conceived without Original Sin, pray for us.",
    "Queen assumed into Heaven, pray for us.",
    "Queen of the most holy Rosary, pray for us.",
    "Queen of peace, pray for us."
  ];

  List bibleVerses = [
    "അവിടുന്ന് തന്റെ മാർഗങ്ങൾ നമ്മെ പഠിപ്പിക്കും (മിക്ക 4:2)",
    "സകല തിന്മകളിൽ നിന്നും കർത്താവ്‌ നിന്നെ കാത്തുകൊള്ളും (സങ്കീ 121:7)",
    "അനോഷിച്ചറിയാതെ കുറ്റം ആരോപിക്കരുത് (പ്രഭാ 11:7)",
    "കർത്താവു വാഗ്ധാനങ്ങളിൽ വിശോസ്ഥനും പ്രവർത്തികളിൽ കാരുണ്യവാനുമാണ് (സങ്കീ 145:13)",
    "എനിക്ക് ജീവിതം ക്രിസ്തുവും മരണം നേട്ടവുമാകുന്നു.  (ഫിലി 1:20)",
    "ദൈവത്തിന്റെ വചനം സജീവവും ഊർജസ്വലവും ആണ് (ഹെബ്രാ 4:12)",
    "അവൻ നമ്മുടെ പാപ പരിഹാര ബലിയാണ് (1യോഹന്നാൻ 2:2)",
    "നിങ്ങളുടെ പിതാവ് കരുണയുള്ളവനായിരിക്കുന്നതുപോലെ നിങ്ങളും കരുണ ഉള്ളവരായിരിക്കുവിൻ. (ലുക്കാ 6:36)",
  ];

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isPerformingRequest ? 0.0 : 1.0,
          child: new CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white70)),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Random colorRandom = Random(9);
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);
    _scrollThreshold = MediaQuery.of(context).size.height * 0.25;
    var appTitleMainFontSize = MediaQuery.of(context).size.height * 0.040;

    final String litanyElement = MemLists().getRandomListElement(
        listType: "litany", locale: userBloc.userDetails.locale);

    if (messages.length == 0) {
      return Expanded(
          child: Container(
              decoration: new BoxDecoration(
                color: Colors.white,
                image: new DecorationImage(
                    image: new AssetImage(widget.bgImageUrl),
                    fit: BoxFit.fitWidth,
                    repeat: ImageRepeat.repeatX),
              ),
              child: Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.white70)))));
    } else {
      return Expanded(
          child: Container(
              decoration: new BoxDecoration(
                color: Colors.white,
                image: new DecorationImage(
                    image: new AssetImage(widget.bgImageUrl),
                    fit: BoxFit.fitWidth,
                    repeat: ImageRepeat.repeatX),
              ),
              child: Stack(children: <Widget>[
                Container(
                    child: NotificationListener<ScrollNotification>(
                        onNotification: _handleScrollNotification,
                        child: ListView.builder(
                          padding: new EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 20.0),
                          key: MemKeys.messageList,
                          reverse: false,
                          shrinkWrap: true,
                          controller: _scrollController,
//                      itemCount: widget.filteredMessages.length,
                          itemCount: messages.length,
                          itemBuilder: (BuildContext context, int index) {
                            //final message = widget.filteredMessages[index];

                            final message = messages[index];
//                    final List<User> authorUsers = AppStateContainer.of(context)
//                            .state
//                            .members
//                            .where((i) => i.id == message.authorID)
//                            .toList() ??
//                        [];

                            if (index == messages.length - 1) {
                              return _buildProgressIndicator();
                            } else {
//                      return FutureBuilder(
//                          future: userBloc.authorUser(message.authorID),
//                          builder: (context, snapshot) {
//                            if (snapshot.connectionState ==
//                                ConnectionState.done) {
//                              return msgItem = MessageItem(
//                                visible: true,
//                                message: message,
////                          headerColor: colors[message.rosaryCount % 9],
//                                headerColor:
//                                    colors[colorRandom.nextInt(colors.length)],
//                                userBloc: userBloc,
//                                litanyElement: MemLists().litanyEN,
////                          litanyElement: litany][randon.nextInt(litany.length)],
//                                //bibleVerse: bibleVerses[random.nextInt(7)],
//                                bibleVerse: MemLists().correctingVerseML,
////                                authorUser: snapshot.data,
//                                appState: AppStateContainer.of(context).state,
//                                onDismissed: (direction) {
//                                  _removeMessage(context, message);
//                                },
//                                onTap: () {
//                                  Navigator.of(context).push(
//                                    MaterialPageRoute(
//                                      builder: (_) {
//                                        return DetailScreen(
//                                          message: message,
//                                          onDelete: () =>
//                                              _removeMessage(context, message),
//                                        );
//                                      },
//                                    ),
//                                  );
//                                },
//                                onCheckboxChanged: (complete) {
//                                  //updateMessage(message, complete: !message.complete);
//                                },
//                                connectionStatus: widget.connectionStatus,
//                              );
//                            } else
//                              return Center(
//                                  widthFactor: 2.0,
//                                  heightFactor: 2.0,
//                                  child: CircularProgressIndicator(
//                                      valueColor: AlwaysStoppedAnimation<Color>(
//                                          Colors.black26)));
//                          });

                              return msgItem = MessageItem(
                                visible: true,
                                message: message,
//                          headerColor: colors[message.rosaryCount % 9],
                                headerColor:
                                    colors[colorRandom.nextInt(colors.length)],
                                userBloc: userBloc,
                                litanyElement: litanyElement,
//                          litanyElement: litany][randon.nextInt(litany.length)],
                                //bibleVerse: bibleVerses[random.nextInt(7)],
                                bibleVerse: MemLists().getRandomListElement(
                                    listType: "bible_verse",
                                    locale: userBloc.userDetails.locale),
//                        authorUser:
//                            authorUsers.length > 0 ? authorUsers[0] : User(),
                                appState: AppStateContainer.of(context).state,
                                onDismissed: (direction) {
                                  _removeMessage(context, message);
                                },
                                onCheckboxChanged: (complete) {
                                  //updateMessage(message, complete: !message.complete);
                                },
                                connectionStatus: widget.connectionStatus,
                                routeObserver: widget.routeObserver,
                              );
                            }
                          },
                        ))),
                Positioned(
                  right: 5.0,
                  top: 10.0,
                  child: Container(
                      padding: EdgeInsets.all(3.0),
                      decoration: BoxDecoration(color: Colors.black),
                      child: Text(
                        "Count: " + messages.length.toString(),
                        style: TextStyle(fontSize: 14.0, color: Colors.white),
                      )),
                ),
                Positioned(
                  right: 5.0,
                  top: 40.0,
                  child: Container(
                      padding: EdgeInsets.all(3.0),
                      decoration: BoxDecoration(color: Colors.black),
                      child: Text(
                        "Sum: " + getPendingRosaryCount().toString(),
                        style: TextStyle(fontSize: 14.0, color: Colors.white),
                      )),
                ),
                Positioned(
                  right: 0.0,
                  top: -10.0,
                  child: FlatButton(
                    child: Icon(Icons.keyboard_arrow_up),
                    onPressed: () {
                      try {
                        _scrollController.animateTo(
                          10.0,
                          curve: Curves.easeOut,
                          duration: const Duration(milliseconds: 300),
                        );
                      } catch (e) {
                        debugPrint("Caught Exception");
                      }
                    },
                  ),
                ),
              ])));
    }
  }

  String getLitany() {
    return "";
  }

  int getPendingRosaryCount() {
    int sum = 0;

    for (int count = 0; count < messages.length; count++) {
      MEMmessage message = messages[count];

      sum += message.rosaryByOthers + message.rosaryCount;
    }

    return sum;
  }

  void _removeMessage(BuildContext context, MEMmessage message) {
    final messageBloc = MessageProvider.of(context);

    var container = AppStateContainer.of(context);
    String _deleteMessage;
    String _undoButtonMessage;
    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
        .messageDeleted(message.title);
    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;

    bool _isSuperAdmin = container.state.userDetails.isSuperAdmin;
    bool _eligibleToDelete =
        _isSuperAdmin || message.authorID == container.state.userDetails.id;
    debugPrint("Eligible to delete : " + _eligibleToDelete.toString());
    debugPrint("Author ID: " + message.authorID);
    debugPrint("User ID: " +
        container.state.userDetails.id +
        "Username" +
        container.state.userDetails.name);

    if (_eligibleToDelete) {
      switch (message.type) {
        case "rosary":
          messageBloc.removeRosaryMessage(message, container.state.userDetails);
          _deleteMessage =
              "Rosary Count reduced by" + message.rosaryCount.toString();
          _undoButtonMessage = "";
          break;

        case "request":
        case "thanks":
          messageBloc.removeMessage(message);
          _deleteMessage = "Requets/ Testimony Deleted";
          _undoButtonMessage = "";
          break;

        case "guidance":
          messageBloc.removeMessage(message);
          _deleteMessage = "Counselling Request Deleted";
          _undoButtonMessage = "";
          break;
        case "admin":
          messageBloc.removeRegionalMessage(
              message, container.state.userDetails);
          _deleteMessage = "Admin Message Delete";
          _undoButtonMessage = "";
          break;
      }
    } else {
      _deleteMessage = "Sorry, You cannot delete this message";
      _undoButtonMessage = "";
    }

    Scaffold.of(context).showSnackBar(
      SnackBar(
        key: MemKeys.snackbar,
        duration: Duration(seconds: 3),
        backgroundColor: Theme.of(context).errorColor,
        content: Text(
          _deleteMessage,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.white),
        ),
        action: SnackBarAction(
          label: _undoButtonMessage,
          onPressed: () {
            messageBloc.messageRepository.addRegionalMessage(
                message.toEntity(), container.state.userDetails.region);
            //msgItem.visible = true;
            //addMessage(message);
          },
        ),
      ),
    );
  }
}

// -----   Message List with listeneres -----//
