import 'package:flutter/material.dart';
import 'dart:math';
import 'package:vector_math/vector_math.dart' show radians;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget radialMenuWidget({AnimationController controller}) {
  return RadialAnimation(controller: controller);
}

// The Animation
class RadialAnimation extends StatelessWidget {
  RadialAnimation({Key key, this.controller})
      : scale = Tween<double>(
          begin: 1.5,
          end: 0.0,
        ).animate(
          CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn),
        ),
        rotation = Tween<double>(
          begin: 0.0,
          end: 360.0,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.3,
              0.9,
              curve: Curves.decelerate,
            ),
          ),
        ),
        translation = Tween<double>(
          begin: 0.0,
          end: 100.0,
        ).animate(
          CurvedAnimation(parent: controller, curve: Curves.linear),
        ),
        super(key: key);

  final AnimationController controller;
  final Animation<double> scale;
  final Animation<double> translation;
  final Animation<double> rotation;

  build(context) {
    var offerRosaryIconSize = MediaQuery.of(context).size.width * 0.13;

    return AnimatedBuilder(
        animation: controller,
        builder: (context, builder) {
          return Transform.rotate(
              // Add rotation
              angle: radians(rotation.value),
              child: Stack(alignment: Alignment.center, children: [
                _buildButton(0,
                    color: Colors.red, icon: FontAwesomeIcons.handHoldingHeart),
//                _buildButton(45,
//                    color: Colors.green, icon: FontAwesomeIcons.prayingHands),
//                _buildButton(90,
//                    color: Colors.orange, icon: FontAwesomeIcons.fire),
//                _buildButton(135,
//                    color: Colors.blue, icon: FontAwesomeIcons.kiwiBird),
                _buildButton(180,
                    color: Colors.black, icon: FontAwesomeIcons.prayingHands),
                _buildButton(225,
                    color: Colors.indigo, icon: FontAwesomeIcons.handsHelping),
                _buildButton(270,
                    color: Colors.transparent,
                    icon: FontAwesomeIcons.crown,
                    size: offerRosaryIconSize),
                _buildButton(315,
                    color: Colors.yellow, icon: FontAwesomeIcons.cross),
                Transform.scale(
                  scale: scale.value -
                      1.5, // subtract the beginning value to run the opposite animation
                  child: FloatingActionButton(
                      child: Icon(FontAwesomeIcons.timesCircle),
                      onPressed: _close,
                      backgroundColor: Colors.red),
                ),
                Transform.scale(
                  scale: scale.value,
                  child: FloatingActionButton(
                      //child: Icon(FontAwesomeIcons.solidDotCircle),
                      child: Container(
                        child: ConstrainedBox(
                          constraints: BoxConstraints.tightFor(
                              height: offerRosaryIconSize,
                              width: offerRosaryIconSize),
                          child: Ink.image(
                            image: AssetImage('assets/bg-admin.png'),
                            fit: BoxFit.fill,
                            child: InkWell(
                              onTap: null,
                            ),
                          ),
                        ),
                      ),
                      onPressed: _open),
                )
              ]));
        });
  }

  _buildButton(double angle, {Color color, IconData icon, double size}) {
    final double rad = radians(angle);

    return Transform(
        transform: Matrix4.identity()
          ..translate(
              (translation.value) * cos(rad), (translation.value) * sin(rad)),
        child: FloatingActionButton(
            child: color == Colors.transparent
                ? Container(
                    child: ConstrainedBox(
                      constraints:
                          BoxConstraints.tightFor(height: size, width: size),
                      child: Ink.image(
                        image: AssetImage('assets/heart-rosary-icon.png'),
                        fit: BoxFit.fill,
                        child: InkWell(
                          onTap: null,
                        ),
                      ),
                    ),
                  )
                : Icon(icon),
            backgroundColor: color,
            onPressed: _close,
            elevation: 0));
  }

  _open() {
    controller.forward();
  }

  _close() {
    controller.reverse();
  }
}
