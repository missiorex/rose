// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:intl/intl.dart';
import 'package:memapp/app_state_container.dart';
import 'package:mem_models/mem_models.dart';
import 'dart:math';
import 'package:share/share.dart';
import 'package:recase/recase.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:memapp/screens/profile_screen.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:utility/src/dialogshower.dart' as DialogShower;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memapp/chatroom/chat_screen.dart';
import 'package:liquid_progress_indicator/src/liquid_custom_progress_indicator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:memapp/widgets/widgets.dart';

class LitanyElement extends StatelessWidget {
  final String litanyElement;

  LitanyElement({this.litanyElement});

  @override
  Widget build(BuildContext context) {
    var litanyFontSize = MediaQuery.of(context).size.width * 0.05 * 0.5;

    return Text(
      litanyElement.length > 30
          ? litanyElement.substring(0, 30) + "..."
          : litanyElement,
      textAlign: TextAlign.center,
      maxLines: 2,
      style: TextStyle(
          fontSize: litanyFontSize, color: Theme.of(context).primaryColor),
    );
  }
}

class MessageItem extends StatefulWidget {
  final DismissDirectionCallback onDismissed;
  final GestureTapCallback onTap;
  final ValueChanged<bool> onCheckboxChanged;
  final MEMmessage message;
  final AppState appState;
  final UserBloc userBloc;
  MemUser _user;
  Color headerColor;
  final String litanyElement;
  String bibleVerse;
  final authorUser;
  bool visible = true;
  bool connectionStatus;
  final RouteObserver<PageRoute> routeObserver;
  final List<ChatMessage> guidanceMessages;
  final String lastVisibleChatKey;
  final int lastVisibleChatValue;

  MessageItem(
      {Key key,
      this.onDismissed,
      this.onTap,
      this.onCheckboxChanged,
      this.message,
      this.appState,
      this.userBloc,
      visible,
      this.headerColor,
      this.litanyElement,
      this.bibleVerse,
      this.authorUser,
      this.guidanceMessages,
      this.lastVisibleChatKey,
      this.lastVisibleChatValue,
      this.connectionStatus,
      @required this.routeObserver})
      : super(key: MemKeys.messageItemWidget);

  @override
  State createState() => MessageItemState();
}

class MessageItemState extends State<MessageItem>
    with TickerProviderStateMixin {
  //MessageItem({Key key}) : super(key: MemKeys.messageItemWidget);
  final _messageHeightLimit = 400;
  int _messageLength;
  Color _bibleMsgColor = Colors.transparent;
  double _bibleMsgHeight = 0.0;
  double _bibleMsgWidth = 0.0;
  AnimationController _controller;
  Animation<double> _frontScale;
  Animation<double> _backScale;
  MalTranslator malTranslator = MalTranslator();
  Animation<double> litanyAnimation;
  AnimationController litanyController;
  Stream rwBadgeStream;
  Stream gsBadgeStream;
  String _litanyElement = MemLists().litanyEN;
  String selectedRegion;
  String selectedLocale;
  List<String> locales;
  List<String> regions;
  String adminMsgResponse = "";
//  final Connectivity _connectivity = Connectivity();
//  StreamSubscription<ConnectivityResult> _connectivitySubscription;
//  bool _connectionStatus;

  @override
  void initState() {
    super.initState();

    debugPrint(widget.message.time.toString());

    _messageLength = _messageHeightLimit;
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );
    _frontScale = Tween(
      begin: 1.0,
      end: 0.0,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(0.0, 0.5, curve: Curves.easeIn),
    ));
    _backScale = CurvedAnimation(
      parent: _controller,
      curve: Interval(0.5, 1.0, curve: Curves.easeOut),
    );

    litanyController =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    litanyAnimation =
        CurvedAnimation(parent: litanyController, curve: Curves.easeIn)
          ..addStatusListener((status) {});
    litanyController.forward();

    rwBadgeStream = FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(widget.message.authorID)
        .collection('badges')
        .where('action', isEqualTo: 'ADD ROSARY')
        .snapshots();

    gsBadgeStream = FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(widget.message.authorID)
        .collection('badges')
        .where('action', isEqualTo: 'COLLECT ROSARY')
        .snapshots();
  }

  @override
  void dispose() {
    litanyController.dispose();
    _controller.dispose();
    super.dispose();
  }

  bool isColorDark(Color color) {
    double darkness = 1 -
        (0.299 * color.red + 0.587 * color.green + 0.114 * color.blue) / 255;
    if (darkness < 0.3) {
      return false; // It's a light color
    } else {
      return true; // It's a dark color
    }
  }

  void deleteRegionalMessage(List<String> _regions, MessageBloc messageBloc,
      String msgID, MemUser userDetails) {
    DatabaseReference _messagesReference =
        FirebaseDatabase.instance.reference();
    String confirmation = "";
    double successRate = 0.0;

    showPleaseWait();

    _regions.forEach((region) async {
      confirmation = await messageBloc.deleteRegionalLocaleMessage(
          msgID, region, userDetails.locale);

      if (confirmation != '') {
        successRate += 0.2;
        adminMsgResponse = adminMsgResponse + "\r\n" + confirmation;
        showAdminMessageProgress(
            successRate: successRate,
            confirmation: adminMsgResponse,
            singleRegion: false);
      }
    });

    // Remove the message from the chat room.
    _messagesReference
        .child("chats")
        .child(userDetails.locale ?? "English")
        .child(msgID)
        .remove();
  }

  void showAdminMessageProgress(
      {double successRate, String confirmation, bool singleRegion}) {
    var deviceHeight = MediaQuery.of(context).size.height;
    var deviceWidth = MediaQuery.of(context).size.width;

    debugPrint("Succes Rate: " + successRate.toString());

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: Center(child: Text('Deleting Admin Message')),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              actions: <Widget>[
                RaisedButton(
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black12,
                    color: Theme.of(context).buttonColor,
                    child: Text(
                      "OK",
                    ),
                    onPressed: successRate > 0.0
                        ? () {
                            setState(() {
                              adminMsgResponse = "";
                            });

                            Navigator.pop(context);
                            Navigator.popUntil(
                                context, ModalRoute.withName('/home'));
                          }
                        : null)
              ],
              backgroundColor: Theme.of(context).primaryColor,
              content: Stack(children: <Widget>[
                Positioned(
                    right: 10.0,
                    top: 10.0,
                    //return dialog;
                    child: LiquidCustomProgressIndicator(
                      value: successRate * 6, // Defaults to 0.5.
                      valueColor: AlwaysStoppedAnimation(Colors
                          .pink), // Defaults to the current Theme's accentColor.
                      backgroundColor: Colors
                          .white, // Defaults to the current Theme's backgroundColor.
                      direction: Axis
                          .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right).
                      shapePath: Path()
                        ..moveTo(50, 0)
                        ..quadraticBezierTo(0, 0, 0, 37.5)
                        ..quadraticBezierTo(0, 75, 25, 75)
                        ..quadraticBezierTo(25, 95, 5, 95)
                        ..quadraticBezierTo(35, 95, 40, 75)
                        ..quadraticBezierTo(100, 75, 100, 37.5)
                        ..quadraticBezierTo(100, 0, 50, 0)
                        ..close(), // A Path object used to draw the shape of the progress indicator. The size of the progress indicator is created from the bounds of this path.
                    )),
                Positioned(
                    left: 10.0,
                    top: 100.0,
                    //return dialog;
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: deviceHeight * 0.7,
                        width: deviceWidth * 0.6,
                        child: Text(
                          successRate > 0.0
                              ? "The message deleted succesfully . "
                              : "Message Deletion in Progress ...",
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Theme.of(context).backgroundColor,
                              fontWeight: FontWeight.bold),
                          maxLines: 10,
                        ))),
                Positioned(
                    left: 10.0,
                    top: 150.0,
                    //return dialog;
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: deviceHeight * 0.7,
                        width: deviceWidth * 0.7,
                        child: Text(
                          "\n Messages deleted from : \n" + confirmation,
                          style: TextStyle(fontSize: 11.0),
                          maxLines: 20,
                        )))
              ]));
        });
  }

  void showPleaseWait() {
    var deviceHeight = MediaQuery.of(context).size.height;
    var deviceWidth = MediaQuery.of(context).size.width;

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: Center(child: Text('Deleting Admin Message')),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              actions: <Widget>[
                RaisedButton(
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black12,
                  child: Text(
                    "OK",
                  ),
                  onPressed: null,
                )
              ],
              backgroundColor: Theme.of(context).primaryColor,
              content: Stack(children: <Widget>[
                Positioned(
                    right: 10.0,
                    top: 10.0,
                    //return dialog;
                    child: LiquidCustomProgressIndicator(
                      value: 0.2, // Defaults to 0.5.
                      valueColor: AlwaysStoppedAnimation(Colors
                          .pink), // Defaults to the current Theme's accentColor.
                      backgroundColor: Colors
                          .white, // Defaults to the current Theme's backgroundColor.
                      direction: Axis
                          .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right).
                      shapePath: Path()
                        ..moveTo(50, 0)
                        ..quadraticBezierTo(0, 0, 0, 37.5)
                        ..quadraticBezierTo(0, 75, 25, 75)
                        ..quadraticBezierTo(25, 95, 5, 95)
                        ..quadraticBezierTo(35, 95, 40, 75)
                        ..quadraticBezierTo(100, 75, 100, 37.5)
                        ..quadraticBezierTo(100, 0, 50, 0)
                        ..close(), // A Path object used to draw the shape of the progress indicator. The size of the progress indicator is created from the bounds of this path.
                    )),
                Positioned(
                    left: 10.0,
                    top: 100.0,
                    //return dialog;
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: deviceHeight * 0.7,
                        width: deviceWidth * 0.6,
                        child: Text(
                          "Please wait",
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Theme.of(context).backgroundColor,
                              fontWeight: FontWeight.bold),
                          maxLines: 10,
                        ))),
                Positioned(
                    left: 10.0,
                    top: 150.0,
                    //return dialog;
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: deviceHeight * 0.7,
                        width: deviceWidth * 0.6,
                        child: Text(
                          "System is preparing to delete the messages from the selected regions....",
                          style: TextStyle(fontSize: 15.0),
                          maxLines: 10,
                        )))
              ]));
        });
  }

  @override
  Widget build(BuildContext context) {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    // Element Dimensions
    var profilePhotoRadius = MediaQuery.of(context).size.width * 0.06;

    var rosaryCountTitleFontSize = MediaQuery.of(context).size.width * 0.05;
    var rosaryCountTitleLeftMargin = MediaQuery.of(context).size.width * 0.19;
    var rosaryCountTitleTopMargin = MediaQuery.of(context).size.width * 0.020;

    var offeredByTopMargin =
        rosaryCountTitleTopMargin + rosaryCountTitleFontSize * 1.15;
    var offeredByLeftMargin =
        rosaryCountTitleLeftMargin + rosaryCountTitleFontSize * .04;
    var offeredByFontSize = rosaryCountTitleFontSize * 0.5;

    var msgTextFontSize = rosaryCountTitleFontSize * 0.7;

    var badgeSize = MediaQuery.of(context).size.width * 0.05;
    var starBadgeSize = MediaQuery.of(context).size.width * 0.044;
    var badgeRightMargin = MediaQuery.of(context).size.width * 0.07;
    var badgeTopMargin = MediaQuery.of(context).size.width * 0.03;
    var iconSize = badgeSize;

    var litanyFontSize = rosaryCountTitleFontSize * 0.5;

    var textContentPadding = msgTextFontSize * 2.5;
    var msgBoxPadding = MediaQuery.of(context).size.width * 0.03;

    bool _isRosaryAdmin = container.state.userAcl != null
        ? container.state.userDetails.isSuperAdmin &&
            container.state.userAcl.rosaryDelete
        : false;
    bool _isMessageAdmin = container.state.userAcl != null
        ? container.state.userDetails.isSuperAdmin &&
            container.state.userAcl.messageDelete
        : false;

    bool _eligibleToDelete = false;

    var msgBodyStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Colors.black, fontWeight: FontWeight.w400, fontSize: 14.0);

    if (widget.message.type == "rosary") {
      _eligibleToDelete = _isRosaryAdmin ||
          widget.message.authorID == container.state.userDetails.id &&
              widget.message.complete;
    } else {
      _eligibleToDelete = _isMessageAdmin ||
          widget.message.authorID == container.state.userDetails.id;
    }

    return Stack(children: <Widget>[
      AnimatedBuilder(
        child: AnimatedOpacity(
            opacity: widget.visible ? 1.0 : 0.0,
            duration: Duration(milliseconds: 500),
            child: Container(
                margin:
                    const EdgeInsets.only(left: 6.0, right: 10.0, bottom: 10.0),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  //Color.fromARGB(205, 254, 246, 226),
                  //widget.message.type != "admin"
//                  ? Color.fromARGB(180, 254, 246, 226)
//                  : Color.fromARGB(230, 236, 234, 217),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.black26,
                        offset: new Offset(1.0, 1.0),
                        blurRadius: 0.3,
                        spreadRadius: 0.3)
                  ],
                  borderRadius:
                      new BorderRadius.all(const Radius.circular(5.0)),
                ),
                child: Column(children: [
                  Container(
                      padding: new EdgeInsets.only(
                          top: 5.0,
                          bottom: 5.0,
                          left: msgBoxPadding,
                          right: msgBoxPadding),
                      decoration: BoxDecoration(
                        color: widget.message.type != "admin"
                            ? widget.headerColor
                            : Theme.of(context).buttonColor,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(5.0),
                            topRight: const Radius.circular(5.0)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  child: Text(
                            widget.message.type != "admin"
                                ? ReCase(widget.message.author).titleCase ?? ""
                                : "Admin",
                            style: TextStyle(
                              color: isColorDark(widget.headerColor)
                                  ? Colors.white
                                  : Colors.black,
                              fontFamily: "Bahnschrift",
                              fontWeight: FontWeight.w600,
                              fontSize: rosaryCountTitleFontSize * 0.6,
                            ),
                          ))),
                          Container(
                              child: Text(
                                new DateFormat.yMMMd()
                                    .format(widget.message.time),
                                style: TextStyle(
                                    color: isColorDark(widget.headerColor)
                                        ? Colors.white
                                        : Colors.black,
                                    fontWeight: FontWeight.w600,
                                    fontSize: rosaryCountTitleFontSize * 0.4),
                              ),

                              ///todo: write a stream for formatted date & time.
                              margin: const EdgeInsets.only(right: 40.0)),
                          Text(
                            new DateFormat.jm().format(widget.message.time),
                            style: TextStyle(
                                color: isColorDark(widget.headerColor)
                                    ? Colors.white
                                    : Colors.black,
                                fontWeight: FontWeight.w600,
                                fontSize: rosaryCountTitleFontSize * 0.4),
                          ),
                        ],
                      )),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Container(
                          margin: EdgeInsets.only(
                              top: 15.0, bottom: 0.0, left: 10.0, right: 10.0),
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColorLight,
                            borderRadius:
                                BorderRadius.all(Radius.circular(2.0)),
//                              border:
//                                  Border.all(color: Colors.grey, width: 0.3)
                          ),
                          key: MemKeys.messageItem(widget.message.id),
                          child: widget.message.type == "rosary"
                              ? Stack(children: <Widget>[
                                  FutureBuilder(
                                      future: widget.userBloc
                                          .authorUser(widget.message.authorID),
                                      builder: (context, snapshot) {
                                        if (snapshot.connectionState ==
                                            ConnectionState.done) {
                                          return CircleAvatar(
                                            radius: profilePhotoRadius,
                                            backgroundImage: snapshot.data !=
                                                    null
                                                ? CachedNetworkImageProvider(snapshot
                                                            .data
                                                            .profilePhotoUrl ??
                                                        "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a") ??
                                                    NetworkImage(snapshot.data
                                                            .profilePhotoUrl ??
                                                        "") ??
                                                    AssetImage(
                                                        "assets/placeholder-face.png")
                                                : AssetImage(
                                                    "assets/placeholder-face.png"),
                                          );
                                        } else
                                          return CircleAvatar(
                                            radius: profilePhotoRadius,
                                            backgroundImage: AssetImage(
                                                "assets/placeholder-face.png"),
                                          );
                                      }),

//                                  CircleAvatar(
//                                    radius: 20.0,
//                                    backgroundImage: CachedNetworkImageProvider(
//                                            widget.authorUser.profilePhotoUrl ??
//                                                "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a") ??
//                                        NetworkImage(
//                                            widget.authorUser.profilePhotoUrl ??
//                                                "") ??
//                                        AssetImage(
//                                            "assets/placeholder-face.png"),
//                                  ),
                                  Positioned(
                                    // draw a red marble
                                    top: 0.0,
                                    right: 0.0,
                                    child: widget.message.complete
                                        ? Container()
//                                      : CircularProgressIndicator(
//                                          valueColor:
//                                              new AlwaysStoppedAnimation<Color>(
//                                                  widget.headerColor),
//                                        )
                                        : Icon(Icons.brightness_1,
                                            size: 8.0, color: Colors.redAccent),
                                  ),
                                  Positioned(
                                      top: rosaryCountTitleTopMargin,
                                      left: rosaryCountTitleLeftMargin,
                                      child: widget.message.type == "rosary"
                                          ? widget.message.rosaryCount == 1 ||
                                                  widget.message
                                                          .rosaryByOthers ==
                                                      1
                                              ? Text(
                                                  widget.message.rosaryCount ==
                                                          0
                                                      ? widget.message
                                                              .rosaryByOthers
                                                              .toString() +
                                                          " | ROSARY"
                                                      : widget.message
                                                              .rosaryCount
                                                              .toString() +
                                                          " | ROSARY",
                                                  style: TextStyle(
                                                      fontSize:
                                                          rosaryCountTitleFontSize,
                                                      color: Color.fromRGBO(
                                                          66, 65, 63, 1.0)),
                                                )
                                              : Text(
                                                  widget.message.rosaryCount ==
                                                          0
                                                      ? widget.message
                                                              .rosaryByOthers
                                                              .toString() +
                                                          " | ROSARIES"
                                                      : widget.message
                                                              .rosaryCount
                                                              .toString() +
                                                          " | ROSARIES",
                                                  style: TextStyle(
                                                      fontSize:
                                                          rosaryCountTitleFontSize,
                                                      color: Color.fromRGBO(
                                                          66, 65, 63, 1.0)),
                                                )
                                          : Container()),
                                  Positioned(
                                      top: badgeTopMargin,
                                      right: badgeRightMargin * 0.7,
                                      child: Container(
                                              height: 40.0,
                                              width: 40.0,
                                              child: StreamBuilder(
                                                stream: rwBadgeStream,
                                                builder: (BuildContext context,
                                                    snapshot) {
                                                  if (snapshot.hasError)
                                                    return Container();
                                                  if (!snapshot.hasData ||
                                                      snapshot.data == null) {
                                                    return Container();
                                                  } else {
                                                    switch (snapshot
                                                        .connectionState) {
                                                      case ConnectionState
                                                          .waiting:
                                                        return SizedBox(
                                                            width: badgeSize,
                                                            height: badgeSize,
                                                            child: CircularProgressIndicator(
                                                                strokeWidth:
                                                                    1.0,
                                                                valueColor:
                                                                    AlwaysStoppedAnimation<
                                                                            Color>(
                                                                        Colors
                                                                            .black26)));
                                                      case ConnectionState.done:
                                                      default:
                                                        try {
                                                          return Stack(
                                                              children: <
                                                                  Widget>[
                                                                Image.asset(
                                                                  "assets/crown_" +
                                                                      snapshot
                                                                          .data
                                                                          .docs
                                                                          .last[
                                                                              "level"]
                                                                          .toString() +
                                                                      ".png",
                                                                  height:
                                                                      badgeSize,
                                                                ),
//                                                        Positioned(
//                                                            top: 6.0,
//                                                            left: 5.0,
//                                                            child: Icon(
//                                                                Icons
//                                                                    .brightness_1,
//                                                                size: 10.0,
//                                                                color: Colors
//                                                                    .redAccent)),
//                                                        Positioned(
//                                                          top: 7.0,
//                                                          left: 8.0,
//                                                          child: Text(
//                                                            snapshot
//                                                                .data
//                                                                .documents
//                                                                .last["level"]
//                                                                .toString(),
//                                                            style: TextStyle(
//                                                                fontSize: 8.0,
//                                                                color: Colors
//                                                                    .white),
//                                                          ),
//                                                        )
                                                              ]);
                                                        } catch (e) {
                                                          return Container();
                                                        }
                                                    }
                                                  }
                                                },
                                              )) ??
                                          Container()),
                                  Positioned(
                                      top: badgeTopMargin,
                                      right: badgeRightMargin - badgeSize,
                                      child: StreamBuilder(
                                            stream: gsBadgeStream,
                                            builder: (BuildContext context,
                                                snapshot) {
                                              if (snapshot.hasError)
                                                return Container();
                                              if (!snapshot.hasData ||
                                                  snapshot.data == null) {
                                                return Container();
                                              } else {
                                                switch (
                                                    snapshot.connectionState) {
                                                  case ConnectionState.waiting:
                                                    return Center(
                                                        child: CircularProgressIndicator(
                                                            valueColor:
                                                                AlwaysStoppedAnimation<
                                                                        Color>(
                                                                    Colors
                                                                        .transparent)));

                                                  default:
                                                    try {
                                                      return Stack(children: <
                                                          Widget>[
                                                        Image.asset(
                                                          "assets/star_" +
                                                              snapshot.data.docs
                                                                  .last["level"]
                                                                  .toString() +
                                                              ".png",
                                                          height: starBadgeSize,
                                                        ),
//                                                        Positioned(
//                                                            top: 6.0,
//                                                            left: 5.0,
//                                                            child: Icon(
//                                                                Icons
//                                                                    .brightness_1,
//                                                                size: 10.0,
//                                                                color: Colors
//                                                                    .redAccent)),
//                                                        Positioned(
//                                                          top: 7.0,
//                                                          left: 8.0,
//                                                          child: Text(
//                                                            snapshot
//                                                                .data
//                                                                .documents
//                                                                .last["level"]
//                                                                .toString(),
//                                                            style: TextStyle(
//                                                                fontSize: 8.0,
//                                                                color: Colors
//                                                                    .white),
//                                                          ),
//                                                        )
                                                      ]);
                                                    } catch (e) {
                                                      return Container();
                                                    }
                                                }
                                              }
                                            },
                                          ) ??
                                          Container()),
//                                  Positioned(
//                                    top: 10.0,
//                                    right: 8.0,
//                                    child: Image.asset(
//                                      "assets/golden_star.png",
//                                      height: 18.0,
//                                    ),
//                                  ),
//                                  Positioned(
//                                    top: 10.0,
//                                    right: 10.0,
//                                    child: Image.asset(
//                                      "assets/blue_star.png",
//                                      height: 15.0,
//                                    ),
//                                  ),
                                  Positioned(
                                    top: offeredByTopMargin,
                                    left: offeredByLeftMargin,
                                    child: widget.message.type == "rosary"
                                        ? Text(
                                            widget.message.title,
                                            style: TextStyle(
                                                fontSize: offeredByFontSize,
                                                color: Color.fromRGBO(
                                                    66, 65, 63, 1.0)),
                                          )
                                        : Container(),
                                  ),
                                  Positioned(
                                      top: 30.0,
                                      left: 0.0,
                                      child: widget.message.type != "rosary"
                                          ? Theme.of(context).platform ==
                                                  TargetPlatform.iOS
                                              ? Text(
                                                  malTranslator.isMal(
                                                          widget.message.body)
                                                      ? malTranslator
                                                          .formatMalContent(
                                                              widget
                                                                  .message.body)
                                                      : widget.message.body,
                                                  key: MemKeys.messageItemBody(
                                                      widget.message.id),
                                                  maxLines: 100,
                                                  textAlign: TextAlign.justify,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: malTranslator.isMal(
                                                          widget.message.body)
                                                      ? Theme.of(context)
                                                          .textTheme
                                                          .subtitle2
                                                          .copyWith(
                                                              fontSize:
                                                                  msgTextFontSize,
                                                              fontFamily:
                                                                  'Karthika')
                                                      : Theme.of(context)
                                                          .textTheme
                                                          .subtitle2
                                                          .copyWith(
                                                              fontSize:
                                                                  msgTextFontSize,
                                                              fontFamily:
                                                                  'Bahnschrift'),
                                                )
                                              : Text(
                                                  widget.message.body,
                                                  key: MemKeys.messageItemBody(
                                                      widget.message.id),
                                                  maxLines: 100,
                                                  textAlign: TextAlign.justify,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subhead
                                                      .copyWith(
                                                        fontSize:
                                                            msgTextFontSize,
                                                      ),
                                                )
                                          : Container()),
                                ])

                              // iOS Message content
                              : ListTile(
                                  contentPadding:
                                      EdgeInsets.only(top: 0.0, left: 5.0),
                                  title: Theme.of(context).platform ==
                                          TargetPlatform.iOS
                                      ? Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                              Row(children: <Widget>[
                                                FutureBuilder(
                                                    future: widget.userBloc
                                                        .authorUser(widget
                                                            .message.authorID),
                                                    builder:
                                                        (context, snapshot) {
                                                      if (snapshot
                                                              .connectionState ==
                                                          ConnectionState
                                                              .done) {
                                                        return CircleAvatar(
                                                          radius:
                                                              profilePhotoRadius,
                                                          backgroundImage: snapshot
                                                                      .data !=
                                                                  null
                                                              ? snapshot.data
                                                                          .role ==
                                                                      "admin"
                                                                  ? AssetImage(
                                                                      "assets/logo_drawer.png")
                                                                  : CachedNetworkImageProvider(snapshot.data.profilePhotoUrl ?? "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a") ??
                                                                      NetworkImage(snapshot
                                                                              .data
                                                                              .profilePhotoUrl ??
                                                                          "") ??
                                                                      AssetImage(
                                                                          "assets/placeholder-face.png")
                                                              : AssetImage(
                                                                  "assets/placeholder-face.png"),
                                                        );
                                                      } else
                                                        return CircleAvatar(
                                                          radius:
                                                              profilePhotoRadius,
                                                          backgroundImage:
                                                              AssetImage(
                                                                  "assets/placeholder-face.png"),
                                                        );
                                                    }),
//                                                CircleAvatar(
//                                                  radius: 20.0,
//                                                  backgroundImage:
//                                                      CachedNetworkImageProvider(
//                                                              widget.authorUser
//                                                                      .profilePhotoUrl ??
//                                                                  "") ??
//                                                          NetworkImage(widget
//                                                                  .authorUser
//                                                                  .profilePhotoUrl ??
//                                                              "") ??
//                                                          AssetImage(
//                                                              "assets/placeholder-face.png"),
//                                                ),
                                              ]),
                                              widget.message.type == "rosary"
                                                  ? Container()
                                                  : Container(
                                                      margin: EdgeInsets.only(
                                                          left: 20.0),
                                                      padding:
                                                          EdgeInsets.all(10.0),
                                                      child: Text(
                                                          widget.message.type ==
                                                                  "admin"
                                                              ? "Message From Admin"
                                                              : widget.message
                                                                          .type ==
                                                                      "request"
                                                                  ? "Prayer Request"
                                                                  : widget.message
                                                                              .type ==
                                                                          "guidance"
                                                                      ? "Guidance Request"
                                                                      : "Testimony",
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'Bahnschrift',
                                                            fontSize:
                                                                msgTextFontSize *
                                                                    1.3,
                                                          ))),
                                            ])

                                      // Android  Message content
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                              Row(children: <Widget>[
                                                FutureBuilder(
                                                    future: widget.userBloc
                                                        .authorUser(widget
                                                            .message.authorID),
                                                    builder:
                                                        (context, snapshot) {
                                                      if (snapshot
                                                              .connectionState ==
                                                          ConnectionState
                                                              .done) {
                                                        return CircleAvatar(
                                                          radius:
                                                              profilePhotoRadius,
                                                          backgroundImage: snapshot
                                                                      .data !=
                                                                  null
                                                              ? snapshot.data
                                                                          .role ==
                                                                      "admin"
                                                                  ? AssetImage(
                                                                      "assets/logo_drawer.png")
                                                                  : CachedNetworkImageProvider(snapshot.data.profilePhotoUrl ?? "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a") ??
                                                                      NetworkImage(snapshot
                                                                              .data
                                                                              .profilePhotoUrl ??
                                                                          "") ??
                                                                      AssetImage(
                                                                          "assets/placeholder-face.png")
                                                              : AssetImage(
                                                                  "assets/placeholder-face.png"),
                                                        );
                                                      } else
                                                        return CircleAvatar(
                                                          radius:
                                                              profilePhotoRadius,
                                                          backgroundImage:
                                                              AssetImage(
                                                                  "assets/placeholder-face.png"),
                                                        );
                                                    }),
//                                                CircleAvatar(
//                                                  radius: 20.0,
//                                                  backgroundImage:
//                                                      CachedNetworkImageProvider(
//                                                              widget.authorUser
//                                                                      .profilePhotoUrl ??
//                                                                  "") ??
//                                                          NetworkImage(widget
//                                                                  .authorUser
//                                                                  .profilePhotoUrl ??
//                                                              "") ??
//                                                          AssetImage(
//                                                              "assets/placeholder-face.png"),
//                                                ),
                                              ]),
                                              widget.message.type == "rosary"
                                                  ? Container()
                                                  : Container(
                                                      margin:
                                                          EdgeInsets.only(
                                                              left: 20.0),
                                                      padding: EdgeInsets.all(
                                                          10.0),
                                                      child: Text(
                                                          widget.message.type ==
                                                                  "admin"
                                                              ? "Message From Admin"
                                                              : widget.message
                                                                          .type ==
                                                                      "request"
                                                                  ? "Prayer Request"
                                                                  : widget.message
                                                                              .type ==
                                                                          "guidance"
                                                                      ? "Guidance Request"
                                                                      : "Testimony",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Bahnschrift',
                                                              fontSize:
                                                                  msgTextFontSize *
                                                                      1.3)))
                                            ]),
                                  subtitle: Column(
                                    children: <Widget>[
                                      Column(children: <Widget>[
                                        widget.message.title != ""
                                            ? Container(
                                                padding: EdgeInsets.only(
                                                  top: 10.0,
                                                ),
                                                child: Theme.of(context)
                                                            .platform ==
                                                        TargetPlatform.iOS
                                                    ? Text(
                                                        widget.message.title,
                                                        key: MemKeys
                                                            .messageItemTitle(
                                                                widget.message
                                                                    .id),
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize:
                                                              msgTextFontSize *
                                                                  1.2,
                                                        ),
                                                      )
                                                    : Text(
                                                        malTranslator.isMal(
                                                                widget.message
                                                                    .title)
                                                            ? malTranslator
                                                                .formatMalContent(
                                                                    widget
                                                                        .message
                                                                        .title)
                                                            : widget
                                                                .message.title,
                                                        key: MemKeys
                                                            .messageItemTitle(
                                                                widget.message
                                                                    .id),
                                                        textAlign: widget
                                                                    .message
                                                                    .type ==
                                                                "rosary"
                                                            ? TextAlign.center
                                                            : TextAlign.left,
                                                        style: malTranslator
                                                                .isMal(widget
                                                                    .message
                                                                    .title)
                                                            ? Theme.of(context)
                                                                .textTheme
                                                                .headline6
                                                                .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        msgTextFontSize *
                                                                            1.2,
                                                                    fontFamily:
                                                                        'Karthika')
                                                            : Theme.of(context)
                                                                .textTheme
                                                                .headline6
                                                                .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        msgTextFontSize *
                                                                            1.2,
                                                                    fontFamily:
                                                                        'Bahnschrift'),
                                                      ))
                                            : Container(),
                                        Container(
                                          padding: EdgeInsets.only(top: 5.0),
//                                          duration:
//                                              const Duration(milliseconds: 120),
                                          child: widget.message.body.length >
                                                  _messageHeightLimit
                                              ? Theme.of(context).platform ==
                                                      TargetPlatform.iOS
                                                  ? Text(
                                                      malTranslator.isMal(widget
                                                              .message.body)
                                                          ? malTranslator
                                                              .formatMalContent(
                                                                  widget.message
                                                                      .body
                                                                      .substring(
                                                                          0,
                                                                          _messageLength))
                                                          : widget.message.body
                                                              .substring(0,
                                                                  _messageLength),
                                                      key: MemKeys
                                                          .messageItemBody(
                                                              widget
                                                                  .message.id),
                                                      maxLines: 100,
                                                      textAlign:
                                                          TextAlign.justify,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: malTranslator
                                                              .isMal(widget
                                                                  .message.body)
                                                          ? Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                                  fontSize:
                                                                      msgTextFontSize,
                                                                  fontFamily:
                                                                      'Karthika')
                                                          : Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                                  fontSize:
                                                                      msgTextFontSize,
                                                                  fontFamily:
                                                                      'Bahnschrift'),
                                                    )
                                                  : Text(
                                                      widget.message.body
                                                          .substring(0,
                                                              _messageLength),
                                                      style: TextStyle(
                                                          fontSize:
                                                              msgTextFontSize))
                                              : Theme.of(context).platform ==
                                                      TargetPlatform.iOS
                                                  ? Text(
                                                      malTranslator.isMal(
                                                              widget
                                                                  .message.body)
                                                          ? malTranslator
                                                              .formatMalContent(
                                                                  widget.message
                                                                      .body)
                                                          : widget.message.body,
                                                      key: MemKeys
                                                          .messageItemBody(
                                                              widget
                                                                  .message.id),
                                                      maxLines: 100,
                                                      textAlign:
                                                          TextAlign.justify,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: malTranslator
                                                              .isMal(widget
                                                                  .message.body)
                                                          ? Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                                  fontSize:
                                                                      msgTextFontSize,
                                                                  fontFamily:
                                                                      'Karthika')
                                                          : Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                                  fontSize:
                                                                      msgTextFontSize,
                                                                  fontFamily:
                                                                      'Bahnschrift'),
                                                    )
                                                  : Text(widget.message.body,
                                                      style: TextStyle(
                                                          fontSize:
                                                              msgTextFontSize)),
                                        ),
                                        GestureDetector(
                                          onTap: () => setState(() {
                                            _messageLength !=
                                                    _messageHeightLimit
                                                ? _messageLength =
                                                    _messageHeightLimit
                                                : _messageLength =
                                                    widget.message.body.length;
                                          }),
                                          child: Container(
                                            child: widget.message.body.length >
                                                    _messageHeightLimit
                                                ? Text(
                                                    _messageLength ==
                                                            _messageHeightLimit
                                                        ? "...Read More"
                                                        : "",
                                                    style: TextStyle(
                                                        color:
                                                            Colors.blueAccent,
                                                        fontSize:
                                                            msgTextFontSize *
                                                                1.1),
                                                  )
                                                : Container(),
                                          ),
                                        ),
                                      ]),
                                    ],
                                  ),
                                ),
                        ))
                      ]),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                          child: Container(
                              padding: !_eligibleToDelete
                                  ? EdgeInsets.only(top: 10.0, bottom: 10.0)
                                  : EdgeInsets.all(0.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  widget.message.type == "rosary"
                                      ? IconTheme(
                                          data: IconThemeData(
                                              color: widget.headerColor),
                                          child: ImageIcon(
                                            AssetImage("assets/cross-icon.png"),
                                            size: 15.5,
                                          ))
                                      : Container(),
                                  widget.message.type == "rosary"
                                      ?
                                      // The green box needs to be the child of the AnimatedOpacity
                                      Container(
                                          margin: EdgeInsets.only(left: 3.0),
                                          child: LitanyElement(
                                              litanyElement: _litanyElement),
//                                          Text(
//                                            widget.litanyElement.length > 30
//                                                ? widget.litanyElement
//                                                        .substring(0, 30) +
//                                                    "..."
//                                                : widget.litanyElement,
//                                            textAlign: TextAlign.center,
//                                            maxLines: 2,
//                                            style: TextStyle(
//                                              fontSize: litanyFontSize,
//                                            ),
//                                          )
                                        )
                                      : Container(),
//                                      AnimatedTextElement(
//                                          //animation: litanyAnimation,
//                                          )
//                                      : Container(),
//                      widget.message.type == "rosary"
//                          ? IconTheme(
//                              data: IconThemeData(color: widget.headerColor),
//                              child: ImageIcon(
//                                AssetImage("assets/cross-icon.png"),
//                                size: 15.5,
//                              ))
//                          : Container(),
                                ],
                              ))),
                      widget.message.type == "rosary"
                          ? Container()
                          : IconButton(
                              color: Colors.grey,
                              icon: ImageIcon(
                                  AssetImage('assets/icons/share.png'),
                                  size: iconSize),
                              onPressed: widget.message.body.isEmpty
                                  ? null
                                  : (() {
                                      final RenderBox box =
                                          context.findRenderObject();
                                      Share.share(widget.message.body,
                                          sharePositionOrigin:
                                              box.localToGlobal(Offset.zero) &
                                                  box.size);
                                    })),
                      widget.message.type != "guidance"
                          ? Container()
                          : IconButton(
                              color: Colors.grey,
                              icon: ImageIcon(
                                  AssetImage(
                                      'assets/icons/counselling-icon.png'),
                                  size: iconSize),
                              onPressed: widget.message.body.isEmpty
                                  ? null
                                  : (() {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => ChatScreen(
                                              userBloc: userBloc,
                                              bgImageUrl: "assets/bg-admin.jpg",
                                              connectionStatus:
                                                  widget.connectionStatus,
                                              publicChat: false,
                                              chatRoomID:
                                                  widget.message.authorID,
                                              fcmNotificationEnable: false,
                                              routeObserver:
                                                  widget.routeObserver,
                                              messages: widget.guidanceMessages,
                                              lastVisibleChatKey:
                                                  widget.lastVisibleChatKey,
                                              lastVisibleChatValue:
                                                  widget.lastVisibleChatValue,
                                            ),
                                          ));

//                                      Navigator.push(
//                                          context,
//                                          MaterialPageRoute(
//                                            builder: (context) =>
//                                                PrivateChatScreen(
//                                              appState:
//                                                  AppStateContainer.of(context)
//                                                      .state,
//                                              userBloc: userBloc,
//                                              routeObserver:
//                                                  widget.routeObserver,
//                                              connectionStatus:
//                                                  widget.connectionStatus,
//                                              bgImageUrl: "assets/bg-star.png",
//                                              initialChatMessage:
//                                                  widget.message.body,
//                                              chatReceiverId:
//                                                  userBloc.userDetails.id,
//                                              chatRequesterID:
//                                                  widget.message.authorID,
//                                              chatRequesterName:
//                                                  widget.message.author,
//                                              chatRequestTime: widget.message
//                                                  .time.millisecondsSinceEpoch,
//                                              chatType: "counselling",
//                                              initialChatId: widget.message.id,
//                                            ),
//                                          ));
                                    })),
                      widget.message.type == "rosary"
                          ? Container()
                          : AnimatedContainer(
                              height: _bibleMsgHeight,
                              width: _bibleMsgWidth,
                              color: _bibleMsgColor,
                              duration: Duration(seconds: 10),
                            ),
                      _eligibleToDelete
                          ? IconButton(
                              iconSize: iconSize,
                              color: Colors.grey,
                              key: MemKeys.deleteMessageButton,
                              tooltip: MemLocalizations(
                                      Localizations.localeOf(context))
                                  .deleteMessage,
                              icon: ImageIcon(
                                  AssetImage('assets/icons/icons8-trash.png'),
                                  color: Colors.redAccent,
                                  size: iconSize),
                              onPressed: () {
                                if (widget.connectionStatus) {
                                  confirmDelete(
                                    errorMsg: MemError(
                                        title:
                                            "Are you sure ? Please confirmx to delete",
                                        body:
                                            "You are deleting a message. Please press OK to delete."),
                                  );
//                                  return ShowDialog(
//                                      title:
//                                          "Are you sure ? Please confirm to delete",
//                                      content:
//                                          "You are deleting a message. Please press Delete to confirm delete.",
//                                      okayText: 'Okay',
//                                      cancelText: "Cancel",
//                                      okayCallBack: deleteConfirmed,
//                                      cancelCallBack: cancelDelete);
                                } else {
                                  showError(
                                    errorMsg: MemError(
                                        title: "Not Connected",
                                        body:
                                            "Please check your internet connection. You can delete a message only when you are online."),
                                  );
                                }

                                //Navigator.pop(context, message);
                              },
                            )
                          : Container(),
                      widget.message.type == "rosary"
                          ? Container()
                          : IconButton(
                              color: Colors.grey,
                              icon: ImageIcon(
                                  AssetImage(
                                      'assets/icons/icons8-holy-bible.png'),
                                  size: iconSize,
                                  color: Color.fromRGBO(132, 4, 16, 1)),
                              //icon: Icon(Icons.book, size: 18.0),
                              onPressed: widget.message.body.isEmpty
                                  ? null
                                  : (() {
//                                      _bibleMsgColor = Colors.green;
//                                      _bibleMsgHeight =
//                                          MediaQuery.of(context).size.height;
//                                      _bibleMsgWidth =
//                                          MediaQuery.of(context).size.width;
//                                      setState(() {});

                                      setState(() {
                                        if (_controller.isCompleted ||
                                            _controller.velocity > 0)
                                          _controller.reverse();
                                        else
                                          _controller.forward();
                                      });
                                    })),
                    ],
                  ),
                ]))),
        animation: _frontScale,
        builder: (BuildContext context, Widget child) {
          final Matrix4 transform = new Matrix4.identity()
            ..scale(1.0, _frontScale.value, 1.0);
          return new Transform(
            transform: transform,
            alignment: FractionalOffset.center,
            child: child,
          );
        },
      ),
      AnimatedBuilder(
        child: AnimatedOpacity(
            opacity: widget.visible ? 1.0 : 0.0,
            duration: Duration(milliseconds: 500),
            child: Container(
                margin:
                    const EdgeInsets.only(left: 6.0, right: 10.0, bottom: 10.0),
                decoration: new BoxDecoration(
                  color: Color.fromARGB(205, 254, 246, 226),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.black26,
                        offset: new Offset(1.0, 1.0),
                        blurRadius: 0.3,
                        spreadRadius: 0.3)
                  ],
                  borderRadius:
                      new BorderRadius.all(const Radius.circular(5.0)),
                ),
                child: Column(children: [
                  Container(
                      padding: new EdgeInsets.only(
                          top: 5.0, bottom: 5.0, left: 10.0, right: 10.0),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(34, 39, 81, 0.8),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(5.0),
                            topRight: const Radius.circular(5.0)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image(
                            image: new AssetImage("assets/bible.png"),
                            width: 18.0,
                            height: 18.0,
                            color: null,
                            fit: BoxFit.scaleDown,
                            alignment: Alignment.center,
                          ),
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(left: 5.0),
                                  child: Text(
                                    "Bible Verse For You ",
                                    style: TextStyle(
                                      color: Color.fromRGBO(192, 192, 192, 0.8),
                                      fontWeight: FontWeight.w600,
                                      fontSize: 13.0,
                                    ),
                                  ))),

//
//                              ///todo: write a stream for formatted date & time.
                        ],
                      )),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Container(
                          margin: EdgeInsets.only(
                              top: 15.0, bottom: 0.0, left: 10.0, right: 10.0),
                          padding: EdgeInsets.all(5.0),
                          key: MemKeys.messageItem(widget.message.id),
                          child: widget.message.type == "rosary"
                              ? Stack(children: <Widget>[
                                  FutureBuilder(
                                      future: widget.userBloc
                                          .authorUser(widget.message.authorID),
                                      builder: (context, snapshot) {
                                        if (snapshot.connectionState ==
                                            ConnectionState.done) {
                                          return CircleAvatar(
                                            radius: profilePhotoRadius,
                                            backgroundImage: snapshot.data !=
                                                    null
                                                ? CachedNetworkImageProvider(snapshot
                                                            .data
                                                            .profilePhotoUrl ??
                                                        "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a") ??
                                                    NetworkImage(snapshot.data
                                                            .profilePhotoUrl ??
                                                        "") ??
                                                    AssetImage(
                                                        "assets/placeholder-face.png")
                                                : AssetImage(
                                                    "assets/placeholder-face.png"),
                                          );
                                        } else
                                          return CircleAvatar(
                                            radius: profilePhotoRadius,
                                            backgroundImage: AssetImage(
                                                "assets/placeholder-face.png"),
                                          );
                                      }),
                                  Positioned(
                                    // draw a red marble
                                    top: 0.0,
                                    right: 0.0,
                                    child: widget.message.complete
                                        ? Container()
                                        : Icon(Icons.brightness_1,
                                            size: 8.0, color: Colors.redAccent),
                                  ),
                                  Positioned(
                                      top: rosaryCountTitleTopMargin,
                                      left: rosaryCountTitleLeftMargin,
                                      child: widget.message.type == "rosary"
                                          ? widget.message.rosaryCount == 1 ||
                                                  widget.message
                                                          .rosaryByOthers ==
                                                      1
                                              ? Text(
                                                  widget.message.rosaryCount ==
                                                          0
                                                      ? widget.message
                                                              .rosaryByOthers
                                                              .toString() +
                                                          " | ROSARY"
                                                      : widget.message
                                                              .rosaryCount
                                                              .toString() +
                                                          " | ROSARY",
                                                  style: TextStyle(
                                                      fontSize: 20.0,
                                                      color: Color.fromRGBO(
                                                          66, 65, 63, 1.0)),
                                                )
                                              : Text(
                                                  widget.message.rosaryCount ==
                                                          0
                                                      ? widget.message
                                                              .rosaryByOthers
                                                              .toString() +
                                                          " | ROSARIES"
                                                      : widget.message
                                                              .rosaryCount
                                                              .toString() +
                                                          " | ROSARIES",
                                                  style: TextStyle(
                                                      fontSize:
                                                          rosaryCountTitleFontSize,
                                                      color: Color.fromRGBO(
                                                          66, 65, 63, 1.0)),
                                                )
                                          : Container()),
                                  Positioned(
                                    top: offeredByTopMargin,
                                    left: offeredByLeftMargin,
                                    child: widget.message.type == "rosary"
                                        ? Text(
                                            widget.message.title,
                                            style: TextStyle(
                                                fontSize: offeredByFontSize,
                                                color: Color.fromRGBO(
                                                    66, 65, 63, 1.0)),
                                          )
                                        : Container(),
                                  ),
                                  Positioned(
                                      top: 30.0,
                                      left: 0.0,
                                      child: widget.message.type != "rosary"
                                          ? Theme.of(context).platform ==
                                                  TargetPlatform.iOS
                                              ? Text(
                                                  malTranslator.isMal(
                                                          widget.message.body)
                                                      ? malTranslator
                                                          .formatMalContent(
                                                              widget
                                                                  .message.body)
                                                      : widget.message.body,
                                                  key: MemKeys.messageItemBody(
                                                      widget.message.id),
                                                  maxLines: 100,
                                                  textAlign: TextAlign.justify,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: malTranslator.isMal(
                                                          widget.message.body)
                                                      ? Theme.of(context)
                                                          .textTheme
                                                          .subtitle2
                                                          .copyWith(
                                                              fontSize: 14.0,
                                                              fontFamily:
                                                                  'Karthika')
                                                      : Theme.of(context)
                                                          .textTheme
                                                          .subtitle2
                                                          .copyWith(
                                                              fontSize: 14.0,
                                                              fontFamily:
                                                                  'Bahnschrift'),
                                                )
                                              : Text(
                                                  widget.message.body,
                                                  key: MemKeys.messageItemBody(
                                                      widget.message.id),
                                                  maxLines: 100,
                                                  textAlign: TextAlign.justify,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subhead
                                                      .copyWith(
                                                        fontSize: 14.0,
                                                      ),
                                                )
                                          : Container()),
                                ])

                              // iOS Message content
                              : ListTile(
                                  contentPadding:
                                      EdgeInsets.only(top: 0.0, left: 5.0),
                                  subtitle: Column(
                                    children: <Widget>[
                                      Column(children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 10.0),
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      color: Colors.grey,
                                                      width: 0.5))),
                                          child: widget.bibleVerse.length >
                                                  _messageHeightLimit
                                              ? Theme.of(context).platform ==
                                                      TargetPlatform.iOS
                                                  ? Text(
                                                      malTranslator.isMal(
                                                              widget.bibleVerse)
                                                          ? malTranslator
                                                              .formatMalContent(widget
                                                                  .bibleVerse
                                                                  .substring(0,
                                                                      _messageLength))
                                                          : widget.bibleVerse
                                                              .substring(0,
                                                                  _messageLength),
                                                      maxLines: 100,
                                                      textAlign:
                                                          TextAlign.justify,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: malTranslator
                                                              .isMal(widget
                                                                  .bibleVerse)
                                                          ? Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                                  fontSize:
                                                                      13.0,
                                                                  fontFamily:
                                                                      'Karthika')
                                                          : Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                                  fontSize:
                                                                      13.0,
                                                                  fontFamily:
                                                                      'Bahnschrift'),
                                                    )
                                                  : Text(widget.bibleVerse
                                                      .substring(
                                                          0, _messageLength))
                                              : Theme.of(context).platform ==
                                                      TargetPlatform.iOS
                                                  ? Text(
                                                      malTranslator.isMal(
                                                              widget.bibleVerse)
                                                          ? malTranslator
                                                              .formatMalContent(
                                                                  widget
                                                                      .bibleVerse)
                                                          : widget.bibleVerse,
                                                      maxLines: 100,
                                                      textAlign:
                                                          TextAlign.justify,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: malTranslator
                                                              .isMal(widget
                                                                  .bibleVerse)
                                                          ? Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                                  fontSize:
                                                                      13.0,
                                                                  fontFamily:
                                                                      'Karthika')
                                                          : Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                                  fontSize:
                                                                      13.0,
                                                                  fontFamily:
                                                                      'Bahnschrift'),
                                                    )
                                                  : Text(widget.bibleVerse),
                                        ),
                                        GestureDetector(
                                          onTap: () => setState(() {
                                            _messageLength !=
                                                    _messageHeightLimit
                                                ? _messageLength =
                                                    _messageHeightLimit
                                                : _messageLength =
                                                    widget.bibleVerse.length;
                                          }),
                                          child: Container(
                                            child: widget.bibleVerse.length >
                                                    _messageHeightLimit
                                                ? Text(
                                                    _messageLength ==
                                                            _messageHeightLimit
                                                        ? "...Read More"
                                                        : "",
                                                    style: TextStyle(
                                                        color:
                                                            Colors.blueAccent),
                                                  )
                                                : Container(),
                                          ),
                                        ),
                                      ]),
                                    ],
                                  ),
                                ),
                        ))
                      ]),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                          child: Container(
                              padding: !_eligibleToDelete
                                  ? EdgeInsets.only(top: 10.0, bottom: 10.0)
                                  : EdgeInsets.all(0.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  widget.message.type == "rosary"
                                      ? IconTheme(
                                          data: IconThemeData(
                                              color: widget.headerColor),
                                          child: ImageIcon(
                                            AssetImage("assets/cross-icon.png"),
                                            size: 15.5,
                                          ))
                                      : Container(),
                                  widget.message.type == "rosary"
                                      ? LitanyElement(
                                          litanyElement: _litanyElement)
                                      // The green box needs to be the child of the AnimatedOpacity

                                      : Container(),
                                ],
                              ))),
                      widget.message.type == "rosary"
                          ? Container()
                          : IconButton(
                              color: Colors.grey,
                              icon: ImageIcon(
                                  AssetImage('assets/icons/share.png'),
                                  size: iconSize),
                              onPressed: widget.bibleVerse.isEmpty
                                  ? null
                                  : (() {
                                      final RenderBox box =
                                          context.findRenderObject();
                                      Share.share(widget.bibleVerse,
                                          sharePositionOrigin:
                                              box.localToGlobal(Offset.zero) &
                                                  box.size);
                                    })),
                      widget.message.type == "rosary"
                          ? Container()
                          : AnimatedContainer(
                              height: _bibleMsgHeight,
                              width: _bibleMsgWidth,
                              color: _bibleMsgColor,
                              duration: Duration(seconds: 10),
                            ),
                      widget.message.type == "rosary"
                          ? Container()
                          : IconButton(
                              color: Colors.grey,
                              icon: Icon(Icons.import_export),
                              onPressed: widget.message.body.isEmpty
                                  ? null
                                  : (() {
                                      setState(() {
                                        if (_controller.isCompleted ||
                                            _controller.velocity > 0)
                                          _controller.reverse();
                                        else
                                          _controller.forward();
                                      });
                                    })),
                    ],
                  ),
                ]))),
        animation: _backScale,
        builder: (BuildContext context, Widget child) {
          final Matrix4 transform = new Matrix4.identity()
            ..scale(1.0, _backScale.value, 1.0);
          return new Transform(
            transform: transform,
            alignment: FractionalOffset.center,
            child: child,
          );
        },
      ),
    ]);
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
        title: errorMsg.title,
        message: errorMsg.body,
        confirm: "Okay",
        confirmFn: () {
          Navigator.of(context, rootNavigator: true).pop();
        });
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void confirmDelete({MemError errorMsg}) {
    var dialog = AlertDialog(
      title: Text(errorMsg.title),
      content: Text(errorMsg.body),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            _removeMessage(context, widget.message);
            Navigator.of(context, rootNavigator: true).pop();
          },
          child: Text("Okay"),
        ),
        TextButton(
          onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
          child: Text('Cancel'),
        )
      ],
    );
//    ShowDialog(
//        title:
//        "Are you sure ? Please confirm to delete",
//        content:
//        "You are deleting a message. Please press Delete to confirm delete.",
//        okayText: 'OK',
//        cancelText: "Cancel",
//        okayCallBack: () {
//          print("delete confirmed");
//          _removeMessage(context, widget.message);
//          Navigator.of(context, rootNavigator: true).pop();
//        },
//        cancelCallBack: cancelDelete
//    );
//    DialogShower.buildDialog(
//        title: errorMsg.title,
//        message: errorMsg.body,
//        cancel: "cancel",
//        confirm: "Delete",
//        confirmFn: () {
//          _removeMessage(context, widget.message);
//          Navigator.of(context, rootNavigator: true).pop();
//        });

    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void _removeMessage(BuildContext context, MEMmessage message) {
    final messageBloc = MessageProvider.of(context);
    final userDetails = UserProvider.of(context).userDetails;

    var container = AppStateContainer.of(context);
    String _deleteMessage;
    String _undoButtonMessage;
    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
        .messageDeleted(message.title);
    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;

    bool _isAdmin = container.state.userDetails.role == "admin";
    bool _isRosaryAdmin = container.state.userDetails.isSuperAdmin &&
        container.state.userAcl.rosaryDelete;
    bool _eligibleToDelete =
        _isRosaryAdmin || message.authorID == container.state.userDetails.id;
    debugPrint("Eligible to delete : " + _eligibleToDelete.toString());

    if (_eligibleToDelete) {
      switch (message.type) {
        case "rosary":
          int deletedCount = 0;
          message.rosaryCount != 0
              ? deletedCount = message.rosaryCount
              : deletedCount = message.rosaryByOthers;
          messageBloc.removeRosaryMessage(message, container.state.userDetails);
          _deleteMessage = "Rosary Count reduced by " + deletedCount.toString();
          _undoButtonMessage = "";
          break;

        case "request":
          messageBloc.removeMessage(message);
          _deleteMessage = "Prayer Request Deleted";
          _undoButtonMessage = "";
          _removeChatRoomMessage(
              locale: userDetails.locale, messageID: message.id);
          break;
        case "testimony":
          debugPrint("Deleted Testimony :" + message.id + message.author);
          messageBloc.removeMessage(message);
          _deleteMessage = "Testimony Deleted";
          _undoButtonMessage = "";
          _removeChatRoomMessage(
              locale: userDetails.locale, messageID: message.id);
          break;
        case "guidance":
          debugPrint("Deleted Gudance :" + message.id + message.author);
          messageBloc.removeMessage(message);
          _deleteMessage = "Counselling Request Deleted";
          _undoButtonMessage = "";
          break;
        case "admin":
          deleteRegionalMessage(
              container.state.regions, messageBloc, message.id, userDetails);
          _removeChatRoomMessage(
              locale: userDetails.locale, messageID: message.id);
          break;
      }
    } else {
      _deleteMessage = "Sorry, You cannot delete this message";
      _undoButtonMessage = "";
    }

    if (message.type != "admin")
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          key: MemKeys.snackbar,
          duration: Duration(seconds: 3),
          backgroundColor: Theme.of(context).errorColor,
          content: Text(
            _deleteMessage,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.white),
          ),
          action: SnackBarAction(
            label: _undoButtonMessage,
            onPressed: () {
              messageBloc.messageRepository.addRegionalMessage(
                  message.toEntity(), container.state.userDetails.region);
              //msgItem.visible = true;
              //addMessage(message);
            },
          ),
        ),
      );
  }

  _removeChatRoomMessage({String locale, String messageID}) {
    DatabaseReference _messagesReference =
        FirebaseDatabase.instance.reference();
    debugPrint("Remove Chat Message ID ==||==" + messageID);

    _messagesReference
        .child("chats")
        .child(locale ?? "English")
        .child(messageID)
        .remove();
  }
}
