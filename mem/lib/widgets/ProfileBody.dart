import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProfileBody extends StatelessWidget {
  ProfileBody(this.member);
  final MemUser member;

  Widget _buildLocationInfo(TextTheme textTheme) {
    return new Row(
      children: <Widget>[
        new Icon(
          Icons.place,
          color: Colors.white,
          size: 16.0,
        ),
        new Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: member.region != null
              ? Text(
                  member.region,
                  style: textTheme.subhead.copyWith(color: Colors.white),
                )
              : Text(""),
        ),
        Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('mem')
                      .doc('rosary')
                      .collection('users')
                      .doc(member.id)
                      .collection('badges')
                      .where('action', isEqualTo: 'ADD ROSARY')
                      .snapshots(),
                  builder: (BuildContext context, snapshot) {
                    if (snapshot.hasError) return Container();
                    if (!snapshot.hasData || snapshot.data == null) {
                      return Container();
                    } else {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                          return SizedBox(
                              width: 20.0,
                              height: 20.0,
                              child: CircularProgressIndicator(
                                  strokeWidth: 1.0,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.black26)));

                        default:
                          try {
                            return Stack(children: <Widget>[
                              Image.asset(
                                "assets/crown_" +
                                    snapshot.data.documents.last["level"]
                                        .toString() +
                                    ".png",
                                height: 40.0,
                              ),
                              Positioned(
                                  top: 15.0,
                                  left: 14.0,
                                  child: Icon(Icons.brightness_1,
                                      size: 14.0, color: Colors.redAccent)),
                              Positioned(
                                top: 16.0,
                                left: 18.0,
                                child: Text(
                                  snapshot.data.documents.last["level"]
                                      .toString(),
                                  style: TextStyle(
                                      fontSize: 10.0, color: Colors.white),
                                ),
                              )
                            ]);
                          } catch (e) {
                            return Container();
                          }
                      }
                    }
                  },
                ) ??
                Container()),
        Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('mem')
                      .doc('rosary')
                      .collection('users')
                      .doc(member.id)
                      .collection('badges')
                      .where('action', isEqualTo: 'COLLECT ROSARY')
                      .snapshots(),
                  builder: (BuildContext context, snapshot) {
                    if (snapshot.hasError) return Container();
                    if (!snapshot.hasData || snapshot.data == null) {
                      return Container();
                    } else {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                          return Center(
                              child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.transparent)));

                        default:
                          try {
                            return Stack(children: <Widget>[
                              Image.asset(
                                "assets/star_" +
                                    snapshot.data.documents.last["level"]
                                        .toString() +
                                    ".png",
                                height: 32.0,
                              ),
                              Positioned(
                                  top: 11.0,
                                  left: 10.0,
                                  child: Icon(Icons.brightness_1,
                                      size: 14.0, color: Colors.redAccent)),
                              Positioned(
                                top: 12.0,
                                left: 14.0,
                                child: Text(
                                  snapshot.data.documents.last["level"]
                                      .toString(),
                                  style: TextStyle(
                                      fontSize: 10.0, color: Colors.white),
                                ),
                              )
                            ]);
                          } catch (e) {
                            return Container();
                          }
                      }
                    }
                  },
                ) ??
                Container()),
      ],
    );
  }

  Widget _createCircleBadgeFA(Icon icon, Color color) {
    return new Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: new CircleAvatar(
        backgroundColor: color,
        child: icon,
        radius: 16.0,
      ),
    );
  }

  Widget _createCircleBadge(IconData icondata, Color color) {
    return new Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: new CircleAvatar(
        backgroundColor: color,
        child: Icon(icondata, color: Colors.white),
        radius: 16.0,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    UserBloc userBloc = UserProvider.of(context);

    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(top: 4.0, left: 10.0),
            child: Text(
              member.name,
              style: TextStyle(color: Colors.white, fontSize: 25.0),
//              style: textTheme.headline.copyWith(color: Colors.white),
            )),
        Padding(
            padding: const EdgeInsets.only(top: 0.0, left: 10.0),
            child: member.userAddedOn != null
                ? Text(
                    "Display Name:  " +
                        member.displayName +
                        " ,\nA member since " +
                        DateFormat('dd-MM-yyyy')
                            .format(member.userAddedOn ?? "") +
                        ".",
                    style: textTheme.body1
                        .copyWith(color: Colors.brown, fontSize: 14.0),
                  )
                : Container()),
        Padding(
          padding: const EdgeInsets.only(top: 4.0, left: 10.0),
          child: _buildLocationInfo(textTheme),
        ),
        new Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: new Row(
            children: <Widget>[
              member.gender != null && member.gender != ""
                  ? _createCircleBadge(
                      Icons.person, Color.fromRGBO(11, 129, 140, 1.0))
                  : Container(),
              member.gender != null
                  ? Text(
                      member.gender == "M"
                          ? "Male"
                          : member.gender == "F"
                              ? "Female"
                              : " ",
                      style: TextStyle(color: Colors.white),
                    )
                  : Container(),

              member.country != null
                  ? _createCircleBadge(
                      Icons.flag,
                      Color.fromRGBO(11, 129, 140, 1.0),
                    )
                  : Container(),
              member.country != null
                  ? Text(
                      " " + member.country,
                      style: TextStyle(color: Colors.white),
                    )
                  : Container(),
              userBloc.userDetails.role == "admin" && member.phoneNumber != null
                  ? _createCircleBadge(
                      Icons.phone,
                      Color.fromRGBO(11, 129, 140, 1.0),
                    )
                  : Container(),
              Expanded(
                  child: userBloc.userDetails.role == "admin" &&
                          member.phoneNumber != null
                      ? Text(
                          member.phoneNumber ?? "",
                          style: TextStyle(color: Colors.white),
                        )
                      : Text("")),

//              _createCircleBadge(Icons.cloud, Colors.white12),
//              _createCircleBadge(Icons.shop, Colors.white12),
            ],
          ),
        ),
        new Padding(
            padding: const EdgeInsets.only(top: 40.0, bottom: 200.0),
            child: Container()),
      ],
    );
  }
}
