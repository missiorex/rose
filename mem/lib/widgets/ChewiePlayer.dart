import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:mem_models/mem_models.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:http/http.dart' as http;

class ChewiePlayer extends StatefulWidget {
  // This will contain the URL/asset path which we want to play
  final VideoPlayerController videoPlayerController;
  final VideoTestimony video;

  ChewiePlayer({
    @required this.videoPlayerController,
    @required this.video,
    Key key,
  }) : super(key: key);

  @override
  _ChewiePlayerState createState() => _ChewiePlayerState();
}

class _ChewiePlayerState extends State<ChewiePlayer> {
  ChewieController _chewieController;
  String _videoThumbNail;
  String _videoId;

  @override
  void initState() {
    super.initState();
    // Wrapper on top of the videoPlayerController

    _videoId = YoutubePlayer.convertUrlToId(widget.video.youtubeUrl);

    _videoThumbNail = getThumbnail(videoId: _videoId);

    _chewieController = ChewieController(
      videoPlayerController: widget.videoPlayerController,
      aspectRatio: 1.7789,
      autoInitialize: true,
      autoPlay: false,
      startAt: Duration.zero,
      looping: false,
      showControls: true,
      fullScreenByDefault: false,
      allowFullScreen: false,
      allowMuting: true,
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );
  }

  String getThumbnail(
      {@required String videoId, String quality = ThumbnailQuality.standard}) {
    String _thumbnailUrl = 'https://i3.ytimg.com/vi/$videoId/';
    switch (quality) {
      case ThumbnailQuality.defaultQuality:
        _thumbnailUrl += 'default.jpg';
        break;
      case ThumbnailQuality.high:
        _thumbnailUrl += 'hqdefault.jpg';
        break;
      case ThumbnailQuality.medium:
        _thumbnailUrl += 'mqdefault.jpg';
        break;
      case ThumbnailQuality.standard:
        _thumbnailUrl += 'sddefault.jpg';
        break;
      case ThumbnailQuality.max:
        _thumbnailUrl += 'maxresdefault.jpg';
        break;
    }
    return _thumbnailUrl;
  }

  Future<String> _fetchVideoURL(String yt) async {
    final response = await http.get(Uri.parse(yt));
    Iterable parseAll = _allStringMatches(
        response.body, RegExp("\"url_encoded_fmt_stream_map\":\"([^\"]*)\""));
    final Iterable<String> parse =
        _allStringMatches(parseAll.toList()[0], RegExp("url=(.*)"));
    final List<String> urls = parse.toList()[0].split('url=');
    parseAll = _allStringMatches(urls[1], RegExp("([^&,]*)[&,]"));
    String finalUrl = Uri.decodeFull(parseAll.toList()[0]);
    if (finalUrl.indexOf('\\u00') > -1)
      finalUrl = finalUrl.substring(0, finalUrl.indexOf('\\u00'));
    return finalUrl;
  }

  Iterable<String> _allStringMatches(String text, RegExp regExp) =>
      regExp.allMatches(text).map((m) => m.group(0));

  @override
  Widget build(BuildContext context) {
    if (!widget.videoPlayerController.value.isPlaying) {
      return Center(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: AspectRatio(
            aspectRatio: 1.7789,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.width / 1.7789,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: widget.video.videoImageUrl != null
                                ? CachedNetworkImageProvider(
                                      _videoThumbNail ??
                                          YoutubePlayer.getThumbnail(
                                            videoId: _videoId,
                                          ),
                                    ) ??
                                    NetworkImage(_videoThumbNail ??
                                        YoutubePlayer.getThumbnail(
                                          videoId: _videoId,
                                        ) ??
                                        "") ??
                                    NetworkImage(
                                        widget.video.videoImageUrl ?? "")
                                : AssetImage("assets/placeholder-face.png")))),

//                Image.network(
//                  _videoThumbURL(video.youtubeUrl),
//                  fit: BoxFit.cover,
//                ),
                Center(
                  child: ClipOval(
                    child: Container(
                      color: Colors.white,
                      child: IconButton(
                        iconSize: 50.0,
                        color: Colors.black,
                        icon: Icon(
                          Icons.play_arrow,
                        ),
                        onPressed: () {
                          widget.videoPlayerController.play();
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return _chewieController != null
          ? FutureBuilder(
              future: _fetchVideoURL(widget.video.youtubeUrl),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Chewie(
                    key: widget.key,
                    controller: _chewieController,
                  );
                } else {
                  return AspectRatio(
                    aspectRatio: 1.7789,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
              })
          : AspectRatio(
              aspectRatio: 1.7789,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
    }
  }

  @override
  void dispose() {
    super.dispose();
    // IMPORTANT to dispose of all the used resources
    _chewieController.dispose();
  }
}
