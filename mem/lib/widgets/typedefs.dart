import 'package:mem_blocs/mem_blocs.dart';
import 'package:mem_models/mem_models.dart';

typedef showMessage(String msg);

typedef confirmFn(MemUser member, UserBloc userBloc);

typedef onPressedAdmin(MemUser member, UserBloc userBloc);

typedef cancelFn();
