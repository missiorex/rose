import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import "package:intl/intl.dart";
import 'package:rive/rive.dart';
import 'package:flutter/services.dart';

class RosaryCountButton extends StatefulWidget {
  /// The function to call when the icon button is pressed.
  final VoidCallback onPressed;

  /// Number of items in the basket. When this is `0`, no badge will be shown.
  final int rosaryCount;

  final Color badgeColor;

  final Color badgeTextColor;

  RosaryCountButton({
    Key key,
    @required this.rosaryCount,
    this.onPressed,
    this.badgeColor: Colors.white,
    this.badgeTextColor: Colors.black,
  }) :
//        assert(unreadCount >= 0),
//        assert(badgeColor != null),
//        assert(badgeTextColor != null),
        super(key: key);

  @override
  RosaryCountButtonState createState() {
    return RosaryCountButtonState();
  }
}

class RosaryCountButtonState extends State<RosaryCountButton>
    with TickerProviderStateMixin {
  AnimationController _animationController, _heartBeatAnimationController;
  Animation<double> _animation;

  //Rive Star Animation
//  Artboard _riveArtboard;
//  RiveAnimationController _riveController;

//  void _playRiveAnimation() {
//    if (_riveController == null) {
//      return;
//    }
//    setState(() => _riveController.isActive = !_riveController.isActive);
//  }

  final Tween<Offset> _badgePositionTween = Tween(
    begin: const Offset(0.0, 0.1),
    end: const Offset(0.0, 0.0),
  );

  @override
  Widget build(BuildContext context) {
    // if (widget.itemCount == 0) {
    //   return IconButton(
    //     icon: Icon(Icons.shopping_cart),
    //     onPressed: widget.onPressed,
    //   );
    // }

    var extraSize = _heartBeatAnimationController.value * 2;
    var heartSize = MediaQuery.of(context).size.width * 0.205;
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    return IconButton(
        tooltip: NumberFormat("##,##,##,##,###").format(widget.rosaryCount) +
            " Rosaries since 2015",
        icon: Stack(
          overflow: Overflow.visible,
          children: [
//            Icon(Icons.add, size: 12.5),
            Positioned(
              top: isTablet ? heartSize * 0.02 : heartSize * 0.15,
              right: -10.0,
              bottom: isTablet ? -20.0 : -5.0,
              child: SlideTransition(
                position: _badgePositionTween.animate(_animation),
                child: Material(
                    type: MaterialType.transparency,
                    elevation: 46.0,
                    color: Colors.white,
                    child: Container(
                      padding:
                          EdgeInsets.only(top: heartSize * 0.01, left: 0.0),
                      height: heartSize + extraSize,
                      width: heartSize + extraSize,
//                      decoration: BoxDecoration(
//                          image: DecorationImage(
//                              fit: BoxFit.cover,
//                              image: AssetImage("assets/rose_heart.png")
//                          )),
                      child: widget.rosaryCount > 0
                          ? Text(
                              NumberFormat("##,##,##,##,###")
                                  .format(widget.rosaryCount),
                              textAlign: TextAlign.center,

//                        widget.unreadCount.toString(),
                              style: TextStyle(
                                fontSize: heartSize * 0.15,
                                color: Colors.white,
                                fontWeight: FontWeight.w900,
                              ),
                            )
                          : Container(),
                    )),
              ),
            ),
//            Positioned(
////              top: heartSize * 0.01,
//              right: -50.0,
//              bottom: isTablet ? -20.0 : -5.0,
//              child: _riveArtboard == null
//                  ? Container(
//                      height: 100.0,
//                    )
//                  : Container(
//                      decoration: BoxDecoration(
//                          border: Border.all(color: Colors.white, width: 1.0)),
//                      width: heartSize,
//                      height: heartSize * (3 / 5),
//                      child: Rive(artboard: _riveArtboard)),
//            )
          ],
        ),
        onPressed: widget.onPressed);
  }

  @override
  void didUpdateWidget(RosaryCountButton oldWidget) {
    if (widget.rosaryCount != oldWidget.rosaryCount &&
        oldWidget.rosaryCount > 0) {
      _animationController.reset();
      _animationController.forward();
      //widget.onPressed();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _animationController.dispose();
    _heartBeatAnimationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );
    _animation =
        CurvedAnimation(parent: _animationController, curve: Curves.elasticOut);
    _animationController.forward();

    _heartBeatAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 800));
    _heartBeatAnimationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _heartBeatAnimationController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _heartBeatAnimationController.forward();
      }
    });
    _heartBeatAnimationController.addListener(() {
      setState(() {});
    });
    _heartBeatAnimationController.forward(from: 0.0);
  }
}
