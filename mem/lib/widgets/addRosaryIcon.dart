import 'package:flutter/material.dart';
import 'package:rive/rive.dart';
import 'package:flutter/services.dart';

class AddRosaryIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Artboard>(
        future: _loadRiveIcon(),
        builder: (BuildContext context, AsyncSnapshot<Artboard> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.active:
            case ConnectionState.none:
//              return Ink.image(
//                image: AssetImage('assets/heart-rosary-icon.png'),
//                fit: BoxFit.fill,
//                child: InkWell(
//                  onTap: null,
//                ),
//              );
              return Text("Connection State None");
            case ConnectionState.done:
              if (snapshot.hasError)
//                return Ink.image(
//                  image: AssetImage('assets/heart-rosary-icon.png'),
//                  fit: BoxFit.fill,
//                  child: InkWell(
//                    onTap: null,
//                  ),
//                );
                return Text("Error");
              else
                return Container(
                  child: Center(
                    child: snapshot.data == null
                        ? const SizedBox()
                        : Rive(artboard: snapshot.data),
                  ),
                );
              break;
            default:
              return Container(
                child: Center(
                  child: snapshot.data == null
                      ? const SizedBox()
                      : Rive(artboard: snapshot.data),
                ),
              );
              break;
          }
        });
  }

  Future<Artboard> _loadRiveIcon() async {
    Artboard _riveArtboard;

    rootBundle.load('assets/rive/addRosaryIcon.riv').then(
      (data) async {
        // Load the RiveFile from the binary data.
        final file = RiveFile.import(data);
        // The artboard is the root of the animation and gets drawn in the
        // Rive widget.
        final artboard = file.mainArtboard;
        // Add a controller to play back a known animation on the main/default
        // artboard.We store a reference to it so we can toggle playback.
        artboard.addController(SimpleAnimation('idle'));
        _riveArtboard = artboard;
        debugPrint("Rive loaded....." + _riveArtboard.artboard.name);
      },
    );

    return Future.value(_riveArtboard);
  }
}
