import 'package:flutter/material.dart';

class MemLabel extends StatelessWidget {
  final Color labelColor;
  final double fontSize;

  MemLabel({this.labelColor, this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text("Marian Eucharistic Ministry",
          style: TextStyle(color: labelColor, fontSize: fontSize)),
    );
  }
}
