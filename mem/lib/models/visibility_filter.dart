enum VisibilityFilter { all, active, completed }
final List<String> categories = [
  'All',
  'Christmas',
  'Heroes',
  'Music',
  'Dance',
  'Catechism',
  'Preschoolers',
  'Stories',
];
