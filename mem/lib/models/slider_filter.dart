enum SliderVisibilityFilter {
  all,
  Heroes,
  Jesus,
  MotherMary,
  Angels,
  ArchAngels,
  Saints,
  DailySaints,
  SaintOfTheDay
}
final List<String> sliderCategories = [
  'all',
  'Heroes',
  'Jesus',
  'Mother Mary',
  'Angels',
  'Arch Angels',
  'Saints',
  'Daily Saints',
  'Saint of the Day'
];
