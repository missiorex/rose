import 'package:memapp/models/models.dart';

final List<String> labels = [
  'Bible Messages',
  'Holy Jokes',
  'Play A Game',
  'Fun Facts',
  'Tell a Story',
  'Prayer Power',
];
