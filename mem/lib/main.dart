import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:mem_repository_firebase/mem_repository_firebase.dart';
import 'package:memapp/app.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memapp/app_state_container.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memapp/widgets/RestartWidget.dart';
import 'package:firebase_core/firebase_core.dart';
import 'dart:io';
import 'package:flame/flame.dart';

import 'package:bloc/bloc.dart';
import 'package:memapp/blocs/blocs.dart';
import 'configure_nonweb.dart' if (dart.library.html) 'configure_web.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:campaign_repository/campaign_repository.dart';
import 'package:memapp/widgets/campaign/widgets.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/foundation.dart';
import 'package:utility/utility.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  print('Handling a background message ${message.messageId}');
}

/// Initialize the [FlutterLocalNotificationsPlugin] package.
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  //debugPaintSizeEnabled = true;
  Future<FirebaseApp> _initialization =
      await Future.value(Firebase.initializeApp());
  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await _setupImages();

  // Campaign
  Bloc.observer = SimpleBlocObserver();
  //

  //Campaign

  configureApp();

  final fileStorage = FileStorage(
    "mem_app",
    getApplicationDocumentsDirectory,
  );

  FlutterError.onError = (FlutterErrorDetails details) {
    print("Error From INSIDE FRAME_WORK");
    print("----------------------");
    print("Error :  ${details.exception}");
    print("StackTrace :  ${details.stack}");
  };

  //Production Config
//  final FirebaseApp app = await FirebaseApp.configure(
//    name: 'memapp',
//    options: new FirebaseOptions(
//      googleAppID: Platform.isIOS
//          ? '1:796739244351:ios:c7aadc7c50c1ed29'
//          : '1:796739244351:android:dcf17a18baaa4e27',
//      gcmSenderID: '796739244351',
//      apiKey: 'AIzaSyCEg9g35fUZHUnRYl4juZztxC2WYoIZe3Q',
//      projectID: 'memapp-4c4c5',
//    ),
//  );
//
//  final FirebaseStorage storage = new FirebaseStorage(
//      app: app, storageBucket: 'gs://memapp-4c4c5.appspot.com');

  //For Production, Comment this out

//  final FirebaseApp app = await FirebaseApp.configure(
//    name: 'memapp-dev',
//    options: new FirebaseOptions(
//      googleAppID: Platform.isIOS
//          ? '1:862130560434:ios:dcf17a18baaa4e27'
//          : '1:862130560434:android:dcf17a18baaa4e27',
////      gcmSenderID: '796739244351',
//      //apiKey: 'AIzaSyARb0epnkHpt95MmWfxatF7Ks3BHPFB0ok',
//      apiKey: 'AIzaSyBlF60eeU8Pb7JwN-PXx8Cm-sGWpx1cesU',
//      projectID: 'memapp-dev',
//    ),
//  );
  //final FirebaseStorage storage = FirebaseStorage.instance;
//      FirebaseStorage(app: app, storageBucket: 'gs://memapp-dev.appspot.com');

//  await Firestore.instance.settings(
//    persistenceEnabled: true,
////      timestampsInSnapshotsEnabled: true,
////      cacheSizeBytes: 41943040
//  );

  runApp(FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return Container(
              child: Center(
                  child: Text(
            "Sorry, unable to open app. Please try later.",
            textDirection: TextDirection.ltr,
          )));
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          final RosaryRepositoryFireStore rosaryRepository =
              RosaryRepositoryFireStore(fileStorage: fileStorage);
          final MessageRepositoryFireStore messageRepository =
              MessageRepositoryFireStore(fileStorage: fileStorage);
          final UserRepositoryFirestore userRepository =
              UserRepositoryFirestore();
          final RewardsRepositoryFireStore rewardsRepository =
              RewardsRepositoryFireStore(fileStorage: fileStorage);
          final EmailAuthRepository emailAuthRepository =
              EmailAuthRepository(FirebaseAuth.instance);

          final rosaryBloc = RosaryBloc(
              messageRepository, rosaryRepository, rewardsRepository);
          final messageBloc = MessageBloc(messageRepository, rosaryRepository,
              userRepository, rewardsRepository);
          final userBloc =
              UserBloc(userRepository, emailAuthRepository, messageRepository);
          final notificationBloc =
              NotificationBloc(messageRepository, userRepository);

          if (!kIsWeb) {
            flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

            /// Create an Android Notification Channel.
            ///
            /// We use this channel in the `AndroidManifest.xml` file to override the
            /// default FCM channel to enable heads up notifications.
            /// memCHannel from constans in utility
            ///
            flutterLocalNotificationsPlugin
                .resolvePlatformSpecificImplementation<
                    AndroidFlutterLocalNotificationsPlugin>()
                ?.createNotificationChannel(memChannel);

            /// Update the iOS foreground notification presentation options to allow
            /// heads up notifications.
            FirebaseMessaging.instance
                .setForegroundNotificationPresentationOptions(
              alert: true,
              badge: true,
              sound: true,
            );
          }

          return BlocProvider<CampaignsBloc>(
            create: (context) {
              return CampaignsBloc(
                  campaignsRepository: FirebaseCampaignRepository())
                ..add(CampaignsLoaded());
            },
            child: RestartWidget(
                child: AppStateContainer(
                    child: MemApp(rosaryBloc, messageBloc, userBloc,
                        notificationBloc, FirebaseStorage.instance))),
          );
        }
        return LoadingIndicator();
      }));
}

Future<void> _setupImages() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Flame.images.loadAll(<String>[
    'star.png',
    'angel.png',
    'bg_sky.png',
    'angel_arc.png',
    'logo.png'
  ]);
}
