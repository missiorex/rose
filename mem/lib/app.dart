// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/localization.dart';
import 'package:memapp/screens/splash_screen.dart';
import 'package:memapp/screens/home_screen.dart';
import 'package:mem_models/mem_models.dart';
import "package:flutter_local_notifications/flutter_local_notifications.dart";
import 'package:memapp/screens/signup.dart';
import 'package:memapp/screens/add_edit_message_screen.dart';
import 'package:memapp/screens/add_edit_rosary_screen.dart';
import 'package:memapp/screens/add_edit_rosary_list.dart';
import 'package:memapp/screens/member_list.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:core';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memapp/app_state_container.dart';
import 'package:memapp/widgets/logo_widget.dart';

import 'package:memapp/blocs/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:campaign_repository/campaign_repository.dart';
import 'package:memapp/widgets/campaign/widgets.dart';
import 'package:memapp/screens/campaign/screens.dart';
import 'package:memapp/screens/screens.dart';

class MemApp extends StatefulWidget {
  final RosaryBloc rosaryBloc;
  final MessageBloc messageBloc;
  final UserBloc userBloc;
  final NotificationBloc notificationBloc;
  final FirebaseStorage storage;

  AppState appState = AppState.loading();

  MemApp(this.rosaryBloc, this.messageBloc, this.userBloc,
      this.notificationBloc, this.storage);

  @override
  State<StatefulWidget> createState() {
    return MemAppState();
  }
}

class MemAppState extends State<MemApp> with TickerProviderStateMixin {
  MemUser _userDetails;
  AppState appState = AppState.loading();
  FirebaseAuth auth = FirebaseAuth.instance;
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  AnimationController _logoController;
  Animation<double> _logoAnimation;
  Animation<Color> _colorAnimation;
  //String _gospelMsg = "";

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

  void initState() {
    super.initState();

    super.initState();
    _logoController =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    _logoAnimation =
        CurvedAnimation(parent: _logoController, curve: Curves.easeIn);
    _logoController.forward();

    //Subscribing to FCM

    // The total Rosary count Stream
    widget.rosaryBloc.rosaryRepository.fetchRosary().then((rosary) {
      debugPrint("Total Rosary Count: " + rosary.toString());

      widget.rosaryBloc.rosaryRepository
          .latestRosaryCount()
          .listen((rosaryStream) {
        List<CountSnapShot> rosaryCount =
            rosaryStream.map(CountSnapShot.fromEntity).toList();

        setState(() {
          widget.rosaryBloc.TotalRosaryAddition.add(rosaryCount[0].totalCount);

          appState = AppState(
              isLoading: false,
              user: widget.userBloc.firebaseUser,
              userDetails: _userDetails);
        });
      });
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _logoController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _logoController.forward();

    if (appState.isLoading) {
      return AnimatedLogo(animation: _logoAnimation);
    } else {
      return MessageProvider(
          messageBloc: widget.messageBloc,
          // ... and the rosary component via InheritedWidget like so.
          // But BLoC works with any other mechanism, including passing
          // down the widget tree.
          child: RosaryProvider(
              rosaryBloc: widget.rosaryBloc,
              child: UserProvider(
                  userBloc: widget.userBloc,
                  child: NotificationProvider(
                      notificationBloc: widget.notificationBloc,
                      child: MaterialApp(
                          title: MemAppLocalizations().appTitle,
                          theme: MemeTheme.theme,
                          navigatorObservers: [routeObserver],
                          debugShowCheckedModeBanner: false,
                          home: SplashScreen(
                            key: MemKeys.splashScreen,
                            appState: appState,
                            storage: widget.storage,
                            userBloc: widget.userBloc,
                            routeObserver: routeObserver,
                            messageBloc: widget.messageBloc,
                            rosaryBloc: widget.rosaryBloc,
                          ),
                          localizationsDelegates: [
                            MemLocalizationsDelegate(),
                            MemAppLocalizationsDelegate(),
                          ],
                          supportedLocales: [
                            const Locale('en', 'US'), // English
                          ],
                          routes: {
                            MemRoutes.auth: (context) {
                              return AuthScreen(
                                key: MemKeys.authScreen,
                                storage: widget.storage,
                                appState: widget.appState,
                              );
                            },
                            MemRoutes.signup: (context) {
                              return AuthScreen(
                                key: MemKeys.signupScreen,
                                storage: widget.storage,
                                appState: widget.appState,
                              );
                            },
                            MemRoutes.memberList: (context) {
                              return MembersListScreen(
                                key: MemKeys.memberList,
                                storage: widget.storage,
                              );
                            },
                            MemRoutes.addMessage: (context) {
                              return AddEditMessageScreen(
                                key: MemKeys.addMessageScreen,
                                msgType: "admin",
                              );
                            },
                            MemRoutes.addRosary: (context) {
                              return AddEditRosaryScreen(
                                key: MemKeys.addRosaryScreen,
                              );
                            },
                            MemRoutes.addRequest: (context) {
                              return AddEditMessageScreen(
                                key: MemKeys.addRequestScreen,
                                msgType: "request",
                              );
                            },
                            MemRoutes.addTestimony: (context) {
                              return AddEditMessageScreen(
                                key: MemKeys.addTestimonyScreen,
                                msgType: "thanks",
                              );
                            },
                            MemRoutes.rosaryList: (context) {
                              return RosaryListScreen(
                                key: MemKeys.rosaryListScreen,
                                routeObserver: routeObserver,
                              );
                            },
                            MemRoutes.notification: (context) {
                              return MessageView();
                            },
                            MemRoutes.campaignHome: (context) {
                              return MultiBlocProvider(
                                providers: [
                                  BlocProvider<TabBloc>(
                                    create: (context) => TabBloc(),
                                  ),
                                  BlocProvider<FilteredCampaignsBloc>(
                                    create: (context) => FilteredCampaignsBloc(
                                      campaignsBloc:
                                          BlocProvider.of<CampaignsBloc>(
                                              context),
                                    ),
                                  ),
                                  BlocProvider<StatsBloc>(
                                    create: (context) => StatsBloc(
                                      campaignsBloc:
                                          BlocProvider.of<CampaignsBloc>(
                                              context),
                                    ),
                                  ),
                                ],
                                child: LifeCycleManager(
                                    child: CampaignHomeScreen()),
                              );
                            },
                            MemRoutes.addCampaign: (context) {
                              return AddEditScreen(
                                key: MemKeys.addCampaignScreen,
                                onSave: (title, description) {
                                  BlocProvider.of<CampaignsBloc>(context).add(
                                    CampaignAdded(Campaign(title,
                                        description: description)),
                                  );
                                },
                                isEditing: false,
                              );
                            },
                          },
                          onGenerateRoute: (RouteSettings settings) {
                            switch (settings.name) {
                              case '/home':
                                return new MemRoute(
                                  builder: (_) => HomeScreen(
                                    key: MemKeys.homeScreen,
                                    appState: appState,
                                    storage: widget.storage,
                                    userBloc: widget.userBloc,
                                    routeObserver: routeObserver,
                                    messageBloc: widget.messageBloc,
                                    rosaryBloc: widget.rosaryBloc,
                                    gospelMsg: "",
                                    container: AppStateContainer.of(context),
                                  ),
                                  settings: settings,
                                );
                              case '/splash':
                                return new MemRoute(
                                  builder: (_) => SplashScreen(
                                    key: MemKeys.splashScreen,
                                    appState: appState,
                                    storage: widget.storage,
                                    userBloc: widget.userBloc,
                                    routeObserver: routeObserver,
                                    messageBloc: widget.messageBloc,
                                    rosaryBloc: widget.rosaryBloc,
                                  ),
                                  settings: settings,
                                );
                            }

                            // If you push the Campaign Details route
                            if (settings.name ==
                                CampaignDetailsScreen.routeName) {
                              final CampaignScreenArguments args =
                                  settings.arguments as CampaignScreenArguments;

                              // Then, extract the required data from
                              // the arguments and pass the data to the
                              // correct screen.
                              return MaterialPageRoute(
                                builder: (context) {
                                  return CampaignDetailsScreen(
                                    id: args.id,
                                  );
                                },
                              );
                            }
                            // The code only supports
                            // PassArgumentsScreen.routeName right now.
                            // Other values need to be implemented if we
                            // add them. The assertion here will help remind
                            // us of that higher up in the call stack, since
                            // this assertion would otherwise fire somewhere
                            // in the framework.
                            assert(false, 'Need to implement ${settings.name}');
                            return null;
                          })))));
    }
  }
}

class MemRoute<T> extends MaterialPageRoute<T> {
  MemRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Duration get transitionDuration => const Duration(milliseconds: 300);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    //if (settings.isInitialRoute) return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    //return new FadeTransition(opacity: animation, child: child);
    return child;
  }
}

// #enddocregion LogoWidget

// #docregion GrowTransition
class GrowTransition extends StatelessWidget {
  final Widget child;
  final Animation<double> animation;

  GrowTransition({@required this.child, @required this.animation});

  Widget build(BuildContext context) => Center(
        child: AnimatedBuilder(
            animation: animation,
            builder: (context, child) => Container(
                  height: animation.value,
                  width: animation.value,
                  child: child,
                ),
            child: child),
      );
}

class ColorTransition extends StatelessWidget {
  final Widget child;
  final Animation<Color> colorAnimation;
  ColorTransition({@required this.child, @required this.colorAnimation});

  Widget build(BuildContext context) => AnimatedBuilder(
        animation: colorAnimation,
        builder: (context, child) => Container(
          decoration: BoxDecoration(
            color: colorAnimation.value,
          ),
          child: child,
        ),
      );
}
