import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/palette.dart';

class StarComponent extends PositionComponent {
  Paint paint = BasicPalette.white.paint();
  final Image image;
  final Offset offset;
  final Vector2 size;

  StarComponent({this.image, this.offset, this.size}) : super();

  @override
  void render(Canvas c) {
    super.render(c);

    c.drawImage(image, offset, paint);
  }

  @override
  void update(t) {
    super.update(t);
  }
}
