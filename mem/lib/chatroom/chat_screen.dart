import 'dart:async';
import 'dart:math';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'type_meme.dart';
import 'platform_adaptive.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:base_helpers/basehelpers.dart';
import 'package:memapp/app_state_container.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;
import 'package:mem_models/mem_models.dart';
import 'package:base_helpers/uuid.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:emoji_picker/emoji_picker.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:io';

class ChatScreen extends StatefulWidget {
  final UserBloc userBloc;
  final String chatRoomID;
  final String bgImageUrl;
  final bool connectionStatus;
  final bool publicChat;
  final bool fcmNotificationEnable;
  final RouteObserver<PageRoute> routeObserver;
  List<ChatMessage> messages;
  var lastVisibleChatKey;
  int lastVisibleChatValue;
  ChatScreen(
      {this.lastVisibleChatKey,
      this.lastVisibleChatValue,
      this.messages,
      this.userBloc,
      this.chatRoomID,
      this.bgImageUrl,
      this.fcmNotificationEnable,
      this.connectionStatus,
      this.publicChat,
      this.routeObserver,
      Key key})
      : super(key: MemKeys.communityChatScreen);

  @override
  State createState() => ChatScreenState();
}

class ChatScreenState extends State<ChatScreen>
    with TickerProviderStateMixin, RouteAware {
  DatabaseReference _messagesReference = FirebaseDatabase.instance.reference();
  TextEditingController _textController = TextEditingController();
  bool _isComposing = false;
  var referenceToOldestKey = "";
  List<ChatMessage> _messages = [];
  bool isPerformingRequest = false;
  double _scrollThreshold = 200;
  var lastVisibleKey;
  var lastVisibleValue;
  var end;
  List<StreamSubscription> listeners = []; // list of listeners
  StreamSubscription currentListener;
  Stream currentStream;
  ScrollController _scrollController = ScrollController();
  bool _rosaryListVisible = false;
  final ImagePicker _picker = ImagePicker();

  var removeMessageSub;
  bool imageLoading = false;
  double _progress;
  final GlobalKey<ScaffoldState> _chatScaffoldKey =
      new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

//    FirebaseDatabase database;
//    database = FirebaseDatabase.instance;
//    database.setPersistenceEnabled(true);
//    database.setPersistenceCacheSizeBytes(10000000);

    if (widget.chatRoomID != widget.userBloc.userDetails.id)
      getFirstMessages();
    else if (widget.messages?.length != 0) {
      _messages = widget.messages;
      lastVisibleKey = widget.lastVisibleChatKey;
      lastVisibleValue = widget.lastVisibleChatValue;
    } else
      getFirstMessages();

    _scrollController.addListener((_onScroll));

    removeMessageSub = _messagesReference
        .child("chats")
        .child(widget.chatRoomID)
        .onChildRemoved
        .listen((Event event) {
      var val = event.snapshot.value;
      _removeLocalMessage(
        id: val['id'],
      );
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    widget.routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    _scrollController.dispose();
    widget.routeObserver.unsubscribe(this);
    listeners.forEach((listener) {
      listener.cancel();
    });
    super.dispose();
  }

  @override
  void didPush() {
    // Route was pushed onto navigator and is now topmost route.
  }

  @override
  void didPopNext() {
    // Covering route was popped off the navigator.
  }

  void didPop() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var container = AppStateContainer.of(context);

    UserBloc userBloc = UserProvider.of(context);

    if (widget.publicChat) {
      prefs.setInt(userBloc.userDetails.locale + "_community_last_open",
          DateTime.now().millisecondsSinceEpoch);

      container.state.unreadChatMsgCount = 0;
    } else {
      prefs.setInt(userBloc.userDetails.id + "_admin_chat_last_open",
          DateTime.now().millisecondsSinceEpoch);

      container.state.unreadAdminChatMsgCount = 0;
    }
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (!isPerformingRequest) {
//        debugPrint("Max Scroll: " +
//            _scrollController.position.maxScrollExtent.toString());
        setState(() => isPerformingRequest = true);
        getNextMessages();
        setState(() {
          isPerformingRequest = false;
        });
      }
      debugPrint("Scroll: " + (maxScroll - currentScroll).toString());
    }
  }

  Future<int> getLatestMessageTime() async {
    Query latest;

    int latestMsgTime = 0;
    latest = _messagesReference
        .child("chats")
        .child(widget.chatRoomID)
        .orderByChild("time")
        .limitToLast(1);
    currentListener = latest.onChildAdded.listen((Event event) {
      latestMsgTime = event.snapshot.value['time'];
    });

    listeners.add(currentListener);

    return Future.value(latestMsgTime);
  }

  void getFirstMessages() {
    Query first;
    int count = 0;
    first = _messagesReference
        .child("chats")
        .child(widget.chatRoomID)
        .orderByChild("time")
        .limitToLast(10);
    debugPrint("Count of Chat messages: " + count.toString());

    currentListener = first.onChildAdded.listen((Event event) {
      count++;
      debugPrint("Count of Chat messages: " + count.toString());
      setState(() {
        if (count == 1) {
          lastVisibleKey = event.snapshot.key;
          lastVisibleValue = event.snapshot.value['time'];
          debugPrint("last message: " +
              lastVisibleKey.toString() +
              " " +
              lastVisibleValue.toString() +
              event.snapshot.value['text'].toString());
        }
        if (count > 0) {
          debugPrint("Fetching messages");

          var val = event.snapshot.value;

          var msgType = val['type'] ?? 'chat';
          var msgRegion = val['region'] ?? 'all';

          if (msgType == 'admin' &&
                  msgRegion == widget.userBloc.userDetails.region ||
              msgRegion == "All") {
            _addMessage(
                id: val['id'],
                name: val['sender']['displayName'] ?? val['sender']['name'],
                senderImageUrl: val['sender']['imageUrl'],
                senderId: val['sender']['id'],
                text: val['text'],
                time: val['time'],
                imageUrl: val['imageUrl'],
                textOverlay: val['textOverlay'],
                addToHead: true);
          } else if (msgType != 'admin') {
            _addMessage(
                id: val['id'],
                name: val['sender']['displayName'] ?? val['sender']['name'],
                senderImageUrl: val['sender']['imageUrl'],
                senderId: val['sender']['id'],
                text: val['text'],
                time: val['time'],
                imageUrl: val['imageUrl'],
                textOverlay: val['textOverlay'],
                addToHead: true);
          }
        }
      });
    });
    listeners.add(currentListener);
  }

  void getNextMessages() async {
    Query next;
    int count = 0;
    debugPrint("last Visible key from getnext: " + lastVisibleKey.toString());
    next = _messagesReference
        .child("chats")
        .child(widget.chatRoomID)
        .orderByChild("time")
        .limitToLast(10)
        .endAt(lastVisibleValue);

    currentListener = next.onChildAdded.listen((Event event) {
      count++;
      setState(() {
        if (count == 1) {
          lastVisibleKey = event.snapshot.key;
          lastVisibleValue = event.snapshot.value['time'];
          debugPrint("last" +
              lastVisibleKey.toString() +
              " " +
              lastVisibleValue.toString());
        }

        if (count > 1) {
          var val = event.snapshot.value;
          var msgType = val['type'] ?? 'chat';
          var msgRegion = val['region'] ?? 'all';

          if (msgType == 'admin' &&
              msgRegion == widget.userBloc.userDetails.region) {
            _addMessage(
                id: val['id'],
                name: val['sender']['displayName'] ?? val['sender']['name'],
                senderImageUrl: val['sender']['imageUrl'],
                senderId: val['sender']['id'],
                text: val['text'],
                time: val['time'],
                imageUrl: val['imageUrl'],
                textOverlay: val['textOverlay'],
                addToHead: true);
          } else if (msgType != 'admin') {
            _addMessage(
                id: val['id'],
                name: val['sender']['displayName'] ?? val['sender']['name'],
                senderImageUrl: val['sender']['imageUrl'],
                senderId: val['sender']['id'],
                text: val['text'],
                time: val['time'],
                imageUrl: val['imageUrl'],
                textOverlay: val['textOverlay'],
                addToHead: true);
          }
        }
      });
    });
    listeners.add(currentListener);
  }

  void _handleMessageChanged(String text) {
//    setState(() {
    _isComposing = text.length > 0;
//    });

    setState(() {
      _rosaryListVisible = false;
      ;
    });
  }

  void _handleSubmitted(String text) {
    MessageBloc messageBloc = MessageProvider.of(context);

    _textController.clear();

    String id = Uuid().generateV4();

    var message = {
      'id': id,
      'sender': {
        'name': widget.userBloc.userDetails.role == "admin"
            ? "Admin"
            : widget.userBloc.userDetails.displayName ??
                widget.userBloc.userDetails.name,
        'id': widget.userBloc.userDetails.id,
        'imageUrl': widget.userBloc.userDetails.profilePhotoUrl
      },
      'text': text,
      'time': DateTime.now().millisecondsSinceEpoch,
      'type': 'chat',
      'region': 'all'
    };
    _messagesReference
        .child("chats")
        .child(widget.chatRoomID)
        .child(id)
        .set(message);

    FCMNotification notification = FCMNotification(
        title: "New message from " + widget.userBloc.userDetails.name,
        body: text ?? "",
        topic: widget.chatRoomID,
        author: widget.userBloc.userDetails.displayName ??
            widget.userBloc.userDetails.name,
        time: DateTime.now().add(Duration(minutes: 1)),
        authorID: widget.userBloc.userDetails.id ?? "",
        priority: "medium");

    if (widget.publicChat) {
      if (widget.fcmNotificationEnable)
        messageBloc.messageRepository.addNotification(notification.toEntity());
    } else {
      MEMmessage msg = MEMmessage(
          id: id,
          title: "From " + widget.userBloc.userDetails.displayName ??
              widget.userBloc.userDetails.name,
          body: text ?? "",
          type: "guidance",
          author: widget.userBloc.userDetails.displayName ??
              widget.userBloc.userDetails.name,
          // author: userDetails.displayName ?? userDetails.name,
          time: DateTime.now(),
          authorID: widget.userBloc.userDetails.id ?? "");
      // Adding Guidance Request to Firestore
      if (widget.chatRoomID == widget.userBloc.userDetails.id)
        messageBloc.messageRepository.addNewMessage(msg.toEntity());
    }

    setState(() {
      _isComposing = false;
      _rosaryListVisible = true;
    });
  }

  void _addMessage(
      {String id,
      String name,
      String text,
      int time,
      String imageUrl,
      String textOverlay,
      String senderImageUrl,
      String senderId,
      bool addToHead}) {
    var animationController = AnimationController(
      duration: Duration(milliseconds: 700),
      vsync: this,
    );
    var sender = ChatUser(name: name, imageUrl: senderImageUrl, id: senderId);
    var message = ChatMessage(
        id: id,
        sender: sender,
        text: text,
        time: time,
        imageUrl: imageUrl,
        textOverlay: textOverlay,
        animationController: animationController);

    setState(() {
      debugPrint(_messages.toString());

      if (addToHead)
        _messages.insert(0, message);
      else
        _messages.insert(_messages.length - 1, message);

      _messages.sort((a, b) => b.time.compareTo(a.time));
    });
    try {
      if (imageUrl != null) {
        NetworkImage image = NetworkImage(imageUrl);
        //Stable Channel
//        image
//            .resolve(createLocalImageConfiguration(context))
//            .addListener((_, __) {
//          animationController?.forward();
//        });
        //Master Channel _ Flutter
        image.resolve(createLocalImageConfiguration(context)).addListener(
            ImageStreamListener((ImageInfo image, bool synchronousCall) {
          animationController?.forward();
        }));
      } else {
        animationController?.forward();
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  void _removeLocalMessage({
    String id,
  }) {
    var animationController = AnimationController(
      duration: Duration(milliseconds: 700),
      vsync: this,
    );
    if (mounted) {
      setState(() {
        _messages.removeWhere((msg) => msg.id == id);
      });
    }

    animationController?.animateBack(0.0);
  }

  void _handleEmojiButtonPressed() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return EmojiPicker(
            rows: 5,
            columns: 7,
            recommendKeywords: ["PRARTHANA", "FELLOWSHIP", "PRIZE", "EMOTIONS"],
            numRecommended: 35,
            onEmojiSelected: (emoji, category) {
              String _message = _textController.text + emoji.emoji;
              setState(() {
                _textController.text = _message;
                _isComposing = true;
              });
            },
          );
        });
  }

  Future<Null> _handlePhotoButtonPressed() async {
    String downloadUrl;

    _picker.getImage(source: ImageSource.gallery).then((pickedFile) {
      if (pickedFile != null) {
        var imageFile = File(pickedFile.path);
        Navigator.push(context, TypeMemeRoute(imageFile)).then((textOverlay) {
          if (textOverlay == null) return;

          FlutterNativeImage.compressImage(imageFile.path,
                  quality: 80, percentage: 80)
              .then((_compressedImage) async {
            String downloadUrl =
                await _uploadFile(_compressedImage, textOverlay);

            debugPrint("Uploaded Image: " + downloadUrl.toString());
            //ref.putFile(imageFile);
          });
        });
      }
    });
  }

  Future<String> _uploadFile(File _image, String textOverlay) async {
    List<String> fileNameList = _image.path.split("/");
    String downloadUrl;
    String fileName = fileNameList[fileNameList.length - 1];

    var random = Random().nextInt(10000);
    var ref = FirebaseStorage.instance
        .ref()
        .child('chats')
        .child(widget.chatRoomID)
        .child('image_$random.jpg');

    final UploadTask uploadTask = ref.putFile(
      _image,
      SettableMetadata(
          customMetadata: <String, String>{'imagetype': 'userUpload'},
          contentType: 'image/jpeg'),
    );

    uploadTask.snapshotEvents.listen((event) {
      setState(() {
        imageLoading = true;
        _progress =
            event.bytesTransferred.toDouble() / event.totalBytes.toDouble();
      });
    }).onError((error) {
      _chatScaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(error.toString()),
        backgroundColor: Colors.red,
      ));
    });
    uploadTask.whenComplete(() {
      setState(() {
        imageLoading = false;
      });
      ref.getDownloadURL().then((url) {
        downloadUrl = url;

        String id = Uuid().generateV4();

        var message = {
          'id': id,
          'sender': {
            'name': widget.userBloc.userDetails.displayName ??
                widget.userBloc.userDetails.name,
            'imageUrl': widget.userBloc.userDetails.profilePhotoUrl,
            'id': widget.userBloc.userDetails.id
          },
          'text': "",
          'imageUrl': downloadUrl,
          'textOverlay': textOverlay,
          'time': DateTime.now().millisecondsSinceEpoch,
          'type': 'chat_image',
          'locale': widget.chatRoomID,
          'region': 'all'
        };
        _messagesReference
            .child("chats")
            .child(widget.chatRoomID)
            .child(id)
            .set(message);
      });
    });

    return downloadUrl;
  }

  Widget _buildTextComposer() {
    return IconTheme(
        data: IconThemeData(color: Theme.of(context).accentColor),
        child: PlatformAdaptiveContainer(
            margin: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
//                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 1.0),
                    child: IconButton(
                      color: Theme.of(context).accentColor,
                      icon: Icon(Icons.photo),
                      onPressed: _handlePhotoButtonPressed,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 1.0),
                    child: IconButton(
                      color: Theme.of(context).accentColor,
                      icon: Icon(Icons.tag_faces),
                      onPressed: _handleEmojiButtonPressed,
                    ),
                  ),
                  Flexible(
                    child: TextField(
                      controller: _textController,
                      onSubmitted: _handleSubmitted,
                      onChanged: _handleMessageChanged,
                      decoration: InputDecoration.collapsed(
                          hintText: widget.publicChat
                              ? 'Send a message'
                              : widget.userBloc.userDetails.role != 'admin'
                                  ? 'Request spiritual guidance'
                                  : 'Give spiritual guidance'),
                      maxLines: null,
                      textInputAction: TextInputAction.newline,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 4.0),
                    child: widget.publicChat
                        ? PlatformAdaptiveButton(
                            icon: Icon(
                              Icons.send,
                              color: _isComposing
                                  ? Color.fromRGBO(11, 129, 140, 1.0)
                                  : Colors.grey,
                            ),
                            onPressed: _isComposing
                                ? () => _handleSubmitted(_textController.text)
                                : null,
                            child: Text('Send'),
                          )
                        : Container(
                            width: 35.0,
                            height: 35.0,
                            child: RawMaterialButton(
                              shape: CircleBorder(),
                              fillColor: Theme.of(context).accentColor,
                              elevation: 1.0,
                              child: ImageIcon(
                                AssetImage('assets/icons/counselling-icon.png'),
                                size: 25.0,
                                color: Colors.white,
                              ),
                              onPressed: _isComposing
                                  ? () => _handleSubmitted(_textController.text)
                                  : null,
                            )),
                  ),
                ])));
  }

  //Scroll Notification Call backs
  _onStartScroll(ScrollMetrics metrics) {
    setState(() {
      _rosaryListVisible = false;
    });
  }

  _onUpdateScroll(ScrollMetrics metrics) {
    setState(() {
      _rosaryListVisible = false;
    });
  }

  _onEndScroll(ScrollMetrics metrics) {
    setState(() {
      _rosaryListVisible = true;
    });
  }

  // youtube

  String getThumbnail(
      {@required String videoId, String quality = ThumbnailQuality.standard}) {
    String _thumbnailUrl = 'https://i3.ytimg.com/vi/$videoId/';
    switch (quality) {
      case ThumbnailQuality.defaultQuality:
        _thumbnailUrl += 'default.jpg';
        break;
      case ThumbnailQuality.high:
        _thumbnailUrl += 'hqdefault.jpg';
        break;
      case ThumbnailQuality.medium:
        _thumbnailUrl += 'mqdefault.jpg';
        break;
      case ThumbnailQuality.standard:
        _thumbnailUrl += 'sddefault.jpg';
        break;
      case ThumbnailQuality.max:
        _thumbnailUrl += 'maxresdefault.jpg';
        break;
    }
    return _thumbnailUrl;
  }

  Widget build(BuildContext context) {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);

    setState(() {
      container.state.unreadChatMsgCount = 0;
    });

    bool _isMessageAdmin = container.state.userAcl != null
        ? container.state.userDetails.isSuperAdmin &&
            container.state.userAcl.messageDelete
        : false;

    return Scaffold(
        key: _chatScaffoldKey,
        appBar: AppBar(
            title: Text(
              widget.publicChat
                  ? "MEM ( " + userBloc.userDetails.locale + " )"
                  : "Spiritual Guidance",
              style: TextStyle(
                  fontSize: 20.0,
                  color: Theme.of(context).primaryIconTheme.color),
            ),

            //platform: Theme.of(context).platform,
            backgroundColor: Theme.of(context).backgroundColor,
            bottom: imageLoading
                ? PreferredSize(
                    child: LinearProgressIndicator(
                      value: _progress,
                      backgroundColor: Colors.white,
                    ),
                    preferredSize: Size(MediaQuery.of(context).size.width, 5.0),
                  )
                : null,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, 0),
                  end: Alignment(
                      1.0, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    Theme.of(context).primaryColorDark,
                    Theme.of(context).backgroundColor
                  ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
            )),
        body: Stack(
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                        image: AssetImage(widget.bgImageUrl),
                        fit: BoxFit.contain,
                        repeat: ImageRepeat.repeat)),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      // Removing Rosary List to make chat room more simple and neat.

//                      widget.publicChat
//                          ? AnimatedOpacity(
//                              // If the widget is visible, animate to 0.0 (invisible).
//                              // If the widget is hidden, animate to 1.0 (fully visible).
//                              opacity: _rosaryListVisible ? 1.0 : 0.0,
//                              duration: Duration(milliseconds: 5000),
//                              // The green box must be a child of the AnimatedOpacity widget.
//                              child: _rosaryListVisible
//                                  ? RosaryList(
//                                      filteredMessages:
//                                          container.state.rosaryMessages,
//                                      //     filteredMessages: container.state.rosaryMessages,
//                                      messageBloc: messageBloc,
//                                      bgImageUrl: "assets/mary_bg.png",
//                                      connectionStatus: true,
//                                      msgType: "rosary",
//                                    )
//                                  : Container())
//                          : Container(),
                      Flexible(
//                          child: NotificationListener<ScrollNotification>(
//                        onNotification: (scrollNotification) {
//                          if (scrollNotification is ScrollStartNotification) {
//                            _onStartScroll(scrollNotification.metrics);
//                          } else if (scrollNotification
//                              is ScrollUpdateNotification) {
//                            _onUpdateScroll(scrollNotification.metrics);
//                          } else if (scrollNotification
//                              is ScrollEndNotification) {
//                            _onEndScroll(scrollNotification.metrics);
//                          }
//
//                        },
                        child: _messages.length > 0
                            ? ListView.builder(
                                padding: EdgeInsets.all(8.0),
                                shrinkWrap: true,
                                controller: _scrollController,
                                reverse: true,
                                itemBuilder: ((_, int index) {
                                  //if (_messages.length == 0) {
                                  return ChatMessageListItem(
                                      _messages[index],
                                      _isMessageAdmin,
                                      widget.connectionStatus,
                                      widget.chatRoomID);

                                  //else {
//                                    return Padding(
//                                        padding: const EdgeInsets.all(8.0),
//                                        child: Center(
//                                            child: CircularProgressIndicator(
//                                                valueColor:
//                                                    AlwaysStoppedAnimation<
//                                                            Color>(
//                                                        Colors.deepOrange))));
//                                  }
                                }),
                                itemCount: _messages.length,
                              )
                            : Container(
                                child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Center(
                                        child: CircularProgressIndicator(
                                            valueColor:
                                                AlwaysStoppedAnimation<Color>(
                                                    Colors.white))))),
                      ),
                      Divider(height: 1.0),
//                      Container(
//                          decoration:
//                              BoxDecoration(color: Theme.of(context).cardColor),
//                          child: _buildTextComposer()),
                    ])),
            Positioned(
              bottom: 0.0,
              left: 0.0,
              width: MediaQuery.of(context).size.width,
              child: Container(
                  decoration: BoxDecoration(color: Theme.of(context).cardColor),
                  child: _buildTextComposer()),
            ),
            Positioned(
                bottom: MediaQuery.of(context).size.height * 0.11,
                right: MediaQuery.of(context).size.width * 0.05,
                child: Container(
                  decoration: new BoxDecoration(
                    color: Color.fromRGBO(65, 65, 65, 0.6),
                    borderRadius: new BorderRadius.all(Radius.circular(30.0)),
                  ),
                  height: 30.0,
                  width: 30.0,
                  child: FlatButton(
                    padding: EdgeInsets.all(2.0),
                    child: Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.white,
                      size: 20.0,
                    ),
                    onPressed: () {
                      try {
                        _scrollController.animateTo(
                          10.0,
                          curve: Curves.easeOut,
                          duration: const Duration(milliseconds: 300),
                        );
                      } catch (e) {
                        debugPrint("Caught Exception");
                      }
                    },
                  ),
                )),
          ],
        ));
  }
}

class ChatMessageListItem extends StatelessWidget {
  ChatMessageListItem(this.message, this.isMessageAdmin, this.connectionStatus,
      this.chatRoomId);

  final ChatMessage message;
  final bool isMessageAdmin;
  final bool connectionStatus;
  final String chatRoomId;

  Widget build(BuildContext context) {
    return SizeTransition(
      sizeFactor: CurvedAnimation(
          parent: message.animationController, curve: Curves.easeOut),
      axisAlignment: 0.0,
      child: ChatMessageContent(
          message, isMessageAdmin, connectionStatus, chatRoomId),
    );
  }
}

class ChatMessageContent extends StatelessWidget {
  final ChatMessage message;
  final bool isMessageAdmin;
  final bool connectionStatus;
  final String chatRoomId;

  ChatMessageContent(this.message, this.isMessageAdmin, this.connectionStatus,
      this.chatRoomId);

  MalTranslator malTranslator = MalTranslator();
  bool _eligibleToDelete = false;
  bool _selfMessage = false;
  MemError errorMsg = MemError(title: "", body: "");
  DatabaseReference _messagesReference = FirebaseDatabase.instance.reference();
  UserBloc userBloc;

  Widget build(BuildContext context) {
    userBloc = UserProvider.of(context);

    var profilePhotoRadius = MediaQuery.of(context).size.width * 0.03;

    var container = AppStateContainer.of(context);

    _eligibleToDelete =
        isMessageAdmin || message.sender.id == userBloc.userDetails.id;

    _selfMessage = message.sender.id == userBloc.userDetails.id;

    //Font Sizes
    var imageCaptionFontSize = MediaQuery.of(context).size.width * 0.05;
    var dateTimeFontSize = imageCaptionFontSize * 0.7;
    var msgTextFontSize = imageCaptionFontSize * 0.5;
    var authorFontSize = imageCaptionFontSize * 0.6;

    if (message.imageUrl != null) {
      var image;
      try {
        image = Image.network(message.imageUrl,
            width: MediaQuery.of(context).size.width * 0.9);
      } catch (e) {
        return Container();
      }
      if (message.textOverlay == null && image != null) {
        return Container(
            decoration: BoxDecoration(
                color: Colors.white54,
                border: Border.all(color: Colors.grey, width: 0.1)),
            child: image);
      } else {
        return Container(
            margin: EdgeInsets.only(
                top: 15.0, bottom: 0.0, left: 10.0, right: 10.0),
            padding: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColorLight,
              borderRadius: BorderRadius.all(Radius.circular(2.0)),
            ),
            child: Stack(
//              alignment: FractionalOffset.topCenter,
              children: [
                image,
                Positioned(
                    bottom: 10.0,
                    child: Container(
                        padding: EdgeInsets.all(20.0),
                        alignment: FractionalOffset.topCenter,
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: Text(message.textOverlay,
                            style: TextStyle(
                                fontFamily: 'Bahnschrift',
                                fontSize: imageCaptionFontSize,
                                color: Colors.white),
                            softWrap: true,
                            textAlign: TextAlign.center))),
                Container(
                    padding: EdgeInsets.only(
                        top: 5.0, left: 2.0, right: 5.0, bottom: 2.0),
                    alignment: FractionalOffset.topCenter,
                    width: MediaQuery.of(context).size.width * 0.9,
                    //width: message.text.length * 0.1,
                    decoration: BoxDecoration(color: Colors.white),
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: const EdgeInsets.only(
                                      left: 17.0, right: 25.0),
                                  child: Text(
                                    isMessageAdmin
                                        ? "Admin"
                                        : message.sender.name,
                                    style: TextStyle(
                                        color: Colors.red[500],
                                        fontWeight: FontWeight.bold,
                                        fontSize: authorFontSize),
                                  ))),
                          Container(
                              child: Text(
                                new DateFormat.yMMMd().format(
                                    DateTime.fromMillisecondsSinceEpoch(
                                        message.time)),
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: dateTimeFontSize),
                              ),

                              ///todo: write a stream for formatted date & time.
                              margin: const EdgeInsets.only(right: 40.0)),
                          Text(
                            new DateFormat.jm().format(
                                DateTime.fromMillisecondsSinceEpoch(
                                    message.time)),
                            style: TextStyle(
                                color: Colors.grey, fontSize: dateTimeFontSize),
                          ),
                        ],
                      )
                    ])),
                Positioned(
                    bottom: 10.0,
                    right: 10.0,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        _eligibleToDelete
                            ? Container(
                                decoration: new BoxDecoration(
                                  color: Colors.blueGrey.shade300,
                                  borderRadius: new BorderRadius.all(
                                      Radius.circular(30.0)),
                                ),
                                height: 30.0,
                                width: 30.0,
                                margin: EdgeInsets.only(left: 5.0),
                                child: IconButton(
                                  iconSize: 14.0,
                                  color: Colors.white,
                                  key: MemKeys.deleteMessageButton,
                                  tooltip: MemLocalizations(
                                          Localizations.localeOf(context))
                                      .deleteMessage,
                                  icon: Icon(Icons.delete),
                                  onPressed: () {
                                    if (connectionStatus) {
                                      confirmDelete(
                                          errorMsg: MemError(
                                              title:
                                                  "Are you sure ? Please confirm to delete",
                                              body:
                                                  "You are deleting a message. Please press Delete to confirm delete."),
                                          context: context);
                                    } else {
                                      showError(
                                          errorMsg: MemError(
                                              title: "Not Connected",
                                              body:
                                                  "Please check your internet connection. You can delete a message only when you are online."),
                                          context: context);
                                    }

                                    //Navigator.pop(context, message);
                                  },
                                ))
                            : Container(),
                        message.imageUrl.isEmpty
                            ? Container()
                            : Container(
                                decoration: new BoxDecoration(
                                  color: Colors.blueGrey.shade300,
                                  borderRadius: new BorderRadius.all(
                                      Radius.circular(30.0)),
                                ),
                                height: 30.0,
                                width: 30.0,
                                margin: EdgeInsets.only(left: 5.0),
                                child: IconButton(
                                    iconSize: 14.0,
                                    color: Colors.white,
                                    icon: Icon(Icons.share),
                                    onPressed: (() {
                                      final RenderBox box =
                                          context.findRenderObject();
                                      Share.share(message.imageUrl,
                                          sharePositionOrigin:
                                              box.localToGlobal(Offset.zero) &
                                                  box.size);
                                    }))),
                      ],
                    ))
              ],
            ));
      }
    } else
      return AnimatedOpacity(
        opacity: 1.0,
        duration: Duration(milliseconds: 500),
        child: Stack(
          children: <Widget>[
//                  Positioned(
//                      top: 10.0,
//                      left: 400.0,
//                      child: IconButton(
//                          color: Colors.grey,
//                          icon: Icon(Icons.share),
//                          onPressed: message.text.isEmpty
//                              ? null
//                              : (() {
//                                  final RenderBox box =
//                                      context.findRenderObject();
//                                  Share.share(message.text,
//                                      sharePositionOrigin:
//                                          box.localToGlobal(Offset.zero) &
//                                              box.size);
//                                }))),
            Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                    top: 15.0, bottom: 0.0, left: 10.0, right: 10.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                ),
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  FutureBuilder(
                      future: userBloc.authorUser(message.sender.id),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          return Container(
                              padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: snapshot.data?.role == "admin"
                                    ? Theme.of(context).buttonColor
                                    : Theme.of(context).accentColor,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0)),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  CircleAvatar(
                                    radius: profilePhotoRadius,
                                    backgroundImage: snapshot.data != null
                                        ? snapshot.data.role == "admin"
                                            ? AssetImage(
                                                "assets/logo_drawer.png")
                                            : CachedNetworkImageProvider(snapshot
                                                        .data.profilePhotoUrl ??
                                                    "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a") ??
                                                NetworkImage(snapshot
                                                        .data.profilePhotoUrl ??
                                                    "") ??
                                                AssetImage(
                                                    "assets/placeholder-face.png")
                                        : AssetImage(
                                            "assets/placeholder-face.png"),
                                  ),
                                  Expanded(
                                      child: Container(
                                          margin: const EdgeInsets.only(
                                              left: 10.0, right: 2.0),
                                          child: Text(
                                            snapshot.data?.role == "admin"
                                                ? "Admin"
                                                : message.sender.name.length >
                                                        19
                                                    ? message.sender.name
                                                            .substring(0, 18) +
                                                        "..."
                                                    : message.sender.name,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 13.0),
                                          ))),
                                  Container(
                                      child: Text(
                                        new DateFormat.MMMMd().format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                message.time)),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 10.0),
                                      ),
//
//                        ///todo: write a stream for formatted date & time.
                                      margin:
                                          const EdgeInsets.only(right: 5.0)),
                                  Text(
                                    new DateFormat.jm().format(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            message.time)),
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 10.0),
                                  ),
                                ],
                              ));
                        } else
                          return CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.white));
                      }),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding:
                        EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
                    margin: EdgeInsets.only(
                        top: 5.0, left: 10.0, right: 10.0, bottom: 0.0),
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                      color: _selfMessage
                          ? Color(0xFFf9f2d3)
                          : Theme.of(context).primaryColorLight,
                      child: Theme.of(context).platform == TargetPlatform.iOS
                          ? Linkify(
                              onOpen: _onOpen,
                              text: malTranslator.isMal(message.text)
                                  ? malTranslator.formatMalContent(message.text)
                                  : message.text,

                              textAlign: TextAlign.center,
                              linkStyle: TextStyle(color: Colors.blue),
                              humanize: true,
                              //overflow: TextOverflow.ellipsis,
                              style: malTranslator.isMal(message.text)
                                  ? Theme.of(context)
                                      .textTheme
                                      .subtitle2
                                      .copyWith(
                                          fontSize: 14.0,
                                          fontFamily: 'Karthika')
                                  : Theme.of(context)
                                      .textTheme
                                      .subtitle2
                                      .copyWith(
                                          fontSize: 14.0,
                                          fontFamily: 'Bahnschrift'),
                            )
                          : Linkify(
                              onOpen: _onOpen,
                              text: message.text,

                              textAlign: TextAlign.left,
                              linkStyle: TextStyle(color: Colors.blue),
                              humanize: true,
                              //overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  .copyWith(
                                      fontSize: 14.0,
                                      fontFamily: 'Bahnschrift'),
                            ),
                    ),
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                    IconButton(
                        color: Color(0xFFadadad),
                        icon: Icon(Icons.share),
                        iconSize: 16.0,
                        onPressed: message.text.isEmpty
                            ? null
                            : (() {
                                final RenderBox box =
                                    context.findRenderObject();
                                Share.share(message.text,
                                    sharePositionOrigin:
                                        box.localToGlobal(Offset.zero) &
                                            box.size);
                              })),
                    _eligibleToDelete
                        ? IconButton(
                            iconSize: 16.0,
                            color: Color(0xFFadadad),
                            key: MemKeys.deleteMessageButton,
                            tooltip: MemLocalizations(
                                    Localizations.localeOf(context))
                                .deleteMessage,
                            icon: Icon(Icons.delete),
                            onPressed: () {
                              if (connectionStatus) {
                                confirmDelete(
                                    errorMsg: MemError(
                                        title:
                                            "Are you sure ? Please confirm to delete",
                                        body:
                                            "You are deleting a message. Please press Delete to confirm delete."),
                                    context: context);
                              } else {
                                showError(
                                    errorMsg: MemError(
                                        title: "Not Connected",
                                        body:
                                            "Please check your internet connection. You can delete a message only when you are online."),
                                    context: context);
                              }

                              //Navigator.pop(context, message);
                            },
                          )
                        : Container(),
                  ]),
                ])),
          ],
        ),
      );
  }

  Future<void> _onOpen(LinkableElement link) async {
    if (await canLaunch(link.url)) {
      await launch(link.url);
    } else {
      throw 'Could not launch $link';
    }
  }

  void showError({MemError errorMsg, BuildContext context}) {
    var dialog = DialogShower.buildDialog(
        title: errorMsg.title,
        message: errorMsg.body,
        confirm: "Okay",
        confirmFn: () {
          Navigator.of(context, rootNavigator: true).pop();
        });
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void confirmDelete({MemError errorMsg, BuildContext context}) {
    var dialog = DialogShower.buildDialog(
        title: errorMsg.title,
        message: errorMsg.body,
        cancel: "Cancel",
        confirm: "Delete",
        confirmFn: () {
          _removeMessage();
          Navigator.of(context, rootNavigator: true).pop();
        },
        cancelFn: () {
          Navigator.of(context, rootNavigator: true).pop();
        });
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  _removeMessage() {
    _messagesReference
        .child("chats")
        .child(chatRoomId)
        .child(message.id)
        .remove();
  }
}
