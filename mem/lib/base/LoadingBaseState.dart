import 'package:flutter/material.dart';
import 'package:utility/src/dialogshower.dart' as DialogShower;

abstract class LoadingBaseState<T extends StatefulWidget> extends State<T> {
  bool _isLoading = false;
  bool _hasUser = false;
  String _title = "";
//  final GlobalKey<ScaffoldState> authScaffoldKey =
//      new GlobalKey<ScaffoldState>();

  Future<bool> _exitApp(BuildContext context) {
    var dialog = AlertDialog(
      title: Text('Do you want to exit this application?'),
      content: Text('We hope to see you back soon,God bless you...'),
      actions: <Widget>[
        new FlatButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: new Text('No'),
        ),
        new FlatButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: new Text('Yes'),
        ),
      ],
    );
    return showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
      onWillPop: () => _exitApp(context),
      child: Scaffold(
        body: _isLoading
            ? new Center(
                child: new CircularProgressIndicator(),
              )
            : Container(
                color: Theme.of(context).primaryColorLight, child: content()),
      ));

  Widget content();

  set isLoading(bool value) {
    _isLoading = value;
  }

  set title(String value) {
    _title = value;
  }

  set hasUser(bool value) {
    _hasUser = value;
  }

  void _logOut() {
    var dialog = DialogShower.buildDialog(
        title: "Leaving?",
        message: "Are you sure you want to log out?",
        confirm: "Yes",
        confirmFn: () {
          setState(() => _isLoading = true);
          Navigator.pop(context);
        },
        cancel: "No",
        cancelFn: () => Navigator.pop(context));
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }
}
