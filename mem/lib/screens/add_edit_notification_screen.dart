// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';

class AddEditNotificationScreen extends StatelessWidget {
  static final GlobalKey<FormState> notificationFormKey =
      GlobalKey<FormState>();
  String dropdownValue;
  AppState appState;
  List<String> locales;

  AddEditNotificationScreen({
    Key key,
  }) : super(key: key ?? MemKeys.addMessageScreen);

  @override
  Widget build(BuildContext context) {
    final userBloc = UserProvider.of(context);
    final container = AppStateContainer.of(context);
    final notificationBloc = NotificationProvider.of(context);
    appState = container.state;

    locales = container.state.locales ??
        <String>['English', 'Malayalam', 'Hindi', 'Tamil', 'Italian'];

    return Scaffold(
      appBar: AppBar(
          title: Text("Send Notification?"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: notificationFormKey,
          autovalidate: false,
          onWillPop: () {
            return Future(() => true);
          },
          child: userBloc.userDetails.isSuperAdmin
              ? ListView(
                  children: [
                    msgTitleField(notificationBloc),
                    msgBodyField(notificationBloc),
                    topicField(notificationBloc),
                    submitButton(notificationBloc, userBloc, container),
                  ],
                )
              : Container(),
        ),
      ),
//      floatingActionButton: StreamBuilder(
//              stream: messageBloc.submitValidMessage,
//              builder: (context, snapshot) {
//                return FloatingActionButton.extended(
//                    label: Text("Submit"),
//                    tooltip: isEditing
//                        ? MemLocalizations(Localizations.localeOf(context))
//                            .saveChanges
//                        : MemLocalizations(Localizations.localeOf(context))
//                            .addMessage,
//                    icon: Icon(isEditing ? Icons.check : Icons.add),
//                    onPressed: () {
//                      final form = formKey.currentState;
//                      if (form.validate() && snapshot.hasData) {
////                        final title = titleKey.currentState.value;
////                        final body = bodyKey.currentState.value;
//
//                        if (isEditing) {
//                          messageBloc.updateMessage(msgType);
//                        } else {
//                          messageBloc.addMessage(
//                              msgType, container.state.userDetails);
//                        }
//
//                        Navigator.pop(context);
//                      }
//                    });
//              }) ??
//          Container(),
    );
  }

  Widget msgTitleField(NotificationBloc bloc) {
    return StreamBuilder(
      stream: bloc.msgTitle,
      builder: (context, snapshot) {
        return TextField(
          //autofocus: isEditing ? false : true,

          onChanged: bloc.changeMsgTitle,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: MemLocalizations(Localizations.localeOf(context))
                .newMessageTitleHint,
            labelText: 'Message Title',
            errorText: snapshot.error,
          ),
        );
      },
    );
  }

  Widget msgBodyField(NotificationBloc bloc) {
    return StreamBuilder(
      stream: bloc.msgBody,
      builder: (context, snapshot) {
        return TextField(
          onChanged: bloc.changeMsgBody,
          keyboardType: TextInputType.text,
          maxLines: 5,
          style: Theme.of(context).textTheme.subhead,
          decoration: InputDecoration(
            hintText:
                MemLocalizations(Localizations.localeOf(context)).messageHint,
            labelText: 'Message Details',
            errorText: snapshot.error,
          ),
        );
      },
    );
  }

//  Widget localeField(UserBloc bloc) {
//    return StreamBuilder(
//      stream: bloc.locale,
//      builder: (context, snapshot) {
//        return Container(
//            decoration:
//            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
//            padding: EdgeInsets.only(top: 20.0),
//            child: Row(children: <Widget>[
//              Icon(
//                Icons.language,
//                color: Colors.grey,
//              ),
//              Expanded(
//                  flex: 2,
//                  child: Container(
//                      padding: EdgeInsets.only(
//                        left: 20.0,
//                      ),
//                      child: DropdownButtonHideUnderline(
//                          child: ButtonTheme(
//                              alignedDropdown: false,
//                              child: DropdownButton<String>(
//                                  hint: Text("Language"),
//                                  value: dropdownValue,
//                                  onChanged: bloc.changeLocale,
//                                  items: locales.map((String value) {
//                                    return DropdownMenuItem<String>(
//                                        value: value, child: Text(value));
//                                  }).toList())))))
//            ]));
//      },
//    );
//  }

  Widget submitButton(NotificationBloc bloc, UserBloc userBloc, var container) {
    return StreamBuilder(
        stream: bloc.submitValidNotificationMessage,
        builder: (context, snapshot) {
          return Container(
              margin: EdgeInsets.only(top: 10.0),
              padding: EdgeInsets.only(
                  top: 5.0, bottom: 20.0, left: 60.0, right: 60.0),
              child: FlatButton(
                  padding: EdgeInsets.only(
                      top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
                  color: Colors.blueGrey,
                  child: Text("Submit"),
                  onPressed: () {
                    final form = notificationFormKey.currentState;
                    if (form.validate() && snapshot.hasData) {
//                        final title = titleKey.currentState.value;
//                        final body = bodyKey.currentState.value;

                      bloc.addNotification(userBloc.userDetails);
//                      if (isEditing) {
//                        messageBloc.updateMessage(msgType);
//                      } else {
//                        messageBloc.addMessage(
//                            msgType, container.state.userDetails);
//                      }

                      Navigator.pop(context);
                    }
                  }));
        });
  }

  Widget topicField(NotificationBloc bloc) {
    return StreamBuilder(
      stream: bloc.topic,
      builder: (context, snapshot) {
        return Container(
            decoration:
                BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
            padding: EdgeInsets.only(top: 20.0),
            child: Row(children: <Widget>[
              Icon(
                Icons.add_alert,
                color: Colors.grey,
              ),
              Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(
                        left: 20.0,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                              alignedDropdown: false,
                              child: DropdownButton<String>(
                                  hint: Text("Notification Type"),
                                  value: dropdownValue,
                                  onChanged: bloc.changeTopic,
                                  items: locales.map((String value) {
                                    return DropdownMenuItem<String>(
                                        value: value, child: Text(value));
                                  }).toList())))))
            ]));
      },
    );
  }

  //bool get isEditing => message != null;
}
