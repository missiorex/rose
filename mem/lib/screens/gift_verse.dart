// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mem_blocs/mem_blocs.dart';

class GiftVerseScreen extends StatefulWidget {
  final giftVerse;
  final bool isBadge;
  final String badgeType;
  Stream<QuerySnapshot> badgeStream;
  String latestBadgeID;
  MessageBloc messageBloc;
  UserBloc userBloc;
  final RouteObserver<PageRoute> routeObserver;

  GiftVerseScreen({
    this.giftVerse,
    this.isBadge,
    this.latestBadgeID,
    this.badgeStream,
    this.badgeType,
    this.messageBloc,
    this.userBloc,
    this.routeObserver,
    Key key,
  }) : super(key: MemKeys.giftVerseScreen);

  @override
  State createState() => _GiftVerseScreenState();
}

class _GiftVerseScreenState extends State<GiftVerseScreen> with RouteAware {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    widget.routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    widget.routeObserver.unsubscribe(this);

    super.dispose();
  }

  @override
  void didPush() {
    // Route was pushed onto navigator and is now topmost route.
    if (widget.isBadge) {
      widget.messageBloc.rewardsRepository.updateBadgeComplete(
          widget.userBloc.userDetails.id, widget.latestBadgeID);
    }
  }

  @override
  void didPopNext() {
    // Covering route was popped off the navigator.
  }

  void didPop() {
    if (widget.isBadge) {
      widget.messageBloc.rewardsRepository.updateBadgeComplete(
          widget.userBloc.userDetails.id, widget.latestBadgeID);
    }
  }

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: badgeBody(context),
    );
  }

  Widget badgeBody(BuildContext context) {
    var deviceWidth = MediaQuery.of(context).size.width;
    var deviceHeight = MediaQuery.of(context).size.height;
    var closeButtonRadius = MediaQuery.of(context).size.width * 0.1;
    UserBloc userBloc = UserProvider.of(context);

    return widget.isBadge
        ? Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: widget.badgeType == "rosary_warrior"
                        ? AssetImage('assets/badges-rw-bg.png')
                        : AssetImage('assets/badges-gs-bg.png'),
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              Positioned(
                  top: deviceHeight * 0.001,
                  left: deviceWidth * 0.03,
                  child: Container(
                      margin: EdgeInsets.only(top: closeButtonRadius),
                      width: closeButtonRadius,
                      height: closeButtonRadius,
                      decoration: BoxDecoration(
                          color: Colors.brown,
                          borderRadius: BorderRadius.all(
                              Radius.circular(closeButtonRadius))),
                      child: IconButton(
                        icon: Icon(
                          Icons.close,
                          color: Colors.white,
                          size: closeButtonRadius * 0.5,
                        ),
                        onPressed: () {
                          try {
                            Navigator.canPop(context)
                                ? Navigator.pop(context)
                                : Navigator.of(context).pushReplacementNamed(
                                    '/home'); //close the popup
                          } catch (e) {}
                        },
                      ))),
              Positioned(
                top: deviceHeight * 0.18,
                left: deviceWidth * 0.12,
                right: deviceWidth * 0.12,
                child: widget.badgeType == "rosary_warrior"
                    ? Image.asset('assets/box-rw.png')
                    : Image.asset('assets/box-gs.png'),
              ),
              Positioned(
                top: deviceHeight * 0.26,
                left: deviceWidth * 0.25,
                right: deviceWidth * 0.25,
                child: widget.badgeType == "rosary_warrior"
                    ? Image.asset(
                        'assets/rosary_warrior.png',
                        height: deviceWidth * 0.55,
                      )
                    : Image.asset('assets/good_samaritan.png',
                        height: deviceWidth * 0.6),
              ),
              Positioned(
                bottom: deviceHeight * 0.14,
                left: deviceWidth * 0.20,
                right: deviceWidth * 0.20,
                child: Image.asset('assets/congrats.png'),
              ),
              Positioned(
                  bottom: deviceHeight * 0.07,
                  left: deviceWidth * 0.20,
                  right: deviceWidth * 0.20,
                  child: StreamBuilder(
                    stream: widget.badgeStream ??
                        FirebaseFirestore.instance
                            .collection('mem')
                            .doc('rosary')
                            .collection('users')
                            .doc(userBloc.userDetails.id)
                            .collection('badges')
                            .snapshots(),
                    builder: (BuildContext context, snapshot) {
                      try {
                        if (snapshot.hasError)
                          return new Text('${snapshot.error}');
                        if (snapshot.hasData && snapshot.data != null) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return Center(child: CircularProgressIndicator());

                            default:
                              widget.latestBadgeID =
                                  snapshot.data.documents.last['id'];
                              return Container(
                                  child: Center(
                                      child: Text(
                                "You Won a Badge \n" +
                                        snapshot.data.documents.last["title"]
                                            .toString()
                                            .toUpperCase() ??
                                    "",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16.0,
                                    color: Colors.white),
                                textAlign: TextAlign.center,
                              )));
                          }
                        } else
                          return Container();
                      } catch (e) {
                        return Container();
                      }
                    },
                  )),
              Positioned(
                  top: deviceHeight * 0.21,
                  left: deviceWidth * 0.17,
                  child: StreamBuilder(
                    stream: widget.badgeStream ??
                        FirebaseFirestore.instance
                            .collection('mem')
                            .doc('rosary')
                            .collection('users')
                            .doc(userBloc.userDetails.id)
                            .collection('badges')
                            .snapshots(),
                    builder: (BuildContext context, snapshot) {
                      try {
                        if (snapshot.hasError)
                          return new Text('${snapshot.error}');
                        if (snapshot.hasData && snapshot.data != null) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return Center(child: CircularProgressIndicator());

                            default:
                              widget.latestBadgeID =
                                  snapshot.data.documents.last['id'];
                              return Container(
                                  height: deviceHeight * 0.07,
                                  width: deviceHeight * 0.07,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image:
                                          AssetImage('assets/level-border.png'),
                                      fit: BoxFit.fitHeight,
                                    ),
                                  ),
                                  child: Center(
                                      child: Text(
                                    snapshot.data.documents.last["level"]
                                            .toString() ??
                                        "",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: 18.0,
                                        color: Colors.yellow),
                                    textAlign: TextAlign.center,
                                  )));
                          }
                        } else
                          return Container();
                      } catch (e) {
                        return Container();
                      }
                    },
                  )),
            ],
          )
        : Container();
  }
}
