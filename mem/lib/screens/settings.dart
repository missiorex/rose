// Copyright 2017 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:memapp/screens/add_edit_notification_screen.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import "package:flutter_local_notifications/flutter_local_notifications.dart";

final Map<String, Item> _items = <String, Item>{};
Item _itemForMessage(Map<String, dynamic> message) {
  final String itemId = message['data']['id'];
  final Item item = _items.putIfAbsent(itemId, () => new Item(itemId: itemId))
    ..status = message['data']['status'];
  return item;
}

class Item {
  Item({this.itemId});
  final String itemId;

  StreamController<Item> _controller = new StreamController<Item>.broadcast();
  Stream<Item> get onChanged => _controller.stream;

  String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<Null>> routes = <String, Route<Null>>{};
  Route<Null> get route {
    final String routeName = '/detail/$itemId';
    return routes.putIfAbsent(
      routeName,
      () => new MaterialPageRoute<Null>(
        settings: new RouteSettings(name: routeName),
        builder: (BuildContext context) => new DetailPage(itemId),
      ),
    );
  }
}

class DetailPage extends StatefulWidget {
  DetailPage(this.itemId);
  final String itemId;
  @override
  _DetailPageState createState() => new _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Item _item;
  StreamSubscription<Item> _subscription;

  @override
  void initState() {
    super.initState();
    _item = _items[widget.itemId];
    _subscription = _item.onChanged.listen((Item item) {
      if (!mounted) {
        _subscription.cancel();
      } else {
        setState(() {
          _item = item;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          title: new Text("Item ${_item.itemId}"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      body: new Material(
        child: new Center(child: new Text("Item status: ${_item.status}")),
      ),
    );
  }
}

class MemSettings extends StatefulWidget {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  MemSettings({this.flutterLocalNotificationsPlugin, Key key})
      : super(key: MemKeys.settingsScreen);
  @override
  _MemSettingsState createState() => new _MemSettingsState();
}

class _MemSettingsState extends State<MemSettings> {
  String _homeScreenText = "Waiting for token...";
//  bool _topicButtonsDisabled = false;

  String appName;
  String packageName;
  String version;
  String buildNumber;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final TextEditingController _topicController =
      new TextEditingController(text: 'topic');

  Widget _buildDialog(BuildContext context, Item item) {
    return new AlertDialog(
      content: new Text("Item ${item.itemId} has been updated"),
      actions: <Widget>[
        new FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        new FlatButton(
          child: const Text('SHOW'),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, _itemForMessage(message)),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  @override
  void initState() {
    super.initState();

    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        _homeScreenText = "Push Messaging token: $token";
      });
      print(_homeScreenText);
    });
  }

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = UserProvider.of(context);
    var container = AppStateContainer.of(context);

    return Scaffold(
        appBar: AppBar(
          title: const Text('Settings'),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          ),
        ),
        // For testing -- simulate a message being received
        floatingActionButton:
            userBloc.userDetails.isSuperAdmin && container.state.userAcl != null
                ? buildFloatingActionButton()
                : Container(),
        body: Material(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 40.0, left: 24.0, right: 24.0),
            children: <Widget>[
              Text("Notification Settings",
                  style:
                      TextStyle(fontSize: 17.0, fontWeight: FontWeight.w800)),

              Container(
                  margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Center(
                    child: Text(
                        """Following Notifications are enabled by default.To subscribe/un-subscribe to a particular notification, press the corresponsding subscribe or un-subscribe button"""),
                  )),
//              Center(
//                child: Text(_homeScreenText),
//              ),
              Container(padding: EdgeInsets.only(top: 10.0)),
              Card(
                  child:
                      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                Container(
                  child: Text(
                    "General Notifications: ",
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                  ),
                  padding: EdgeInsets.only(top: 20.0, bottom: 10.0),
                ),
                FlatButton(
                  child: Row(
                      // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(Icons.notifications_active),
                        Text(" Subscribe")
                      ]),
                  onPressed: () {
                    _firebaseMessaging.subscribeToTopic("five");
                    //_clearTopicText();
                  },
                ),
                FlatButton(
                  child: Row(
                      // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(Icons.notifications_off),
                        Text(" Un-Subscribe")
                      ]),
                  onPressed: () {
                    _firebaseMessaging.unsubscribeFromTopic("five");
                    _cancelAllNotifications();
                    // _clearTopicText();
                  },
                ),
              ])),
              Card(
                  child:
                      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                Container(
                  child: Text(
                    userBloc.userDetails.locale +
                            " Prayer & Community Chat Notifications: " ??
                        "English " + " Prayer & Community Chat Notifications: ",
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                  ),
                  padding: EdgeInsets.only(
                      left: 10.0, right: 10.0, top: 20.0, bottom: 10.0),
                ),
                FlatButton(
                  child: Row(
                      // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(Icons.notifications_active),
                        Text(" Subscribe")
                      ]),
                  onPressed: () {
                    _firebaseMessaging
                        .subscribeToTopic(userBloc.userDetails.locale);
                    //_clearTopicText();
                  },
                ),
                FlatButton(
                  child: Row(
                      // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(Icons.notifications_off),
                        Text(" Un-Subscribe")
                      ]),
                  onPressed: () {
                    _firebaseMessaging
                        .unsubscribeFromTopic(userBloc.userDetails.locale);
                    //_clearTopicText();
                  },
                ),
              ]))

//                  child:  TextField(
//                      controller: _topicController,
//                      onChanged: (String v) {
//                        setState(() {
//                          _topicButtonsDisabled = v.isEmpty;
//                        });
//                      }),
            ],
          ),
        ));
  }

  Widget buildFloatingActionButton() {
    var container = AppStateContainer.of(context);

    if (container.state.userAcl.localeAdmin) {
      return FloatingActionButton.extended(
        tooltip: 'Send A Notification',
        backgroundColor: Colors.blueGrey,
        icon: Icon(Icons.notifications_active),
        label: Text("Send A Notification"),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddEditNotificationScreen(
                  key: MemKeys.addEditNotificationScreen,
                ),
              ));
        },
      );
    } else
      return Container();
  }

  void _clearTopicText() {
    setState(() {
      _topicController.text = "";
      //_topicButtonsDisabled = true;
    });
  }

  Future _cancelAllNotifications() async {
    await widget.flutterLocalNotificationsPlugin.cancelAll();
  }
}
