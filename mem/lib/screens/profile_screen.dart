import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/widgets/profile_header.dart';
import 'package:utility/utility.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:path/path.dart' as p;
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/screens/profile_edit_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({key: Key, @required this.member, this.avatarTag, this.storage})
      : super(key: MemKeys.profileScreen);

  final MemUser member;
  final Object avatarTag;
  final FirebaseStorage storage;

  @override
  _ProfileScreenState createState() => new _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  File _image;
  Future<PickedFile> _imageFile;
  bool imageLoading = false;
  //SharedPreferences prefs;

  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      floatingActionButton: editProfileButton("Edit Profile",
          backgroundColor: Theme.of(context).buttonColor,
          textColor: Theme.of(context).primaryIconTheme.color),
      body: Container(
          height: MediaQuery.of(context).size.height,
          color: Theme.of(context).primaryColorLight,
          child: SingleChildScrollView(
            child: Container(
              color: Theme.of(context).primaryColorLight,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ProfileDetailHeader(
                      widget.member,
                      widget.avatarTag,
                      profilePhotoField("Upload Profile Photo",
                          backgroundColor: Theme.of(context).backgroundColor,
                          textColor: Theme.of(context).primaryIconTheme.color),
                      editProfileButton("Edit Profile",
                          backgroundColor: Theme.of(context).buttonColor,
                          textColor: Theme.of(context).primaryIconTheme.color),
                      _previewImage(),
                      imageLoading),
                ],
              ),
            ),
          )),
    );
  }

  Widget profilePhotoField(
    String text, {
    Color backgroundColor,
    Color textColor,
  }) {
    UserBloc userBloc = UserProvider.of(context);
    File _profileImage;

    return ClipRRect(
        borderRadius:
            BorderRadius.circular(MediaQuery.of(context).size.width * 0.1),
        child: Container(
            color: Theme.of(context).primaryColor,
            child: IconButton(
                iconSize: MediaQuery.of(context).size.width * 0.05,
                icon: Icon(Icons.camera_alt),
                color: backgroundColor,
                onPressed: () {
                  imageLoading = true;
                  setState(() {});
                  saveImageLocal(userBloc.userDetails.id).then((image) {
                    _profileImage = image;

                    _uploadFile(image, userBloc.userDetails.id)
                        .then((profilePhotoUrl) {
                      MemUser userDetails = userBloc.userDetails;

                      userDetails.profilePhotoUrl = profilePhotoUrl.toString();

                      userBloc.userRepository
                          .updateUser(userDetails.toEntity());

                      imageLoading = false;
                      setState(() {});

                      ///todo upload profile photo userBloc.userRepository.updateUser(user)
                    });
                  });
                })));
  }

  Widget editProfileButton(
    String text, {
    Color backgroundColor,
    Color textColor,
  }) {
    UserBloc userBloc = UserProvider.of(context);

    return FloatingActionButton(
        child: const Icon(Icons.edit),
        backgroundColor: backgroundColor,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProfileEditScreen(
                    key: MemKeys.profileEditScreen,
                    userBloc: userBloc,
                    storage: widget.storage,
                    member: widget.member),
              ));
        });
  }

  Future<String> _uploadFile(File _image, String uid) async {
    List<String> fileNameList = _image.path.split("/");

    String fileName = fileNameList[fileNameList.length - 1];

    final Reference ref = widget.storage.ref().child('profile').child(fileName);
    final UploadTask uploadTask = ref.putFile(
      _image,
      SettableMetadata(
          customMetadata: <String, String>{'iamgetype': 'profile'},
          contentType: 'image/jpeg'),
    );

    final String downloadUrl = await ref.getDownloadURL();

    return downloadUrl;
  }

  Future<File> saveImageLocal(String uid) async {
    _imageFile = ImagePicker().getImage(
        source: ImageSource.gallery, maxHeight: 200.0, maxWidth: 200.0);

    var pickedImage = await _imageFile;
    var image = File(pickedImage.path);
    // getting a directory path for saving
    final String path = await _localPath;

    // copy the file to a  path
    //final File newImage = await image.copy('$path/' + uid + '.jpeg');
    ImageProperties properties =
        await FlutterNativeImage.getImageProperties(image.path);
    File compressedFile = await FlutterNativeImage.compressImage(image.path,
        quality: 80,
        targetWidth: 200,
        targetHeight: (properties.height * 200 / properties.width).round());

    File profilePic = await compressedFile.copy(p.join(path, uid + ".jpeg"));

    setState(() {
      _image = profilePic;
    });

    return _image;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Widget _previewImage() {
    return FutureBuilder<PickedFile>(
        future: _imageFile,
        builder: (BuildContext context, AsyncSnapshot<PickedFile> snapshot) {
          if (imageLoading) return profilePhotoLoader();
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            //return Image.file(snapshot.data);
            return CircleAvatar(
              backgroundImage: Image.file(File(snapshot.data.path)).image,
              radius: 50.0,
            );
          } else if (snapshot.error != null) {
            return const Text(
              'Error picking image.',
              textAlign: TextAlign.center,
            );
          } else {
            return profilePhoto();
          }
        });
  }

  Widget profilePhotoLoader() {
    return FutureBuilder<ImageProvider>(
        future: _loadImage(),
        builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.active:
            case ConnectionState.none:
              return CircularProgressIndicator(
                  strokeWidth: 1.0,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black));
              break;
            case ConnectionState.done:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return CircleAvatar(
                    backgroundImage: snapshot.data,
                    //radius: MediaQuery.of(context).size.height * 0.2,
                    radius: 50.0,
                    child: CircularProgressIndicator(
                        strokeWidth: 1.0,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.black)));
              break;

            default:
              return Container();
          }
        });
  }

  Widget profilePhoto() {
    return FutureBuilder<ImageProvider>(
        future: _loadImage(),
        builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.active:
            case ConnectionState.none:
              return CircularProgressIndicator(
                  backgroundColor: Colors.white70, strokeWidth: 1.5);
              break;
            case ConnectionState.done:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return CircleAvatar(
                  backgroundImage: snapshot.data,
                  radius: 50.0,
                );
              break;

            default:
              return Container();
          }
        });
  }

  Future<ImageProvider> _loadImage() async {
    ImageProvider profilePhoto =
        CachedNetworkImageProvider(widget.member.profilePhotoUrl ?? "") ??
            NetworkImage(widget.member.profilePhotoUrl ?? "") ??
            AssetImage("assets/placeholder-face.png");

    return profilePhoto;
  }
}
