import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/app_state_container.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/screens/add_gospel_card_screen.dart';
import 'package:utility/utility.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cached_network_image/cached_network_image.dart';

const double _kMinFlingVelocity = 800.0;
const String _kGalleryAssetsPackage = 'flutter_gallery_assets';

enum GCTileStyle { imageOnly, oneLine, twoLine }

typedef BannerTapCallback = void Function(GospelCard gospelCard);

class GospelCardViewer extends StatefulWidget {
  const GospelCardViewer({Key key, this.gospelCard}) : super(key: key);

  final GospelCard gospelCard;

  @override
  _GospelCardViewerState createState() => _GospelCardViewerState();
}

class _GospelCardTitleText extends StatelessWidget {
  const _GospelCardTitleText(this.text);

  final String text;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Text(text),
    );
  }
}

class _GospelCardViewerState extends State<GospelCardViewer>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _flingAnimation;
  Offset _offset = Offset.zero;
  double _scale = 1.0;
  Offset _normalizedOffset;
  double _previousScale;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this)
      ..addListener(_handleFlingAnimation);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

// The maximum offset value is 0,0. If the size of this renderer's box is w,h
// then the minimum offset value is w - _scale * w, h - _scale * h.
  Offset _clampOffset(Offset offset) {
    final Size size = context.size;
    final Offset minOffset = Offset(size.width, size.height) * (1.0 - _scale);
    return Offset(
        offset.dx.clamp(minOffset.dx, 0.0), offset.dy.clamp(minOffset.dy, 0.0));
  }

  void _handleFlingAnimation() {
    setState(() {
      _offset = _flingAnimation.value;
    });
  }

  void _handleOnScaleStart(ScaleStartDetails details) {
    setState(() {
      _previousScale = _scale;
      _normalizedOffset = (details.focalPoint - _offset) / _scale;
      // The fling animation stops if an input gesture starts.
      _controller.stop();
    });
  }

  void _handleOnScaleUpdate(ScaleUpdateDetails details) {
    setState(() {
      _scale = (_previousScale * details.scale).clamp(1.0, 4.0);
      // Ensure that image location under the focal point stays in the same place despite scaling.
      _offset = _clampOffset(details.focalPoint - _normalizedOffset * _scale);
    });
  }

  void _handleOnScaleEnd(ScaleEndDetails details) {
    final double magnitude = details.velocity.pixelsPerSecond.distance;
    if (magnitude < _kMinFlingVelocity) return;
    final Offset direction = details.velocity.pixelsPerSecond / magnitude;
    final double distance = (Offset.zero & context.size).shortestSide;
    _flingAnimation = _controller.drive(Tween<Offset>(
      begin: _offset,
      end: _clampOffset(_offset + direction * distance),
    ));
    _controller
      ..value = 0.0
      ..fling(velocity: magnitude / 1000.0);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onScaleStart: _handleOnScaleStart,
      onScaleUpdate: _handleOnScaleUpdate,
      onScaleEnd: _handleOnScaleEnd,
      child: ClipRect(
        child: Transform(
          transform: Matrix4.identity()
            ..translate(_offset.dx, _offset.dy)
            ..scale(_scale),
          child: gospelCardImage(widget.gospelCard.cardImageUrl),
        ),
      ),
    );
  }
}

class GospelCardItem extends StatelessWidget {
  GospelCardItem({
    Key key,
    @required this.gospelCard,
    @required this.tileStyle,
    @required this.onBannerTap,
  })  : assert(gospelCard != null),
        assert(tileStyle != null),
        assert(onBannerTap != null),
        super(key: key);

  final GospelCard gospelCard;
  final GCTileStyle tileStyle;
  final BannerTapCallback
      onBannerTap; // User taps on the photo's header or footer.

  void showPhoto(BuildContext context) {
    Navigator.push(context,
        MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text(gospelCard.title),
        ),
        body: SizedBox.expand(
          child: Hero(
            tag: gospelCard.id,
            child: GospelCardViewer(gospelCard: gospelCard),
          ),
        ),
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    final Widget image = GestureDetector(
      onTap: () {
        showPhoto(context);
      },
      child: Hero(
          key: Key(gospelCard.id),
          tag: gospelCard.id,
          child: gospelCardImage(gospelCard.cardImageUrl)),
    );

//    final IconData icon = badge.isComplete ? Icons.star : Icons.star_border;

    switch (tileStyle) {
      case GCTileStyle.imageOnly:
        return image;

      case GCTileStyle.oneLine:
        return GridTile(
          footer: GestureDetector(
            onTap: () {
              onBannerTap(gospelCard);
            },
            child: GridTileBar(
              title: _GospelCardTitleText(gospelCard.title),
              backgroundColor: Colors.black45,
              leading: Icon(Icons.check),
//              leading: Icon(
//                icon,
//                color: Colors.white,
//              ),
            ),
          ),
          child: image,
        );

      case GCTileStyle.twoLine:
        return GridTile(
          header: GestureDetector(
            onTap: () {
              onBannerTap(gospelCard);
            },
            child: GridTileBar(
              backgroundColor: Colors.black45,
              title: _GospelCardTitleText(gospelCard.title),
              subtitle: _GospelCardTitleText(gospelCard.cardDetails),
              trailing: Icon(
                Icons.check,
                color: Colors.white,
              ),
            ),
          ),
          child: image,
        );
    }
    assert(tileStyle != null);
    return null;
  }
}

class GospelCardScreen extends StatefulWidget {
  const GospelCardScreen({Key key, @required this.storage}) : super(key: key);
  final FirebaseStorage storage;
  //static const String routeName = '/material/grid-list';

  @override
  GospelCardScreenState createState() => GospelCardScreenState();
}

class GospelCardScreenState extends State<GospelCardScreen> {
  GCTileStyle _tileStyle = GCTileStyle.oneLine;

  List<GospelCard> gospelCards;

  void changeTileStyle(GCTileStyle value) {
    setState(() {
      _tileStyle = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isVideoManager = false;
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    gospelCards = container.state.gospelCards;

    if (userBloc.userDetails.role == "admin" &&
        container.state.userAcl != null) {
      if (container.state.userAcl.addVideoTestimony) {
        isVideoManager = true;
      } else {
        isVideoManager = false;
      }
    }

    final Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Gospel Cards'),
        backgroundColor: Color.fromRGBO(11, 129, 140, 1.0),
        actions: <Widget>[
          PopupMenuButton<GCTileStyle>(
            onSelected: changeTileStyle,
            itemBuilder: (BuildContext context) => <PopupMenuItem<GCTileStyle>>[
              const PopupMenuItem<GCTileStyle>(
                value: GCTileStyle.imageOnly,
                child: Text('Image only'),
              ),
              const PopupMenuItem<GCTileStyle>(
                value: GCTileStyle.oneLine,
                child: Text('One line'),
              ),
              const PopupMenuItem<GCTileStyle>(
                value: GCTileStyle.twoLine,
                child: Text('Two line'),
              ),
            ],
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SafeArea(
              top: false,
              bottom: false,
              child: GridView.count(
                //crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
                crossAxisCount: 3,
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
                padding: const EdgeInsets.all(4.0),
                childAspectRatio:
                    (orientation == Orientation.portrait) ? 1.0 : 1.3,
                children: gospelCards.map<Widget>((GospelCard gospelCard) {
                  return GospelCardItem(
                    gospelCard: gospelCard,
                    tileStyle: _tileStyle,
                    onBannerTap: (GospelCard gospelCard) {
                      setState(() {});
                    },
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: isVideoManager
          ? FloatingActionButton.extended(
              tooltip: 'Add A Gospel Card',
              backgroundColor: Color.fromRGBO(100, 100, 100, 0.8),
              icon: Icon(Icons.person_pin),
              label: Text('Add A Gospel Card'.toUpperCase()),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (c) {
                      return AddGospelCardScreen(
                        storage: widget.storage,
                        key: MemKeys.addGospelCardScreen,
                      );
                    },
                  ),
                );
              })
          : Container(),
    );
  }
}

Widget gospelCardImage(String url) {
  return FutureBuilder<ImageProvider>(
      future: _loadGospelCardImage(url),
      builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
          case ConnectionState.active:
          case ConnectionState.none:
            return CircularProgressIndicator(
                strokeWidth: 3.0,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.black));
          case ConnectionState.done:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              return CircleAvatar(
                backgroundImage: snapshot.data,
                radius: 25.0,
              );
        }
      });
}

Future<ImageProvider> _loadGospelCardImage(String url) async {
  ImageProvider gospelCardImg = CachedNetworkImageProvider(url ?? "") ??
      NetworkImage(url ?? "") ??
      AssetImage("assets/chat_bg.png");

  return gospelCardImg;
}
