import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:memapp/widgets/rosary_count_button.dart';
import 'package:memapp/widgets/app_bar.dart';
import 'package:memapp/widgets/memLabel.dart';
import 'package:memapp/app_state_container.dart';
import 'package:utility/utility.dart';
import 'package:memapp/widgets/message_list.dart';
import 'package:memapp/widgets/drawer.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/screens/admin_dashboard.dart';
import 'package:memapp/screens/notification_dashboard.dart';
import 'package:memapp/screens/add_edit_rosary_list.dart';
import 'package:memapp/screens/add_edit_message_screen.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memapp/screens/member_list.dart';
import 'package:utility/src/dialogshower.dart' as DialogShower;
import 'package:memapp/screens/settings.dart';
import 'package:memapp/screens/help.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity/connectivity.dart';
import 'package:meta/meta.dart';
import "package:intl/intl.dart";
import 'package:memapp/screens/gift_verse.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memapp/screens/rewards_list_screen.dart';
import 'package:shimmer/shimmer.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:path_provider/path_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:ui' as ui;
import "package:flutter_local_notifications/flutter_local_notifications.dart";
import 'package:mem_entities/mem_entities.dart';
import 'package:memapp/chatroom/chat_screen.dart';
import 'package:share/share.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:memapp/widgets/RestartWidget.dart';
import 'package:memapp/widgets/bordered_text.dart';
import 'package:memapp/widgets/count_up.dart';
import 'package:memapp/widgets/popup/popuplib.dart';
import 'package:memapp/widgets/scratch_card.dart';
import 'package:memapp/screens/videos.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:memapp/widgets/logo_widget.dart';
import 'dart:math' as math;
import 'package:memapp/screens/consecration.dart';
import 'package:rive/rive.dart' as rive;
import 'package:memapp/widgets/pop_navigation_grid.dart';
import 'package:memapp/screens/screens.dart';

enum AppTab { home, requests, messages }

typedef NavigateToScreen();

// pop up pages for super admin user
final List<FacingPage> _popUpSuperAdminPages = <FacingPage>[
  FacingPage(
      label: 'My Rosary List',
      fabLabel: 'Rosary List',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Members List',
      fabLabel: 'Members List',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Admin Dashboard',
      fabLabel: 'Admin Dashboard',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Settings',
      fabLabel: 'Settings',
      colors: Colors.orange,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Contact Us',
      fabLabel: 'Contact Us',
      colors: Colors.orange,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Badges',
      fabLabel: 'Badges',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Share This App',
      fabLabel: 'Share This App',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.share,
      popUpElement: true),
];

// pop up pages for admin user
final List<FacingPage> _popUpAdminPages = <FacingPage>[
  FacingPage(
      label: 'My Rosary List',
      fabLabel: 'Rosary List',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Settings',
      fabLabel: 'Settings',
      colors: Colors.orange,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Contact Us',
      fabLabel: 'Contact Us',
      colors: Colors.orange,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Badges',
      fabLabel: 'Badges',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Share This App',
      fabLabel: 'Share This App',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.share,
      popUpElement: true),
];

// pop up pages for normal user
final List<FacingPage> _popUpUserPages = <FacingPage>[
  FacingPage(
      label: 'My Rosary List',
      fabLabel: 'Rosary List',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Settings',
      fabLabel: 'Settings',
      colors: Colors.orange,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Contact Us',
      fabLabel: 'Contact Us',
      colors: Colors.orange,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Badges',
      fabLabel: 'Badges',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Share This App',
      fabLabel: 'Share This App',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.share,
      popUpElement: true),
];

// All FAB pages
final List<FacingPage> _allPages = <FacingPage>[
  FacingPage(
      label: 'My Rosary List',
      fabLabel: 'Rosary List',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Rosary',
      fabLabel: 'Rosary',
      colors: Colors.deepOrange,
      tabIconData: Icons.home,
      fabIconData: Icons.add,
      popUpElement: false,
      unReadCount: 0.toString(),
      isEditingTitle: "Edit Rosary Count",
      isAddTitle: "Add Rosary Count",
      fabImgUrl: "assets/globe-icon.png"),
  FacingPage(
    label: 'Requests',
    fabLabel: 'Prayer Request',
    colors: Colors.green,
    tabIconData: Icons.record_voice_over,
    fabIconData: Icons.playlist_add,
    popUpElement: false,
    unReadCount: 0.toString(),
    isEditingTitle: "Edit Prayer Request",
    isAddTitle: "Add Prayer Request",
  ),
  FacingPage(
      label: 'Messages',
      fabLabel: 'Admin Messages',
      colors: Colors.teal,
      tabIconData: Icons.message,
      fabIconData: Icons.message,
      popUpElement: false,
      isEditingTitle: "Edit Admin Message",
      isAddTitle: "Add Admin Message"),
  FacingPage(
      label: 'Testimony',
      fabLabel: 'Testimony',
      unReadCount: 0.toString(),
      colors: Colors.teal,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: false,
      isEditingTitle: "Edit Testimony",
      isAddTitle: "Add Testimony"),
];

// The home screen.
class HomeScreen extends StatefulWidget {
  final AppState appState;
  final FirebaseStorage storage;
  final UserBloc userBloc;
  final MessageBloc messageBloc;
  final RosaryBloc rosaryBloc;
  var container;
  final RouteObserver<PageRoute> routeObserver;
  final String gospelMsg;
  final String navigateToScreen;

  HomeScreen(
      {Key key,
      this.userBloc,
      this.messageBloc,
      this.rosaryBloc,
      this.container,
      @required this.appState,
      @required this.storage,
      @required this.routeObserver,
      this.gospelMsg,
      this.navigateToScreen})
      : super(key: MemKeys.homeScreen);

  @override
  State createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  static final GlobalKey<FormState> rosaryFormKey = GlobalKey<FormState>();
  TabController _tabController;
  FacingPage _selectedPage;
  int latestRosaryCount, latestRequestCount = 0, latestTestimonyCount = 0;
  DateTime lastOpenDateRosary, lastOpenDateRequest, lastOpenDateTestimony;
  int lastOpenCommunityChat;
  int lastOpenAdminChat = DateTime.now().millisecondsSinceEpoch;
  DateTime lastOpenDateGuidance;
  bool _extendedButtons = false;
  VisibilityFilter activeFilter = VisibilityFilter.rosaryOnly;
  AppTab activeTab = AppTab.home;
  String selectedPrayer;
  bool _showBottomAdminBar = false;
  double _adminBarHeightLimit;
  double _adminBarHeight;
  Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _connectionStatus = false;
  bool _homeScreenActive = false;
  bool _showShortCutScreen = false;
  Animation _bibleAnimation;
  AnimationController _bibleAnimationController;
  StreamSubscription<QuerySnapshot> badgeStreamSub;

  List<ChatMessage> _messages = [];
  List<ChatMessage> _guidanceMessages = [];
  var lastVisibleKey;
  var lastVisibleValue;
  var lastVisibleGuidanceKey;
  var lastVisibleGuidanceValue;
  var end;
  List<StreamSubscription> listeners = []; // list of listeners
  StreamSubscription currentListener;
  Stream currentStream;
  Stream chatStream;
  Stream guidanceChatStream;
  FacingPage _selectedPopPage = _allPages[0]; // The app's "state".
  Duration timeout = const Duration(seconds: 2);
  // Duration ms = const Duration(milliseconds: 1);
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  bool _showTitle = false;
  DatabaseReference _messagesReference = FirebaseDatabase.instance.reference();
  RemoteConfig remoteConfig;
  StreamSubscription chatCountListener;
  AnimationController _controller;
  AnimationController _splashController;
  Animation<double> _splashAnimation;

  //Rive Animations
  rive.Artboard _riveArtboard;
  rive.RiveAnimationController _riveController;

  rive.Artboard _starArtboard;
  rive.StateMachineController _starController;
  rive.SMIInput<bool> _start;

  bool get isPlaying => _starController.isActive ?? false;

  //The latest total rosary count
  int kEndValue = 0;

  var _duration = Duration(seconds: 4);

  String _message = "";
  //TextEditingController msgController = TextEditingController();

  void _selectPopPage(FacingPage page) {
    setState(() {
      // Causes the app to rebuild with the new _selectedChoice.
      _selectedPopPage = page;
    });
  }

  @override
  void initState() {
    _adminBarHeight = _adminBarHeightLimit;
    latestRosaryCount = 0;

    //Loader set, to wait for app getting ready

    widget.appState.isLoading = true;

    //Start a timeout of 10 seconds to wait for app ready
    startTimeout(timeout);

    // initialise the Flutter Local Notification plugin. app_icon needs to be a added as a drawable resource to the Android head project
    var initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    // Initialise Remote Config
    initializeRemoteConfigs();

    //get the chat messages
    getFirstMessages();

    getFirstGuidanceMessages();

    super.initState();

    fillContainerFromDb();

    _controller = new AnimationController(vsync: this, duration: _duration);

    _controller.forward(from: 0.0);

    _splashController = AnimationController(
        duration: const Duration(milliseconds: 40000), vsync: this);

    _splashAnimation = Tween(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: _splashController,
        curve: Interval(0.0, 0.67, curve: Curves.easeOut),
      ),
    );

    chatStream = _messagesReference
        .child("chats")
        .child(widget.userBloc.userDetails.locale ?? "English")
        .orderByChild("time")
        .startAt(lastOpenCommunityChat)
        .onValue;

    guidanceChatStream = _messagesReference
        .child("chats")
        .child(widget.userBloc.userDetails.id)
        .orderByChild("time")
        .startAt(lastOpenAdminChat)
        .onValue;

    // Bible Verse Animation
    _bibleAnimationController = AnimationController(
        duration: Duration(milliseconds: 6000), vsync: this);

    final Animation<double> curve = CurvedAnimation(
        parent: _bibleAnimationController, curve: Curves.easeIn);

    _bibleAnimation = Tween(begin: 0.0, end: 400.0).animate(curve)
      ..addListener(() {
        setState(() {});
      });

    _bibleAnimationController.forward();

//Subscibing to connectivity
    initConnectivity();

    badgeStreamSub = FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(widget.userBloc.userDetails.id)
        .collection('badges')
        .where('action', isEqualTo: 'ADD ROSARY')
        .snapshots()
        .listen(onBadge);

    // Lstening to rosary count stream to activate animation.

    widget.rosaryBloc.rosaryTotalCount.listen((value) {
      _playRiveAnimation();
    });

//    msgStreamSub = Firestore.instance
//        .collection('mem')
//        .document('rosary')
//        .collection('messages')
//        .where('msgtype', isEqualTo: 'rosary')
//        .snapshots()
//        .listen(onMessage);

//    actionStreamSub = Firestore.instance
//        .collection('mem')
//        .document('rosary')
//        .collection('actions')
//        .orderBy('time')
//        .snapshots()
//        .listen(onAction);

//    userActionStreamSub = Firestore.instance
//        .collection('mem')
//        .document('rosary')
//        .collection('users')
//        .document(widget.userBloc.userDetails.id)
//        .collection('actions')
//        .orderBy('time')
//        .snapshots()
//        .listen(onUserAction);
//
//    adminMsgStreamSub = Firestore.instance
//        .collection('mem')
//        .document('groups')
//        .collection('regions')
//        .document(widget.userBloc.userDetails.region)
//        .collection('messages')
//        .where('msgtype', isEqualTo: 'admin')
//        .snapshots()
//        .listen(onAdminMessage);

    try {
      _connectivitySubscription = _connectivity.onConnectivityChanged
          .listen((ConnectivityResult result) {
        debugPrint("Connection Status from home: " + result.toString());
        setState(() {
          if (result == ConnectivityResult.mobile) {
            // I am connected to a mobile network.
            _connectionStatus = true;
          } else if (result == ConnectivityResult.wifi) {
            // I am connected to a wifi network.
            _connectionStatus = true;
          } else {
            _connectionStatus = false;
          }
        });
      });
    } on PlatformException catch (e) {
      setState(() {
        _connectionStatus = false;
      });
    }

    try {
      _tabController =
          new TabController(vsync: this, length: _allPages.length - 1);
      _tabController.addListener(_handleTabSelection);
      _selectedPage = _allPages[1];
    } catch (e) {
      debugPrint("Exception");
    }

    // _scrollController = ScrollController()..addListener(() => setState(() {}));

    //Initialize notification subscription
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {
        Navigator.pushNamed(context, '/message',
            arguments: MessageArguments(message, true));
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null && !kIsWeb) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                memChannel.id,
                memChannel.name,
                memChannel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      Navigator.pushNamed(context, '/message',
          arguments: MessageArguments(message, true));
    });

    //Rive Animations
    // Load the animation file from the bundle, note that you could also
    // download this. The RiveFile just expects a list of bytes.
    rootBundle.load('assets/rive/addRosary.riv').then(
      (data) async {
        // Load the RiveFile from the binary data.
        final file = rive.RiveFile.import(data);

        // The artboard is the root of the animation and gets drawn in the
        // Rive widget.
        final artboard = file.mainArtboard;
        // Add a controller to play back a known animation on the main/default
        // artboard. We store a reference to it so we can toggle playback.
        artboard.addController(rive.SimpleAnimation('idle'));
        setState(() => _riveArtboard = artboard);
      },
    );

    //Rive Animation
    rootBundle.load('assets/rive/star_blast.riv').then(
      (data) async {
        // Load the RiveFile from the binary data.
        final file = rive.RiveFile.import(data);

        // The artboard is the root of the animation and  gets drawn in the
        // Rive widget.
        final artboard = file.mainArtboard;
        // Add a controller to play back a known animation on the main/default
        // artboard. We store a reference to it so we can toggle playback.
        var controller =
            rive.StateMachineController.fromArtboard(artboard, 'star_state');

        if (controller != null) {
          artboard.addController(controller);
          setState(() => _start = controller.findInput('addRosary'));
        }

        setState(() => _starArtboard = artboard);
      },
    );
  }

  void _playRiveAnimation() {
    _start?.value = true;
    startRiveTimeout(Duration(seconds: 4));
  }

  // Timeout for setting the rosaryUpdateCount to false (Rive Animation Stop)
  startRiveTimeout(Duration timeout) {
    //var duration = milliseconds == null ? timeout : ms * milliseconds;
    return Timer(timeout, handleRiveTimeout);
  }

  // After 10 Sec, stop the rosary Update animation.
  void handleRiveTimeout() {
    // callback function
    _start?.value = false;
  }

  void getFirstMessages() {
    var first;
    int count = 0;
    first = _messagesReference
        .child("chats")
        .child(widget.userBloc.userDetails.locale ?? "English")
        .orderByChild("time")
        .limitToLast(20);
    debugPrint("Count of Chat messages: " + count.toString());

    currentListener = first.onChildAdded.listen((Event event) {
      count++;
      debugPrint("Count of Chat messages: " + count.toString());
      setState(() {
        if (count == 1) {
          lastVisibleKey = event.snapshot.key;
          lastVisibleValue = event.snapshot.value['time'];
          debugPrint("last message: " +
              lastVisibleKey.toString() +
              " " +
              lastVisibleValue.toString() +
              event.snapshot.value['text'].toString());
        }
        if (count > 0) {
          debugPrint("Fetching messages");

          var val = event.snapshot.value;

          var msgType = val['type'] ?? 'chat';
          var msgRegion = val['region'] ?? 'all';

          if (msgType == 'admin' &&
                  msgRegion == widget.userBloc.userDetails.region ||
              msgRegion == "All") {
            _addMessage(
                id: val['id'],
                name: val['sender']['displayName'] ?? val['sender']['name'],
                senderImageUrl: val['sender']['imageUrl'],
                senderId: val['sender']['id'],
                text: val['text'],
                time: val['time'],
                imageUrl: val['imageUrl'],
                textOverlay: val['textOverlay'],
                addToHead: true,
                public: true);
          } else if (msgType != 'admin') {
            _addMessage(
                id: val['id'],
                name: val['sender']['displayName'] ?? val['sender']['name'],
                senderImageUrl: val['sender']['imageUrl'],
                senderId: val['sender']['id'],
                text: val['text'],
                time: val['time'],
                imageUrl: val['imageUrl'],
                textOverlay: val['textOverlay'],
                addToHead: true,
                public: true);
          }
        }
      });
    });
    listeners.add(currentListener);
  }

  void getFirstGuidanceMessages() {
    var first;
    int count = 0;
    first = _messagesReference
        .child("chats")
        .child(widget.userBloc.userDetails.id)
        .orderByChild("time")
        .limitToLast(20);
    debugPrint("Count of Guidance Chat messages: " + count.toString());

    currentListener = first.onChildAdded.listen((Event event) {
      count++;
      debugPrint("Count of Chat messages: " + count.toString());
      setState(() {
        if (count == 1) {
          lastVisibleGuidanceKey = event.snapshot.key;
          lastVisibleGuidanceValue = event.snapshot.value['time'];
          debugPrint("last message: " +
              lastVisibleGuidanceKey.toString() +
              " " +
              lastVisibleGuidanceValue.toString() +
              event.snapshot.value['text'].toString());
        }
        if (count > 0) {
          debugPrint("Fetching messages");

          var val = event.snapshot.value;

          _addMessage(
              id: val['id'],
              name: val['sender']['displayName'] ?? val['sender']['name'],
              senderImageUrl: val['sender']['imageUrl'],
              senderId: val['sender']['id'],
              text: val['text'],
              time: val['time'],
              imageUrl: val['imageUrl'],
              textOverlay: val['textOverlay'],
              addToHead: true,
              public: false);
        }
      });
    });
    listeners.add(currentListener);
  }

  void _addMessage(
      {String id,
      String name,
      String text,
      int time,
      String imageUrl,
      String textOverlay,
      String senderImageUrl,
      String senderId,
      bool addToHead,
      bool public}) {
    var animationController = AnimationController(
      duration: Duration(milliseconds: 700),
      vsync: this,
    );
    var sender = ChatUser(name: name, imageUrl: senderImageUrl, id: senderId);
    var message = ChatMessage(
        id: id,
        sender: sender,
        text: text,
        time: time,
        imageUrl: imageUrl,
        textOverlay: textOverlay,
        animationController: animationController);
    setState(() {
      if (public) {
        if (addToHead)
          _messages.insert(0, message);
        else
          _messages.insert(_messages.length - 1, message);

        _messages.sort((a, b) => b.time.compareTo(a.time));
      } else {
        if (addToHead)
          _guidanceMessages.insert(0, message);
        else
          _guidanceMessages.insert(_guidanceMessages.length - 1, message);

        _guidanceMessages.sort((a, b) => b.time.compareTo(a.time));
      }
    });
    try {
      if (imageUrl != null) {
        NetworkImage image = NetworkImage(imageUrl);
        //Stable Channel
//        image
//            .resolve(createLocalImageConfiguration(context))
//            .addListener((_, __) {
//          animationController?.forward();
//        });
        //Master Channel _ Flutter
        image.resolve(createLocalImageConfiguration(context)).addListener(
            ImageStreamListener((ImageInfo image, bool synchronousCall) {
          animationController?.forward();
        }));
      } else {
        animationController?.forward();
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<Null> initConnectivity() async {
    bool connectionStatus;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile) {
        // I am connected to a mobile network.
        connectionStatus = true;
      } else if (connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a wifi network.
        connectionStatus = true;
      } else {
        connectionStatus = false;
      }
    } on PlatformException catch (e) {
      print(e.toString());
      connectionStatus = false;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return;
    }

    setState(() {
      _connectionStatus = connectionStatus;
    });
  }

  void fillContainerFromDb() {
    widget.rosaryBloc.rewardsRepository
        .badges(widget.userBloc.userDetails.id)
        .listen((result) {
      widget.container.state.badges = result.map(Badge.fromEntity).toList();

      debugPrint("Badges: " + widget.container.state.badges.toString());
    }).onError((error) {
      debugPrint("Error in getting badges from firebase" + error.toString());
    });

    widget.messageBloc.messageRepository
        .gospelCards(
            widget.userBloc.userDetails.id, widget.userBloc.userDetails.locale)
        .listen((result) {
      widget.container.state.gospelCards =
          result.map(GospelCard.fromEntity).toList();

      debugPrint(
          "Gospel Cards: " + widget.container.state.gospelCards.toString());
    }).onError((error) {
      debugPrint(
          "Error in getting Gospel Cards from firebase" + error.toString());
    });

    widget.messageBloc.messageRepository
        .benefactorMessages()
        .listen((bmessages) {
      List<Benefactor> _bmessages =
          bmessages.map(Benefactor.fromEntity).toList();
      widget.container.state.benefactorMessages = _bmessages;
    }).onError((error) {
      debugPrint("Error in getting benefactor message data from firebase" +
          error.toString());
    });

    widget.messageBloc.messageRepository
        .videoTestimonies()
        .listen((vtestimonies) {
      List<VideoTestimony> _vtestimonies =
          vtestimonies.map(VideoTestimony.fromEntity).toList();
      widget.container.state.videoTestimonies = _vtestimonies;
    }).onError((error) {
      debugPrint("Error in getting Video Testimonies data from firebase" +
          error.toString());
    });
  }

  void _handleTabSelection() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    UserBloc userBloc = UserProvider.of(context);

    prefs.setInt(_selectedPage.label.toString() + "_last_open",
        DateTime.now().millisecondsSinceEpoch);

    setState(() {
      _selectedPage = _allPages[_tabController.index + 1];
      debugPrint("Selected Page: " +
          _selectedPage.label.toString() +
          DateTime.now().toString());
    });

    if (_selectedPage.label != "Home" && userBloc.userDetails.role == "guest") {
      setState(() {
        //_selectedPage = _allPages[1];
        _tabController.index = 0;

        _registerNow();
      });
    }
  }

  void selectAddPage() {
    switch (_selectedPage.label) {
      case "Home":
        Navigator.pushNamed(context, MemRoutes.addRosary);
        break;

      case "Requests":
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddEditMessageScreen(
                key: MemKeys.addMessageScreen,
                msgType: "request",
                selectedPage: _selectedPage,
                remoteConfig: remoteConfig,
              ),
            ));

        break;
      case "Messages":
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddEditMessageScreen(
                  key: MemKeys.addMessageScreen,
                  msgType: "admin",
                  remoteConfig: remoteConfig,
                  selectedPage: _selectedPage),
            ));

        break;
      case "Testimony":
        //Navigator.pushNamed(context, MemRoutes.addTestimony);
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddEditMessageScreen(
                  key: MemKeys.addMessageScreen,
                  msgType: "testimony",
                  remoteConfig: remoteConfig,
                  selectedPage: _selectedPage),
            ));
        break;
      default:
        //Navigator.pushNamed(context, MemRoutes.addRosary);
        break;
    }
  }

  Widget buildFloatingActionButton(FacingPage page, AppState appState) {
    final rosaryBloc = RosaryProvider.of(context);
    final messageBloc = MessageProvider.of(context);
    final userBloc = UserProvider.of(context);
    var fabIconSize = MediaQuery.of(context).size.width * 0.09;
    Size screenSize = MediaQuery.of(context).size;

    if (!page.fabDefined) return null;

    if (_selectedPage.label == 'Messages' &&
        appState.userDetails.role == "user") {
      return Container();
    } else if (_selectedPage.label == 'Messages' &&
        appState.userDetails.role == "admin" &&
        appState.userAcl != null) {
      if (appState.userAcl.regionalAdmin) {
        return new FloatingActionButton(
            key: new ValueKey<Key>(page.fabKey),
            tooltip: 'Regional Messages',
            //backgroundColor: page.fabColor,
            child: ImageIcon(
              AssetImage('assets/icons/regional-message-icon.png'),
              size: fabIconSize,
            ),
            onPressed: () {
              ///To collect Spritual Guidance
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddEditMessageScreen(
                        key: MemKeys.addMessageScreen,
                        msgType: "admin",
                        remoteConfig: remoteConfig,
                        selectedPage: _selectedPage),
                  ));
            });
      }
    }

    if (_extendedButtons) {
      return new FloatingActionButton.extended(
          key: new ValueKey<Key>(page.fabKey),
          tooltip: 'Add Message',
          //backgroundColor: page.fabColor,
          icon: page.fabIcon,
          label: new Text(page.fabLabel.toUpperCase()),
          onPressed: () {
            setState(() {
              selectAddPage();
            });
          });
    }

    if (page.label == "Rosary") {
      return null;
    } else if (page.label == "Requests") {
      return new FloatingActionButton(
        key: page.fabKey,
        tooltip: 'Add Message',
        //backgroundColor: page.fabColor,
        child: ImageIcon(
          AssetImage('assets/icons/prayer-request.png'),
          size: fabIconSize,
        ),
        onPressed: () {
          if (mounted) {
            setState(() {
              selectAddPage();
            });
          }
        },
      );
    } else if (page.label == "Testimony") {
      return new FloatingActionButton(
        key: page.fabKey,
        tooltip: 'Add Testimony',
        //backgroundColor: page.fabColor,
        child: ImageIcon(
          AssetImage('assets/icons/thanksgiving.png'),
          size: fabIconSize,
        ),
        onPressed: () {
          if (mounted) {
            setState(() {
              selectAddPage();
            });
          }
        },
      );
    }
  }

  Future<bool> _exitApp(BuildContext context) {
    var dialog = AlertDialog(
      title: Text('We hope to see you back soon, God bless you...'),
      content: Text('Do you want to exit this application?'),
      actions: <Widget>[
        new FlatButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: new Text('No'),
        ),
        new FlatButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: new Text('Yes'),
        ),
      ],
    );
    return showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  @override
  Widget build(BuildContext context) {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    getUnreadChatCount();
    getUnreadAdminChatCount();

    //Navigate to a screen if navigation button already pressed.

    //Subscribe by default to Five Continents Prayer & Locale for locale based notifications.
    if (userBloc.userDetails.role != "guest") {
      FirebaseMessaging.instance.subscribeToTopic("five");
      FirebaseMessaging.instance
          .subscribeToTopic(userBloc.userDetails.locale ?? "English");
      container.state.userDetails.role == "admin"
          ? FirebaseMessaging.instance.subscribeToTopic("new_user")
          : null;
      FirebaseMessaging.instance.getToken().then((String token) {
        assert(token != null);

        userBloc.userRepository
            .updatePushNotificationToken(token, userBloc.userDetails.id);
      });

//      _showWeeklyNotification();
    }
    Widget home = _pageToDisplay;

    debugPrint("Unread Guidance Count:" +
        container.state.unreadGuidanceReqCount.toString());

    return WillPopScope(
        onWillPop: () => _exitApp(context),
        child: _showShortCutScreen ? _popMessageView : home);
  }

  Widget get _homeView {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    return Scaffold(
      backgroundColor: Color.fromRGBO(100, 100, 100, 0.8),
      body: _homeBody,
      drawer: MemDrawer(
        storage: widget.storage,
        connectionStatus: _connectionStatus,
        routeObserver: widget.routeObserver,
      ),
      floatingActionButton:
          buildFloatingActionButton(_selectedPage, container.state),
      bottomNavigationBar: userBloc.userDetails.role == "admin"
          ? _adminBottomNavigationBar
          : userBloc.userDetails.role == "user"
              ? _userBottomNaviagtionBar
              : null,
    );
  }

  Widget get _adminBottomNavigationBar {
    UserBloc userBloc = UserProvider.of(context);
    RosaryBloc rosaryBloc = RosaryProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var offerRosaryIconSize = MediaQuery.of(context).size.width * 0.13;
    var bottomBarIconSize = MediaQuery.of(context).size.width * 0.08;
    var iconFontSize = MediaQuery.of(context).size.width * 0.03;
    var container = AppStateContainer.of(context);

    return SafeArea(
        left: false,
        top: false,
        right: false,
        bottom: true,
        minimum: const EdgeInsets.only(bottom: 0.0),
        child: Theme(
            data: Theme.of(context).copyWith(
                // sets the background color of the `BottomNavigationBar`
                canvasColor: Theme.of(context).backgroundColor,
                // sets the active color of the `BottomNavigationBar` if `Brightness` is light
                primaryColor: Color.fromRGBO(164, 138, 58, 1.0),
                textTheme: Theme.of(context).textTheme.copyWith(
                    caption: TextStyle(
                        color: Theme.of(context)
                            .primaryIconTheme
                            .color))), // sets the inactive color of the `BottomNavigationBar`
            child: Container(
                decoration: BoxDecoration(boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      offset: new Offset(0.5, 0.5),
                      blurRadius: 3.0,
                      spreadRadius: 3.0)
                ]),
                child: BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  currentIndex: 0,
                  onTap: ((index) {
                    switch (index) {
                      case 0:
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NotificationScreen(
                                    storage: widget.storage,
                                    key: MemKeys.notificationScreen,
                                    routeObserver: widget.routeObserver,
                                    guidanceMessages: _guidanceMessages,
                                    lastVisibleChatValue:
                                        lastVisibleGuidanceValue,
                                    lastVisibleChatKey: lastVisibleGuidanceKey,
                                    connectionStatus: _connectionStatus)));
                        break;
                      case 1:
                        UserBloc userBloc = UserProvider.of(context);

                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ChatScreen(
                                userBloc: userBloc,
                                bgImageUrl: "assets/images/bg-pattern-1.jpg",
                                connectionStatus: _connectionStatus,
                                publicChat: true,
                                chatRoomID: userBloc.userDetails.locale,
                                fcmNotificationEnable: remoteConfig
                                    .getBool('fcm_notifcation_enable'),
                                routeObserver: widget.routeObserver,
                                messages: _messages,
                                lastVisibleChatKey: lastVisibleKey,
                                lastVisibleChatValue: lastVisibleValue,
                              ),
                            ));
                        break;
                      case 2:
                        if (_connectionStatus) {
                          if (userBloc.userDetails.role == "guest") {
                            _registerNow();
                          } else
                            openAddRosaryBottomSheet(
                                rosaryBloc, userBloc, messageBloc);
                        } else {
                          showError(
                              errorMsg: MemError(
                                  title: "Not Connected",
                                  body:
                                      "Please check your internet connection. You can submit Rosary only when you are online."));
                        }

                        break;
                      case 3:
                        userBloc.userDetails.isSuperAdmin
                            ? Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NotificationScreen(
                                        storage: widget.storage,
                                        key: MemKeys.notificationScreen,
                                        routeObserver: widget.routeObserver,
                                        guidanceMessages: _guidanceMessages,
                                        lastVisibleChatValue:
                                            lastVisibleGuidanceValue,
                                        lastVisibleChatKey:
                                            lastVisibleGuidanceKey,
                                        connectionStatus: _connectionStatus)))
                            : Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => VideoScreen(),
                                ));
                        break;
                      case 4:
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NotificationScreen(
                                    storage: widget.storage,
                                    key: MemKeys.notificationScreen,
                                    routeObserver: widget.routeObserver,
                                    guidanceMessages: _guidanceMessages,
                                    lastVisibleChatValue:
                                        lastVisibleGuidanceValue,
                                    lastVisibleChatKey: lastVisibleGuidanceKey,
                                    connectionStatus: _connectionStatus)));
                        break;
                      default:
                        break;
                    }
                  }),
                  // this will be set when a new tab is tapped
                  items: [
                    BottomNavigationBarItem(
                      icon: Stack(children: <Widget>[
                        ImageIcon(AssetImage('assets/icons/icons8-users.png'),
                            color: Theme.of(context).primaryIconTheme.color,
                            size: bottomBarIconSize),
                        Positioned(
                          // draw a red marble
                          top: 0.0,
                          right: 0.0,
                          child: container.state.unreadNewUserReqCount > 0
                              ? new Icon(Icons.brightness_1,
                                  size: 8.0, color: Colors.redAccent)
                              : Container(),
                        )
                      ]),
                      title: Text('Users',
                          style: TextStyle(
                            fontSize: iconFontSize,
                            color: Theme.of(context).primaryIconTheme.color,
                          )),
                    ),
                    BottomNavigationBarItem(
                      icon: StreamBuilder(
                          stream: chatStream,
                          builder: (BuildContext context, snapshot) {
                            return Stack(children: <Widget>[
                              ImageIcon(
                                AssetImage('assets/icons/community.png'),
                                size: bottomBarIconSize,
                                color: Theme.of(context).primaryIconTheme.color,
                              ),
                              Positioned(
                                // draw a red marble
                                top: 0.0,
                                right: 0.0,
                                child: container.state.unreadChatMsgCount > 0
                                    ? container.state.unreadChatMsgCount < 100
                                        ? Icon(Icons.brightness_1,
                                            size: 16.0, color: Colors.redAccent)
                                        : Container(
                                            width: 20.0,
                                            height: 10.0,
                                            decoration: new BoxDecoration(
                                                color: Colors.redAccent,
                                                borderRadius: BorderRadius.all(
                                                    const Radius.circular(
                                                        5.0))),
                                          )
                                    : Container(),
                              ),
                              container.state.unreadChatMsgCount > 0 &&
                                      container.state.unreadChatMsgCount < 100
                                  ? Positioned(
                                      // draw a red marble
                                      top: 3.0,
                                      right: 3.0,
                                      child: Text(
                                        container.state.unreadChatMsgCount
                                            .toString(),
                                        style: TextStyle(
                                            fontSize: 8.0,
                                            fontWeight: FontWeight.w800,
                                            color: Colors.white),
                                        textAlign: TextAlign.center,
                                      ))
                                  : Positioned(
                                      // draw a red marble
                                      top: 1.0,
                                      right: 3.0,
                                      child: container
                                                  .state.unreadChatMsgCount >=
                                              100
                                          ? Text(
                                              "99+",
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  fontWeight: FontWeight.w800,
                                                  color: Colors.white),
                                              textAlign: TextAlign.center,
                                            )
                                          : Container(),
                                    )
                            ]);
                          }),
                      title: Text('MEM',
                          style: TextStyle(
                            fontSize: iconFontSize,
                            color: Theme.of(context).primaryIconTheme.color,
                          )),
                    ),
                    BottomNavigationBarItem(
                      title: Container(),
                      icon: Container(
                        child: ConstrainedBox(
                          constraints: BoxConstraints.tightFor(
                              height: offerRosaryIconSize,
                              width: offerRosaryIconSize * 0.69),
                          child: Center(
                            child: _riveArtboard == null
                                ? Ink.image(
                                    image: AssetImage(
                                        'assets/heart-rosary-icon.png'),
                                    fit: BoxFit.fill,
                                    child: InkWell(
                                      onTap: null,
                                    ))
                                : rive.Rive(artboard: _riveArtboard),
                          ),
                        ),
                      ),
                    ),
                    userBloc.userDetails.isSuperAdmin &&
                            container.state.userAcl != null
                        ? container.state.userAcl.viewCounselling
                            ? BottomNavigationBarItem(
                                title: Text('Help',
                                    style: TextStyle(
                                      fontSize: iconFontSize,
                                      color: Theme.of(context)
                                          .primaryIconTheme
                                          .color,
                                    )),
                                icon: new Stack(children: <Widget>[
                                  ImageIcon(
                                    AssetImage(
                                        'assets/icons/counselling-icon.png'),
                                    size: bottomBarIconSize,
                                    color: Theme.of(context)
                                        .primaryIconTheme
                                        .color,
                                  ),
                                  new Positioned(
                                    // draw a red marble
                                    top: 0.0,
                                    right: 0.0,
                                    child:
                                        container.state.unreadGuidanceReqCount >
                                                0
                                            ? new Icon(Icons.brightness_1,
                                                size: 6.0,
                                                color: Colors.redAccent)
                                            : Container(),
                                  )
                                ]),
                              )
                            : BottomNavigationBarItem(
                                icon: Icon(Icons.dashboard,
                                    size: bottomBarIconSize),
                                title: Text("Dashboard",
                                    style: TextStyle(
                                      fontSize: iconFontSize,
                                      color: Theme.of(context)
                                          .primaryIconTheme
                                          .color,
                                    )))
                        : BottomNavigationBarItem(
                            title: Text(
                              'Videos',
                              style: TextStyle(
                                fontSize: iconFontSize,
                                color: Theme.of(context).primaryIconTheme.color,
                              ),
                            ),
                            icon: Stack(children: <Widget>[
                              ImageIcon(
                                AssetImage('assets/icons/video.png'),
                                size: bottomBarIconSize,
                                color: Theme.of(context).primaryIconTheme.color,
                              ),
                            ]),
                          ),
                    BottomNavigationBarItem(
                        icon: Stack(children: <Widget>[
                          ImageIcon(
                            AssetImage('assets/icons/icons8-error.png'),
                            size: bottomBarIconSize,
                            color: Theme.of(context).primaryIconTheme.color,
                          ),
                          Positioned(
                            // draw a red marble
                            top: 0.0,
                            right: 0.0,
                            child: container.state.unreadRosaryLimitsCount > 0
                                ? new Icon(Icons.brightness_1,
                                    size: 8.0, color: Colors.redAccent)
                                : Container(),
                          )
                        ]),
                        title: Text('Limits',
                            style: TextStyle(
                              fontSize: iconFontSize,
                              color: Theme.of(context).primaryIconTheme.color,
                            )))
                  ],
                ))));
  }

  Widget get _userBottomNaviagtionBar {
    UserBloc userBloc = UserProvider.of(context);
    RosaryBloc rosaryBloc = RosaryProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var offerRosaryIconSize = MediaQuery.of(context).size.width * 0.13;
    var bottomBarIconSize = MediaQuery.of(context).size.width * 0.08;
    var iconFontSize = MediaQuery.of(context).size.width * 0.03;
    var container = AppStateContainer.of(context);
    return SafeArea(
        left: false,
        top: false,
        right: false,
        bottom: true,
        minimum: const EdgeInsets.only(bottom: 0.0),
        child: Theme(
            data: Theme.of(context).copyWith(
                // sets the background color of the `BottomNavigationBar`
                canvasColor: Theme.of(context).backgroundColor,
                // sets the active color of the `BottomNavigationBar` if `Brightness` is light
                primaryColor: Color.fromRGBO(164, 138, 58, 1.0),
                textTheme: Theme.of(context).textTheme.copyWith(
                    caption: TextStyle(
                        color: Color.fromRGBO(164, 138, 58,
                            1.0)))), // sets the inactive color of the `BottomNavigationBar`
            child: Container(
                decoration: BoxDecoration(boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      offset: new Offset(0.5, 0.5),
                      blurRadius: 3.0,
                      spreadRadius: 3.0)
                ]),
                child: BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  //backgroundColor: Colors.transparent,
                  currentIndex: 0,
                  onTap: ((index) {
                    switch (index) {
                      case 0:
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ChatScreen(
                                userBloc: userBloc,
                                bgImageUrl: "assets/bg-admin.jpg",
                                connectionStatus: _connectionStatus,
                                publicChat: false,
                                chatRoomID: userBloc.userDetails.id,
                                fcmNotificationEnable: remoteConfig
                                    .getBool('fcm_notifcation_enable'),
                                routeObserver: widget.routeObserver,
                                messages: _guidanceMessages,
                                lastVisibleChatKey: lastVisibleGuidanceKey,
                                lastVisibleChatValue: lastVisibleGuidanceValue,
                              ),
                            ));

                        break;
                      case 1:
                        if (_connectionStatus) {
                          if (userBloc.userDetails.role == "guest") {
                            _registerNow();
                          } else
                            openAddRosaryBottomSheet(
                                rosaryBloc, userBloc, messageBloc);
                        } else {
                          showError(
                              errorMsg: MemError(
                                  title: "Not Connected",
                                  body:
                                      "Please check your internet connection. You can submit Rosary only when you are online."));
                        }

                        break;

                      case 2:
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ChatScreen(
                                userBloc: userBloc,
                                bgImageUrl: "assets/images/bg-pattern-1.jpg",
                                connectionStatus: _connectionStatus,
                                publicChat: true,
                                chatRoomID: userBloc.userDetails.locale,
                                fcmNotificationEnable: remoteConfig
                                    .getBool('fcm_notifcation_enable'),
                                routeObserver: widget.routeObserver,
                                messages: _messages,
                              ),
                            ));

                        break;

                      default:
                        break;
                    }
                  }),
                  // this will be set when a new tab is tapped
                  items: [
                    BottomNavigationBarItem(
                      icon: StreamBuilder(
                          stream: guidanceChatStream,
                          builder: (BuildContext context, snapshot) {
                            return Stack(children: <Widget>[
                              ImageIcon(AssetImage('assets/icons/admin.png'),
                                  color:
                                      Theme.of(context).primaryIconTheme.color,
                                  size: bottomBarIconSize),
                              new Positioned(
                                // draw a red marble
                                top: 0.0,
                                right: 0.0,
                                child: container.state.unreadAdminChatMsgCount >
                                        0
                                    ? container.state.unreadAdminChatMsgCount <
                                            100
                                        ? Icon(Icons.brightness_1,
                                            size: 16.0, color: Colors.redAccent)
                                        : Container(
                                            width: 20.0,
                                            height: 10.0,
                                            decoration: new BoxDecoration(
                                                color: Colors.redAccent,
                                                borderRadius: BorderRadius.all(
                                                    const Radius.circular(
                                                        5.0))),
                                          )
                                    : Container(),
                              ),
                              container.state.unreadAdminChatMsgCount > 0 &&
                                      container.state.unreadAdminChatMsgCount <
                                          100
                                  ? Positioned(
                                      // draw a red marble
                                      top: 3.0,
                                      right: 3.0,
                                      child: Text(
                                        "",
                                        style: TextStyle(
                                            fontSize: 8.0,
                                            fontWeight: FontWeight.w800,
                                            color: Colors.white),
                                        textAlign: TextAlign.center,
                                      ))
                                  : Positioned(
                                      // draw a red marble
                                      top: 1.0,
                                      right: 3.0,
                                      child: container.state
                                                  .unreadAdminChatMsgCount >=
                                              100
                                          ? Text(
                                              "",
//                                                        "99+",
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  fontWeight: FontWeight.w800,
                                                  color: Colors.white),
                                              textAlign: TextAlign.center,
                                            )
                                          : Container(),
                                    )
                            ]);
                          }),
                      title: Text('Admin',
                          style: TextStyle(
                            fontSize: iconFontSize,
                            color: Theme.of(context).primaryIconTheme.color,
                          )),
                    ),

                    BottomNavigationBarItem(
                      title: Container(),
                      icon: Container(
                        child: ConstrainedBox(
                          constraints: BoxConstraints.tightFor(
                              height: offerRosaryIconSize,
                              width: offerRosaryIconSize),
                          child: Center(
                            child: _riveArtboard == null
                                ? Ink.image(
                                    image: AssetImage(
                                        'assets/heart-rosary-icon.png'),
                                    fit: BoxFit.fill,
                                    child: InkWell(onTap: null),
                                  )
                                : rive.Rive(artboard: _riveArtboard),
                          ),
                        ),
                      ),
                    ),
                    BottomNavigationBarItem(
                      icon: StreamBuilder(
                          stream: _messagesReference
                              .child("chats")
                              .child(widget.userBloc.userDetails.locale ??
                                  "English")
                              .orderByChild("time")
                              .startAt(lastOpenCommunityChat)
                              .onValue,
                          builder: (BuildContext context, snapshot) {
                            return Stack(children: <Widget>[
                              ImageIcon(
                                AssetImage('assets/icons/community.png'),
                                size: bottomBarIconSize,
                                color: Theme.of(context).primaryIconTheme.color,
                              ),
                              new Positioned(
                                // draw a red marble
                                top: 0.0,
                                right: 0.0,
                                child: container.state.unreadChatMsgCount > 0
                                    ? container.state.unreadChatMsgCount < 100
                                        ? Icon(Icons.brightness_1,
                                            size: 16.0, color: Colors.redAccent)
                                        : Container(
                                            width: 20.0,
                                            height: 10.0,
                                            decoration: new BoxDecoration(
                                                color: Colors.redAccent,
                                                borderRadius: BorderRadius.all(
                                                    const Radius.circular(
                                                        5.0))),
                                          )
                                    : Container(),
                              ),
                              container.state.unreadChatMsgCount > 0 &&
                                      container.state.unreadChatMsgCount < 100
                                  ? Positioned(
                                      // draw a red marble
                                      top: 3.0,
                                      right: 3.0,
                                      child: Text(
                                        container.state.unreadChatMsgCount
                                            .toString(),
                                        style: TextStyle(
                                            fontSize: 8.0,
                                            fontWeight: FontWeight.w800,
                                            color: Colors.white),
                                        textAlign: TextAlign.center,
                                      ))
                                  : Positioned(
                                      // draw a red marble
                                      top: 1.0,
                                      right: 3.0,
                                      child: container
                                                  .state.unreadChatMsgCount >=
                                              100
                                          ? Text(
                                              "99+",
                                              style: TextStyle(
                                                  fontSize: 8.0,
                                                  fontWeight: FontWeight.w800,
                                                  color: Colors.white),
                                              textAlign: TextAlign.center,
                                            )
                                          : Container(),
                                    )
                            ]);
                          }),
                      title: new Text('MEM',
                          style: TextStyle(
                            fontSize: iconFontSize,
                            color: Theme.of(context).primaryIconTheme.color,
                          )),
                    ),
//                    BottomNavigationBarItem(
//                      title: Text(
//                        'Badges',
//                        style: TextStyle(
//                          fontSize: iconFontSize,
//                          color: Theme.of(context).primaryIconTheme.color,
//                        ),
//                      ),
//                      icon: Stack(children: <Widget>[
//                        ImageIcon(
//                          AssetImage('assets/icons/icons8-badge.png'),
//                          size: bottomBarIconSize,
//                          color: Theme.of(context).primaryIconTheme.color,
//                        ),
//                      ]),
//                    ),
//                    BottomNavigationBarItem(
//                      title: Text(
//                        'Videos',
//                        style: TextStyle(
//                            fontSize: iconFontSize,
//                            color: Theme.of(context).primaryIconTheme.color),
//                      ),
//                      icon: Stack(children: <Widget>[
//                        ImageIcon(
//                          AssetImage('assets/icons/video.png'),
//                          size: bottomBarIconSize,
//                          color: Theme.of(context).primaryIconTheme.color,
//                        ),
//                      ]),
//                    )
                  ],
                ))));
  }

  Widget get _homeBody {
    final rosaryBloc = RosaryProvider.of(context);
    final container = AppStateContainer.of(context);
    final userBloc = UserProvider.of(context);
    final messageBloc = MessageProvider.of(context);
    final Paint foreColorTab = Paint();
    foreColorTab.color = Theme.of(context).backgroundColor;

    var tabBarFontSize = MediaQuery.of(context).size.width * 0.033;
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    return RefreshIndicator(
        onRefresh: _refreshMessages,
        child: NestedScrollView(
          key: MemKeys.filterButton,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: MediaQuery.of(context).size.height * 0.30,
                titleSpacing: 2.0,
                centerTitle: true,
                floating: true,
                pinned: true,
                title: _showTitle
                    ? Container(
                        margin: EdgeInsets.only(right: 15.0),
                        child: Text(
                          "MARIAN EUCHARISTIC MINISTRY",
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w500,
                            fontFamily: "Impact",
                            color: Colors.white54,
                            //color: Color.fromRGBO(41, 147, 178, 1.0)
                          ),
                        ))
                    : null,
                backgroundColor: Theme.of(context).backgroundColor,
                flexibleSpace: FlexibleSpaceBar(
                    collapseMode: CollapseMode.parallax,
                    centerTitle: true,
                    //title: _showTitle ? null : _titleBar,
                    background: _mainHeader),
                actions: <Widget>[
                  //FlatButton(child: Text(userBloc.userDetails.region)),
                  StreamBuilder<int>(
                      stream: rosaryBloc.rosaryTotalCount,
                      initialData: 0,
                      builder: (context, snapshot) => RosaryCountButton(
                          key: MemKeys.unreadCountButton,
                          rosaryCount: snapshot.data,
                          onPressed: _playRiveAnimation)),
                  IconButton(
                      tooltip: "Quick Links",
                      icon: Icon(Icons.home_outlined,
                          color: Theme.of(context).primaryIconTheme.color),
                      onPressed: () {
                        if (!_showShortCutScreen) {
                          setState(() {
                            _showShortCutScreen = true;
                          });
                        }
                      }),
                  userBloc.userDetails.role != "guest"
                      ? PopupMenuButton<FacingPage>(
                          // overflow menu

                          onSelected: _selectPopPage,
                          itemBuilder: (BuildContext context) {
                            List<FacingPage> _popUpPages =
                                container.state.userDetails.role == "admin"
                                    ? container.state.userDetails.isSuperAdmin
                                        ? _popUpSuperAdminPages
                                        : _popUpAdminPages
                                    : _popUpUserPages;

                            return _popUpPages
                                .where((FacingPage page) => (page.popUpElement))
                                .map((FacingPage page) {
                              return PopupMenuItem<FacingPage>(
                                  value: page,
                                  child: SizedBox(
                                      child: FlatButton(
                                          textTheme: ButtonTextTheme.normal,
                                          onPressed: () {
                                            Navigator.pop(context);
                                            switch (page.label) {
                                              case 'My Rosary List':
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => RosaryListScreen(
                                                          connectionStatus:
                                                              _connectionStatus,
                                                          fcmNotificationEnable:
                                                              remoteConfig.getBool(
                                                                  'fcm_notifcation_enable'),
                                                          routeObserver: widget
                                                              .routeObserver,
                                                          rosaryBloc:
                                                              rosaryBloc,
                                                          userBloc: userBloc,
                                                          container: container,
                                                          badgeStream: FirebaseFirestore
                                                              .instance
                                                              .collection('mem')
                                                              .doc('rosary')
                                                              .collection(
                                                                  'users')
                                                              .doc(userBloc
                                                                  .userDetails
                                                                  .id)
                                                              .collection(
                                                                  'badges')
                                                              .where('action',
                                                                  isEqualTo:
                                                                      'COLLECT ROSARY')
                                                              .snapshots(),
                                                          key: MemKeys
                                                              .rosaryListScreen)),
                                                );
                                                break;
                                              case 'Badges':
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          RewardsListScreen()),
                                                );
                                                break;

                                              case 'Members List':
                                                if (container.state.userDetails
                                                        .role ==
                                                    "admin") {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              MembersListScreen(
                                                                  storage: widget
                                                                      .storage,
                                                                  key: MemKeys
                                                                      .memberList)));
                                                } else {
                                                  showError(
                                                      errorMsg: MemError(
                                                          title:
                                                              "You are not an Admin",
                                                          body:
                                                              "You are not allowed to access Members List"));
                                                }

                                                break;

                                              case 'Admin Dashboard':
                                                if (container.state.userDetails
                                                        .isSuperAdmin
                                                    //Todo
                                                    //Temporaily disabling acl check.
//                                                    &&
//                                                    container.state.userAcl !=
//                                                        null

                                                    ) {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) => AdminScreen(
                                                              storage: widget
                                                                  .storage,
                                                              connectionStatus:
                                                                  _connectionStatus,
                                                              fcmNotificationEnable:
                                                                  remoteConfig.getBool(
                                                                      'fcm_notifcation_enable'),
                                                              routeObserver: widget
                                                                  .routeObserver,
                                                              guidanceMessages:
                                                                  _guidanceMessages,
                                                              lastVisibleChatKey:
                                                                  lastVisibleGuidanceKey,
                                                              lastVisibleChatValue:
                                                                  lastVisibleGuidanceValue,
                                                              key: MemKeys
                                                                  .adminScreen)));
                                                } else {
                                                  showError(
                                                      errorMsg: MemError(
                                                          title:
                                                              "You are not an Admin",
                                                          body:
                                                              "You are not allowed to access the admin dashboard"));
                                                }

                                                break;

                                              case 'Settings':
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => MemSettings(
                                                          flutterLocalNotificationsPlugin:
                                                              flutterLocalNotificationsPlugin,
                                                          key: MemKeys
                                                              .settingsScreen)),
                                                );
                                                break;

                                              case 'Contact Us':
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          MemHelp(
                                                              key: MemKeys
                                                                  .helpScreen)),
                                                );
                                                break;
                                              case 'Share This App':
                                                final String shareURL = Theme
                                                                .of(context)
                                                            .platform ==
                                                        TargetPlatform.iOS
                                                    ? "Download the Marian Eucharistic Ministry App for iOS from : " +
                                                        remoteConfig.getString(
                                                            'appstore_url')
                                                    : "Download the Marian Eucharistic Ministry App for Android from : " +
                                                        remoteConfig.getString(
                                                            'playstore_url');

                                                final RenderBox box =
                                                    context.findRenderObject();
                                                Share.share(shareURL,
                                                    sharePositionOrigin:
                                                        box.localToGlobal(
                                                                Offset.zero) &
                                                            box.size);
                                                break;
                                            }
                                          },
                                          child: Text(page.label))));
                            }).toList();
                          },
                        )
                      : Container(width: 20.0),
                ],
              ),
              PreferredSize(
                  preferredSize: Size(MediaQuery.of(context).size.width,
                      MediaQuery.of(context).size.height),
                  child: SliverPersistentHeader(
                    delegate: SliverAppBarDelegate(
                      TabBar(
                        unselectedLabelStyle: TextStyle(
                          fontSize: tabBarFontSize * 1.05,
                          fontWeight: FontWeight.w800,
                          foreground: foreColorTab,
                        ),
                        labelColor: Theme.of(context).backgroundColor,
                        labelPadding: EdgeInsets.only(
                          top: tabBarFontSize * 0.1,
                          bottom: tabBarFontSize * 0.1,
                          left: tabBarFontSize * 0.005,
                          right: tabBarFontSize * 0.4,
                        ),
                        controller: _tabController,
                        indicatorSize: TabBarIndicatorSize.tab,
//                        indicator: new BubbleTabIndicator(
//                          padding: EdgeInsets.all(tabBarFontSize * 0.3),
//                          indicatorHeight: tabBarFontSize * 2.2,
//                          //indicatorColor: Color.fromRGBO(231, 158, 22, 1.0),
//                          indicatorColor: Theme.of(context).backgroundColor,
//                          tabBarIndicatorSize: TabBarIndicatorSize.tab,
//                          indicatorRadius: tabBarFontSize * 0.3,
//                        ),
                        tabs: _allPages
                            .where((FacingPage page) => (!page.popUpElement))
                            .map((FacingPage page) => Tab(
                                  text: page.label,
                                ))
                            .toList(),
                        labelStyle: TextStyle(
                            fontSize: tabBarFontSize * 1.05,
                            fontWeight: FontWeight.w800,
                            color: Theme.of(context).accentColor),
                      ),
                      "assets/memapp_icon.png",
                    ),
                    pinned: true,
                    floating: false,
                  )),
            ];
          },
          body: TabBarView(
            controller: _tabController,
            children: [
              Column(
                key: MemKeys.homeTab,
                //modified

                children: <Widget>[
                  MessageList(
                      filteredMessages: container.state.rosaryMessages,
                      //     filteredMessages: container.state.rosaryMessages,
                      messageBloc: messageBloc,
                      bgImageUrl: "assets/chat_bg.png",
                      connectionStatus: _connectionStatus,
                      msgType: "rosary",
                      routeObserver: widget.routeObserver),
                ], //new
              ),
              Column(
                key: MemKeys.requestTab,
                //modified
                children: <Widget>[
                  MessageList(
                      messageBloc: messageBloc,
                      bgImageUrl: "assets/chat_bg.png",
                      connectionStatus: _connectionStatus,
                      msgType: "request",
                      routeObserver: widget.routeObserver),
                ], //new
              ),
              Column(
                key: MemKeys.messageTab,
                //modified
                children: <Widget>[
                  //gospelCardCarousel(),
                  MessageList(
                      messageBloc: messageBloc,
                      userBloc: userBloc,
                      msgType: "admin",
                      bgImageUrl: "assets/chat_bg.png",
                      connectionStatus: _connectionStatus,
                      routeObserver: widget.routeObserver),
                ], //new
              ),
              Column(
                key: MemKeys.testimonyTab,
                children: <Widget>[
                  MessageList(
                      messageBloc: messageBloc,
                      bgImageUrl: "assets/chat_bg.png",
                      connectionStatus: _connectionStatus,
                      msgType: "testimony",
                      routeObserver: widget.routeObserver),
                ], //new
              ),
            ],
          ),
        ));
  }

  Widget get _loadingView {
    return new Center(
      child: new CircularProgressIndicator(),
    );
  }

  Widget get _popMessageView {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    return Scaffold(
//      appBar: AppBar(
//          leading: Container(),
//          elevation: 0.0,
//          backgroundColor: Theme.of(context).backgroundColor,
//          actions: [
////            widget.appState.isLoading
////                ? Container()
////                :
//            IconButton(
//                tooltip: "Go to Home Screen",
//                icon: Icon(Icons.arrow_forward, color: Colors.white),
//                onPressed: () {
//                  setState(() {
//                    _showShortCutScreen = false;
//                  });
//                })
//          ]),
      body: PopNavigationGrid(
        openBottomAddRosarySheet: _addRosaryBottomSheetCallBack,
        collectRosary: _collectRosaryCallBack,
        rosaryCount: rosaryCount,
        addPrayerRequest: _addPrayerRequestCallBack,
        addTestimony: _addTestimonyCallBack,
        communityChat: _communityChatCallBack,
        guidanceChat: _guidanceChatCallBack,
        videos: _videosCallBack,
        consecration: _consecrationCallBack,
        navigationReady: true,
        home: _goToHomeScreen,
      ),
//      ListView(
////          mainAxisSize: MainAxisSize.min,
////          mainAxisAlignment: MainAxisAlignment.center,
//          children: [

//            Container(
//                width: MediaQuery.of(context).size.width,
//                child: Center(
//                    child: IconButton(
//                        tooltip: "Go to Home Screen",
//                        icon: Icon(Icons.arrow_forward, color: Colors.white),
//                        onPressed: () => _goToHomeScreen)))
//          ]

      backgroundColor: Theme.of(context).backgroundColor,
    );
  }

  Widget get rosaryCount {
    RosaryBloc rosaryBloc = RosaryProvider.of(context);

    return StreamBuilder<int>(
      stream: rosaryBloc.rosaryTotalCount,
      initialData: 0,
      builder: (context, snapshot) => Countup(
          animation: new StepTween(
        begin: snapshot.data,
        end: snapshot.data,
      ).animate(_controller)),
    );
  }

  Widget get _pageToDisplay {
    //return _homeView;

    if (widget.appState.isLoading) {
      return _popMessageView;
    } else {
      return _homeView;
    }
  }

  Widget get _mainHeader {
    Size screenSize = MediaQuery.of(context).size;
    RosaryBloc rosaryBloc = RosaryProvider.of(context);

    return Stack(children: [
      Positioned(
          top: 0.0,
          right: 0.0,
          bottom: 0.0,
          left: 0.0,
          child: Container(
            width: screenSize.width,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(0, -1.0),
                end: Alignment(
                    0.0, 1.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Color(0xFFb60503),
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      Positioned(
        top: 0.0,
        right: 0.0,
        left: 0.0,
        bottom: 0.0,
        child: _starArtboard == null
            ? Container(
                height: 100.0,
              )
            : SizedBox(
                width: screenSize.width,
                height: screenSize.height * 0.3,
                child: rive.Rive(artboard: _starArtboard)),
      ),
      Positioned(
          top: 0.0,
          right: 0.0,
          bottom: 0.0,
          left: 0.0,
          child: Container(
              margin: EdgeInsets.only(
                  top: screenSize.height * 0.07,
                  bottom: screenSize.width * 0.06,
                  right: screenSize.width * 0.2,
                  left: screenSize.width * 0.2),
              child: Image.asset(
                "assets/memapp_icon.png",
                fit: BoxFit.contain,
              ))),
//      Positioned(
//          top: screenSize.height * 0.22,
//          left: 0.0,
//          bottom: 0.0,
//          right: screenSize.width * 0.4,
//          child: Container(
////            margin: EdgeInsets.only(
////                bottom: screenSize.height * 0.06, left: screenSize.width * 0.4),
////            margin: EdgeInsets.only(
////                bottom: screenSize.height * 0.06, left: screenSize.width * 0.5),
////            child: IconButton(
////                icon: Icon(Icons.home, color: Theme.of(context).primaryColor),
////                onPressed: () {}),
//            child: StreamBuilder<int>(
//                stream: rosaryBloc.rosaryTotalCount,
//                initialData: 0,
//                builder: (context, snapshot) => RosaryCountButton(
//                    key: MemKeys.unreadCountButton,
//                    rosaryCount: snapshot.data,
//                    onPressed: _playRiveAnimation)),
//          )),
    ]);
  }

  Widget get _titleBar {
    var mainTitleFontSize = MediaQuery.of(context).size.width * 0.04;
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;
    RosaryBloc rosaryBloc = RosaryProvider.of(context);

    return isTablet
        ? Stack(children: <Widget>[
            Container(height: 1.0, width: MediaQuery.of(context).size.width),
            Align(
                alignment: Alignment.topRight,
                child: Container(
                    margin: EdgeInsets.only(right: 200.0, top: 140.0),
                    height: 110.0,
                    width: 110.0,
                    child: StreamBuilder<int>(
                        stream: rosaryBloc.rosaryTotalCount,
                        initialData: 0,
                        builder: (context, snapshot) => RosaryCountButton(
                            key: MemKeys.unreadCountButton,
                            rosaryCount: snapshot.data,
                            onPressed: _playRiveAnimation)))),
            Positioned(
                bottom: 0.0,
                left: MediaQuery.of(context).size.width * 0.33,
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(children: <Widget>[
                      Container(
                          child: Text(
                        "MARIAN EUCHARISTIC",
                        style: TextStyle(
                            fontSize: mainTitleFontSize,
                            //fontWeight: FontWeight.bold,
                            fontFamily: "Impact",
                            color: Color.fromRGBO(41, 147, 178, 1.0)),
                      )),
                      Text(
                        'MINISTRY',
                        style: TextStyle(
                            fontSize: mainTitleFontSize * 0.7,
                            fontFamily: "Impact",
                            color: Color.fromRGBO(60, 59, 50, 1.0)),
                      ),
                    ])))
          ])
        : Container(
            padding: EdgeInsets.only(top: mainTitleFontSize * 0.1),
            height: mainTitleFontSize * 2.0,
            child: Stack(children: <Widget>[
              Container(
                  child: Text(
                "MARIAN EUCHARISTIC",
                style: TextStyle(
                    fontSize: mainTitleFontSize,
                    //fontWeight: FontWeight.bold,
                    fontFamily: "Impact",
                    color: Color.fromRGBO(41, 147, 178, 1.0)),
              )),
              Positioned(
                // draw a red marble
                top: mainTitleFontSize * 1.05,
                left: mainTitleFontSize * 3.0,
                child: Text(
                  'MINISTRY',
                  style: TextStyle(
                      fontSize: mainTitleFontSize * 0.7,
                      fontFamily: "Impact",
                      color: Color.fromRGBO(60, 59, 50, 1.0)),
                ),
              ),
            ]));
  }

  //Adds a new row for adding rosary by others
  void _addOthersRow(String text) {}

  _updateVisibility(VisibilityFilter filter) {
    setState(() {
      activeFilter = filter;
    });
  }

  _updateTab(AppTab tab) {
    setState(() {
      activeTab = tab;
    });
  }

//Refresh All messages
  Future<void> _refreshMessages() async {
    print('refreshing...');
    final userBloc = UserProvider.of(context);
    final rosaryBloc = RosaryProvider.of(context);
    final messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);

    DateTime lastOpenDateGuidance;
    DateTime lastOpenNewUserNotification;
    DateTime lastOpenDateRosaryLimitNotification;
    //Initialise user state if user already once logged in.
    SharedPreferences.getInstance().then((prefs) {
      lastOpenDateGuidance = DateTime.fromMillisecondsSinceEpoch(
          prefs.getInt("guidance" + "_last_open") ??
              DateTime.now().millisecondsSinceEpoch);

      lastOpenNewUserNotification = DateTime.fromMillisecondsSinceEpoch(
          prefs.getInt("new_user" + "_last_open") ??
              DateTime.now().millisecondsSinceEpoch);

      lastOpenDateRosaryLimitNotification = DateTime.fromMillisecondsSinceEpoch(
          prefs.getInt("rosary_limits" + "_last_open") ??
              DateTime.now().millisecondsSinceEpoch);
    });

    setState(() {
//      userBloc.userRepository.locales().listen((_locales) {
//        container.state.locales = _locales;
//        debugPrint("locales from splash" + _locales.toString());
//      });
//
//      userBloc.userRepository.regions().listen((_regions) {
//        container.state.regions = _regions;
//        debugPrint("regions from splash" + _regions.toString());
//      });
//
//      Stream<List<MessageEntity>> msgList =
//          messageBloc.messageRepository.guidanceMessages();
//      msgList.listen((result) {
//        container.state.guidanceMessages =
//            result.map(MEMmessage.fromEntity).toList();
//
//        container.state.unreadGuidanceReqCount = container
//            .state.guidanceMessages
//            .where((i) =>
//                i.type == "guidance" && i.time.isAfter(lastOpenDateGuidance))
//            .length;
//      }).onError((error) {
//        debugPrint(
//            "Error in getting  messages from firebase" + error.toString());
//      });

      rosaryBloc.rewardsRepository
          .badges(userBloc.userDetails.id)
          .listen((result) {
        container.state.badges = result.map(Badge.fromEntity).toList();

        debugPrint("Badges: " + container.state.badges.toString());
      }).onError((error) {
        debugPrint("Error in getting badges from firebase" + error.toString());
      });

      userBloc.userActive(userBloc.userDetails.id).then((isActive) {
        if (isActive) {
          //Get the rosary List of user
          List<RosaryCount> savedRosaryList;

          rosaryBloc.rosaryRepository.fetchRosary().then((rosary) {
            rosaryBloc.rosaryRepository
                .rosaryCountList(container.state.user.uid)
                .listen((rosaryList) {
              savedRosaryList = rosaryList.isNotEmpty
                  ? rosaryList.map(RosaryCount.fromEntity).toList()
                  : null;
              container.state.rosaryList = savedRosaryList;
            });
          });

          messageBloc.messageRepository
              .gospelCards(userBloc.userDetails.id, userBloc.userDetails.locale)
              .listen((result) {
            container.state.gospelCards =
                result.map(GospelCard.fromEntity).toList();

            debugPrint(
                "Gospel Cards: " + container.state.gospelCards.toString());
          }).onError((error) {
            debugPrint("Error in getting Gospel Cards from firebase" +
                error.toString());
          });

          //ACL List 1.9.823 onwards
          userBloc.userRepository
              .userRole(userBloc.userDetails.id)
              .then((role) {
            if (role == "admin") {
              userBloc.userRepository
                  .getUserAcl(userBloc.userDetails.id)
                  .then((userAcl) {
                container.state.userAcl = Acl.fromEntity(userAcl);
              });

              userBloc.userRepository
                  .isSuperAdmin(userBloc.userDetails.id)
                  .then((isSuperAdmin) {
                if (isSuperAdmin) {
                  userBloc.userRepository.users().listen((members) {
                    List<MemUser> _members =
                        members.map(MemUser.fromEntity).toList();
                    container.state.members = _members;
                  }).onError((error) {
                    debugPrint("Error in getting members data from firebase" +
                        error.toString());
                  });

                  Stream<List<MessageEntity>> guidanceMessages =
                      messageBloc.messageRepository.guidanceMessages();
                  guidanceMessages.listen((result) {
                    container.state.guidanceMessages =
                        result.map(MEMmessage.fromEntity).toList();

                    container.state.unreadGuidanceReqCount = container
                        .state.guidanceMessages
                        .where((i) =>
                            i.type == "guidance" &&
                            i.time.isAfter(lastOpenDateGuidance))
                        .length;
                  }).onError((error) {
                    debugPrint("Error in getting  messages from firebase" +
                        error.toString());
                  });
                }
              });
            } else {
              container.state.userAcl = Acl(
                  id: "",
                  name: "",
                  grantAdminRole: false,
                  revokeAdminRole: false,
                  grantSuperAdminRole: false,
                  revokeSuperAdminRole: false,
                  activateUser: false,
                  deactivateUser: false,
                  blockUser: false,
                  unBlockUser: false,
                  deleteUser: false,
                  regionalAdmin: false,
                  localeAdmin: false,
                  viewCounselling: false,
                  rosaryDelete: false,
                  messageDelete: false,
                  addVideoTestimony: false,
                  addBishopsMessage: false,
                  editAboutUsContent: false);
            }
          });

          rosaryBloc.rosaryRepository.fetchRosary().then((rosary) {
            rosaryBloc.rosaryRepository
                .rosaryCountList(container.state.user.uid)
                .listen((rosaryList) {
              savedRosaryList = rosaryList.isNotEmpty
                  ? rosaryList.map(RosaryCount.fromEntity).toList()
                  : null;
              container.state.rosaryList = savedRosaryList;

              //ACL List 1.9.823 onwards

//                    userBloc.userRepository.acl().listen((aclList) {
//                      List<Acl> _aclList = aclList.map(Acl.fromEntity).toList();
//                      container.state.aclList = _aclList;
//                      debugPrint("ACL List from spalsh: " + aclList.toString());
//                    }).onError((error) {
//                      debugPrint("Error in getting ACL List from firebase" +
//                          error.toString());
//                    });

              messageBloc.messageRepository
                  .getLatestGospelCard()
                  .then((gospelCard) {
                container.state.latestGospelCard = gospelCard.cardDetails;
              });
            }).onError((error) {
              debugPrint(
                  "Error in getting data from firebase" + error.toString());
            });
          });
        }
      });

//      //Listening to the regional messages stream.
//      messageBloc.messageRepository
//          .regionalMessages(userBloc.userDetails.region)
//          .listen((result) {
//        container.state.regionalMessages =
//            result.map(Message.fromEntity).toList();
//      }).onError((error) {
//        debugPrint("Error in getting regional messages from firebase" +
//            error.toString());
//      });

//      messageBloc.messageRepository
//          .localeMessages(userBloc.userDetails.locale)
//          .listen((result) {
//        container.state.localeMessages =
//            result.map(MEMmessage.fromEntity).toList();
//
//        debugPrint("Locale Messages (from splash):" +
//            container.state.localeMessages.toString());
//      }).onError((error) {
//        debugPrint("Error in getting locale messages from firebase" +
//            error.toString());
//      });
    });
  }

  Future<int> getUnreadChatCount() async {
    UserBloc userBloc = UserProvider.of(context);
    var container = AppStateContainer.of(context);
    var _chatQuery;
    int count = 0;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Get the latest community chat open time
//    lastOpenCommunityChat = DateTime.fromMillisecondsSinceEpoch(
//        prefs.getInt(userBloc.userDetails.locale + "_community_last_open") ??
//            DateTime.now().millisecondsSinceEpoch);

    lastOpenCommunityChat =
        prefs.getInt(userBloc.userDetails.locale + "_community_last_open") ??
            DateTime.now().millisecondsSinceEpoch;
    _chatQuery = _messagesReference
        .child("chats")
        .child(widget.userBloc.userDetails.locale ?? "English")
        .orderByChild("time")
        .startAt(lastOpenCommunityChat);

    chatCountListener = _chatQuery.onChildAdded.listen((Event event) {
      count++;
      if (count >= 0) container.state.unreadChatMsgCount = count;
    });

    _chatQuery.onChildRemoved.listen((Event event) {
      debugPrint("Count from chat message loop " + count.toString());
      count--;

      if (count >= 0) container.state.unreadChatMsgCount = count;
    });
    debugPrint("Last open chat screen : " +
        DateTime.fromMillisecondsSinceEpoch(lastOpenCommunityChat).toString());
    //setState(() {});

    return Future.value(count);
  }

  Future<int> getUnreadAdminChatCount() async {
    UserBloc userBloc = UserProvider.of(context);
    var container = AppStateContainer.of(context);
    var _chatQuery;
    int count = 0;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Get the latest community chat open time
//    lastOpenCommunityChat = DateTime.fromMillisecondsSinceEpoch(
//        prefs.getInt(userBloc.userDetails.locale + "_community_last_open") ??
//            DateTime.now().millisecondsSinceEpoch);

    lastOpenAdminChat =
        prefs.getInt(userBloc.userDetails.id + "_admin_chat_last_open");

    _chatQuery = _messagesReference
        .child("chats")
        .child(widget.userBloc.userDetails.id)
        .orderByChild("time")
        .startAt(lastOpenAdminChat);

    _chatQuery.onChildAdded.listen((Event event) {
      count++;
      if (count >= 0) container.state.unreadAdminChatMsgCount = count;
    });

    _chatQuery.onChildRemoved.listen((Event event) {
      debugPrint("Count from chat message loop " + count.toString());
      count--;

      if (count >= 0) container.state.unreadAdminChatMsgCount = count;
    });
//    debugPrint("Last open Admin chat screen : " +
//        DateTime.fromMillisecondsSinceEpoch(lastOpenAdminChat).toString());
    //setState(() {});

    return Future.value(count);
  }

  Future getLatestMsgCount(String msgType) async {
    var container = AppStateContainer.of(context);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    lastOpenDateRosary = DateTime.fromMillisecondsSinceEpoch(
        prefs.getInt("Rosary" + "_last_open") ??
            DateTime.now().millisecondsSinceEpoch);
    lastOpenDateRequest = DateTime.fromMillisecondsSinceEpoch(
        prefs.getInt("Requests" + "_last_open") ??
            DateTime.now().millisecondsSinceEpoch);
    lastOpenDateTestimony = DateTime.fromMillisecondsSinceEpoch(
        prefs.getInt("Testimony" + "_last_open") ??
            DateTime.now().millisecondsSinceEpoch);

    debugPrint("prefs: " +
        DateTime.fromMillisecondsSinceEpoch(
                prefs.getInt(_selectedPage.label.toString() + "_last_open") ??
                    DateTime.now().millisecondsSinceEpoch)
            .toString());

    container.state.messages.forEach((msg) {
      if (msg.time.isAfter(lastOpenDateRosary) && msg.type == "rosary") {
        setState(() {
          latestRosaryCount++;
        });
      }
      if (msg.time.isAfter(lastOpenDateRequest) && msg.type == "request") {
        latestRequestCount++;
      }
      if (msg.time.isAfter(lastOpenDateRosary) && msg.type == "testimony") {
        latestTestimonyCount++;
      }

      debugPrint(
          "Unread Rosary message Counter : " + latestRosaryCount.toString());
      debugPrint(
          "Unread Request message Counter : " + latestRequestCount.toString());
      debugPrint("Unread Testimony message Counter : " +
          latestTestimonyCount.toString());
    });
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "OK",
      confirmFn: () {
        Navigator.pop(context);
//        Navigator.of(context).pushNamedAndRemoveUntil(
//            "/splash", (Route<dynamic> route) => false);
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void showBibleVerse() {
    showDialog(
        context: context,
        builder: (context) {
          //return dialog;
          return BibleVerseAnimation(
            animation: _bibleAnimation,
          );
        });
  }

  _onUnreadButtonPressed() {}

  // Navigation CallBacks from popMessage View

  void _goToHomeScreen() {
    setState(() {
      if (widget.appState.isLoading) widget.appState.isLoading = false;
      _showShortCutScreen = false;
    });
  }

  void _addRosaryBottomSheetCallBack() {
    UserBloc userBloc = UserProvider.of(context);
    RosaryBloc rosaryBloc = RosaryProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);

    if (_connectionStatus) {
      if (userBloc.userDetails.role == "guest") {
        _registerNow();
      } else
        openAddRosaryBottomSheet(rosaryBloc, userBloc, messageBloc);
    } else {
      showError(
          errorMsg: MemError(
              title: "Not Connected",
              body:
                  "Please check your internet connection. You can submit Rosary only when you are online."));
    }
  }

  void _collectRosaryCallBack() {
    UserBloc userBloc = UserProvider.of(context);
    RosaryBloc rosaryBloc = RosaryProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);

    if (_connectionStatus) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => RosaryListScreen(
                connectionStatus: _connectionStatus,
                fcmNotificationEnable:
                    remoteConfig.getBool('fcm_notifcation_enable'),
                routeObserver: widget.routeObserver,
                rosaryBloc: rosaryBloc,
                userBloc: userBloc,
                container: container,
                badgeStream: FirebaseFirestore.instance
                    .collection('mem')
                    .doc('rosary')
                    .collection('users')
                    .doc(userBloc.userDetails.id)
                    .collection('badges')
                    .where('action', isEqualTo: 'COLLECT ROSARY')
                    .snapshots(),
                key: MemKeys.rosaryListScreen)),
      );
    } else {
      showError(
          errorMsg: MemError(
              title: "Not Connected",
              body:
                  "Please check your internet connection. You can submit Rosary only when you are online."));
    }
  }

  // Add Prayer Request Call Back
  void _addPrayerRequestCallBack() {
    setState(() {
      _selectedPage = _allPages[2];
    });

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddEditMessageScreen(
            key: MemKeys.addMessageScreen,
            msgType: "request",
            selectedPage: _selectedPage,
            remoteConfig: remoteConfig,
          ),
        ));
  }

  // Add Testimony Call Back
  void _addTestimonyCallBack() {
    setState(() {
      _selectedPage = _allPages[4];
    });

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddEditMessageScreen(
            key: MemKeys.addMessageScreen,
            msgType: "testimony",
            selectedPage: _selectedPage,
            remoteConfig: remoteConfig,
          ),
        ));
  }

  //Community Call Back
  void _communityChatCallBack() {
    UserBloc userBloc = UserProvider.of(context);

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChatScreen(
            userBloc: userBloc,
            bgImageUrl: "assets/images/bg-pattern-1.jpg",
            connectionStatus: _connectionStatus,
            publicChat: true,
            chatRoomID: userBloc.userDetails.locale,
            fcmNotificationEnable:
                remoteConfig.getBool('fcm_notifcation_enable'),
            routeObserver: widget.routeObserver,
            messages: _messages,
            lastVisibleChatKey: lastVisibleKey,
            lastVisibleChatValue: lastVisibleValue,
          ),
        ));
  }

  //Guidance Request Callback
  void _guidanceChatCallBack() {
    UserBloc userBloc = UserProvider.of(context);

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChatScreen(
            userBloc: userBloc,
            bgImageUrl: "assets/images/bg-pattern-2.jpg",
            connectionStatus: _connectionStatus,
            publicChat: false,
            chatRoomID: userBloc.userDetails.id,
            fcmNotificationEnable:
                remoteConfig.getBool('fcm_notifcation_enable'),
            routeObserver: widget.routeObserver,
            messages: _guidanceMessages,
            lastVisibleChatKey: lastVisibleGuidanceKey,
            lastVisibleChatValue: lastVisibleGuidanceValue,
          ),
        ));
  }

  //Videos Callback
  void _videosCallBack() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => VideoScreen(),
        ));
  }

  //Consecration Callback
  void _consecrationCallBack() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ConsecrationScreen(),
        ));
  }

  @override
  void dispose() {
//    msgStreamSub?.cancel();
//    actionStreamSub?.cancel();
//    userActionStreamSub?.cancel();
    badgeStreamSub?.cancel();
//    adminMsgStreamSub?.cancel();
    //msgController?.dispose();
    chatCountListener.cancel();
    _splashController?.dispose();

    _tabController?.dispose();
    _bibleAnimationController?.dispose();
    // Clean up the controller when the Widget is disposed
    _controller.dispose();

    listeners.forEach((listener) {
      listener.cancel();
    });

    try {
      _connectivitySubscription?.cancel();
    } catch (e) {
      print(e.toString());
    }

    //_scrollController.dispose();
    super.dispose();
  }

  // Rosary Add Bottom Sheet

  void openAddRosaryBottomSheet(
      RosaryBloc rosaryBloc, UserBloc userBloc, MessageBloc messageBloc) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
//          return Container(
//
//              decoration: BoxDecoration(
//                boxShadow: <BoxShadow>[
//                  BoxShadow(
//                      color: Colors.black26,
//                      offset: new Offset(0.5, 0.5),
//                      blurRadius: 0.9,
//                      spreadRadius: 0.9)
//                ],
////                gradient: LinearGradient(
////                  begin: Alignment.topLeft,
////                  end: Alignment(
////                      1.1, 0.0), // 10% of the width, so there are ten blinds.
////                  colors: [
////                    Color.fromRGBO(24, 113, 170, 1.0),
////                    Color.fromRGBO(242, 150, 13, 1.0)
////                  ], // whitish to gray
////                  tileMode:
////                      TileMode.repeated, // repeats the gradient over the canvas
////                ),
//              ),
//              child:

          return Container(
            margin: const EdgeInsets.only(top: 0.0),
            padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
              gradient: LinearGradient(
                begin: Alignment(0, -1.0),
                end: Alignment(
                    0.0, 1.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Color(0xFFc90f0b),
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
            child: Form(
              key: rosaryFormKey,
              autovalidate: false,
              onWillPop: () {
                return Future(() => true);
              },
              child: StreamBuilder<int>(
                  stream: rosaryBloc.rosaryTotalCount,
                  initialData: 0,
                  builder: (context, snapshot) => new ListView(
                        children: [
//                      msgTitleField(messageBloc),

                          Container(
                              padding: EdgeInsets.only(left: 15.0, right: 15.0),
                              decoration: new BoxDecoration(
                                color: Theme.of(context).primaryColor,
//                                    image: new DecorationImage(
//                                        image: new AssetImage(
//                                            "assets/bg-drk-ptn.png"),
//                                        repeat: ImageRepeat.repeat),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      flex: 3,
                                      child: Text(
                                        "Rosary Count : ",
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Theme.of(context)
                                                .backgroundColor),
                                      )),
                                  Expanded(
                                      flex: 2,
                                      child: Container(
                                          margin: EdgeInsets.only(
                                              top: 20.0, bottom: 20.0),
                                          child: rosaryCountField(
                                              messageBloc, context))),
                                  submitButton(messageBloc, userBloc),
                                ],
                              )),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: Text(
                              "Total Rosary Count : " +
                                  snapshot.data.toString(),
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.w600,
                                  color:
                                      Theme.of(context).primaryIconTheme.color),
                            ),
                          ),
//                      msgBodyField(messageBloc),
                        ],
                      )),
            ),
          );
          //);
        });
  }

  Widget rosaryCountField(MessageBloc messageBloc, BuildContext context) {
    final rosaryBloc = RosaryProvider.of(context);
    return StreamBuilder(
      stream: messageBloc.rosaryCount,
      builder: (context, snapshot) {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Container(
                    color: Colors.white,
                    child: TextField(
                      onChanged: messageBloc.changeRosaryCount,
                      keyboardType: TextInputType.number,
                      maxLines: 1,
                      autofocus: true,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18.0,
//                    height: 0.05,
                        color: Colors.black,
                      ),
                      cursorColor: Color.fromRGBO(11, 129, 140, 1.0),
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0.0)),
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(217, 178, 80, 1.0))),
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0.0)),
                              borderSide: new BorderSide(
                                  color: Color.fromRGBO(217, 178, 80, 1.0))),

//                    hintText: MemLocalizations(Localizations.localeOf(context))
//                        .messageHint,
                          //labelText: 'Rosary Count',
                          contentPadding: EdgeInsets.all(12.0),
                          errorText: snapshot.error,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0.0)),
                              borderSide: new BorderSide(color: Colors.white)),
                          errorStyle: TextStyle(fontSize: 8.0)),
                    ))),
          ],
        );
      },
    );
  }

  Widget submitButton(MessageBloc messageBloc, UserBloc userBloc) {
    var container = AppStateContainer.of(context);

    return StreamBuilder(
      stream: messageBloc.submitValidRosaryMessage,
      builder: (context, snapshot) {
        return Container(
            margin: EdgeInsets.only(top: 10.0),
            padding: EdgeInsets.only(
                top: 0.0, bottom: 10.0, left: 10.0, right: 10.0),
            child: FlatButton(
                padding: EdgeInsets.only(
                    top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
                color: Theme.of(context).backgroundColor,
                splashColor: Theme.of(context).primaryColor,
                child: Text(
                  "Submit",
                  style: TextStyle(
                      color: Theme.of(context).primaryIconTheme.color),
                ),
                onPressed: () {
                  try {
                    final form = rosaryFormKey.currentState;
                    if (form.validate() && snapshot.hasData) {
                      setState(() {
                        messageBloc.addRosary(
                          userBloc.userDetails,
                          MemLists().getRandomListElement(
                              listType: "bible_verse",
                              locale: userBloc.userDetails.locale),
                        );
                      });

                      Navigator.pop(context);

                      //showBibleVerse();

                      showPopup(context, _popupBody(), 'Add Rosary Gift Verse');

//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                            builder: (context) => GiftVerseScreen(
//                                isBadge: false,
//                                giftVerse: MemLists().getRandomListElement(
//                                    listType: "bible_verse",
//                                    locale: userBloc.userDetails.locale),
//                                key: MemKeys.giftVerseScreen),
//                          ));
                    }
                  } catch (e) {
                    showMessage(context, e.toString());
                    debugPrint("Error from Rosary Screen : " + e.toString());
                  }
                }));
      },
    );
  }

  _registerNow() {
    var userBloc = UserProvider.of(context);
    var dialog = buildDialog(
        title: "Please register",
        message:
            "Would you like to log out and register with your phone number?",
        confirm: "Okay",
        confirmFn: () {
          if (userBloc.userDetails != null) {
            FirebaseAuth.instance.signOut();
            Navigator.of(context, rootNavigator: true).pop(context);
            RestartWidget.restartApp(context);
          }
        },
        cancel: "Not now",
        cancelFn: () {
          Navigator.of(context).pushNamedAndRemoveUntil(
              "/home", (Route<dynamic> route) => false);
        });

    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void showMessage(BuildContext context, String message,
      [MaterialColor color = Colors.red]) {
    debugPrint("Error:$message");
    Scaffold.of(context).showSnackBar(new SnackBar(
      backgroundColor: color,
      content: new Text(message),
      duration: Duration(seconds: 10),
    ));
  }

  void onBadge(QuerySnapshot snap) {
    MessageBloc messageBloc = MessageProvider.of(context);
    UserBloc userBloc = UserProvider.of(context);

    try {
      if (snap.docs.length != 0 && !snap.docs.last["isComplete"]) {
        //Disabling Message to chat room and notification on winning a badge
        //_sendMsgtoChatRoom(snap.documents.last.data);

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => GiftVerseScreen(
                  messageBloc: messageBloc,
                  userBloc: userBloc,
                  isBadge: true,
                  latestBadgeID: snap.docs.last["id"],
                  routeObserver: widget.routeObserver,
                  badgeType: "rosary_warrior",
                  giftVerse: "You Have a Badge",
                  key: MemKeys.giftVerseScreen),
            ));
      }
    } catch (e) {}
  }

  void _sendMsgtoChatRoom(Map badge) async {
    MessageBloc messageBloc = MessageProvider.of(context);
    UserBloc userBloc = UserProvider.of(context);

    String id = Uuid().generateV4();

    String msgTitle = "Congratulations " +
        userBloc.userDetails.title +
        " " +
        userBloc.userDetails.name;

    String _message = userBloc.userDetails.title +
        " " +
        userBloc.userDetails.name +
        " has won a badge : " +
        badge['title'];

    String _fcmMessage = "Let us congratulate " +
        userBloc.userDetails.title +
        " " +
        userBloc.userDetails.name +
        " for winning a badge : " +
        badge['title'];

    var message = {
      'id': id,
      'sender': {
        'name': "Admin",
        'id': "",
        'imageUrl':
            "https://firebasestorage.googleapis.com/v0/b/memapp-dev.appspot.com/o/profile%2Fd3aoKg9if4Mc3oMt7xKFkfyR2ia2.jpeg?alt=media&token=3b8d515e-9ff4-4110-b130-f959b381b085"
      },
      'text': "\n🎉 " + msgTitle + " 🎉\n\n" + _message,
      'time': DateTime.now().millisecondsSinceEpoch,
      'type': 'badge_rosary_warrior',
      'locale': userBloc.userDetails.locale,
      'region': 'all'
    };
    _messagesReference
        .child("chats")
        .child(userBloc.userDetails.locale ?? "English")
        .child(id)
        .set(message);

    FCMNotification notification = FCMNotification(
        title: "Let us congratulate " +
            userBloc.userDetails.title +
            " " +
            userBloc.userDetails.name,
        body: _fcmMessage ?? "",
        topic: userBloc.userDetails.locale ?? "English",
        author: userBloc.userDetails.name,
        time: DateTime.now().add(Duration(minutes: 1)),
        authorID: userBloc.userDetails.id ?? "",
        priority: "medium");

    if (remoteConfig.getBool('fcm_notifcation_enable'))
      messageBloc.messageRepository.addNotification(notification.toEntity());
  }

  Future<void> _showBigPictureNotification(QuerySnapshot snap) async {
    final String largeIconPath = await _downloadAndSaveFile(
        'https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/notifications%2Fic_launcher.png?alt=media&token=eb505f14-c991-44ed-b741-3c1845de555e',
        'largeIcon');
    final String bigPicturePath = await _downloadAndSaveFile(
        remoteConfig.getString('rosary_reminder_big_imageurl'), 'bigPicture');
    final BigPictureStyleInformation bigPictureStyleInformation =
        BigPictureStyleInformation(FilePathAndroidBitmap(bigPicturePath),
            largeIcon: FilePathAndroidBitmap(largeIconPath),
            contentTitle: snap.docs.last["title"],
            htmlFormatContentTitle: true,
            summaryText: '',
            htmlFormatSummaryText: true);
    final AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'memworld.mem.org', 'MEM', 'Notification Channel of MEM App',
            styleInformation: bigPictureStyleInformation);
    final NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(0, snap.docs.last["title"],
        snap.docs.last["body"], platformChannelSpecifics);
  }

  Future<String> _downloadAndSaveFile(String url, String fileName) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    final String filePath = '${directory.path}/$fileName';
    final http.Response response = await http.get(Uri.parse(url));
    final File file = File(filePath);
    await file.writeAsBytes(response.bodyBytes);
    return filePath;
  }

  Future _showBigTextNotification(QuerySnapshot snap) async {
    BigTextStyleInformation bigTextStyleInformation = BigTextStyleInformation(
        '<b> ' + snap.docs.last["title"] + ' </b> ' + snap.docs.last["body"],
        htmlFormatBigText: true,
        contentTitle: ' <b>' + snap.docs.last["title"] + '</b>',
        htmlFormatContentTitle: true,
        summaryText: '<i>' + snap.docs.last["title"] + '</i>',
        htmlFormatSummaryText: true);
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'memworld.mem.org',
      'MEM',
      'Notification Channel of MEM App',
      styleInformation: bigTextStyleInformation,
    );
    var platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(0, snap.docs.last["title"],
        snap.docs.last["body"], platformChannelSpecifics);
  }

  void sendAndroidNotification(QuerySnapshot snap) {
    switch (snap.docs.last["topic"]) {
      case "big_text":
        _showBigTextNotification(snap);
        break;
      case "big_pic":
        _showBigPictureNotification(snap);
        break;
      default:
        _showBigTextNotification(snap);
        break;
    }
  }

  Future initializeRemoteConfigs() async {
    //remoteConfig = await RemoteConfig.instance;
    remoteConfig = await RemoteConfig.instance;

    final defaults = <String, dynamic>{
      'rosary_reminder_big_imageurl':
          'https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/notifications%2Frosary_reminder.jpg?alt=media&token=2de912ca-62ec-4cf9-8663-50fa2a7eee55',
      'fcm_notifcation_enable': false,
      'playstore_url':
          'https://play.google.com/store/apps/details?id=org.memworld.memapp',
      'appstore_url':
          'https://apps.apple.com/us/app/marian-eucharistic-ministry/id1462384218'
    };
    await remoteConfig.setDefaults(defaults);

    await remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(seconds: 10),
      minimumFetchInterval: Duration.zero,
    ));
    await remoteConfig.fetchAndActivate();
    debugPrint('welcome message: ' +
        remoteConfig.getString('rosary_reminder_big_imageurl'));
    debugPrint('remote config fcm_enable: ' +
        remoteConfig.getBool('fcm_notifcation_enable').toString());
    debugPrint(
        'playstore url: ' + remoteConfig.getString('playstore_url').toString());
    debugPrint(
        'Appstore url: ' + remoteConfig.getString('appstore_url').toString());
  }

//  void onAdminMessage(QuerySnapshot snap) {}

  // Timeout for app to get ready
  startTimeout(Duration timeout) {
    return Timer(timeout, handleTimeout);
  }

  // Once app is ready, set isLoading to false and show the home screen
  void handleTimeout() {
    // callback function
//    if (!_homeScreenActive) {
//      if (mounted)
//        setState(() {
//          widget.appState.isLoading = false;
//        });
//    }
    setState(() {
      widget.appState.isLoading = false;
    });
  }

  Widget gospelCardCarousel() {
    var container = AppStateContainer.of(context);

    List<GospelCard> gospelCards = container.state.gospelCards;

    if (gospelCards.length != 0) {
      return Container(
          child: CarouselSlider(
        height: MediaQuery.of(context).size.height * 0.5,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 15),
        viewportFraction: 1.1,
        pauseAutoPlayOnTouch: Duration(seconds: 40),
        aspectRatio: 1.0,
        items: gospelCards.map((i) {
          return Builder(
            builder: (BuildContext context) {
              return gospelCard(
                  url: i.cardImageUrl,
                  title: i.title,
                  message: i.cardDetails,
                  id: i.id);
//              return gospelCard(
//                  url: i.cardImageUrl, title: i.title, message: i.cardDetails);
            },
          );
        }).toList(),
      ));
    } else
      return Image.asset(
        "assets/logo_drawer.png",
        fit: BoxFit.cover,
      );

//    return StreamBuilder(
//      stream: Firestore.instance
//          .collection('mem')
//          .document('rosary')
//          .collection('gospelcards')
//          .where('sliderCard', isEqualTo: true)
//          .snapshots(),
//      builder: (BuildContext context, snapshot) {
//        if (snapshot.hasError) return Container();
//        if (!snapshot.hasData || snapshot.data == null) {
//          return Container();
//        } else {
//          switch (snapshot.connectionState) {
//            case ConnectionState.waiting:
//              return Center(
//                  child: CircularProgressIndicator(
//                      valueColor:
//                          AlwaysStoppedAnimation<Color>(Colors.transparent)));
//
//            default:
//              try {
//                return Stack(children: <Widget>[
//                  CarouselSlider(
//                    height: MediaQuery.of(context).size.height * 0.46,
//                    autoPlay: true,
//                    autoPlayInterval: Duration(seconds: 20),
//                    viewportFraction: 1.0,
//                    pauseAutoPlayOnTouch: Duration(seconds: 20),
//                    items: gospelCards.map((i) {
//                      return Builder(
//                        builder: (BuildContext context) {
//                          //return gospelCardImage(i.cardImageUrl);
//                          return Text(i.cardDetails);
//                        },
//                      );
//                    }).toList(),
//                  )
//                ]);
//              } catch (e) {
//                return Container();
//              }
//          }
//        }
//      },
//    );
  }

  Widget gospelCard({String url, String title, String message, String id}) {
    return Card(
        color: Color.fromRGBO(255, 213, 154, 1.0),
        margin: EdgeInsets.only(top: 0.0, bottom: 0.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            gospelCardImage(url),
            Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new AssetImage("assets/bg-gospel-msg.jpg"),
                    fit: BoxFit.cover,
                    repeat: ImageRepeat.noRepeat),
              ),
              child: leftColumn(title: title, message: message, id: id),
              width: MediaQuery.of(context).size.width * 0.6,
            ),
          ],
        ));
  }

  Widget stars() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(Icons.star, color: Colors.green[500]),
        Icon(Icons.star, color: Colors.green[500]),
        Icon(Icons.star, color: Colors.green[500]),
        Icon(Icons.star, color: Colors.black),
        Icon(Icons.star, color: Colors.black),
      ],
    );
  }

// DefaultTextStyle.merge() allows you to create a default text
// style that is inherited by its child and all subsequent children.

  Widget ratings() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          stars(),
          Text(
            '170 Reviews',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w800,
              fontFamily: 'Roboto',
              letterSpacing: 0.5,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }

  Widget titleText(String title) {
    var fontSize = MediaQuery.of(context).size.width * 0.02;
    return Container(
        padding: EdgeInsets.only(top: 10.0, bottom: 2.0, left: 10.0),
        child: Text(
          "- " + title,
          style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w500),
          textAlign: TextAlign.left,
        ));
  }

  Widget messageText(String msg) {
    var fontSize = MediaQuery.of(context).size.width * 0.030;
    return Container(
      padding: EdgeInsets.all(fontSize),
      child: Text(msg,
          style: TextStyle(
              fontSize: fontSize,
              color: Colors.black,
              fontWeight: FontWeight.w500)),
    );
  }

  Widget leftColumn({String title, String message, String id}) {
    var container = AppStateContainer.of(context);
    bool _isAdmin = container.state.userDetails.role == "admin";
    bool _isLocaleAdmin = container.state.userAcl != null
        ? container.state.userDetails.isSuperAdmin &&
            container.state.userAcl.localeAdmin
        : false;
    bool _eligibleToDelete =
        container.state.userAcl != null ? _isLocaleAdmin && _isAdmin : false;
    var iconSize = MediaQuery.of(context).size.width * 0.05;
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    return Container(
      //decoration: BoxDecoration(color: Color.fromRGBO(255, 213, 154, 1.0)),

      padding: EdgeInsets.fromLTRB(15, 15, 15, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment:
            isTablet ? MainAxisAlignment.center : MainAxisAlignment.end,
        children: [
          messageText(message),
//          titleText(title),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              IconButton(
                  tooltip: "Share the message",
                  color: Color.fromRGBO(0, 114, 164, 1.0),
                  icon: ImageIcon(AssetImage('assets/icons/share.png'),
                      size: iconSize),
                  onPressed: message.isEmpty
                      ? null
                      : (() {
                          final RenderBox box = context.findRenderObject();
                          Share.share(message,
                              sharePositionOrigin:
                                  box.localToGlobal(Offset.zero) & box.size);
                        })),
              _eligibleToDelete
                  ? IconButton(
                      iconSize: 18.0,
                      color: Color.fromRGBO(0, 114, 164, 1.0),
                      key: MemKeys.deleteMessageButton,
                      tooltip: MemLocalizations(Localizations.localeOf(context))
                          .deleteMessage,
                      icon: ImageIcon(
                          AssetImage('assets/icons/icons8-trash.png'),
                          size: iconSize),
                      onPressed: () {
                        if (_connectionStatus) {
                          confirmDelete(
                              errorMsg: MemError(
                                  title:
                                      "Are you sure ? Please confirm to delete",
                                  body:
                                      "You are deleting a message. Please press Delete to confirm delete."),
                              id: id);
                        } else {
                          showError(
                            errorMsg: MemError(
                                title: "Not Connected",
                                body:
                                    "Please check your internet connection. You can delete a message only when you are online."),
                          );
                        }

                        //Navigator.pop(context, message);
                      },
                    )
                  : Container(),
            ],
          )

//          ratings(),
//          iconList(),
        ],
      ),
    );
  }

  Widget gospelCardImage(String url) {
    return FutureBuilder<ImageProvider>(
        future: _loadImage(url),
        builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.active:
            case ConnectionState.none:
              return CircularProgressIndicator(
                  strokeWidth: 3.0,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black));
            case ConnectionState.done:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return Container(
                    width: MediaQuery.of(context).size.width * 0.45,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      image: new DecorationImage(
                        image: snapshot.data,
                        fit: BoxFit.cover,
                      ),
                    ));
              break;
            default:
              return Container(
                  width: MediaQuery.of(context).size.width * 0.4,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    image: new DecorationImage(
                      image: snapshot.data,
                      fit: BoxFit.cover,
                    ),
                  ));
              break;
          }
        });
  }

  Future<ImageProvider> _loadImage(String url) async {
    ImageProvider profilePhoto = CachedNetworkImageProvider(url ?? "") ??
        NetworkImage(url ?? "") ??
        AssetImage("assets/placeholder-face.png");

    return profilePhoto;
  }

  void confirmDelete({MemError errorMsg, String id}) {
    var dialog = DialogShower.buildDialog(
        title: errorMsg.title,
        message: errorMsg.body,
        cancel: "cancel",
        confirm: "Delete",
        confirmFn: () {
          _removeGospelCard(context, id);
          Navigator.of(context, rootNavigator: true).pop();
        });
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void _removeGospelCard(BuildContext context, String id) {
    final messageBloc = MessageProvider.of(context);

    var container = AppStateContainer.of(context);
    String _deleteMessage;
    String _undoButtonMessage;
    _deleteMessage = MemLocalizations(Localizations.localeOf(context))
        .messageDeleted("Gospel Card");
    _undoButtonMessage = MemLocalizations(Localizations.localeOf(context)).undo;

    bool _isAdmin = container.state.userDetails.role == "admin";
    bool _isLocaleAdmin = container.state.userAcl != null
        ? container.state.userDetails.isSuperAdmin &&
            container.state.userAcl.localeAdmin
        : false;
    bool _eligibleToDelete =
        container.state.userAcl != null ? _isLocaleAdmin && _isAdmin : false;
    debugPrint("Eligible to delete : " + _eligibleToDelete.toString());
//    debugPrint("Author ID: " + message.authorID);
//    debugPrint("User ID: " +
//        container.state.userDetails.id +
//        "Username" +
//        container.state.userDetails.name);

    if (_eligibleToDelete) {
      messageBloc.messageRepository.deleteGospelCard(id);

      _deleteMessage = "Sorry, You cannot delete this gospel card";
      _undoButtonMessage = "";
    } else {
      _deleteMessage = "Sorry, You cannot delete this message";
      _undoButtonMessage = "";
    }
  }

  showPopup(BuildContext context, Widget widget, String title,
      {BuildContext popupContext}) {
    Navigator.push(
      context,
      PopupLayout(
        top: 80,
        left: 30,
        right: 30,
        bottom: 50,
        child: PopupContent(
          content: Scaffold(
            resizeToAvoidBottomInset: false,
            //resizeToAvoidBottomPadding: false,
            body: widget,
            backgroundColor: Colors.transparent,
          ),
        ),
      ),
    );
  }

  Widget _popupBody() {
    var closeButtonRadius = MediaQuery.of(context).size.width * 0.1;
    var iconSize = MediaQuery.of(context).size.width * 0.065;
    String giftVerse = MemLists().getRandomListElement(
        listType: "bible_verse", locale: widget.userBloc.userDetails.locale);
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;
    var giftVerseFontSize = isTablet ? 30.0 : 12.0;
    var giftVerseLeftMargin = isTablet
        ? MediaQuery.of(context).size.width * 0.24
        : MediaQuery.of(context).size.width * 0.16;
    var giftVerseRightMargin = isTablet
        ? MediaQuery.of(context).size.width * 0.24
        : MediaQuery.of(context).size.width * 0.16;
    var shareTopMargin = isTablet
        ? MediaQuery.of(context).size.height * 0.45
        : MediaQuery.of(context).size.height * 0.36;

    return Stack(
      children: <Widget>[
        Align(
            alignment: Alignment.center,
            child: ScratchCard(
              cover: Stack(fit: StackFit.expand, children: <Widget>[
                Align(
                    alignment: Alignment.center,
                    child: Container(
                        height: MediaQuery.of(context).size.height * 0.6,
                        decoration: new BoxDecoration(
                          image: new DecorationImage(
                              image:
                                  new AssetImage("assets/gift_verse_cover.png"),
                              fit: BoxFit.contain,
                              repeat: ImageRepeat.noRepeat),
                        ),
                        child: Container()))
              ]),
              reveal: Stack(fit: StackFit.expand, children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    decoration: BoxDecoration(
                        image: new DecorationImage(
                            image: new AssetImage("assets/gift_verse_bg.png"),
                            fit: BoxFit.contain,
                            repeat: ImageRepeat.noRepeat)),
                    child: Center(
                      child: Container(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.15,
                              left: giftVerseLeftMargin,
                              right: giftVerseRightMargin),
                          child: Text(
                            giftVerse,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: giftVerseFontSize,
                              fontWeight: FontWeight.bold,
                              color: Color.fromRGBO(59, 35, 35, 1.0),
                            ),
                          )),
                    ),
                  ),
                )
              ]),
              strokeWidth: 15.0,
              finishPercent: 50,
              onComplete: () => print('The card is now clear!'),
            )),
        Align(
            alignment: Alignment.topCenter,
            child: Container(
                margin: EdgeInsets.only(bottom: closeButtonRadius),
                width: closeButtonRadius,
                height: closeButtonRadius,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(62, 83, 117, 0.8),
                    borderRadius:
                        BorderRadius.all(Radius.circular(closeButtonRadius))),
                child: IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: closeButtonRadius * 0.5,
                  ),
                  onPressed: () {
                    try {
                      Navigator.pop(context); //close the popup
                    } catch (e) {}
                  },
                ))),
//        Positioned(
//            bottom: MediaQuery.of(context).size.height * 0.18,
//            left: MediaQuery.of(context).size.width * 0.35,
        Align(
            alignment: Alignment.center,
            child: Container(
                margin: EdgeInsets.only(top: shareTopMargin),
                child: IconButton(
                    color: Colors.black,
                    icon: ImageIcon(AssetImage('assets/icons/share.png'),
                        size: isTablet ? iconSize * 3.0 : iconSize),
                    onPressed: (() {
                      final RenderBox box = context.findRenderObject();
                      Share.share(giftVerse,
                          sharePositionOrigin:
                              box.localToGlobal(Offset.zero) & box.size);
                    }))))
      ],
    );
  }

  Widget gospelMessage() {
    var deviceWidth = MediaQuery.of(context).size.width;
    var deviceHeight = MediaQuery.of(context).size.height;
    var gospelMessageTopPosition = deviceHeight * 0.3;
    var gospelMessageFontSize = MediaQuery.of(context).size.height * 0.025;
    UserBloc userBloc = UserProvider.of(context);

    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('mem')
          .doc('rosary')
          .collection('gospelcards')
          .where('locale', isEqualTo: userBloc.userDetails.locale)
          .where('startUpCard', isEqualTo: true)
          .limit(1)
          .snapshots(),
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasError) return Container();
        if (!snapshot.hasData || snapshot.data == null) {
          return Container();
        } else {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
//              return Container(
//                alignment: Alignment.bottomCenter,
//                margin: EdgeInsets.symmetric(vertical: 32.0, horizontal: 32.0),
//                child: RevealProgressButton(appState: widget.appState),
//              );

              return Container();
//              return Center(
//                  child: CircularProgressIndicator(
//                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white)));

            default:
              try {
                return SizedBox(
                  width: deviceWidth * 0.9,
                  height: deviceHeight * 0.3,
                  child: Container(
//                      margin: EdgeInsets.only(
//                        top: MediaQuery.of(context).size.height * 0.6,
//                        bottom: MediaQuery.of(context).size.height * 0.2,
//                      ),
                      //padding: EdgeInsets.all(gospelMessageFontSize * 0.8),
                      child: BorderedText(
                          strokeCap: StrokeCap.square,
                          strokeColor: Colors.white24,
                          strokeJoin: StrokeJoin.miter,
                          strokeWidth: 3.0,
                          child: Text(
                            snapshot.data.documents.last["cardDetails"]
                                .toUpperCase(),
                            style: TextStyle(
                                color: Color.fromRGBO(58, 43, 104, 1),
                                fontSize: gospelMessageFontSize,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                            // or Alignment.topLeft
                          ))),
                );
              } catch (e) {
                return Container();
              }
          }
        }
      },
    );
  }
}

class BibleVerseAnimation extends AnimatedWidget {
  BibleVerseAnimation({Key key, Animation animation})
      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    Animation animation = listenable;
    return Center(
        child: Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        image: new DecorationImage(
            image: new AssetImage("assets/chat_bg.png"),
            fit: BoxFit.fitWidth,
            repeat: ImageRepeat.repeatX),
      ),
      height: animation.value,
      width: animation.value,
      child: Center(child: Text("God Loves You")),
    ));
  }
}

class RosaryCountHeart extends AnimatedWidget {
  /// The function to call when the icon button is pressed.
  final VoidCallback onPressed;

  final int rosaryCount;

  final Color badgeColor;

  final Color badgeTextColor;

  final Tween<Offset> _badgePositionTween = Tween(
    begin: const Offset(-0.5, 0.9),
    end: const Offset(0.0, 0.0),
  );

  RosaryCountHeart(
      {Key key,
      Animation animation,
      @required this.rosaryCount,
      this.badgeColor: Colors.white,
      this.badgeTextColor: Colors.black,
      this.onPressed})
      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    Animation animation = listenable;

    return IconButton(
        icon: Stack(
          overflow: Overflow.visible,
          children: [
            Positioned(
              right: -10.0,
              bottom: 0.0,
              child: SlideTransition(
                position: _badgePositionTween.animate(animation),
//                position: animation.value,
                child: Material(
                    type: MaterialType.transparency,
                    elevation: 46.0,
                    color: Colors.white,
                    child: Container(
                      padding: const EdgeInsets.only(top: 24.0, left: 0.0),
                      height: 50.0,
                      width: 50.0,
//                      decoration: BoxDecoration(
//                          image: DecorationImage(
//                              fit: BoxFit.cover,
//                              image: AssetImage("assets/rose_heart.png"))),
                      child: rosaryCount > 0
                          ? Text(
                              NumberFormat("##,##,##,##,###")
                                  .format(rosaryCount),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.white,
                                fontWeight: FontWeight.w900,
                              ),
                            )
                          : Container(),
                    )),
              ),
            ),
          ],
        ),
        onPressed: onPressed);
  }
}
