import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memapp/screens/add_benefactor_screen.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:cached_network_image/cached_network_image.dart';

class AboutScreen extends StatefulWidget {
  final List<Benefactor> benefactors;
  final FirebaseStorage storage;

  AboutScreen({
    @required this.benefactors,
    @required this.storage,
    Key key,
  }) : super(key: MemKeys.aboutScreen);
  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  int _messageLength = 0;
  int _messageLengthLimit = 300;
  List<Benefactor> priests = [];
  List<Benefactor> bishops = [];
  bool isContentEditor = false;

  @override
  void initState() {
    _messageLength = _messageLengthLimit;
    priests = widget.benefactors.where((i) => i.isPriest).toList();
    bishops = widget.benefactors.where((i) => !i.isPriest).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget content;
    UserBloc userBloc = UserProvider.of(context);

    var container = AppStateContainer.of(context);

    if (userBloc.userDetails.role == "admin" &&
        container.state.userAcl != null) {
      if (container.state.userAcl.editAboutUsContent) {
        isContentEditor = true;
      } else {
        isContentEditor = false;
      }
    }

    if (widget.benefactors.isEmpty) {
      content = Center(child: Container());
    } else {
      content = Container(
          decoration: new BoxDecoration(
            color: Theme.of(context).primaryColorLight,
          ),
          child: DefaultTabController(
              length: 2,
              child: ListView(
                shrinkWrap: false,
                children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height * 0.30,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/aboutus-banner.jpg"),
                          fit: BoxFit.cover,
                        ),
                      )),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(bottom: 30.0),
                      child: Container(
                          color: Color(0xFFddb15e),
                          child: TabBar(
                            labelColor: Theme.of(context).backgroundColor,
                            indicatorColor: Theme.of(context).backgroundColor,
                            unselectedLabelColor:
                                Theme.of(context).backgroundColor,
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: BoxDecoration(
                                color: Theme.of(context).primaryColor),
                            labelStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 15.0),
                            //unselectedLabelColor: Colors.white,
                            unselectedLabelStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 15.0),
                            tabs: [
                              Tab(
                                text: "History",
                              ),
                              Tab(
                                text: "Benefactors",
                              ),
                            ],
                          ))),
                  SizedBox(
                      height: MediaQuery.of(context).size.height * 2.6,
                      child: TabBarView(children: [
                        Container(
                          margin: EdgeInsets.only(
                              top: 0.0, left: 30.0, right: 30.0, bottom: 20.0),
                          child: Text(
                            "Marian Eucharistic Ministry(MEM) is a global network of faithful, devoted to our mother, Blessed Virgin Mary with a focus on gathering rosaries for the salvation of the souls and the sanctification of the Holy church.\n\nIt had its beginnings in 2015, as a small rosary prayer group at NewZealand. Holy Spirit gave this group a vision of spreading this devotion by joining hands with like minded brothers and sisters across the world through the medium of the internet.By the working of the Holy Spirit, gradually it evolved as a chain of Rosary groups in WhatsApp with members joining from across the globe.\n\nMany prayer requests are answered and many miracles are witnessed testifying the powerful working of the Holy Spirit and our Lady through MEM.\n\nA MEM prayer group was formed in Trivandrum under the guidance and leadership of His Grace Most Rev. Bishop Samuel Mar Irenios, the then Auxiliary Bishop of Syro Malankara Catholic Church Trivandrum. This was a divine plan, as this association with His Grace Most Rev. Bishop Samuel Mar Irenios grew and He later became the Patron of MEM.\n\nMEM, is now spread in all five continents, with hundred of priests, religious and lay faithful as its members. The member priests daily offer all the MEM members and its ministry in their daily Holy Mass.\n\nInspired by Holy Spirit, this MEM mobile App was developed. \n\nAt MEM members are encouraged to update the count of rosaries they personally offer or collect from others. At the same time , for the members of MEM,neither there is an obligation to offer a particular number of rosaries nor an obligation to offer all the rosaries they recite or collect for the intentions of MEM. Offering prayer requests and rosaries along with MEM obtains a special grace for the members and the whole world. We have been witnessing tremendous victories in the heavenly battles using rosary, the strongest weapon against the enemy. May God bless many more to join this mission and obtain the heavenly blessings.",
                            style: TextStyle(fontSize: 14.0),
                            textAlign: TextAlign.justify,
                          ),
                        ),

//Church Fathers... Messages. Not to delete.
//              Container(
//                  decoration: new BoxDecoration(
//                    color: Color.fromRGBO(217, 178, 80, 1.0),
//                    image: new DecorationImage(
//                        image: new AssetImage("assets/bg-drk-ptn.png"),
//                        repeat: ImageRepeat.repeat),
//                  ),
//                  child: Container(
//                    //color: Color.fromRGBO(217, 178, 80, 1.0),
//                    margin: EdgeInsets.only(
//                      top: 20.0,
//                      left: 30.0,
//                      right: 30.0,
//                    ),
//                    child: Text(
//                      "Endorsements",
//                      style: TextStyle(
//                          fontWeight: FontWeight.w700, fontSize: 20.0),
//                    ),
//                  )),
//              Container(
//                  decoration: new BoxDecoration(
//                    color: Color.fromRGBO(217, 178, 80, 1.0),
//                    image: new DecorationImage(
//                        image: new AssetImage("assets/bg-drk-ptn.png"),
//                        repeat: ImageRepeat.repeat),
//                  ),
//                  child: Container(
//                      margin: EdgeInsets.only(
//                          top: 20.0, left: 30.0, right: 30.0, bottom: 20.0),
//                      child: Container(
//                          child: ListView.builder(
//                        shrinkWrap: true,
//                        itemCount: bishops.length,
//                        itemBuilder: _buildBenefactorView,
//                      ))
//                  )),

                        //#Church Fathers

//                Column(
//                  children: <Widget>[
//                    Row(
//                      children: <Widget>[
//                        Container(
//                            padding: EdgeInsets.only(
//                                right: 20.0, top: 20.0, bottom: 20.0),
//                            child: Image(
//                              image: AssetImage("assets/bishop.jpeg"),
//                              width: 50.0,
//                            )),
//                        Expanded(
//                            child: Column(
//                              children: <Widget>[
//                                Container(
//                                    margin: EdgeInsets.only(bottom: 5.0),
//                                    child: Text(
//                                      "Most Rev. Cardinal Mar George Allenchery",
//                                      style: TextStyle(
//                                          fontWeight: FontWeight.w600,
//                                          fontSize: 12.0),
//                                    )),
//                                Text(
//                                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus at ligula cursus laoreet eget quis elit. Nunc at risus vel eros tempor ultrices at vitae purus. Aenean massa lorem, convallis gravida tellus id, faucibus iaculis augue. Nullam porta at sem at mattis. Praesent iaculis nisi et dignissim accumsan.",
//                                  style: TextStyle(fontSize: 12.0),
//                                  textAlign: TextAlign.justify,
//                                ),
//                                AnimatedContainer(
//                                  padding: EdgeInsets.only(top: 5.0),
//                                  duration: const Duration(milliseconds: 120),
//                                  child:
//                                  widget.message.body.length >
//                                      _messageHeightLimit
//                                      ? Text(widget.message.body
//                                      .substring(0, _messageLength))
//                                      : Text(widget.message.body),
//                                ),
//                                GestureDetector(
//                                  onTap: () =>
//                                      setState(() {
//                                        _messageLength != _messageHeightLimit
//                                            ?
//                                        _messageLength = _messageHeightLimit
//                                            : _messageLength =
//                                            widget.message.body.length;
//                                      }),
//                                  child: Container(
//                                    child: widget.message.body.length >
//                                        _messageHeightLimit
//                                        ? Text(
//                                      _messageLength == _messageHeightLimit
//                                          ? "...Read More"
//                                          : "",
//                                      style:
//                                      TextStyle(color: Colors.blueAccent),
//                                    )
//                                        : Container(),
//                                  ),
//                                ),
//                              ],
//                            ))
//                      ],
//                    )
//                  ],
//                ),

//                  Container(
//                      decoration: new BoxDecoration(
//                        color: Color.fromRGBO(234, 203, 111, 1.0),
//                        image: new DecorationImage(
//                            image: new AssetImage("assets/bg-lgt-ptn1.png"),
//                            repeat: ImageRepeat.repeat),
//                      ),
//                      child: Container(
//                        margin: EdgeInsets.only(
//                          top: 20.0,
//                          left: 30.0,
//                          right: 30.0,
//                        ),
//                        child: Text(
//                          "Benefactor Priests",
//                          style: TextStyle(
//                              fontWeight: FontWeight.w700, fontSize: 20.0),
//                        ),
//                      )),

                        Container(
                            margin: EdgeInsets.only(
                                top: 0.0,
                                left: 30.0,
                                right: 30.0,
                                bottom: 20.0),
                            child: GridView.count(
                              // Create a grid with 2 columns. If you change the scrollDirection to
                              // horizontal, this would produce 2 rows.
                              crossAxisCount: 3,
                              childAspectRatio: 0.75,
                              crossAxisSpacing: 5.0,
                              mainAxisSpacing: 5.0,
                              physics: ScrollPhysics(),

                              children: List.generate(priests.length, (index) {
                                return Container(
//                                    padding: EdgeInsets.only(
//                                        top: 10.0, right: 10.0, left: 10.0),
                                    child: Column(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                          image: new DecorationImage(
                                            image: priests[
                                                            index]
                                                        .profilePhotoUrl ==
                                                    null
                                                ? AssetImage(
                                                    'assets/placeholder-face.png')
                                                : CachedNetworkImageProvider(
                                                        priests[index]
                                                                .profilePhotoUrl ??
                                                            "") ??
                                                    NetworkImage(priests[index]
                                                            .profilePhotoUrl ??
                                                        ""),
                                            fit: BoxFit.cover,
                                          ),
                                          border: Border.all(
                                              color: Colors.black12,
                                              width: 3.0,
                                              style: BorderStyle.solid)),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.16,
                                    ),
                                    Container(
                                      child: Text(
                                        priests[index].title +
                                            priests[index].name,
                                        style: TextStyle(
                                            fontSize: 10.0,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.center,
                                      ),
                                      padding: EdgeInsets.only(top: 3.0),
                                    ),
                                  ],
                                ));
                              }),
                            )),
                      ]))
                ],
              )));
    }

    return Scaffold(
      appBar: AppBar(
          title: Text(
              MemLocalizations(Localizations.localeOf(context)).aboutUsTitle,
              style: TextStyle(
                color: Theme.of(context).primaryIconTheme.color,
              )),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      body: content,
      backgroundColor: Color.fromRGBO(234, 203, 111, 1.0),
      floatingActionButton: userBloc.userDetails.isSuperAdmin && isContentEditor
          ? FloatingActionButton.extended(
              tooltip: 'Add Benefactor',
              backgroundColor: Colors.blueGrey,
              icon: Icon(Icons.person_pin),
              label: Text('Add Benefactor'.toUpperCase()),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (c) {
                      return AddBenefactorScreen(
                        storage: widget.storage,
                        key: MemKeys.addBenefactorScreen,
                      );
                    },
                  ),
                );
              })
          : Container(),
    );
  }

  Widget _buildBenefactorView(BuildContext context, int index) {
    var benefactor = bishops[index];
    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);

    return Container(
        padding: EdgeInsets.only(bottom: 20.0),
        child: Row(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(right: 15.0, bottom: 10.0),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.black12,
                                width: 3.0,
                                style: BorderStyle.solid)),
                        child: Image(
                          image: benefactor.profilePhotoUrl == null
                              ? AssetImage('assets/placeholder-face.png')
                              : CachedNetworkImageProvider(
                                      benefactor.profilePhotoUrl ?? "") ??
                                  NetworkImage(
                                      benefactor.profilePhotoUrl ?? ""),
                          height: 50.0,
                        )),
                    userBloc.userDetails.isSuperAdmin && isContentEditor
                        ? FlatButton(
                            onPressed: () {
                              messageBloc.messageRepository
                                  .deleteBenefactor(benefactor.toEntity());
                            },
                            child: Icon(
                              Icons.delete,
                              color: Colors.brown,
                              size: 14.5,
                            ))
                        : Container(width: 1.0),
                  ],
                )),
            Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(bottom: 3.0),
                        child: Text(
                          benefactor.title + benefactor.name,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 12.0),
                        )),
                    AnimatedContainer(
                      padding: EdgeInsets.only(top: 1.0),
                      duration: const Duration(milliseconds: 120),
                      child: benefactor.message.length > _messageLengthLimit
                          ? Text(
                              benefactor.message.substring(0, _messageLength),
                              style: TextStyle(fontSize: 12.0),
                              textAlign: TextAlign.justify,
                            )
                          : Text(
                              benefactor.message,
                              style: TextStyle(fontSize: 12.0),
                              textAlign: TextAlign.justify,
                            ),
                    ),
                    GestureDetector(
                      onTap: () => setState(() {
                        _messageLength != _messageLengthLimit
                            ? _messageLength = _messageLengthLimit
                            : _messageLength = benefactor.message.length;
                      }),
                      child: Container(
                        margin: EdgeInsets.only(left: 140.0),
                        child: benefactor.message.length > _messageLengthLimit
                            ? Text(
                                _messageLength == _messageLengthLimit
                                    ? "...Read More"
                                    : "",
                                style: TextStyle(
                                    color: Colors.brown, fontSize: 12.0),
                              )
                            : Container(),
                      ),
                    ),
                  ],
                ))
          ],
        ));
  }
}
