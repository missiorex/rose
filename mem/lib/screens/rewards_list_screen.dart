import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/app_state_container.dart';

const double _kMinFlingVelocity = 800.0;
const String _kGalleryAssetsPackage = 'flutter_gallery_assets';

enum RewardsTileStyle { imageOnly, oneLine, twoLine }

typedef BadgeTapCallback = void Function(Badge badge);

class RewardsViewer extends StatefulWidget {
  const RewardsViewer({Key key, this.badge}) : super(key: key);

  final Badge badge;

  @override
  _RewardsViewerState createState() => _RewardsViewerState();
}

class _RewardsTitleText extends StatelessWidget {
  const _RewardsTitleText(this.text);

  final String text;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Text(text),
    );
  }
}

class _RewardsViewerState extends State<RewardsViewer>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _flingAnimation;
  Offset _offset = Offset.zero;
  double _scale = 1.0;
  Offset _normalizedOffset;
  double _previousScale;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this)
      ..addListener(_handleFlingAnimation);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

// The maximum offset value is 0,0. If the size of this renderer's box is w,h
// then the minimum offset value is w - _scale * w, h - _scale * h.
  Offset _clampOffset(Offset offset) {
    final Size size = context.size;
    final Offset minOffset = Offset(size.width, size.height) * (1.0 - _scale);
    return Offset(
        offset.dx.clamp(minOffset.dx, 0.0), offset.dy.clamp(minOffset.dy, 0.0));
  }

  void _handleFlingAnimation() {
    setState(() {
      _offset = _flingAnimation.value;
    });
  }

  void _handleOnScaleStart(ScaleStartDetails details) {
    setState(() {
      _previousScale = _scale;
      _normalizedOffset = (details.focalPoint - _offset) / _scale;
      // The fling animation stops if an input gesture starts.
      _controller.stop();
    });
  }

  void _handleOnScaleUpdate(ScaleUpdateDetails details) {
    setState(() {
      _scale = (_previousScale * details.scale).clamp(1.0, 4.0);
      // Ensure that image location under the focal point stays in the same place despite scaling.
      _offset = _clampOffset(details.focalPoint - _normalizedOffset * _scale);
    });
  }

  void _handleOnScaleEnd(ScaleEndDetails details) {
    final double magnitude = details.velocity.pixelsPerSecond.distance;
    if (magnitude < _kMinFlingVelocity) return;
    final Offset direction = details.velocity.pixelsPerSecond / magnitude;
    final double distance = (Offset.zero & context.size).shortestSide;
    _flingAnimation = _controller.drive(Tween<Offset>(
      begin: _offset,
      end: _clampOffset(_offset + direction * distance),
    ));
    _controller
      ..value = 0.0
      ..fling(velocity: magnitude / 1000.0);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onScaleStart: _handleOnScaleStart,
      onScaleUpdate: _handleOnScaleUpdate,
      onScaleEnd: _handleOnScaleEnd,
      child: ClipRect(
        child: Transform(
          transform: Matrix4.identity()
            ..translate(_offset.dx, _offset.dy)
            ..scale(_scale),
          child: Container(
              padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.15),
              color: Colors.white,
              child: Image.asset(
                widget.badge.assetName,
                fit: BoxFit.contain,
              )),
        ),
      ),
    );
  }
}

class RewardsItem extends StatelessWidget {
  RewardsItem({
    Key key,
    @required this.badge,
    @required this.tileStyle,
    @required this.onBadgeTap,
  })  : assert(badge != null),
        assert(tileStyle != null),
        assert(onBadgeTap != null),
        super(key: key);

  final Badge badge;
  final RewardsTileStyle tileStyle;
  final BadgeTapCallback
      onBadgeTap; // User taps on the photo's header or footer.

  void showPhoto(BuildContext context) {
    Navigator.push(context,
        MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
            title: Text(badge.title),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, 0),
                  end: Alignment(
                      1.0, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    Theme.of(context).primaryColorDark,
                    Theme.of(context).backgroundColor
                  ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
            )),
        body: SizedBox.expand(
          child: Hero(
            tag: badge.id,
            child: RewardsViewer(badge: badge),
          ),
        ),
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    final Widget image = GestureDetector(
      onTap: () {
        showPhoto(context);
      },
      child: Hero(
          key: Key(badge.assetName),
          tag: badge.id,
          child: Image.asset(
            badge.assetName,
            fit: BoxFit.cover,
          )),
    );

//    final IconData icon = badge.isComplete ? Icons.star : Icons.star_border;

    switch (tileStyle) {
      case RewardsTileStyle.imageOnly:
        return image;

      case RewardsTileStyle.oneLine:
        return GridTile(
          footer: GestureDetector(
            onTap: () {
              onBadgeTap(badge);
            },
            child: GridTileBar(
              title: _RewardsTitleText(badge.title),
              backgroundColor: Colors.black45,
              leading: ImageIcon(
                  AssetImage('assets/star_' + badge.level.toString() + '.png')),
//              leading: Icon(
//                icon,
//                color: Colors.white,
//              ),
            ),
          ),
          child: image,
        );

      case RewardsTileStyle.twoLine:
        return GridTile(
          header: GestureDetector(
            onTap: () {
              onBadgeTap(badge);
            },
            child: GridTileBar(
              backgroundColor: Colors.black45,
              title: _RewardsTitleText(badge.title),
              subtitle: _RewardsTitleText(badge.level.toString()),
              trailing: ImageIcon(
                  AssetImage('assets/star_' + badge.level.toString() + '.png')),
//              trailing: Icon(
//                icon,
//                color: Colors.white,
//              ),
            ),
          ),
          child: image,
        );
    }
    assert(tileStyle != null);
    return null;
  }
}

class RewardsListScreen extends StatefulWidget {
  const RewardsListScreen({Key key}) : super(key: key);

  //static const String routeName = '/material/grid-list';

  @override
  RewardsListScreenState createState() => RewardsListScreenState();
}

class RewardsListScreenState extends State<RewardsListScreen> {
  RewardsTileStyle _tileStyle = RewardsTileStyle.oneLine;

  List<Badge> badges;

  void changeTileStyle(RewardsTileStyle value) {
    setState(() {
      _tileStyle = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    var container = AppStateContainer.of(context);
    badges = container.state.badges;

    final Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Badges'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-1.0, 0),
              end: Alignment(
                  1.0, 0.0), // 10% of the width, so there are ten blinds.
              colors: [
                Theme.of(context).primaryColorDark,
                Theme.of(context).backgroundColor
              ], // whitish to gray
              tileMode:
                  TileMode.repeated, // repeats the gradient over the canvas
            ),
          ),
        ),
        actions: <Widget>[
          PopupMenuButton<RewardsTileStyle>(
            onSelected: changeTileStyle,
            itemBuilder: (BuildContext context) =>
                <PopupMenuItem<RewardsTileStyle>>[
              const PopupMenuItem<RewardsTileStyle>(
                value: RewardsTileStyle.imageOnly,
                child: Text('Image only'),
              ),
              const PopupMenuItem<RewardsTileStyle>(
                value: RewardsTileStyle.oneLine,
                child: Text('One line'),
              ),
              const PopupMenuItem<RewardsTileStyle>(
                value: RewardsTileStyle.twoLine,
                child: Text('Two line'),
              ),
            ],
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SafeArea(
              top: false,
              bottom: false,
              child: GridView.count(
                //crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
                crossAxisCount: 3,
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
                padding: const EdgeInsets.all(4.0),
                childAspectRatio:
                    (orientation == Orientation.portrait) ? 1.0 : 1.3,
                children: badges.map<Widget>((Badge badge) {
                  return RewardsItem(
                    badge: badge,
                    tileStyle: _tileStyle,
                    onBadgeTap: (Badge badge) {
                      setState(() {});
                    },
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
