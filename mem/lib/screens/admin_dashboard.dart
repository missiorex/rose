import 'dart:async';
import 'package:flutter/material.dart';
import 'package:memapp/screens/profile_screen.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:utility/utility.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;
import 'package:memapp/widgets/typedefs.dart';
import 'package:memapp/widgets/message_list.dart';
import 'package:intl/intl.dart';
import 'package:recase/recase.dart';
import 'package:memapp/screens/add_gospel_card_screen.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:memapp/chatroom/chat_screen.dart';
import 'package:memapp/screens/member_list.dart';
import 'package:memapp/screens/rosary_dashboard.dart';

final List<FacingPage> _popUpPages = <FacingPage>[
  FacingPage(
      label: 'Add Gospel Card',
      fabLabel: 'Add Gosepl Card',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.add,
      popUpElement: true),
  FacingPage(
      label: 'Members List',
      fabLabel: 'Members List',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Rosary Dashboard',
      fabLabel: 'Rosary Dashboard',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
];

class AdminScreen extends StatefulWidget {
  final FirebaseStorage storage;
  final bool connectionStatus;
  final bool fcmNotificationEnable;
  final RouteObserver<PageRoute> routeObserver;
  // To send to chat screen
  final List<ChatMessage> guidanceMessages;
  final String lastVisibleChatKey;
  final int lastVisibleChatValue;

  AdminScreen(
      {Key key,
      this.storage,
      this.connectionStatus,
      this.fcmNotificationEnable,
      this.guidanceMessages,
      this.lastVisibleChatKey,
      this.lastVisibleChatValue,
      @required this.routeObserver})
      : super(key: MemKeys.adminScreen);
  @override
  _AdminScreenState createState() => _AdminScreenState();
}

class _AdminScreenState extends State<AdminScreen> {
  List<MemUser> _members = [];
  List<MemUser> _activeMembers = [];
  List<MemUser> _pendingMembers = [];
  List<MemUser> _blockedMembers = [];
  List<MemUser> _adminMembers = [];
  List<MemUser> _activeListSearchResult = [];
  List<MemUser> _adminListSearchResult = [];
  List<MemUser> _pendingListSearchResult = [];
  List<MemUser> _blockedListSearchResult = [];

  Acl _userAcl;
  TextEditingController activeListSearchTextController =
      TextEditingController();
  TextEditingController adminListSearchTextController = TextEditingController();
  TextEditingController pendingListSearchTextController =
      TextEditingController();
  TextEditingController blockedListSearchTextController =
      TextEditingController();
  FacingPage _selectedPopPage = _popUpPages[0];

  DatabaseReference _messagesReference = FirebaseDatabase.instance.reference();
  String _message = "";
  TextEditingController msgController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    activeListSearchTextController.dispose();
    adminListSearchTextController.dispose();
    pendingListSearchTextController.dispose();
    blockedListSearchTextController.dispose();
    msgController.dispose();
    super.dispose();
  }

  void _selectPopPage(FacingPage page) {
    setState(() {
      // Causes the app to rebuild with the new _selectedChoice.
      _selectedPopPage = page;
    });
  }

  void _loadMembers() {
    var container = AppStateContainer.of(context);
    var userBloc = UserProvider.of(context);
    setState(() {
      _members = container.state.members;
      //_userAcl = container.state.userAcl;
//      userBloc.userRepository.users().listen((members) {
//        _members = members.map(User.fromEntity).toList();
//      });
      _activeMembers = _members.where((i) => i.status).toList();
      _pendingMembers =
          _members.where((i) => !i.status && !i.isBlocked).toList();
      _blockedMembers = _members.where((i) => i.isBlocked).toList();
      _adminMembers = _members.where((i) => i.role == "admin").toList();
    });
  }

  Widget _buildActiveMemberListTile(BuildContext context, int index) {
    var member = _activeMembers[index];

    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);
    var iconSize = MediaQuery.of(context).size.width * 0.05;

    return Card(
        color: member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
            ? member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -60
                ? Colors.red[500]
                : Colors.amber[200]
            : Colors.white,
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          ListTile(
            onTap: () => _navigateToMemberDetails(member, index),
            leading: Hero(
              tag: index,
              child: CircleAvatar(
                backgroundImage:
                    CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                        NetworkImage(member.profilePhotoUrl ?? "") ??
                        AssetImage("assets/placeholder-face.png"),
              ),
            ),
            title: Row(children: <Widget>[
              Text(member.name),
              Text(member.name != member.displayName
                  ? " ( " + member.displayName + " )"
                  : ""),
              Text(
                member.role == "user" ? "" : "  ~Admin",
                style: TextStyle(color: Colors.red, fontSize: 8.0),
              )
            ]),
            subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Phone: " + member.phoneNumber),
                  Text(
                      "Last Rosary On: " +
                          DateFormat.yMMMd().format(member.rosaryLastAddedOn),
                      style: TextStyle(color: Colors.black87, fontSize: 8.0)),
                ]),
          ),
          ButtonBar(
            children: <Widget>[
              FlatButton(
                onPressed: member.id != userBloc.userDetails.id
                    ? () {
                        _launchURL("tel:" + member.phoneNumber);
                      }
                    : null,
                child: Icon(Icons.phone_forwarded, size: 16.0),
              ),
            ],
          ),
          ButtonBar(
            children: <Widget>[
              member.title == "Fr." ||
                      member.title == "Sr." ||
                      member.title == "Most Rev."
                  ? IconTheme(
                      data: IconThemeData(color: Colors.redAccent),
                      child: ImageIcon(
                        AssetImage("assets/cross-icon.png"),
                        size: 15.5,
                      ))
                  : Container(),
              member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
                  ? FlatButton(
                      onPressed: member.id != userBloc.userDetails.id
                          ? () {
                              String _title = MemLists().getRandomListElement(
                                  listType: "rosary_reminder_title",
                                  locale: member.locale);
                              String _body = MemLists().getRandomListElement(
                                  listType: "rosary_reminder_body",
                                  locale: member.locale);

                              FCMNotification notification = FCMNotification(
                                  title: _title,
                                  body: _body,
                                  topic: "rosary_reminder",
                                  token: member.pushNotificationToken ?? "",
                                  author: userBloc.userDetails.name ?? "",
                                  time: DateTime.now(),
                                  authorID: userBloc.userDetails.id ?? "",
                                  priority: "medium");

                              messageBloc.messageRepository
                                  .addNotification(notification.toEntity());

                              MEMAction rosaryInviteAction = MEMAction(
                                  author: userBloc.userDetails.name,
                                  notified: false,
                                  priority: "high",
                                  summary: _title,
                                  time: DateTime.now().millisecondsSinceEpoch,
                                  title: _title,
                                  body: _body,
                                  topic: "big_pic");

                              messageBloc.messageRepository.addUserAction(
                                  member.id, rosaryInviteAction.toEntity());
                            }
                          : null,
                      child: Icon(Icons.notifications_active,
                          size: 16.0, color: Colors.black87),
                    )
                  : Container(),
              IconButton(
                  color: Colors.grey,
                  icon: ImageIcon(
                      AssetImage('assets/icons/counselling-icon.png'),
                      size: iconSize),
                  onPressed: (() {
//                    Navigator.push(
//                        context,
//                        MaterialPageRoute(
//                          builder: (context) => PrivateChatScreen(
//                            appState: AppStateContainer.of(context).state,
//                            userBloc: userBloc,
//                            routeObserver: widget.routeObserver,
//                            connectionStatus: widget.connectionStatus,
//                            bgImageUrl: "assets/bg-star.png",
//                            chatReceiverId: userBloc.userDetails.id,
//                            chatRequesterID: member.id,
//                            chatRequesterName: member.name,
//                            chatRequestTime:
//                                DateTime.now().millisecondsSinceEpoch,
//                            chatType: "admin",
//                          ),
//                        ));
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChatScreen(
                            userBloc: userBloc,
                            bgImageUrl: "assets/bg-admin.jpg",
                            connectionStatus: widget.connectionStatus,
                            publicChat: false,
                            chatRoomID: member.id,
                            fcmNotificationEnable: false,
                            routeObserver: widget.routeObserver,
                            messages: widget.guidanceMessages,
                            lastVisibleChatKey: widget.lastVisibleChatKey,
                            lastVisibleChatValue: widget.lastVisibleChatValue,
                          ),
                        ));
                  })),
              FlatButton(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    member.role == "user" ? "Make Admin" : "Revoke Admin",
                    style: new TextStyle(
                      color: member.role == "user" ? Colors.blue : Colors.red,
                    ),
                  ),

                  // Admin Privilege to Super Admin's with the special privilege to grant admin role in user acl.
                  onPressed: member.role == "user" &&
                          userBloc.userDetails.isSuperAdmin
//                      &&
//                          container.state.userAcl.grantAdminRole
                      ? member.id != userBloc.userDetails.id
                          ? () {
                              /* ... */
                              setState(() {
                                userBloc.userRepository
                                    .assignAdminRole(member.toEntity());
                              });

                              showSnackMessage(
                                  context, member.name + " is now an admin ",
                                  color: Colors.blueGrey, actionText: "Okay");
                            }
                          : null
                      : member.role == "admin" &&
                              userBloc.userDetails.isSuperAdmin
//                      &&
//                              container.state.userAcl.revokeAdminRole
                          ? member.id != userBloc.userDetails.id
                              ? () {
                                  /* ... */
                                  setState(() {
                                    userBloc.userRepository
                                        .revokeAdminRole(member.toEntity());
                                  });

                                  showSnackMessage(context,
                                      member.name + " is now a normal user ",
                                      color: Colors.blueGrey,
                                      actionText: "Okay");
                                }
                              : null
                          : null),
              FlatButton(
                child: const Text(
                  'Deactivate',
                  style: TextStyle(color: Colors.black87),
                ),
                onPressed: userBloc.userDetails.role == "admin"
//                    &&
//                        container.state.userAcl.deactivateUser
                    ? member.id != userBloc.userDetails.id
                        ? () {
                            /* ... */
                            setState(() {
                              userBloc.userRepository.deActivateUser(member.id);
                            });
                            showSnackMessage(
                                context,
                                member.name +
                                    " is deactivated and is available in pending list ",
                                color: Colors.red,
                                actionText: "Okay");
                          }
                        : null
                    : null,
              ),
            ],
          ),
        ]));
  }

  Widget _buildSearchActiveMemberListTile(BuildContext context, int index) {
    var member = _activeListSearchResult[index];

    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);
    var iconSize = MediaQuery.of(context).size.width * 0.05;

    return Card(
        color: member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
            ? member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -60
                ? Colors.red[500]
                : Colors.amber[200]
            : Colors.white,
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          ListTile(
            onTap: () => _navigateToMemberDetails(member, index),
            leading: Hero(
              tag: index,
              child: CircleAvatar(
                backgroundImage:
                    CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                        NetworkImage(member.profilePhotoUrl ?? "") ??
                        AssetImage("assets/placeholder-face.png"),
              ),
            ),
            title: Row(children: <Widget>[
              Text(member.name),
              Text(member.name != member.displayName
                  ? " ( " + member.displayName + " )"
                  : ""),
              Text(
                member.role == "user" ? "" : "  ~Admin",
                style: TextStyle(color: Colors.red, fontSize: 8.0),
              )
            ]),
            subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Phone: " + member.phoneNumber),
                  Text(
                      "Last Rosary On: " +
                          DateFormat.yMMMd().format(member.rosaryLastAddedOn),
                      style: TextStyle(color: Colors.black87, fontSize: 8.0)),
                ]),
          ),
          ButtonBar(
            children: <Widget>[
              FlatButton(
                onPressed: member.id != userBloc.userDetails.id
                    ? () {
                        _launchURL("tel:" + member.phoneNumber);
                      }
                    : null,
                child: Icon(Icons.phone_forwarded, size: 16.0),
              ),
            ],
          ),
          ButtonBar(
            children: <Widget>[
              member.title == "Fr." ||
                      member.title == "Sr." ||
                      member.title == "Most Rev."
                  ? IconTheme(
                      data: IconThemeData(color: Colors.redAccent),
                      child: ImageIcon(
                        AssetImage("assets/cross-icon.png"),
                        size: 15.5,
                      ))
                  : Container(),
              member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
                  ? FlatButton(
                      onPressed: member.id != userBloc.userDetails.id
                          ? () {
                              String _title = MemLists().getRandomListElement(
                                  listType: "rosary_reminder_title",
                                  locale: member.locale);
                              String _body = MemLists().getRandomListElement(
                                  listType: "rosary_reminder_body",
                                  locale: member.locale);

                              FCMNotification notification = FCMNotification(
                                  title: _title,
                                  body: _body,
                                  topic: "rosary_reminder",
                                  token: member.pushNotificationToken ?? "",
                                  author: userBloc.userDetails.name ?? "",
                                  time: DateTime.now(),
                                  authorID: userBloc.userDetails.id ?? "",
                                  priority: "medium");

                              messageBloc.messageRepository
                                  .addNotification(notification.toEntity());

                              MEMAction rosaryInviteAction = MEMAction(
                                  author: userBloc.userDetails.name,
                                  notified: false,
                                  priority: "high",
                                  summary: _title,
                                  time: DateTime.now().millisecondsSinceEpoch,
                                  title: _title,
                                  body: _body,
                                  topic: "big_pic");

                              messageBloc.messageRepository.addUserAction(
                                  member.id, rosaryInviteAction.toEntity());
                            }
                          : null,
                      child: Icon(
                        Icons.notifications_active,
                        size: 16.0,
                      ),
                    )
                  : Container(),
              IconButton(
                  color: Colors.grey,
                  icon: ImageIcon(
                      AssetImage('assets/icons/counselling-icon.png'),
                      size: iconSize),
                  onPressed: (() {
//                    Navigator.push(
//                        context,
//                        MaterialPageRoute(
//                          builder: (context) => PrivateChatScreen(
//                            appState: AppStateContainer.of(context).state,
//                            userBloc: userBloc,
//                            routeObserver: widget.routeObserver,
//                            connectionStatus: widget.connectionStatus,
//                            bgImageUrl: "assets/bg-star.png",
//                            chatReceiverId: userBloc.userDetails.id,
//                            chatRequesterID: member.id,
//                            chatRequesterName: member.name,
//                            chatRequestTime:
//                                DateTime.now().millisecondsSinceEpoch,
//                            chatType: "admin",
//                          ),
//                        ));
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChatScreen(
                            userBloc: userBloc,
                            bgImageUrl: "assets/bg-admin.jpg",
                            connectionStatus: widget.connectionStatus,
                            publicChat: false,
                            chatRoomID: member.id,
                            fcmNotificationEnable: false,
                            routeObserver: widget.routeObserver,
                            messages: widget.guidanceMessages,
                            lastVisibleChatKey: widget.lastVisibleChatKey,
                            lastVisibleChatValue: widget.lastVisibleChatValue,
                          ),
                        ));
                  })),
              FlatButton(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    member.role == "user" ? "Make Admin" : "Revoke Admin",
                    style: new TextStyle(
                      color: member.role == "user" ? Colors.blue : Colors.red,
                    ),
                  ),

                  // Admin Privilege to Super Admin's with the special privilege to grant admin role in user acl.
                  onPressed: member.role == "user" &&
                          userBloc.userDetails.isSuperAdmin
//                      &&
//                          container.state.userAcl.grantAdminRole
                      ? member.id != userBloc.userDetails.id
                          ? () {
                              /* ... */
                              setState(() {
                                userBloc.userRepository
                                    .assignAdminRole(member.toEntity());
                              });

                              showSnackMessage(
                                  context, member.name + " is now an admin ",
                                  color: Colors.blueGrey, actionText: "Okay");
                            }
                          : null
                      : member.role == "admin" &&
                              userBloc.userDetails.isSuperAdmin
//                      &&
//                              container.state.userAcl.revokeAdminRole
                          ? member.id != userBloc.userDetails.id
                              ? () {
                                  /* ... */
                                  setState(() {
                                    userBloc.userRepository
                                        .revokeAdminRole(member.toEntity());
                                  });

                                  showSnackMessage(context,
                                      member.name + " is now a normal user ",
                                      color: Colors.blueGrey,
                                      actionText: "Okay");
                                }
                              : null
                          : null),
              FlatButton(
                child: const Text('Deactivate'),
                onPressed: userBloc.userDetails.role == "admin"
//                    &&
//                        container.state.userAcl.deactivateUser
                    ? member.id != userBloc.userDetails.id
                        ? () {
                            /* ... */
                            setState(() {
                              userBloc.userRepository.deActivateUser(member.id);
                            });
                            showSnackMessage(
                                context,
                                member.name +
                                    " is deactivated and is available in pending list ",
                                color: Colors.red,
                                actionText: "Okay");
                          }
                        : null
                    : null,
              ),
            ],
          ),
        ]));
  }

  Widget _buildAdminMemberListTile(BuildContext context, int index) {
    var member = _adminMembers[index];

    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);

    return Card(
        color: member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
            ? member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -60
                ? Colors.red[500]
                : Colors.amber[200]
            : Colors.white,
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          ListTile(
            onTap: () => _navigateToMemberDetails(member, index),
            leading: Hero(
              tag: index,
              child: CircleAvatar(
                backgroundImage:
                    CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                        NetworkImage(member.profilePhotoUrl ?? "") ??
                        AssetImage("assets/placeholder-face.png"),
              ),
            ),
            title: Row(children: <Widget>[
              Text(member.name),
              Text(member.name != member.displayName
                  ? " ( " + member.displayName + " )"
                  : ""),
              Text(
                member.role == "user" ? "" : "  ~Admin",
                style: TextStyle(color: Colors.red, fontSize: 12.0),
              )
            ]),
            subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Phone: " + member.phoneNumber),
//                  Text("Last Rosary On: " +
//                      new DateFormat.yMMMd().format(member.rosaryLastAddedOn)),
                ]),
          ),
          ButtonBar(
            children: <Widget>[
//                member.title == "Fr." ||
//                        member.title == "Sr." ||
//                        member.title == "Most Rev."
//                    ? IconTheme(
//                        data: IconThemeData(color: Colors.redAccent),
//                        child: ImageIcon(
//                          AssetImage("assets/cross-icon.png"),
//                          size: 15.5,
//                        ))
//                    : Container(),
//                member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
//                    ? FlatButton(
//                        onPressed: member.id != userBloc.userDetails.id
//                            ? () {
//                                FCMNotification notification = FCMNotification(
//                                    title: "No Rosaries for a while",
//                                    body:
//                                        "Please recite rosaries and offer at MEMAPP",
//                                    topic: "rosary_reminder",
//                                    token: member.pushNotificationToken ?? "",
//                                    author: userBloc.userDetails.name ?? "",
//                                    time: DateTime.now(),
//                                    authorID: userBloc.userDetails.id ?? "",
//                                    priority: "medium");
//
//                                messageBloc.messageRepository
//                                    .addNotification(notification.toEntity());
//                              }
//                            : null,
//                        child: Icon(Icons.notifications_active),
//                      )
//                    : Container(),
              FlatButton(
                onPressed: member.id != userBloc.userDetails.id
                    ? () {
                        _launchURL("tel:" + member.phoneNumber);
                      }
                    : null,
                child: Icon(Icons.phone_forwarded, size: 16.0),
              ),
              FlatButton(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    member.role == "user" ? "Make Admin" : "Revoke Admin",
                    style: TextStyle(
                      color: member.role == "user" ? Colors.blue : Colors.red,
                    ),
                  ),

                  // Admin Privilege to Super Admin's with the special privilege to grant admin role in user acl.
                  onPressed: member.role == "user" &&
                          userBloc.userDetails.isSuperAdmin
//                      &&
//                          container.state.userAcl.grantAdminRole
                      ? member.id != userBloc.userDetails.id
                          ? () {
                              /* ... */
                              setState(() {
                                userBloc.userRepository
                                    .assignAdminRole(member.toEntity());
                              });

                              showSnackMessage(
                                  context, member.name + " is now an admin ",
                                  color: Colors.blueGrey, actionText: "Okay");
                            }
                          : null
                      : member.role == "admin" &&
                              userBloc.userDetails.isSuperAdmin
//                      &&
//                              container.state.userAcl.revokeAdminRole
                          ? member.id != userBloc.userDetails.id
                              ? () {
                                  /* ... */
                                  setState(() {
                                    userBloc.userRepository
                                        .revokeAdminRole(member.toEntity());
                                  });

                                  showSnackMessage(context,
                                      member.name + " is now a normal user ",
                                      color: Colors.blueGrey,
                                      actionText: "Okay");
                                }
                              : null
                          : null),
              FlatButton(
                child: const Text('Deactivate'),
                onPressed: userBloc.userDetails.role == "admin"
//                    &&
//                        container.state.userAcl.deactivateUser
                    ? member.id != userBloc.userDetails.id
                        ? () {
                            /* ... */
                            setState(() {
                              userBloc.userRepository.deActivateUser(member.id);
                            });
                            showSnackMessage(
                                context,
                                member.name +
                                    " is deactivated and is available in pending list ",
                                color: Colors.red,
                                actionText: "Okay");
                          }
                        : null
                    : null,
              ),
            ],
          ),
        ]));
  }

  Widget _buildSearchAdminMemberListTile(BuildContext context, int index) {
    var member = _adminListSearchResult[index];

    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);

    return Card(
        color: member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
            ? member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -60
                ? Colors.red[500]
                : Colors.amber[200]
            : Colors.white,
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          ListTile(
            onTap: () => _navigateToMemberDetails(member, index),
            leading: Hero(
              tag: index,
              child: CircleAvatar(
                backgroundImage:
                    CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                        NetworkImage(member.profilePhotoUrl ?? "") ??
                        AssetImage("assets/placeholder-face.png"),
              ),
            ),
            title: Row(children: <Widget>[
              Text(member.name),
              Text(member.name != member.displayName
                  ? " ( " + member.displayName + " )"
                  : ""),
              Text(
                member.role == "user" ? "" : "  ~Admin",
                style: TextStyle(color: Colors.red, fontSize: 12.0),
              )
            ]),
            subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Phone: " + member.phoneNumber),
//                  Text("Last Rosary On: " +
//                      new DateFormat.yMMMd().format(member.rosaryLastAddedOn)),
                ]),
          ),
          ButtonBar(
            children: <Widget>[
//                member.title == "Fr." ||
//                        member.title == "Sr." ||
//                        member.title == "Most Rev."
//                    ? IconTheme(
//                        data: IconThemeData(color: Colors.redAccent),
//                        child: ImageIcon(
//                          AssetImage("assets/cross-icon.png"),
//                          size: 15.5,
//                        ))
//                    : Container(),
//                member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
//                    ? FlatButton(
//                        onPressed: member.id != userBloc.userDetails.id
//                            ? () {
//                                FCMNotification notification = FCMNotification(
//                                    title: "No Rosaries for a while",
//                                    body:
//                                        "Please recite rosaries and offer at MEMAPP",
//                                    topic: "rosary_reminder",
//                                    token: member.pushNotificationToken ?? "",
//                                    author: userBloc.userDetails.name ?? "",
//                                    time: DateTime.now(),
//                                    authorID: userBloc.userDetails.id ?? "",
//                                    priority: "medium");
//
//                                messageBloc.messageRepository
//                                    .addNotification(notification.toEntity());
//                              }
//                            : null,
//                        child: Icon(Icons.notifications_active),
//                      )
//                    : Container(),
              FlatButton(
                onPressed: member.id != userBloc.userDetails.id
                    ? () {
                        _launchURL("tel:" + member.phoneNumber);
                      }
                    : null,
                child: Icon(Icons.phone_forwarded, size: 16.0),
              ),
              FlatButton(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    member.role == "user" ? "Make Admin" : "Revoke Admin",
                    style: TextStyle(
                      color: member.role == "user" ? Colors.blue : Colors.red,
                    ),
                  ),

                  // Admin Privilege to Super Admin's with the special privilege to grant admin role in user acl.
                  onPressed: member.role == "user" &&
                          userBloc.userDetails.isSuperAdmin
//                      &&
//                          container.state.userAcl.grantAdminRole
                      ? member.id != userBloc.userDetails.id
                          ? () {
                              /* ... */
                              setState(() {
                                userBloc.userRepository
                                    .assignAdminRole(member.toEntity());
                              });

                              showSnackMessage(
                                  context, member.name + " is now an admin ",
                                  color: Colors.blueGrey, actionText: "Okay");
                            }
                          : null
                      : member.role == "admin" &&
                              userBloc.userDetails.isSuperAdmin
//                      &&
//                              container.state.userAcl.revokeAdminRole
                          ? member.id != userBloc.userDetails.id
                              ? () {
                                  /* ... */
                                  setState(() {
                                    userBloc.userRepository
                                        .revokeAdminRole(member.toEntity());
                                  });

                                  showSnackMessage(context,
                                      member.name + " is now a normal user ",
                                      color: Colors.blueGrey,
                                      actionText: "Okay");
                                }
                              : null
                          : null),
              FlatButton(
                child: const Text(
                  'Deactivate',
                  style: TextStyle(color: Colors.black87),
                ),
                onPressed: userBloc.userDetails.role == "admin"
//                    "" &&
//                        container.state.userAcl.deactivateUser
                    ? member.id != userBloc.userDetails.id
                        ? () {
                            /* ... */
                            setState(() {
                              userBloc.userRepository.deActivateUser(member.id);
                            });
                            showSnackMessage(
                                context,
                                member.name +
                                    " is deactivated and is available in pending list ",
                                color: Colors.red,
                                actionText: "Okay");
                          }
                        : null
                    : null,
              ),
            ],
          ),
        ]));
  }

  Widget _buildPendingMemberListTile(BuildContext context, int index) {
    var member = _pendingMembers[index];
    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);

    return Card(
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      ListTile(
        onTap: () => _navigateToMemberDetails(member, index),
        leading: Hero(
          tag: index,
          child: CircleAvatar(
            backgroundImage:
                CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                    NetworkImage(member.profilePhotoUrl ?? "") ??
                    AssetImage("assets/placeholder-face.png"),
          ),
        ),
        title: Row(children: <Widget>[
          Text(member.name),
          Text(member.name != member.displayName
              ? " ( " + member.displayName + " )"
              : ""),
          Text(
            member.role == "user" ? "" : "  ~Admin",
            style: TextStyle(color: Colors.red, fontSize: 12.0),
          )
        ]),
        subtitle: Text("Phone: " + member.phoneNumber),
      ),
      ButtonBar(
        children: <Widget>[
          member.title == "Fr." ||
                  member.title == "Sr." ||
                  member.title == "Most Rev."
              ? IconTheme(
                  data: IconThemeData(color: Colors.redAccent),
                  child: ImageIcon(
                    AssetImage("assets/cross-icon.png"),
                    size: 15.5,
                  ))
              : Container(),
          FlatButton(
              onPressed: member.id != userBloc.userDetails.id
                  ? () {
                      _launchURL("tel:" + member.phoneNumber);
                    }
                  : null,
              child: Icon(
                Icons.phone_forwarded,
                size: 16.0,
              )),
          FlatButton(
            child: const Text('Activate'),
            onPressed: member.id != userBloc.userDetails.id &&
                    userBloc.userDetails.role == "admin"
//                &&
//                    container.state.userAcl.activateUser
                ? !member.isBlocked
                    ? () {
                        /* ... */

                        setState(() {
                          userBloc.userRepository.activateUser(member.id);
                        });

                        FCMNotification notification = FCMNotification(
                            title: "Your MEM account is active now",
                            body:
                                "Welcome to MEM, Please open MEM app and login.",
                            token: member.pushNotificationToken ?? "",
                            topic: "user_active",
                            author: "Admin" ?? "",
                            time: DateTime.now(),
                            authorID: userBloc.userDetails.id ?? "",
                            priority: "medium");

                        messageBloc.messageRepository
                            .addNotification(notification.toEntity());

                        // Disabling New member welcome message in chat room and notification on user request
                        //_sendMsgtoChatRoom(member);

                        showSnackMessage(context,
                            member.name + " is activated and can log in. ",
                            color: Colors.blueGrey, actionText: "Okay");

//                            _pendingMembers.where((i) {
//                              i.id == member.id ? i.status == true : null;
//                            });
                      }
                    : null
                : null,
          ),
          FlatButton(
            child: const Text('Block'),
            onPressed: member.id != userBloc.userDetails.id &&
                    userBloc.userDetails.role == "admin"
//                &&
//                    container.state.userAcl.blockUser
                ? !member.isBlocked
                    ? () {
                        /* ... */
                        setState(() {
                          userBloc.userRepository.blockUser(member.id);
                        });
                        showSnackMessage(
                            context,
                            member.name +
                                " is blocked, and is available in blocked list",
                            color: Colors.red,
                            actionText: "Okay");
                      }
                    : null
                : null,
          ),
        ],
      ),
    ]));
  }

  Widget _buildSearchPendingMemberListTile(BuildContext context, int index) {
    var member = _pendingListSearchResult[index];
    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);

    return Card(
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      ListTile(
        onTap: () => _navigateToMemberDetails(member, index),
        leading: Hero(
          tag: index,
          child: CircleAvatar(
            backgroundImage:
                CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                    NetworkImage(member.profilePhotoUrl ?? "") ??
                    AssetImage("assets/placeholder-face.png"),
          ),
        ),
        title: Row(children: <Widget>[
          Text(member.name),
          Text(member.name != member.displayName
              ? "( " + member.displayName + " )"
              : ""),
          Text(
            member.role == "user" ? "" : "  ~Admin",
            style: TextStyle(color: Colors.red, fontSize: 12.0),
          )
        ]),
        subtitle: Text("Phone: " + member.phoneNumber),
      ),
      ButtonBar(
        children: <Widget>[
          member.title == "Fr." ||
                  member.title == "Sr." ||
                  member.title == "Most Rev."
              ? IconTheme(
                  data: IconThemeData(color: Colors.redAccent),
                  child: ImageIcon(
                    AssetImage("assets/cross-icon.png"),
                    size: 15.5,
                  ))
              : Container(),
          FlatButton(
              onPressed: member.id != userBloc.userDetails.id
                  ? () {
                      _launchURL("tel:" + member.phoneNumber);
                    }
                  : null,
              child: Icon(
                Icons.phone_forwarded,
                size: 16.0,
              )),
          FlatButton(
            child: const Text('Activate'),
            onPressed: member.id != userBloc.userDetails.id &&
                    userBloc.userDetails.role == "admin"
//                &&
//                    container.state.userAcl.activateUser
                ? !member.isBlocked
                    ? () {
                        /* ... */

                        setState(() {
                          userBloc.userRepository.activateUser(member.id);
                        });

                        FCMNotification notification = FCMNotification(
                            title: "Your MEM account is active now",
                            body:
                                "Welcome to MEM, Please open MEM app and login.",
                            topic: "user_active",
                            token: member.pushNotificationToken ?? "",
                            author: userBloc.userDetails.name ?? "",
                            time: DateTime.now(),
                            authorID: userBloc.userDetails.id ?? "",
                            priority: "medium");

                        messageBloc.messageRepository
                            .addNotification(notification.toEntity());

                        showSnackMessage(context,
                            member.name + " is activated and can log in. ",
                            color: Colors.blueGrey, actionText: "Okay");

//                            _pendingMembers.where((i) {
//                              i.id == member.id ? i.status == true : null;
//                            });
                      }
                    : null
                : null,
          ),
          FlatButton(
            child: const Text('Block'),
            onPressed: member.id != userBloc.userDetails.id &&
                    userBloc.userDetails.role == "admin"
//                &&
//                    container.state.userAcl.blockUser
                ? !member.isBlocked
                    ? () {
                        /* ... */
                        setState(() {
                          userBloc.userRepository.blockUser(member.id);
                        });
                        showSnackMessage(
                            context,
                            member.name +
                                " is blocked, and is available in blocked list",
                            color: Colors.red,
                            actionText: "Okay");
                      }
                    : null
                : null,
          ),
        ],
      ),
    ]));
  }

  Widget _buildBlockedMemberListTile(BuildContext context, int index) {
    var member = _blockedMembers[index];
    UserBloc userBloc = UserProvider.of(context);
    var container = AppStateContainer.of(context);

    return Card(
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      ListTile(
        onTap: () => _navigateToMemberDetails(member, index),
        leading: Hero(
          tag: index,
          child: CircleAvatar(
            backgroundImage:
                CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                    NetworkImage(member.profilePhotoUrl ?? "") ??
                    AssetImage("assets/placeholder-face.png"),
          ),
        ),
        title: Row(children: <Widget>[
          Text(member.name),
          Text(member.name != member.displayName
              ? " ( " + member.displayName + " )"
              : ""),
          Text(
            member.role == "user" ? "" : "  ~Admin",
            style: TextStyle(color: Colors.red, fontSize: 12.0),
          )
        ]),
        subtitle: Text("Phone: " + member.phoneNumber),
      ),
      ButtonTheme(
        // make buttons use the appropriate styles for cards
        child: ButtonBar(
          children: <Widget>[
            member.title == "Fr." ||
                    member.title == "Sr." ||
                    member.title == "Most Rev."
                ? IconTheme(
                    data: IconThemeData(color: Colors.redAccent),
                    child: ImageIcon(
                      AssetImage("assets/cross-icon.png"),
                      size: 15.5,
                    ))
                : Container(),
            FlatButton(
                onPressed: member.id != userBloc.userDetails.id
                    ? () {
                        _launchURL("tel:" + member.phoneNumber);
                      }
                    : null,
                child: Icon(Icons.phone_forwarded, size: 16.0)),
            FlatButton(
              child: const Text('Delete'),
              onPressed: member.id != userBloc.userDetails.id &&
                      userBloc.userDetails.role == "admin"
//                  &&
//                      container.state.userAcl.deleteUser
                  ? () {
                      setState(() {
                        userBloc.userRepository
                            .deleteSingleUser(member.toEntity());
                      });
                      showSnackMessage(
                          context, member.name + " is deleted permanently",
                          color: Colors.red, actionText: "Okay");
                      /* ... */
//                      showAlert(
//                          errorMsg: MemError(
//                              title: "You are about to delete a user",
//                              body: """You are about to delete a user : """ +
//                                  member.name +
//                                  """"
//                    Are you sure ? """),
//                          confirmFn: confirmAndDelete(member, userBloc),
//                          cancelFn: () {
//                            Navigator.pop(context);
//                          });
//
//                      Navigator.pop(context);
                    }
                  : null,
            ),
            FlatButton(
              child: const Text('UnBlock'),
              onPressed: member.id != userBloc.userDetails.id &&
                      userBloc.userDetails.role == "admin"
//                  &&
//                      container.state.userAcl.unBlockUser
                  ? () {
                      /* ... */

                      setState(() {
                        userBloc.userRepository.unBlockUser(member.id);
                      });
                      showSnackMessage(
                          context,
                          member.name +
                              " is unblocked and is available in pending list ",
                          color: Colors.blueGrey,
                          actionText: "Okay");
                    }
                  : null,
            ),
          ],
        ),
      ),
    ]));
  }

  Widget _buildSearchBlockedMemberListTile(BuildContext context, int index) {
    var member = _blockedListSearchResult[index];
    UserBloc userBloc = UserProvider.of(context);
    var container = AppStateContainer.of(context);
    var iconSize = MediaQuery.of(context).size.width * 0.05;

    return Card(
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      ListTile(
        onTap: () => _navigateToMemberDetails(member, index),
        leading: Hero(
          tag: index,
          child: CircleAvatar(
            backgroundImage:
                CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                    NetworkImage(member.profilePhotoUrl ?? "") ??
                    AssetImage("assets/placeholder-face.png"),
          ),
        ),
        title: Row(children: <Widget>[
          Text(member.name),
          Text(member.name != member.displayName
              ? " ( " + member.displayName + " )"
              : ""),
          Text(
            member.role == "user" ? "" : "  ~Admin",
            style: TextStyle(color: Colors.red, fontSize: 12.0),
          )
        ]),
        subtitle: Text("Phone: " + member.phoneNumber),
      ),
      ButtonTheme(
        // make buttons use the appropriate styles for cards
        child: ButtonBar(
          children: <Widget>[
            member.title == "Fr." ||
                    member.title == "Sr." ||
                    member.title == "Most Rev."
                ? IconTheme(
                    data: IconThemeData(color: Colors.redAccent),
                    child: ImageIcon(
                      AssetImage("assets/cross-icon.png"),
                      size: 15.5,
                    ))
                : Container(),
            FlatButton(
                onPressed: member.id != userBloc.userDetails.id
                    ? () {
                        _launchURL("tel:" + member.phoneNumber);
                      }
                    : null,
                child: ImageIcon(AssetImage('assets/icons/icons8-phone.png'),
                    size: iconSize)),
            FlatButton(
              child: const Text('Delete'),
              onPressed: member.id != userBloc.userDetails.id &&
                      userBloc.userDetails.role == "admin"
//                  &&
//                      container.state.userAcl.deleteUser
                  ? () {
                      setState(() {
                        userBloc.userRepository
                            .deleteSingleUser(member.toEntity());
                      });
                      showSnackMessage(
                          context, member.name + " is deleted permanently",
                          color: Colors.red, actionText: "Okay");
                      /* ... */
//                      showAlert(
//                          errorMsg: MemError(
//                              title: "You are about to delete a user",
//                              body: """You are about to delete a user : """ +
//                                  member.name +
//                                  """"
//                    Are you sure ? """),
//                          confirmFn: confirmAndDelete(member, userBloc),
//                          cancelFn: () {
//                            Navigator.pop(context);
//                          });
//
//                      Navigator.pop(context);
                    }
                  : null,
            ),
            FlatButton(
              child: const Text('UnBlock'),
              onPressed: member.id != userBloc.userDetails.id &&
                      userBloc.userDetails.role == "admin"
//                  &&
//                      container.state.userAcl.unBlockUser
                  ? () {
                      /* ... */

                      setState(() {
                        userBloc.userRepository.unBlockUser(member.id);
                      });
                      showSnackMessage(
                          context,
                          member.name +
                              " is unblocked and is available in pending list ",
                          color: Colors.blueGrey,
                          actionText: "Okay");
                    }
                  : null,
            ),
          ],
        ),
      ),
    ]));
  }

  void _navigateToMemberDetails(MemUser member, Object avatarTag) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (c) {
          return ProfileScreen(
              member: member, avatarTag: avatarTag, storage: widget.storage);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _loadMembers();
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);

    Widget content;

    if (_members.isEmpty) {
      content = Center(
        child: CircularProgressIndicator(),
      );
    } else {
      content = Container(
          color: Theme.of(context).primaryColorLight,
//          decoration: BoxDecoration(
//              image: DecorationImage(
//                  image: AssetImage("assets/bg-drk  -ptn.png"),
//                  repeat: ImageRepeat.repeat)),
          child: TabBarView(children: [
            Column(children: <Widget>[
              Container(
                color: Colors.brown,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: ListTile(
                      leading: Icon(Icons.search),
                      title: TextField(
                        controller: activeListSearchTextController,
                        decoration: InputDecoration(
                            hintText: 'Search', border: InputBorder.none),
                        onChanged: onActiveListSearchTextChanged,
                      ),
                      trailing: IconButton(
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          activeListSearchTextController.clear();
                          onActiveListSearchTextChanged('');
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height * 0.9,
                      child: _activeListSearchResult.length != 0 ||
                              activeListSearchTextController.text.isNotEmpty
                          ? ListView.builder(
                              itemCount: _activeListSearchResult.length,
                              itemBuilder: _buildSearchActiveMemberListTile,
                            )
                          : RefreshIndicator(
                              onRefresh: _refreshAdminDashBoard,
                              child: ListView.builder(
                                itemCount: _activeMembers.length,
                                itemBuilder: _buildActiveMemberListTile,
                              ))))
            ]),
            Column(children: <Widget>[
              Container(
                color: Colors.brown,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: ListTile(
                      leading: Icon(Icons.search),
                      title: TextField(
                        controller: pendingListSearchTextController,
                        decoration: InputDecoration(
                            hintText: 'Search', border: InputBorder.none),
                        onChanged: onPendingListSearchTextChanged,
                      ),
                      trailing: IconButton(
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          pendingListSearchTextController.clear();
                          onPendingListSearchTextChanged('');
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height * 0.9,
                      child: _pendingListSearchResult.length != 0 ||
                              pendingListSearchTextController.text.isNotEmpty
                          ? ListView.builder(
                              itemCount: _pendingListSearchResult.length,
                              itemBuilder: _buildSearchPendingMemberListTile,
                            )
                          : RefreshIndicator(
                              onRefresh: _refreshAdminDashBoard,
                              child: ListView.builder(
                                itemCount: _pendingMembers.length,
                                itemBuilder: _buildPendingMemberListTile,
                              ))))
            ]),
            Column(children: <Widget>[
              Container(
                color: Colors.brown,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: ListTile(
                      leading: Icon(Icons.search),
                      title: TextField(
                        controller: blockedListSearchTextController,
                        decoration: InputDecoration(
                            hintText: 'Search', border: InputBorder.none),
                        onChanged: onBlockedListSearchTextChanged,
                      ),
                      trailing: IconButton(
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          blockedListSearchTextController.clear();
                          onBlockedListSearchTextChanged('');
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height * 0.9,
                      child: _blockedListSearchResult.length != 0 ||
                              blockedListSearchTextController.text.isNotEmpty
                          ? ListView.builder(
                              itemCount: _blockedListSearchResult.length,
                              itemBuilder: _buildSearchBlockedMemberListTile,
                            )
                          : RefreshIndicator(
                              onRefresh: _refreshAdminDashBoard,
                              child: ListView.builder(
                                itemCount: _blockedMembers.length,
                                itemBuilder: _buildBlockedMemberListTile,
                              ))))
            ]),
            Column(children: <Widget>[
              Container(
                color: Colors.brown,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: ListTile(
                      leading: Icon(Icons.search),
                      title: TextField(
                        controller: adminListSearchTextController,
                        decoration: InputDecoration(
                            hintText: 'Search', border: InputBorder.none),
                        onChanged: onAdminListSearchTextChanged,
                      ),
                      trailing: IconButton(
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          adminListSearchTextController.clear();
                          onAdminListSearchTextChanged('');
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height * 0.9,
                      child: _adminListSearchResult.length != 0 ||
                              adminListSearchTextController.text.isNotEmpty
                          ? ListView.builder(
                              itemCount: _adminListSearchResult.length,
                              itemBuilder: _buildSearchAdminMemberListTile,
                            )
                          : RefreshIndicator(
                              onRefresh: _refreshAdminDashBoard,
                              child: ListView.builder(
                                itemCount: _adminMembers.length,
                                itemBuilder: _buildAdminMemberListTile,
                              ))))
            ]),
//            container.state.userAcl != null &&
            userBloc.userDetails.isSuperAdmin
                //&&
//                    container.state.userAcl.viewCounselling
                ? Column(
//                    key: MemKeys.testimonyTab,
                    //modified
                    children: <Widget>[
                      MessageList(
                        filteredMessages: container.state.guidanceMessages,
                        messageBloc: messageBloc,
                        bgImageUrl: "assets/chat_bg.png",
                        connectionStatus: widget.connectionStatus,
                        msgType: "guidance",
                        routeObserver: widget.routeObserver,
                        guidanceMessages: widget.guidanceMessages,
                        lastVisibleChatValue: widget.lastVisibleChatValue,
                        lastVisibleChatKey: widget.lastVisibleChatKey,
                      )
                    ], //new
                  )
                : Container(),
          ]));
    }

    return DefaultTabController(
        length: 5,
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, 0),
                  end: Alignment(
                      1.0, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    Theme.of(context).primaryColorDark,
                    Theme.of(context).backgroundColor
                  ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
            ),
            bottom: TabBar(
              indicatorColor: Color.fromRGBO(217, 178, 80, 1.0),
              labelColor: Theme.of(context).primaryColor,
              labelStyle: TextStyle(fontSize: 8.0),
              labelPadding: EdgeInsets.only(right: 3.0),
              unselectedLabelColor: Theme.of(context).primaryIconTheme.color,
              tabs: [
                Tab(
                  icon: Icon(Icons.verified_user,
                      color: Theme.of(context).primaryIconTheme.color),
                  text: "Active\n(" + _activeMembers.length.toString() + ")",
                ),
                Tab(
                    icon: Icon(Icons.assistant_photo,
                        color: Theme.of(context).primaryIconTheme.color),
                    text:
                        "Pending\n(" + _pendingMembers.length.toString() + ")"),
                Tab(
                    icon: Icon(Icons.block,
                        color: Theme.of(context).primaryIconTheme.color),
                    text:
                        "Blocked\n(" + _blockedMembers.length.toString() + ")"),
                Tab(
                    icon: Icon(Icons.supervised_user_circle,
                        color: Theme.of(context).primaryIconTheme.color),
                    text: "Admins\n(" + _adminMembers.length.toString() + ")"),
                userBloc.userDetails.isSuperAdmin
//                    &&
//                        container.state.userAcl.viewCounselling
                    ? Tab(
                        icon: Icon(Icons.forum,
                            color: Theme.of(context).primaryIconTheme.color),
                        text: "Spiritual Guidance")
                    : Container()
              ],
            ),
            title: Text('Admin Dashboard'),
            actions: <Widget>[
              PopupMenuButton<FacingPage>(
                // overflow menu
                onSelected: _selectPopPage,
                itemBuilder: (BuildContext context) {
                  return _popUpPages
                      .where((FacingPage page) => (page.popUpElement))
                      .map((FacingPage page) {
                    return PopupMenuItem<FacingPage>(
                        value: page,
                        child: FlatButton(
                            textTheme: ButtonTextTheme.normal,
                            onPressed: () {
                              Navigator.pop(context);
                              switch (page.label) {
                                case 'Add Gospel Card':
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            AddGospelCardScreen(
                                                storage: widget.storage,
                                                key: MemKeys
                                                    .addGospelCardScreen)),
                                  );

                                  break;

                                case 'Members List':
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              MembersListScreen(
                                                  storage: widget.storage,
                                                  key: MemKeys.memberList)));

                                  break;

                                case 'Rosary Dashboard':
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RosaryDashBoardScreen(
                                                key: MemKeys.adminScreen,
                                                routeObserver:
                                                    widget.routeObserver,
                                                connectionStatus:
                                                    widget.connectionStatus,
                                              )));

                                  break;
                              }
                            },
                            child: Text(page.label)));
                  }).toList();
                },
              )
            ],
          ),
          body: content,
        ));
  }

  Future<void> _refreshAdminDashBoard() async {
    final userBloc = UserProvider.of(context);
    final rosaryBloc = RosaryProvider.of(context);
    final messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);
    setState(() {
      userBloc.userRepository.users().listen((members) {
        List<MemUser> _members = members.map(MemUser.fromEntity).toList();
        container.state.members = _members;
        debugPrint("Members from spalsh: " + members.toString());
      }).onError((error) {
        debugPrint(
            "Error in getting members data from firebase" + error.toString());
      });
    });

    Stream<List<MessageEntity>> msgList =
        messageBloc.messageRepository.messages();
    msgList.listen((result) {
      container.state.messages = result.map(MEMmessage.fromEntity).toList();
    }).onError((error) {
      debugPrint("Error in getting  messages from firebase" + error.toString());
    });
  }

  _launchURL(String url) async {
    try {
      await launch(url);
    } catch (e) {
      showAlert(
          errorMsg: MemError(
              title: "Couldn't dial",
              body: "Some issue with phone number, please check"));
    }

//    if (await canLaunch(url)) {
//      await launch(url);
//    } else {
//      //throw 'Could not launch $url';
//      showError(
//          errorMsg: MemError(
//              title: "Couldn't dial",
//              body: "Some issue with phone number, please check"));
//    }
  }

  void showAlert({MemError errorMsg, confirmFn confirmFn, cancelFn cancelFn}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "OK",
      confirmFn: confirmFn,
      cancel: "Cancel",
      cancelFn: cancelFn,
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void showSnackMessage(BuildContext context, String message,
      {MaterialColor color = Colors.lightBlue, actionText: "OK"}) {
    debugPrint("Error:$message");
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: color,
      content: Text(message),
      duration: Duration(seconds: 4),
      action: SnackBarAction(
        label: actionText,
        onPressed: () {
          Scaffold.of(context).removeCurrentSnackBar();
          // Some code to undo the change!
        },
      ),
    ));
  }

  confirmAndDelete(MemUser member, UserBloc userBloc) {
    userBloc.userRepository.deleteSingleUser(member.toEntity());
  }

  onActiveListSearchTextChanged(String text) async {
    _activeListSearchResult.clear();

    ReCase rc = ReCase(text);

    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _activeMembers.forEach((user) {
      if (user.name.contains(text) ||
          user.name.contains(rc.titleCase) ||
          user.name.contains(rc.camelCase) ||
          user.name.contains(text.toLowerCase()) ||
          user.displayName.contains(text) ||
          user.displayName.contains(rc.titleCase) ||
          user.displayName.contains(rc.camelCase) ||
          user.displayName.contains(text.toLowerCase()) ||
          user.phoneNumber.contains(text) ||
          user.country.contains(text) ||
          user.country.contains(text.toLowerCase()) ||
          user.country.contains(rc.titleCase) ||
          user.region.contains(text) ||
          user.region.contains(text.toLowerCase()) ||
          user.role.contains(text) ||
          user.role.contains(text.toLowerCase()))
        _activeListSearchResult.add(user);
    });

    setState(() {});
  }

  onAdminListSearchTextChanged(String text) async {
    _adminListSearchResult.clear();

    ReCase rc = ReCase(text);

    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _adminMembers.forEach((user) {
      if (user.name.contains(text) ||
          user.name.contains(rc.titleCase) ||
          user.name.contains(rc.camelCase) ||
          user.name.contains(text.toLowerCase()) ||
          user.displayName.contains(text) ||
          user.displayName.contains(rc.titleCase) ||
          user.displayName.contains(rc.camelCase) ||
          user.displayName.contains(text.toLowerCase()) ||
          user.phoneNumber.contains(text) ||
          user.country.contains(text) ||
          user.country.contains(text.toLowerCase()) ||
          user.country.contains(rc.titleCase) ||
          user.region.contains(text) ||
          user.region.contains(text.toLowerCase()) ||
          user.role.contains(text) ||
          user.role.contains(text.toLowerCase()))
        _adminListSearchResult.add(user);
    });

    setState(() {});
  }

  onPendingListSearchTextChanged(String text) async {
    _pendingListSearchResult.clear();

    ReCase rc = ReCase(text);

    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _pendingMembers.forEach((user) {
      if (user.name.contains(text) ||
          user.name.contains(rc.titleCase) ||
          user.name.contains(rc.camelCase) ||
          user.name.contains(text.toLowerCase()) ||
          user.displayName.contains(text) ||
          user.displayName.contains(rc.titleCase) ||
          user.displayName.contains(rc.camelCase) ||
          user.displayName.contains(text.toLowerCase()) ||
          user.phoneNumber.contains(text) ||
          user.country.contains(text) ||
          user.country.contains(text.toLowerCase()) ||
          user.country.contains(rc.titleCase) ||
          user.region.contains(text) ||
          user.region.contains(text.toLowerCase()) ||
          user.role.contains(text) ||
          user.role.contains(text.toLowerCase()))
        _pendingListSearchResult.add(user);
    });

    setState(() {});
  }

  onBlockedListSearchTextChanged(String text) async {
    _blockedListSearchResult.clear();

    ReCase rc = ReCase(text);

    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _blockedMembers.forEach((user) {
      if (user.name.contains(text) ||
          user.name.contains(rc.titleCase) ||
          user.name.contains(rc.camelCase) ||
          user.name.contains(text.toLowerCase()) ||
          user.displayName.contains(text) ||
          user.displayName.contains(rc.titleCase) ||
          user.displayName.contains(rc.camelCase) ||
          user.displayName.contains(text.toLowerCase()) ||
          user.phoneNumber.contains(text) ||
          user.country.contains(text) ||
          user.country.contains(text.toLowerCase()) ||
          user.country.contains(rc.titleCase) ||
          user.region.contains(text) ||
          user.region.contains(text.toLowerCase()) ||
          user.role.contains(text) ||
          user.role.contains(text.toLowerCase()))
        _blockedListSearchResult.add(user);
    });

    setState(() {});
  }

  void _sendMsgtoChatRoom(MemUser member) {
    MessageBloc messageBloc = MessageProvider.of(context);
    UserBloc userBloc = UserProvider.of(context);

    String id = Uuid().generateV4();

    String msgTitle = "Welcome " + member.title + " " + member.name;

    String _message = "Welcome " +
        member.title +
        " " +
        member.name +
        " to MEM Community. God Bless you.";

    String _fcmMessage = "Let us welcome " +
        member.title +
        " " +
        member.name +
        " to MEM Community. God Bless.";

    var message = {
      'id': id,
      'sender': {
        'name': "Admin",
        'id': userBloc.userDetails.id,
        'imageUrl': userBloc.userDetails.profilePhotoUrl
      },
      'text': "\n💐 " + msgTitle + " 💐\n\n" + _message,
      'time': DateTime.now().millisecondsSinceEpoch
    };
    _messagesReference
        .child("chats")
        .child(member.locale ?? "English")
        .child(id)
        .set(message);

    FCMNotification notification = FCMNotification(
        title: "Let us welcome our new member : " +
            member.title +
            " " +
            member.name,
        body: _fcmMessage ?? "",
        topic: member.locale ?? "English",
        author: "Admin",
        time: DateTime.now().add(Duration(minutes: 1)),
        authorID: userBloc.userDetails.id ?? "",
        priority: "medium");

    if (widget.fcmNotificationEnable)
      messageBloc.messageRepository.addNotification(notification.toEntity());
  }

//  bool canGrantAdminPrivilege(String uid) {
//    bool _isPrivileged = false;
//
//    _aclList.forEach((i) {
//      if (i.id == uid && i.grantAdminRole) {
//        _isPrivileged = true;
//      }
//    });
//
//    return _isPrivileged;
//  }
//
//  bool canActivateUsersPrivilege(String uid) {
//    bool _isPrivileged = false;
//
//    _aclList.forEach((i) {
//      if (i.id == uid && i.activateUser) {
//        _isPrivileged = true;
//      }
//    });
//
//    return _isPrivileged;
//  }

//  unBlock(UserBloc userBloc, User member) {
//    setState(() {
//      userBloc.userRepository.unBlockUser(member.id);
//    });
//  }

}
