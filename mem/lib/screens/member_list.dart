import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:memapp/screens/profile_screen.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:utility/utility.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;

class MembersListScreen extends StatefulWidget {
  final FirebaseStorage storage;
  MembersListScreen({Key key, this.storage}) : super(key: MemKeys.memberList);
  @override
  _MembersListScreenState createState() => _MembersListScreenState();
}

class _MembersListScreenState extends State<MembersListScreen> {
  List<MemUser> _members = [];

  @override
  void initState() {
    super.initState();
  }

  void _loadMembers() {
    var container = AppStateContainer.of(context);

    setState(() {
      _members = container.state.members;
    });
  }

  Widget _buildMemberListTile(BuildContext context, int index) {
    var member = _members[index];
    UserBloc userBloc = UserProvider.of(context);

    return ListTile(
      onTap: () => _navigateToFriendDetails(member, index),
      leading: Hero(
        tag: index,
        child: CircleAvatar(
          backgroundImage:
              CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
                  NetworkImage(member.profilePhotoUrl ?? "") ??
                  AssetImage("assets/placeholder-face.png"),
        ),
      ),
      title: Row(children: <Widget>[
        Expanded(flex: 4, child: Text(member.displayName ?? member.name)),
        Text(
          member.role == "user" ? "" : "  ~Admin",
          style: TextStyle(color: Colors.red, fontSize: 12.0),
        )
      ]),
      subtitle: Text("Rosaries Offered: " + member.rosariesOffered.toString()),
      enabled: member.status,
      trailing: FlatButton(
        onPressed: member.role == "admin"
            ? member.id != userBloc.userDetails.id
                ? () {
                    _launchURL("tel:" + member.phoneNumber);
                  }
                : null
            : null,
        child: Icon(Icons.phone_forwarded),
      ),
    );
  }

  void _navigateToFriendDetails(MemUser member, Object avatarTag) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (c) {
          return ProfileScreen(
              member: member, avatarTag: avatarTag, storage: widget.storage);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _loadMembers();

    Widget content;

    if (_members.isEmpty) {
      content = Center(
        child: CircularProgressIndicator(),
      );
    } else {
      content = ListView.builder(
        itemCount: _members.length,
        itemBuilder: _buildMemberListTile,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Group Members'),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )
      ),
      body: content,
    );
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "OK",
      confirmFn: () {
        Navigator.pop(context);
//        Navigator.of(context).pushNamedAndRemoveUntil(
//            "/splash", (Route<dynamic> route) => false);
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  _launchURL(String url) async {
    try {
      await launch(url);
    } catch (e) {
      showError(
          errorMsg: MemError(
              title: "Couldn't dial",
              body: "Some issue with phone number, please check"));
    }
  }
}
