// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:base_helpers/basehelpers.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';

import 'package:utility/src/dialogshower.dart' as DialogShower;
import 'package:memapp/screens/add_bishop_message_screen.dart';

class BishopMessageScreen extends StatefulWidget {
  final List<BishopsMessage> patronsMessages;
  final BishopsMessage mainMessage;
  final FirebaseStorage storage;

  BishopMessageScreen({
    @required this.patronsMessages,
    @required this.mainMessage,
    @required this.storage,
    Key key,
  }) : super(key: MemKeys.bishopsMessageScreen);

  @override
  _BishopMessageScreenState createState() => _BishopMessageScreenState();
}

class _BishopMessageScreenState extends State<BishopMessageScreen> {
  bool isVideoManager = false;
  int _messageLength = 1200;
  int _messageLengthLimit = 1200;
  String patronMessage =
      """The greatest salvific grace the world has witnessed and recognised is the visitation of Jesus Christ. It is through accepting and proclaiming him as our saviour that the world is saved. Offering his divinity to the world is our calling. 
      
  God Almighty used Holy Mother Mary as the medium for giving his only begotten son. It is imperative that we seek the intercession of the blessed Mother to make this salvific mission our personal experience and impart it to the world. In these modern times when the world is rotting with excesses of consumerism, insatiable greed, unquenchable ambition and wrathful arrogance Jesus Christ is the only answer. This decadence has also seeped into the church which is the body of Christ. This dangerous situation is heightened by the challenges posed by atheism and the fourth estate which has lost all moral authority. The tendency to view religion satirically in the wake of secularism has led to a negation of faith and apathetic indifference. The Church which should be through its sacrificial neutrality and transcendence rise above the corporeal world has failed to be so.
  
  The Church heads who fail to live their calling, the committed who rely more on worldliness than spirituality, the proclaimers of faith who have lost their radiance, the shepherds who have their eyes fixed on authority and power rather than service, the chosen who abuse the face of Christ…these are some of the failings of the Christian church today. Only if the church is critically sanctified, it can perform its vital responsibilities during this crucial times. The church is a sacrament of God. We must be able to reach the Messiah through the church. Therefore the church must be in a continuous ablution.
  
  These are critical hours when you have to keep constant watch…The passionate question asked by Christ in his hour of agony at Gethsemane is still relevant today
  Are you still restfully sleeping …wake up…keep watch... this is the hour to be watchful.
  
  We are being vigilant and wakeful through The MARIAN EUCHARISTIC MINISTRY (MEM)
  
  Ours is a Redemptive mission encompassing all the continents. It is a mission focussed on the Holy Bible and the Holy Qurbano through the mediation of the Holy Mother. It is a peaceful battle utilising all the spiritual weapons made available to us.
  
  The Mission built up over the last three years with the rhythmic chanting of the rosary is being strengthened to make it more comprehensive and effective. We are just a small loop in the long chain of people who hold the rosary in their hands. In this great initiative for the building up of the Kingdom of God, I join the Bishops and the Priests and the Religious and The lay Faithful. This Mission is reinforced by the Patient suffering of the terminally ill and the people in agony requesting Prayer support.
  
  The blessed Rosary is a potent arsenal. The crucifix in the Rosary hold together God’s word signified by each bead of the Rosary. It is the salvific face of Jesus that is revealed through this. The Prayer of Trinity repeated in the Rosary is actually the magnificent praise of God. Every time your finger touches a bead it reaches the soul and heart of Christ. The Rosary that starts with the Apostle’s Creed makes the rosary a wholesome experience. It proclaims loudly all the beliefs decreed by the Church. The profundity of the Lord’s Prayers deepens our spiritual insight. The Rosary celebrates the fatherhood of God in the brotherhood of His children.
 
  The “Hail Mary” is also the word of God. The words said by the angel of God to Mother Mary and the words of response by Elizabeth are significant. The crux of this prayer is “ “blessed is the fruit of thy womb” …therefore the Hail Mary prayer is focussed on Christ. The Rosary prepares us for the Holy confession and other sacraments. We hold the saints to our hearts and implore their mediation.
  
  We make the worldly incarnation of Jesus Christ, His life, His worldly mission, His actions, His Passion, His Crucifixion, His Resurrection, Ascension and His Second Coming all form the basis of the Rosary. In short whatever we celebrate in the Mass is adored through the beads of the Rosary. We pray for the Holy unction of Faith, Hope, Love through The “Glory Be’ remembering the holy Trinity .The Rosary then becomes the kernel of the holiness of the church entreating for its sanctification through the intercession of the Holy Mother.
  
  It will destroy all the fortresses of the Devil…there will be a divine intervention in all the prayers ascending into heaven. All the continents will rally behind the flag of  the Blessed Virgin.MEM will become an integral part in the building of the kingdom of God. The mission of MEM will be a battalion of missionaries who cannot be dispersed by the ways of the world.  We will be blessed as a single family in our pilgrimage towards blissful eternity..
  
  Let God in his infinite mercy lead us on…Let the immaculate heart of The Holy Mother be our sanctuary …

  (Bishop Samuel Mar Irenios)""";

  @override
  void initState() {
    _messageLength = _messageLengthLimit;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget content;
    UserBloc userBloc = UserProvider.of(context);
    var container = AppStateContainer.of(context);
    var profilePhotoRadius = MediaQuery.of(context).size.width * 0.25;

    if (userBloc.userDetails.role == "admin" &&
        container.state.userAcl != null) {
      if (container.state.userAcl.addBishopsMessage) {
        isVideoManager = true;
      } else {
        isVideoManager = false;
      }
    }

//    if (widget.patronsMessages.isEmpty) {
//      content = Center(
//        child: CircularProgressIndicator(),
//      );
//    } else {
    content = Container(
      child: Container(
          decoration: new BoxDecoration(
            color: Theme.of(context).primaryColorLight,
          ),
          child: ListView(
            physics: ScrollPhysics(),
            shrinkWrap: true,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              // Expanded(
              //   flex: 1,
              //    child:

              Container(
                margin: EdgeInsets.all(40.0),
                child: Container(
                  padding: EdgeInsets.all(30.0),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: new AssetImage("assets/bishop-face.png"),
                      fit: BoxFit.contain,
                    ),
                    shape: BoxShape.circle,
                  ),
                  width: profilePhotoRadius * 2 - 20.0,
                  height: profilePhotoRadius * 2 - 20.0,
                ),
                width: profilePhotoRadius * 2,
                height: profilePhotoRadius * 2,
                padding: const EdgeInsets.all(5.0), // borde width
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor, // border color
                  shape: BoxShape.circle,
                ),
              ),

              Container(
                  padding: EdgeInsets.only(
                      top: 0.0, left: 40.0, right: 40.0, bottom: 1.0),
                  margin: EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    "HIS EXCELLENCY MOST REV. DR. SAMUEL MAR IRENIOS",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0,
                        color: Theme.of(context).backgroundColor),
                  )),
              AnimatedContainer(
                margin: EdgeInsets.only(
                    top: 0.0, left: 30.0, right: 30.0, bottom: 20.0),
                duration: const Duration(milliseconds: 120),
                child: patronMessage.length > _messageLengthLimit
                    ? Text(
                        patronMessage.substring(0, _messageLength),
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Theme.of(context).backgroundColor),
                        textAlign: TextAlign.justify,
                      )
                    : Text(
                        patronMessage,
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Theme.of(context).backgroundColor),
                        textAlign: TextAlign.justify,
                      ),
              ),
              GestureDetector(
                onTap: () => setState(() {
                  _messageLength != _messageLengthLimit
                      ? _messageLength = _messageLengthLimit
                      : _messageLength = patronMessage.length;
                }),
                child: Container(
                  margin: EdgeInsets.only(left: 140.0, bottom: 30.0),
                  child: patronMessage.length > _messageLengthLimit
                      ? Text(
                          _messageLength == _messageLengthLimit
                              ? "...Read More"
                              : "",
                          style: TextStyle(color: Colors.brown, fontSize: 14.0),
                        )
                      : Container(),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.5)
            ],
          )),
    );
    //}

    return Scaffold(
      appBar: AppBar(
        title: Text(
          MemLocalizations(Localizations.localeOf(context)).bishopsMessageTitle,
//          style: TextStyle(color: Theme.of(context).primaryIconTheme.color),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-1.0, 0),
              end: Alignment(
                  1.0, 0.0), // 10% of the width, so there are ten blinds.
              colors: [
                Theme.of(context).primaryColorDark,
                Theme.of(context).backgroundColor
              ], // whitish to gray
              tileMode:
                  TileMode.repeated, // repeats the gradient over the canvas
            ),
          ),
        ),
      ),
      body: content,
      floatingActionButton: isVideoManager
          ? FloatingActionButton.extended(
              tooltip: 'Add Bishop' 's Message',
              backgroundColor: Theme.of(context).backgroundColor,
              icon: Icon(Icons.person_pin),
              label: Text('Add Bishop' 's Message'.toUpperCase()),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (c) {
                      return AddBishopMessageScreen(
                        storage: widget.storage,
                        key: MemKeys.addBishopMessageScreen,
                      );
                    },
                  ),
                );
              })
          : Container(),
    );
  }
}
