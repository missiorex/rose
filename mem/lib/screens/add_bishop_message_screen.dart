import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:base_helpers/basehelpers.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memapp/app_state_container.dart';
import 'dart:io';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart' as p;
import 'package:mem_blocs/mem_blocs.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;

class AddBishopMessageScreen extends StatefulWidget {
  AddBishopMessageScreen({key: Key, this.storage})
      : super(key: MemKeys.addBishopMessageScreen);

  final FirebaseStorage storage;

  @override
  _AddBishopMessageScreenState createState() =>
      new _AddBishopMessageScreenState();
}

class _AddBishopMessageScreenState extends State<AddBishopMessageScreen> {
  File _image;
  Future<File> _imageFile;
  static final GlobalKey<FormState> addBishopMessageFormKey =
      GlobalKey<FormState>();
  AppState appState;
  final TextEditingController _dateController = TextEditingController();
  BishopsMessage bishopsMessage = BishopsMessage();

  initState() {}

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final container = AppStateContainer.of(context);
    final MessageBloc messageBloc = MessageProvider.of(context);

    appState = container.state;

    return Scaffold(
      appBar: AppBar(
          title: Text("Add Bishop's Message"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      body: Container(
        color: Theme.of(context).primaryColorLight,
        padding: EdgeInsets.all(16.0),
        child: Form(
            key: addBishopMessageFormKey,
            autovalidate: false,
            onWillPop: () {
              return Future(() => true);
            },
            child: ListView(
              children: [
                videoTitleField(),
                videoDetails(),
                youTubeUrlField(),
//                witnessField(),
//                witnessDetails(),
                _previewImage(),
                videoImageField("Upload Video Image"),
                AddVideoButton(messageBloc),
              ],
            )),
      ),
    );
  }

  Widget videoTitleField() {
    return Container(
        decoration:
            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: const Icon(Icons.title),
            hintText: 'The Video Title',
            labelText: 'The Video Title',
            border: InputBorder.none,
          ),
          inputFormatters: [LengthLimitingTextInputFormatter(100)],
          validator: (val) => val.isEmpty ? 'Title is required' : null,
          onSaved: (val) => bishopsMessage.title = val,
        ));
  }

  Widget youTubeUrlField() {
    return Container(
        decoration:
            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: const Icon(Icons.ondemand_video),
            hintText: 'Youtube URL of the Video',
            labelText: 'Youtube URL of the Video',
            border: InputBorder.none,
          ),
          inputFormatters: [LengthLimitingTextInputFormatter(100)],
          validator: (val) => val.isEmpty ? 'Youtube URL is required' : null,
          onSaved: (val) => bishopsMessage.youtubeUrl = val,
        ));
  }

  Widget videoDetails() {
    return Container(
        decoration:
            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: const Icon(Icons.details),
            hintText: 'Video Details',
            labelText: 'Video Details',
            border: InputBorder.none,
          ),
          inputFormatters: [LengthLimitingTextInputFormatter(200)],
          validator: (val) => val.isEmpty ? 'Video Details, is required' : null,
          onSaved: (val) => bishopsMessage.body = val,
        ));
  }

  Widget videoImageField(
    String text, {
    Color backgroundColor = Colors.brown,
    Color textColor = Colors.white,
  }) {
    UserBloc userBloc = UserProvider.of(context);
    File _profileImage;

    return Container(
        margin: EdgeInsets.only(left: 50.0, right: 50.0, top: 10.0),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: MaterialButton(
                child: Text(text),
                minWidth: 80.0,
                color: backgroundColor,
                textColor: textColor,
                onPressed: () {
                  saveImageLocal(bishopsMessage.id).then((image) {
                    _profileImage = image;

                    _uploadFile(image, bishopsMessage.id).then((videoImageUrl) {
                      debugPrint(
                          "Video Image Url :" + videoImageUrl.toString());
                      bishopsMessage.videoImageUrl = videoImageUrl.toString();

                      ///todo upload profile photo userBloc.userRepository.updateUser(user)
                    });
                  });
                })));
  }

  Future<String> _uploadFile(File _image, String uid) async {
    List<String> fileNameList = _image.path.split("/");

    String fileName = fileNameList[fileNameList.length - 1];

    final Reference ref =
        widget.storage.ref().child('videoImage').child(fileName);
    final UploadTask uploadTask = ref.putFile(
      _image,
      SettableMetadata(
          customMetadata: <String, String>{'iamgetype': 'profile'},
          contentType: 'image/jpeg'),
    );

    //final Uri downloadUrl = (await uploadTask.future).downloadUrl;
    final String downloadUrl = await ref.getDownloadURL();

    return downloadUrl;
  }

  Future<File> saveImageLocal(String uid) async {
    var _imageFile;

    ImagePicker()
        .getImage(
            source: ImageSource.gallery, maxHeight: 200.0, maxWidth: 200.0)
        .then((pickedFile) {
      if (pickedFile != null) {
        _imageFile = File(pickedFile.path);
      }
    });

    var image = await _imageFile;
    // getting a directory path for saving
    final String path = await _localPath;

    ImageProperties properties =
        await FlutterNativeImage.getImageProperties(image.path);
    File compressedFile = await FlutterNativeImage.compressImage(image.path,
        quality: 80,
        targetWidth: 200,
        targetHeight: (properties.height * 200 / properties.width).round());

    File profilePic = await compressedFile.copy(p.join(path, uid + ".jpeg"));

    setState(() {
      _image = profilePic;
    });

    return _image;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Widget _previewImage() {
    return Container(
        margin: EdgeInsets.only(top: 10.0),
        child: FutureBuilder<File>(
            future: _imageFile,
            builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.data != null) {
                return Image.file(snapshot.data);
              } else if (snapshot.error != null) {
                return const Text(
                  'Error picking image.',
                  textAlign: TextAlign.center,
                );
              } else {
                return const Text(
                  'You have not yet picked an image.',
                  textAlign: TextAlign.center,
                );
              }
            }));
  }

  Widget AddVideoButton(MessageBloc messageBloc) {
    return Container(
        margin: EdgeInsets.only(top: 5.0, left: 40.0, right: 40.0),
        padding:
            EdgeInsets.only(top: 0.0, bottom: 10.0, left: 10.0, right: 10.0),
        child: FlatButton(
            padding:
                EdgeInsets.only(top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
            color: Color.fromRGBO(11, 129, 140, 1.0),
            splashColor: Colors.deepOrange,
            child: Text(
              "Add Bishop's Message",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              final FormState form = addBishopMessageFormKey.currentState;

              if (!form.validate()) {
                debugPrint(
                    "Video Details Collected: " + bishopsMessage.toString());
                debugPrint('Some Details are missing or incorrect');

                showError(
                    errorMsg: MemError(
                        title: "Some Details are missing or incorrect",
                        body: "Please correct the missing details"));
              } else {
                form.save(); //This invokes each onSaved event
                try {
                  bishopsMessage.time = DateTime.now();
                  bishopsMessage.author = "Most Rev. Dr. Samuel Mar Irenios";
                  bishopsMessage.authorTitle = "Patron, MEM Ministries";
                  messageBloc.messageRepository
                      .addBishopsMessage(bishopsMessage.toEntity());

                  showError(
                      errorMsg: MemError(
                          title: "Video Added. Please check",
                          body: "Video Testimony Added"));
                } catch (e) {
                  showError(
                      errorMsg: MemError(
                          title: "Couldnt Add the video.", body: e.toString()));
                }
              }
            }));
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      cancel: "OK",
      cancelFn: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }
}
