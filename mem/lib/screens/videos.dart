import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/services/youtube_api_service.dart';
import 'package:memapp/screens/video_player.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoScreen extends StatefulWidget {
  @override
  _VideoScreenState createState() => _VideoScreenState();
}

class _VideoScreenState extends State<VideoScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  YoutubeChannel _channel;
  bool _isLoading = false;
  bool _newVideoLoading = false;
  YoutubePlayerController _controller;
  int _selectedIndex = -1;
  bool _muted = false;

  PlayerState _playerState;
  YoutubeMetaData _videoMetaData;
  bool _isPlayerReady = false;

  @override
  void initState() {
    super.initState();
    _initChannel();
  }

  void listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller?.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _initChannel() async {
    YoutubeChannel channel = await APIService.instance
        .fetchChannel(channelId: 'UCfwiNhvSCYcu0fmwVZOQdGQ');

    _initVideoPlayer();
    setState(() {
      _channel = channel;
    });
  }

  _initVideoPlayer() {
    _controller = YoutubePlayerController(
      //initialVideoId: _channel.videos[_selectedIndex].id,
      initialVideoId: "kdqAyxFWoas",
      flags: YoutubePlayerFlags(
        mute: _muted,
        autoPlay: false,
      ),
    )..addListener(listener);
  }

  _onSelected(int index) {
    if (index != _selectedIndex) {
      _controller.pause();

      setState(() {
        _selectedIndex = index;
        _showSnackBar('Loading the selected video.');
        _controller.load(_channel.videos[_selectedIndex].id);
      });
    }
    //chewieController.play();
  }

  Color _getStateColor(PlayerState state) {
    switch (state) {
      case PlayerState.unknown:
        return Colors.grey[700];
      case PlayerState.unStarted:
        return Colors.pink;
      case PlayerState.ended:
        return Colors.red;
      case PlayerState.playing:
        return Colors.blueAccent;
      case PlayerState.paused:
        return Colors.orange;
      case PlayerState.buffering:
        return Colors.yellow;
      case PlayerState.cued:
        return Colors.blue[900];
      default:
        return Colors.blue;
    }
  }

  _buildProfileInfo() {
    var deviceWidth = MediaQuery.of(context).size.width;

    return Container(
      margin: EdgeInsets.all(20.0),
      padding: EdgeInsets.all(20.0),
      height: 100.0,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(0, 1),
            blurRadius: 6.0,
          ),
        ],
      ),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            backgroundColor: Colors.white,
            radius: 35.0,
            backgroundImage: NetworkImage(_channel.profilePictureUrl),
          ),
          SizedBox(width: 12.0),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "MEM Channel",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  '${_channel.subscriberCount} subscribers',
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 8.0,
                    fontWeight: FontWeight.w600,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          FlatButton(
              color: Colors.red,
              child: Text(
                "SUBSCRIBE",
                style: TextStyle(color: Colors.white, fontSize: 11.0),
              ),
//              icon: ImageIcon(
//                  AssetImage("assets/icons/icons8-play-button-48.png"),
//                  size: deviceWidth * 0.1),
              onPressed: (() {
                const url =
                    "https://www.youtube.com/channel/UCfwiNhvSCYcu0fmwVZOQdGQ";
                launch(url);
              }))
        ],
      ),
    );
  }

  _buildVideo(YoutubeVideo video, int index) {
    var deviceWidth = MediaQuery.of(context).size.width;
    var deviceHeight = MediaQuery.of(context).size.height;

    return Container(
//        padding: EdgeInsets.only(
//            ),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColorLight,
            border: Border(
                bottom: BorderSide(
                    width: 0.25,
                    color: Theme.of(context).primaryColorLight,
                    style: BorderStyle.solid))),
        height: deviceHeight * 0.17,
        child: GestureDetector(
            onTap: () => _onSelected(index),
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
//                    backgroundBlendMode: BlendMode.colorBurn,
                    color: _selectedIndex != null && _selectedIndex == index
                        ? Color.fromRGBO(255, 245, 230, 1.0)
                        : Colors.white,
//                    image: DecorationImage(
//                        image: _selectedIndex != null && _selectedIndex == index
//                            ? AssetImage('assets/bg-drk-ptn.png')
//                            : AssetImage('assets/bg-lgt-ptn1.png'),
//                        fit: BoxFit.contain,
//                        repeat: ImageRepeat.repeat),
                  ),
                ),
                Positioned(
                    top: deviceWidth * 0.04,
                    left: deviceWidth * 0.40,
                    child: Container(
                        padding: EdgeInsets.only(left: 20.0, right: 10.0),
                        width: deviceWidth * 0.56,
                        height: deviceWidth * 0.3,
                        child: Text(
                          video.title.length > 160
                              ? video.title.substring(0, 160) + "..."
                              : video.title,
                          style: TextStyle(
                              color: Theme.of(context).backgroundColor,
                              fontSize: 12.0,
                              fontWeight: FontWeight.w900),
                        ))),
//                Positioned(
//                    top: deviceWidth * 0.10,
//                    left: deviceWidth * 0.38,
//                    child: Container(
//                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
//                        height: deviceWidth * 0.2,
//                        width: deviceWidth * 0.6,
//                        child: Text(
//                          video.description.length > 50
//                              ? video.description.substring(0, 50) + "..."
//                              : video.description,
//                          style: TextStyle(
//                              color: Color.fromRGBO(64, 64, 64, 1.0),
//                              fontSize: 10.0,
//                              fontWeight: FontWeight.w500),
//                        ))),
                Positioned(
                    top: deviceWidth * 0.20,
                    left: deviceWidth * 0.40,
                    child: Container(
                        padding: EdgeInsets.only(left: 20.0, right: 10.0),
                        height: deviceWidth * 0.2,
                        width: deviceWidth * 0.6,
                        child: Text(
                          DateFormat.yMMMd().format(DateTime.parse(
                            video.publishedAt,
                          )),
                          style: TextStyle(
                              color: Color.fromRGBO(64, 64, 64, 1.0),
                              fontSize: 10.0,
                              fontWeight: FontWeight.w500),
                        ))),
                Positioned(
                    top: deviceWidth * 0.04,
                    left: deviceWidth * 0.05,
                    bottom: deviceWidth * 0.04,
                    child: Container(
                      width: deviceWidth * 0.35,
                      height: deviceWidth * 0.3,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        fit: BoxFit.cover,
                        image: video.thumbnailUrl != null
                            ? NetworkImage(video.thumbnailUrl)
                            : AssetImage("assets/placeholder-face.png"),
                      )),
                    )),
              ],
            )));
  }

  _buildSelectedVideo(YoutubeVideo video) {
    var deviceWidth = MediaQuery.of(context).size.width;
    var deviceHeight = MediaQuery.of(context).size.height;

    return Container(
        padding: EdgeInsets.only(
            left: deviceWidth * 0.02, right: deviceWidth * 0.02),
        height: deviceHeight * 0.3,
        child: Stack(
          children: <Widget>[
            Positioned(
                top: deviceWidth * 0.04,
                left: deviceWidth * 0.04,
                bottom: deviceWidth * 0.04,
                child: Container(
                  width: deviceWidth * 0.3,
                  height: deviceWidth * 0.3,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    fit: BoxFit.contain,
                    image: video.thumbnailUrl != null
                        ? NetworkImage(video.thumbnailUrl)
                        : AssetImage("assets/placeholder-face.png"),
                  )),
                )),
          ],
        ));
  }

  _loadMoreVideos() async {
    _isLoading = true;
    List<YoutubeVideo> moreVideos = await APIService.instance
        .fetchVideosFromPlaylist(playlistId: _channel.uploadPlaylistId);
    List<YoutubeVideo> allVideos = _channel.videos..addAll(moreVideos);
    setState(() {
      _channel.videos = allVideos;
    });
    _isLoading = false;
  }

  void _showSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(
          message,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16.0,
          ),
        ),
        backgroundColor: Colors.red[600],
        behavior: SnackBarBehavior.floating,
        elevation: 1.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var deviceWidth = MediaQuery.of(context).size.width;
    var deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).primaryColorLight,
      appBar: AppBar(
          title: Text('MEM - Videos'),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      body: _channel != null
          ? NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollDetails) {
                if (!_isLoading &&
                    _channel.videos.length != int.parse(_channel.videoCount) &&
                    scrollDetails.metrics.pixels ==
                        scrollDetails.metrics.maxScrollExtent) {
                  _loadMoreVideos();
                }
                return false;
              },
              child:
//              SizedBox(
//                  height: MediaQuery.of(context).size.height * 2.0,
//                  child:
                  Column(mainAxisAlignment: MainAxisAlignment.start, children: <
                      Widget>[
                Container(
                    child: Stack(
                  children: <Widget>[
//                                SizedBox(
//                                  height:
//                                      deviceWidth / 1.7789 + deviceWidth * 0.2,
//                                  child:
//                              YoutubePlayerBuilder(
//                                player:

                    Column(children: [
                      YoutubePlayer(
                        controller: _controller,
                        showVideoProgressIndicator: true,
                        progressColors: ProgressBarColors(
                            backgroundColor: Colors.black12,
                            playedColor: Colors.redAccent,
                            handleColor: Colors.redAccent,
                            bufferedColor: Colors.white),
                        bottomActions: [
                          CurrentPosition(),
                          RemainingDuration(),
                        ],
                        onReady: () {
                          _isPlayerReady = true;
                          print('Player is ready.');
                        },
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              left: deviceWidth * 0.05, top: 7.0, bottom: 7.0),
                          width: deviceWidth,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                width: deviceWidth * 0.13,
                                padding: EdgeInsets.only(
                                  right: 5.0,
                                ),
                                child: Material(
                                  color: Colors.transparent,
                                  child: Container(
//                                                padding: EdgeInsets.only(
//                                                    top: 10.0,
//                                                    right: 5.0,
//                                                    bottom: 10.0),
                                      child: InkWell(
                                    onTap: (() {
                                      const url =
                                          "https://www.youtube.com/channel/UCfwiNhvSCYcu0fmwVZOQdGQ";
                                      launch(url);
                                    }), // needed
                                    child: Image.asset(
                                      "assets/yt_icon_rgb.png",
                                      width: 32,
                                      fit: BoxFit.cover,
                                    ),
                                  )),
                                ),
                              ),
                              Container(
                                  width: deviceWidth * 0.80,
                                  padding: EdgeInsets.only(
                                    left: 5.0,
                                    right: 5.0,
                                  ),
                                  child: Text(
                                    _selectedIndex == -1
                                        ? "Please tap on the video you want to play from the below list."
                                        : _channel.videos[_selectedIndex].title,
                                    style: TextStyle(
                                        color:
                                            Theme.of(context).backgroundColor,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w900),
                                  )),
                            ],
                          ))
                    ]),

//                                builder: (context, player) {
//                                  return Column(children: [
//                                    //player,
//                                    _buildSelectedVideo(_channel.videos[2])
//                                  ]);
//                                },
//                              ),
//                                ),
//                            Row(
//                              children: <Widget>[
//                                SizedBox(width: 12.0),
//                                Expanded(
//                                  child: Row(
//                                    mainAxisAlignment: MainAxisAlignment.center,
//                                    crossAxisAlignment:
//                                        CrossAxisAlignment.start,
//                                    children: <Widget>[
//                                      Text(
//                                        "Visit Our Youtube Channel | ${_channel.videoCount} Videos.",
//                                        style: TextStyle(
//                                          color:
//                                              Color.fromRGBO(193, 152, 86, 1.0),
//                                          fontSize: 12.0,
//                                          fontWeight: FontWeight.w600,
//                                        ),
//                                        overflow: TextOverflow.ellipsis,
//                                      ),
//                                    ],
//                                  ),
//                                ),
//                                Material(
//                                  color: Colors.transparent,
//                                  child: Container(
//                                      padding: EdgeInsets.only(
//                                          left: 5.0,
//                                          top: 5.0,
//                                          right: 25.0,
//                                          bottom: 5.0),
//                                      child: InkWell(
//                                        onTap: (() {
//                                          const url =
//                                              "https://www.youtube.com/channel/UCfwiNhvSCYcu0fmwVZOQdGQ";
//                                          launch(url);
//                                        }), // needed
//                                        child: Image.asset(
//                                          "assets/yt_icon_rgb.png",
//                                          width: 32,
//                                          fit: BoxFit.cover,
//                                        ),
//                                      )),
//                                ),
//
////              icon: ImageIcon(
////                  AssetImage("assets/icons/icons8-play-button-48.png"),
////                  size: deviceWidth * 0.1),
////                                onPressed: (() {
////                                  const url =
////                                      "https://www.youtube.com/channel/UCfwiNhvSCYcu0fmwVZOQdGQ";
////                                  launch(url);
////                                }))
//                              ],
//                            ),
                    Positioned(
                      bottom: deviceWidth * 0.18,
                      right: deviceWidth * 0.03,
                      child: Container(
                          decoration: new BoxDecoration(
                            color: Colors.blueGrey.shade300,
                            borderRadius:
                                new BorderRadius.all(Radius.circular(30.0)),
                          ),
                          height: 30.0,
                          width: 30.0,
                          margin: EdgeInsets.only(left: 5.0),
                          child: IconButton(
                              iconSize: 14.0,
                              color: Colors.black,
                              icon: ImageIcon(
                                  AssetImage('assets/icons/share.png'),
                                  size: deviceWidth * 0.05),
                              onPressed: (() {
                                final RenderBox box =
                                    context.findRenderObject();
                                Share.share(
                                    _selectedIndex == -1
                                        ? "https://www.youtube.com/watch?v=kdqAyxFWoas"
                                        : "https://www.youtube.com/watch?v=" +
                                            _channel.videos[_selectedIndex].id,
                                    sharePositionOrigin:
                                        box.localToGlobal(Offset.zero) &
                                            box.size);
                              }))),
                    ),
                    Positioned(
                      bottom: deviceWidth * 0.18,
                      right: deviceWidth * 0.12,
                      child: Container(
                          decoration: new BoxDecoration(
                            color: Colors.blueGrey.shade300,
                            borderRadius:
                                new BorderRadius.all(Radius.circular(30.0)),
                          ),
                          height: 30.0,
                          width: 30.0,
                          margin: EdgeInsets.only(left: 5.0),
                          child: IconButton(
                            iconSize: 14.0,
                            color: Colors.black,
                            icon: Icon(
                                _muted ? Icons.volume_off : Icons.volume_up),
                            onPressed: () {
                              _muted
                                  ? _controller.unMute()
                                  : _controller.mute();
                              setState(() {
                                _muted = !_muted;
                              });
                            },
                          )),
                    ),
//                            Positioned(
//                              bottom: deviceWidth * 0.38,
//                              left: deviceWidth * 0.12,
//                              child:
//                              Material(
//                                color: Colors.transparent,
//                                child: Container(
////                                                  padding: EdgeInsets.only(
////                                                      left: 5.0,
////                                                      top: 0.0,
////                                                      right: 25.0,
////                                                      bottom: 5.0),
//                                    child: InkWell(
//                                  onTap: (() {
//                                    const url =
//                                        "https://www.youtube.com/channel/UCfwiNhvSCYcu0fmwVZOQdGQ";
//                                    launch(url);
//                                  }), // needed
//                                  child: Image.asset(
//                                    "assets/yt_icon_rgb.png",
//                                    width: 32,
//                                    fit: BoxFit.cover,
//                                  ),
//                                )),
//                              ),
//                            ),
//                            Positioned(
//                                bottom: 0.0,
//                                left: 0.0,
//                                child: Container(
//                                    padding: EdgeInsets.only(
//                                        left: deviceWidth * 0.05,
//                                        top: 7.0,
//                                        bottom: 7.0),
//                                    width: deviceWidth,
//                                    decoration: BoxDecoration(
//                                      color: Theme.of(context).primaryColor,
//                                    ),
//                                    child: Row(
//                                      mainAxisAlignment:
//                                          MainAxisAlignment.spaceEvenly,
//                                      children: <Widget>[
//                                        Container(
//                                          width: deviceWidth * 0.13,
//                                          padding: EdgeInsets.only(
//                                            right: 5.0,
//                                          ),
//                                          child: Material(
//                                            color: Colors.transparent,
//                                            child: Container(
////                                                padding: EdgeInsets.only(
////                                                    top: 10.0,
////                                                    right: 5.0,
////                                                    bottom: 10.0),
//                                                child: InkWell(
//                                              onTap: (() {
//                                                const url =
//                                                    "https://www.youtube.com/channel/UCfwiNhvSCYcu0fmwVZOQdGQ";
//                                                launch(url);
//                                              }), // needed
//                                              child: Image.asset(
//                                                "assets/yt_icon_rgb.png",
//                                                width: 32,
//                                                fit: BoxFit.cover,
//                                              ),
//                                            )),
//                                          ),
//                                        ),
//                                        Container(
//                                            width: deviceWidth * 0.80,
//                                            padding: EdgeInsets.only(
//                                              left: 5.0,
//                                              right: 5.0,
//                                            ),
//                                            child: Text(
//                                              _selectedIndex == -1
//                                                  ? "Please tap on the video you want to play from the below list."
//                                                  : _channel
//                                                      .videos[_selectedIndex]
//                                                      .title,
//                                              style: TextStyle(
//                                                  color: Theme.of(context)
//                                                      .backgroundColor,
//                                                  fontSize: 12.0,
//                                                  fontWeight: FontWeight.w900),
//                                            )),
//                                      ],
//                                    )))
                  ],
                )),
                Expanded(
                    child: ListView.separated(
                  itemCount: _channel.videos.length,
                  separatorBuilder: (_, i) =>
                      SizedBox(height: deviceWidth * 0.005),
                  itemBuilder: (BuildContext context, int index) {
                    YoutubeVideo video = _channel.videos[index];
                    return _buildVideo(video, index);
                  },
                ))
              ])
              //),
              )
          : Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Theme.of(context).primaryColor, // Red
                ),
              ),
            ),
    );
  }

//  Iterable<String> _allStringMatches(String text, RegExp regExp) =>
//      regExp.allMatches(text).map((m) => m.group(0));
//
//  Future<String> _fetchVideoURL(String yt) async {
//    final response = await http.get(yt);
//    Iterable parseAll = _allStringMatches(
//        response.body, RegExp("\"url_encoded_fmt_stream_map\":\"([^\"]*)\""));
//    Iterable<String> parse;
//    if (parseAll.toList().isEmpty) {
//      parseAll = _allStringMatches(response.body, RegExp("streamingData.*]"));
//      parse =
//          _allStringMatches(parseAll.toList()[0], RegExp(r'"url\\":\\".*?\\"'));
//      final List<String> urls = parse.toList()[0].split(r'":\');
//      String finalUrl = Uri.decodeFull(urls[1]
//          .substring(1, urls[1].length - 2)
//          .replaceAll(r'\\u0026', '&')
//          .replaceAll(r'\/', r'/'));
//      return finalUrl;
//    } else {
//      parse = _allStringMatches(parseAll.toList()[0], RegExp("url=(.*)"));
//      final List<String> urls = parse.toList()[0].split('url=');
//      parseAll = _allStringMatches(urls[1], RegExp("([^&,]*)[&,]"));
//      String finalUrl = Uri.decodeFull(parseAll.toList()[0]);
//      if (finalUrl.indexOf('\\u00') > -1)
//        finalUrl = finalUrl.substring(0, finalUrl.indexOf('\\u00'));
//      return finalUrl;
//    }
//  }
}
