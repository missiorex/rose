// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:mem_blocs/mem_blocs.dart';

class AddEditRosaryScreen extends StatefulWidget {
  final MEMmessage rosaryMessage;
  AddEditRosaryScreen({
    Key key,
    this.rosaryMessage,
  }) : super(key: key ?? MemKeys.addRosaryScreen);
  @override
  AddEditRosaryScreenState createState() => AddEditRosaryScreenState();
}

class AddEditRosaryScreenState extends State<AddEditRosaryScreen>
    with SingleTickerProviderStateMixin {
  static final GlobalKey<FormState> rosaryFormKey = GlobalKey<FormState>();
  Animation animation;
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: Duration(milliseconds: 2000), vsync: this);

    animation = Tween(begin: 0.0, end: 400.0).animate(animationController);
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animationController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animationController.forward();
      }
    });
    animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    final rosaryBloc = RosaryProvider.of(context);
    final messageBloc = MessageProvider.of(context);
    final userBloc = UserProvider.of(context);
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-1.0, 0),
              end: Alignment(
                  1.0, 0.0), // 10% of the width, so there are ten blinds.
              colors: [
                Theme.of(context).primaryColorDark,
                Theme.of(context).backgroundColor
              ], // whitish to gray
              tileMode:
                  TileMode.repeated, // repeats the gradient over the canvas
            ),
          ),
        ),
        title: Text(isEditing
            ? MemLocalizations(Localizations.localeOf(context)).editRosary
            : MemLocalizations(Localizations.localeOf(context)).addRosary),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: rosaryFormKey,
          autovalidate: false,
          onWillPop: () {
            return Future(() => true);
          },
          child: StreamBuilder<int>(
              stream: rosaryBloc.rosaryTotalCount,
              initialData: 0,
              builder: (context, snapshot) => new ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: Text(
                            "Total Rosary Count : " + snapshot.data.toString()),
                      ),
//                      msgTitleField(messageBloc),
                      rosaryCountField(messageBloc, context),
                      submitButton(messageBloc, userBloc),
//                      msgBodyField(messageBloc),
                    ],
                  )),
        ),
      ),
//      floatingActionButton: StreamBuilder(
//          stream: messageBloc.submitValidRosaryMessage,
//          builder: (context, snapshot) {
//            return Container(
//                child: FloatingActionButton.extended(
//                    label: Text("Submit"),
//                    tooltip: isEditing
//                        ? MemLocalizations(Localizations.localeOf(context))
//                            .saveChanges
//                        : MemLocalizations(Localizations.localeOf(context))
//                            .addMessage,
//                    icon: Icon(isEditing ? Icons.check : Icons.add),
//                    onPressed: () {
//                      final form = rosaryFormKey.currentState;
//                      if (form.validate() && snapshot.hasData) {
//                        if (isEditing) {
//                          messageBloc.updateRosary(rosaryMessage);
//                        } else {
//                          messageBloc.addRosary(userBloc.userDetails);
//                          //rosaryBloc.RosaryAddition.add(rosaryMessage.rosaryCount);
//                          //appState.rosary.totalCount = int.parse(count);
//
//                        }
//
//                        Navigator.pop(context);
//                      }
//                    }));
//          }),
    );
  }

  bool get isEditing => widget.rosaryMessage != null;

  Widget msgTitleField(MessageBloc bloc) {
    return StreamBuilder(
      stream: bloc.msgTitle,
      builder: (context, snapshot) {
        return TextField(
          autofocus: isEditing ? false : true,
          onChanged: bloc.changeMsgTitle,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: MemLocalizations(Localizations.localeOf(context))
                .newMessageTitleHint,
            labelText: 'Message Title',
            errorText: snapshot.error,
          ),
        );
      },
    );
  }

  Widget msgBodyField(MessageBloc bloc) {
    return StreamBuilder(
      stream: bloc.msgBody,
      builder: (context, snapshot) {
        return TextField(
          onChanged: bloc.changeMsgBody,
          keyboardType: TextInputType.text,
          maxLines: 5,
          style: Theme.of(context).textTheme.subhead,
          decoration: InputDecoration(
            hintText:
                MemLocalizations(Localizations.localeOf(context)).messageHint,
            labelText: 'Message Details',
            errorText: snapshot.error,
          ),
        );
      },
    );
  }

  Widget rosaryCountField(MessageBloc messageBloc, BuildContext context) {
    final rosaryBloc = RosaryProvider.of(context);
    return StreamBuilder(
      stream: messageBloc.rosaryCount,
      builder: (context, snapshot) {
        return Row(
          children: <Widget>[
            Expanded(
                flex: 2,
                child: TextField(
                  onChanged: messageBloc.changeRosaryCount,
                  keyboardType: TextInputType.number,
                  maxLines: 1,
                  style: Theme.of(context).textTheme.subhead,
                  decoration: InputDecoration(
                    hintText: MemLocalizations(Localizations.localeOf(context))
                        .messageHint,
                    labelText: 'Rosary Count',
                    errorText: snapshot.error,
                  ),
                )),
          ],
        );
      },
    );
  }

  Widget submitButton(MessageBloc messageBloc, UserBloc userBloc) {
    return StreamBuilder(
      stream: messageBloc.submitValidRosaryMessage,
      builder: (context, snapshot) {
        return Container(
            margin: EdgeInsets.only(top: 40.0),
            padding: EdgeInsets.only(
                top: 20.0, bottom: 20.0, left: 60.0, right: 60.0),
            child: FlatButton(
                padding: EdgeInsets.only(
                    top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
                color: Colors.deepOrange,
                splashColor: Colors.deepOrange,
                child: Text("Submit"),
                onPressed: () async {
                  try {
                    final form = rosaryFormKey.currentState;
                    if (form.validate() && snapshot.hasData) {
                      setState(() {
                        if (isEditing) {
                          messageBloc.updateRosary(widget.rosaryMessage);
                        } else {
                          messageBloc.addRosary(userBloc.userDetails,
                              MemLists().shortBibleVerseML);
                          //rosaryBloc.RosaryAddition.add(rosaryMessage.rosaryCount);
                          //appState.rosary.totalCount = int.parse(count);

                        }
                      });

                      Navigator.pop(context);
                    }
                  } catch (e) {
                    showMessage(context, e.toString());
                    debugPrint("Error from Rosary Screen : " + e.toString());
                  }
                }));
      },
    );
  }

  void showMessage(BuildContext context, String message,
      [MaterialColor color = Colors.red]) {
    debugPrint("Error:$message");
    Scaffold.of(context).showSnackBar(new SnackBar(
      backgroundColor: color,
      content: new Text(message),
      duration: Duration(seconds: 10),
    ));
  }
}
