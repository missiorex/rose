import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:memapp/widgets/popup/popuplib.dart';
import 'package:memapp/widgets/scratch_card.dart';
import 'package:share/share.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:utility/src/dialogshower.dart' as DialogShower;
import 'package:liquid_progress_indicator/src/liquid_custom_progress_indicator.dart';
import 'package:liquid_progress_indicator/src/liquid_linear_progress_indicator.dart';

class AddEditMessageScreen extends StatefulWidget {
  final MEMmessage message;
  final String msgType;
  final FacingPage selectedPage;
  RemoteConfig remoteConfig;

  @override
  _AddEditMessageState createState() => _AddEditMessageState();

  AddEditMessageScreen({
    Key key,
    this.message,
    this.msgType,
    this.selectedPage,
    this.remoteConfig,
  }) : super(key: key ?? MemKeys.addMessageScreen);
}

class _AddEditMessageState extends State<AddEditMessageScreen> {
  static final GlobalKey<FormState> messageFormKey = GlobalKey<FormState>();
  DatabaseReference _messagesReference = FirebaseDatabase.instance.reference();
  String _message = "";
  String _adminMsgTitle = "";
  TextEditingController msgController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController adminMsgController = TextEditingController();
  String selectedRegion;
  String selectedLocale;
  List<String> locales;
  List<String> regions;
  String adminMsgResponse = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    msgController.dispose();
    titleController.dispose();
    adminMsgController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final messageBloc = MessageProvider.of(context);
    final userBloc = UserProvider.of(context);
    final container = AppStateContainer.of(context);

    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-1.0, 0),
              end: Alignment(
                  1.0, 0.0), // 10% of the width, so there are ten blinds.
              colors: [
                Theme.of(context).primaryColorDark,
                Theme.of(context).backgroundColor
              ], // whitish to gray
              tileMode:
                  TileMode.repeated, // repeats the gradient over the canvas
            ),
          ),
        ),
        title: widget.msgType == "guidance"
            ? Text("Need Guidance ?")
            : Text(isEditing
                ? widget.selectedPage.isEditingTitle
                : widget.selectedPage.isAddTitle),
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 10.0),
        padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
        decoration: new BoxDecoration(
          color: Theme.of(context).primaryColorLight,
        ),
        child: Form(
          key: messageFormKey,
          autovalidate: false,
          onWillPop: () {
            return Future(() => true);
          },
          child: ListView(
            children: [
              widget.msgType == "admin"
                  ? msgTitleField(messageBloc)
                  : Container(),
              widget.msgType == "admin"
                  ? adminMsgBodyField(messageBloc)
                  : msgBodyField(messageBloc),
              widget.msgType == "admin" ? localeField(userBloc) : Container(),
              widget.msgType == "admin" ? regionField(userBloc) : Container(),
              submitButton(messageBloc, container),
            ],
          ),
        ),
      ),
    );
  }

  Widget msgTitleField(MessageBloc bloc) {
    var textStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).backgroundColor,
        fontWeight: FontWeight.w600,
        fontSize: 16.0);

    return StreamBuilder(
      stream: bloc.msgTitle,
      builder: (context, snapshot) {
        return Container(
            color: Colors.white,
            child: TextField(
              onChanged: bloc.changeMsgTitle,
              keyboardType: TextInputType.text,
              autofocus: true,
              controller: titleController,
              style: textStyle,
              cursorColor: Color.fromRGBO(11, 129, 140, 1.0),
              decoration: InputDecoration(
                  hintText: MemLocalizations(Localizations.localeOf(context))
                      .newMessageTitleHint,
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(217, 178, 80, 1.0))),
                  fillColor: Colors.white,
                  contentPadding: EdgeInsets.all(12.0),
                  errorText: snapshot.error,
                  errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(color: Colors.white)),
                  errorStyle: TextStyle(fontSize: 8.0)),
            ));
      },
    );
  }

  Widget msgBodyField(MessageBloc bloc) {
    var textStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).backgroundColor,
        fontWeight: FontWeight.w600,
        fontSize: 16.0);

    return StreamBuilder(
      stream: bloc.msgBody,
      builder: (context, snapshot) {
        return Container(
            color: Colors.white,
            child: TextField(
              onChanged: bloc.changeMsgBody,
              keyboardType: TextInputType.multiline,
              controller: msgController,
              maxLines: 5,
              autofocus: true,
              textInputAction: TextInputAction.newline,
              style: textStyle,
              cursorColor: Color.fromRGBO(11, 129, 140, 1.0),
              decoration: InputDecoration(
                  hintText: widget.msgType == "guidance"
                      ? "Guidance Request..."
                      : widget.msgType == "request"
                          ? MemLocalizations(Localizations.localeOf(context))
                              .requestHint
                          : MemLocalizations(Localizations.localeOf(context))
                              .testimonyHint,
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(217, 178, 80, 1.0))),
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(
                          color: Color.fromRGBO(217, 178, 80, 1.0))),
                  contentPadding: EdgeInsets.all(12.0),
                  errorText: snapshot.error,
                  errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(color: Colors.white)),
                  errorStyle: TextStyle(fontSize: 8.0)),
            ));
      },
    );
  }

  Widget adminMsgBodyField(MessageBloc bloc) {
    return StreamBuilder(
      stream: bloc.adminMsgBody,
      builder: (context, snapshot) {
        return Container(
            margin: EdgeInsets.only(top: 10.0),
            color: Colors.white,
            child: TextField(
              onChanged: bloc.changeAdminMsgBody,
              keyboardType: TextInputType.multiline,
              maxLines: 5,
              controller: adminMsgController,
              textInputAction: TextInputAction.newline,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 18.0,
//                    height: 0.05,
                color: Colors.black,
              ),
              decoration: InputDecoration(
                  hintText: "Admin Message...",
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(217, 178, 80, 1.0))),
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(
                          color: Color.fromRGBO(217, 178, 80, 1.0))),
                  contentPadding: EdgeInsets.all(12.0),
                  errorText: snapshot.error,
                  errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(color: Colors.white)),
                  errorStyle: TextStyle(fontSize: 8.0)),
            ));
      },
    );
  }

  Widget localeField(UserBloc bloc) {
    var container = AppStateContainer.of(context);
//    List<String> languages = this.locales.where((i) => i != "Send to All").toList();
    this.locales = container.state.locales ?? <String>['English', 'Malayalam'];
    debugPrint("locales:  " + locales.toString());

    return StreamBuilder(
      stream: bloc.locale,
      builder: (context, snapshot) {
        return Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 10.0),
            child: Row(children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(
                        left: 10.0,
                        right: 20.0,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                              alignedDropdown: false,
                              child: DropdownButton<String>(
                                  hint: Text("Language"),
                                  value: selectedLocale,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18.0,
                                    color: Colors.black,
                                  ),
                                  onChanged: (String changedValue) {
                                    bloc.changeLocale(changedValue);
                                    setState(() {
                                      selectedLocale = changedValue;
                                    });
                                  },
                                  items: locales.map((String value) {
                                    return DropdownMenuItem<String>(
                                        value: value, child: Text(value));
                                  }).toList())))))
            ]));
      },
    );
  }

  Widget regionField(UserBloc bloc) {
    var container = AppStateContainer.of(context);
//    List<String> languages = this.locales.where((i) => i != "Send to All").toList();
    this.regions = <String>[
      'Asia',
      'Australia',
      'Africa',
      'Europe',
      'North America',
      'South America',
      'Antarctica',
      "All"
    ];

    debugPrint("Regions at field:  " + regions.toString());

    return StreamBuilder(
      stream: bloc.region,
      builder: (context, snapshot) {
        return Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 10.0),
            child: Row(children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(
                        left: 10.0,
                        right: 20.0,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                              alignedDropdown: false,
                              child: DropdownButton<String>(
                                  hint: Text("Region"),
                                  value: selectedRegion,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18.0,
                                    color: Colors.black,
                                  ),
                                  onChanged: (String changedValue) {
                                    bloc.changeRegion(changedValue);
                                    setState(() {
                                      selectedRegion = changedValue;
                                    });
                                  },
                                  items: regions.map((String value) {
                                    return DropdownMenuItem<String>(
                                        value: value, child: Text(value));
                                  }).toList())))))
            ]));
      },
    );
  }

  Widget submitButton(MessageBloc messageBloc, var container) {
    UserBloc userBloc = UserProvider.of(context);
    String confirmation = "";

    return StreamBuilder(
        stream: widget.msgType == "admin"
            ? messageBloc.submitValidAdminMessage
            : messageBloc.submitValidMessage,
        builder: (context, snapshot) {
          return Container(
              margin: EdgeInsets.only(top: 10.0, left: 70.0, right: 70.0),
              padding: EdgeInsets.only(
                  top: 0.0, bottom: 10.0, left: 10.0, right: 10.0),
              child: FlatButton(
                  padding: EdgeInsets.only(
                      top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
                  color: Theme.of(context).buttonColor,
                  splashColor: Theme.of(context).accentColor,
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    final form = messageFormKey.currentState;
                    double successRate = 0.0;

                    if (form.validate() && snapshot.hasData) {
                      if (isEditing) {
                        messageBloc.updateMessage(widget.msgType);
                      } else {
                        selectedRegion =
                            selectedRegion ?? userBloc.userDetails.region;
                        selectedLocale =
                            selectedLocale ?? userBloc.userDetails.locale;

                        if (widget.msgType == "admin") {
                          List<MemUser> _members = container.state.members;
                          List<MemUser> _regionalMembers;
//
//                          if (selectedRegion == "All") {
//                            _regionalMembers = _members
//                                .where((i) => i.locale == selectedLocale)
//                                .toList();
//                          } else {
//                            _regionalMembers = _members
//                                .where((i) =>
//                                    i.region == selectedRegion &&
//                                    i.locale == selectedLocale)
//                                .toList();
//                          }
//
//                          _regionalMembers.forEach((member) {
//                            MEMAction adminMsgAction = MEMAction(
//                                author: userBloc.userDetails.name,
//                                notified: false,
//                                priority: "high",
//                                summary: titleController.value.text,
//                                time: DateTime.now().millisecondsSinceEpoch,
//                                title: titleController.value.text,
//                                body: adminMsgController.value.text,
//                                topic: "big_text");
//
//                            messageBloc.messageRepository.addUserAction(
//                                member.id, adminMsgAction.toEntity());
//                          });
                          setState(() {
                            _message = adminMsgController.value.text;
                            _adminMsgTitle = titleController.value.text;
                          });

                          if (selectedRegion == "All") {
                            List<String> _regions = container.state.regions;

                            showPleaseWait();

                            String id = Uuid().generateV4();

                            debugPrint("Message ID for all :" + id);

                            _regions.forEach((region) async {
                              confirmation = await messageBloc.addMessage(
                                  widget.msgType,
                                  container.state.userDetails,
                                  region,
                                  selectedLocale,
                                  msgId: id);

                              if (confirmation != '') {
                                successRate += 0.2;
                                adminMsgResponse =
                                    adminMsgResponse + "\r\n" + confirmation;
                                showAdminMessageProgress(
                                    successRate: successRate,
                                    confirmation: adminMsgResponse,
                                    singleRegion: false);
                              }
                            });

                            _sendMsgtoChatRoom(
                                selectedLocale: selectedLocale,
                                selectedRegion: "All",
                                msgId: id);
                          } else {
                            //Anonymous function to return the message after async await .
                            showPleaseWait();
                            var sendSingleRegionalMessage = (() async {
                              String id = Uuid().generateV4();

                              var confirmation = await messageBloc.addMessage(
                                  widget.msgType,
                                  container.state.userDetails,
                                  selectedRegion,
                                  selectedLocale,
                                  msgId: id);

                              _sendMsgtoChatRoom(
                                  selectedLocale: selectedLocale,
                                  selectedRegion: selectedRegion,
                                  msgId: id);
                              if (confirmation != '') {
                                successRate += 0.2;
                                showAdminMessageProgress(
                                    successRate: successRate,
                                    confirmation: confirmation,
                                    singleRegion: true);
                              }
                            });

                            sendSingleRegionalMessage();
                          }
                        } else {
                          setState(() {
                            _message = msgController.value.text;
                          });

                          String id = Uuid().generateV4();

                          messageBloc.addMessage(
                              widget.msgType,
                              container.state.userDetails,
                              selectedRegion,
                              selectedLocale,
                              msgId: id);

                          debugPrint("Firestore Message ID ==||==" + id);

                          _sendMsgtoChatRoom(
                              selectedLocale: selectedLocale,
                              selectedRegion: selectedRegion,
                              msgId: id);

                          if (widget.msgType == "request")
                            showPopup(context, _popupBody(),
                                'Add Prayer Request Gift Verse');
                          else if (widget.msgType == "testimony") {
//                            debugPrint("Testimony added");
                            showError(
                              errorMsg: MemError(
                                  title: "Your Testimony Added Succesfully",
                                  body:
                                      "Thanks for sharing your testimony. God bless you"),
                            );
                          } else if (widget.msgType == "guidance") {
                            showError(
                                errorMsg: MemError(
                                    title:
                                        "We have recieved your guidance request ",
                                    body:
                                        "We will get back to you soon.Watch out this space for our response. God bless you."));
                          }
                        }
                      }
                    }
                  }));
        });
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "OK",
      confirmFn: () {
        Navigator.pop(context);
        Navigator.popUntil(context, ModalRoute.withName('/home'));
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void showAdminMessageProgress(
      {double successRate, String confirmation, bool singleRegion}) {
    var deviceHeight = MediaQuery.of(context).size.height;
    var deviceWidth = MediaQuery.of(context).size.width;

    debugPrint("Succes Rate: " + successRate.toString());

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: Center(child: Text('Sending Admin Message')),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              actions: <Widget>[
                RaisedButton(
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black12,
                  color: Colors.green,
                  child: Text(
                    "OK",
                  ),
                  onPressed: singleRegion
                      ? successRate == 0.2
                          ? () {
                              Navigator.pop(context);
                              Navigator.popUntil(
                                  context, ModalRoute.withName('/home'));
                            }
                          : null
                      : successRate == 1.4
                          ? () {
                              Navigator.pop(context);
                              Navigator.popUntil(
                                  context, ModalRoute.withName('/home'));
                            }
                          : null,
                )
              ],
              backgroundColor: Colors.amberAccent,
              content: Stack(overflow: Overflow.visible, children: <Widget>[
                Positioned(
                    right: 10.0,
                    top: 10.0,
                    //return dialog;
                    child: LiquidCustomProgressIndicator(
                      value: singleRegion
                          ? successRate * 6
                          : successRate, // Defaults to 0.5.
                      valueColor: AlwaysStoppedAnimation(Colors
                          .pink), // Defaults to the current Theme's accentColor.
                      backgroundColor: Colors
                          .white, // Defaults to the current Theme's backgroundColor.
                      direction: Axis
                          .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right).
                      shapePath: Path()
                        ..moveTo(50, 0)
                        ..quadraticBezierTo(0, 0, 0, 37.5)
                        ..quadraticBezierTo(0, 75, 25, 75)
                        ..quadraticBezierTo(25, 95, 5, 95)
                        ..quadraticBezierTo(35, 95, 40, 75)
                        ..quadraticBezierTo(100, 75, 100, 37.5)
                        ..quadraticBezierTo(100, 0, 50, 0)
                        ..close(), // A Path object used to draw the shape of the progress indicator. The size of the progress indicator is created from the bounds of this path.
                    )),
                Positioned(
                    left: 10.0,
                    top: 100.0,
                    //return dialog;
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: deviceHeight * 0.7,
                        width: deviceWidth * 0.6,
                        child: singleRegion
                            ? Text(
                                successRate == 0.2
                                    ? "The message sent succesfully . "
                                    : "Message Sending in Progress ...",
                                style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.pink,
                                    fontWeight: FontWeight.bold),
                                maxLines: 10,
                              )
                            : Text(
                                successRate == 1.4
                                    ? "All messages succesfully sent. "
                                    : "Messages Sending in Progress ...",
                                style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.pink,
                                    fontWeight: FontWeight.bold),
                                maxLines: 10,
                              ))),
                Positioned(
                    left: 10.0,
                    top: 150.0,
                    //return dialog;
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: deviceHeight * 0.7,
                        width: deviceWidth * 0.7,
                        child: Text(
                          "\n Messages sent to : \n" + confirmation,
                          style: TextStyle(fontSize: 11.0),
                          maxLines: 20,
                        )))
              ]));
        });
  }

  void showPleaseWait() {
    var deviceHeight = MediaQuery.of(context).size.height;
    var deviceWidth = MediaQuery.of(context).size.width;

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: Center(child: Text('Sending Admin Message')),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              actions: <Widget>[
                RaisedButton(
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black12,
                  child: Text(
                    "OK",
                  ),
                  onPressed: null,
                )
              ],
              backgroundColor: Colors.amberAccent,
              content: Stack(overflow: Overflow.visible, children: <Widget>[
                Positioned(
                    right: 10.0,
                    top: 10.0,
                    //return dialog;
                    child: LiquidCustomProgressIndicator(
                      value: 0.2, // Defaults to 0.5.
                      valueColor: AlwaysStoppedAnimation(Colors
                          .pink), // Defaults to the current Theme's accentColor.
                      backgroundColor: Colors
                          .white, // Defaults to the current Theme's backgroundColor.
                      direction: Axis
                          .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right).
                      shapePath: Path()
                        ..moveTo(50, 0)
                        ..quadraticBezierTo(0, 0, 0, 37.5)
                        ..quadraticBezierTo(0, 75, 25, 75)
                        ..quadraticBezierTo(25, 95, 5, 95)
                        ..quadraticBezierTo(35, 95, 40, 75)
                        ..quadraticBezierTo(100, 75, 100, 37.5)
                        ..quadraticBezierTo(100, 0, 50, 0)
                        ..close(), // A Path object used to draw the shape of the progress indicator. The size of the progress indicator is created from the bounds of this path.
                    )),
                Positioned(
                    left: 10.0,
                    top: 100.0,
                    //return dialog;
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: deviceHeight * 0.7,
                        width: deviceWidth * 0.6,
                        child: Text(
                          "Please wait",
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.pink,
                              fontWeight: FontWeight.bold),
                          maxLines: 10,
                        ))),
                Positioned(
                    left: 10.0,
                    top: 150.0,
                    //return dialog;
                    child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: deviceHeight * 0.7,
                        width: deviceWidth * 0.6,
                        child: Text(
                          "System is preparing to send the messages to the selected regions....",
                          style: TextStyle(fontSize: 15.0),
                          maxLines: 10,
                        )))
              ]));
        });
  }

  showPopup(BuildContext context, Widget widget, String title,
      {BuildContext popupContext}) {
    Navigator.push(
      context,
      PopupLayout(
        top: 80,
        left: 30,
        right: 30,
        bottom: 50,
        child: PopupContent(
          content: Scaffold(
            resizeToAvoidBottomInset: false,
            //resizeToAvoidBottomPadding: false,
            body: widget,
            backgroundColor: Colors.transparent,
          ),
        ),
      ),
    );
  }

  Widget _popupBody() {
    var closeButtonRadius = MediaQuery.of(context).size.width * 0.1;
    var iconSize = MediaQuery.of(context).size.width * 0.065;
    UserBloc userBloc = UserProvider.of(context);

    String giftVerse = MemLists().getRandomListElement(
        listType: "bible_verse", locale: userBloc.userDetails.locale);
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;
    var giftVerseFontSize = isTablet ? 30.0 : 12.0;
    var giftVerseLeftMargin = isTablet
        ? MediaQuery.of(context).size.width * 0.24
        : MediaQuery.of(context).size.width * 0.16;
    var giftVerseRightMargin = isTablet
        ? MediaQuery.of(context).size.width * 0.24
        : MediaQuery.of(context).size.width * 0.16;
    var shareTopMargin = isTablet
        ? MediaQuery.of(context).size.height * 0.45
        : MediaQuery.of(context).size.height * 0.40;

    return Stack(
      children: <Widget>[
        Align(
            alignment: Alignment.center,
            child: ScratchCard(
              cover: Stack(fit: StackFit.expand, children: <Widget>[
                Align(
                    alignment: Alignment.center,
                    child: Container(
                        height: MediaQuery.of(context).size.height * 0.6,
                        decoration: new BoxDecoration(
                          image: new DecorationImage(
                              image:
                                  new AssetImage("assets/gift_verse_cover.png"),
                              fit: BoxFit.contain,
                              repeat: ImageRepeat.noRepeat),
                        ),
                        child: Container()))
              ]),
              reveal: Stack(fit: StackFit.expand, children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    decoration: BoxDecoration(
                        image: new DecorationImage(
                            image: new AssetImage("assets/gift_verse_bg.png"),
                            fit: BoxFit.contain,
                            repeat: ImageRepeat.noRepeat)),
                    child: Center(
                      child: Container(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.15,
                              left: giftVerseLeftMargin,
                              right: giftVerseRightMargin),
                          child: Text(
                            giftVerse,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: giftVerseFontSize,
                              fontWeight: FontWeight.bold,
                              color: Color.fromRGBO(59, 35, 35, 1.0),
                            ),
                          )),
                    ),
                  ),
                )
              ]),
              strokeWidth: 15.0,
              finishPercent: 50,
              onComplete: () => print('The card is now clear!'),
            )),
        Align(
            alignment: Alignment.topCenter,
            child: Container(
                margin: EdgeInsets.only(bottom: closeButtonRadius),
                width: closeButtonRadius,
                height: closeButtonRadius,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(62, 83, 117, 0.8),
                    borderRadius:
                        BorderRadius.all(Radius.circular(closeButtonRadius))),
                child: IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: closeButtonRadius * 0.5,
                  ),
                  onPressed: () {
                    try {
//                      Navigator.pop(context); //close the popup
                      Navigator.popUntil(context, ModalRoute.withName('/home'));
                    } catch (e) {}
                  },
                ))),
//        Positioned(
//            bottom: MediaQuery.of(context).size.height * 0.18,
//            left: MediaQuery.of(context).size.width * 0.35,
        Align(
            alignment: Alignment.center,
            child: Container(
                margin: EdgeInsets.only(top: shareTopMargin),
                child: IconButton(
                    color: Colors.black,
                    icon: ImageIcon(AssetImage('assets/icons/share.png'),
                        size: isTablet ? iconSize * 3.0 : iconSize),
                    onPressed: (() {
                      final RenderBox box = context.findRenderObject();
                      Share.share(giftVerse,
                          sharePositionOrigin:
                              box.localToGlobal(Offset.zero) & box.size);
                    }))))
      ],
    );
  }

  void _sendMsgtoChatRoom(
      {String selectedLocale, String selectedRegion, String msgId}) {
    MessageBloc messageBloc = MessageProvider.of(context);
    UserBloc userBloc = UserProvider.of(context);

    String id = msgId ?? Uuid().generateV4();

    String msgTitle = "";

    switch (widget.msgType) {
      case "request":
        msgTitle = "Prayer Request";
        break;
      case "testimony":
        msgTitle = "Testimony";
        break;
      case "admin":
        msgTitle = "Admin Message";
        break;
    }

    if (widget.msgType == "request" || widget.msgType == "testimony") {
      var message = {
        'id': id,
        'sender': {
          'name': userBloc.userDetails.displayName ?? userBloc.userDetails.name,
          'id': userBloc.userDetails.id,
          'imageUrl': userBloc.userDetails.profilePhotoUrl
        },
        'text': widget.msgType == "request"
            ? "\n🙏 " + msgTitle + " 🙏\n\n" + _message
            : "\n✨ " + msgTitle + " ✨\n\n" + _message,
        'time': DateTime.now().millisecondsSinceEpoch,
        'type': widget.msgType,
        'region': 'All',
        'locale': userBloc.userDetails.locale
      };
      _messagesReference
          .child("chats")
          .child(userBloc.userDetails.locale ?? "English")
          .child(id)
          .set(message);

      debugPrint("Chat Message ID ==||==" + id);

      FCMNotification notification = FCMNotification(
          title:
              "New " + msgTitle + " from " + userBloc.userDetails.displayName ??
                  userBloc.userDetails.name,
          body: _message ?? "",
          topic: userBloc.userDetails.locale ?? "English",
          author: userBloc.userDetails.name,
          time: DateTime.now().add(Duration(minutes: 1)),
          authorID: userBloc.userDetails.id ?? "",
          priority: "medium");

      if (widget.remoteConfig.getBool('fcm_notifcation_enable'))
        messageBloc.messageRepository.addNotification(notification.toEntity());
    } else if (widget.msgType == "admin") {
      var message = {
        'id': id,
        'sender': {
          'name': "Admin",
          'id': userBloc.userDetails.id,
          'imageUrl': userBloc.userDetails.profilePhotoUrl
        },
        'text':
            "\n📢 " + msgTitle + " 📢\n\n" + _adminMsgTitle + "\n\n" + _message,
        'time': DateTime.now().millisecondsSinceEpoch,
        'type': 'admin',
        'region': selectedRegion ?? userBloc.userDetails.region ?? "All",
        'locale': selectedLocale ?? userBloc.userDetails.locale ?? "English",
      };
      _messagesReference
          .child("chats")
          .child(selectedLocale ?? userBloc.userDetails.locale ?? "English")
          .child(id)
          .set(message);

//Disabling Admin message notification to avoid repetition for every region submission.

      FCMNotification notification = FCMNotification(
          title: "New " + msgTitle,
          body: _message ?? "",
          topic: selectedLocale ?? userBloc.userDetails.locale ?? "English",
          author: userBloc.userDetails.name,
          time: DateTime.now().add(Duration(minutes: 1)),
          authorID: userBloc.userDetails.id ?? "",
          priority: "medium");

      messageBloc.messageRepository.addNotification(notification.toEntity());
    }
  }

  bool get isEditing => widget.message != null;
}

class _AnimatedLiquidLinearProgressIndicator extends StatefulWidget {
  @override
  State<StatefulWidget> createState() =>
      _AnimatedLiquidLinearProgressIndicatorState();
}

class _AnimatedLiquidLinearProgressIndicatorState
    extends State<_AnimatedLiquidLinearProgressIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );

    _animationController.addListener(() => setState(() {}));
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var deviceWidth = MediaQuery.of(context).size.width;

    final percentage = _animationController.value * 100;
    return Center(
      child: Container(
        width: deviceWidth * 0.5,
        height: 75.0,
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: LiquidLinearProgressIndicator(
          value: _animationController.value,
          backgroundColor: Colors.white,
          valueColor: AlwaysStoppedAnimation(Colors.blue),
          borderRadius: 12.0,
          center: Text(
            "${percentage.toStringAsFixed(0)}%",
            style: TextStyle(
              color: Colors.lightBlueAccent,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
