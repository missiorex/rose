import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:utility/utility.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memapp/app_state_container.dart';
import 'dart:io';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart' as p;
import 'package:mem_blocs/mem_blocs.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;
import 'package:cached_network_image/cached_network_image.dart';

class AddGospelCardScreen extends StatefulWidget {
  AddGospelCardScreen({key: Key, this.storage})
      : super(key: MemKeys.addGospelCardScreen);

  final FirebaseStorage storage;

  @override
  _AddGospelCardScreenState createState() => new _AddGospelCardScreenState();
}

class _AddGospelCardScreenState extends State<AddGospelCardScreen> {
  File _image;
  Future<File> _imageFile;
  String selectedLocale;
  String selectedCardType;
  List<String> locales;
  List<String> cardTypes;

  static final GlobalKey<FormState> addGospelCardFormKey =
      GlobalKey<FormState>();
  GospelCard gospelCard = GospelCard();
  bool imageLoading = true;

  initState() {
    imageLoading = true;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final container = AppStateContainer.of(context);
    final MessageBloc messageBloc = MessageProvider.of(context);

    return Scaffold(
      appBar: AppBar(
          title: Text("Add Gospel Card"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      body: Container(
        color: Theme.of(context).primaryColorLight,
        padding: EdgeInsets.all(16.0),
        child: Form(
            key: addGospelCardFormKey,
            autovalidate: false,
            onWillPop: () {
              return Future(() => true);
            },
            child: ListView(
              children: [
                gospelCardTitleField(),
                gospelCardDetails(),
                localeField(),
                gospelCardTypeField(),
//                contentUrlField(),
                _previewImage(),
                //gospelCardImageField("Upload Image"),
                imageLoading ? Container() : addGospelCardButton(messageBloc),
              ],
            )),
      ),
    );
  }

  Widget gospelCardTitleField() {
    return Container(
        decoration:
            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: const Icon(Icons.title),
            hintText: 'Gospel Card Title',
            labelText: 'Gospel Card Title',
            border: InputBorder.none,
          ),
          inputFormatters: [LengthLimitingTextInputFormatter(100)],
          validator: (val) => val.isEmpty ? 'Title is required' : null,
          onSaved: (val) => gospelCard.title = val,
        ));
  }

  Widget contentUrlField() {
    return Container(
        decoration:
            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: const Icon(Icons.ondemand_video),
            hintText: 'Content Url',
            labelText: 'Content Url',
            border: InputBorder.none,
          ),
          inputFormatters: [LengthLimitingTextInputFormatter(100)],
          validator: (val) => val.isEmpty ? 'Content Url is required' : null,
          onSaved: (val) => gospelCard.contentUrl = val,
        ));
  }

  Widget gospelCardDetails() {
    return Container(
        decoration:
            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: const Icon(Icons.details),
            hintText: 'Gospel Card Details',
            labelText: 'Gospel Card  Details',
            border: InputBorder.none,
          ),
          inputFormatters: [LengthLimitingTextInputFormatter(200)],
          validator: (val) =>
              val.isEmpty ? 'Gospel Card  Details, is required' : null,
          onSaved: (val) => gospelCard.cardDetails = val,
        ));
  }

  Widget gospelCardImageField(
    String text, {
    Color backgroundColor = const Color.fromRGBO(11, 129, 140, 1.0),
    Color textColor = Colors.white,
  }) {
    UserBloc userBloc = UserProvider.of(context);
    File _profileImage;

    return ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: MaterialButton(
            child: Text(text),
            minWidth: 140.0,
            color: backgroundColor,
            textColor: textColor,
            onPressed: () {
              imageLoading = true;
              setState(() {});
              saveImageLocal(gospelCard.id).then((image) {
                _profileImage = image;

                _uploadFile(image, userBloc.userDetails.id)
                    .then((gospelCardImage) {
                  ///todo upload profile photo userBloc.userRepository.updateUser(user)
                });
              });
            }));
  }

  Widget localeField() {
    var container = AppStateContainer.of(context);
//    List<String> languages = this.locales.where((i) => i != "Send to All").toList();
    this.locales = container.state.locales ?? <String>['English', 'Malayalam'];
    debugPrint("locales:  " + locales.toString());

    return Container(
        color: Colors.white,
        margin: EdgeInsets.only(top: 10.0),
        child: Row(children: <Widget>[
          Expanded(
              flex: 2,
              child: Container(
                  padding: EdgeInsets.only(
                    left: 10.0,
                    right: 20.0,
                  ),
                  child: DropdownButtonHideUnderline(
                      child: ButtonTheme(
                          alignedDropdown: false,
                          child: DropdownButton<String>(
                              hint: Text("Language"),
                              value: selectedLocale,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18.0,
                                color: Colors.black,
                              ),
                              onChanged: (String changedValue) {
                                setState(() {
                                  selectedLocale = changedValue;
                                });
                              },
                              items: locales.map((String value) {
                                return DropdownMenuItem<String>(
                                    value: value, child: Text(value));
                              }).toList())))))
        ]));
  }

  Widget gospelCardTypeField() {
    this.cardTypes = <String>['Start', 'Slider'];

    return Container(
        color: Colors.white,
        margin: EdgeInsets.only(top: 10.0),
        child: Row(children: <Widget>[
          Expanded(
              flex: 2,
              child: Container(
                  padding: EdgeInsets.only(
                    left: 10.0,
                    right: 20.0,
                  ),
                  child: DropdownButtonHideUnderline(
                      child: ButtonTheme(
                          alignedDropdown: false,
                          child: DropdownButton<String>(
                              hint: Text("Card Type"),
                              value: selectedCardType,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18.0,
                                color: Colors.black,
                              ),
                              onChanged: (String changedValue) {
                                setState(() {
                                  selectedCardType = changedValue;
                                });
                              },
                              items: cardTypes.map((String value) {
                                return DropdownMenuItem<String>(
                                    value: value, child: Text(value));
                              }).toList())))))
        ]));
  }

  Future<String> _uploadFile(File _image, String id) async {
    String downloadUrl = "";

    List<String> fileNameList = _image.path.split("/");

    String fileName = fileNameList[fileNameList.length - 1];

    final Reference ref =
        widget.storage.ref().child('gospelCardImage').child(fileName);
    ref
        .putFile(
      _image,
      SettableMetadata(
          customMetadata: <String, String>{'iamgetype': 'profile'},
          contentType: 'image/jpeg'),
    )
        .whenComplete(() {
      ref.getDownloadURL().then((imageURI) {
        downloadUrl = imageURI.toString();
        debugPrint("Image URL from uploadTeask:" + downloadUrl);
        gospelCard.cardImageUrl = downloadUrl;

        setState(() {
          imageLoading = false;
        });
      });
    });

    return Future.value(downloadUrl);
  }

  Future<File> saveImageLocal(String uid) async {
    var _imageFile;

    ImagePicker().getImage(source: ImageSource.gallery,maxHeight: 600.0, maxWidth: 600.0).then((pickedFile) {
      if (pickedFile != null) {
        _imageFile = File(pickedFile.path);

      }
    });


    var image = await _imageFile;
    // getting a directory path for saving
    final String path = await _localPath;

    ImageProperties properties =
    await FlutterNativeImage.getImageProperties(image.path);
    File compressedFile = await FlutterNativeImage.compressImage(image.path,
        quality: 100,
        targetWidth: 600,
        targetHeight: (properties.height * 600 / properties.width).round());

    File profilePic = await compressedFile.copy(p.join(path, uid + ".jpeg"));

    setState(() {
      _image = profilePic;
    });

    return _image;
  }


  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Widget _previewImage() {
    return FutureBuilder<File>(
        future: _imageFile,
        builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
          if (imageLoading) return gospelCardImageLoader();
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            //return Image.file(snapshot.data);
            return Container(
              height: 300.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: Image.file(snapshot.data).image,
                      fit: BoxFit.cover)),
            );
          } else if (snapshot.error != null) {
            return const Text(
              'Error picking image.',
              textAlign: TextAlign.center,
            );
          } else {
            return gospelCardImage();
          }
        });
  }

  Widget gospelCardImageLoader() {
    return FutureBuilder<ImageProvider>(
        future: _loadImage(),
        builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.active:
            case ConnectionState.none:
              return CircularProgressIndicator(
                  strokeWidth: 1.0,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black));
            case ConnectionState.done:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return Container(
                    height: 300.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(image: snapshot.data)),
                    child: Stack(
                      children: <Widget>[
                        gospelCardImageField("Upload Image"),
                        Center(
                            child: CircularProgressIndicator(
                                strokeWidth: 1.0,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.black)))
                      ],
                    ));
          }
        });
  }

  Widget gospelCardImage() {
    return FutureBuilder<ImageProvider>(
        future: _loadImage(),
        builder: (BuildContext context, AsyncSnapshot<ImageProvider> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.active:
            case ConnectionState.none:
              return CircularProgressIndicator(
                  backgroundColor: Colors.white70, strokeWidth: 1.5);
            case ConnectionState.done:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return Container(
                  height: 300.0,
                  decoration: BoxDecoration(
                      image: DecorationImage(image: snapshot.data)),
                );
          }
        });
  }

  Future<ImageProvider> _loadImage() async {
    ImageProvider gospelCardImage =
        CachedNetworkImageProvider(gospelCard.cardImageUrl ?? "") ??
            NetworkImage(gospelCard.cardImageUrl ?? "") ??
            AssetImage("assets/placeholder-face.png");

    return gospelCardImage;
  }

  Widget addGospelCardButton(MessageBloc messageBloc) {
    return Container(
        margin: EdgeInsets.only(top: 5.0, left: 40.0, right: 40.0),
        padding:
            EdgeInsets.only(top: 0.0, bottom: 10.0, left: 10.0, right: 10.0),
        child: FlatButton(
            padding:
                EdgeInsets.only(top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
            color: Color.fromRGBO(11, 129, 140, 1.0),
            splashColor: Colors.deepOrange,
            child: Text(
              "Add Gospel Card",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              final FormState form = addGospelCardFormKey.currentState;

              if (!form.validate()) {
                debugPrint(
                    "Gospel Card Details Collected: " + gospelCard.toString());
                debugPrint('Some Details are missing or incorrect');
//            var dialog = DialogShower.buildDialog(
//                title: "Some Details are missing or incorrect",
//                message: """Please enter all the required details.""",
//                confirm: "OK",
//                confirmFn: () {
//                  Navigator.pop(context);
//                });
//
//            showDialog(
//                context: context,
//                builder: (context) {
//                  return dialog;
//                });
                showError(
                    errorMsg: MemError(
                        title: "Some Details are missing or incorrect",
                        body: "Please correct the missing details"));
              } else {
                form.save(); //This invokes each onSaved event
                try {
                  gospelCard.locale = selectedLocale;
                  gospelCard.time = DateTime.now();
                  gospelCard.sliderCard = true;
                  selectedCardType == "Start"
                      ? gospelCard.startUpCard = true
                      : gospelCard.startUpCard = false;
                  selectedCardType == "Slider"
                      ? gospelCard.sliderCard = true
                      : gospelCard.sliderCard = false;
                  gospelCard.isComplete = false;
                  gospelCard.title = gospelCard.title ?? "";
                  gospelCard.cardDetails = gospelCard.cardDetails ?? "";

                  messageBloc.messageRepository
                      .addGospelCard(gospelCard.toEntity());

                  showError(
                      errorMsg: MemError(
                          title: "Gospel Card Added. Please check",
                          body: "Gospel Card Added"));
                } catch (e) {
                  showError(
                      errorMsg: MemError(
                          title: "Couldnt Add the Gospel Card.",
                          body: e.toString()));
                }
              }
            }));
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      cancel: "OK",
      cancelFn: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
//        Navigator.of(context).pushNamedAndRemoveUntil(
//            "/splash", (Route<dynamic> route) => false);
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }
}
