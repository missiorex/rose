// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memapp/screens/add_benefactor_screen.dart';
import 'package:memapp/widgets/typedefs.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memapp/screens/add_benefactor_screen.dart';
import 'package:memapp/widgets/typedefs.dart';
import 'package:mem_blocs/mem_blocs.dart';

class ConsecrationScreen extends StatefulWidget {
  ConsecrationScreen({
    Key key,
  }) : super(key: MemKeys.consecrationScreen);
  @override
  _ConsecrationScreenState createState() => _ConsecrationScreenState();
}

class _ConsecrationScreenState extends State<ConsecrationScreen> {
  int _messageLength = 1000;
  int _messageLengthLimit = 1000;
  String consecrationMessage;
  String consecrationMsgTitle;

  String consecrationMsgTitleEN =
      "Consecaration To Immaculate Heart Of our Lady according to the book True Devotion To Mary by St Louis De Montfort : ";
  String consecrationMsgTitleML =
      "വിമല ഹൃദയ പ്രതിഷ്ഠ മാർപ്പാപ്പമാരുടെ പ്രസ്താവനകളിൽ നിന്ന് :";

  String consecrationMessageEN = """COMMENDATIONS OF THE POPES

Blessed Pope Pius IX (1846–78): Declared that Saint Louis De Montfort’s devotion to Mary was the best and most acceptable form of devotion to Our Lady.

Pope Leo XIII (1878–1903): Granted a Plenary Indulgence to those who make Saint Louis De Montfort’s act of consecration to the Blessed Virgin. On his deathbed he renewed the act himself and invoked the heavenly aid of Saint Louis De Montfort, whom he had beatified in 1888.

Pope Saint Pius X (1903–14): “I heartily recommend True Devotion to The Blessed Virgin, so admirably written by [Saint] De Montfort, and to all who read it grant the Apostolic Benediction.” . . .”There is no surer or easier way than Mary in uniting all men with Christ.”

Pope Benedict XV (1914–22): “A book of high authority and unction.”

Pope Pius XI (1922–39): “I have practiced this devotion ever since my youth.”

Pope Pius XII (1939–58): “God Alone was everything to him. Remain faithful to the precious heritage, which this great saint left you. It is a glorious inheritance, worthy, that you continue to sacrifice your strength and your life, as you have done until today.”

Pope Paul VI (1963–78): “We are convinced without any doubt that devotion to Our Lady is essentially joined with devotion to Christ, that it assures a firmness of conviction to faith in Him and in His Church, a vital adherence to Him and to His Church which, without devotion to Mary, would be impoverished and compromised.”

Blessed Pope John Paul II (1978–2005): “The reading of this book was a decisive turning-point in my life. I say ‘turning-point,’ but in fact it was a long inner journey . . . This ‘perfect devotion’ is indispensable to anyone who means to give himself without reserve to Christ and to the work of redemption.” . . .“It is from Montfort that I have taken my motto: ‘Totus tuus’ (‘I am all thine’). Someday I’ll have to tell you Montfortians how I discovered De Montfort’s Treatise on True Devotion to Mary, and how often I had to reread it to understand it.”

Vatican Council II (1962–1965): ‘The maternal duty of Mary toward men in no way obscures or diminishes this unique mediation of Christ, but rather shows its power. All her saving influence on men originates not from some inner necessity, but from the divine pleasure. It flows forth from the superabundance of the merits of Christ, rests on His mediation, depends entirely on it and draws all its power from it.’ . . . ‘The practices and exercises of devotion to her recommended by the Church in the course of the centuries [are to] be treasured.’ (Lumen Gentium: 60, 67).


Considering the importance and relevance of Consecration to the Immaculate Heart of Our Lady, Marian Eucharistic Ministry is having a watsapp group to support faithful to complete the 33 days preparation,

For further information Kindly whatsapp to :
+919600379011 (Jithu )
+64226196098 (Sherin )
+919447578594 (Premji)""";

  String consecrationMessageML =
      """പീയൂസ് IX: വി.ലൂയിസ് ഡി മോൺഫോർട്ടിന്റെ, മാതാവിനോടുള്ള ഭക്തി വളരെ അഭികാമ്യവും ഏറ്റവും അംഗീകാരം ഉള്ളതുമായ ദൈവ മാതൃ ഭക്തിയാകുന്നു.

ലിയോ XIII: വി.ലൂയിസ് ഡി മോൺഫോർട്ടിന്റെ, പരിശുദ്ധ കന്യകയോടുള്ള പ്രതിഷ്ഠ നടത്തുന്നവർക്ക്, പൂർണ്ണ ദണ്ഡ വിമോചനം കല്പിച്ച നുവദിച്ചു. പരിശുദ്ധ പിതാവ് തന്റെ മരണസമയത്ത് ഈ പ്രതിഷ്ഠ പുതുക്കുകയും, താൻ 1888-ൽ വാഴ്ത്തപ്പെട്ടവൻ എന്ന് പ്രഖ്യാപിച്ച വി.ലൂയിസിന്റെ സ്വർഗ്ഗീയ സഹായം അപേക്ഷിക്കുകയും ചെയ്തു.

പീയൂസ് X: "വി. ലൂയിസ് ഡി മോൺഫോർട്ടിന്റെ ഏറ്റവും മികച്ച ഗ്രന്ഥമായ "യഥാർത്ഥ മരിയഭക്തി ''യെ ഞാൻ ശക്തമായി ശുപാർശ ചെയ്യുകയും അതു വായിക്കുന്നവർക്ക് ഞാൻ അപ്പസ്തോലിക ആശീർവ്വാദം നൽകുകയും ചെയ്യുന്നു."

ബനദിക്തോസ് XV: " മികച്ച ആധികാരികതയും അഭിഷേകവും ഉള്ള പുസ്തകം"

പീയൂസ് XI: "എന്റെ യൗവ്വനകാലം മുതലേ ഞാൻ ഈ ഭക്തി പാലിച്ചു പോന്നു."

പീയൂസ് XII: "വി. ലൂയിസ് ഡി മോൺഫോർട്ടിന്റെ അപ്പസ്തോലിക ശുശ്രൂഷയ്ക്കു പുറകിലുള്ള ഏറ്റവും വലിയ ശക്തിയും, യേശുവിനു വേണ്ടി ആത്മാക്കളെ ആകർഷിക്കുകയും നേടുകയും ചെയ്യാൻ കഴിഞ്ഞതിന്റെ വലിയ രഹസ്യവും അദ്ദേഹത്തിന്റെ മരിയഭക്തിയാണ്."( 1947 ജൂലൈ 20-ന് വിശുദ്ധനെന്നു പ്രഖ്യാപിച്ചു നൽകിയ സന്ദേശത്തിൽ നിന്ന്).

പോൾ VI: "പരിശുദ്ധ കന്യകയോടുള്ള ഭക്തി യഥാർത്ഥത്തിൽ ക്രിസ്തുവിനോടുള്ള ഭക്തിയോട് ചേർന്നു നിൽക്കുന്നു. ഈ ഭക്തി യേശുവിനെയും അവിടുത്തെ സഭയെയും കുറിച്ചുള്ള നമ്മുടെ ബോധ്യങ്ങൾ ഉറപ്പിക്കും. ഇതിന്റെ അഭാവം മൂലം സഭയോടും യേശുവിനോടുമുള്ള ബന്ധം ശുഷ്ക്കവും ശിഥിലവുമാകും എന്നതിൽ നമുക്ക് സംശയാതീതമായ ബോദ്ധ്യമുണ്ട്."

ജോൺ പോൾ II: "ഈ പുസ്തക പാരായണം, എന്റെ ജീവിതത്തിലെ നിർണ്ണായകമായ വഴിത്തിരിവാണ്. ഞാൻ വഴിത്തിരിവ് എന്നു പറഞ്ഞല്ലൊ, അത് എന്റെ ആന്തരികമായ യാത്രയായിരുന്നു. ഈ "യഥാർത്ഥ ഭക്തി " കലവറ  കൂടാതെ യേശുവിനു സ്വയം നൽകുവാനും രക്ഷാകര പ്രവർത്തനങ്ങളിൽ ഏർപ്പെടുവാനും അഭിലഷിക്കുന്ന ഏതൊരുവനും അത്യാവശ്യമത്രേ. വി. ലൂയിസ് ഡി മോൺഫോർട്ടിൽ നിന്നാണ് "ഞാൻ പുർണ്ണമായും അങ്ങയുടേതാണ്'' (Totus tuus) എന്ന മുദ്രാവാക്യം സ്വീകരിച്ചിരിക്കുന്നത്.  ഞാൻ എങ്ങനെയാണ് " യഥാർത്ഥ മരിയ ഭക്തി " കണ്ടു പിടിച്ചതെന്നും അതു മനസ്സിലാക്കുന്നതിന് പലപ്പോഴും ആവർത്തിച്ചു വായിക്കേണ്ടി വന്നതെന്നും മോൺ ഫോർട്ട് സഭാംഗങ്ങളേ, നിങ്ങളോടു ഞാൻ പിന്നീട് പറയാം."

വത്തിക്കാൻ കൗൺസിൽ II: " മനുഷ്യരക്ഷയിൽ മറിയത്തിനുള്ള സ്വാധീനം, ഏതെങ്കിലും വസ്തുനിഷ്ഠമായ ആവശ്യകതയിൽ നിന്നും ഉണ്ടായതല്ല, ദൈവം തിരുമനസ്സായതുകൊണ്ടു മാത്രം അവൾ പങ്കുകാരിയായതാണ്. ക്രിസ്തുവിന്റെ യോഗ്യതകളാണ് മറിയത്തിന്റെ സ്വാധീനതയുടെ ഉദ്ഭവസ്ഥാനം. പ്രസ്തുത മാധ്യസ്ഥ്യത്തിൽ അതു പൂർണ്ണമായി ആശ്രയിച്ചു നിൽക്കുന്നു. അതിന്റെ എല്ലാ ശക്തിയും ക്രിസ്തുവിന്റെ മാധ്യസ്ഥ്യത്തിൽ നിന്നു പ്രാപിക്കുന്നു.... " (തിരുസഭ 60 ). " നൂറ്റാണ്ടുകളിലൂടെ സഭയുടെ പ്രബോധനാധികാരം അംഗീകരിച്ചിട്ടുള്ള ഭക്ത മുറകൾ വിലമതിക്കുകയും ക്രിസ്തുവിന്റെയും പരിശുദ്ധ കന്യകയുടെയും പുണ്യവാന്മാരുടെയും സ്വരൂപങ്ങൾ വണങ്ങുന്നതിനെപ്പറ്റി മുൻകാലങ്ങളിൽ ചെയ്തിട്ടുള്ള തീരുമാനങ്ങൾ ആദരപൂർവ്വം പിൻചെല്ലുകയും ചെയ്യേണ്ടിയിരിക്കുന്നു... " ( തിരുസഭ 67).

ഈ കാലഘട്ടത്തിൽ വിമലഹൃദയ പ്രതിഷ്ഠയുടെ പ്രാധാന്യം ഉൾക്കൊണ്ടുകൊണ്ട് മാതൃ ഭക്തരെ പരിശുദ്ധ ദൈവമാതാവിന്റെ വിമലഹൃദയത്തിൽ പ്രതിഷ്ഠിക്കുവാൻ സഹായിക്കുന്ന 33 ദിവസത്തെ ഒരുക്കം വാട്ട്സപ്പ് വഴി മരിയൻ യൂക്കരിസ്റ്റിക്ക് മിനിസ്ട്രി വഴി നടത്തപ്പെടുന്നു. ഇതിൽ പങ്കെടുക്കാൻ ആഗ്രഹിക്കുന്നവർ താഴെ ചേർത്തിരിക്കുന്ന നമ്പരിൽ വാട്ട്സപ്പ് ചെയ്യുക:
+919600379011 ( ജിത്തു )
+64226196098 (ഷെറിൻ)
+919447578594 (പ്രേംജി)""";

  @override
  void initState() {
    _messageLength = _messageLengthLimit;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = UserProvider.of(context);

    userBloc.userDetails.locale == "English"
        ? consecrationMessage = consecrationMessageEN
        : consecrationMessage = consecrationMessageML;
    userBloc.userDetails.locale == "English"
        ? consecrationMsgTitle = consecrationMsgTitleEN
        : consecrationMsgTitle = consecrationMsgTitleML;

    Widget content;

    content = Container(
      child: Container(
          decoration: new BoxDecoration(
            color: Theme.of(context).primaryColorLight,
          ),
          child: ListView(
            physics: ScrollPhysics(),
            shrinkWrap: true,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              // Expanded(
              //   flex: 1,
              //    child:
              Container(
                  height: MediaQuery.of(context).size.height * 0.35,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/consecration-banner.jpg"),
                      fit: BoxFit.cover,
                    ),
                  )),
              Container(
                  margin: EdgeInsets.only(
                      top: 30.0, left: 30.0, right: 30.0, bottom: 20.0),
                  child: Text(
                    consecrationMsgTitle,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0,
                    ),
                  )),
              AnimatedContainer(
                margin: EdgeInsets.only(
                    top: 5.0, left: 30.0, right: 30.0, bottom: 20.0),
                duration: const Duration(milliseconds: 120),
                child: consecrationMessage.length > _messageLengthLimit
                    ? Text(
                        consecrationMessage.substring(0, _messageLength),
                        style: TextStyle(fontSize: 14.0),
                        textAlign: TextAlign.justify,
                      )
                    : Text(
                        consecrationMessage,
                        style: TextStyle(fontSize: 14.0),
                        textAlign: TextAlign.justify,
                      ),
              ),
              GestureDetector(
                onTap: () => setState(() {
                  _messageLength != _messageLengthLimit
                      ? _messageLength = _messageLengthLimit
                      : _messageLength = consecrationMessage.length;
                }),
                child: Container(
                  margin: EdgeInsets.only(left: 140.0, bottom: 30.0),
                  child: consecrationMessage.length > _messageLengthLimit
                      ? Text(
                          _messageLength == _messageLengthLimit
                              ? "...Read More"
                              : "",
                          style: TextStyle(color: Colors.brown, fontSize: 12.0),
                        )
                      : Container(),
                ),
              ),

              // ),
            ],
          )),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          userBloc.userDetails.locale == "Malayalam"
              ? "വിമലഹൃദയപ്രതിഷ്ഠ"
              : "MARIAN CONSECRATION",
//          style: TextStyle(
//            fontWeight: FontWeight.w700,
//            fontSize: userBloc.userDetails.locale == "Malayalam" ? 15.0 : 17.0,
//          ),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-1.0, 0),
              end: Alignment(
                  1.0, 0.0), // 10% of the width, so there are ten blinds.
              colors: [
                Theme.of(context).primaryColorDark,
                Theme.of(context).backgroundColor
              ], // whitish to gray
              tileMode:
                  TileMode.repeated, // repeats the gradient over the canvas
            ),
          ),
        ),
      ),
      body: content,
      backgroundColor: Color.fromRGBO(11, 129, 140, 1.0),
    );
  }
}
