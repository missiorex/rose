import 'package:flutter/material.dart';
import 'package:memapp/screens/profile_screen.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:utility/utility.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;
import 'package:memapp/widgets/typedefs.dart';
import 'package:memapp/widgets/message_list.dart';
import 'package:memapp/widgets/notification_list.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationScreen extends StatefulWidget {
  final FirebaseStorage storage;
  final RouteObserver<PageRoute> routeObserver;
  final bool connectionStatus;
  // To send to chat screen
  final List<ChatMessage> guidanceMessages;
  final String lastVisibleChatKey;
  final int lastVisibleChatValue;
  NotificationScreen(
      {Key key,
      this.storage,
      @required this.routeObserver,
      this.connectionStatus,
      this.guidanceMessages,
      this.lastVisibleChatKey,
      this.lastVisibleChatValue})
      : super(key: MemKeys.notificationScreen);
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen>
    with SingleTickerProviderStateMixin {
  List<FCMNotification> _newUserNotifications = [];
  List<FCMNotification> _rosaryLimitNotifications = [];
  TabController _controller;

  @override
  void initState() {
    _controller = new TabController(vsync: this, length: 3);
    _controller.addListener(_handleTabSelection);
    super.initState();
  }

  bool _loadNotifications() {
    var container = AppStateContainer.of(context);

    setState(() {
      _newUserNotifications = container.state.newUserNotifications;
      _rosaryLimitNotifications = container.state.rosaryLimitNotifications;
    });

    return false;
  }

//  Widget _buildActiveMemberListTile(BuildContext context, int index) {
//    var member = _activeMembers[index];
//
//    UserBloc userBloc = UserProvider.of(context);
//    MessageBloc messageBloc = MessageProvider.of(context);
//
//    return Card(
//        color: member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
//            ? Colors.amber[200]
//            : Colors.white,
//        child: new Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
//          ListTile(
//            onTap: () => _navigateToMemberDetails(member, index),
//            leading: Hero(
//              tag: index,
//              child: CircleAvatar(
//                backgroundImage:
//                    CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
//                        NetworkImage(member.profilePhotoUrl ?? "") ??
//                        AssetImage("assets/placeholder-face.png"),
//              ),
//            ),
//            title: Row(children: <Widget>[
//              Text(member.name),
//              Text(
//                member.role == "user" ? "" : "  ~Admin",
//                style: TextStyle(color: Colors.red, fontSize: 12.0),
//              )
//            ]),
//            subtitle: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Text("Phone: " + member.phoneNumber),
//                  Text("Last Rosary On: " +
//                      new DateFormat.yMMMd().format(member.rosaryLastAddedOn)),
//                ]),
//          ),
//          ButtonTheme.bar(
//            // make buttons use the appropriate styles for cards
//            child: ButtonBar(
//              children: <Widget>[
//                member.rosaryLastAddedOn.difference(DateTime.now()).inDays < -30
//                    ? FlatButton(
//                        onPressed: member.id != userBloc.userDetails.id
//                            ? () {
//                                FCMNotification notification = FCMNotification(
//                                    title: "No Rosaries for a while",
//                                    body:
//                                        "Please recite rosaries and offer at MEMAPP",
//                                    topic: "",
//                                    token: member.pushNotificationToken ?? "",
//                                    author: userBloc.userDetails.name ?? "",
//                                    time: DateTime.now(),
//                                    authorID: userBloc.userDetails.id ?? "",
//                                    priority: "medium");
//
//                                messageBloc.messageRepository
//                                    .addNotification(notification.toEntity());
//                              }
//                            : null,
//                        child: Icon(Icons.notifications_active),
//                      )
//                    : Container(),
//                FlatButton(
//                  onPressed: member.id != userBloc.userDetails.id
//                      ? () {
//                          _launchURL("tel:" + member.phoneNumber);
//                        }
//                      : null,
//                  child: Icon(Icons.phone_forwarded),
//                ),
//                FlatButton(
//                    padding: EdgeInsets.all(5.0),
//                    child: const Text('Make Admin'),
//                    onPressed: member.role == "user" &&
//                            userBloc.userDetails.isSuperAdmin
//                        ? member.id != userBloc.userDetails.id
//                            ? () {
//                                /* ... */
//                                setState(() {
//                                  userBloc.userRepository
//                                      .assignAdminRole(member.toEntity());
//                                });
//
//                                showSnackMessage(
//                                    context, member.name + " is now an admin ",
//                                    color: Colors.blueGrey, actionText: "Okay");
//                              }
//                            : null
//                        : null),
//                FlatButton(
//                  child: const Text('Deactivate'),
//                  onPressed: userBloc.userDetails.isSuperAdmin
//                      ? member.id != userBloc.userDetails.id
//                          ? () {
//                              /* ... */
//                              setState(() {
//                                userBloc.userRepository
//                                    .deActivateUser(member.id);
//                              });
//                              showSnackMessage(
//                                  context,
//                                  member.name +
//                                      " is deactivated and is available in pending list ",
//                                  color: Colors.red,
//                                  actionText: "Okay");
//                            }
//                          : null
//                      : null,
//                ),
//              ],
//            ),
//          ),
//        ]));
//  }
//
//  Widget _buildPendingMemberListTile(BuildContext context, int index) {
//    var member = _pendingMembers[index];
//    UserBloc userBloc = UserProvider.of(context);
//
//    return Card(
//        child: new Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
//      ListTile(
//        onTap: () => _navigateToMemberDetails(member, index),
//        leading: Hero(
//          tag: index,
//          child: CircleAvatar(
//            backgroundImage:
//                CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
//                    NetworkImage(member.profilePhotoUrl ?? "") ??
//                    AssetImage("assets/placeholder-face.png"),
//          ),
//        ),
//        title: Row(children: <Widget>[
//          Text(member.name),
//          Text(
//            member.role == "user" ? "" : "  ~Admin",
//            style: TextStyle(color: Colors.red, fontSize: 12.0),
//          )
//        ]),
//        subtitle: Text("Phone: " + member.phoneNumber),
//      ),
//      ButtonTheme.bar(
//        // make buttons use the appropriate styles for cards
//        child: ButtonBar(
//          children: <Widget>[
//            FlatButton(
//                onPressed: member.id != userBloc.userDetails.id
//                    ? () {
//                        _launchURL("tel:" + member.phoneNumber);
//                      }
//                    : null,
//                child: Icon(Icons.phone_forwarded)),
//            FlatButton(
//              child: const Text('Activate'),
//              onPressed: member.id != userBloc.userDetails.id
//                  ? !member.isBlocked
//                      ? () {
//                          /* ... */
//
//                          setState(() {
//                            userBloc.userRepository.activateUser(member.id);
//                          });
//                          showSnackMessage(context,
//                              member.name + " is activated and can log in. ",
//                              color: Colors.blueGrey, actionText: "Okay");
//
////                            _pendingMembers.where((i) {
////                              i.id == member.id ? i.status == true : null;
////                            });
//                        }
//                      : null
//                  : null,
//            ),
//            FlatButton(
//              child: const Text('Block'),
//              onPressed: member.id != userBloc.userDetails.id
//                  ? !member.isBlocked
//                      ? () {
//                          /* ... */
//                          setState(() {
//                            userBloc.userRepository.blockUser(member.id);
//                          });
//                          showSnackMessage(
//                              context,
//                              member.name +
//                                  " is blocked, and is available in blocked list",
//                              color: Colors.red,
//                              actionText: "Okay");
//                        }
//                      : null
//                  : null,
//            ),
//          ],
//        ),
//      ),
//    ]));
//  }
//
//  Widget _buildBlockedMemberListTile(BuildContext context, int index) {
//    var member = _blockedMembers[index];
//    UserBloc userBloc = UserProvider.of(context);
//
//    return Card(
//        child: new Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
//      ListTile(
//        onTap: () => _navigateToMemberDetails(member, index),
//        leading: Hero(
//          tag: index,
//          child: CircleAvatar(
//            backgroundImage:
//                CachedNetworkImageProvider(member.profilePhotoUrl ?? "") ??
//                    NetworkImage(member.profilePhotoUrl ?? "") ??
//                    AssetImage("assets/placeholder-face.png"),
//          ),
//        ),
//        title: Row(children: <Widget>[
//          Text(member.name),
//          Text(
//            member.role == "user" ? "" : "  ~Admin",
//            style: TextStyle(color: Colors.red, fontSize: 12.0),
//          )
//        ]),
//        subtitle: Text("Phone: " + member.phoneNumber),
//      ),
//      ButtonTheme.bar(
//        // make buttons use the appropriate styles for cards
//        child: ButtonBar(
//          children: <Widget>[
//            FlatButton(
//                onPressed: member.id != userBloc.userDetails.id
//                    ? () {
//                        _launchURL("tel:" + member.phoneNumber);
//                      }
//                    : null,
//                child: Icon(Icons.phone_forwarded)),
//            FlatButton(
//              child: const Text('Delete'),
//              onPressed: member.id != userBloc.userDetails.id
//                  ? () {
//                      setState(() {
//                        userBloc.userRepository
//                            .deleteSingleUser(member.toEntity());
//                      });
//                      showSnackMessage(
//                          context, member.name + " is deleted permanently",
//                          color: Colors.red, actionText: "Okay");
//                      /* ... */
////                      showAlert(
////                          errorMsg: MemError(
////                              title: "You are about to delete a user",
////                              body: """You are about to delete a user : """ +
////                                  member.name +
////                                  """"
////                    Are you sure ? """),
////                          confirmFn: confirmAndDelete(member, userBloc),
////                          cancelFn: () {
////                            Navigator.pop(context);
////                          });
////
////                      Navigator.pop(context);
//                    }
//                  : null,
//            ),
//            FlatButton(
//              child: const Text('UnBlock'),
//              onPressed: member.id != userBloc.userDetails.id
//                  ? () {
//                      /* ... */
//
//                      setState(() {
//                        userBloc.userRepository.unBlockUser(member.id);
//                      });
//                      showSnackMessage(
//                          context,
//                          member.name +
//                              " is unblocked and is available in pending list ",
//                          color: Colors.blueGrey,
//                          actionText: "Okay");
//                    }
//                  : null,
//            ),
//          ],
//        ),
//      ),
//    ]));
//  }

  void _navigateToMemberDetails(MemUser member, Object avatarTag) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (c) {
          return ProfileScreen(
              member: member, avatarTag: avatarTag, storage: widget.storage);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    bool _isLoading = true;

    //_isLoading set to false, once notifications are loaded.
    _isLoading = _loadNotifications();
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    Widget content;

    if (_isLoading) {
      content = Center(
        child: CircularProgressIndicator(),
      );
    } else {
      content = TabBarView(controller: _controller, children: [
        Column(
          //modified
          children: <Widget>[
            NotificationList(
                filteredNotifications: _newUserNotifications,
                bgImageUrl: "assets/chat_bg.jpg"),
          ], //new
        ),
        Column(
          //modified
          children: <Widget>[
            NotificationList(
                filteredNotifications: _rosaryLimitNotifications,
                bgImageUrl: "assets/chat_bg.jpg"),
          ], //new
        ),
        userBloc.userDetails.isSuperAdmin && container.state.userAcl != null
            ? container.state.userAcl.viewCounselling
                ? Column(
                    //modified
                    children: <Widget>[
                      MessageList(
//                              filteredMessages: container.state
//                                  .filteredGlobalMessages(
//                                      VisibilityFilter.guidanceOnly),
                        msgType: "guidance",
                        filteredMessages: container.state.guidanceMessages,
                        bgImageUrl: "assets/chat_bg.png",
                        routeObserver: widget.routeObserver,
                        guidanceMessages: widget.guidanceMessages,
                        lastVisibleChatValue: widget.lastVisibleChatValue,
                        lastVisibleChatKey: widget.lastVisibleChatKey,
                        connectionStatus: widget.connectionStatus,
                      ),
                    ], //new
                  )
                : Column(
                    key: MemKeys.messageTab,
                    //modified
                    children: <Widget>[
                      new MessageList(
                        filteredMessages: container.state.filteredMessages(
                            VisibilityFilter.myAdminMsgsOnly,
                            userBloc.userDetails.id),
                        bgImageUrl: "assets/chat_bg.jpg",
                        routeObserver: widget.routeObserver,
                      ),
                    ], //new
                  )
            : Column(
                key: MemKeys.messageTab,
                //modified
                children: <Widget>[
                  new MessageList(
                    filteredMessages: container.state.filteredMessages(
                        VisibilityFilter.myAdminMsgsOnly,
                        userBloc.userDetails.id),
                    bgImageUrl: "assets/chat_bg.jpg",
                    routeObserver: widget.routeObserver,
                  ),
                ], //new
              ),
      ]);
    }

//    return DefaultTabController(
//        length: 4,
//        child: Scaffold(
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-1.0, 0),
              end: Alignment(
                  1.0, 0.0), // 10% of the width, so there are ten blinds.
              colors: [
                Theme.of(context).primaryColorDark,
                Theme.of(context).backgroundColor
              ], // whitish to gray
              tileMode:
                  TileMode.repeated, // repeats the gradient over the canvas
            ),
          ),
        ),
        bottom: TabBar(
          indicatorColor: Color.fromRGBO(217, 178, 80, 1.0),
          controller: _controller,
          tabs: [
            Tab(
              icon: Stack(children: <Widget>[
                new Icon(Icons.people),
                new Positioned(
                  // draw a red marble
                  top: 0.0,
                  right: 0.0,
                  child: container.state.unreadNewUserReqCount > 0
                      ? Icon(Icons.brightness_1,
                          size: 8.0, color: Colors.redAccent)
                      : Container(),
                )
              ]),
              text: "New User",
            ),
            Tab(
              icon: Stack(children: <Widget>[
                new Icon(Icons.warning),
                new Positioned(
                  // draw a red marble
                  top: 0.0,
                  right: 0.0,
                  child: container.state.unreadRosaryLimitsCount > 0
                      ? Icon(Icons.brightness_1,
                          size: 8.0, color: Colors.redAccent)
                      : Container(),
                )
              ]),
              text: "Rosary Limits",
            ),
            userBloc.userDetails.isSuperAdmin && container.state.userAcl != null
                ? container.state.userAcl.viewCounselling
                    ? Tab(
                        icon: Stack(children: <Widget>[
                          new Icon(Icons.message),
                          new Positioned(
                            // draw a red marble
                            top: 0.0,
                            right: 0.0,
                            child: container.state.unreadGuidanceReqCount > 0
                                ? Stack(children: <Widget>[
                                    Icon(Icons.brightness_1,
                                        size: 8.0, color: Colors.redAccent),
//                                Positioned(
//                                    top: 0.0,
//                                    right: 0.0,
//                                    child: Text(container
//                                        .state.unreadGuidanceReqCount
//                                        .toString()))
                                  ])
                                : Container(),
                          )
                        ]),
                        text: "Counselling Request",
                      )
                    : Tab(
                        icon: Stack(children: <Widget>[
                          new Icon(Icons.assignment),
                        ]),
                        text: "My Admin Messages",
                      )
                : Tab(
                    icon: Stack(children: <Widget>[
                      new Icon(Icons.assignment),
                    ]),
                    text: "My Admin Messages",
                  ),
          ],
        ),
        title: Text('Notifications Dashboard'),
      ),
      body: content,
    );
  }

  _launchURL(String url) async {
    try {
      await launch(url);
    } catch (e) {
      showAlert(
          errorMsg: MemError(
              title: "Couldn't dial",
              body: "Some issue with phone number, please check"));
    }

//    if (await canLaunch(url)) {
//      await launch(url);
//    } else {
//      //throw 'Could not launch $url';
//      showError(
//          errorMsg: MemError(
//              title: "Couldn't dial",
//              body: "Some issue with phone number, please check"));
//    }
  }

  void showAlert({MemError errorMsg, confirmFn confirmFn, cancelFn cancelFn}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "OK",
      confirmFn: confirmFn,
      cancel: "Cancel",
      cancelFn: cancelFn,
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void showSnackMessage(BuildContext context, String message,
      {MaterialColor color = Colors.lightBlue, actionText: "OK"}) {
    debugPrint("Error:$message");
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: color,
      content: Text(message),
      duration: Duration(seconds: 4),
      action: SnackBarAction(
        label: actionText,
        onPressed: () {
          Scaffold.of(context).removeCurrentSnackBar();
          // Some code to undo the change!
        },
      ),
    ));
  }

  confirmAndDelete(MemUser member, UserBloc userBloc) {
    userBloc.userRepository.deleteSingleUser(member.toEntity());
  }

  void _handleTabSelection() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var container = AppStateContainer.of(context);

//    prefs.setInt(
//        "guidance" + "_last_open", DateTime.now().millisecondsSinceEpoch);
//    setState(() {
//      container.state.unreadGuidanceReqCount = 0;
//    });

    switch (_controller.index) {
      case 0:
        prefs.setInt(
            "new_user" + "_last_open", DateTime.now().millisecondsSinceEpoch);
        setState(() {
          container.state.unreadNewUserReqCount = 0;
        });
        break;
      case 1:
        prefs.setInt("rosary_limits" + "_last_open",
            DateTime.now().millisecondsSinceEpoch);
        setState(() {
          container.state.unreadRosaryLimitsCount = 0;
        });
        break;
      case 2:
        prefs.setInt(
            "guidance" + "_last_open", DateTime.now().millisecondsSinceEpoch);
        setState(() {
          container.state.unreadGuidanceReqCount = 0;
        });

        break;
      default:
        break;
    }
  }

//  unBlock(UserBloc userBloc, User member) {
//    setState(() {
//      userBloc.userRepository.unBlockUser(member.id);
//    });
//  }
}
