// Copyright 2017 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:url_launcher/url_launcher.dart';

import 'package:flutter/material.dart';
import 'package:utility/utility.dart';

import 'package:mem_blocs/mem_blocs.dart';
import 'package:package_info/package_info.dart';

class MemHelp extends StatefulWidget {
  MemHelp({Key key}) : super(key: MemKeys.helpScreen);
  @override
  _MemHelpState createState() => new _MemHelpState();
}

class _MemHelpState extends State<MemHelp> {
  String appName = "MEM";

  String version = "1.9.3";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = UserProvider.of(context);

    var buttonColor = Theme.of(context).primaryColorLight;
    var buttonTextStyle = Theme.of(context)
        .textTheme
        .headline3
        .copyWith(color: Theme.of(context).backgroundColor, fontSize: 16.0);

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        appName = packageInfo.appName;
        version = packageInfo.version;
      });
    });

    return Scaffold(
        appBar: AppBar(
            title: const Text('Contact Us'),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, 0),
                  end: Alignment(
                      1.0, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    Theme.of(context).primaryColorDark,
                    Theme.of(context).backgroundColor
                  ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
            )),
        body: Material(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 40.0, left: 24.0, right: 24.0),
            children: <Widget>[
              Text("About MEM",
                  style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.w800,
                  )),
              Container(
                  margin: EdgeInsets.only(top: 10.0, bottom: 20.0),
                  child: Center(
                    child: Text(
                        """Marian Eucharistic Ministry(MEM) is a global network of faithful, interceding for the catholic church and the whole world."""),
                  )),
              Text("App Info",
                  style:
                      TextStyle(fontSize: 17.0, fontWeight: FontWeight.w800)),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 20.0),
                child: Text(appName + "-Version:" + version),
              ),
              Text("Initiative Of:",
                  style:
                      TextStyle(fontSize: 17.0, fontWeight: FontWeight.w800)),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 20.0),
                color: buttonColor,
                child: MaterialButton(
                  color: buttonColor,
                  onPressed: () {
                    _launchURL("http://marianeucharisticministry.org");
                  },
                  child: Text('MARIAN EUCHARISTIC MINISTRY (MEM)',
                      style: buttonTextStyle),
                ),
              ),
              Text("Developed By:",
                  style:
                      TextStyle(fontSize: 17.0, fontWeight: FontWeight.w800)),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 20.0),
                child: MaterialButton(
                  color: buttonColor,
                  onPressed: () {
                    _launchURL("http://missiorex.com");
                  },
                  child: Text(
                    'MISSIOREX TECHNOLOGY SOLUTIONS LLP.',
                    style: buttonTextStyle,
                  ),
                ),
              ),
              Text("Contact Us",
                  style:
                      TextStyle(fontSize: 17.0, fontWeight: FontWeight.w800)),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: MaterialButton(
                  color: buttonColor,
                  onPressed: () {
                    _launchURL("http://marianeucharisticministry.org");
                  },
                  child: Text('marianeucharisticministry@gmail.com',
                      style: buttonTextStyle),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 0.0, bottom: 20.0),
                child: MaterialButton(
                  color: buttonColor,
                  onPressed: () {
                    _launchURL("http://marianeucharisticministry.org");
                  },
                  child: Text("Phone : +64226352376", style: buttonTextStyle),
                ),
              ),
            ],
          ),
        ));
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
