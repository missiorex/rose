import 'dart:async';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memapp/widgets/reactive_refresh_indicator.dart';
import 'package:utility/utility.dart';
import 'package:memapp/screens/splash_screen.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:mem_models/mem_models.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;
import 'package:memapp/app_state_container.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memapp/screens/user_active.dart';

enum AuthStatus {
  INIT,
  CODE_SENT,
  VALIDATION_SUCCESS,
  VERIFICATION_FAILED,
  AUTOVERIFICATION_FAILED,
  VERIFICATION_COMPLETE,
  USER_CREATED,
  USER_ACTIVATED,
}

class AuthScreen extends StatefulWidget {
  final AppState appState;
  final FirebaseStorage storage;

  AuthScreen({Key key, @required this.appState, @required this.storage})
      : super(key: MemKeys.authScreen);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  static const String TAG = "SIGNUP";
  AuthStatus status = AuthStatus.INIT;
  MemUser userDetails = MemUser();
  User firebaseUser;
  var container;

  // Keys
  final GlobalKey<ScaffoldState> _authScaffoldKey = GlobalKey<ScaffoldState>();
//  final GlobalKey<MaskedTextFieldState> _maskedPhoneKey =
//      GlobalKey<MaskedTextFieldState>();

  // Controllers
  TextEditingController smsCodeController = TextEditingController();
  //TextEditingController phoneNumberController = TextEditingController();
  final phoneController = TextEditingController();
  final nameController = TextEditingController();
  // Variables
  String _errorMessage;
  int mResendToken;
  String _verificationId;
  Timer _codeTimer;
  final _phoneAuthFormKey = GlobalKey<FormState>();

  bool _isRefreshing = false;
  bool _codeTimedOut = false;
  bool _codeVerified = false;
  Duration _timeOut = const Duration(minutes: 1);
  var _duration = new Duration(seconds: 1);

  List<String> locales;
  List<String> _titles;
  String selectedLocale;
  String selectedTitle;

  // Firebase
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  //Streams
  StreamSubscription<QuerySnapshot> userActiveStreamSub;

  @override
  void initState() {
    super.initState();

    userActiveStreamSub = FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('notifications')
        .doc('user_active')
        .collection('messages')
        .orderBy('time')
        .snapshots()
        .listen(onUserActive);
  }

  // PhoneVerificationCompleted
  // This callback will be invoked in two situations:
  // 1 - Instant verification. In some cases the phone number can be instantly
  //     verified without needing to send or enter a verification code.
  // 2 - Auto-retrieval. On some devices Google Play services can automatically
  //     detect the incoming verification SMS and perform verification without
  //     user action.
  verificationCompleted(AuthCredential credential) async {
    //Logger.log(TAG, message: "onVerificationCompleted, user: $user");
    await _auth.signInWithCredential(credential).then((credential) async {
      if (await _onCodeVerified(credential.user)) {
        //await _finishSignIn(user);
        setState(() {
          firebaseUser = credential.user;
          AppStateContainer.of(context).state.user = credential.user;

          if (AppStateContainer.of(context).state.userDetails != null) {
            AppStateContainer.of(context).state.userDetails.status
                ? this.status = AuthStatus.USER_ACTIVATED
                : this.status = AuthStatus.USER_CREATED;
          } else {
            this.status = AuthStatus.VERIFICATION_COMPLETE;
          }
          _updateRefreshing(true);
        });
      } else {
        setState(() {
          this.status = AuthStatus.CODE_SENT;
          Logger.log(TAG, message: "Changed status to $status");
        });
      }
    });
  }

  Future<void> _linkWithPhoneNumber(AuthCredential credential) async {
    final errorMessage = "We couldn't verify your code, please try again!";

    await firebaseUser.linkWithCredential(credential).catchError((error) {
      print("Failed to verify SMS code: $error");
      _showErrorSnackbar(errorMessage);
    });

    Logger.log(TAG, message: "onVerificationCompleted, user: $firebaseUser");

    if (await _onCodeVerified(firebaseUser)) {
//    await _onCodeVerified(firebaseUser).then((codeVerified) async {
      setState(() {
        AppStateContainer.of(context).state.user = firebaseUser;

        if (AppStateContainer.of(context).state.userDetails != null) {
          AppStateContainer.of(context).state.userDetails.status
              ? this.status = AuthStatus.USER_ACTIVATED
              : this.status = AuthStatus.USER_CREATED;
        } else {
          this.status = AuthStatus.VERIFICATION_COMPLETE;
        }
        _updateRefreshing(true);
      });
    } else {
      setState(() {
        this.status = AuthStatus.CODE_SENT;
        Logger.log(TAG, message: "Changed status to $status");
      });
    }
  }

  // PhoneVerificationFailed
  // This callback is invoked in an invalid request for verification is made,
  // for instance if the the phone number format is not valid.
  verificationFailed(FirebaseAuthException authException) {
    _showErrorSnackbar(
        "We couldn't verify your code for now, please try again!");
    setState(() {
      this.status = AuthStatus.VERIFICATION_FAILED;
      Logger.log(TAG,
          message:
              'onVerificationFailed, code: ${authException.code}, message: ${authException.message}, Please try again later');

      showError(
          errorMsg: MemError(
              title: "Verification Failed: ${authException.code}",
              body: '${authException.message}, Please try again later'));
    });
  }

  // PhoneCodeSent
  // The SMS verification code has been sent to the provided phone number, we
  // now need to ask the user to enter the code and then construct a credential
  // by combining the code with a verification ID.
  codeSent(String verificationId, [int forceResendingToken]) async {
    Logger.log(TAG,
        message: "Verification code sent to number ${phoneController.text}");
    _codeTimer = Timer(_timeOut, () {
      setState(() {
        _codeTimedOut = true;
      });
    });
    _updateRefreshing(false);
    setState(() {
      this._verificationId = verificationId;
      this.mResendToken = forceResendingToken;
      this.status = AuthStatus.CODE_SENT;
      Logger.log(TAG, message: "Changed status to $status");
    });
  }

  // PhoneCodeAutoRetrievalTimeout
  codeAutoRetrievalTimeout(String verificationId) {
    Logger.log(TAG, message: "onCodeTimeout");
    _updateRefreshing(true);
    setState(() {
      this._verificationId = verificationId;
      this._codeTimedOut = true;
      this.status = AuthStatus.AUTOVERIFICATION_FAILED;
      Logger.log(TAG, message: "Changed status to $status");
    });
  }

  // Styling

  final decorationStyle = TextStyle(color: Colors.grey[50], fontSize: 16.0);
  final titleStyle = TextStyle(color: Colors.white, fontSize: 12.0);
  final hintStyle = TextStyle(color: Colors.white24);
  final welcomeStyle = TextStyle(color: Colors.brown, fontSize: 18.0);

  //

  @override
  void dispose() {
    _codeTimer?.cancel();
    nameController.dispose();
    phoneController.dispose();
    userActiveStreamSub.cancel();
    super.dispose();
  }

  Future<Null> _updateRefreshing(bool isRefreshing) async {
    Logger.log(TAG,
        message: "Setting _isRefreshing ($_isRefreshing) to $isRefreshing");
    if (_isRefreshing) {
      if (mounted)
        setState(() {
          this._isRefreshing = false;
        });
    }
    if (mounted)
      setState(() {
        this._isRefreshing = isRefreshing;
      });
  }

  _showErrorSnackbar(String message) {
    _updateRefreshing(false);
    _authScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(message)),
    );
  }

  Future<Null> _submitPhoneNumber() async {
    _updateRefreshing(true);

    final result = await _verifyPhoneNumber();
    Logger.log(TAG, message: "Returning $result from _submitPhoneNumber");
    return result;
  }

//  String get phoneNumber {
//    String unmaskedText = _maskedPhoneKey.currentState.unmaskedText;
//    String formatted = "+55$unmaskedText".trim();
//    return formatted;
//  }

  Future<Null> _verifyPhoneNumber() async {
    debugPrint("Phone Number: " +
        phoneController.text +
        "Code: " +
        userDetails.countryCode);
    Logger.log(TAG,
        message:
            "Got phone number as: ${userDetails.countryCode + phoneController.text}");
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: userDetails.countryCode + phoneController.text,
          timeout: _timeOut,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed);
    } catch (e) {
      Logger.log(TAG, message: e);
    }
    Logger.log(TAG, message: "Returning null from _verifyPhoneNumber");
    return null;
  }

  Future<Null> _submitSmsCode() async {
    final error = _smsInputValidator();
    if (error != null) {
      _updateRefreshing(false);
      _showErrorSnackbar(error);
      return null;
    } else {
      if (this._codeVerified) {
        //await _finishSignIn(await _auth.currentUser());

        if (_auth.currentUser != null) {
          this.firebaseUser = _auth.currentUser;
          _updateRefreshing(true);
          setState(() {
            AppStateContainer.of(context).state.user = this.firebaseUser;
            this.status = AuthStatus.VERIFICATION_COMPLETE;
            if (this.container.state.userDetails != null) {
              AppStateContainer.of(context).state.userDetails.status
                  ? this.status = AuthStatus.USER_ACTIVATED
                  : this.status = AuthStatus.USER_CREATED;
            }
          });
        }
      } else {
        Logger.log(TAG, message: "_signInWithPhoneNumber called");
        await _signInWithPhoneNumber();
//        await _linkWithPhoneNumber(
//          PhoneAuthProvider.getCredential(
//            smsCode: smsCodeController.text,
//            verificationId: _verificationId,
//          ),
//        );
      }
      return null;
    }
  }

  Future<Null> _checkUserExists() async {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    Logger.log(TAG,
        message:
            'from _checkUserStatus, container.state.user: ${container.state.user}, container.state.userDetails: ${container.state.userDetails}');
    _updateRefreshing(true);

    if (container.state.user != null) {
      Logger.log(TAG, message: "container.state.user != null");

      userBloc.userRepository
          .userExists(container.state.user.uid)
          .then((userExists) async {
        if (userExists) {
          userBloc.userProfile(container.state.user.uid).then((userProfile) {
            container.state.userDetails = userProfile;

            if (container.state.userDetails.status != null &&
                container.state.userDetails.status == true) {
              Logger.log(TAG, message: "user is active");
              setState(() {
                this.status = AuthStatus.USER_ACTIVATED;
                _isRefreshing = true;
              });
            } else {
              setState(() {
                this.status = AuthStatus.USER_CREATED;
                _isRefreshing = true;
              });
              Logger.log(TAG, message: "user not active");
            }
          });

          Logger.log(TAG, message: "user exists");
        } else {
          Logger.log(TAG, message: "user doesn't exist");
        }
      });
    } else {
      Logger.log(TAG, message: "container.state.user == null");
    }
    return null;
  }

  Future<Null> _checkUserActive() async {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    Logger.log(TAG,
        message:
            'from _checkUserStatus, container.state.user: ${container.state.user}, container.state.userDetails: ${container.state.userDetails}');

    if (container.state.user != null) {
      Logger.log(TAG, message: "container.state.user != null");

      userBloc.userRepository
          .userExists(container.state.user.uid)
          .then((userExists) async {
        if (userExists) {
          Logger.log(TAG, message: "user exists");

          userBloc.userProfile(container.state.user.uid).then((userProfile) {
            container.state.userDetails = userProfile;

            if (container.state.userDetails.status != null &&
                container.state.userDetails.status == true) {
              Logger.log(TAG, message: "user is active");
              setState(() {
                this.status = AuthStatus.USER_ACTIVATED;
              });
            } else {
              Logger.log(TAG, message: "user not active");
            }
          });
        } else {
          Logger.log(TAG, message: "user doesn't exist");
        }
      });
    } else {
      Logger.log(TAG, message: "container.state.user == null");
    }
    return null;
  }

  Future<void> _signInWithPhoneNumber() async {
    final errorMessage = "We couldn't verify your code, please try again!";

    //Breaking changes Auth Package
    final AuthCredential credential = PhoneAuthProvider.credential(
      verificationId: _verificationId,
      smsCode: smsCodeController.text,
    );

    await _auth.signInWithCredential(credential).then((userCredential) async {
//    await _auth
//        .signInWithPhoneNumber(
//            verificationId: _verificationId, smsCode: smsCodeController.text)
//        .then((user) async {
      await _onCodeVerified(userCredential.user).then((codeVerified) async {
        this._codeVerified = codeVerified;

        Logger.log(
          TAG,
          message: "Returning ${this._codeVerified} from _onCodeVerified",
        );
        if (this._codeVerified) {
          setState(() {
            this.firebaseUser = userCredential.user;
            AppStateContainer.of(context).state.user = userCredential.user;

            if (AppStateContainer.of(context).state.userDetails != null) {
              AppStateContainer.of(context).state.userDetails.status
                  ? this.status = AuthStatus.USER_ACTIVATED
                  : this.status = AuthStatus.USER_CREATED;
            } else {
              this.status = AuthStatus.VERIFICATION_COMPLETE;
            }
          });

          //await _finishSignIn(user);
        } else {
          _showErrorSnackbar(errorMessage);
        }
      });
    }, onError: (error) {
      debugPrint("Failed to verify SMS code: $error");
      _showErrorSnackbar(errorMessage);
    });
  }

  Future<bool> _onCodeVerified(User user) async {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    final isUserValid = (user != null &&
        (user.phoneNumber != null && user.phoneNumber.isNotEmpty));
    if (isUserValid) {
      Logger.log(
        TAG,
        message: "From onCodeVerified: Verification Success",
      );

      // Here we change the status once more to guarantee that the SMS's
      // text input isn't available while you do any other request
      // with the gathered data
      this.firebaseUser = user;
      container.state.user = user;

      await userBloc.userRepository
          .userExists(user.uid)
          .then((userExists) async {
        if (userExists) {
          Logger.log(TAG, message: "user exists");

          await userBloc
              .userProfile(container.state.user.uid)
              .then((userProfile) async {
            container.state.userDetails = userProfile;
            setState(() {
              if (this.container.state.userDetails != null) {
                this.container.state.userDetails.status
                    ? this.status = AuthStatus.USER_ACTIVATED
                    : this.status = AuthStatus.USER_CREATED;
              } else {
                this.status = AuthStatus.VERIFICATION_COMPLETE;
              }
              _updateRefreshing(true);
            });
          });
        } else {
          Logger.log(TAG, message: "user doesn't exist");
        }
      }).catchError((onError) {
        this.status = AuthStatus.VERIFICATION_FAILED;
      });
    } else {
      _showErrorSnackbar("We couldn't verify your code, please try again!");
    }
    return isUserValid;
  }

  _finishSignIn(User user) async {
    var container = AppStateContainer.of(context);
    userDetails.id = user.uid;

    await _onCodeVerified(user).then((result) {
      if (result) {
        // Here, instead of navigating to another screen, you should do whatever you want
        // as the user is already verified with Firebase from
        // phone number methods
        // Example: authenticate with your own API, use the data gathered
        // to post your profile/user, etc.
        UserBloc bloc = UserProvider.of(context);

        try {
          //Save the profile details and complete registration.
          bloc.pSignUp(userDetails, user).then((user) {
            //Once details saved give a confirmation and inform regarding account activation.
            _confirmRegistration();

            setState(() {
              container.state.user = user;
              if (container.state.userDetails != null) {
                AppStateContainer.of(context).state.userDetails.status
                    ? this.status = AuthStatus.USER_ACTIVATED
                    : this.status = AuthStatus.USER_CREATED;
              } else {
                this.status = AuthStatus.VERIFICATION_COMPLETE;
              }
              _updateRefreshing(true);
            });

            //Get shared Preferences
//            SharedPreferences.getInstance().then((prefs) {
//              prefs.setString('signupstate', "signin");
//            });
          });
        } catch (e) {
          debugPrint("error message : " + e.toString());
          showError(
              errorMsg: MemError(
                  title: "Error, Please Try again", body: e.toString()));
        }
      } else {
        setState(() {
          this.status = AuthStatus.VERIFICATION_FAILED;
        });
        _showErrorSnackbar(
            "We couldn't create your profile for now, please try again later");
      }
    });
  }

  Future<User> signInAnon() async {
    UserCredential userCredential = await _auth.signInAnonymously();
    print("Signed in ${userCredential.user.uid}");
    return userCredential.user;
  }

  // Widgets

  Widget _buildConfirmInputButton() {
    double deviceWidth = MediaQuery.of(context).size.width;

    return FloatingActionButton.extended(
        backgroundColor: Theme.of(context).primaryColor,
        label:
            Icon(Icons.arrow_forward, color: Theme.of(context).backgroundColor),
        onPressed: () => _updateRefreshing(true));
  }

  Widget _buildConfirmPhoneNumberButton() {
    double deviceWidth = MediaQuery.of(context).size.width;
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    return FloatingActionButton.extended(
        backgroundColor: Theme.of(context).primaryColor,
        label:
            Icon(Icons.arrow_forward, color: Theme.of(context).backgroundColor),
//        label: Text(
//          "Verify",
//          style: TextStyle(color: Theme.of(context).backgroundColor),
//        ),
        onPressed: () {
          if (_phoneAuthFormKey.currentState.validate()) {
            setState(() {
              this.status = AuthStatus.VALIDATION_SUCCESS;
              _updateRefreshing(true);
            });
          } else {
            setState(() {
              _errorMessage =
                  "The number is not valid, please check and try again";
            });
          }
        });

//      Stack(
//      children: <Widget>[
////        Align(
////            alignment: Alignment.center,
////            child: Container(
////                decoration: new BoxDecoration(
////                  color: Color.fromRGBO(68, 68, 68, 1.0),
////                  borderRadius: new BorderRadius.all(Radius.circular(
////                      isTablet ? deviceWidth * 0.08 : deviceWidth * 0.12)),
////                ),
////                height: deviceWidth * 0.08,
////                width: deviceWidth * 0.08)),
//        Align(
//            alignment: Alignment.center,
//            child: FloatingActionButton(
//                backgroundColor: Color.fromRGBO(68, 68, 68, 1.0),
//                child: Icon(Icons.arrow_forward),
//                onPressed: () {
//                  if (_phoneAuthFormKey.currentState.validate()) {
//                    setState(() {
//                      this.status = AuthStatus.VALIDATION_SUCCESS;
//                      _updateRefreshing(true);
//                    });
//                  } else {
//                    setState(() {
//                      _errorMessage =
//                          "The number is not valid, please check and try again";
//                    });
//                  }
//                })),
////        Positioned(
////            top: isTablet ? deviceWidth * 0.01 : deviceWidth * 0.08,
////            left: isTablet ? deviceWidth * 0.42 : deviceWidth * 0.08,
////            child: IconButton(
////                icon: Icon(Icons.arrow_forward,
////                    size: isTablet ? deviceWidth * 0.08 : deviceWidth * 0.08),
////                color: Colors.white,
////                disabledColor: Colors.grey,
////                onPressed: () {
////                  if (_phoneAuthFormKey.currentState.validate()) {
////                    setState(() {
////                      this.status = AuthStatus.VALIDATION_SUCCESS;
////                      _updateRefreshing(true);
////                    });
////                  } else {
////                    setState(() {
////                      _errorMessage =
////                          "The number is not valid, please check and try again";
////                    });
////                  }
////                }))
//      ],
//    );
  }

  Widget countryCodeField() {
    return CountryCodePicker(
      padding: EdgeInsets.only(top: 3.0, bottom: 3.0, right: 3.0),

      onChanged: (selectedItem) {
        userDetails.country = selectedItem.country;
        userDetails.countryCode = selectedItem.phoneCode;
        userDetails.region = selectedItem.region;

        ///todo add country code also to user details.
        debugPrint(selectedItem.country);
        debugPrint(selectedItem.phoneCode);
      },

      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
      initialSelection: 'Select',
      favorite: ['+91', 'IN', 'NZ', 'US'],
    );
  }

  Widget phoneField() {
    return Form(
        key: _phoneAuthFormKey,
        child: Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      height: 45.0,
                      padding: EdgeInsets.all(4.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(3.0))),
                      child: Row(children: <Widget>[countryCodeField()])),
                  Expanded(
                      flex: 1,
                      child: Container(
                          height: 45.0,
                          margin: EdgeInsets.only(left: 12.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(3.0))),
                          child: TextFormField(
                            controller: phoneController,
                            keyboardType: TextInputType.phone,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18.0,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              //errorText: _errorMessage,
                              hintText: 'Phone Number',
//                            focusedBorder: OutlineInputBorder(
//                                borderRadius:
//                                    BorderRadius.all(Radius.circular(0.0)),
//                                borderSide: BorderSide(
//                                    color: Color.fromRGBO(217, 178, 80, 1.0))),
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.all(12.0),
                            ),
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(30)
                            ],
                            validator: (val) {
                              if (val.isEmpty)
                                return "Please enter a valid phone number";
                              if (!isValidPhoneNumber(val)) return "Not Valid";
                              if (isWithCountryCode(val))
                                return "Exclude Country Code";
                            },
                            onFieldSubmitted: (val) {},
                            onSaved: (val) => setState(() {
                              userDetails.phoneNumber = val;
                            }),
                          ))),
                ])));
  }

  Widget nameField() {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(3.0)),
        child: TextFormField(
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 18.0,
            color: Colors.black,
          ),
          decoration: InputDecoration(
            hintText: 'Name',
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,

//            focusedBorder: OutlineInputBorder(
//              borderRadius: BorderRadius.all(Radius.circular(4.0)),
////                borderSide:
////                    BorderSide(color: Color.fromRGBO(217, 178, 80, 1.0))
//            ),
            contentPadding: EdgeInsets.all(12.0),
            fillColor: Colors.white,
          ),
          controller: nameController,
          inputFormatters: [LengthLimitingTextInputFormatter(30)],
          validator: (val) => val.isEmpty ? 'Name is required' : null,
          onSaved: (val) => setState(() {
            userDetails.name = val;
          }),
        ));
  }

  Widget titleField(UserBloc bloc) {
    return StreamBuilder(
      stream: bloc.title,
      builder: (context, snapshot) {
        return Container(
            margin: EdgeInsets.only(top: 20.0, bottom: 10.0),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(3.0)),
            child: Row(children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(
                        left: 10.0,
                        right: 20.0,
                      ),
//                      decoration: BoxDecoration(
//                          border: Border(bottom: BorderSide(width: 1.0))),
                      child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                              alignedDropdown: false,
                              child: DropdownButton<String>(
                                  hint: Text("Title"),
                                  value: selectedTitle,
                                  onChanged: (String changedValue) {
                                    bloc.changeTitle(changedValue);
                                    setState(() {
                                      selectedTitle = changedValue;
                                    });
                                  },
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18.0,
                                    color: Colors.black,
                                  ),
                                  items: _titles.map((String value) {
                                    return DropdownMenuItem<String>(
                                        value: value, child: Text(value));
                                  }).toList())))))
            ]));
      },
    );
  }

  Widget localeField(UserBloc bloc) {
//    List<String> languages = this.locales.where((i) => i != "Send to All").toList();
    this.locales = container.state.locales ?? <String>['English', 'Malayalam'];
    debugPrint("locales:  " + locales.toString());

    return StreamBuilder(
      stream: bloc.locale,
      builder: (context, snapshot) {
        return Container(
            margin: EdgeInsets.only(top: 10.0),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(3.0)),
            child: Row(children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(
                        left: 10.0,
                        right: 20.0,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                              alignedDropdown: false,
                              child: DropdownButton<String>(
                                  hint: Text("Language"),
                                  value: selectedLocale,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18.0,
                                    color: Colors.black,
                                  ),
                                  onChanged: (String changedValue) {
                                    bloc.changeLocale(changedValue);
                                    setState(() {
                                      selectedLocale = changedValue;
                                    });
                                  },
                                  items: locales.map((String value) {
                                    return DropdownMenuItem<String>(
                                        value: value, child: Text(value));
                                  }).toList())))))
            ]));
      },
    );
  }

  Widget signUpButton(UserBloc bloc) {
    final container = AppStateContainer.of(context);

    userDetails.name = nameController.text;
    userDetails.phoneNumber = phoneController.text;

    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      userDetails.pushNotificationToken = token;
      debugPrint("Push Notification Token from Signup" + token);
//      bloc.userRepository
//          .setPushNotificationToken(token, bloc.userDetails.id);
    });

    return StreamBuilder(
      stream: bloc.submitValidPSignup,
      builder: (context, snapshot) {
        return Container(
            margin: EdgeInsets.only(top: 10.0),
            padding: EdgeInsets.only(
                top: 0.0, bottom: 10.0, left: 10.0, right: 10.0),
            child: FloatingActionButton.extended(
              backgroundColor: Theme.of(context).primaryColor,
              label: Text("Submit",
                  style: TextStyle(color: Theme.of(context).backgroundColor)),
              splashColor: Colors.deepOrange,
              onPressed: () async {
                ///todo get other details to submit.

                debugPrint(
                    "Submission is valid ? : " + snapshot.hasData.toString());

                bool _isActive;
                if (snapshot.hasData) {
                  _showErrorSnackbar(
                      "Thanks for Signing up, we are verifying the details entered,Please wait for a notification from us");

                  debugPrint("User Details from phone sign up: " +
                      userDetails.toString());

                  //Subscribe by default to Five Continents Prayer & Locale for locale based notifications.
                  _firebaseMessaging.subscribeToTopic("five");
                  _firebaseMessaging
                      .subscribeToTopic(userDetails.locale ?? "English");

                  setState(() {
                    container.state.isLoading = false;
                    container.state.user = firebaseUser;
                  });

                  _finishSignIn(firebaseUser);
                } else {
                  showError(
                      errorMsg: MemError(
                          title: "Please Enter the details to Sign Up",
                          body: "Please Enter the details to Sign Up"));
                }

                ///todo: the appropriate implementation needs to be introduced. Its a quick fix now.
              },
            ));
      },
    );
  }

  Future guestLogin() async {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);

    User user = await signInAnon();

    MemUser _user = MemUser(
        id: user.uid,
        title: "",
        name: "Guest",
        displayName: "Guest",
        status: true,
        locale: "English",
        role: "guest",
        isSuperAdmin: false,
        pushNotificationToken: "",
        isBlocked: false);

    container.state.user = user;

    container.state.userDetails = _user;
    userBloc.userDetails = _user;
  }

//  Widget _buildPhoneNumberInput() {
//    return MaskedTextField(
//      key: _maskedPhoneKey,
//      mask: "(xx) xxxxx-xxxx",
//      keyboardType: TextInputType.number,
//      maskedTextFieldController: phoneNumberController,
//      maxLength: 15,
//      onSubmitted: (text) => _updateRefreshing(true),
//      style: Theme.of(context)
//          .textTheme
//          .subhead
//          .copyWith(fontSize: 18.0, color: Colors.white),
//      inputDecoration: InputDecoration(
//        isDense: false,
//        enabled: this.status == AuthStatus.COLLECT_PHONE_NUMBER,
//        counterText: "",
//        icon: const Icon(
//          Icons.phone,
//          color: Colors.white,
//        ),
//        labelText: "Phone",
//        labelStyle: decorationStyle,
//        hintText: "(99) 99999-9999",
//        hintStyle: hintStyle,
//        errorText: _errorMessage,
//      ),
//    );
//  }

  Widget _buildPhoneAuthBody() {
    bool isIOS = Theme.of(context).platform == TargetPlatform.iOS;
    double deviceWidth = MediaQuery.of(context).size.width;
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
//          margin: EdgeInsets.only(top: deviceWidth * 0.05),
          padding: EdgeInsets.symmetric(
              vertical: deviceWidth * 0.01, horizontal: deviceWidth * 0.10),
          child: Text(
            "Verify Your Phone Number",
            style: TextStyle(
                fontSize: isTablet ? 24.0 : 17.0,
                color: Colors.white,
                fontWeight: FontWeight.w600),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(
              vertical: deviceWidth * 0.02, horizontal: deviceWidth * 0.05),
          child: Text(
            "Please enter your country code and phone number,\n We will send an SMS with verification code.",
            style: TextStyle(
                fontFamily: "Roboto",
                fontSize: isTablet ? 15.0 : 12.0,
                color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: 8.0, horizontal: isTablet ? deviceWidth * 0.2 : 24.0),
          child: Flex(
            direction: Axis.horizontal,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(flex: 5, child: phoneField()),
            ],
          ),
        ),
        _buildConfirmPhoneNumberButton(),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
          child: Text(
            _errorMessage ?? "",
            style: decorationStyle,
            textAlign: TextAlign.center,
          ),
        ),

//        isIOS
//            ?
        Row(
          children: <Widget>[
            Expanded(
                flex: 3,
                child: Container(
                    child: FlatButton.icon(
                        color: Colors.transparent,
                        icon: Icon(
                          Icons.person,
                          color: Colors.white,
                        ), //`Icon` to display
                        label: Text('Guest Login',
                            style: TextStyle(
                                color: Colors.white)), //`Text` to display
                        onPressed: () async {
                          await guestLogin();
                          Timer(_duration, () => navigationPage(context));
                          //Navigator.of(context).pushReplacementNamed('/home');
                        }))),
          ],
        )
        // : Container(),
//        Padding(
//          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
//          child: Flex(
//            direction: Axis.horizontal,
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Flexible(flex: 5, child: guestLogin()),
//            ],
//          ),
//        ),
      ],
    );
  }

  Widget _buildSmsCodeInput() {
//    final enabled = this.status == AuthStatus.CODE_SENT;

    final enabled = true;

    return TextField(
      keyboardType: TextInputType.number,
      enabled: enabled,
      textAlign: TextAlign.center,
      controller: smsCodeController,
      maxLength: 6,
      onSubmitted: (text) => _updateRefreshing(true),
      style: Theme.of(context).textTheme.subtitle1.copyWith(
            fontSize: 32.0,
            color: enabled ? Colors.white : Theme.of(context).buttonColor,
          ),
      decoration: InputDecoration(
        border: OutlineInputBorder(borderSide: BorderSide.none),
        counterText: "",
        enabled: enabled,
        hintText: "--- ---",
        hintStyle: hintStyle.copyWith(
            fontSize: 42.0, color: Theme.of(context).primaryColorLight),
      ),
    );
  }

  Widget _buildResendSmsWidget() {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    return InkWell(
      onTap: () async {
        if (_codeTimedOut) {
          await _verifyPhoneNumber();
        } else {
          _showErrorSnackbar("You can't retry yet!");
        }
      },
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "If your code does not arrive in 1 minute, touch",
            style: TextStyle(
                fontSize: isTablet ? 15.0 : 12.0, color: Colors.white),
            children: <TextSpan>[
              TextSpan(
                text: " here",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSmsAuthBody() {
    var deviceWidth = MediaQuery.of(context).size.width;
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
//          margin: EdgeInsets.only(top: deviceWidth * 0.20),
          padding: EdgeInsets.symmetric(
              vertical: deviceWidth * 0.02, horizontal: deviceWidth * 0.10),
          child: Text(
            "Verification code",
            style: TextStyle(
                fontSize: isTablet ? 24.0 : 17.0,
                color: Colors.white,
                fontWeight: FontWeight.w600),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
            padding: EdgeInsets.symmetric(
                vertical: deviceWidth * 0.02, horizontal: deviceWidth * 0.05),
            child: _buildSmsCodeInput()
//    Flex(
//            direction: Axis.horizontal,
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Flexible(flex: 7, child: _buildSmsCodeInput()),
//              Flexible(flex: 1, child: _buildConfirmInputButton())
//            ],
//          ),
            ),
        _buildConfirmInputButton(),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
          child: _buildResendSmsWidget(),
        ),
//        Container(
//          margin: EdgeInsets.only(top: 15.0),
//          child: Text(
//            "email: marianeucharisticministry@gmail.com ",
//            textAlign: TextAlign.center,
//            style: TextStyle(
//                fontSize: 12.0, color: Color.fromRGBO(60, 59, 50, 1.0)),
//          ),
//        ),
//        Container(
//          margin: EdgeInsets.only(top: 5.0),
//          child: Text(
//            " Phone:  +64226352376",
//            textAlign: TextAlign.center,
//            style: TextStyle(
//                fontSize: 12.0, color: Color.fromRGBO(60, 59, 50, 1.0)),
//          ),
//        ),
      ],
    );
  }

//  Widget _signUpOrSignIn() {
//    return FutureBuilder<SharedPreferences>(
//      future: SharedPreferences.getInstance(),
//      builder:
//          (BuildContext context, AsyncSnapshot<SharedPreferences> snapshot) {
//        switch (snapshot.connectionState) {
//          case ConnectionState.none:
//          case ConnectionState.waiting:
//            return Container(
//                decoration: BoxDecoration(
//                  image: DecorationImage(

//                      fit: BoxFit.cover),
//                  color: Colors.transparent,
//                ),
//                child: Center(
//                    child: new CircularProgressIndicator(
//                  valueColor:
//                      new AlwaysStoppedAnimation<Color>(Colors.deepOrange),
//                )));
//          default:
//            if (!snapshot.hasError) {
//              return snapshot.data.getString("signupstate") == null
//                  ? _buildProfileBody()
//                  : _signInView();
//            } else {
//              return ErrorView(snapshot.error);
//            }
//        }
//      },
//    );
//  }

  Widget _navigateToHome(BuildContext context) {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);
    _updateRefreshing(true);
    Logger.log(TAG,
        message:
            'from signup or signin, container.state.user: ${container.state.user}, container.state.userDetails: ${container.state.userDetails}');

    if (container.state.user != null) {
      Logger.log(TAG, message: "container.state.user != null");

      userBloc.userRepository
          .userExists(container.state.user.uid)
          .then((userExists) async {
        if (userExists) {
          Logger.log(TAG, message: "user exists");

          userBloc.userProfile(container.state.user.uid).then((userProfile) {
            container.state.userDetails = userProfile;

            if (container.state.userDetails.status != null &&
                container.state.userDetails.status == true) {
              Logger.log(TAG, message: "user is active");
              return new Timer(_duration, () => navigationPage(context));
            } else {
              Logger.log(TAG, message: "user not active");
              return _userNotActiveView();
            }
          });
        } else {
          Logger.log(TAG, message: "user doesn't exist");
          return _buildProfileBody();
        }
      });
    } else {
      Logger.log(TAG, message: "container.state.user == null");
      return _buildProfileBody();
    }
  }

  Widget _signUpOrSignIn(BuildContext context) {
    var container = AppStateContainer.of(context);
    UserBloc userBloc = UserProvider.of(context);
    _updateRefreshing(true);
    Logger.log(TAG,
        message:
            'from signup or signin, container.state.user: ${container.state.user}, container.state.userDetails: ${container.state.userDetails}');

    if (container.state.user != null) {
      Logger.log(TAG, message: "container.state.user != null");

      userBloc.userRepository
          .userExists(container.state.user.uid)
          .then((userExists) async {
        if (userExists) {
          Logger.log(TAG, message: "user exists");

          userBloc.userProfile(container.state.user.uid).then((userProfile) {
            container.state.userDetails = userProfile;

            if (container.state.userDetails.status != null &&
                container.state.userDetails.status == true) {
              Logger.log(TAG, message: "user is active");
              return new Timer(_duration, () => navigationPage(context));
            } else {
              Logger.log(TAG, message: "user not active");
              return _userNotActiveView();
            }
          });
        } else {
          Logger.log(TAG, message: "user doesn't exist");
          return _buildProfileBody();
        }
      });
    } else {
      Logger.log(TAG, message: "container.state.user == null");
      return _buildProfileBody();
    }
  }

  Widget _userNotActiveView() {
    var deviceWidth = MediaQuery.of(context).size.width;

    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: deviceWidth * 0.30),
            padding: EdgeInsets.symmetric(
                vertical: deviceWidth * 0.02, horizontal: deviceWidth * 0.10),
            child: Text(
              "Your account is not active yet. We will notify you once its activated",
              style: TextStyle(
                  fontSize: 17.0,
                  color: Colors.white,
                  fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
          ),
        ]);
  }

//Navigate to Splash Screen after few seconds of delay
  Widget navigationPage(BuildContext context) {
    Navigator.of(context).pushReplacementNamed('/splash');
  }

  Widget _buildProfileBody() {
    var container = AppStateContainer.of(context);
    final userBloc = UserProvider.of(context);

    _titles = <String>[
      '',
      'Mr.',
      'Mrs.',
      'Miss',
      'Fr.',
      'Sr.',
      'Rev.',
      'Mar.',
      'Bishop.',
      'Arch Bishop.',
      'Cardinal.'
    ];

    var deviceWidth = MediaQuery.of(context).size.width;

    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: deviceWidth * 0.15),
            padding: EdgeInsets.symmetric(
                vertical: deviceWidth * 0.01, horizontal: deviceWidth * 0.10),
            child: Text(
              "Enter Your Profile",
              style: TextStyle(
                  fontSize: 17.0,
                  color: Theme.of(context).backgroundColor,
                  fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
                vertical: deviceWidth * 0.005, horizontal: deviceWidth * 0.06),
            child: titleField(userBloc),
          ),
          Container(
            padding: EdgeInsets.symmetric(
                vertical: deviceWidth * 0.005, horizontal: deviceWidth * 0.06),
            child: nameField(),
          ),
          Container(
            padding: EdgeInsets.symmetric(
                vertical: deviceWidth * 0.005, horizontal: deviceWidth * 0.06),
            child: localeField(userBloc),
          ),
          signUpButton(userBloc),
        ]);
  }

  String _phoneInputValidator() {
    if (phoneController.text.isEmpty) {
      return "Your phone number can't be empty!";
    } else if (isWithCountryCode(phoneController.text)) {
      return "Enter phone number without country code please";
    } else if (!isValidPhoneNumber(phoneController.text)) {
      return "This phone number is invalid!";
    }
    return null;
  }

  static bool isValidPhoneNumber(String input) {
    final RegExp regex = new RegExp(r'^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$');
    return regex.hasMatch(input);
  }

  static bool isWithCountryCode(String input) {
    final RegExp regex = new RegExp(r'[+]');
    return regex.hasMatch(input);
  }

  String _smsInputValidator() {
    if (smsCodeController.text.isEmpty) {
      return "Your verification code can't be empty!";
    } else if (smsCodeController.text.length < 6) {
      return "This verification code is invalid!";
    }
    return null;
  }

  void _confirmRegistration() {
    var dialog = DialogShower.buildDialog(
      title: "Thanks for Registration, Your account will be activated soon",
      message:
          "Thanks for your interest to join MEM. We will soon activate your account and you will be notified. Login again once your account is activated.",
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "OK",
      confirmFn: () {
        Navigator.pop(context);
//        Navigator.of(context).pushNamedAndRemoveUntil(
//            "/splash", (Route<dynamic> route) => false);
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  Widget _buildBody(BuildContext context) {
    Widget body;
    switch (this.status) {
      case AuthStatus.INIT:
        body = _buildPhoneAuthBody();
        break;
      case AuthStatus.VALIDATION_SUCCESS:
        break;
      case AuthStatus.CODE_SENT:
      case AuthStatus.AUTOVERIFICATION_FAILED:
        body = _buildSmsAuthBody();
        break;
      case AuthStatus.VERIFICATION_FAILED:
        body = _buildPhoneAuthBody();
        break;
      case AuthStatus.VERIFICATION_COMPLETE:
        _isRefreshing = false;
        body = _buildProfileBody();
        break;
      case AuthStatus.USER_CREATED:
        body = _userNotActiveView();
        break;
      case AuthStatus.USER_ACTIVATED:
        body = _navigateToHome(context);
        break;
    }
    return body;
  }

  Future<Null> _onRefresh() async {
    switch (this.status) {
      case AuthStatus.VERIFICATION_FAILED:
      case AuthStatus.INIT:
        Logger.log(TAG, message: 'Current Staus:{$status}');
        break;
      case AuthStatus.VALIDATION_SUCCESS:
        return await _submitPhoneNumber();
        break;
      case AuthStatus.CODE_SENT:
      case AuthStatus.AUTOVERIFICATION_FAILED:
        return await _submitSmsCode();
        break;
      case AuthStatus.USER_CREATED:
        return await _checkUserActive();
        break;
      case AuthStatus.VERIFICATION_COMPLETE:
        return await _checkUserExists();
        break;
      case AuthStatus.USER_ACTIVATED:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    this.container = AppStateContainer.of(context);
    var deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      key: _authScaffoldKey,
      body: ListView(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.3,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(0, -1.0),
                end: Alignment(
                    0.0, 1.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  this.status == AuthStatus.VERIFICATION_COMPLETE
                      ? Theme.of(context).backgroundColor
                      : Color(0xFF8e0904)
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
            child: Container(
              padding:
                  EdgeInsets.all(MediaQuery.of(context).size.height * 0.06),
              child: Image.asset(
                "assets/memapp_icon.png",
              ),
//              height: MediaQuery.of(context).size.width * 0.10,
//              width: MediaQuery.of(context).size.width * 0.10,
            ),
          ),
          ReactiveRefreshIndicator(
            displacement: deviceWidth * 0.35,
            onRefresh: _onRefresh,
            isRefreshing: _isRefreshing,
            child: Container(
                height: MediaQuery.of(context).size.height * 0.6,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment(0, -1.0),
                    end: Alignment(
                        0.0, 1.0), // 10% of the width, so there are ten blinds.
                    colors: this.status == AuthStatus.VERIFICATION_COMPLETE
                        ? [
                            Theme.of(context).primaryColorLight,
                            Theme.of(context).primaryColorLight,
                          ]
                        : [
                            Color(0xFF8e0904),
                            Color(0xFF5b0e04)
                          ], // whitish to gray
                    tileMode: TileMode
                        .repeated, // repeats the gradient over the canvas
                  ),
                ),
                //Color.fromRGBO(0, 162, 190, 1.0),
                child: _buildBody(context)),
          ),
          Container(
              width: deviceWidth,
              height: MediaQuery.of(context).size.height * 0.1,
              //color: Color.fromRGBO(0, 162, 190, 1.0),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(0, -1.0),
                  end: Alignment(
                      0.0, 1.0), // 10% of the width, so there are ten blinds.
                  colors: this.status == AuthStatus.VERIFICATION_COMPLETE
                      ? [
                          Theme.of(context).primaryColorLight,
                          Theme.of(context).primaryColorLight,
                        ]
                      : [
                          Color(0xFF5b0e04),
                          Theme.of(context).backgroundColor,
                        ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
              child: Container(
                //color: Color.fromRGBO(0, 142, 166, 0.7),

                padding: EdgeInsets.all(20.0),
                child: Center(
                    child: Text(
                  this.status == AuthStatus.VERIFICATION_COMPLETE
                      ? "Marian Eucharistic Ministry"
                      : "email: marianeucharisticministry@gmail.com \n Phone:  +64226352376",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: deviceWidth * 0.025,
                      color: this.status == AuthStatus.VERIFICATION_COMPLETE
                          ? Theme.of(context).backgroundColor
                          : Colors.white,
                      letterSpacing: 1.2),
                )),
              )),
        ],
      ),
    );
  }

  void onUserActive(QuerySnapshot snap) {
    try {
      debugPrint("user token: " + userDetails.pushNotificationToken);
      debugPrint("From onUserActive");

      if (snap.docs.length != 0 &&
          snap.docs.last["token"] == userDetails.pushNotificationToken) {
        debugPrint("From onUserActive, push notification matches");
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  UserActiveScreen(key: MemKeys.userActiveScreen),
            ));
      }
    } catch (e) {}
  }
}
