import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:utility/utility.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:memapp/app_state_container.dart';
import 'dart:io';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart' as p;
import 'package:mem_blocs/mem_blocs.dart';
import 'package:utility/src/dialogshower.dart' as DialogShower;

class AddBenefactorScreen extends StatefulWidget {
  AddBenefactorScreen({key: Key, this.storage})
      : super(key: MemKeys.addBenefactorScreen);

  final FirebaseStorage storage;

  @override
  _AddBenefactorScreenState createState() => new _AddBenefactorScreenState();
}

class _AddBenefactorScreenState extends State<AddBenefactorScreen> {
  File _image;
  Future<File> _imageFile;
  static final GlobalKey<FormState> addBenefactorFormKey =
      GlobalKey<FormState>();
  AppState appState;
  Benefactor benefactor = Benefactor();

  initState() {}

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final container = AppStateContainer.of(context);
    final MessageBloc messageBloc = MessageProvider.of(context);
    final UserBloc userBloc = UserProvider.of(context);
    BishopsMessage bishopsMessage;

    appState = container.state;

    return Scaffold(
      appBar: AppBar(
          title: Text("Add Benefactor Message"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, 0),
                end: Alignment(
                    1.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [
                  Theme.of(context).primaryColorDark,
                  Theme.of(context).backgroundColor
                ], // whitish to gray
                tileMode:
                    TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          )),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
            key: addBenefactorFormKey,
            autovalidate: false,
            onWillPop: () {
              return Future(() => true);
            },
            child: ListView(
              children: [
                benefactorTitleField(),
                benefactorNameField(),
                benefactorMessage(),
                _previewImage(),
                benefactorProfilePhotoField(
                    "Upload Benefactor's Profile Image"),
                AddBenefactorButton(messageBloc),
              ],
            )),
      ),
    );
  }

  Widget benefactorTitleField() {
    String _title = '';
    List<String> _titles = <String>[
      '',
      'Rev. Fr.',
      'Rev. Sr.',
      'Rev.',
      'Mar.',
      'Most Rev. Bishop.',
      'Most Rev. Arch Bishop.',
      'His Grace Most Rev.Dr.',
      'Most Rev. Cardinal.',
      'Moran Mor'
    ];

    return FormField<String>(
      builder: (FormFieldState<String> state) {
        return InputDecorator(
          decoration: InputDecoration(
            icon: const Icon(Icons.label),
            labelText: 'Title',
          ),
          isEmpty: _title == '',
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton<String>(
              isExpanded: true,
              value: _title,
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  _title = newValue;

                  benefactor.title = _title;
                  newValue == 'Rev. Fr.'
                      ? benefactor.isPriest = true
                      : benefactor.isPriest = false;
                  state.didChange(newValue);
                });
              },
              items: _titles.map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }

  Widget benefactorNameField() {
    return Container(
        decoration:
            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: const Icon(Icons.person),
            hintText: 'Name',
            labelText: 'Name',
            border: InputBorder.none,
          ),
          inputFormatters: [LengthLimitingTextInputFormatter(100)],
          validator: (val) => val.isEmpty ? 'Name is required' : null,
          onSaved: (val) => benefactor.name = val,
        ));
  }

  Widget benefactorMessage() {
    return Container(
        decoration:
            BoxDecoration(border: Border(bottom: BorderSide(width: 1.0))),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: const Icon(Icons.message),
            hintText: 'Message',
            labelText: 'Message',
            border: InputBorder.none,
          ),
          inputFormatters: [LengthLimitingTextInputFormatter(200)],
          onSaved: (val) => benefactor.message = val,
        ));
  }

  Widget benefactorProfilePhotoField(
    String text, {
    Color backgroundColor = Colors.blueGrey,
    Color textColor = Colors.white70,
  }) {
    UserBloc userBloc = UserProvider.of(context);
    File _profileImage;

    return ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: MaterialButton(
            child: Text(text),
            minWidth: 80.0,
            color: backgroundColor,
            textColor: textColor,
            onPressed: () {
              saveImageLocal(benefactor.id).then((image) {
                _profileImage = image;

                _uploadFile(image, benefactor.id).then((profilePhotoUrl) {
                  debugPrint("Benefactor Profile Photo Url :" +
                      profilePhotoUrl.toString());
                  benefactor.profilePhotoUrl = profilePhotoUrl.toString();

                  ///todo upload profile photo userBloc.userRepository.updateUser(user)
                });
              });
            }));
  }

  Future<String> _uploadFile(File _image, String uid) async {
    List<String> fileNameList = _image.path.split("/");

    String fileName = fileNameList[fileNameList.length - 1];

    final Reference ref =
        widget.storage.ref().child('benefactorProfile').child(fileName);
    final UploadTask uploadTask = ref.putFile(
      _image,
      SettableMetadata(
          customMetadata: <String, String>{'iamgetype': 'profile'},
          contentType: 'image/jpeg'),
    );

    // final Uri downloadUrl = (await uploadTask.future).downloadUrl;
    final String downloadUrl = await ref.getDownloadURL();

    return downloadUrl;
  }

  Future<File> saveImageLocal(String uid) async {
    var _imageFile;

    ImagePicker()
        .getImage(
            source: ImageSource.gallery, maxHeight: 400.0, maxWidth: 400.0)
        .then((pickedFile) {
      if (pickedFile != null) {
        _imageFile = File(pickedFile.path);
      }
    });

    var image = await _imageFile;
    // getting a directory path for saving
    final String path = await _localPath;

    ImageProperties properties =
        await FlutterNativeImage.getImageProperties(image.path);
    File compressedFile = await FlutterNativeImage.compressImage(image.path,
        quality: 80,
        targetWidth: 400,
        targetHeight: (properties.height * 400 / properties.width).round());

    File profilePic = await compressedFile.copy(p.join(path, uid + ".jpeg"));

    setState(() {
      _image = profilePic;
    });

    return _image;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Widget _previewImage() {
    return FutureBuilder<File>(
        future: _imageFile,
        builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            return Image.file(snapshot.data);
          } else if (snapshot.error != null) {
            return const Text(
              'Error picking image.',
              textAlign: TextAlign.center,
            );
          } else {
            return const Text(
              'You have not yet picked an image.',
              textAlign: TextAlign.center,
            );
          }
        });
  }

  Widget AddBenefactorButton(MessageBloc messageBloc) {
    return RaisedButton(
        color: Colors.lightBlue,
        splashColor: Colors.white70,
        child: Text("Add Benefactor", style: TextStyle(color: Colors.white)),
        onPressed: () {
          final FormState form = addBenefactorFormKey.currentState;

          if (!form.validate()) {
            debugPrint("Benefactor Collected: " + benefactor.toString());
            debugPrint('Some Details are missing or incorrect');
//            var dialog = DialogShower.buildDialog(
//                title: "Some Details are missing or incorrect",
//                message: """Please enter all the required details.""",
//                confirm: "OK",
//                confirmFn: () {
//                  Navigator.pop(context);
//                });
//
//            showDialog(
//                context: context,
//                builder: (context) {
//                  return dialog;
//                });
            showError(
                errorMsg: MemError(
                    title: "Some Details are missing or incorrect",
                    body: "Please correct the missing details"));
          } else {
            form.save(); //This invokes each onSaved event
            try {
              messageBloc.messageRepository
                  .addBenefactor(benefactor.toEntity());

              showError(
                  errorMsg: MemError(
                      title: "Benefactor Added. Please check",
                      body: "Benefactor Added"));
            } catch (e) {
              showError(
                  errorMsg: MemError(
                      title: "Couldnt Add the benefactor.",
                      body: e.toString()));
            }
          }
        });
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      cancel: "OK",
      cancelFn: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }
}
