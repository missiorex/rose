// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_models/mem_models.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;

class NotificationDetailScreen extends StatefulWidget {
  final FCMNotification message;
  final Function onDelete;

  NotificationDetailScreen({
    Key key,
    @required this.message,
    @required this.onDelete,
  }) : super(key: MemKeys.notificationDetailsScreen);

  @override
  State createState() => NotificationDetailsScreenState();
}

class NotificationDetailsScreenState extends State<NotificationDetailScreen> {
//  DetailScreen({
//    @required this.message,
//  }) : super(key: MemKeys.messageDetailsScreen);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-1.0, 0),
              end: Alignment(
                  1.0, 0.0), // 10% of the width, so there are ten blinds.
              colors: [
                Theme.of(context).primaryColorDark,
                Theme.of(context).backgroundColor
              ], // whitish to gray
              tileMode:
                  TileMode.repeated, // repeats the gradient over the canvas
            ),
          ),
        ),
        title: Text(
            MemLocalizations(Localizations.localeOf(context)).messageDetails),
        actions: [
          IconButton(
            key: MemKeys.deleteMessageButton,
            tooltip:
                MemLocalizations(Localizations.localeOf(context)).deleteMessage,
            icon: Icon(Icons.delete),
            onPressed: () {
              //_removeMessage(widget.message);
              widget.onDelete();
              Navigator.pop(context, widget.message);

//              showDialog(
//                  context: context,
//                  builder: (context) {
//                    return MyDialog(message: widget.message);
//                  });
//
//              Navigator.pop(context, widget.message);
            },
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: ListView(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 8.0),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          top: 8.0,
                          bottom: 16.0,
                        ),
                        child: Text(
                          widget.message.title,
                          key: MemKeys.detailsMessageItemTitle,
                          style: Theme.of(context).textTheme.headline,
                        ),
                      ),
                      Text(
                        widget.message.body,
                        key: MemKeys.detailsMessageItemBody,
                        style: Theme.of(context).textTheme.subhead,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
//      floatingActionButton: FloatingActionButton(
//        tooltip: MemLocalizations(Localizations.localeOf(context)).editMessage,
//        child: Icon(Icons.edit),
//        key: MemKeys.editMessageFab,
//        onPressed: () {
//          Navigator.of(context).push(
//            MaterialPageRoute(
//              builder: (context) {
//                return AddEditMessageScreen(
//                  key: MemKeys.editMessageScreen,
//                  message: widget.message,
//                );
//              },
//            ),
//          );
//        },
//      ),
    );
  }

  void _removeMessage(MEMmessage message) {
    final messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);

//    Scaffold.of(context).showSnackBar(
//          SnackBar(
//            key: MemKeys.snackbar,
//            duration: Duration(seconds: 2),
//            backgroundColor: Theme.of(context).backgroundColor,
//            content: Text(
//              MemLocalizations(Localizations.localeOf(context))
//                  .messageDeleted(message.title),
//              maxLines: 1,
//              overflow: TextOverflow.ellipsis,
//            ),
//            action: SnackBarAction(
//              label: MemLocalizations(Localizations.localeOf(context)).undo,
//              onPressed: () {
//                messageBloc.messageRepository.addRegionalMessage(
//                    message.toEntity(), container.state.userDetails.region);
//              },
//            ),
//          ),
//        );
  }
}

class MyDialog extends StatefulWidget {
  final MEMmessage message;

  MyDialog({
    Key key,
    @required this.message,
  }) : super(key: key);

  @override
  State createState() => new MyDialogState();
}

class MyDialogState extends State<MyDialog> {
  Widget build(BuildContext context) {
    final messageBloc = MessageProvider.of(context);
    var container = AppStateContainer.of(context);

    var dialog = DialogShower.buildDialog(
        title: "Delete message : " + widget.message.title,
        message: """

            You are about to delete this message. Sure ?
            """,
        confirm: "Yes",
        confirmFn: () {
          if (messageBloc.removeRegionalMessage(
              widget.message, container.state.userDetails)) {
          } else {}
          Navigator.pop(context);
        },
        cancel: "No",
        cancelFn: () {
          Navigator.pop(context);
        });
    return dialog;
  }
}
