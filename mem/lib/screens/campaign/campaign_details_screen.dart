import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:utility/utility.dart';
import 'package:memapp/blocs/blocs.dart';
import 'package:memapp/screens/campaign/screens.dart';
import 'package:memapp/widgets/campaign/widgets.dart';

class CampaignDetailsScreen extends StatelessWidget {
  final String id;
  final DismissDirectionCallback onDismissed;
  final GestureTapCallback onTap;
  final ValueChanged<bool> onCheckboxChanged;
  final GestureTapCallback onRosaryIncrement;
  static const routeName = '/campaign';

  CampaignDetailsScreen(
      {Key key,
      @required this.id,
      this.onDismissed,
      this.onRosaryIncrement,
      this.onCheckboxChanged,
      this.onTap})
      : super(key: key ?? MemKeys.campaignDetailsScreen);

  SliverPersistentHeader makeHeader() {
    return SliverPersistentHeader(
      pinned: true,
      delegate: SliverAppBarDelegate(
        minHeight: 200.0,
        maxHeight: 200.0,
        child: AdorationLive(),
      ),
    );
  }

//  SliverPersistentHeader campaignDetails() {
//    return SliverPersistentHeader(
//      pinned: true,
//      delegate: SliverAppBarDelegate(
//        minHeight: 60.0,
//        maxHeight: 600.0,
//        child: Container(
//            color: Colors.red,
//            child: Center(
//                child: Container(
//              child: Text(
//                "Hello",
//                key: MemKeys.detailsCampaignItemTitle,
//                style: TextStyle(color: Colors.white),
//              ),
//            ))),
//      ),
//    );
//  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CampaignsBloc, CampaignsState>(
      builder: (context, state) {
        final campaign = (state as CampaignsLoadSuccess)
            .campaigns
            .firstWhere((campaign) => campaign.id == id, orElse: () => null);
        final localizations = MemLocalizations.of(context);
        return Scaffold(
          appBar: AppBar(
            title: Text(localizations.campaignDetails),
            actions: [
              IconButton(
                tooltip: localizations.deleteCampaign,
                key: MemKeys.deleteCampaignButton,
                icon: Icon(Icons.delete),
                onPressed: () {
                  BlocProvider.of<CampaignsBloc>(context)
                      .add(CampaignDeleted(campaign));
                  Navigator.pop(context, campaign);
                },
              )
            ],
          ),
          body: CustomScrollView(slivers: <Widget>[
            makeHeader(),
            campaign == null
                ? Container(
                    key: MemKeys.emptyDetailsContainer,
                    color: Colors.red,
                    height: 100.0)
                : SliverFixedExtentList(
                    itemExtent: 250.0,
                    delegate: SliverChildListDelegate(
                      [
                        CampaignItem(
                          campaign: campaign,
                          onCheckboxChanged: (_) {
                            BlocProvider.of<CampaignsBloc>(context).add(
                              CampaignUpdated(campaign.copyWith(
                                  complete: !campaign.complete)),
                            );
                          },
                          onRosaryIncrement: () {
                            BlocProvider.of<CampaignsBloc>(context).add(
                              RosaryCountUpdated(campaign),
                            );
                          },
                          onTap: this.onTap,
                          onDismissed: this.onDismissed,
                        ),
                        Container(color: Colors.red),
                        Container(color: Colors.purple),
                        Container(color: Colors.green),
                        Container(color: Colors.orange),
                        Container(color: Colors.yellow),
                      ],
                    ),
                  ),

//            Padding(
//                    padding: EdgeInsets.all(16.0),
//                    child: SliverFixedExtentList(
//                      itemExtent: 150.0,
//                      delegate: SliverChildListDelegate(
//                        [
//                          Container(
//                            padding: EdgeInsets.only(right: 8.0),
//                            child: Checkbox(
//                                key: MemKeys.detailsScreenCheckBox,
//                                value: campaign.complete,
//                                onChanged: (_) {
//                                  BlocProvider.of<CampaignsBloc>(context).add(
//                                    CampaignUpdated(
//                                      campaign.copyWith(
//                                          complete: !campaign.complete),
//                                    ),
//                                  );
//                                }),
//                          ),
//                          Container(
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                Hero(
//                                  tag: '${campaign.id}__heroTag',
//                                  child: Container(
//                                    width: MediaQuery.of(context).size.width,
//                                    padding: EdgeInsets.only(
//                                      top: 8.0,
//                                      bottom: 16.0,
//                                    ),
//                                    child: Text(
//                                      campaign.title,
//                                      key: MemKeys.detailsCampaignItemTitle,
//                                      style:
//                                          Theme.of(context).textTheme.headline5,
//                                    ),
//                                  ),
//                                ),
//                                Text(
//                                  campaign.description,
//                                  key: MemKeys.detailsCampaignItemDescription,
//                                  style: Theme.of(context).textTheme.subtitle1,
//                                ),
//                              ],
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
          ]),
          floatingActionButton: FloatingActionButton(
            key: MemKeys.editCampaignFab,
            tooltip: localizations.editCampaign,
            child: Icon(Icons.edit),
            onPressed: campaign == null
                ? null
                : () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return AddEditScreen(
                            key: MemKeys.editCampaignScreen,
                            onSave: (title, description) {
                              BlocProvider.of<CampaignsBloc>(context).add(
                                CampaignUpdated(
                                  campaign.copyWith(
                                      title: title, description: description),
                                ),
                              );
                            },
                            isEditing: true,
                            campaign: campaign,
                          );
                        },
                      ),
                    );
                  },
          ),
        );
      },
    );
  }
}
