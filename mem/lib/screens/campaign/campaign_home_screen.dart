import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memapp/blocs/blocs.dart';
import 'package:memapp/widgets/campaign/widgets.dart';
import 'package:memapp/models/models.dart';
import 'package:memapp/localization.dart';

class CampaignHomeScreen extends StatelessWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => CampaignHomeScreen());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TabBloc, AppTab>(
      builder: (context, activeTab) {
        return Scaffold(
          appBar: AppBar(
            title: Text(MemLocalizations.of(context).appTitle),
            actions: [
              FilterButton(visible: activeTab == AppTab.campaigns),
              ExtraActions(),
            ],
          ),
          body: activeTab == AppTab.campaigns ? FilteredCampaigns() : Stats(),
          floatingActionButton: FloatingActionButton(
            key: MemKeys.addCampaignFab,
            onPressed: () {
              Navigator.pushNamed(context, MemRoutes.addCampaign);
            },
            child: Icon(Icons.add),
            tooltip: MemLocalizations.of(context).addCampaign,
          ),
          bottomNavigationBar: TabSelector(
            activeTab: activeTab,
            onTabSelected: (tab) =>
                BlocProvider.of<TabBloc>(context).add(TabUpdated(tab)),
          ),
        );
      },
    );
  }
}
