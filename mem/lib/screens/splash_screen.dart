// Splash Screen Widgets
import 'dart:core';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:utility/utility.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:memapp/widgets/bordered_text.dart';
import 'package:memapp/widgets/pop_navigation_grid.dart';
import 'package:utility/src/dialogshower.dart' as DialogShower;
import 'package:rive/rive.dart' as rive;
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:memapp/screens/consecration.dart';
import 'package:memapp/screens/videos.dart';
import 'package:flame/game.dart';
import 'package:memapp/splash_game/splash_star.dart';
import 'package:memapp/widgets/count_up.dart';

class SplashScreen extends StatefulWidget {
  final AppState appState;
  final FirebaseStorage storage;
  final UserBloc userBloc;
  final RouteObserver routeObserver;
  final MessageBloc messageBloc;
  final RosaryBloc rosaryBloc;

  SplashScreen(
      {Key key,
      @required this.appState,
      @required this.storage,
      this.userBloc,
      this.routeObserver,
      this.messageBloc,
      this.rosaryBloc})
      : super(key: MemKeys.splashScreen);

  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  //Animation Controller for counter up
  AnimationController _controller;
  AnimationController _splashController;
  Animation<double> _splashAnimation;
  String tappedRequest = "";

  //The latest total rosary count
  int kEndValue = 0;

  var _duration = new Duration(seconds: 6);
  DocumentSnapshot snapshot;
  var container;
  String _gospelMessage = "";

  rive.Artboard _loaderArtboard;
  rive.StateMachineController _loaderController;
  rive.SMIInput<bool> _isLoading;

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _controller.dispose();
    _splashController.dispose();

    super.dispose();
  }

  @override
  void initState() {
    // Always call super.initState
    super.initState();

    _controller = new AnimationController(vsync: this, duration: _duration);

    _controller.forward(from: 0.0);

    _splashController = AnimationController(
        duration: const Duration(milliseconds: 3000), vsync: this);

    _splashAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _splashController,
        curve: Interval(0.0, 0.67, curve: Curves.easeIn),
      ),
    );

    rootBundle.load('assets/rive/loader.riv').then(
      (data) async {
        // Load the RiveFile from the binary data.
        final file = rive.RiveFile.import(data);

        // The artboard is the root of the animation and  gets drawn in the
        // Rive widget.
        final artboard = file.mainArtboard;
        // Add a controller to play back a known animation on the main/default
        // artboard. We store a reference to it so we can toggle playback.
        var controller = rive.StateMachineController.fromArtboard(
            artboard, 'load_state_machine');

        if (controller != null) {
          artboard.addController(controller);
          setState(() => _isLoading = controller.findInput('isLoading'));
        }

        setState(() => _loaderArtboard = artboard);
      },
    );

    FirebaseDatabase.instance.setPersistenceEnabled(false);
    //FirebaseDatabase.instance.setPersistenceCacheSizeBytes(10000000);
  }

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = UserProvider.of(context);
    RosaryBloc rosaryBloc = RosaryProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    container = AppStateContainer.of(context);

    DateTime lastOpenDateGuidance;
    DateTime lastOpenNewUserNotitification;
    DateTime lastOpenDateRosaryLimitNotification;
    //int lastOpenChatRoom;
    //Initialise user state if user already once logged in.

    SharedPreferences.getInstance().then((prefs) {
      lastOpenDateGuidance = DateTime.fromMillisecondsSinceEpoch(
          prefs.getInt("guidance" + "_last_open") ??
              DateTime.now().millisecondsSinceEpoch);

      lastOpenNewUserNotitification = DateTime.fromMillisecondsSinceEpoch(
          prefs.getInt("new_user" + "_last_open") ??
              DateTime.now().millisecondsSinceEpoch);

      lastOpenDateRosaryLimitNotification = DateTime.fromMillisecondsSinceEpoch(
          prefs.getInt("rosary_limits" + "_last_open") ??
              DateTime.now().millisecondsSinceEpoch);

      //lastOpenChatRoom = prefs.getInt("English" + "_chatroom_last_open");
    });

    userBloc.userRepository.locales().listen((_locales) {
      container.state.locales = _locales;
    });

    userBloc.userRepository.regions().listen((_regions) {
      container.state.regions = _regions;
      debugPrint("regions from splash" + _regions.toString());
    });

    userBloc.authRepository.initUser().then((user) async {
      if (user != null) {
        await userBloc.userRepository
            .userExists(user.uid)
            .then((userExists) async {
          if (userExists) {
            userBloc.userProfile(user.uid).then((userProfile) {
              container.state.userDetails = userProfile;
              container.state.user = user;

              Stream<List<MessageEntity>> rosaryMessages =
                  messageBloc.messageRepository.rosaryMessages();
              rosaryMessages.listen((result) {
                container.state.rosaryMessages =
                    result.map(MEMmessage.fromEntity).toList();
              }).onError((error) {
                debugPrint("Error in getting  messages from firebase" +
                    error.toString());
              });

//
//              Stream<List<MessageEntity>> requests =
//                  messageBloc.messageRepository.requests();
//              requests.listen((result) {
//                container.state.requests =
//                    result.map(MEMmessage.fromEntity).toList();
//              }).onError((error) {
//                debugPrint("Error in getting  messages from firebase" +
//                    error.toString());
//              });
//
//              Stream<List<MessageEntity>> testimonies =
//                  messageBloc.messageRepository.testimonies();
//              testimonies.listen((result) {
//                container.state.testimonies =
//                    result.map(MEMmessage.fromEntity).toList();
//              }).onError((error) {
//                debugPrint("Error in getting  messages from firebase" +
//                    error.toString());
//              });
            });

            await userBloc.userActive(user.uid).then((isActive) {
              if (isActive) {
                debugPrint("User from splash screen: " + user.toString());

                //ACL List 1.9.823 onwards
                userBloc.userRepository.userRole(user.uid).then((role) {
                  if (role == "admin") {
                    userBloc.userRepository
                        .getUserAcl(user.uid)
                        .then((userAcl) {
                      container.state.userAcl = Acl.fromEntity(userAcl);
                    });

                    userBloc.userRepository
                        .isSuperAdmin(user.uid)
                        .then((isSuperAdmin) {
                      if (isSuperAdmin) {
                        userBloc.userRepository.users().listen((members) {
                          List<MemUser> _members =
                              members.map(MemUser.fromEntity).toList();
                          container.state.members = _members;
                          debugPrint(
                              "Members from spalsh: " + members.toString());
                        }).onError((error) {
                          debugPrint(
                              "Error in getting members data from firebase" +
                                  error.toString());
                        });

                        Stream<List<MessageEntity>> guidanceMessages =
                            messageBloc.messageRepository.guidanceMessages();
                        guidanceMessages.listen((result) {
                          container.state.guidanceMessages =
                              result.map(MEMmessage.fromEntity).toList();

                          container.state.unreadGuidanceReqCount = container
                              .state.guidanceMessages
                              .where((i) =>
                                  i.type == "guidance" &&
                                  i.time.isAfter(lastOpenDateGuidance))
                              .length;
                        }).onError((error) {
                          debugPrint(
                              "Error in getting  messages from firebase" +
                                  error.toString());
                        });

                        //Admin Notifications for new user registration
                        messageBloc.messageRepository
                            .adminNotifications("new_user")
                            .listen((newUserNotifications) {
                          List<FCMNotification> _newUserNotifications =
                              newUserNotifications
                                  .map(FCMNotification.fromEntity)
                                  .toList();
                          container.state.newUserNotifications =
                              _newUserNotifications;

                          container.state.newUserNotifications.forEach((msg) {
                            if (msg.time
                                .isAfter(lastOpenNewUserNotitification)) {
                              container.state.unreadNewUserReqCount++;
                            }
                          });
                        }).onError((error) {
                          debugPrint(
                              "Error in getting New User Notifications  data from firebase" +
                                  error.toString());
                        });

                        messageBloc.messageRepository
                            .adminNotifications("rosary_limits")
                            .listen((rosaryLimitNotifications) {
                          List<FCMNotification> _rosaryLimitNotifications =
                              rosaryLimitNotifications
                                  .map(FCMNotification.fromEntity)
                                  .toList();
                          container.state.rosaryLimitNotifications =
                              _rosaryLimitNotifications;

                          container.state.rosaryLimitNotifications
                              .forEach((msg) {
                            if (msg.time
                                .isAfter(lastOpenDateRosaryLimitNotification)) {
                              container.state.unreadRosaryLimitsCount++;
                            }
                          });
                        }).onError((error) {
                          debugPrint(
                              "Error in getting New User Notifications  data from firebase" +
                                  error.toString());
                        });
                      }
                    });
                  } else {
                    container.state.userAcl = Acl(
                        id: "",
                        name: "",
                        grantAdminRole: false,
                        revokeAdminRole: false,
                        grantSuperAdminRole: false,
                        revokeSuperAdminRole: false,
                        activateUser: false,
                        deactivateUser: false,
                        blockUser: false,
                        unBlockUser: false,
                        deleteUser: false,
                        regionalAdmin: false,
                        localeAdmin: false,
                        viewCounselling: false,
                        rosaryDelete: false,
                        messageDelete: false,
                        addVideoTestimony: false,
                        addBishopsMessage: false,
                        editAboutUsContent: false);
                  }
                });

                //Get the rosary List of user
                List<RosaryCount> savedRosaryList;

                rosaryBloc.rosaryRepository.fetchRosary().then((rosary) {
                  rosaryBloc.rosaryRepository
                      .rosaryCountList(container.state.user.uid)
                      .listen((rosaryList) {
                    savedRosaryList = rosaryList.isNotEmpty
                        ? rosaryList.map(RosaryCount.fromEntity).toList()
                        : null;
                    container.state.rosaryList = savedRosaryList;

                    //ACL List 1.9.823 onwards

//                    userBloc.userRepository.acl().listen((aclList) {
//                      List<Acl> _aclList = aclList.map(Acl.fromEntity).toList();
//                      container.state.aclList = _aclList;
//                      debugPrint("ACL List from spalsh: " + aclList.toString());
//                    }).onError((error) {
//                      debugPrint("Error in getting ACL List from firebase" +
//                          error.toString());
//                    });

                    messageBloc.messageRepository
                        .getLatestGospelCard()
                        .then((gospelCard) {
                      container.state.latestGospelCard = gospelCard.cardDetails;
                    });
                  }).onError((error) {
                    debugPrint("Error in getting data from firebase" +
                        error.toString());
                  });
                });
              }
            });
          }
        });
      }
    });

// Timer to load initial data.

    Timer(_duration, () {
      this.container.state.isLoading = false;
      if (this.container.state.user != null) {
        if (this.container.state.userDetails.status != null &&
            this.container.state.userDetails.status == true) {
//          if (tappedRequest == "")
          Navigator.of(context).pushReplacementNamed('/home');

//          Navigator.pushReplacement(
//              context,
//              MaterialPageRoute(
//                builder: (context) => HomeScreen(
//                  key: MemKeys.homeScreen,
//                  appState: widget.appState,
//                  storage: widget.storage,
//                  userBloc: widget.userBloc,
//                  routeObserver: widget.routeObserver,
//                  messageBloc: widget.messageBloc,
//                  rosaryBloc: widget.rosaryBloc,
//                  gospelMsg: "",
//                  container: AppStateContainer.of(context),
//                  navigateToScreen: tappedRequest,
//                ),
//              ));
//          } else {
//            switch (tappedRequest) {
//              case "videos":
//                {
//                  Navigator.pushReplacement(
//                      context,
//                      MaterialPageRoute(
//                        builder: (context) => VideoScreen(),
//                      ));
//
//                  tappedRequest = "";
//                }
//                break;
//
//              default:
//                Navigator.of(context).pushReplacementNamed('/home');
//            }
//          }
        } else {
          Navigator.of(context).pushReplacementNamed('/auth');
        }
      } else {
        Navigator.of(context).pushReplacementNamed('/auth');
      }
    });

    // Timer to load initial data.

    return _popMessageView;
  }

  @override
  Widget get _popMessageView {
    var appTitleMainFontSize = MediaQuery.of(context).size.height * 0.040;
    var gospelMessageFontSize = MediaQuery.of(context).size.height * 0.025;
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    bool isTablet = shortestSide >= 600;
    var rosariesOfferedFontSize = MediaQuery.of(context).size.width * 0.040;
    RosaryBloc rosaryBloc = RosaryProvider.of(context);
    _splashController.forward();

//    return Scaffold(
//      backgroundColor: Colors.white,
//      body: StreamBuilder<int>(
//          stream: rosaryBloc.rosaryTotalCount,
//          initialData: 0,
//          builder: (context, snapshot) => GameWidget<TwinklingStars>(
//                game: TwinklingStars(
//                    screenSize: MediaQuery.of(context).size,
//                    rosariesOffered: snapshot.data),
//              )),
//    );

    return Scaffold(
//      appBar: AppBar(
//        elevation: 0.0,
//        backgroundColor: Theme.of(context).backgroundColor,
//      ),
      body: PopNavigationGrid(
        openBottomAddRosarySheet: _addRosaryBottomSheetCallBack,
        collectRosary: _collectRosaryCallBack,
        rosaryCount: rosaryCount,
        addPrayerRequest: _addPrayerRequestCallBack,
        addTestimony: _addTestimonyCallBack,
        communityChat: _communityChatCallBack,
        guidanceChat: _guidanceChatCallBack,
        videos: _videosCallBack,
        consecration: _consecrationCallBack,
        navigationReady: false,
        isLoading: _isLoading,
        loaderArtboard: _loaderArtboard,
      ),
      backgroundColor: Theme.of(context).backgroundColor,
    );
  }

  Widget get rosaryCount {
    RosaryBloc rosaryBloc = RosaryProvider.of(context);

//    return StreamBuilder<int>(
//      stream: rosaryBloc.rosaryTotalCount,
//      initialData: 0,
//      builder: (context, snapshot) => Countup(
//          animation: new StepTween(
//        begin: snapshot.data,
//        end: snapshot.data,
//      ).animate(_controller)),
//    );

    return StreamBuilder<int>(
      stream: rosaryBloc.rosaryTotalCount,
//      initialData: 0,
      builder: (context, snapshot) {
        if (snapshot.hasError) return Container();
        if (!snapshot.hasData || snapshot.data == null) {
          //_isLoading.value = true;
          return Container(
            color: Theme.of(context).backgroundColor,
            height: 30,
          );
        } else {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
//              return Container();
              _isLoading.value = true;
              return _loaderArtboard == null
                  ? Container()
                  : SizedBox(
                      height: 30.0,
                      child: rive.Rive(artboard: _loaderArtboard));

            default:
              return Countup(
                  animation: new StepTween(
                begin: snapshot.data - snapshot.data % 300,
                end: snapshot.data,
              ).animate(_controller));
          }
        }
      },
    );
  }

  Widget rosariesOffered(BuildContext buildContext, TwinklingStars game) {
    var rosariesOfferedFontSize = MediaQuery.of(context).size.width * 0.040;
    RosaryBloc rosaryBloc = RosaryProvider.of(context);

    return Stack(
      children: <Widget>[
//                              Align(
//                                  alignment: AlignmentDirectional.bottomCenter,
//                                  child: Image.asset("assets/rosary-icon.png",
//                                      width: MediaQuery.of(context).size.width *
//                                          0.5)),
        Align(
            alignment: AlignmentDirectional.center,
            child: Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.02),
                child: BorderedText(
                    strokeCap: StrokeCap.round,
                    strokeColor: Colors.white,
                    strokeJoin: StrokeJoin.round,
                    strokeWidth: 2.0,
                    child: Text(
                      'ROSARIES  OFFERED',
                      style: new TextStyle(
                        fontSize: rosariesOfferedFontSize,
                        color: Colors.redAccent,
                        fontWeight: FontWeight.bold,
                      ),
                    )))),
        Align(
            alignment: AlignmentDirectional.center,
            child: Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.02 +
                        rosariesOfferedFontSize * 2.8),
                child: StreamBuilder<int>(
                  stream: rosaryBloc.rosaryTotalCount,
                  initialData: 0,
                  builder: (context, snapshot) => Countup(
                      animation: new StepTween(
                    begin: snapshot.data - snapshot.data % 300,
                    end: snapshot.data,
                  ).animate(_controller)),
                ))),
      ],
    );
  }

  Widget gospelMessage() {
    var deviceWidth = MediaQuery.of(context).size.width;
    var deviceHeight = MediaQuery.of(context).size.height;
    var gospelMessageTopPosition = deviceHeight * 0.3;
    var gospelMessageFontSize = MediaQuery.of(context).size.height * 0.025;
    UserBloc userBloc = UserProvider.of(context);

    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('mem')
          .doc('rosary')
          .collection('gospelcards')
          .where('locale', isEqualTo: userBloc.userDetails.locale)
          .where('startUpCard', isEqualTo: true)
          .limit(1)
          .snapshots(),
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasError) return Container();
        if (!snapshot.hasData || snapshot.data == null) {
          return Container();
        } else {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
//              return Container(
//                alignment: Alignment.bottomCenter,
//                margin: EdgeInsets.symmetric(vertical: 32.0, horizontal: 32.0),
//                child: RevealProgressButton(appState: widget.appState),
//              );

              return Container();
//              return Center(
//                  child: CircularProgressIndicator(
//                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white)));

            default:
              try {
                return SizedBox(
                  width: deviceWidth * 0.9,
                  height: deviceHeight * 0.3,
                  child: Container(
//                      margin: EdgeInsets.only(
//                        top: MediaQuery.of(context).size.height * 0.6,
//                        bottom: MediaQuery.of(context).size.height * 0.2,
//                      ),
                      //padding: EdgeInsets.all(gospelMessageFontSize * 0.8),
                      child: BorderedText(
                          strokeCap: StrokeCap.square,
                          strokeColor: Colors.white24,
                          strokeJoin: StrokeJoin.miter,
                          strokeWidth: 3.0,
                          child: Text(
                            snapshot.data.documents.last["cardDetails"]
                                .toUpperCase(),
                            style: TextStyle(
                                color: Color.fromRGBO(58, 43, 104, 1),
                                fontSize: gospelMessageFontSize,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                            // or Alignment.topLeft
                          ))),
                );
              } catch (e) {
                return Container();
              }
          }
        }
      },
    );
  }

  // Navigation CallBacks from popMessage View

  void _addRosaryBottomSheetCallBack() {
    tappedRequest = "addRosary";
  }

  void _collectRosaryCallBack() {
    setState(() {
      tappedRequest = "collectRosary";
    });
//    showError(
//        errorMsg: MemError(
//            title: "Not Connected",
//            body:
//                "Please check your internet connection. You can submit Rosary only when you are online."));
  }

  // Add Prayer Request Call Back
  void _addPrayerRequestCallBack() {
    setState(() {
      tappedRequest = "prayerRequest";
    });
  }

// Add Testimony Call Back
  void _addTestimonyCallBack() {
    setState(() {
      tappedRequest = "testimony";
    });
  }

//Community Call Back
  void _communityChatCallBack() {
    setState(() {
      tappedRequest = "communityChat";
    });
  }

//Guidance Request Callback
  void _guidanceChatCallBack() {
    setState(() {
      tappedRequest = "adminChat";
    });
  }

//Videos Callback
  void _videosCallBack() {
    setState(() {
      tappedRequest = "videos";
    });
  }

//Consecration Callback
  void _consecrationCallBack() {
    setState(() {
      tappedRequest = "consecration";
    });

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ConsecrationScreen(),
        ));
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "OK",
      confirmFn: () {
        Navigator.pop(context);
//        Navigator.of(context).pushNamedAndRemoveUntil(
//            "/splash", (Route<dynamic> route) => false);
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }
}
