import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:base_helpers/basehelpers.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/app_state_container.dart';
import 'package:utility/src/dialogshower.dart' as DialogShower;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:memapp/screens/gift_verse.dart';
import 'dart:async';
import 'package:firebase_database/firebase_database.dart';

class RosaryListScreen extends StatefulWidget {
  var container;
  final RosaryBloc rosaryBloc;
  final UserBloc userBloc;
  final bool connectionStatus;
  final bool fcmNotificationEnable;
  Stream<QuerySnapshot> badgeStream;
  final RouteObserver<PageRoute> routeObserver;

  @override
  _RosaryListState createState() => _RosaryListState();
  RosaryListScreen(
      {Key key,
      this.container,
      this.rosaryBloc,
      this.connectionStatus,
      this.fcmNotificationEnable,
      this.badgeStream,
      @required this.routeObserver,
      this.userBloc})
      : super(key: key ?? MemKeys.rosaryListScreen);
}

class _RosaryListState extends State<RosaryListScreen> {
  static final GlobalKey<FormState> rosaryListFormKey = GlobalKey<FormState>();
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  ListModel<RosaryCount> _list;
  List<RosaryCount> savedRosaryList = const [];
  RosaryCount _selectedItem;
  StreamSubscription<QuerySnapshot> badgeStreamSub;
  DatabaseReference _messagesReference = FirebaseDatabase.instance.reference();
  String _message = "";
  TextEditingController msgController = TextEditingController();

//  RosaryCount
//      _nextItem; // The next item inserted when the user presses the '+' button.

  @override
  void initState() {
    super.initState();
    badgeStreamSub = widget.badgeStream.listen(onBadge);
//    _nextItem = _list._items[widget.container.state.rosaryCountList];

//    RosaryBloc rosaryBloc = RosaryProvider.of(context);
  }

  @override
  void dispose() {
    badgeStreamSub.cancel();
    msgController.dispose();
    super.dispose();
  }

  // Used to build list items that haven't been removed.
  Widget _buildItem(
      BuildContext context, int index, Animation<double> animation) {
    //debugPrint("Initial Value:  " + _list[index].toString());

    return CardItem(
      connectionStatus: widget.connectionStatus,
      animation: animation,
      item: _list[index],
      selected: _selectedItem == _list[index],
      onTap: () {
        setState(() {
          _selectedItem = _selectedItem == _list[index] ? null : _list[index];
        });
      },
    );
  }

  // Used to build an item after it has been removed from the list. This method is
  // needed because a removed item remains  visible until its animation has
  // completed (even though it's gone as far this ListModel is concerned).
  // The widget will be used by the [AnimatedListState.removeItem] method's
  // [AnimatedListRemovedItemBuilder] parameter.
  Widget _buildRemovedItem(
      RosaryCount item, BuildContext context, Animation<double> animation) {
    return CardItem(
      animation: animation,
      item: item,
      selected: false,
      // No gesture detector here: we don't want removed items to be interactive.
    );
  }

  // Insert the "next item" into the list model.
  void _insert() {
    final int index =
        _selectedItem == null ? _list.length : _list.indexOf(_selectedItem);
    _list.insert(
        index,
        new RosaryCount(
            author: "Name",
            rosaryCount: 0,
            time: DateTime.now(),
            totalCount: 0));
    savedRosaryList.insert(
        index,
        new RosaryCount(
            author: "Name",
            rosaryCount: 0,
            time: DateTime.now(),
            totalCount: 0));
  }

  void _insertFromBS(RosaryCount rc) {
    setState(() {
      List<RosaryCount> initialList = [rc];

      final int index =
          _selectedItem == null ? _list.length : _list.indexOf(_selectedItem);

      if (_list == null) {
        _list = ListModel<RosaryCount>(
          listKey: _listKey,
          initialItems: initialList,
          removedItemBuilder: _buildRemovedItem,
        );
      } else {
        _list.insert(index, rc);
      }
      savedRosaryList.insert(index, rc);
    });
  }

  // Remove the selected item from the list model.
  void _remove(RosaryBloc rosaryBloc) {
    if (_selectedItem != null) {
//      if (_list.indexOf(_selectedItem) == 0) {
//        debugPrint("Cannot delete this record");
//      } else {
      setState(() {
        _list.removeAt(_list.indexOf(_selectedItem));
        rosaryBloc.rosaryRepository
            .deleteRosaryListEntry(_selectedItem.toEntity());

        _selectedItem = null;
      });
      //}
    }
  }

  @override
  Widget build(BuildContext context) {
    var container = AppStateContainer.of(context);
    final rosaryBloc = RosaryProvider.of(context);
    //Initialise a default rosary list

//    RosaryCount rosaryCount = RosaryCount(
//        author: widget.container.state.user.displayName ?? "Name",
//        uid: widget.container.state.user.uid,
//        totalCount: 0,
//        rosaryCount: 0);
//
//    debugPrint("Author from savedRosaryList " + rosaryCount.author);

    savedRosaryList = container.state.rosaryList;

    _list = ListModel<RosaryCount>(
      listKey: _listKey,
      initialItems: savedRosaryList,
      removedItemBuilder: _buildRemovedItem,
    );

    ///////////////////////////////////////////////

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorLight,
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment(-1.0, 0),
              end: Alignment(
                  1.0, 0.0), // 10% of the width, so there are ten blinds.
              colors: [
                Theme.of(context).primaryColorDark,
                Theme.of(context).backgroundColor
              ], // whitish to gray
              tileMode:
                  TileMode.repeated, // repeats the gradient over the canvas
            ),
          ),
        ),
        title: const Text('Rosary List'),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.add_circle),
              tooltip: 'Add a new contributor',
              //onPressed: _insert,
              onPressed: () {
                setState(() {
                  if (widget.connectionStatus) {
                    addRosaryListEntry(rosaryBloc);
                  } else {
                    showError(
                        errorMsg: MemError(
                            title: "Not Connected",
                            body:
                                "Please check your internet connection. You can submit Rosary only when you are online."));
                  }
                });
              }),
          IconButton(
            icon: const Icon(Icons.remove_circle),
            onPressed: () {
              _remove(rosaryBloc);
            },
            tooltip: 'remove the selected contributor',
          ),
        ],
      ),
      body: Column(children: <Widget>[
        _list.length == 0
            ? Container(
                decoration: new BoxDecoration(
                    color: Theme.of(context).primaryColorLight),
                padding: EdgeInsets.all(15.0),
                child: Text(
                    """Please press the + button to add a new rosary list entry. 
  
To delete an entry, press an entry and click the - button"""))
            : Container(),
        Expanded(
          child: Container(
            child: AnimatedList(
              key: _listKey,
              initialItemCount: _list.length,
              itemBuilder: _buildItem,
            ),
            decoration:
                new BoxDecoration(color: Theme.of(context).primaryColorLight
//
                    ),
          ),
        )
      ]),
    );
  }

  // Fields for bottom sheet

  Widget authorBSField(BuildContext context) {
    final rosaryBloc = RosaryProvider.of(context);
    //TextEditingController _controller = new TextEditingController();
    return StreamBuilder(
      stream: rosaryBloc.author,
      builder: (context, snapshot) {
        try {
          return Container(
              color: Theme.of(context).primaryColorLight,
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              child: TextField(
                  onChanged: rosaryBloc.changeAuthor,
                  keyboardType: TextInputType.text,
                  maxLines: 1,
                  autocorrect: true,
                  autofocus: true,
                  //controller: _controller,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18.0,
//                    height: 0.05,
                    color: Colors.black,
                  ),
                  cursorColor: Color.fromRGBO(11, 129, 140, 1.0),
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        borderSide: BorderSide(
                            color: Color.fromRGBO(217, 178, 80, 1.0))),
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        borderSide: new BorderSide(
                            color: Color.fromRGBO(217, 178, 80, 1.0))),

                    hintText: "Name",
                    //labelText: 'Rosary Count',
                    contentPadding: EdgeInsets.all(12.0),
                  )));
        } catch (e) {
          return Container();
        }
      },
    );
  }

  Widget rosaryCountBSField(BuildContext context) {
    final rosaryBloc = RosaryProvider.of(context);
    //TextEditingController _controller = new TextEditingController();
    return StreamBuilder(
      stream: rosaryBloc.rosaryCount,
      builder: (context, snapshot) {
        try {
          return Container(
              height: 40.0,
              color: Colors.white,
              child: TextFormField(
                onChanged: rosaryBloc.changeRosaryCount,
                keyboardType: TextInputType.number,
                maxLines: 1,

                //autofocus: true,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18.0,
//                    height: 0.05,
                  color: Colors.black,
                ),
                cursorColor: Color.fromRGBO(11, 129, 140, 1.0),
                decoration: InputDecoration(
                  helperText: ' ',
//                  focusedBorder: OutlineInputBorder(
//                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
//                      borderSide:
//                          BorderSide(color: Color.fromRGBO(217, 178, 80, 1.0))),
                  fillColor: Colors.white,
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
//                      borderSide: new BorderSide(
//                          color: Color.fromRGBO(217, 178, 80, 1.0))),

//                    hintText: MemLocalizations(Localizations.localeOf(context))
//                        .messageHint,
                  //labelText: 'Rosary Count',
                  contentPadding: EdgeInsets.only(top: 5.0),
                  errorText: snapshot.error,
//                  errorBorder: OutlineInputBorder(
//                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
//                      borderSide: new BorderSide(color: Colors.white)),
                  errorStyle: TextStyle(fontSize: 8.0),
                  hintText: "0",
                ),
              ));
        } catch (e) {
          return Container();
        }
      },
    );
  }

  Widget offerRosaryBSButton(BuildContext context) {
    RosaryBloc rosaryBloc = RosaryProvider.of(context);
    UserBloc userBloc = UserProvider.of(context);

    var container = AppStateContainer.of(context);

    return StreamBuilder(
      stream: rosaryBloc.submitValidRosaryCount,
      builder: (context, snapshot) {
        try {
          return Container(
              margin: EdgeInsets.only(top: 10.0),
              padding: EdgeInsets.only(
                  top: 0.0, bottom: 10.0, left: 10.0, right: 10.0),
              child: FlatButton(
                padding: EdgeInsets.only(
                    top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
                color: Theme.of(context).backgroundColor,
                splashColor: Theme.of(context).primaryColorLight,
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  if (snapshot.hasData) {
                    try {
                      RosaryCount rc = rosaryBloc
                          .addRosaryListEntry(container.state.userDetails);
                      _insertFromBS(rc);
                      Navigator.pop(context);
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                            builder: (context) => GiftVerseScreen(
//                                isBadge: false,
//                                giftVerse: MemLists().getRandomListElement(
//                                    listType: "bible_verse",
//                                    locale: userBloc.userDetails.locale),
//                                key: MemKeys.giftVerseScreen),
//                          ));
                      //messageBloc.addRosary(userDetails)
                      //_confirmRosaryListEntry(rc);
                      showError(
                          errorMsg: MemError(
                              title: "Rosary Added",
                              body: "Thanks for offering rosary"));
                    } catch (e) {
                      debugPrint("error message : " + e.toString());
                      Navigator.pop(context);
                    }
                  }

                  ///todo: the appropriate implementation needs to be introduced. Its a quick fix now.
                },
              ));
        } catch (e) {
          return Container();
        }
      },
    );
  }

//# Bottom Sheet Fields END

  void addRosaryListEntry(RosaryBloc rosaryBloc) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
              margin: const EdgeInsets.only(top: 0.0),
              padding:
                  const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                gradient: LinearGradient(
                  begin: Alignment(0, -1.0),
                  end: Alignment(
                      0.0, 1.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    Color(0xFFc90f0b),
                    Theme.of(context).backgroundColor
                  ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
              child: Form(
                  key: rosaryListFormKey,
                  autovalidate: false,
                  onWillPop: () {
                    return Future(() => true);
                  },
                  child: StreamBuilder<int>(
                      stream: rosaryBloc.rosaryTotalCount,
                      initialData: 0,
                      builder: (context, snapshot) => new ListView(children: [
                            Container(
                                padding: EdgeInsets.only(
                                    left: 15.0, right: 15.0, top: 5.0),
                                color: Theme.of(context).primaryColor,
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                        flex: 3, child: authorBSField(context)),
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                top: 20.0, bottom: 20.0),
                                            child:
                                                rosaryCountBSField(context))),
                                    offerRosaryBSButton(context),
                                  ],
                                ))
                          ]))));
        });
  }

  _confirmRosaryListEntry(RosaryCount item) {
    var container = AppStateContainer.of(context);
    RosaryBloc rosaryBloc = RosaryProvider.of(context);

    var dialog = buildDialog(
      title: "Rosaries Offered, is saved",
      message: "Add More Rosary Contributions ?",
      confirm: "Yes",
      confirmFn: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
      cancel: "No",
      cancelFn: () {
        Navigator.of(context, rootNavigator: true).pop();
        Navigator.pop(context);
      },
    );

    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "Okay",
      confirmFn: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void onBadge(QuerySnapshot snap) {
    MessageBloc messageBloc = MessageProvider.of(context);
    UserBloc userBloc = UserProvider.of(context);

    try {
      snap.docs.forEach((doc) {
        if (snap.docs.length != 0 &&
            doc["action"] == 'COLLECT ROSARY' &&
            !doc["isComplete"]) {
          //Disabling congratulations message on customer request.
          //_sendMsgtoChatRoom(doc.data);

          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => GiftVerseScreen(
                    messageBloc: messageBloc,
                    userBloc: userBloc,
                    isBadge: true,
                    latestBadgeID: snap.docs.last["id"],
                    routeObserver: widget.routeObserver,
                    badgeType: "good_samaritan",
                    giftVerse: "You Have a Good Samaritan Badge",
                    badgeStream: FirebaseFirestore.instance
                        .collection('mem')
                        .doc('rosary')
                        .collection('users')
                        .doc(widget.userBloc.userDetails.id)
                        .collection('badges')
                        .where('action', isEqualTo: 'COLLECT ROSARY')
                        .snapshots(),
                    key: MemKeys.giftVerseScreen),
              ));
        }
      });
    } catch (e) {}
  }

  void _sendMsgtoChatRoom(Map badge) {
    MessageBloc messageBloc = MessageProvider.of(context);
    UserBloc userBloc = UserProvider.of(context);

    String id = Uuid().generateV4();

    String msgTitle = "Congratulations " +
        userBloc.userDetails.title +
        " " +
        userBloc.userDetails.name;

    String _message = userBloc.userDetails.title +
        " " +
        userBloc.userDetails.name +
        " has won a badge : " +
        badge['title'];

    String _fcmMessage = "Let us congratulate " +
        userBloc.userDetails.title +
        " " +
        userBloc.userDetails.name +
        "for winning a badge : " +
        badge['title'];

    var message = {
      'id': id,
      'sender': {
        'name': "Admin",
        'id': "",
        'imageUrl':
            "https://firebasestorage.googleapis.com/v0/b/memapp-dev.appspot.com/o/profile%2Fd3aoKg9if4Mc3oMt7xKFkfyR2ia2.jpeg?alt=media&token=3b8d515e-9ff4-4110-b130-f959b381b085"
      },
      'text': "\n🎉 " + msgTitle + " 🎉\n\n" + _message,
      'time': DateTime.now().millisecondsSinceEpoch,
      'type': 'badge_good_samaritan',
      'region': 'all',
      'locale': userBloc.userDetails.locale
    };
    _messagesReference
        .child("chats")
        .child(userBloc.userDetails.locale ?? "English")
        .child(id)
        .set(message);

    FCMNotification notification = FCMNotification(
        title: "Let us congratulate " +
            userBloc.userDetails.title +
            " " +
            userBloc.userDetails.name,
        body: _fcmMessage ?? "",
        topic: userBloc.userDetails.locale ?? "English",
        author: userBloc.userDetails.name,
        time: DateTime.now().add(Duration(minutes: 1)),
        authorID: userBloc.userDetails.id ?? "",
        priority: "medium");

    if (widget.fcmNotificationEnable)
      messageBloc.messageRepository.addNotification(notification.toEntity());
  }
}

/// Keeps a Dart List in sync with an AnimatedList.
///
/// The [insert] and [removeAt] methods apply to both the internal list and the
/// animated list that belongs to [listKey].
///
/// This class only exposes as much of the Dart List API as is needed by the
/// sample app. More list methods are easily added, however methods that mutate the
/// list must make the same changes to the animated list in terms of
/// [AnimatedListState.insertItem] and [AnimatedList.removeItem].
class ListModel<E> {
  ListModel({
    @required this.listKey,
    @required this.removedItemBuilder,
    Iterable<E> initialItems,
  })  : assert(listKey != null),
        assert(removedItemBuilder != null),
        _items = List<E>.from(initialItems ?? <E>[]);

  final GlobalKey<AnimatedListState> listKey;
  final dynamic removedItemBuilder;
  List<E> _items = [];

  AnimatedListState get _animatedList => listKey.currentState;

  void insert(int index, E item) {
    _items.insert(index, item);
    _animatedList.insertItem(index);
  }

  E removeAt(int index) {
    final E removedItem = _items.removeAt(index);
    if (removedItem != null) {
      _animatedList.removeItem(index,
          (BuildContext context, Animation<double> animation) {
        return removedItemBuilder(removedItem, context, animation);
      });
    }
    return removedItem;
  }

  int get length => _items.length;

  E operator [](int index) => _items[index];

  int indexOf(E item) => _items.indexOf(item);
}

/// Displays its integer item as 'item N' on a Card whose color is based on
/// the item's value. The text is displayed in bright green if selected is true.
/// This widget's height is based on the animation parameter, it varies
/// from 0 to 128 as the animation varies from 0.0 to 1.0.
class CardItem extends StatelessWidget {
  const CardItem(
      {Key key,
      @required this.animation,
      this.onTap,
      @required this.item,
      this.selected: false,
      this.connectionStatus})
      : assert(animation != null),
        assert(selected != null),
        super(key: key);

  final Animation<double> animation;
  final VoidCallback onTap;
  final RosaryCount item;
  final bool selected;
  final bool connectionStatus;

  void showError({MemError errorMsg, BuildContext context}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "Okay",
      confirmFn: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.display1;

    if (selected) {
      textStyle = textStyle.copyWith(color: Colors.lightGreenAccent[400]);
    }
    return Container(
      padding:
          const EdgeInsets.only(left: 20.0, top: 4.0, right: 20.0, bottom: 0.0),
      child: SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: onTap,
          child: SizedBox(
            height: 80.0,
            child: Card(
                color: selected ? Colors.grey : Theme.of(context).primaryColor,
                //Colors.primaries[item.rosaryCount % Colors.primaries.length],
                child: Container(
                    child: Row(
                  children: <Widget>[
                    Expanded(
                        flex: 2,
                        child: Container(
                            margin: const EdgeInsets.all(20.0),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 4.0, top: 6.0, bottom: 6.0, right: 4.0),
                              //child: authorField(context, item),
                              child: Text(
                                item.author,
                                style: TextStyle(
                                    color: Theme.of(context).backgroundColor,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w900),
                              ),
                            ))),
                    Expanded(
                        flex: 1,
                        child: Container(
                            margin: const EdgeInsets.all(1.0),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 2.0, top: 6.0, bottom: 6.0, right: 6.0),
                              child: rosaryCountField(context, item),
                            ))),
                    Flexible(
                        flex: 1,
                        child: Container(
                            margin: const EdgeInsets.all(1.0),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 0.0,
                                  top: 6.0,
                                  bottom: 6.0,
                                  right: 15.0),
                              child: offerRosaryButton(context, item),
                            ))),
                  ],
                ))),
          ),
        ),
      ),
    );
  }

  Widget authorField(BuildContext context, RosaryCount item) {
    final rosaryBloc = RosaryProvider.of(context);
    //TextEditingController _controller = new TextEditingController();
    return StreamBuilder(
      stream: rosaryBloc.author,
      builder: (context, snapshot) {
        try {
          return TextField(
            onChanged: rosaryBloc.changeAuthor,
            keyboardType: TextInputType.text,
            maxLines: 1,
            autocorrect: true,
            //controller: _controller,
            style: Theme.of(context).textTheme.subhead,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(6.0),
              fillColor: Colors.white,
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black26, width: 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(1.0))),
              hintText: "Name",
              errorText: snapshot.error,
            ),
          );
        } catch (e) {
          return Container();
        }
      },
    );
  }

  Widget rosaryCountField(BuildContext context, RosaryCount item) {
    final rosaryBloc = RosaryProvider.of(context);
    //TextEditingController _controller = new TextEditingController();
    return StreamBuilder(
      stream: rosaryBloc.rosaryCount,
      builder: (context, snapshot) {
        try {
          return Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: TextField(
                keyboardType: TextInputType.number,
                onChanged: rosaryBloc.changeRosaryCount,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18.0,
//                    height: 0.05,
                  color: Colors.black,
                ),
                cursorColor: Color.fromRGBO(11, 129, 140, 1.0),
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: BorderSide(color: Colors.white)),
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(color: Colors.white)),
//          autocorrect: true,

//          keyboardType: TextInputType.number,
//          maxLines: 1,
//          //controller: _controller,
//          style: Theme.of(context).textTheme.subhead,
//          decoration: InputDecoration(
//            fillColor: Colors.white30,
//            contentPadding: const EdgeInsets.all(6.0),
//            border: OutlineInputBorder(
//                borderSide: BorderSide(color: Colors.black, width: 1.0),
//                borderRadius: BorderRadius.all(Radius.circular(1.0))),
                  hintText: "0",
                  //labelText: item.totalCount.toString(),
                  contentPadding:
                      EdgeInsets.only(left: 5.0, right: 5.0, top: 10.0),
                  errorText: snapshot.error,
                  errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(color: Colors.white)),
                  errorStyle: TextStyle(
                      color: Theme.of(context).backgroundColor,
                      fontSize: 10.0,
                      height: 0),
                ),
              ));
        } catch (e) {
          return Container();
        }
      },
    );
  }

  Widget offerRosaryButton(BuildContext context, RosaryCount item) {
    RosaryBloc rosaryBloc = RosaryProvider.of(context);
    UserBloc userBloc = UserProvider.of(context);

    var container = AppStateContainer.of(context);

    return StreamBuilder(
      stream: rosaryBloc.submitValidRosaryListCount,
      builder: (context, snapshot) {
        try {
          return Container(
              color: Theme.of(context).backgroundColor,
              margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
              padding: EdgeInsets.only(left: 1.0, right: 1.0),
              child: IconButton(
                padding: EdgeInsets.only(
                    top: 2.0, bottom: 2.0, left: 0.0, right: 0.0),
                color: Theme.of(context).backgroundColor,
                splashColor: Theme.of(context).primaryColorLight,
                icon: Icon(
                  Icons.arrow_forward,
                  color: Theme.of(context).primaryIconTheme.color,
                ),
//                ImageIcon(AssetImage("assets/cross-icon.png"),
//                    color: Colors.white),
                onPressed: () async {
                  if (snapshot.hasData) {
                    try {
                      debugPrint(
                          "Rosary Author from Rosary List: " + item.author);

                      if (connectionStatus) {
                        rosaryBloc.updateRosaryListEntry(
                            item, container.state.userDetails);

//                        var dialog = buildDialog(
//                          title: "Rosaries Offered, is saved",
//                          message: "Add More Rosary Contributions ?",
//                          confirm: "Yes",
//                          confirmFn: () {
//                            Navigator.of(context, rootNavigator: true).pop();
//                          },
//                          cancel: "No",
//                          cancelFn: () {
//                            Navigator.of(context, rootNavigator: true).pop();
//                            Navigator.pop(context);
//                          },
//                        );

//                        isBadge
//                            ? null
//                            : showDialog(context: context, child: dialog);

                        showError(
                            context: context,
                            errorMsg: MemError(
                                title: "Rosary Added",
                                body: "Thanks for offering rosary"));
                      } else {
                        showError(
                            context: context,
                            errorMsg: MemError(
                                title: "Not Connected",
                                body:
                                    "Please check your internet connection. You can submit Rosary only when you are online."));
                      }

//                rosaryBloc.rosaryRepository
//                    .rosaryListEntryExists(item.toEntity())
//                    .then((entryExists) {
//                  entryExists
//                      ? rosaryBloc.updateRosaryListEntry(
//                          item, container.state.userDetails)
//                      : rosaryBloc
//                          .addRosaryListEntry(container.state.userDetails);
//
//
//
//
//                  //messageBloc.addRosary(userDetails)
//                });

                    } catch (e) {
                      debugPrint("error message : " + e.toString());
                    }
                  }

                  ///todo: the appropriate implementation needs to be introduced. Its a quick fix now.
                },
              ));
        } catch (e) {
          return Container();
        }
      },
    );
  }
}
