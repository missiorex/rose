// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utility/utility.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/widgets/RestartWidget.dart';

class UserActiveScreen extends StatelessWidget {
  UserActiveScreen({
    Key key,
  }) : super(key: MemKeys.giftVerseScreen);

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = UserProvider.of(context);
    MessageBloc messageBloc = MessageProvider.of(context);
    String latestBadgeID;

    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.close,
                color: Colors.black87,
              ),
              onPressed: () {
                Navigator.pop(context);
                RestartWidget.restartApp(context);
              },
            );
          },
        ),
        elevation: 0.0,
//        backgroundColor:  Colors.transparent,
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: Colors.white,
      body: Material(
          //color: Color.fromARGB(0, 254, 246, 226),
          color: Color.fromARGB(0, 254, 246, 226),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 20.0, left: 40.0),
                  width: 280.0,
                  height: 250.0,
                  decoration: new BoxDecoration(
                    color: Theme.of(context).primaryColorLight,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black26,
                          offset: new Offset(1.0, 1.0),
                          blurRadius: 0.3,
                          spreadRadius: 0.3)
                    ],
                    borderRadius:
                        new BorderRadius.all(const Radius.circular(5.0)),
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(
                      top: 10.0,
                    ),
                    child: Center(
                        child: Text(
                      "Welcome, " +
                          userBloc.userDetails.title +
                          " " +
                          userBloc.userDetails.name,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Color.fromRGBO(11, 129, 140, 1.0)),
                    ))),
                Container(
                  padding: EdgeInsets.only(
                      top: 10.0,
                      left: MediaQuery.of(context).size.width * 0.1,
                      right: MediaQuery.of(context).size.width * 0.1),
                  child: Center(
                      child: Text(
                    "Your account is active now. \n Press enter to continue.",
                    style: TextStyle(
                        color: Colors.black87,
                        fontWeight: FontWeight.w500,
                        fontSize: 15.0),
                    textAlign: TextAlign.center,
                  )),
                ),
                Container(
                    padding: EdgeInsets.only(
                      top: 10.0,
                    ),
                    child: Center(
                        child: FlatButton(
                            padding: EdgeInsets.only(
                                top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
                            color: Color.fromRGBO(11, 129, 140, 1.0),
                            splashColor: Colors.deepOrange,
                            onPressed: () {
                              RestartWidget.restartApp(context);
                              Navigator.pop(context);
                            },
                            child: Text(
                              "ENTER",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )))),
                Container(
                    padding: EdgeInsets.only(
                      top: 40.0,
                    ),
                    child: Center(
                        child: Text(
                      "MARIAN EUCHARISTIC MINISTRY",
                      style: TextStyle(
                          fontSize: 15.0,
                          //fontWeight: FontWeight.bold,
                          fontFamily: "Impact",
                          color: Color.fromRGBO(41, 147, 178, 1.0)),
                    ))),
              ])),
    );
  }
}
