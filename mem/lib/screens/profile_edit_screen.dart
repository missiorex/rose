import 'package:flutter/material.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:utility/utility.dart';
import 'package:flutter/services.dart';
import 'package:mem_models/mem_models.dart';
import 'package:memapp/app_state_container.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:base_helpers/src/dialogshower.dart' as DialogShower;
import 'package:intl/intl.dart';
import 'dart:async';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class ProfileEditScreen extends StatefulWidget {
  final FirebaseStorage storage;
  final MemUser member;
  final UserBloc userBloc;
  ProfileEditScreen({Key key, this.storage, this.member, this.userBloc})
      : super(key: MemKeys.signupScreen);

  @override
  State createState() => ProfileEditState();
}

class ProfileEditState extends State<ProfileEditScreen>
    with SingleTickerProviderStateMixin {
  AppState appState;
  SignState _signState = SignState.SIGNUP;
  String _signin = "Sign In";
  String _signup = "Sign Up";
  String _alreadyHaveAccount = "Already have account?";
  String _haveNoAccount = "Have no account?";
  bool needAddProfile = true;
  final nameController = TextEditingController();
  final displayNameController = TextEditingController();
  int _currentStep = 0;
  TabController _controller;
  List<String> regions;
  List<String> locales;
  List<String> _titles;
  String dropdownValue;
  final GlobalKey<FormState> _editProfileFormKey = GlobalKey<FormState>();
  MemUser userDetails = MemUser();
  int _radioValue = -1;
  File _image;
  String selectedLocale;
  String selectedRegion;
  String selectedTitle;
  String selectedName;
  String selectedDisplayName;

  @override
  void initState() {
    super.initState();

    widget.userBloc.userProfile(widget.member.id).then((registeredUser) {
      setState(() {
        selectedLocale = registeredUser.locale;
        selectedRegion = registeredUser.region;
        selectedTitle = registeredUser.title;
        selectedName = registeredUser.name;
        selectedDisplayName = registeredUser.displayName;
        nameController.text = selectedName;
        displayNameController.text = selectedDisplayName;
      });
    });

    _controller = TabController(length: 2, vsync: this);
    _controller.addListener(() {
      setState(() {});
    });

    //dropdownValue = regions.first;
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    nameController.dispose();
    displayNameController.dispose();
    _dateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
      onWillPop: () async => true,
      child: Scaffold(
        appBar: AppBar(
            title: Text("Update Profile"),
            elevation: 0.0,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, 0),
                  end: Alignment(
                      1.0, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    Theme.of(context).primaryColorDark,
                    Theme.of(context).backgroundColor
                  ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
            )),
        body: Container(
            color: Theme.of(context).primaryColorLight, child: content()),
      ));

  final TextEditingController _dateController = TextEditingController();
  Future _chooseDate(BuildContext context, String initialDateString) async {
    var now = DateTime.now();
    var initialDate = convertToDate(initialDateString) ?? now;
    initialDate = (initialDate.year >= 1900 && initialDate.isBefore(now)
        ? initialDate
        : now);

    var result = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: DateTime(1900),
        lastDate: DateTime.now());

    if (result == null) return;

    setState(() {
      _dateController.text = DateFormat.yMd().format(result);
    });
  }

  DateTime convertToDate(String input) {
    try {
      var d = DateFormat.yMd().parseStrict(input);
      return d;
    } catch (e) {
      return null;
    }
  }

  @override
  Widget content() {
    var container = AppStateContainer.of(context);
    var userBloc = UserProvider.of(context);
    appState = container.state;

    regions = container.state.regions ??
        <String>[
          'Asia',
          'Australia',
          'Africa',
          'Europe',
          'North America',
          'South America',
          'Antarctica'
        ];

    locales = container.state.locales ?? <String>['English', 'Malayalam'];

    _titles = <String>[
      '',
      'Mr.',
      'Mrs.',
      'Miss.',
      'Fr.',
      'Sr.',
      'Rev.',
      'Mar.',
      'Bishop.',
      'Arch Bishop.',
      'Cardinal.'
    ];

    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.only(top: 24.0),
        padding: const EdgeInsets.only(top: 10.0),
        child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _editProfileFormKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                titleField(userBloc),
                nameField(),
                displayNameField(),
                localeField(userBloc),
                userBloc.userDetails.role == "admin"
                    ? regionField(userBloc)
                    : Container(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[profileSubmitButton(userBloc)],
                ),
              ],
            )));
  }

  Widget nameField() {
    var textStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).backgroundColor,
        fontWeight: FontWeight.w600,
        fontSize: 16.0);
    return Container(
        color: Colors.white,
        margin: EdgeInsets.only(top: 10.0, left: 30.0, right: 30.0),
        child: TextFormField(
          style: textStyle,
          decoration: InputDecoration(
            hintText: 'Name',
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                borderSide: BorderSide(color: Theme.of(context).primaryColor)),
            contentPadding: EdgeInsets.all(12.0),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                borderSide:
                    BorderSide(color: Theme.of(context).backgroundColor)),
            errorStyle: TextStyle(
                fontSize: 10.0, color: Theme.of(context).backgroundColor),
          ),
          controller: nameController,
          inputFormatters: [LengthLimitingTextInputFormatter(30)],
//          validator: (val) => val.isEmpty ? 'Name is required' : null,
          onSaved: (val) => setState(() {
            selectedName = val;
          }),
        ));
  }

  Widget titleField(UserBloc bloc) {
    var textStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).backgroundColor,
        fontWeight: FontWeight.w600,
        fontSize: 15.0);

    return StreamBuilder(
      stream: bloc.title,
      builder: (context, snapshot) {
        return Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 10.0, left: 30.0, right: 30.0),
            child: Row(children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(
                        left: 10.0,
                        right: 20.0,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                              alignedDropdown: false,
                              child: DropdownButton<String>(
                                  hint: Text("Title", style: textStyle),
                                  value: selectedTitle,
                                  onChanged: (String changedValue) {
                                    bloc.changeTitle(changedValue);
                                    setState(() {
                                      selectedTitle = changedValue;
                                    });
                                  },
                                  style: textStyle,
                                  items: _titles.map((String value) {
                                    return DropdownMenuItem<String>(
                                        value: value, child: Text(value));
                                  }).toList())))))
            ]));
      },
    );
  }

  Widget localeField(UserBloc bloc) {
    var container = AppStateContainer.of(context);
    var textStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).backgroundColor,
        fontWeight: FontWeight.w600,
        fontSize: 15.0);

    this.locales = container.state.locales ?? <String>['English', 'Malayalam'];

    return StreamBuilder(
      stream: bloc.locale,
      builder: (context, snapshot) {
        return Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 10.0, left: 30.0, right: 30.0),
            child: Row(children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(
                        left: 10.0,
                        right: 20.0,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                              alignedDropdown: false,
                              child: DropdownButton<String>(
                                  hint: Text("Language", style: textStyle),
                                  value: selectedLocale,
                                  style: textStyle,
                                  onChanged: (String changedValue) {
                                    bloc.changeLocale(changedValue);
                                    setState(() {
                                      selectedLocale = changedValue;
                                    });
                                  },
                                  items: locales.map((String value) {
                                    return DropdownMenuItem<String>(
                                        value: value, child: Text(value));
                                  }).toList())))))
            ]));
      },
    );
  }

  Widget regionField(UserBloc bloc) {
    var container = AppStateContainer.of(context);
    var textStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).backgroundColor,
        fontWeight: FontWeight.w600,
        fontSize: 15.0);

    this.regions = container.state.regions ??
        <String>[
          'Asia',
          'Australia',
          'Africa',
          'Europe',
          'North America',
          'South America',
          'Antarctica'
        ];
    debugPrint("Regions at field:  " + regions.toString());

    return StreamBuilder(
      stream: bloc.region,
      builder: (context, snapshot) {
        return Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 10.0, left: 30.0, right: 30.0),
            child: Row(children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                      padding: EdgeInsets.only(
                        left: 10.0,
                        right: 20.0,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                              alignedDropdown: false,
                              child: DropdownButton<String>(
                                  hint: Text("Region", style: textStyle),
                                  value: selectedRegion,
                                  style: textStyle,
                                  onChanged: (String changedValue) {
                                    bloc.changeRegion(changedValue);
                                    setState(() {
                                      selectedRegion = changedValue;
                                    });
                                  },
                                  items: regions.map((String value) {
                                    return DropdownMenuItem<String>(
                                        value: value, child: Text(value));
                                  }).toList())))))
            ]));
      },
    );
  }

  Widget genderField() {
    return Container(
        color: Colors.white,
        margin: EdgeInsets.only(top: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(left: 10.0),
                child: Text(
                  "Gender",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18.0,
                    color: Colors.black54,
                  ),
                )),
            Radio(
              value: 0,
              activeColor: Color.fromRGBO(11, 129, 140, 1.0),
              groupValue: _radioValue,
              onChanged: _handleRadioValueChange,
            ),
            Text('Female'),
            Radio(
              value: 1,
              activeColor: Color.fromRGBO(11, 129, 140, 1.0),
              groupValue: _radioValue,
              onChanged: _handleRadioValueChange,
            ),
            Text('Male'),
          ],
        ));
  }

  Widget dobField() {
    return Container(
        margin: EdgeInsets.only(bottom: 10.0),
        color: Colors.white,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                  child: TextFormField(
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18.0,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  hintText: 'mm/dd/yyyy',
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(217, 178, 80, 1.0))),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(
                          color: Color.fromRGBO(217, 178, 80, 1.0))),
                  errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      borderSide: new BorderSide(color: Colors.white)),
                  errorStyle: TextStyle(fontSize: 10.0, color: Colors.grey),
                  contentPadding: EdgeInsets.all(12.0),
                ),
                controller: _dateController,
                keyboardType: TextInputType.datetime,
                validator: (val) => isValidDob(val) ? null : 'Not a valid date',
                onSaved: (val) => userDetails.birthday = convertToDate(val),
              )),
              IconButton(
                icon: Icon(Icons.more_horiz),
                tooltip: 'Choose date',
                onPressed: (() {
                  _chooseDate(context, _dateController.text);
                }),
              )
            ]));
  }

  Widget displayNameField() {
    var textStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).backgroundColor,
        fontWeight: FontWeight.w600,
        fontSize: 15.0);

    return Container(
        margin: EdgeInsets.only(top: 10.0, left: 30.0, right: 30.0),
        color: Colors.white,
        child: TextFormField(
          style: textStyle,
          decoration: InputDecoration(
              hintText: 'Display Name',
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  borderSide:
                      BorderSide(color: Theme.of(context).primaryColor)),
              contentPadding: EdgeInsets.all(12.0),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  borderSide:
                      BorderSide(color: Theme.of(context).backgroundColor)),
              errorStyle: TextStyle(
                  fontSize: 10.0, color: Theme.of(context).backgroundColor)),
          inputFormatters: [LengthLimitingTextInputFormatter(15)],
          controller: displayNameController,
          //validator: (val) => val.isEmpty ? 'Dispaly Name is required' : null,
          onSaved: (val) => selectedDisplayName = val,
        ));
  }

  Widget profileSubmitButton(UserBloc userBloc) {
    var textStyle = Theme.of(context).textTheme.subtitle2.copyWith(
        color: Theme.of(context).primaryIconTheme.color,
        fontWeight: FontWeight.w600,
        fontSize: 15.0);

    return Container(
        width: MediaQuery.of(context).size.width * 0.5,
        margin: EdgeInsets.only(top: 30.0),
        padding:
            EdgeInsets.only(top: 0.0, bottom: 10.0, left: 30.0, right: 20.0),
        child: MaterialButton(
            padding:
                EdgeInsets.only(top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
            color: Theme.of(context).buttonColor,
            splashColor: Theme.of(context).primaryColor,
            child: Text(
              "Update",
              style: textStyle,
            ),
            onPressed: () {
              final FormState form = _editProfileFormKey.currentState;

              if (!form.validate()) {
                debugPrint('Some Details are missing or incorrect');
                var dialog = DialogShower.buildDialog(
                    title: "Some Details are missing or incorrect",
                    message: """Please enter all the required details.""",
                    confirm: "OK",
                    confirmFn: () {
                      Navigator.pop(context);
                    });

                showDialog(
                    context: context,
                    builder: (context) {
                      return dialog;
                    });
              } else if (widget.member.id != null) {
                form.save(); //This invokes each onSaved event

                userBloc.userProfile(widget.member.id).then((registeredUser) {
                  //debugPrint("Locale: " + selectedLocale ?? "");
                  registeredUser.locale =
                      selectedLocale ?? registeredUser.locale;
                  registeredUser.region =
                      selectedRegion ?? registeredUser.region;
                  registeredUser.title = selectedTitle ?? registeredUser.title;
                  registeredUser.name = selectedName ?? registeredUser.name;
                  registeredUser.displayName =
                      selectedDisplayName ?? registeredUser.displayName;

                  userBloc.userRepository.updateUser(registeredUser.toEntity());

                  var dialog = DialogShower.buildDialog(
                      title: "Your Profile is updated",
                      message: """The details you entered are updated""",
                      confirm: "OK",
                      confirmFn: () {
                        Navigator.pop(context);
                        _controller.animateTo(0);
                        Navigator.pop(context);
                      });

                  showDialog(
                      context: context,
                      builder: (context) {
                        return dialog;
                      });
                });
              } else {
                debugPrint('Some issue in retreiveing profile info');
                var dialog = DialogShower.buildDialog(
                    title: "Some issue in retreiveing profile info",
                    message: """Some issue in retreiveing profile info""",
                    confirm: "OK",
                    confirmFn: () {
                      Navigator.pop(context);
                      _controller.animateTo(0);
                    });

                showDialog(
                    context: context,
                    builder: (context) {
                      return dialog;
                    });
              }
            }));
  }

  bool isValidDob(String dob) {
    if (dob.isEmpty) return false;
    var d = convertToDate(dob);
    return d != null &&
        d.isBefore(DateTime.now().subtract(Duration(days: 3650)));
  }

  void _toggleSignState() {
    setState(() {
      _signState =
          _signState == SignState.SIGNUP ? SignState.SIGNIN : SignState.SIGNUP;
    });
  }

  String getLabel() {
    return _signState == SignState.SIGNIN ? _signin : _signup;
  }

  void _userNotActive(User user) {
    var dialog = DialogShower.buildDialog(
      title: "Your Account is not active",
      message: """
              
            A verification mail sent to the registered email id. Please click the link in the mail to verify your email account.  
        
            You will be receiving a call from the MEM team.Your account will be activated after verifying authenticity.Thanks for your patience.
            
            Please fill in your profile details.
            """,
      confirm: "Ok",
      confirmFn: () {
        Navigator.pop(context);
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          userDetails.gender = "F";
          break;
        case 1:
          userDetails.gender = "M";
          break;
      }
    });
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    debugPrint("Error:$message");
//    authScaffoldKey.currentState.showSnackBar(
//        new SnackBar(backgroundColor: color, content: new Text(message)));
  }

  Future<String> _uploadFile(File _image, String uid) async {
//    final Directory systemTempDir = Directory.systemTemp;
//    final File file = await new File('${systemTempDir.path}/uId').create();
//    file.writeAsString(kTestString);
//    assert(await file.readAsString() == kTestString);
//    final String rand = "${new Random().nextInt(10000)}";
    final Reference ref =
        widget.storage.ref().child('profile').child(_image.path);
    final UploadTask uploadTask = ref.putFile(
      _image,
      SettableMetadata(
          customMetadata: <String, String>{'iamgetype': 'profile'},
          contentType: 'image/jpeg'),
    );

    //final Uri downloadUrl = (await uploadTask.future).downloadUrl;
    final String downloadUrl = await ref.getDownloadURL();

    return downloadUrl;
  }

  Future<File> saveImageLocal(String uid) async {
    var pickedImage = await ImagePicker().getImage(source: ImageSource.camera);
    // getting a directory path for saving
    var image = File(pickedImage.path);
    final String path = await _localPath;

    // copy the file to a new path
    final File newImage = await image.copy('$path/' + uid + '.jpeg');

    setState(() {
      _image = image;
    });

    return _image;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/counter.txt');
  }

  void showError({MemError errorMsg}) {
    var dialog = DialogShower.buildDialog(
      title: errorMsg.title,
      message: errorMsg.body,
      confirm: "OK",
      confirmFn: () {
//        Navigator.of(context).pushNamedAndRemoveUntil(
//            "/splash", (Route<dynamic> route) => false);
      },
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }

  void showSnackMessage(BuildContext context, String message,
      [MaterialColor color = Colors.lightBlue]) {
    debugPrint("Error:$message");
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: color,
      content: Text(message),
      duration: Duration(seconds: 1),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          Scaffold.of(context).removeCurrentSnackBar();
          // Some code to undo the change!
        },
      ),
    ));
  }
}
