import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:utility/utility.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:memapp/app_state_container.dart';
import 'package:memapp/widgets/failed_rosary_list.dart';
import 'package:intl/intl.dart';

final List<FacingPage> _popUpPages = <FacingPage>[
  FacingPage(
      label: 'Add Gospel Card',
      fabLabel: 'Add Gosepl Card',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.add,
      popUpElement: true),
  FacingPage(
      label: 'Members List',
      fabLabel: 'Members List',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
  FacingPage(
      label: 'Rosary Dashboard',
      fabLabel: 'Rosary Dashboard',
      colors: Colors.red,
      tabIconData: Icons.accessibility,
      fabIconData: Icons.playlist_add,
      popUpElement: true),
];

class RosaryDashBoardScreen extends StatefulWidget {
  final bool connectionStatus;
  final RouteObserver<PageRoute> routeObserver;

  RosaryDashBoardScreen({Key key, this.connectionStatus, this.routeObserver})
      : super(key: MemKeys.adminScreen);
  @override
  _RosaryDashBoardScreenState createState() => _RosaryDashBoardScreenState();
}

class _RosaryDashBoardScreenState extends State<RosaryDashBoardScreen> {
  final TextEditingController _dateController = TextEditingController();
  final GlobalKey<FormState> _pendingRosaryFormKey = GlobalKey<FormState>();
  int selectedDate = 0;
  int pendingRosaryCount = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _dateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget content;

    var container = AppStateContainer.of(context);
    MessageBloc msgBloc = MessageProvider.of(context);

    content = Container(
        color: Theme.of(context).primaryColorLight,
        child: TabBarView(children: [
//          Form(
//              autovalidate: true,
//              key: _pendingRosaryFormKey,
//              child: Column(children: <Widget>[
//                Container(
//                  color: Colors.brown,
//                  child: Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: startDateField(),
//                  ),
//                ),
//                Expanded(
//                    child: SizedBox(
//                  height: MediaQuery.of(context).size.height * 0.9,
//                  child: Text(pendingRosaryCount.toString()),
//                )),
//                Expanded(child: startDateSubmitButton())
//              ])),
          Column(
//                    key: MemKeys.testimonyTab,
            //modified
            children: <Widget>[
              FailedRosaryList(
                filteredMessages: container.state.rosaryMessages,
                messageBloc: msgBloc,
                bgImageUrl: "assets/chat_bg.png",
                connectionStatus: widget.connectionStatus,
                msgType: "pendingRosaries",
                routeObserver: widget.routeObserver,
              )
            ], //new
          )
        ]));

    return DefaultTabController(
        length: 1,
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, 0),
                  end: Alignment(
                      1.0, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    Theme.of(context).primaryColorDark,
                    Theme.of(context).backgroundColor
                  ], // whitish to gray
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
            ),
            bottom: TabBar(
              indicatorColor: Color.fromRGBO(217, 178, 80, 1.0),
              labelStyle: TextStyle(fontSize: 8.0),
              labelPadding: EdgeInsets.only(right: 3.0),
              tabs: [
//                Tab(
//                  icon: Icon(Icons.date_range),
//                  text: "Pending Rosaries",
//                ),
                Tab(
                  icon: Icon(Icons.cached),
                  text: "Pending Rosary Messages",
                ),
              ],
            ),
            title: Text('Rosary Dashboard'),
          ),
          body: content,
        ));
  }

  Widget startDateField() {
    return Container(
        margin: EdgeInsets.only(bottom: 10.0),
        color: Colors.white,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <
            Widget>[
          Expanded(
              child: TextFormField(
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18.0,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    hintText: 'mm/dd/yyyy',
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        borderSide: BorderSide(
                            color: Color.fromRGBO(217, 178, 80, 1.0))),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        borderSide: new BorderSide(
                            color: Color.fromRGBO(217, 178, 80, 1.0))),
                    errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        borderSide: new BorderSide(color: Colors.white)),
                    errorStyle: TextStyle(fontSize: 10.0, color: Colors.grey),
                    contentPadding: EdgeInsets.all(12.0),
                  ),
                  controller: _dateController,
                  keyboardType: TextInputType.datetime,
                  validator: (val) =>
                      isValidDate(val) ? null : 'Not a valid date',
                  onSaved: (val) {
                    selectedDate = convertToDate(val).millisecondsSinceEpoch;
                    debugPrint(selectedDate.toString());

                    setState(() {});
                  })),
          IconButton(
            icon: Icon(Icons.more_horiz),
            tooltip: 'Choose date',
            onPressed: (() {
              _chooseDate(context, _dateController.text);
            }),
          )
        ]));
  }

  Widget startDateSubmitButton() {
    MessageBloc msgBloc = MessageProvider.of(context);

    return Container(
        margin: EdgeInsets.only(top: 30.0),
        padding:
            EdgeInsets.only(top: 0.0, bottom: 10.0, left: 30.0, right: 20.0),
        child: FlatButton(
          padding:
              EdgeInsets.only(top: 2.0, bottom: 2.0, left: 20.0, right: 20.0),
          color: Color.fromRGBO(11, 129, 140, 1.0),
          splashColor: Colors.deepOrange,
          child: Text(
            "Get Count",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () async {
            final FormState form = _pendingRosaryFormKey.currentState;

            if (!form.validate()) {
              debugPrint('Some Details are missing or incorrect');
//              var dialog = DialogShower.buildDialog(
//                  title: "Some Details are missing or incorrect",
//                  message: """Please enter all the required details.""",
//                  confirm: "OK",
//                  confirmFn: () {
//                    Navigator.pop(context);
//                  });
//
//              showDialog(
//                  context: context,
//                  builder: (context) {
//                    return dialog;
//                  });
            } else {
              form.save(); //This invokes each onSaved event

              msgBloc.messageRepository
                  .getTotalIncompleteRosaryCount(selectedDate)
                  .then((pendingRosaryCount) {
                setState(() {
                  debugPrint(
                      "pending rosaies = " + pendingRosaryCount.toString());
                });
              });

//              var dialog = DialogShower.buildDialog(
//                  title: "Your Profile is updated",
//                  message: """The details you entered are updated""",
//                  confirm: "",
//                  confirmFn: () {});
//
//              showDialog(
//                  context: context,
//                  builder: (context) {
//                    return dialog;
//                  });

              //Navigator.pop(context);
            }
          },
        ));
  }

  Future _chooseDate(BuildContext context, String initialDateString) async {
    var now = DateTime.now();
    var initialDate = convertToDate(initialDateString) ?? now;
    initialDate = (initialDate.year >= 1900 && initialDate.isBefore(now)
        ? initialDate
        : now);

    var result = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: DateTime(1900),
        lastDate: DateTime.now());

    if (result == null) return;

    setState(() {
      _dateController.text = DateFormat.yMd().format(result);
    });
  }

  DateTime convertToDate(String input) {
    try {
      var d = DateFormat.yMd().parseStrict(input);

      return d;
    } catch (e) {
      return null;
    }
  }

  bool isValidDate(String dob) {
    if (dob.isEmpty) return false;
    var d = convertToDate(dob);
    return d != null && d.isAfter(DateTime.now().subtract(Duration(days: 365)));
  }
}
