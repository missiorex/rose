import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:mem_models/mem_models.dart';
import 'package:utility/utility.dart';
import 'dart:async';
import 'package:flutter/material.dart';

class APIService {
  APIService._instantiate();

  static final APIService instance = APIService._instantiate();

  final String _baseUrl = 'www.googleapis.com';
  String _nextPageToken = '';

  Future<YoutubeChannel> fetchChannel({String channelId}) async {
    Map<String, String> parameters = {
      'part': 'snippet, contentDetails, statistics',
      'id': channelId,
      'key': MemKeys.Youtube_API_KEY,
    };
    Uri uri = Uri.https(
      _baseUrl,
      '/youtube/v3/channels',
      parameters,
    );
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    // Get Channel
    var response = await http.get(uri, headers: headers);
    if (response.statusCode == 200) {
      Map<String, dynamic> data = json.decode(response.body)['items'][0];
      YoutubeChannel channel = YoutubeChannel.fromMap(data);

      //To reset next page token if videos list is null (screen open)
      channel.videos == null
          ? _nextPageToken = ''
          : _nextPageToken = _nextPageToken;

      // Fetch first batch of videos from uploads playlist
      channel.videos = await fetchVideosFromPlaylist(
        playlistId: channel.uploadPlaylistId,
      );
      //Fetch the latest videos
//      channel.videos = await fetchLatestVideos(
//        channelId: channel.id,
//      );
      return channel;
    } else {
      throw json.decode(response.body)['error']['message'];
    }
  }

  Future<List<YoutubeVideo>> fetchLatestVideos({String channelId}) async {
    Map<String, String> parameters = {
      'part': 'snippet',
      'channelId': channelId,
      'maxResults': '8',
      'order': 'date',
      'type': 'video',
      'key': MemKeys.Youtube_API_KEY,
    };
    Uri uri = Uri.https(
      _baseUrl,
      '/youtube/v3/search',
      parameters,
    );
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    // Get Latest Videos
    var response = await http.get(uri, headers: headers);
    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      _nextPageToken = data['nextPageToken'] ?? '';
      List<dynamic> videosJson = data['items'];

      // Fetch first eight videos from uploads playlist
      List<YoutubeVideo> videos = [];
      videosJson.forEach(
        (json) => videos.add(
          YoutubeVideo.fromMap(json['snippet']),
        ),
      );
      return videos;
    } else {
      throw json.decode(response.body)['error']['message'];
    }
  }

  Future<List<YoutubeVideo>> fetchVideosFromPlaylist(
      {String playlistId}) async {
    Map<String, String> parameters = {
      'part': 'snippet',
      'playlistId': playlistId,
      'maxResults': '8',
      'pageToken': _nextPageToken,
      'key': MemKeys.Youtube_API_KEY,
    };
    Uri uri = Uri.https(
      _baseUrl,
      '/youtube/v3/playlistItems',
      parameters,
    );
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    // Get Playlist Videos
    var response = await http.get(uri, headers: headers);
    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      _nextPageToken = data['nextPageToken'] ?? '';
      List<dynamic> videosJson = data['items'];

      debugPrint("Video json: " + videosJson.toString());

      // Fetch first eight videos from uploads playlist
      List<YoutubeVideo> videos = [];
      videosJson.forEach(
        (json) => videos.add(
          YoutubeVideo.fromMap(json['snippet']),
        ),
      );
      return videos;
    } else {
      throw json.decode(response.body)['error']['message'];
    }
  }
}
