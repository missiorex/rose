package org.memworld.memapp
import androidx.core.content.FileProvider;
import android.content.Intent;
import android.net.Uri;
import java.io.File;
import androidx.annotation.NonNull;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;


class MainActivity: FlutterActivity() {

  override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine){
    GeneratedPluginRegistrant.registerWith(flutterEngine);

     MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), "channel:org.memworld.memapp/share").setMethodCallHandler { methodCall, _ ->
      if (methodCall.method == "shareFile") {
        shareFile(methodCall.arguments as String)
      }
    }

  }

    private fun shareFile(path:String) {
      val imageFile = File(this.applicationContext.cacheDir,path)
      val contentUri = FileProvider.getUriForFile(this,"org.memworld.memapp",imageFile)

      val shareIntent = Intent()
      shareIntent.action = Intent.ACTION_SEND
      shareIntent.type="image/jpg"
      shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
      startActivity(Intent.createChooser(shareIntent,"Share Using"))
    }
}


