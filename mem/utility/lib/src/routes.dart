// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

class MemRoutes {
  static final home = "/home";
  static final splash = "/splash";
  static final addMessage = "/addmessage";
  static final addRosary = "/addrosary";
  static final addRequest = "/addrequest";
  static final addTestimony = "/addtestimony";
  static final auth = "/auth";
  static final signup = "/signup";
  static final emailSignup = "/emailsignup";
  static final phoneSignup = "/phonesignup";
  static final signin = "/signin";
  static final rosaryList = "/rosarylist";
  static final memberList = "/memberList";
  static final addVideoTestimony = "/vtestimony";
  static final community = "/community";
  //Campaign
  static final addCampaign = '/addCampaign';
  static final campaign = '/campaign';
  static final campaignHome = '/campaignhome';
  static final notification = '/notification';
}
