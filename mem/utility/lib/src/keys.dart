// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/widgets.dart';

class MemKeys {
  // Home Screens
  static final homeScreen = const Key('__homeScreen__');
  static final addMessageFab = const Key('__addMessageFab__');
  static final snackbar = const Key('__snackbar__');
  static Key snackbarAction(String id) => Key('__snackbar_action_${id}__');
  static Key unreadCountButton = const Key('__unread_count_button__');
  static final drawer = const Key('__sideDrawer__');

  // Splash Screen
  static final splashScreen = const Key('__splashScreen__');
  static final authScreen = const Key('__authScreen__');

  //Signup Screen
  static final signupScreen = const Key('__signUpScreen__');
  static final emailSignupScreen = const Key('__emailSignUpScreen__');
  static final phoneSignupScreen = const Key('__phoneSignUpScreen__');
  static final signinScreen = const Key('__signInScreen__');
  static final resetPwdScreen = const Key('__ResetPasswordScreen__');
  static final profileScreen = const Key('__ProfileScreen__');
  static final profileEditScreen = const Key('__ProfileEditScreen__');
  static final memberList = const Key('__MemberListScreen__');
  static final adminScreen = const Key('__AdminScreen__');
  static final notificationScreen = const Key('__NotificationScreen__');

  static final messageList = const Key('__messageList__');
  static final rosaryList = const Key('__rosaryList__');
  static final messagesLoading = const Key('__messagesLoading__');
  static final messageItemWidget = const Key('__messageItemWidget__');
  static final messageItem = (String id) => Key('MessageItem__${id}');
  static final messageItemCheckbox =
      (String id) => Key('MessageItem__${id}__Checkbox');
  static final messageItemTitle =
      (String id) => Key('MessageItem__${id}__Title');
  static final messageItemBody = (String id) => Key('MessageItem__${id}__Body');

  // Tabs
  static final tabs = const Key('__tabs__');
  static final messageTab = const Key('__messageTab__');
  static final homeTab = const Key('__homeTab__');
  static final requestTab = const Key('__requestTab__');
  static final testimonyTab = const Key('__testimonyTab__');

  // Extra Actions
  static final extraActionsButton = const Key('__extraActionsButton__');
  static final toggleAll = const Key('__markAllDone__');
  static final clearCompleted = const Key('__clearCompleted__');

  // Filters
  static final filterButton = const Key('__filterButton__');
  static final allFilter = const Key('__allFilter__');
  static final activeFilter = const Key('__activeFilter__');
  static final completedFilter = const Key('__completedFilter__');

  // Stats
  static final statsCounter = const Key('__statsCounter__');
  static final statsLoading = const Key('__statsLoading__');
  static final statsNumActive = const Key('__statsActiveItems__');
  static final statsNumCompleted = const Key('__statsCompletedItems__');

  // Details Screen
  static final editMessageFab = const Key('__editMessageFab__');
  static final deleteMessageButton = const Key('__deleteMessageFab__');
  static final messageDetailsScreen = const Key('__messageDetailsScreen__');
  static final notificationDetailsScreen =
      const Key('__notificationDetailsScreen__');
  static final detailsMessageItemCheckbox = Key('DetailsMessage__Checkbox');
  static final detailsMessageItemTitle = Key('DetailsMessage__Title');
  static final detailsMessageItemBody = Key('DetailsMessage__Body');
  static final bishopsMessageScreen = Key('Bishops_Message__Screen');
  static final aboutScreen = Key('About__Screen');
  static final videoTestimonyScreen = Key('Video_Testimony__Screen');

  // Add Screen

  static final addMessageScreen = const Key('__addMessageScreen__');
  static final addVideoScreen = const Key('__addVideoScreen__');
  static final addRosaryScreen = const Key('__addRosaryScreen__');
  static final addRequestScreen = const Key('__addRequestScreen__');
  static final addTestimonyScreen = const Key('__addTestimonyScreen__');
  static final addBishopMessageScreen = const Key('__addBishopMessageScreen__');
  static final saveNewMessage = const Key('__saveNewMessage__');
  static final addBenefactorScreen = const Key('__addBenefactorScreen__');
  static final addGospelCardScreen = const Key('__addGospelCardScreen__');

  // Edit Screen
  static final editMessageScreen = const Key('__editMessageScreen__');
  static final saveMessageFab = const Key('__saveMessageFab__');

  //Rosary Screen

  static final rosaryListScreen = const Key('__rosaryListScreen__');
  static final offerRosaryOthers = const Key('__offerRosaryOthers__');
  static final settingsScreen = const Key('__settingsScreen__');
  static final helpScreen = const Key('__helpScreen__');
  static final addEditNotificationScreen =
      const Key('__addEditNotificationScreen__');
  static final giftVerseScreen = const Key('__giftVerseScreen__');
  static final userActiveScreen = const Key('__userActiveScreen__');
  //Notifications
  static final notificationList = const Key('__notificationList__');
  static final consecrationScreen = const Key('__consectrationScreen__');

  //ChatScreen
  static final communityChatScreen = const Key('__communityChatScreen__');

  static const String Youtube_API_KEY =
      'AIzaSyCokG0NUNZcaxly5yA5MQ5eVgMM_hCDjgw';

  //Campaigns Extra added

  static const addCampaignFab = Key('__addCampaignFab__');

  // Todos
  static const campaignList = Key('__campaignList__');
  static const campaignsLoading = Key('__campaignsLoading__');
  static final campaignItem = (String id) => Key('CampaignItem__${id}');
  static final campaignItemCheckbox =
      (String id) => Key('CampaignItem__${id}__Checkbox');
  static final campaignItemTitle =
      (String id) => Key('CampaignItem__${id}__Title');
  static final campaignItemDescription =
      (String id) => Key('CampaignItem__${id}__Description');

  // Tabs

  static const campaignTab = Key('__campaignTab__');
  static const statsTab = Key('__statsTab__');

  // Details Screen
  static const editCampaignFab = Key('__editCampaignFab__');
  static const deleteCampaignButton = Key('__deleteCampaignFab__');
  static const campaignDetailsScreen = Key('__campaignDetailsScreen__');
  static final detailsCampaignItemCheckbox = Key('DetailsCampaign__Checkbox');
  static final detailsCampaignItemTitle = Key('DetailsCampaign__Title');
  static final detailsCampaignItemDescription = Key('DetailsTodo__Description');

  // Add Screen
  static const addCampaignScreen = Key('__addCampaignScreen__');
  static const saveNewCampaign = Key('__saveNewCampaign__');
  static const titleField = Key('__titleField__');
  static const descriptionField = Key('__descriptionField__');

  // Edit Screen
  static const editCampaignScreen = Key('__editCampaignScreen__');
  static const saveCampaignFab = Key('__saveCampaignFab__');

  static final extraActionsPopupMenuButton =
      const Key('__extraActionsPopupMenuButton__');
  static final extraActionsEmptyContainer =
      const Key('__extraActionsEmptyContainer__');
  static final filteredCampaignsEmptyContainer =
      const Key('__filteredCampaignsEmptyContainer__');
  static final statsLoadInProgressIndicator =
      const Key('__statsLoadingIndicator__');
  static final emptyStatsContainer = const Key('__emptyStatsContainer__');
  static final emptyDetailsContainer = const Key('__emptyDetailsContainer__');
  static final detailsScreenCheckBox = const Key('__detailsScreenCheckBox__');
}
