// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

class MEMStarLevels {
  MEMStarLevels();

  static final rosaryWarrior = {
    0: 1,
    1: 100,
    2: 250,
    3: 500,
    4: 1000,
    5: 2500,
    6: 5000,
    7: 10000,
    8: 25000,
    9: 50000,
    10: 100000,
    11: 150000,
  };

  static final goodSamaritan = {
    0: 1,
    1: 100,
    2: 1000,
    3: 5000,
    4: 10000,
    5: 25000,
    6: 50000,
    7: 100000,
    8: 500000,
    9: 1000000,
    10: 2000000,
    11: 3000000
  };

  int rosaryWarriorLevel(int stars) {
    return rosaryWarrior.keys
        .firstWhere((k) => rosaryWarrior[k] > stars, orElse: () => 7);
  }

  int goodSamaritanLevel(int stars) {
    return goodSamaritan.keys
        .firstWhere((k) => goodSamaritan[k] >= stars, orElse: () => 7);
  }

//
}
