import 'package:flutter_local_notifications/flutter_local_notifications.dart';

/// Create a [AndroidNotificationChannel] for heads up notifications
AndroidNotificationChannel memChannel = const AndroidNotificationChannel(
  'mem_default_notification_channel', // id
  'MEM Notifications', // title
  'This channel is used for important MEM notifications.', // description
  importance: Importance.high,
);
