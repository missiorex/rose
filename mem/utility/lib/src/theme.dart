// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/material.dart';

class MemeTheme {
  static get theme {
    final originalTextTheme = ThemeData.light().textTheme;
    final originalBody1 = originalTextTheme.body1;

    return ThemeData(
        primarySwatch: Colors.grey,
        primaryIconTheme: IconThemeData(color: Colors.white),
        fontFamily: 'Bahnschrift',
        primaryColorLight: Color(0xFFe9e9e9),
        primaryColor: Color(0xFFcea963),
        accentColor: Color(0xFFbc0402),
        dialogBackgroundColor: Color(0xFFFFFFFF),
        buttonColor: Color(0xFF179fcd),
        primaryColorDark: Color(0xFFb80503),
        backgroundColor: Color(0xFF720c04),
        floatingActionButtonTheme: FloatingActionButtonThemeData(
            foregroundColor: Color(0xFFFFFFFF),
            backgroundColor: Color(0xFF179fcd)),
        primaryTextTheme: TextTheme(
          headline6: TextStyle(
            color: Colors.white,
            fontFamily: 'Bahnschrift',
          ),
        ),
        textTheme: originalTextTheme.copyWith(
          headline1: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'Bahnschrift',
          ),
          headline6: TextStyle(
            fontSize: 22.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Bahnschrift',
          ),
          headline3: TextStyle(
            fontSize: 22.0,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w600,
            fontFamily: 'Bahnschrift',
          ),
          headline5: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'Bahnschrift'),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Bahnschrift'),
        ));
  }
}
