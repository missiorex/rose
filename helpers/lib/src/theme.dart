// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/material.dart';

class MemTheme {
  static get theme {
    final originalTextTheme = ThemeData.light().textTheme;
    final originalBody1 = originalTextTheme.body1;

    return ThemeData(
        primaryColorLight: Color(0xFFe9e9e9),
        primaryColor: Color(0xFFebbf6a),
        accentColor: Color(0xFFec4c44),
        buttonColor: Color(0xFF10a5bd),
        textSelectionColor: Colors.cyan[100],
        backgroundColor: Color(0xFF7c0a03),
        textTheme: originalTextTheme.copyWith(
            button: TextStyle(fontSize: 16.0, color: Colors.grey),
            body1: originalBody1.copyWith(
              decorationColor: Colors.transparent,
            )));
  }
}
