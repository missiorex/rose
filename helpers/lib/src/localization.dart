// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
//import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:base_helpers/src/localizations/messages_all.dart';
import 'package:intl/intl.dart';

class MemLocalizations {
  MemLocalizations(this.locale);

  final Locale locale;

  static Future<MemLocalizations> load(Locale locale) {
    return initializeMessages(locale.toString()).then((_) {
      return MemLocalizations(locale);
    });
  }

  static MemLocalizations of(BuildContext context) {
    return Localizations.of<MemLocalizations>(context, MemLocalizations);
  }

//  String get appTitle => "Vanilla Example";

//  String get appTitle => Intl.message(
//        'Marian Eucharistic Ministry',
//        name: 'appTitle',
//        args: [],
//        locale: locale.toString(),
//      );

  String get bishopsMessageTitle => Intl.message(
        'Our Patron' 's Message',
        name: 'bishopsMessageTitle',
        args: [],
        locale: locale.toString(),
      );

  String get aboutUsTitle => Intl.message(
        'About MEM',
        name: 'aboutUsTitle',
        args: [],
        locale: locale.toString(),
      );

  String get videoTestimonyTitle => Intl.message(
        'Videos',
        name: 'videoTestimonyTitle',
        args: [],
        locale: locale.toString(),
      );

  String get messages => Intl.message(
        'Messages',
        name: 'messages',
        args: [],
        locale: locale.toString(),
      );

  String get requests => Intl.message(
        'Prayer Requests',
        name: 'requets',
        args: [],
        locale: locale.toString(),
      );

  String get showAll => Intl.message(
        'Show All',
        name: 'showAll',
        args: [],
        locale: locale.toString(),
      );

  String get showActive => Intl.message(
        'Show Active',
        name: 'showActive',
        args: [],
        locale: locale.toString(),
      );

  String get showCompleted => Intl.message(
        'Show Completed',
        name: 'showCompleted',
        args: [],
        locale: locale.toString(),
      );

  String get newMessageTitleHint => Intl.message(
        'A Suitable Title for the message',
        name: 'newMessageTitleHint',
        args: [],
        locale: locale.toString(),
      );

  String get newRequestTitleHint => Intl.message(
        'Title of your prayer request',
        name: 'newRequestTitleHint',
        args: [],
        locale: locale.toString(),
      );

  String get newTestimonyTitleHint => Intl.message(
        'Title of your Testimony',
        name: 'newTestimonyTitleHint',
        args: [],
        locale: locale.toString(),
      );

  String get markAllComplete => Intl.message(
        'Mark all complete',
        name: 'markAllComplete',
        args: [],
        locale: locale.toString(),
      );

  String get markAllIncomplete => Intl.message(
        'Mark all incomplete',
        name: 'markAllIncomplete',
        args: [],
        locale: locale.toString(),
      );

  String get clearCompleted => Intl.message(
        'Clear completed',
        name: 'clearCompleted',
        args: [],
        locale: locale.toString(),
      );

  String get addMessage => Intl.message(
        'Add Message',
        name: 'addMessage',
        args: [],
        locale: locale.toString(),
      );

  String get editMessage => Intl.message(
        'Edit Message',
        name: 'editMessage',
        args: [],
        locale: locale.toString(),
      );

  String get addRosary => Intl.message(
        'Offer Rosary',
        name: 'addRosary',
        args: [],
        locale: locale.toString(),
      );

  String get editRosary => Intl.message(
        'Edit Rosary Count',
        name: 'editRosary',
        args: [],
        locale: locale.toString(),
      );

  String get saveChanges => Intl.message(
        'Save changes',
        name: 'saveChanges',
        args: [],
        locale: locale.toString(),
      );

  String get filterMessages => Intl.message(
        'Filter Messages',
        name: 'filterMessages',
        args: [],
        locale: locale.toString(),
      );

  String get deleteMessage => Intl.message(
        'Delete Message',
        name: 'deleteMessage',
        args: [],
        locale: locale.toString(),
      );

  String get messageDetails => Intl.message(
        'Message Details',
        name: 'MessageDetails',
        args: [],
        locale: locale.toString(),
      );

  String get emptyMessageError => Intl.message(
        'Please enter some text',
        name: 'emptyMessageError',
        args: [],
        locale: locale.toString(),
      );

  String get requestHint => Intl.message(
        'Please enter your prayer need',
        name: 'requestHint',
        args: [],
        locale: locale.toString(),
      );

  String get messageHint => Intl.message(
        'Please enter the message from admin',
        name: 'requestHint',
        args: [],
        locale: locale.toString(),
      );

  String get testimonyHint => Intl.message(
        'Please enter your Testimony',
        name: 'testimonyHint',
        args: [],
        locale: locale.toString(),
      );

  String get completedMessages => Intl.message(
        'Completed Messages',
        name: 'completedMessages',
        args: [],
        locale: locale.toString(),
      );

  String get activeMessages => Intl.message(
        'Active Messages',
        name: 'activeMessages',
        args: [],
        locale: locale.toString(),
      );

  String messageDeleted(String title) => Intl.message(
        'Deleted "$title"',
        name: 'messageDeleted',
        args: [title],
        locale: locale.toString(),
      );

  String get undo => Intl.message(
        'Undo',
        name: 'undo',
        args: [],
        locale: locale.toString(),
      );

  String get deleteMessageConfirmation => Intl.message(
        'Delete this Message?',
        name: 'deleteMessageConfirmation',
        args: [],
        locale: locale.toString(),
      );

  String get delete => Intl.message(
        'Delete',
        name: 'delete',
        args: [],
        locale: locale.toString(),
      );

  String get cancel => Intl.message(
        'Cancel',
        name: 'cancel',
        args: [],
        locale: locale.toString(),
      );
}

class MemLocalizationsDelegate extends LocalizationsDelegate<MemLocalizations> {
  @override
  Future<MemLocalizations> load(Locale locale) => MemLocalizations.load(locale);

  @override
  bool shouldReload(MemLocalizationsDelegate old) => false;

  @override
  bool isSupported(Locale locale) =>
      locale.languageCode.toLowerCase().contains("en");
}
