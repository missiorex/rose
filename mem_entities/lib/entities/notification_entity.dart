import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:mem_entities/mem_entities.dart';

class NotificationEntity {
  final String id;
  final String title;
  final String body;
  final String topic;
  final int time;
  final String author;
  final String authorID;
  final String priority;
  final String token;

  NotificationEntity(this.id, this.title, this.body, this.topic, this.token,
      this.time, this.author, this.authorID, this.priority);

  @override
  int get hashCode =>
      title.hashCode ^
      body.hashCode ^
      topic.hashCode ^
      token.hashCode ^
      id.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorID.hashCode ^
      priority.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NotificationEntity &&
          runtimeType == other.runtimeType &&
          body == other.body &&
          title == other.title &&
          topic == other.topic &&
          token == other.token &&
          id == other.id &&
          time == other.time &&
          author == other.author &&
          authorID == other.authorID &&
          priority == other.priority;

  Map<String, Object> toJson() {
    return {
      "title": title,
      "body": body,
      "topic": topic,
      "id": id,
      "token": token,
      "time": jsonEncode(time, toEncodable: obEncode),
      "author": author,
      "authorID": authorID,
      "priority": priority,
    };
  }

  @override
  String toString() {
    return 'MessageEntity{ title: $title, body: $body,topic:$topic,token:$token, id: $id, time:$time,author:$author,authorID:$authorID,priority:$priority}';
  }

  static NotificationEntity fromJson(Map<String, Object> json) {
    return NotificationEntity(
        json["id"] as String,
        json["title"] as String,
        json["body"] as String,
        json["topic"] as String,
        json["token"] as String,
        int.parse(json["time"]),
        json["author"],
        json["authorID"],
        json["priority"]
        //DateTime.parse(json["time"]),
        );
  }
}

//class JsonMsgType {
//  final MessageType value;
//  JsonMsgType(this.value);
//
//  String toJson() => value != null ? value.toString() : null;
//  @override
//  String toString() {
//    return value.toString();
//  }
//}

//enum MessageType { admin, prayerRequest, thanks, rosary }
