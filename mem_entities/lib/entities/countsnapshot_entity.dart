import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:mem_entities/mem_entities.dart';

class CountSnapShotEntity {
  final String msgId;
  final int rosarySelfCount;
  final int rosaryExternalCount;
  final int totalCount;
  final int time;
  final String updatedBy;
  final String updatedByID;
  final String details;
  final bool isExternal;

  CountSnapShotEntity(
      this.msgId,
      this.rosarySelfCount,
      this.rosaryExternalCount,
      this.totalCount,
      this.time,
      this.updatedBy,
      this.updatedByID,
      this.details,
      this.isExternal);

  @override
  int get hashCode =>
      msgId.hashCode ^
      rosarySelfCount.hashCode ^
      totalCount.hashCode ^
      rosaryExternalCount.hashCode ^
      time.hashCode ^
      updatedByID.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CountSnapShotEntity &&
          runtimeType == other.runtimeType &&
          msgId == other.msgId &&
          rosarySelfCount == other.rosarySelfCount &&
          totalCount == other.totalCount &&
          rosaryExternalCount == other.rosaryExternalCount &&
          time == other.time &&
          updatedByID == other.updatedByID;

  Map<String, Object> toJson() {
    return {
      "msgId": msgId,
      "rosarySelfCount": rosarySelfCount,
      "rosaryExternalCount": rosaryExternalCount,
      "totalCount": totalCount,
      "time": jsonEncode(time, toEncodable: obEncode),
      "updatedBy": updatedBy,
      "updatedByID": updatedByID,
      "details": details,
      "isExternal": isExternal,
    };
  }

  @override
  String toString() {
    return 'CountSnapshotEntity{ id: $msgId, rosaryCount:$rosarySelfCount,totalCount:$totalCount,uid:$updatedByID,time:$time,author:$updatedBy}';
  }

  static CountSnapShotEntity fromJson(Map<String, Object> json) {
    return CountSnapShotEntity(
      json["msgId"] as String,
      json["rosarySelfCount"] as int,
      json["rosaryExternalCount"] as int,
      json["totalCount"] as int,
      json["time"] as int,
      json["updatedBy"] as String,
      json["updatedByID"] as String,
      json["details"] as String,
      json["isExternal"] as bool,
    );
  }
}
