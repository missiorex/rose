class BadgeEntity {
  final String id;
  final String title;
  final String uid;
  final int time;
  final int level;
  final String action;
  final int starsValue;
  final bool isComplete;
  final String assetName;

  BadgeEntity(this.id, this.title, this.uid, this.time, this.level, this.action,
      this.starsValue, this.isComplete, this.assetName);

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      level.hashCode ^
      action.hashCode ^
      starsValue.hashCode ^
      isComplete.hashCode ^
      assetName.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BadgeEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          uid == other.uid &&
          time == other.time &&
          level == other.level &&
          action == other.action &&
          starsValue == other.starsValue &&
          isComplete == other.isComplete &&
          assetName == other.assetName;

  Map<String, Object> toJson() {
    return {
      "id": id,
      "title": title,
      "uid": uid,
      "time": time,
      "level": level,
      "action": action,
      "starsValue": starsValue,
      "isComplete": isComplete,
      "assetName": assetName
    };
  }

  @override
  String toString() {
    return 'BadgeEntity{ id: $id, title:$title,uid:$uid,time:$time,level:$level,action:$action,starsValue:$starsValue,isComplete:$isComplete,assetName:$assetName}';
  }

  static BadgeEntity fromJson(Map<String, Object> json) {
    return BadgeEntity(
        json["id"] as String,
        json["title"],
        json["uid"],
        json["time"] as int,
        json["level"] as int,
        json["action"],
        json["starsValue"],
        json["isComplete"],
        json["assetName"]);
  }
}
