class GiftEntity {
  final String id;
  final String title;
  final String uid;
  final int time;
  final String giftType;
  final String giftVerse;
  final String giftImageUrl;
  final String giftDetails;

  GiftEntity(this.id, this.title, this.uid, this.time, this.giftVerse,
      this.giftType, this.giftImageUrl, this.giftDetails);

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      giftType.hashCode ^
      giftVerse.hashCode ^
      giftImageUrl.hashCode ^
      giftDetails.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GiftEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          uid == other.uid &&
          time == other.time &&
          giftType == other.giftType &&
          giftVerse == other.giftVerse &&
          giftImageUrl == other.giftImageUrl &&
          giftDetails == other.giftDetails;

  Map<String, Object> toJson() {
    return {
      "id": id,
      "badgeDoc": title,
      "uid": uid,
      "time": time,
      "giftType": giftType,
      "giftVerse": giftVerse,
      "giftImageUrl": giftImageUrl,
      "giftDetails": giftDetails,
    };
  }

  @override
  String toString() {
    return 'BadgeEntity{ id: $id, badgeDoc:$title,uid:$uid,time:$time,giftType:$giftType,giftVerse:$giftVerse,giftImageUrl:$giftImageUrl,giftDetails:$giftDetails}';
  }

  static GiftEntity fromJson(Map<String, Object> json) {
    return GiftEntity(
        json["id"] as String,
        json["badgeDoc"],
        json["uid"],
        json["time"],
        json["giftType"],
        json["giftVerse"],
        json["giftImageUrl"],
        json["giftDetails"]);
  }
}
