// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

class AclEntity {
  String id;
  String name;
  bool grantAdminRole;
  bool revokeAdminRole;
  bool grantSuperAdminRole;
  bool revokeSuperAdminRole;
  bool activateUser;
  bool deactivateUser;
  bool blockUser;
  bool unBlockUser;
  bool deleteUser;
  bool regionalAdmin;
  bool localeAdmin;
  bool viewCounselling;
  bool rosaryDelete;
  bool messageDelete;
  bool addVideoTestimony;
  bool addBishopsMessage;
  bool editAboutUsContent;

  AclEntity(
      {this.id,
      this.name,
      this.grantAdminRole,
      this.revokeAdminRole,
      this.grantSuperAdminRole,
      this.revokeSuperAdminRole,
      this.activateUser,
      this.deactivateUser,
      this.blockUser,
      this.unBlockUser,
      this.deleteUser,
      this.regionalAdmin,
      this.localeAdmin,
      this.viewCounselling,
      this.rosaryDelete,
      this.messageDelete,
      this.addVideoTestimony,
      this.addBishopsMessage,
      this.editAboutUsContent});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AclEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          grantAdminRole == other.grantAdminRole &&
          revokeAdminRole == other.revokeAdminRole &&
          grantSuperAdminRole == other.grantSuperAdminRole &&
          revokeSuperAdminRole == other.revokeSuperAdminRole &&
          activateUser == other.activateUser &&
          deactivateUser == other.deactivateUser &&
          blockUser == other.blockUser &&
          unBlockUser == other.unBlockUser &&
          deleteUser == other.deleteUser &&
          regionalAdmin == other.regionalAdmin &&
          localeAdmin == other.localeAdmin &&
          viewCounselling == other.viewCounselling &&
          rosaryDelete == other.rosaryDelete &&
          messageDelete == other.messageDelete &&
          addVideoTestimony == other.addVideoTestimony &&
          addBishopsMessage == other.addBishopsMessage &&
          editAboutUsContent == other.editAboutUsContent;

  @override
  int get hashCode => id.hashCode ^ grantAdminRole.hashCode;

  @override
  String toString() {
    return 'ACLEntity{id: $id,name: $name, grantAdminRole: $grantAdminRole,revokeAdminRole: $revokeAdminRole,grantSuperAdminRole: $grantSuperAdminRole,revokeSuperAdminRole: $revokeSuperAdminRole,activateUser: $activateUser,deactivateUser: $deactivateUser,blockUser: $blockUser,unBlockUser: $unBlockUser,deleteUser: $deleteUser,regionalAdmin: $regionalAdmin,localeAdmin: $localeAdmin,viewCounselling: $viewCounselling,rosaryDelete:$rosaryDelete,messageDelete:$messageDelete,"addVideoTestimony":$addVideoTestimony,addBishopsMessage:$addBishopsMessage,editAboutUsContent:$editAboutUsContent}';
  }

  Map<String, Object> toJson() {
    return {
      "id": id,
      "name": name,
      "grantAdminRole": grantAdminRole,
      "revokeAdminRole": revokeAdminRole,
      "grantSuperAdminRole": grantSuperAdminRole,
      "revokeSuperAdminRole": revokeSuperAdminRole,
      "activateUser": activateUser,
      "deactivateUser": deactivateUser,
      "blockUser": blockUser,
      "unBlockUser": unBlockUser,
      "deleteUser": deleteUser,
      "regionalAdmin": regionalAdmin,
      "localeAdmin": localeAdmin,
      "viewCounselling": viewCounselling,
      "rosaryDelete": rosaryDelete,
      "messageDelete": messageDelete,
      "addVideoTestimony": addVideoTestimony,
      "addBishopsMessage": addBishopsMessage,
      "editAboutUsContent": editAboutUsContent
    };
  }

  static AclEntity fromJson(Map<String, Object> json) {
    return AclEntity(
        id: json["id"] as String,
        name: json["name"] as String,
        grantAdminRole: json["grantAdminRole"] as bool,
        revokeAdminRole: json["revokeAdminRole"] as bool,
        grantSuperAdminRole: json["grantSuperAdminRole"] as bool,
        revokeSuperAdminRole: json["revokeSuperAdminRole"] as bool,
        activateUser: json["activateUser"] as bool,
        deactivateUser: json["deactivateUser"] as bool,
        blockUser: json["blockUser"] as bool,
        unBlockUser: json["unBlockUser"] as bool,
        deleteUser: json["deleteUser"] as bool,
        regionalAdmin: json["regionalAdmin"] as bool,
        localeAdmin: json["localeAdmin"] as bool,
        viewCounselling: json["viewCounselling"] as bool,
        rosaryDelete: json["rosaryDelete"] as bool,
        messageDelete: json["messageDelete"] as bool,
        addVideoTestimony: json["addVideoTestimony"] as bool,
        addBishopsMessage: json["addBishopsMessage"] as bool,
        editAboutUsContent: json["editAboutUsContent"] as bool);
  }
}
