import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:mem_entities/mem_entities.dart';

class BishopsMessageEntity {
  final String id;
  final String title;
  final String body;
  final int time;
  final String author;
  final String authorID;
  final String authorTitle;
  final String youtubeUrl;
  final String videoImageUrl;

  BishopsMessageEntity(this.id, this.title, this.body, this.time, this.author,
      this.authorID, this.authorTitle, this.youtubeUrl, this.videoImageUrl);

  @override
  int get hashCode =>
      title.hashCode ^
      body.hashCode ^
      id.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorID.hashCode ^
      authorTitle.hashCode ^
      youtubeUrl.hashCode ^
      videoImageUrl.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BishopsMessageEntity &&
          runtimeType == other.runtimeType &&
          body == other.body &&
          title == other.title &&
          id == other.id &&
          time == other.time &&
          author == other.author &&
          authorID == other.authorID &&
          authorTitle == other.authorTitle &&
          youtubeUrl == other.youtubeUrl &&
          videoImageUrl == other.videoImageUrl;

  Map<String, Object> toJson() {
    return {
      "title": title,
      "body": body,
      "id": id,
      "time": jsonEncode(time, toEncodable: obEncode),
      "author": author,
      "authorID": authorID,
      "authorTitle": authorTitle,
      "youtubeUrl": youtubeUrl,
      "videoImageUrl": videoImageUrl,
    };
  }

  @override
  String toString() {
    return 'VideoTestimonyEntity{ title: $title, body: $body, id: $id, time:$time,author:$author,authorID:$authorID,authorTitle:$authorTitle,videoImageUrl:$videoImageUrl}';
  }

  static BishopsMessageEntity fromJson(Map<String, Object> json) {
    return BishopsMessageEntity(
        json["id"] as String,
        json["title"] as String,
        json["body"] as String,

        //int.parse(json["rosaryCount"]),
        int.parse(json["time"]),
        json["author"],
        json["authorID"],
        json["authorTitle"],
        json["youtubeUrl"],
        json["videoImageUrl"]
        //DateTime.parse(json["time"]),
        );
  }
}

//class JsonMsgType {
//  final MessageType value;
//  JsonMsgType(this.value);
//
//  String toJson() => value != null ? value.toString() : null;
//  @override
//  String toString() {
//    return value.toString();
//  }
//}

//enum MessageType { admin, prayerRequest, thanks, rosary }
