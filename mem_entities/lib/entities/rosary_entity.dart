// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.
import 'dart:convert';
import 'package:mem_entities/mem_entities.dart';

class RosaryEntity {
  int totalByUsers;
  int totalCount;
  int totalExternal;
  final String id;
  final int time;

  RosaryEntity(this.id, this.totalByUsers, this.totalCount, this.totalExternal,
      this.time);

  @override
  int get hashCode =>
      id.hashCode ^
      totalByUsers.hashCode ^
      totalCount.hashCode ^
      totalExternal.hashCode ^
      time.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RosaryEntity &&
          id == other.id &&
          runtimeType == other.runtimeType &&
          totalByUsers == other.totalByUsers &&
          totalCount == other.totalCount &&
          totalExternal == other.totalExternal &&
          time == other.time;

  Map<String, Object> toJson() {
    return {
      "id": id,
      "totalByUsers": totalByUsers,
      "totalCount": totalCount,
      "totalExternal": totalExternal,
      "time": jsonEncode(time, toEncodable: obEncode),
    };
  }

  @override
  String toString() {
    return 'RosaryEntity{id: $id,totalByUsers: $totalByUsers, totalCount: $totalCount, totalExternal: $totalExternal,time:$time}';
  }

  static RosaryEntity fromJson(Map<String, Object> json) {
    return RosaryEntity(
        json["id"] as String,
        json["totalByUsers"] as int,
        json["total_count"] as int,
        json["totalExternal"] as int,
        json["time"] as int);
  }
}
