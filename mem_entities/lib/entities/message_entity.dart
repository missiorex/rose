import 'dart:convert';
import 'package:intl/intl.dart';

class MessageEntity {
  final bool complete;
  final String id;
  final String title;
  final String body;
  final String msgType;
  final int rosaryCount;
  final int rosaryByOthers;
  final int time;
  final String author;
  final String authorID;

  MessageEntity(
      this.id,
      this.title,
      this.body,
      this.complete,
      this.msgType,
      this.rosaryCount,
      this.rosaryByOthers,
      this.time,
      this.author,
      this.authorID);

  @override
  int get hashCode =>
      complete.hashCode ^
      title.hashCode ^
      body.hashCode ^
      id.hashCode ^
      msgType.hashCode ^
      rosaryCount.hashCode ^
      rosaryByOthers.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorID.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MessageEntity &&
          runtimeType == other.runtimeType &&
          complete == other.complete &&
          msgType == other.msgType &&
          body == other.body &&
          title == other.title &&
          id == other.id &&
          rosaryCount == other.rosaryCount &&
          rosaryByOthers == other.rosaryByOthers &&
          time == other.time &&
          author == other.author &&
          authorID == other.authorID;

  Map<String, Object> toJson() {
    return {
      "complete": complete,
      "title": title,
      "body": body,
      "id": id,
      "msgtype": msgType,
      "rosaryCount": rosaryCount,
      "rosaryByOthers": rosaryByOthers,
      "time": jsonEncode(time, toEncodable: obEncode),
      "author": author,
      "authorID": authorID
    };
  }

  @override
  String toString() {
    return 'MessageEntity{complete: $complete, title: $title, body: $body, id: $id, msgtype: $msgType, rosaryCount:$rosaryCount,rosaryByOthers:$rosaryByOthers,time:$time,author:$author,authorID:$authorID}';
  }

  static MessageEntity fromJson(Map<String, Object> json) {
    return MessageEntity(
      json["id"] as String,
      json["title"] as String,
      json["body"] as String,
      json["complete"] as bool,
      json["msgtype"] as String,
      json["rosaryCount"],
      json["rosaryByOthers"],
      //int.parse(json["rosaryCount"]),
      int.parse(json["time"]),
      json["author"],
      json["authorID"],
      //DateTime.parse(json["time"]),
    );
  }
}

dynamic obEncode(dynamic item) {
  if (item is DateTime) {
    return item.millisecondsSinceEpoch.toInt();
  } else {
    return item.toString();
  }
}

class JsonDateTime {
  DateTime value;
  JsonDateTime(this.value);

  String toJson() => value != null ? value.toIso8601String() : null;

  @override
  String toString() {
    return value.toIso8601String();
  }
}

//class JsonMsgType {
//  final MessageType value;
//  JsonMsgType(this.value);
//
//  String toJson() => value != null ? value.toString() : null;
//  @override
//  String toString() {
//    return value.toString();
//  }
//}

//enum MessageType { admin, prayerRequest, thanks, rosary }
