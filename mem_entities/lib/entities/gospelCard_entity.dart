class GospelCardEntity {
  String id;
  String title;
  String uid;
  int time;
  String cardType;
  String gospelVerse;
  String cardImageUrl;
  String cardDetails;
  String contentUrl;
  bool isComplete;
  bool startUpCard;
  bool sliderCard;
  String locale;

  GospelCardEntity(
      this.id,
      this.title,
      this.uid,
      this.time,
      this.gospelVerse,
      this.cardType,
      this.cardImageUrl,
      this.cardDetails,
      this.contentUrl,
      this.isComplete,
      this.startUpCard,
      this.sliderCard,
      this.locale);

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      cardType.hashCode ^
      gospelVerse.hashCode ^
      cardImageUrl.hashCode ^
      cardDetails.hashCode ^
      contentUrl.hashCode ^
      isComplete.hashCode ^
      startUpCard.hashCode ^
      locale.hashCode ^
      sliderCard.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GospelCardEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          uid == other.uid &&
          time == other.time &&
          cardType == other.cardType &&
          gospelVerse == other.gospelVerse &&
          cardImageUrl == other.cardImageUrl &&
          cardDetails == other.cardDetails &&
          contentUrl == other.contentUrl &&
          isComplete == other.isComplete &&
          startUpCard == other.startUpCard &&
          locale == other.locale &&
          sliderCard == other.sliderCard;

  Map<String, Object> toJson() {
    return {
      "id": id,
      "title": title,
      "uid": uid,
      "time": time,
      "cardType": cardType,
      "gospelVerse": gospelVerse,
      "cardImageUrl": cardImageUrl,
      "cardDetails": cardDetails,
      "contentUrl": contentUrl,
      "isComplete": isComplete,
      "startUpCard": startUpCard,
      "locale": locale,
      "sliderCard": sliderCard
    };
  }

  @override
  String toString() {
    return 'GospelCardEntity{ id: $id, title:$title,uid:$uid,time:$time,cardType:$cardType,gospelVerse:$gospelVerse,cardImageUrl:$cardImageUrl,cardDetails:$cardDetails,contentUrl:$contentUrl,isComplete:$isComplete,startUpCard:$startUpCard,sliderCard:$sliderCard,locale:$locale}';
  }

  static GospelCardEntity fromJson(Map<String, Object> json) {
    return GospelCardEntity(
      json["id"] as String,
      json["title"],
      json["uid"],
      json["time"],
      json["cardType"],
      json["gospelVerse"],
      json["cardImageUrl"],
      json["cardDetails"],
      json["contentUrl"],
      json["isComplete"],
      json["startUpCard"],
      json["sliderCard"],
      json["locale"],
    );
  }
}
