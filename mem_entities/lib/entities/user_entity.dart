// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.
import 'package:base_helpers/uuid.dart';

class UserEntity {
  String id;
  String displayName;
  String photoUrl;
  String role;
  String emailId;
  String password;
  String title;
  String name;
  String country;
  String region;
  String gender;
  String countryCode;
  String phoneNumber;
  String profilePhotoUrl;
  int rosariesOffered;
  int rosariesCollected;
  //List<String> groupMemberOf;
  int userAddedOn;
  int rosaryLastAddedOn;
  int birthday;
  bool isSuperAdmin;
  bool status;
  String pushNotificationToken;
  String locale;
  bool isBlocked;
  int registrationCalls;

  UserEntity(
      {this.id,
      this.displayName,
      this.photoUrl,
      this.emailId,
      this.password,
      this.title,
      this.name,
      this.country,
      this.region,
      this.gender,
      this.countryCode,
      this.phoneNumber,
      this.profilePhotoUrl,
      this.rosariesOffered,
      this.rosariesCollected,
      //this.groupMemberOf,
      this.userAddedOn,
      this.rosaryLastAddedOn,
      this.birthday,
      this.isSuperAdmin,
      this.status,
      this.pushNotificationToken,
      this.locale,
      this.role,
      this.isBlocked,
      this.registrationCalls});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          displayName == other.displayName &&
          photoUrl == other.photoUrl &&
          emailId == other.emailId &&
          password == other.password &&
          title == other.title &&
          name == other.name &&
          country == other.country &&
          region == other.region &&
          gender == other.gender &&
          countryCode == other.countryCode &&
          phoneNumber == other.phoneNumber &&
          profilePhotoUrl == other.profilePhotoUrl &&
          rosariesOffered == other.rosariesOffered &&
          rosariesCollected == other.rosariesCollected &&
          //groupMemberOf == other.groupMemberOf &&
          userAddedOn == other.userAddedOn &&
          rosaryLastAddedOn == other.rosaryLastAddedOn &&
          birthday == other.birthday &&
          isSuperAdmin == other.isSuperAdmin &&
          status == other.status &&
          pushNotificationToken == other.pushNotificationToken &&
          locale == other.locale &&
          role == other.role &&
          isBlocked == other.isBlocked &&
          registrationCalls == other.registrationCalls;

  @override
  int get hashCode =>
      id.hashCode ^ displayName.hashCode ^ photoUrl.hashCode ^ role.hashCode;

  @override
  String toString() {
    return 'UserEntity{id: $id, displayName: $displayName, profilePhotoUrl: $profilePhotoUrl}';
  }

  Map<String, Object> toJson() {
    return {
      "id": id,
      "displayName": displayName,
      "photoUrl": photoUrl,
      "emailId": emailId,
      "password": password,
      "title": title,
      "name": name,
      "country": country,
      "region": region,
      "gender": gender,
      "countryCode": countryCode,
      "phoneNumber": phoneNumber,
      "profilePhotoUrl": profilePhotoUrl,
      "rosariesOffered": rosariesOffered,
      "rosariesCollected": rosariesCollected,
      //"groupMemberOf": groupMemberOf,
      "userAddedOn": userAddedOn,
      "rosaryLastAddedOn": rosaryLastAddedOn,
      "birthday": birthday,
      "isSuperAdmin": isSuperAdmin,
      "status": status,
      "pushNotificationToken": pushNotificationToken,
      "locale": locale,
      "role": role,
      "isBlocked": isBlocked,
      "registrationCalls": registrationCalls
    };
  }

  static UserEntity fromJson(Map<String, Object> json) {
    return UserEntity(
      id: json["id"] as String,
      displayName: json["displayName"] as String,
      photoUrl: json["photoUrl"] as String,
      emailId: json["emailId"] as String,
      password: json["password"] as String,
      title: json["title"] as String,
      name: json["name"] as String,
      country: json["country"] as String,
      countryCode: json["countryCode"] as String,
      region: json["region"] as String,
      gender: json["gender"] as String,
      phoneNumber: json["phoneNumber"] as String,
      profilePhotoUrl: json["profilePhotoUrl"] as String,
      rosariesOffered: json["rosariesOffered"] as int,
      rosariesCollected: json["rosariesCollected"] as int,
      //groupMemberOf: json["groupMemberof"] as String,
      birthday: json["birthday"] as int,
      userAddedOn: json["userAddedOn"] as int,
      rosaryLastAddedOn: json["rosaryLastAddedOn"] as int,
      isSuperAdmin: json["isSuperAdmin"] as bool,
      status: json["status"] as bool,
      pushNotificationToken: json["pushNotificationToken"] as String,
      locale: json["locale"] as String,
      role: json["role"] as String,
      isBlocked: json["isBlocked"] as bool,
      registrationCalls: json["registrationCalls"] as int,
    );
  }
}
