import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:mem_entities/mem_entities.dart';

class RosaryCountEntity {
  final String id;
  final int rosaryCount;
  final int totalCount;
  final String uid;
  final int time;
  final String author;

  RosaryCountEntity(this.id, this.rosaryCount, this.totalCount, this.uid,
      this.time, this.author);

  @override
  int get hashCode =>
      id.hashCode ^
      rosaryCount.hashCode ^
      totalCount.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      author.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RosaryCountEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          rosaryCount == other.rosaryCount &&
          totalCount == other.totalCount &&
          uid == other.uid &&
          time == other.time &&
          author == other.author;

  Map<String, Object> toJson() {
    return {
      "id": id,
      "rosaryCount": rosaryCount,
      "totalCount": totalCount,
      "uid": uid,
      "time": jsonEncode(time, toEncodable: obEncode),
      "author": author
    };
  }

  @override
  String toString() {
    return 'MessageEntity{ id: $id, rosaryCount:$rosaryCount,totalCount:$totalCount,uid:$uid,time:$time,author:$author}';
  }

  static RosaryCountEntity fromJson(Map<String, Object> json) {
    return RosaryCountEntity(
        json["id"] as String,
        json["rosaryCount"],
        json["totalCount"],
        json["uid"],
        int.parse(json["time"]),
        json["author"]);
  }
}
