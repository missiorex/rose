import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:mem_entities/mem_entities.dart';

class ActionEntity {
  final String id;
  final String title;
  final String body;
  final String topic;
  final int time;
  final String author;
  final String authorID;
  final String priority;
  final String token;
  bool notified;
  String notificationType;
  String summary;
  List<String> actionItems;
  bool weekly;
  bool daily;
  int repeatHour;
  int repeatMinute;
  int repeatSecond;
  int day;

  ActionEntity(
      this.id,
      this.title,
      this.body,
      this.topic,
      this.token,
      this.time,
      this.author,
      this.authorID,
      this.priority,
      this.notified,
      this.notificationType,
      this.summary,
      this.actionItems,
      this.weekly,
      this.daily,
      this.repeatHour,
      this.repeatMinute,
      this.repeatSecond,
      this.day);

  @override
  int get hashCode =>
      title.hashCode ^
      body.hashCode ^
      topic.hashCode ^
      token.hashCode ^
      id.hashCode ^
      time.hashCode ^
      author.hashCode ^
      authorID.hashCode ^
      priority.hashCode ^
      notified.hashCode ^
      notificationType.hashCode ^
      summary.hashCode ^
      actionItems.hashCode ^
      weekly.hashCode ^
      daily.hashCode ^
      repeatHour.hashCode ^
      repeatMinute.hashCode ^
      repeatSecond.hashCode ^
      day.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ActionEntity &&
          runtimeType == other.runtimeType &&
          body == other.body &&
          title == other.title &&
          topic == other.topic &&
          token == other.token &&
          id == other.id &&
          time == other.time &&
          author == other.author &&
          authorID == other.authorID &&
          priority == other.priority &&
          notified == other.notified &&
          notificationType == other.notificationType &&
          summary == other.summary &&
          actionItems == other.actionItems &&
          weekly == other.weekly &&
          daily == other.daily &&
          repeatHour == other.repeatHour &&
          repeatMinute == other.repeatMinute &&
          repeatSecond == other.repeatSecond &&
          day == other.day;

  Map<String, Object> toJson() {
    return {
      "title": title,
      "body": body,
      "topic": topic,
      "id": id,
      "token": token,
      "time": jsonEncode(time, toEncodable: obEncode),
      "author": author,
      "authorID": authorID,
      "priority": priority,
      "notified": notified,
      "notificationType": notificationType,
      "summary": summary,
      "actionItems": actionItems,
      "weekly": weekly,
      "daily": daily,
      "repeatHour": repeatHour,
      "repeatMinute": repeatMinute,
      "repeatSecond": repeatSecond,
      "day": day
    };
  }

  @override
  String toString() {
    return 'ActionEntity{ title: $title, body: $body,topic:$topic,token:$token, id: $id, time:$time,author:$author,authorID:$authorID,priority:$priority}';
  }

  static ActionEntity fromJson(Map<String, Object> json) {
    return ActionEntity(
        json["id"] as String,
        json["title"] as String,
        json["body"] as String,
        json["topic"] as String,
        json["token"] as String,
        int.parse(json["time"]),
        json["author"],
        json["authorID"],
        json["priority"],
        json["notified"],
        json["notificationType"] as String,
        json["summary"] as String,
        json["actionItems"],
        json["weekly"],
        json["daily"],
        json["repeatHour"],
        json["repeatMinute"],
        json["repeatSecond"],
        json["day"]
        //DateTime.parse(json["time"]),
        );
  }
}

//class JsonMsgType {
//  final MessageType value;
//  JsonMsgType(this.value);
//
//  String toJson() => value != null ? value.toString() : null;
//  @override
//  String toString() {
//    return value.toString();
//  }
//}

//enum MessageType { admin, prayerRequest, thanks, rosary }
