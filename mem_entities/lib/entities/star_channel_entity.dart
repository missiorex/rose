class StarsChannelEntity {
  final String id;
  final String title;
  final String uid;
  final int time;
  final String action;
  final int count;
  final int level;

  StarsChannelEntity(this.id, this.title, this.uid, this.time, this.action,
      this.count, this.level);

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      uid.hashCode ^
      time.hashCode ^
      level.hashCode ^
      action.hashCode ^
      count.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StarsChannelEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          uid == other.uid &&
          time == other.time &&
          level == other.level &&
          action == other.action &&
          count == other.count;

  Map<String, Object> toJson() {
    return {
      "id": id,
      "title": title,
      "uid": uid,
      "time": time,
      "level": level,
      "action": action,
      "count": count
    };
  }

  @override
  String toString() {
    return 'BadgeEntity{ id: $id, title:$title,uid:$uid,time:$time,level:$level,action:$action,count:$count}';
  }

  static StarsChannelEntity fromJson(Map<String, Object> json) {
    return StarsChannelEntity(
      json["id"] as String,
      json["title"],
      json["uid"],
      json["time"],
      json["action"],
      json["count"],
      json["level"],
    );
  }
}
