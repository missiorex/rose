import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:mem_entities/mem_entities.dart';

class BenefactorEntity {
  final String id;
  final String title;
  final String name;
  final String message;
  final bool isPriest;
  final String profilePhotoUrl;

  BenefactorEntity(this.id, this.title, this.name, this.message, this.isPriest,
      this.profilePhotoUrl);

  @override
  int get hashCode =>
      title.hashCode ^
      name.hashCode ^
      message.hashCode ^
      id.hashCode ^
      isPriest.hashCode ^
      profilePhotoUrl.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BenefactorEntity &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          name == other.name &&
          message == other.message &&
          id == other.id &&
          isPriest == other.isPriest &&
          profilePhotoUrl == other.profilePhotoUrl;

  Map<String, Object> toJson() {
    return {
      "title": title,
      "name": name,
      "message": message,
      "id": id,
      "isPriest": isPriest,
      "profilePhotoUrl": profilePhotoUrl,
    };
  }

  @override
  String toString() {
    return 'BenefactorEntity{ title: $title, name: $name, id: $id, isPriest:$isPriest,profilePhotoUrl:$profilePhotoUrl}';
  }

  static BenefactorEntity fromJson(Map<String, Object> json) {
    return BenefactorEntity(
        json["id"] as String,
        json["title"] as String,
        json["name"] as String,
        json["message"] as String,
        json["isPriest"] as bool,
        json["profilePhotoUrl"]
        //DateTime.parse(json["time"]),
        );
  }
}

//class JsonMsgType {
//  final MessageType value;
//  JsonMsgType(this.value);
//
//  String toJson() => value != null ? value.toString() : null;
//  @override
//  String toString() {
//    return value.toString();
//  }
//}

//enum MessageType { admin, prayerRequest, thanks, rosary }
