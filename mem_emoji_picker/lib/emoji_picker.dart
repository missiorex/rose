library emoji_picker;

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math';

enum Category {
  RECOMMENDED,
  SMILEYS,
  ANIMALS,
  FOODS,
  TRAVEL,
  ACTIVITIES,
  OBJECTS,
  SYMBOLS,
  FLAGS
}

enum ButtonMode { MATERIAL, CUPERTINO }

typedef void OnEmojiSelected(Emoji emoji, Category category);

class EmojiPicker extends StatefulWidget {
  @override
  _EmojiPickerState createState() => new _EmojiPickerState();

  final int columns;
  final int rows;

  Category selectedCategory;

  final OnEmojiSelected onEmojiSelected;

  Color bgColor;
  Color indicatorColor;

  final Color _defaultBgColor = Color.fromRGBO(242, 242, 242, 1);

  final List<String> recommendKeywords;
  final int numRecommended;

  final String noRecommendationsText;
  TextStyle noRecommendationsStyle;

  CategoryIcons categoryIcons;

  final ButtonMode buttonMode;

  Icon unavailableEmojiIcon;

  EmojiPicker({
    Key key,
    @required this.onEmojiSelected,
    this.columns = 7,
    this.rows = 3,
    this.selectedCategory,
    this.bgColor,
    this.indicatorColor = Colors.blue,
    this.recommendKeywords,
    this.numRecommended = 10,
    this.noRecommendationsText = "No Recommendations",
    this.noRecommendationsStyle,
    this.categoryIcons,
    this.buttonMode = ButtonMode.MATERIAL,
    //this.unavailableEmojiIcon
  }) : super(key: key) {
    if (selectedCategory == null) {
      if (recommendKeywords == null) {
        selectedCategory = Category.SMILEYS;
      } else {
        selectedCategory = Category.RECOMMENDED;
      }
    } else if (recommendKeywords == null &&
        selectedCategory == Category.RECOMMENDED) {
      selectedCategory = Category.SMILEYS;
    }

    if (this.noRecommendationsStyle == null) {
      noRecommendationsStyle = TextStyle(fontSize: 20, color: Colors.black26);
    }

    if (this.bgColor == null) {
      bgColor = _defaultBgColor;
    }

    if (categoryIcons == null) {
      categoryIcons = CategoryIcons();
    }
//    if (unavailableEmojiIcon == null) {
//      unavailableEmojiIcon = Icon(Icons.error_outline, size: 24, color: Color.fromRGBO(211, 211, 211, 1),);
//    }
  }

  final Map<String, String> _smileys = new Map.fromIterables([
    'EMOTIONS 1',
    'Grinning Face With Big Eyes',
    'Grinning Face With Smiling Eyes',
    'Beaming Face With Smiling Eyes',
    'Grinning Squinting Face',
    'Grinning Face With Sweat',
    'Rolling on the Floor Laughing',
    'Face With Tears of Joy',
    'Slightly Smiling Face',
    'Upside-Down Face',
    'Smiling Face With Smiling Eyes',
    'EMOTIONS 2',
    'EMOTIONS 3',
    'Waving Hand',
    'Raised Back of Hand',
    'Hand With Fingers Splayed',
    'Raised Hand',
    'OK Hand',
    'Backhand Index Pointing Left',
    'Backhand Index Pointing Right',
    'Backhand Index Pointing Up',
    'Backhand Index Pointing Down',
    'Thumbs Up',
    'Thumbs Down',
    'EMOTIONS 4',
    'EMOTIONS 5',
    'EMOTIONS 6',
    'EMOTIONS 7',
    'PRIZE 1',
    'PRARTHANA 16'
  ], [
    '😀',
    '😃',
    '😄',
    '😁',
    '😆',
    '😅',
    '🤣',
    '😂',
    '🙂',
    '🙃',
    '😊',
    '😇',
    '😌',
    '👋',
    '🤚',
    '🖐',
    '✋',
    '👌',
    '👈',
    '👉',
    '👆',
    '👇',
    '👍',
    '👎',
    '👏',
    '🙌',
    '👐',
    '🙏',
    '👑',
    '👼'
  ]);

  final Map<String, String> _animals = new Map.fromIterables([
    'PRARTHANA 1',
    'Eagle',
    'Butterfly',
    'FELLOWSHIP Bouquet',
    'Cherry Blossom',
    'White Flower',
    'Rosette',
    'PRARTHANA 2',
    'Wilted Flower',
    'Hibiscus',
    'Sunflower',
    'Blossom',
    'Tulip',
    'Seedling',
    'Evergreen Tree',
    'Deciduous Tree',
    'Sheaf of Rice',
    'Herb',
  ], [
    '🕊',
    '🦅',
    '🦋',
    '💐',
    '🌸',
    '💮',
    '🏵',
    '🌹',
    '🥀',
    '🌺',
    '🌻',
    '🌼',
    '🌷',
    '🌱',
    '🌲',
    '🌳',
    '🌾',
    '🌿',
  ]);

  final Map<String, String> _foods = new Map.fromIterables([
    'Grapes',
    'Bread',
    'Baguette Bread',
    'FELLOWSHIP Birthday Cake',
    'FELLOWSHIP Balloon',
    'Party Popper',
    'Wind Chime',
    'Ribbon',
    'FELLOWSHIP Wrapped Gift',
  ], [
    '🍇',
    '🍞',
    '🥖',
    '🎂',
    '🎈',
    '🎉',
    '🎊',
    '🎀',
    '🎁',
  ]);

  final Map<String, String> _travel = new Map.fromIterables([
    'Mountain',
    'Desert',
    'PRARTHANA 7',
    'PRARTHANA 8',
    'Sunrise Over Mountains',
    'Sunrise',
    'Cityscape at Dusk',
    'PRARTHANA 3',
    'PRARTHANA 4',
    'PRARTHANA 5',
    'Globe With Meridians',
    'New Moon',
    'Full Moon',
    'Crescent Moon',
    'Sun',
    'PRIZE 2',
    'Glowing Star',
    'Shooting Star',
    'Collision',
    'Dashing Away',
    'Sweat Droplets',
    'Cloud',
    'Sun Behind Cloud',
    'Cloud With Lightning and Rain',
    'Sun Behind Small Cloud',
    'Sun Behind Large Cloud',
    'Sun Behind Rain Cloud',
    'Cloud With Rain',
    'Cloud With Snow',
    'Cloud With Lightning',
    'Rainbow',
    'Umbrella With Rain Drops',
    'High Voltage',
    'Snowflake',
    'PRARTHANA 6',
    'Droplet',
    'Water Wave',
    'Christmas Tree',
    'Sparkles',
  ], [
    '🗻',
    '🏜',
    '🏠',
    '⛪',
    '🌄',
    '🌅',
    '🌆',
    '🌍',
    '🌎',
    '🌏',
    '🌐',
    '🌑',
    '🌕',
    '🌙',
    '☀',
    '⭐',
    '🌟',
    '🌠',
    '💥',
    '💫',
    '💦',
    '☁',
    '⛅',
    '⛈',
    '🌤',
    '🌥',
    '🌦',
    '🌧',
    '🌨',
    '🌩',
    '🌈',
    '☔',
    '⚡',
    '❄',
    '🔥',
    '💧',
    '🌊',
    '🎄',
    '✨'
  ]);

  final Map<String, String> _activities = new Map.fromIterables([
    'PRIZE 3',
    'Sports Medal',
    '1st Place Medal',
    '2nd Place Medal',
    '3rd Place Medal',
    'FELLOWSHIP Musical Score',
    'Microphone',
    'Headphone',
    'Saxophone',
    'Guitar',
    'Musical Keyboard',
    'Trumpet',
    'FELLOWSHIP Violin',
    'Drum',
  ], [
    '🏆',
    '🏅',
    '🥇',
    '🥈',
    '🥉',
    '🎼',
    '🎤',
    '🎧',
    '🎷',
    '🎸',
    '🎹',
    '🎺',
    '🎻',
    '🥁',
  ]);

  final Map<String, String> _objects = new Map.fromIterables([
    'World Map',
    'Notification Bell',
    'Hour Glass',
    'Alarm Clock',
    'Mobile Phone',
    'Telephone',
    'Telephone Receiver',
    'Search',
    'PRARTHANA 9',
    'Light Bulb',
    'Flashlight',
    'PRARTHANA 10',
    'PRARTHANA 11',
    'Green Book',
    'Blue Book',
    'Orange Book',
    'Books',
    'Notebook',
    'Page With Curl',
    'Scroll',
    'Page Facing Up',
    'Newspaper',
    'E-Mail',
    'Pencil',
    'Black Nib',
    'Fountain Pen',
    'Pen',
    'Paintbrush',
    'Locked',
    'Unlocked',
    'Key',
    'Shield',
    'Door',
  ], [
    '🗺',
    '🛎',
    '⌛',
    '⏰',
    '📱',
    '☎',
    '📞',
    '🔍',
    '🕯',
    '💡',
    '🔦',
    '📕',
    '📖',
    '📗',
    '📘',
    '📙',
    '📚',
    '📓',
    '📃',
    '📜',
    '📄',
    '📰',
    '✉',
    '🖋',
    '🖊',
    '🖌',
    '🖍',
    '📝',
    '🔒',
    '🔓',
    '🔑',
    '🛡',
    '🚪',
  ]);

  final Map<String, String> _symbols = new Map.fromIterables([
    'Hundred Points',
    'Speech Balloon',
    'Zzz',
    'PRARTHANA 12',
    'Muted Speaker',
    'Speaker Low Volume',
    'Loudspeaker',
    'Megaphone',
    'PRIZE 4',
    'Bell With Slash',
    'Musical Note',
    'Musical Notes',
    'No Entry',
    'Prohibited',
    'PRARTHANA 13',
    'PRARTHANA 14',
    'FELLOWSHIP 15',
    'Stop Button',
    'Dim Button',
    'Bright Button',
    'Antenna Bars',
    'Vibration Mode',
    'Mobile Phone Off',
    'White Heavy Check Mark',
    'Cross Mark',
    'Heavy Plus Sign',
    'Heavy Minus Sign',
    'Heavy Division Sign',
    'Question Mark',
    'Exclamation Mark',
    'Keycap Number Sign',
    'Keycap Digit Zero',
    'Keycap Digit One',
    'Keycap Digit Two',
    'Keycap Digit Three',
    'Keycap Digit Four',
    'Keycap Digit Five',
    'Keycap Digit Six',
    'Keycap Digit Seven',
    'Keycap Digit Eight',
    'Keycap Digit Nine',
    'Keycap: 10',
    'Red Circle',
  ], [
    '💯',
    '💬',
    '💤',
    '🕒',
    '🔇',
    '🔈',
    '📢',
    '📣',
    '🔔',
    '🔕',
    '🎵',
    '🎶',
    '⛔',
    '🚫',
    '🛐',
    '✝',
    '▶',
    '⏹',
    '🔅',
    '🔆',
    '📶',
    '📳',
    '📴',
    '✅',
    '❌',
    '➕',
    '➖',
    '➗',
    '❓',
    '❗',
    '#️⃣',
    '0️⃣',
    '1️⃣',
    '2️⃣',
    '3️⃣',
    '4️⃣',
    '5️⃣',
    '6️⃣',
    '7️⃣',
    '8️⃣',
    '9️⃣',
    '🔟',
    '🔴',
  ]);
  final Map<String, String> _flags = new Map.fromIterables([
    'Chequered Flag',
    'Triangular Flag',
    'Crossed Flags',
    'Black Flag',
    'White Flag',
    'Flag: Ascension Island',
    'Flag: Andorra',
    'Flag: United Arab Emirates',
    'Flag: Afghanistan',
    'Flag: Antigua &amp; Barbuda',
    'Flag: Anguilla',
    'Flag: Albania',
    'Flag: Armenia',
    'Flag: Angola',
    'Flag: Antarctica',
    'Flag: Argentina',
    'Flag: American Samoa',
    'Flag: Austria',
    'Flag: Australia',
    'Flag: Aruba',
    'Flag: Åland Islands',
    'Flag: Azerbaijan',
    'Flag: Bosnia &amp; Herzegovina',
    'Flag: Barbados',
    'Flag: Bangladesh',
    'Flag: Belgium',
    'Flag: Burkina Faso',
    'Flag: Bulgaria',
    'Flag: Bahrain',
    'Flag: Burundi',
    'Flag: Benin',
    'Flag: St. Barthélemy',
    'Flag: Bermuda',
    'Flag: Brunei',
    'Flag: Bolivia',
    'Flag: Caribbean Netherlands',
    'Flag: Brazil',
    'Flag: Bahamas',
    'Flag: Bhutan',
    'Flag: Bouvet Island',
    'Flag: Botswana',
    'Flag: Belarus',
    'Flag: Belize',
    'Flag: Canada',
    'Flag: Cocos (Keeling) Islands',
    'Flag: Congo - Kinshasa',
    'Flag: Central African Republic',
    'Flag: Congo - Brazzaville',
    'Flag: Switzerland',
    'Flag: Côte d’Ivoire',
    'Flag: Cook Islands',
    'Flag: Chile',
    'Flag: Cameroon',
    'Flag: China',
    'Flag: Colombia',
    'Flag: Clipperton Island',
    'Flag: Costa Rica',
    'Flag: Cuba',
    'Flag: Cape Verde',
    'Flag: Curaçao',
    'Flag: Christmas Island',
    'Flag: Cyprus',
    'Flag: Czechia',
    'Flag: Germany',
    'Flag: Diego Garcia',
    'Flag: Djibouti',
    'Flag: Denmark',
    'Flag: Dominica',
    'Flag: Dominican Republic',
    'Flag: Algeria',
    'Flag: Ceuta &amp; Melilla',
    'Flag: Ecuador',
    'Flag: Estonia',
    'Flag: Egypt',
    'Flag: Western Sahara',
    'Flag: Eritrea',
    'Flag: Spain',
    'Flag: Ethiopia',
    'Flag: European Union',
    'Flag: Finland',
    'Flag: Fiji',
    'Flag: Falkland Islands',
    'Flag: Micronesia',
    'Flag: Faroe Islands',
    'Flag: France',
    'Flag: Gabon',
    'Flag: United Kingdom',
    'Flag: Grenada',
    'Flag: Georgia',
    'Flag: French Guiana',
    'Flag: Guernsey',
    'Flag: Ghana',
    'Flag: Gibraltar',
    'Flag: Greenland',
    'Flag: Gambia',
    'Flag: Guinea',
    'Flag: Guadeloupe',
    'Flag: Equatorial Guinea',
    'Flag: Greece',
    'Flag: South Georgia &amp; South Sandwich Islands',
    'Flag: Guatemala',
    'Flag: Guam',
    'Flag: Guinea-Bissau',
    'Flag: Guyana',
    'Flag: Hong Kong SAR China',
    'Flag: Heard &amp; McDonald Islands',
    'Flag: Honduras',
    'Flag: Croatia',
    'Flag: Haiti',
    'Flag: Hungary',
    'Flag: Canary Islands',
    'Flag: Indonesia',
    'Flag: Ireland',
    'Flag: Israel',
    'Flag: Isle of Man',
    'Flag: India',
    'Flag: British Indian Ocean Territory',
    'Flag: Iraq',
    'Flag: Iran',
    'Flag: Iceland',
    'Flag: Italy',
    'Flag: Jersey',
    'Flag: Jamaica',
    'Flag: Jordan',
    'Flag: Japan',
    'Flag: Kenya',
    'Flag: Kyrgyzstan',
    'Flag: Cambodia',
    'Flag: Kiribati',
    'Flag: Comoros',
    'Flag: St. Kitts &amp; Nevis',
    'Flag: North Korea',
    'Flag: South Korea',
    'Flag: Kuwait',
    'Flag: Cayman Islands',
    'Flag: Kazakhstan',
    'Flag: Laos',
    'Flag: Lebanon',
    'Flag: St. Lucia',
    'Flag: Liechtenstein',
    'Flag: Sri Lanka',
    'Flag: Liberia',
    'Flag: Lesotho',
    'Flag: Lithuania',
    'Flag: Luxembourg',
    'Flag: Latvia',
    'Flag: Libya',
    'Flag: Morocco',
    'Flag: Monaco',
    'Flag: Moldova',
    'Flag: Montenegro',
    'Flag: St. Martin',
    'Flag: Madagascar',
    'Flag: Marshall Islands',
    'Flag: North Macedonia',
    'Flag: Mali',
    'Flag: Myanmar (Burma)',
    'Flag: Mongolia',
    'Flag: Macau Sar China',
    'Flag: Northern Mariana Islands',
    'Flag: Martinique',
    'Flag: Mauritania',
    'Flag: Montserrat',
    'Flag: Malta',
    'Flag: Mauritius',
    'Flag: Maldives',
    'Flag: Malawi',
    'Flag: Mexico',
    'Flag: Malaysia',
    'Flag: Mozambique',
    'Flag: Namibia',
    'Flag: New Caledonia',
    'Flag: Niger',
    'Flag: Norfolk Island',
    'Flag: Nigeria',
    'Flag: Nicaragua',
    'Flag: Netherlands',
    'Flag: Norway',
    'Flag: Nepal',
    'Flag: Nauru',
    'Flag: Niue',
    'Flag: New Zealand',
    'Flag: Oman',
    'Flag: Panama',
    'Flag: Peru',
    'Flag: French Polynesia',
    'Flag: Papua New Guinea',
    'Flag: Philippines',
    'Flag: Pakistan',
    'Flag: Poland',
    'Flag: St. Pierre &amp; Miquelon',
    'Flag: Pitcairn Islands',
    'Flag: Puerto Rico',
    'Flag: Palestinian Territories',
    'Flag: Portugal',
    'Flag: Palau',
    'Flag: Paraguay',
    'Flag: Qatar',
    'Flag: Réunion',
    'Flag: Romania',
    'Flag: Serbia',
    'Flag: Russia',
    'Flag: Rwanda',
    'Flag: Saudi Arabia',
    'Flag: Solomon Islands',
    'Flag: Seychelles',
    'Flag: Sudan',
    'Flag: Sweden',
    'Flag: Singapore',
    'Flag: St. Helena',
    'Flag: Slovenia',
    'Flag: Svalbard &amp; Jan Mayen',
    'Flag: Slovakia',
    'Flag: Sierra Leone',
    'Flag: San Marino',
    'Flag: Senegal',
    'Flag: Somalia',
    'Flag: Suriname',
    'Flag: South Sudan',
    'Flag: São Tomé &amp; Príncipe',
    'Flag: El Salvador',
    'Flag: Sint Maarten',
    'Flag: Syria',
    'Flag: Swaziland',
    'Flag: Tristan Da Cunha',
    'Flag: Turks &amp; Caicos Islands',
    'Flag: Chad',
    'Flag: French Southern Territories',
    'Flag: Togo',
    'Flag: Thailand',
    'Flag: Tajikistan',
    'Flag: Tokelau',
    'Flag: Timor-Leste',
    'Flag: Turkmenistan',
    'Flag: Tunisia',
    'Flag: Tonga',
    'Flag: Turkey',
    'Flag: Trinidad &amp; Tobago',
    'Flag: Tuvalu',
    'Flag: Taiwan',
    'Flag: Tanzania',
    'Flag: Ukraine',
    'Flag: Uganda',
    'Flag: U.S. Outlying Islands',
    'Flag: United Nations',
    'Flag: United States',
    'Flag: Uruguay',
    'Flag: Uzbekistan',
    'Flag: Vatican City',
    'Flag: St. Vincent &amp; Grenadines',
    'Flag: Venezuela',
    'Flag: British Virgin Islands',
    'Flag: U.S. Virgin Islands',
    'Flag: Vietnam',
    'Flag: Vanuatu',
    'Flag: Wallis &amp; Futuna',
    'Flag: Samoa',
    'Flag: Kosovo',
    'Flag: Yemen',
    'Flag: Mayotte',
    'Flag: South Africa',
    'Flag: Zambia',
    'Flag: Zimbabwe'
  ], [
    '🏁',
    '🚩',
    '🎌',
    '🏴',
    '🏳',
    '🇦🇨',
    '🇦🇩',
    '🇦🇪',
    '🇦🇫',
    '🇦🇬',
    '🇦🇮',
    '🇦🇱',
    '🇦🇲',
    '🇦🇴',
    '🇦🇶',
    '🇦🇷',
    '🇦🇸',
    '🇦🇹',
    '🇦🇺',
    '🇦🇼',
    '🇦🇽',
    '🇦🇿',
    '🇧🇦',
    '🇧🇧',
    '🇧🇩',
    '🇧🇪',
    '🇧🇫',
    '🇧🇬',
    '🇧🇭',
    '🇧🇮',
    '🇧🇯',
    '🇧🇱',
    '🇧🇲',
    '🇧🇳',
    '🇧🇴',
    '🇧🇶',
    '🇧🇷',
    '🇧🇸',
    '🇧🇹',
    '🇧🇻',
    '🇧🇼',
    '🇧🇾',
    '🇧🇿',
    '🇨🇦',
    '🇨🇨',
    '🇨🇩',
    '🇨🇫',
    '🇨🇬',
    '🇨🇭',
    '🇨🇮',
    '🇨🇰',
    '🇨🇱',
    '🇨🇲',
    '🇨🇳',
    '🇨🇴',
    '🇨🇵',
    '🇨🇷',
    '🇨🇺',
    '🇨🇻',
    '🇨🇼',
    '🇨🇽',
    '🇨🇾',
    '🇨🇿',
    '🇩🇪',
    '🇩🇬',
    '🇩🇯',
    '🇩🇰',
    '🇩🇲',
    '🇩🇴',
    '🇩🇿',
    '🇪🇦',
    '🇪🇨',
    '🇪🇪',
    '🇪🇬',
    '🇪🇭',
    '🇪🇷',
    '🇪🇸',
    '🇪🇹',
    '🇪🇺',
    '🇫🇮',
    '🇫🇯',
    '🇫🇰',
    '🇫🇲',
    '🇫🇴',
    '🇫🇷',
    '🇬🇦',
    '🇬🇧',
    '🇬🇩',
    '🇬🇪',
    '🇬🇫',
    '🇬🇬',
    '🇬🇭',
    '🇬🇮',
    '🇬🇱',
    '🇬🇲',
    '🇬🇳',
    '🇬🇵',
    '🇬🇶',
    '🇬🇷',
    '🇬🇸',
    '🇬🇹',
    '🇬🇺',
    '🇬🇼',
    '🇬🇾',
    '🇭🇰',
    '🇭🇲',
    '🇭🇳',
    '🇭🇷',
    '🇭🇹',
    '🇭🇺',
    '🇮🇨',
    '🇮🇩',
    '🇮🇪',
    '🇮🇱',
    '🇮🇲',
    '🇮🇳',
    '🇮🇴',
    '🇮🇶',
    '🇮🇷',
    '🇮🇸',
    '🇮🇹',
    '🇯🇪',
    '🇯🇲',
    '🇯🇴',
    '🇯🇵',
    '🇰🇪',
    '🇰🇬',
    '🇰🇭',
    '🇰🇮',
    '🇰🇲',
    '🇰🇳',
    '🇰🇵',
    '🇰🇷',
    '🇰🇼',
    '🇰🇾',
    '🇰🇿',
    '🇱🇦',
    '🇱🇧',
    '🇱🇨',
    '🇱🇮',
    '🇱🇰',
    '🇱🇷',
    '🇱🇸',
    '🇱🇹',
    '🇱🇺',
    '🇱🇻',
    '🇱🇾',
    '🇲🇦',
    '🇲🇨',
    '🇲🇩',
    '🇲🇪',
    '🇲🇫',
    '🇲🇬',
    '🇲🇭',
    '🇲🇰',
    '🇲🇱',
    '🇲🇲',
    '🇲🇳',
    '🇲🇴',
    '🇲🇵',
    '🇲🇶',
    '🇲🇷',
    '🇲🇸',
    '🇲🇹',
    '🇲🇺',
    '🇲🇻',
    '🇲🇼',
    '🇲🇽',
    '🇲🇾',
    '🇲🇿',
    '🇳🇦',
    '🇳🇨',
    '🇳🇪',
    '🇳🇫',
    '🇳🇬',
    '🇳🇮',
    '🇳🇱',
    '🇳🇴',
    '🇳🇵',
    '🇳🇷',
    '🇳🇺',
    '🇳🇿',
    '🇴🇲',
    '🇵🇦',
    '🇵🇪',
    '🇵🇫',
    '🇵🇬',
    '🇵🇭',
    '🇵🇰',
    '🇵🇱',
    '🇵🇲',
    '🇵🇳',
    '🇵🇷',
    '🇵🇸',
    '🇵🇹',
    '🇵🇼',
    '🇵🇾',
    '🇶🇦',
    '🇷🇪',
    '🇷🇴',
    '🇷🇸',
    '🇷🇺',
    '🇷🇼',
    '🇸🇦',
    '🇸🇧',
    '🇸🇨',
    '🇸🇩',
    '🇸🇪',
    '🇸🇬',
    '🇸🇭',
    '🇸🇮',
    '🇸🇯',
    '🇸🇰',
    '🇸🇱',
    '🇸🇲',
    '🇸🇳',
    '🇸🇴',
    '🇸🇷',
    '🇸🇸',
    '🇸🇹',
    '🇸🇻',
    '🇸🇽',
    '🇸🇾',
    '🇸🇿',
    '🇹🇦',
    '🇹🇨',
    '🇹🇩',
    '🇹🇫',
    '🇹🇬',
    '🇹🇭',
    '🇹🇯',
    '🇹🇰',
    '🇹🇱',
    '🇹🇲',
    '🇹🇳',
    '🇹🇴',
    '🇹🇷',
    '🇹🇹',
    '🇹🇻',
    '🇹🇼',
    '🇹🇿',
    '🇺🇦',
    '🇺🇬',
    '🇺🇲',
    '🇺🇳',
    '🇺🇸',
    '🇺🇾',
    '🇺🇿',
    '🇻🇦',
    '🇻🇨',
    '🇻🇪',
    '🇻🇬',
    '🇻🇮',
    '🇻🇳',
    '🇻🇺',
    '🇼🇫',
    '🇼🇸',
    '🇽🇰',
    '🇾🇪',
    '🇾🇹',
    '🇿🇦',
    '🇿🇲',
    '🇿🇼'
  ]);
}

class _Recommended {
  final String name;
  final String emoji;
  final int tier;
  final int numSplitEqualKeyword;
  final int numSplitPartialKeyword;

  _Recommended(
      {this.name,
      this.emoji,
      this.tier,
      this.numSplitEqualKeyword = 0,
      this.numSplitPartialKeyword = 0});
}

class CategoryIcon {
  IconData icon;
  Color color;
  Color selectedColor;

  CategoryIcon({@required this.icon, this.color, this.selectedColor}) {
    if (this.color == null) {
      this.color = Color.fromRGBO(211, 211, 211, 1);
    }
    if (this.selectedColor == null) {
      this.selectedColor = Color.fromRGBO(178, 178, 178, 1);
    }
  }
}

class CategoryIcons {
  CategoryIcon recommendationIcon;
  CategoryIcon smileyIcon;
  CategoryIcon animalIcon;
  CategoryIcon foodIcon;
  CategoryIcon travelIcon;
  CategoryIcon activityIcon;
  CategoryIcon objectIcon;
  CategoryIcon symbolIcon;
  CategoryIcon flagIcon;

  CategoryIcons(
      {this.recommendationIcon,
      this.smileyIcon,
      this.animalIcon,
      this.foodIcon,
      this.travelIcon,
      this.activityIcon,
      this.objectIcon,
      this.symbolIcon,
      this.flagIcon}) {
    if (recommendationIcon == null) {
      recommendationIcon = CategoryIcon(icon: Icons.search);
    }
    if (smileyIcon == null) {
      smileyIcon = CategoryIcon(icon: Icons.tag_faces);
    }
    if (animalIcon == null) {
      animalIcon = CategoryIcon(icon: Icons.local_florist);
    }
    if (foodIcon == null) {
      foodIcon = CategoryIcon(icon: Icons.cake);
    }
    if (travelIcon == null) {
      travelIcon = CategoryIcon(icon: Icons.wb_sunny);
    }
    if (activityIcon == null) {
      activityIcon = CategoryIcon(icon: Icons.music_note);
    }
    if (objectIcon == null) {
      objectIcon = CategoryIcon(icon: Icons.book);
    }
    if (symbolIcon == null) {
      symbolIcon = CategoryIcon(icon: Icons.looks_one);
    }
    if (flagIcon == null) {
      flagIcon = CategoryIcon(icon: Icons.flag);
    }
  }
}

class Emoji {
  final String name;
  final String emoji;

  Emoji({@required this.name, @required this.emoji});

  @override
  String toString() {
    return "Name: " + name + ", Emoji: " + emoji;
  }
}

bool isAvailable(String emoji) {
  return true;
  //TextPainter painter = new TextPainter(text: TextSpan(text: emoji, style: TextStyle(fontSize: 10)), textDirection: TextDirection.ltr)..layout();
  //return painter.maxIntrinsicWidth >= 10 ;
}

class _EmojiPickerState extends State<EmojiPicker> {
  @override
  Widget build(BuildContext context) {
    int recommendedPagesNum = 0;
    List<_Recommended> recommendedEmojis = new List();
    List<Widget> recommendedPages = new List();

    if (widget.recommendKeywords != null) {
      List<String> allNames = new List();
      allNames.addAll(widget._smileys.keys);
      allNames.addAll(widget._animals.keys);
      allNames.addAll(widget._foods.keys);
      allNames.addAll(widget._travel.keys);
      allNames.addAll(widget._activities.keys);
      allNames.addAll(widget._objects.keys);
      allNames.addAll(widget._symbols.keys);
      allNames.addAll(widget._flags.keys);

      List<String> allEmojis = new List();
      allEmojis.addAll(widget._smileys.values);
      allEmojis.addAll(widget._animals.values);
      allEmojis.addAll(widget._foods.values);
      allEmojis.addAll(widget._travel.values);
      allEmojis.addAll(widget._activities.values);
      allEmojis.addAll(widget._objects.values);
      allEmojis.addAll(widget._symbols.values);
      allEmojis.addAll(widget._flags.values);

      allNames.forEach((name) {
        int numSplitEqualKeyword = 0;
        int numSplitPartialKeyword = 0;

        widget.recommendKeywords.forEach((keyword) {
          if (name.toLowerCase() == keyword.toLowerCase()) {
            recommendedEmojis.add(_Recommended(
                name: name, emoji: allEmojis[allNames.indexOf(name)], tier: 1));
          } else {
            List<String> splitName = name.split(" ");

            splitName.forEach((splitName) {
              if (splitName.replaceAll(":", "").toLowerCase() ==
                  keyword.toLowerCase()) {
                numSplitEqualKeyword += 1;
              } else if (splitName
                  .replaceAll(":", "")
                  .toLowerCase()
                  .contains(keyword.toLowerCase())) {
                numSplitPartialKeyword += 1;
              }
            });
          }
        });

        if (numSplitEqualKeyword > 0) {
          if (numSplitEqualKeyword == name.split(" ").length) {
            recommendedEmojis.add(_Recommended(
                name: name, emoji: allEmojis[allNames.indexOf(name)], tier: 1));
          } else {
            recommendedEmojis.add(_Recommended(
                name: name,
                emoji: allEmojis[allNames.indexOf(name)],
                tier: 2,
                numSplitEqualKeyword: numSplitEqualKeyword,
                numSplitPartialKeyword: numSplitPartialKeyword));
          }
        } else if (numSplitPartialKeyword > 0) {
          recommendedEmojis.add(_Recommended(
              name: name,
              emoji: allEmojis[allNames.indexOf(name)],
              tier: 3,
              numSplitPartialKeyword: numSplitPartialKeyword));
        }
      });

      recommendedEmojis.sort((a, b) {
        if (a.tier < b.tier) {
          return -1;
        } else if (a.tier > b.tier) {
          return 1;
        } else {
          if (a.tier == 1) {
            if (a.name.split(" ").length > b.name.split(" ").length) {
              return -1;
            } else if (a.name.split(" ").length < b.name.split(" ").length) {
              return 1;
            } else {
              return 0;
            }
          } else if (a.tier == 2) {
            if (a.numSplitEqualKeyword > b.numSplitEqualKeyword) {
              return -1;
            } else if (a.numSplitEqualKeyword < b.numSplitEqualKeyword) {
              return 1;
            } else {
              if (a.numSplitPartialKeyword > b.numSplitPartialKeyword) {
                return -1;
              } else if (a.numSplitPartialKeyword < b.numSplitPartialKeyword) {
                return 1;
              } else {
                if (a.name.split(" ").length < b.name.split(" ").length) {
                  return -1;
                } else if (a.name.split(" ").length >
                    b.name.split(" ").length) {
                  return 1;
                } else {
                  return 0;
                }
              }
            }
          } else if (a.tier == 3) {
            if (a.numSplitPartialKeyword > b.numSplitPartialKeyword) {
              return -1;
            } else if (a.numSplitPartialKeyword < b.numSplitPartialKeyword) {
              return 1;
            } else {
              return 0;
            }
          }
        }
      });

      if (recommendedEmojis.length > widget.numRecommended) {
        recommendedEmojis =
            recommendedEmojis.getRange(0, widget.numRecommended).toList();
      }

      if (recommendedEmojis.length != 0) {
        recommendedPagesNum =
            (recommendedEmojis.length / (widget.rows * widget.columns)).ceil();

        for (var i = 0; i < recommendedPagesNum; i++) {
          recommendedPages.add(Container(
            color: widget.bgColor,
            child: GridView.count(
              shrinkWrap: true,
              primary: true,
              crossAxisCount: widget.columns,
              children: List.generate(widget.rows * widget.columns, (index) {
                if (index + (widget.columns * widget.rows * i) <
                    recommendedEmojis.length) {
                  switch (widget.buttonMode) {
                    case ButtonMode.MATERIAL:
                      return Center(
                          child: FlatButton(
                        padding: EdgeInsets.all(0),
                        child: Center(
                          child: Text(
                            recommendedEmojis[
                                    index + (widget.columns * widget.rows * i)]
                                .emoji,
                            style: TextStyle(fontSize: 24),
                          ),
                        ),
                        onPressed: () {
                          _Recommended recommended = recommendedEmojis[
                              index + (widget.columns * widget.rows * i)];
                          widget.onEmojiSelected(
                              Emoji(
                                  name: recommended.name,
                                  emoji: recommended.emoji),
                              widget.selectedCategory);
                        },
                      ));
                      break;
                    case ButtonMode.CUPERTINO:
                      return Center(
                          child: CupertinoButton(
                        pressedOpacity: 0.4,
                        padding: EdgeInsets.all(0),
                        child: Center(
                          child: Text(
                            recommendedEmojis[
                                    index + (widget.columns * widget.rows * i)]
                                .emoji,
                            style: TextStyle(fontSize: 24),
                          ),
                        ),
                        onPressed: () {
                          _Recommended recommended = recommendedEmojis[
                              index + (widget.columns * widget.rows * i)];
                          widget.onEmojiSelected(
                              Emoji(
                                  name: recommended.name,
                                  emoji: recommended.emoji),
                              widget.selectedCategory);
                        },
                      ));

                      break;
                  }
                } else {
                  return Container();
                }
              }),
            ),
          ));
        }
      } else {
        recommendedPagesNum = 1;

        if (widget.selectedCategory == Category.RECOMMENDED) {
          widget.selectedCategory = Category.SMILEYS;
        }

        recommendedPages.add(Container(
            color: widget.bgColor,
            child: Center(
                child: Text(
              widget.noRecommendationsText,
              style: widget.noRecommendationsStyle,
            ))));
      }
    }

    int smileyPagesNum = (widget._smileys.values.toList().length /
            (widget.rows * widget.columns))
        .ceil();

    List<Widget> smileyPages = new List();

    for (var i = 0; i < smileyPagesNum; i++) {
      smileyPages.add(Container(
        color: widget.bgColor,
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: widget.columns,
          children: List.generate(widget.rows * widget.columns, (index) {
            if (index + (widget.columns * widget.rows * i) <
                widget._smileys.values.toList().length) {
              String emojiTxt = widget._smileys.values
                  .toList()[index + (widget.columns * widget.rows * i)];

              switch (widget.buttonMode) {
                case ButtonMode.MATERIAL:
                  return Center(
                      child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: isAvailable(emojiTxt)
                          ? Text(
                              emojiTxt,
                              style: TextStyle(fontSize: 24),
                            )
                          : widget.unavailableEmojiIcon,
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._smileys.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._smileys.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
                case ButtonMode.CUPERTINO:
                  return Center(
                      child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: isAvailable(emojiTxt)
                          ? Text(
                              emojiTxt,
                              style: TextStyle(fontSize: 24),
                            )
                          : widget.unavailableEmojiIcon,
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._smileys.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._smileys.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
              }
            } else {
              return Container();
            }
          }),
        ),
      ));
    }

    int animalPagesNum = (widget._animals.values.toList().length /
            (widget.rows * widget.columns))
        .ceil();

    List<Widget> animalPages = new List();

    for (var i = 0; i < animalPagesNum; i++) {
      animalPages.add(Container(
        color: widget.bgColor,
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: widget.columns,
          children: List.generate(widget.rows * widget.columns, (index) {
            if (index + (widget.columns * widget.rows * i) <
                widget._animals.values.toList().length) {
              switch (widget.buttonMode) {
                case ButtonMode.MATERIAL:
                  return Center(
                      child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._animals.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._animals.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._animals.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
                case ButtonMode.CUPERTINO:
                  return Center(
                      child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._animals.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._animals.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._animals.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
              }
            } else {
              return Container();
            }
          }),
        ),
      ));
    }

    int foodPagesNum =
        (widget._foods.values.toList().length / (widget.rows * widget.columns))
            .ceil();

    List<Widget> foodPages = new List();

    for (var i = 0; i < foodPagesNum; i++) {
      foodPages.add(Container(
        color: widget.bgColor,
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: widget.columns,
          children: List.generate(widget.rows * widget.columns, (index) {
            if (index + (widget.columns * widget.rows * i) <
                widget._foods.values.toList().length) {
              switch (widget.buttonMode) {
                case ButtonMode.MATERIAL:
                  return Center(
                      child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._foods.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._foods.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._foods.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
                case ButtonMode.CUPERTINO:
                  return Center(
                      child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._foods.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._foods.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._foods.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
              }
            } else {
              return Container();
            }
          }),
        ),
      ));
    }

    int travelPagesNum =
        (widget._travel.values.toList().length / (widget.rows * widget.columns))
            .ceil();

    List<Widget> travelPages = new List();

    for (var i = 0; i < travelPagesNum; i++) {
      travelPages.add(Container(
        color: widget.bgColor,
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: widget.columns,
          children: List.generate(widget.rows * widget.columns, (index) {
            if (index + (widget.columns * widget.rows * i) <
                widget._travel.values.toList().length) {
              switch (widget.buttonMode) {
                case ButtonMode.MATERIAL:
                  return Center(
                      child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._travel.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._travel.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._travel.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
                case ButtonMode.CUPERTINO:
                  return Center(
                      child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._travel.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._travel.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._travel.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
              }
            } else {
              return Container();
            }
          }),
        ),
      ));
    }

    int activityPagesNum = (widget._activities.values.toList().length /
            (widget.rows * widget.columns))
        .ceil();

    List<Widget> activityPages = new List();

    for (var i = 0; i < activityPagesNum; i++) {
      activityPages.add(Container(
        color: widget.bgColor,
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: widget.columns,
          children: List.generate(widget.rows * widget.columns, (index) {
            if (index + (widget.columns * widget.rows * i) <
                widget._activities.values.toList().length) {
              String emojiTxt = widget._activities.values
                  .toList()[index + (widget.columns * widget.rows * i)];

              switch (widget.buttonMode) {
                case ButtonMode.MATERIAL:
                  return Center(
                      child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._activities.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._activities.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._activities.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
                case ButtonMode.CUPERTINO:
                  return Center(
                      child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: isAvailable(emojiTxt)
                          ? Text(
                              emojiTxt,
                              style: TextStyle(fontSize: 24),
                            )
                          : widget.unavailableEmojiIcon,
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._activities.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._activities.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
              }
            } else {
              return Container();
            }
          }),
        ),
      ));
    }

    int objectPagesNum = (widget._objects.values.toList().length /
            (widget.rows * widget.columns))
        .ceil();

    List<Widget> objectPages = new List();

    for (var i = 0; i < objectPagesNum; i++) {
      objectPages.add(Container(
        color: widget.bgColor,
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: widget.columns,
          children: List.generate(widget.rows * widget.columns, (index) {
            if (index + (widget.columns * widget.rows * i) <
                widget._objects.values.toList().length) {
              switch (widget.buttonMode) {
                case ButtonMode.MATERIAL:
                  return Center(
                      child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._objects.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._objects.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._objects.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
                case ButtonMode.CUPERTINO:
                  return Center(
                      child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._objects.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._objects.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._objects.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
              }
            } else {
              return Container();
            }
          }),
        ),
      ));
    }

    int symbolPagesNum = (widget._symbols.values.toList().length /
            (widget.rows * widget.columns))
        .ceil();

    List<Widget> symbolPages = new List();

    for (var i = 0; i < symbolPagesNum; i++) {
      symbolPages.add(Container(
        color: widget.bgColor,
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: widget.columns,
          children: List.generate(widget.rows * widget.columns, (index) {
            if (index + (widget.columns * widget.rows * i) <
                widget._symbols.values.toList().length) {
              switch (widget.buttonMode) {
                case ButtonMode.MATERIAL:
                  return Center(
                      child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._symbols.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._symbols.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._symbols.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
                case ButtonMode.CUPERTINO:
                  return Center(
                      child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._symbols.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._symbols.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._symbols.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
              }
            } else {
              return Container();
            }
          }),
        ),
      ));
    }

    int flagPagesNum =
        (widget._flags.values.toList().length / (widget.rows * widget.columns))
            .ceil();

    List<Widget> flagPages = new List();

    for (var i = 0; i < flagPagesNum; i++) {
      flagPages.add(Container(
        color: widget.bgColor,
        child: GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: widget.columns,
          children: List.generate(widget.rows * widget.columns, (index) {
            if (index + (widget.columns * widget.rows * i) <
                widget._flags.values.toList().length) {
              switch (widget.buttonMode) {
                case ButtonMode.MATERIAL:
                  return Center(
                      child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._flags.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._flags.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._flags.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
                case ButtonMode.CUPERTINO:
                  return Center(
                      child: CupertinoButton(
                    pressedOpacity: 0.4,
                    padding: EdgeInsets.all(0),
                    child: Center(
                      child: Text(
                        widget._flags.values.toList()[
                            index + (widget.columns * widget.rows * i)],
                        style: TextStyle(fontSize: 24),
                      ),
                    ),
                    onPressed: () {
                      widget.onEmojiSelected(
                          Emoji(
                              name: widget._flags.keys.toList()[
                                  index + (widget.columns * widget.rows * i)],
                              emoji: widget._flags.values.toList()[
                                  index + (widget.columns * widget.rows * i)]),
                          widget.selectedCategory);
                    },
                  ));
                  break;
              }
            } else {
              return Container();
            }
          }),
        ),
      ));
    }

    List<Widget> pages = new List();
    pages.addAll(recommendedPages);
    pages.addAll(smileyPages);
    pages.addAll(animalPages);
    pages.addAll(foodPages);
    pages.addAll(travelPages);
    pages.addAll(activityPages);
    pages.addAll(objectPages);
    pages.addAll(symbolPages);
    pages.addAll(flagPages);

    PageController pageController;
    if (widget.selectedCategory == Category.RECOMMENDED) {
      pageController = PageController(initialPage: 0);
    } else if (widget.selectedCategory == Category.SMILEYS) {
      pageController = PageController(initialPage: recommendedPagesNum);
    } else if (widget.selectedCategory == Category.ANIMALS) {
      pageController =
          PageController(initialPage: smileyPagesNum + recommendedPagesNum);
    } else if (widget.selectedCategory == Category.FOODS) {
      pageController = PageController(
          initialPage: smileyPagesNum + animalPagesNum + recommendedPagesNum);
    } else if (widget.selectedCategory == Category.TRAVEL) {
      pageController = PageController(
          initialPage: smileyPagesNum +
              animalPagesNum +
              foodPagesNum +
              recommendedPagesNum);
    } else if (widget.selectedCategory == Category.ACTIVITIES) {
      pageController = PageController(
          initialPage: smileyPagesNum +
              animalPagesNum +
              foodPagesNum +
              travelPagesNum +
              recommendedPagesNum);
    } else if (widget.selectedCategory == Category.OBJECTS) {
      pageController = PageController(
          initialPage: smileyPagesNum +
              animalPagesNum +
              foodPagesNum +
              travelPagesNum +
              activityPagesNum +
              recommendedPagesNum);
    } else if (widget.selectedCategory == Category.SYMBOLS) {
      pageController = PageController(
          initialPage: smileyPagesNum +
              animalPagesNum +
              foodPagesNum +
              travelPagesNum +
              activityPagesNum +
              objectPagesNum +
              recommendedPagesNum);
    } else if (widget.selectedCategory == Category.FLAGS) {
      pageController = PageController(
          initialPage: smileyPagesNum +
              animalPagesNum +
              foodPagesNum +
              travelPagesNum +
              activityPagesNum +
              objectPagesNum +
              symbolPagesNum +
              recommendedPagesNum);
    }
    pageController.addListener(() {
      setState(() {});
    });

    return Column(
      children: <Widget>[
        SizedBox(
          height: (MediaQuery.of(context).size.width / widget.columns) *
              widget.rows,
          width: MediaQuery.of(context).size.width,
          child: PageView(
              children: pages,
              controller: pageController,
              onPageChanged: (index) {
                if (widget.recommendKeywords != null &&
                    index < recommendedPagesNum) {
                  widget.selectedCategory = Category.RECOMMENDED;
                } else if (index < smileyPagesNum + recommendedPagesNum) {
                  widget.selectedCategory = Category.SMILEYS;
                } else if (index <
                    smileyPagesNum + animalPagesNum + recommendedPagesNum) {
                  widget.selectedCategory = Category.ANIMALS;
                } else if (index <
                    smileyPagesNum +
                        animalPagesNum +
                        foodPagesNum +
                        recommendedPagesNum) {
                  widget.selectedCategory = Category.FOODS;
                } else if (index <
                    smileyPagesNum +
                        animalPagesNum +
                        foodPagesNum +
                        travelPagesNum +
                        recommendedPagesNum) {
                  widget.selectedCategory = Category.TRAVEL;
                } else if (index <
                    smileyPagesNum +
                        animalPagesNum +
                        foodPagesNum +
                        travelPagesNum +
                        activityPagesNum +
                        recommendedPagesNum) {
                  widget.selectedCategory = Category.ACTIVITIES;
                } else if (index <
                    smileyPagesNum +
                        animalPagesNum +
                        foodPagesNum +
                        travelPagesNum +
                        activityPagesNum +
                        objectPagesNum +
                        recommendedPagesNum) {
                  widget.selectedCategory = Category.OBJECTS;
                } else if (index <
                    smileyPagesNum +
                        animalPagesNum +
                        foodPagesNum +
                        travelPagesNum +
                        activityPagesNum +
                        objectPagesNum +
                        symbolPagesNum +
                        recommendedPagesNum) {
                  widget.selectedCategory = Category.SYMBOLS;
                } else {
                  widget.selectedCategory = Category.FLAGS;
                }
                setState(() {});
              }),
        ),
        Container(
            color: widget.bgColor,
            height: 6,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 4, bottom: 0, right: 2, left: 2),
            child: CustomPaint(
              painter: _ProgressPainter(
                  context,
                  pageController,
                  new Map.fromIterables([
                    Category.RECOMMENDED,
                    Category.SMILEYS,
                    Category.ANIMALS,
                    Category.FOODS,
                    Category.TRAVEL,
                    Category.ACTIVITIES,
                    Category.OBJECTS,
                    Category.SYMBOLS,
                    Category.FLAGS
                  ], [
                    recommendedPagesNum,
                    smileyPagesNum,
                    animalPagesNum,
                    foodPagesNum,
                    travelPagesNum,
                    activityPagesNum,
                    objectPagesNum,
                    symbolPagesNum,
                    flagPagesNum
                  ]),
                  widget.selectedCategory,
                  widget.indicatorColor),
            )),
        Container(
            height: 50,
            color: widget.bgColor,
            child: Row(
              children: <Widget>[
                widget.recommendKeywords != null
                    ? SizedBox(
                        width: MediaQuery.of(context).size.width / 9,
                        height: MediaQuery.of(context).size.width / 9,
                        child: widget.buttonMode == ButtonMode.MATERIAL
                            ? FlatButton(
                                padding: EdgeInsets.all(0),
                                color: widget.selectedCategory ==
                                        Category.RECOMMENDED
                                    ? Colors.black12
                                    : Colors.transparent,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(0))),
                                child: Center(
                                  child: Icon(
                                    widget
                                        .categoryIcons.recommendationIcon.icon,
                                    size: 22,
                                    color: widget.selectedCategory ==
                                            Category.RECOMMENDED
                                        ? widget.categoryIcons
                                            .recommendationIcon.selectedColor
                                        : widget.categoryIcons
                                            .recommendationIcon.color,
                                  ),
                                ),
                                onPressed: () {
                                  if (widget.selectedCategory ==
                                      Category.RECOMMENDED) {
                                    return;
                                  }

                                  pageController.jumpToPage(0);
                                },
                              )
                            : CupertinoButton(
                                pressedOpacity: 0.4,
                                padding: EdgeInsets.all(0),
                                color: widget.selectedCategory ==
                                        Category.RECOMMENDED
                                    ? Colors.black12
                                    : Colors.transparent,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(0)),
                                child: Center(
                                  child: Icon(
                                    widget
                                        .categoryIcons.recommendationIcon.icon,
                                    size: 22,
                                    color: widget.selectedCategory ==
                                            Category.RECOMMENDED
                                        ? widget.categoryIcons
                                            .recommendationIcon.selectedColor
                                        : widget.categoryIcons
                                            .recommendationIcon.color,
                                  ),
                                ),
                                onPressed: () {
                                  if (widget.selectedCategory ==
                                      Category.RECOMMENDED) {
                                    return;
                                  }

                                  pageController.jumpToPage(0);
                                },
                              ),
                      )
                    : Container(),
                SizedBox(
                  width: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  height: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  child: widget.buttonMode == ButtonMode.MATERIAL
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.SMILEYS
                              ? Colors.black12
                              : Colors.transparent,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.smileyIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.SMILEYS
                                  ? widget
                                      .categoryIcons.smileyIcon.selectedColor
                                  : widget.categoryIcons.smileyIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.SMILEYS) {
                              return;
                            }

                            pageController.jumpToPage(0 + recommendedPagesNum);
                          },
                        )
                      : CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.SMILEYS
                              ? Colors.black12
                              : Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.smileyIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.SMILEYS
                                  ? widget
                                      .categoryIcons.smileyIcon.selectedColor
                                  : widget.categoryIcons.smileyIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.SMILEYS) {
                              return;
                            }

                            pageController.jumpToPage(0 + recommendedPagesNum);
                          },
                        ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  height: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  child: widget.buttonMode == ButtonMode.MATERIAL
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.ANIMALS
                              ? Colors.black12
                              : Colors.transparent,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.animalIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.ANIMALS
                                  ? widget
                                      .categoryIcons.animalIcon.selectedColor
                                  : widget.categoryIcons.animalIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.ANIMALS) {
                              return;
                            }

                            pageController.jumpToPage(
                                smileyPagesNum + recommendedPagesNum);
                          },
                        )
                      : CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.ANIMALS
                              ? Colors.black12
                              : Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.animalIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.ANIMALS
                                  ? widget
                                      .categoryIcons.animalIcon.selectedColor
                                  : widget.categoryIcons.animalIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.ANIMALS) {
                              return;
                            }

                            pageController.jumpToPage(
                                smileyPagesNum + recommendedPagesNum);
                          },
                        ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  height: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  child: widget.buttonMode == ButtonMode.MATERIAL
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.FOODS
                              ? Colors.black12
                              : Colors.transparent,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.foodIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.FOODS
                                  ? widget.categoryIcons.foodIcon.selectedColor
                                  : widget.categoryIcons.foodIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.FOODS) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                recommendedPagesNum);
                          },
                        )
                      : CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.FOODS
                              ? Colors.black12
                              : Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.foodIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.FOODS
                                  ? widget.categoryIcons.foodIcon.selectedColor
                                  : widget.categoryIcons.foodIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.FOODS) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                recommendedPagesNum);
                          },
                        ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  height: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  child: widget.buttonMode == ButtonMode.MATERIAL
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.TRAVEL
                              ? Colors.black12
                              : Colors.transparent,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.travelIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.TRAVEL
                                  ? widget
                                      .categoryIcons.travelIcon.selectedColor
                                  : widget.categoryIcons.travelIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.TRAVEL) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                recommendedPagesNum);
                          },
                        )
                      : CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.TRAVEL
                              ? Colors.black12
                              : Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.travelIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.TRAVEL
                                  ? widget
                                      .categoryIcons.travelIcon.selectedColor
                                  : widget.categoryIcons.travelIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.TRAVEL) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                recommendedPagesNum);
                          },
                        ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  height: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  child: widget.buttonMode == ButtonMode.MATERIAL
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.ACTIVITIES
                              ? Colors.black12
                              : Colors.transparent,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.activityIcon.icon,
                              size: 22,
                              color:
                                  widget.selectedCategory == Category.ACTIVITIES
                                      ? widget.categoryIcons.activityIcon
                                          .selectedColor
                                      : widget.categoryIcons.activityIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory ==
                                Category.ACTIVITIES) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                travelPagesNum +
                                recommendedPagesNum);
                          },
                        )
                      : CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.ACTIVITIES
                              ? Colors.black12
                              : Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.activityIcon.icon,
                              size: 22,
                              color:
                                  widget.selectedCategory == Category.ACTIVITIES
                                      ? widget.categoryIcons.activityIcon
                                          .selectedColor
                                      : widget.categoryIcons.activityIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory ==
                                Category.ACTIVITIES) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                travelPagesNum +
                                recommendedPagesNum);
                          },
                        ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  height: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  child: widget.buttonMode == ButtonMode.MATERIAL
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.OBJECTS
                              ? Colors.black12
                              : Colors.transparent,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.objectIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.OBJECTS
                                  ? widget
                                      .categoryIcons.objectIcon.selectedColor
                                  : widget.categoryIcons.objectIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.OBJECTS) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                activityPagesNum +
                                travelPagesNum +
                                recommendedPagesNum);
                          },
                        )
                      : CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.OBJECTS
                              ? Colors.black12
                              : Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.objectIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.OBJECTS
                                  ? widget
                                      .categoryIcons.objectIcon.selectedColor
                                  : widget.categoryIcons.objectIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.OBJECTS) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                activityPagesNum +
                                travelPagesNum +
                                recommendedPagesNum);
                          },
                        ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  height: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  child: widget.buttonMode == ButtonMode.MATERIAL
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.SYMBOLS
                              ? Colors.black12
                              : Colors.transparent,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.symbolIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.SYMBOLS
                                  ? widget
                                      .categoryIcons.symbolIcon.selectedColor
                                  : widget.categoryIcons.symbolIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.SYMBOLS) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                activityPagesNum +
                                travelPagesNum +
                                objectPagesNum +
                                recommendedPagesNum);
                          },
                        )
                      : CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.SYMBOLS
                              ? Colors.black12
                              : Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.symbolIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.SYMBOLS
                                  ? widget
                                      .categoryIcons.symbolIcon.selectedColor
                                  : widget.categoryIcons.symbolIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.SYMBOLS) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                activityPagesNum +
                                travelPagesNum +
                                objectPagesNum +
                                recommendedPagesNum);
                          },
                        ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  height: MediaQuery.of(context).size.width /
                      (widget.recommendKeywords == null ? 8 : 9),
                  child: widget.buttonMode == ButtonMode.MATERIAL
                      ? FlatButton(
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.FLAGS
                              ? Colors.black12
                              : Colors.transparent,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0))),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.flagIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.FLAGS
                                  ? widget.categoryIcons.flagIcon.selectedColor
                                  : widget.categoryIcons.flagIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.FLAGS) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                activityPagesNum +
                                travelPagesNum +
                                objectPagesNum +
                                symbolPagesNum +
                                recommendedPagesNum);
                          },
                        )
                      : CupertinoButton(
                          pressedOpacity: 0.4,
                          padding: EdgeInsets.all(0),
                          color: widget.selectedCategory == Category.FLAGS
                              ? Colors.black12
                              : Colors.transparent,
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                          child: Center(
                            child: Icon(
                              widget.categoryIcons.flagIcon.icon,
                              size: 22,
                              color: widget.selectedCategory == Category.FLAGS
                                  ? widget.categoryIcons.flagIcon.selectedColor
                                  : widget.categoryIcons.flagIcon.color,
                            ),
                          ),
                          onPressed: () {
                            if (widget.selectedCategory == Category.FLAGS) {
                              return;
                            }

                            pageController.jumpToPage(smileyPagesNum +
                                animalPagesNum +
                                foodPagesNum +
                                activityPagesNum +
                                travelPagesNum +
                                objectPagesNum +
                                symbolPagesNum +
                                recommendedPagesNum);
                          },
                        ),
                ),
              ],
            ))
      ],
    );
  }
}

class _ProgressPainter extends CustomPainter {
  final BuildContext context;
  final PageController pageController;
  final Map<Category, int> pages;
  final Category selectedCategory;
  final Color indicatorColor;

  _ProgressPainter(this.context, this.pageController, this.pages,
      this.selectedCategory, this.indicatorColor);

  @override
  void paint(Canvas canvas, Size size) {
    double actualPageWidth = MediaQuery.of(context).size.width;
    double offsetInPages = 0;
    if (selectedCategory == Category.RECOMMENDED) {
      offsetInPages = pageController.offset / actualPageWidth;
    } else if (selectedCategory == Category.SMILEYS) {
      offsetInPages = (pageController.offset -
              (pages[Category.RECOMMENDED] * actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.ANIMALS) {
      offsetInPages = (pageController.offset -
              ((pages[Category.RECOMMENDED] + pages[Category.SMILEYS]) *
                  actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.FOODS) {
      offsetInPages = (pageController.offset -
              ((pages[Category.RECOMMENDED] +
                      pages[Category.SMILEYS] +
                      pages[Category.ANIMALS]) *
                  actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.TRAVEL) {
      offsetInPages = (pageController.offset -
              ((pages[Category.RECOMMENDED] +
                      pages[Category.SMILEYS] +
                      pages[Category.ANIMALS] +
                      pages[Category.FOODS]) *
                  actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.ACTIVITIES) {
      offsetInPages = (pageController.offset -
              ((pages[Category.RECOMMENDED] +
                      pages[Category.SMILEYS] +
                      pages[Category.ANIMALS] +
                      pages[Category.FOODS] +
                      pages[Category.TRAVEL]) *
                  actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.OBJECTS) {
      offsetInPages = (pageController.offset -
              ((pages[Category.RECOMMENDED] +
                      pages[Category.SMILEYS] +
                      pages[Category.ANIMALS] +
                      pages[Category.FOODS] +
                      pages[Category.TRAVEL] +
                      pages[Category.ACTIVITIES]) *
                  actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.SYMBOLS) {
      offsetInPages = (pageController.offset -
              ((pages[Category.RECOMMENDED] +
                      pages[Category.SMILEYS] +
                      pages[Category.ANIMALS] +
                      pages[Category.FOODS] +
                      pages[Category.TRAVEL] +
                      pages[Category.ACTIVITIES] +
                      pages[Category.OBJECTS]) *
                  actualPageWidth)) /
          actualPageWidth;
    } else if (selectedCategory == Category.FLAGS) {
      offsetInPages = (pageController.offset -
              ((pages[Category.RECOMMENDED] +
                      pages[Category.SMILEYS] +
                      pages[Category.ANIMALS] +
                      pages[Category.FOODS] +
                      pages[Category.TRAVEL] +
                      pages[Category.ACTIVITIES] +
                      pages[Category.OBJECTS] +
                      pages[Category.SYMBOLS]) *
                  actualPageWidth)) /
          actualPageWidth;
    }
    double indicatorPageWidth = size.width / pages[selectedCategory];

    Rect bgRect = Offset(0, 0) & size;

    Rect indicator = Offset(max(0, offsetInPages * indicatorPageWidth), 0) &
        Size(
            indicatorPageWidth -
                max(
                    0,
                    (indicatorPageWidth +
                            (offsetInPages * indicatorPageWidth)) -
                        size.width) +
                min(0, offsetInPages * indicatorPageWidth),
            size.height);

    canvas.drawRect(bgRect, Paint()..color = Colors.black12);
    canvas.drawRect(indicator, Paint()..color = indicatorColor);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
