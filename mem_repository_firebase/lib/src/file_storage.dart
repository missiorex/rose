// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:mem_entities/mem_entities.dart';

/// Loads and saves a List of Todos using a text file stored on the device.
///
/// Note: This class has no direct dependencies on any Flutter dependencies.
/// Instead, the `getDirectory` method should be injected. This allows for
/// testing.
class FileStorage {
  final String tag;
  final Future<Directory> Function() getDirectory;

  const FileStorage(
    this.tag,
    this.getDirectory,
  );

  Future<List<MessageEntity>> loadMessages() async {
    final file = await _getLocalFile();
    final string = await file.readAsString();
    final json = JsonDecoder().convert(string);
    final msgs = (json['msgs'])
        .map<MessageEntity>((msg) => MessageEntity.fromJson(msg))
        .toList();

    return msgs;
  }

  Future<File> saveMessages(List<MessageEntity> msgs) async {
    final file = await _getLocalFile();

    return file.writeAsString(JsonEncoder().convert({
      'msgs': msgs.map((msg) => msg.toJson()).toList(),
    }));

//    return file.writeAsString(JsonEncoder().convert({
//      'msgs': msgs.map((msg) {
//        json.encode(msg, toEncodable: (value) {
//          if (value is DateTime) {
//            return value.toIso8601String();
//          } else {
//            return value.toJson;
//          }
//        });
//      }).toList(),
//    }));
  }

  Future<File> _getLocalFile() async {
    final dir = await getDirectory();

//    File f = File('${dir.path}/MemStorage__$tag.json');
//
//    if (f.existsSync()) {
//      return f;
//    } else
    return File('${dir.path}/MemStorage__$tag.json');
  }

  Future<FileSystemEntity> clean() async {
    final file = await _getLocalFile();

    return file.delete();
  }
}
