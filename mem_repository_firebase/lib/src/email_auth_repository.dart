import 'package:firebase_auth/firebase_auth.dart';
import 'package:mem_repository/mem_repository.dart';
import 'package:mem_entities/mem_entities.dart';
import 'dart:async';
import 'package:mem_repository_firebase/mem_repository_firebase.dart';

class EmailAuthRepository implements AuthRepository {
  FirebaseAuth auth = FirebaseAuth.instance;
  UserRepositoryFirestore userrepo = UserRepositoryFirestore();
  EmailAuthRepository(this.auth);

  //GoogleSignInAccount googleUser;
  //var googleSignIn = new GoogleSignIn();

  @override
  Future<UserEntity> loginAnonymous() async {
    final credential = await auth.signInAnonymously();

    return UserEntity(
      id: credential.user.uid,
      displayName: credential.user.displayName,
      photoUrl: credential.user.photoURL,
    );
  }

  @override
  Future<dynamic> initUser() async {
    dynamic firebaseUser = await ensureLoggedInOnStartUp();

    return firebaseUser;
  }

  @override
  Future<dynamic> ensureLoggedInOnStartUp() async {
    //GoogleSignInAccount user = googleSignIn.currentUser;
//    if (user == null) {
//      user = await googleSignIn.signInSilently();
//    }
//    googleUser = user;
    User firebaseUser = await auth.currentUser;

    return firebaseUser;
  }

  @override
  Future<User> logIntoFirebase({String userName, String password}) async {
    final credential = await auth.signInWithEmailAndPassword(
        email: userName, password: password);

    final User user = credential.user;

    assert(user != null);
    assert(await user.getIdToken() != null);

    final User currentUser = await auth.currentUser;
    assert(user.uid == currentUser.uid);

    print('signInEmail succeeded: $user');

    return user;
  }

  @override
  Future<User> phoneLogin({String verificationId, String smsCode}) async {
    //Breaking changes Auth Package
    final AuthCredential credential = PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: smsCode,
    );

    final userCredential = await auth.signInWithCredential(credential);
    final User user = userCredential.user;

//    final FirebaseUser user = await auth.signInWithPhoneNumber(
//        verificationId: verificationId, smsCode: smsCode);
    assert(user != null);
    assert(await user.getIdToken() != null);

    final User currentUser = await auth.currentUser;
    assert(user.uid == currentUser.uid);

    print('signIn Phone succeeded: $user');

    return user;
  }

  @override
  Future<User> addUser({
    String userName,
    String password,
  }) async {
    final userCredential = await auth.createUserWithEmailAndPassword(
        email: userName, password: password);
    final User user = userCredential.user;

    assert(user != null);
    assert(await user.getIdToken() != null);
    return user;
  }

  @override
  Future<UserEntity> getCurrentUserDetails() async {
    final User currentUser = await auth.currentUser;
    assert(currentUser != null);
    assert(await currentUser.getIdToken() != null);
    UserEntity _user;
    userrepo.getUser(currentUser.uid).then((user) {
      _user = user;
    });

    return _user;
  }
}
