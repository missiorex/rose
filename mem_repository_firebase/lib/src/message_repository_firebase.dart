import 'dart:async';
import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mem_repository/mem_repository.dart';
import 'package:mem_repository_firebase/src/file_storage.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:flutter/foundation.dart';
import 'package:mem_repository_firebase/mem_repository_firebase.dart';

class MessageRepositoryFireStore implements ReactiveMessageRepository {
  static const String path = 'mem';
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FileStorage fileStorage;

  MessageRepositoryFireStore({
    @required this.fileStorage,
  });

  /// Loads messages first from File storage. If they don't exist or encounter an
  /// error, it attempts to load the Messages from firebase
  @override
  Future<List<MessageEntity>> loadMessages() async {
    return await fileStorage.loadMessages();
  }

  @override
  Future<List<MessageEntity>> messageList() async {
    return await fileStorage.loadMessages();
  }

  // Persists messages to local disk and the web
  @override
  Future saveMessages(List<MessageEntity> msgs) {
    return Future.wait<dynamic>([
      fileStorage.saveMessages(msgs),
      Future.value(true)

      ///todo implement the firebase version of saveMessages
    ]);
  }

  @override
  Future<void> addNewMessage(MessageEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('messages')
          .doc(msg.id)
          .set(msg.toJson());
    } catch (e) {
      throw "Some issue in adding the message, Please notify the admin";
    }
  }

  @override
  Future<void> addBishopsMessage(BishopsMessageEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('bishop_messages')
          .doc(msg.id)
          .set(msg.toJson());
    } catch (e) {
      throw "Some issue in adding the message, Please notify the admin";
    }
  }

  @override
  Future<void> addBenefactor(BenefactorEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('benefactor_messages')
          .doc(msg.id)
          .set(msg.toJson());
    } catch (e) {
      throw "Some issue in adding the benefactor message, Please notify the admin";
    }
  }

  @override
  Future<void> addVideoTestimony(VideoTestimonyEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('video_testimony')
          .doc(msg.id)
          .set(msg.toJson());
    } catch (e) {
      throw "Some issue in adding the message, Please notify the admin";
    }
  }

  @override
  Future<void> addNotification(NotificationEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('notifications')
          .doc(msg.topic)
          .collection('messages')
          .doc()
          .set(msg.toJson());
    } catch (e) {
      throw "Some issue in adding the notification, Please notify the admin";
    }
  }

  @override
  Future<void> addAdminNotification(NotificationEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('admin_notifications')
          .doc(msg.topic)
          .collection('messages')
          .doc()
          .set(msg.toJson());
    } catch (e) {
      throw "Some issue in adding the notification, Please notify the admin";
    }
  }

  @override
  Future<void> addRegionalMessage(MessageEntity msg, String region) {
    try {
      return firestore
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(region)
          .collection('messages')
          .doc(msg.id)
          .set(msg.toJson());
    } catch (e) {
      throw "Some issue in adding the message, Please notify the admin";
    }
  }

  @override
  Future<String> addRegionalLocaleMessage(
      MessageEntity msg, String region, String locale) async {
    try {
      await firestore
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(region)
          .collection('locales')
          .doc(locale)
          .collection('messages')
          .doc(msg.id)
          .set(msg.toJson());

      return Future.value("Region: $region, Language : $locale.");
    } catch (e) {
      throw "Some issue in adding the message, Please notify the admin";
    }
  }

  @override
  Future<String> deleteRegionalLocaleMessage(
      String id, String region, String locale) async {
    String response = "";

    try {
      //Check if the message exists in the path chosen
      DocumentSnapshot snapshot = await FirebaseFirestore.instance
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(region)
          .collection('locales')
          .doc(locale)
          .collection('messages')
          .doc(id)
          .get();

      snapshot.exists
          ? response = "Region: $region, Language : $locale."
          : response = "";

      if (snapshot.exists)
        await firestore
            .collection(path)
            .doc('groups')
            .collection('regions')
            .doc(region)
            .collection('locales')
            .doc(locale)
            .collection('messages')
            .doc(id)
            .delete();

      return Future.value(response);
    } catch (e) {
      throw "Some issue in deleting the message, Please notify the admin";
    }
  }

  @override
  Future<void> addLocaleMessage(MessageEntity msg, String locale) {
    try {
      return firestore
          .collection(path)
          .doc('groups')
          .collection('locales')
          .doc(locale)
          .collection('messages')
          .doc(msg.id)
          .set(msg.toJson());
    } catch (e) {
      throw "Some issue in adding the message, Please notify the admin";
    }
  }

  @override
  Future<void> deleteMessage(List<String> idList) async {
    await Future.wait<void>(idList.map((id) {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('messages')
          .doc(id)
          .delete();
    }));
  }

  @override
  Future<void> deleteNotification(List<String> idList, String topic) async {
    await Future.wait<void>(idList.map((id) {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('admin_notifications')
          .doc(topic)
          .collection('messages')
          .doc(id)
          .delete();
    }));
  }

  @override
  Future<void> deleteRegionalMessage(MessageEntity msg, String region) {
    try {
      return firestore
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(region)
          .collection('messages')
          .doc(msg.id)
          .delete();
    } catch (e) {
      throw "Some issue in deleting the message, Please notify the admin";
    }
  }

//  @override
//  Stream<List<MessageEntity>> messages() {
//    return firestore.collection(path).snapshots().map((snapshot) {
//      return snapshot.docs.map((doc) {
//        return MessageEntity(
//            doc['title'],
//            doc.docID,
//            doc['body'] ?? '',
//            doc['complete'] ?? false,
//            doc['msgType'],
//            doc['rosaryCount'],
//            doc['rosaryByOthers'],
//            doc['time']);
//      }).toList();
//    });
//  }

  @override
  Stream<List<MessageEntity>> messages() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .orderBy('time', descending: true)
        .limit(10)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return MessageEntity.fromJson(doc.data());
      }).toList();
    });
  }

  Stream<List<MessageEntity>> messagesPageWise(
      int lastMsgTime, String msgType, String lastId) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .where("msgtype", isEqualTo: msgType)
        .orderBy('time', descending: false)
        .orderBy('id', descending: false)
        .startAfter([lastMsgTime, lastId])
        .limit(10)
        .snapshots()
        .map((snapshot) {
          return snapshot.docs.map((doc) {
            return MessageEntity.fromJson(doc.data());
          }).toList();
        });
  }

  @override
  Stream<List<MessageEntity>> rosaryMessages() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .where("msgtype", isEqualTo: "rosary")
        .orderBy('time', descending: true)
        .limit(25)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return MessageEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Future<Stream<List<MessageEntity>>> moreRosaryMessages(String lastId) async {
    var docRef = firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .doc(lastId);

    DocumentSnapshot doc = await docRef.get();

    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .where("msgtype", isEqualTo: "rosary")
        .orderBy('time', descending: true)
        .startAfter([doc.data()])
        .limit(10)
        .snapshots()
        .map((snapshot) {
          return snapshot.docs.map((doc) {
            return MessageEntity.fromJson(doc.data());
          }).toList();
        });
  }

  @override
  Stream<List<MessageEntity>> requests() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .where("msgtype", isEqualTo: "request")
        .orderBy('time', descending: true)
        .limit(10)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return MessageEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Stream<List<MessageEntity>> testimonies() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .where("msgtype", isEqualTo: "testimony")
        .orderBy('time', descending: true)
        .limit(10)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return MessageEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Stream<List<MessageEntity>> guidanceMessages() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .where("msgtype", isEqualTo: "guidance")
        .orderBy('time', descending: true)
        .limit(20)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return MessageEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Stream<List<NotificationEntity>> adminNotifications(String topic) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('admin_notifications')
        .doc(topic)
        .collection('messages')
        .orderBy('time', descending: true)
        .limit(100)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return NotificationEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Stream<List<BishopsMessageEntity>> bishopsMessages() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('bishop_messages')
        .orderBy('time', descending: true)
        .limit(3)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return BishopsMessageEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Stream<List<BenefactorEntity>> benefactorMessages() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('benefactor_messages')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return BenefactorEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Stream<List<VideoTestimonyEntity>> videoTestimonies() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('video_testimony')
        .orderBy('time', descending: true)
        .limit(10)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return VideoTestimonyEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Future<void> deleteVideoTestimony(VideoTestimonyEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('video_testimony')
          .doc(msg.id)
          .delete();
    } catch (e) {
      throw "Some issue in deleting the message, Please notify the admin";
    }
  }

  @override
  Future<void> deleteBishopsMessage(BishopsMessageEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('bishop_messages')
          .doc(msg.id)
          .delete();
    } catch (e) {
      throw "Some issue in deleting the message, Please notify the admin";
    }
  }

  @override
  Future<void> deleteBenefactor(BenefactorEntity msg) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('benefactor_messages')
          .doc(msg.id)
          .delete();
    } catch (e) {
      throw "Some issue in deleting the message, Please notify the admin";
    }
  }

  @override
  Future<BishopsMessageEntity> getBishopsMessage() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('bishop_messages')
        .doc("latest")
        .get()
        .then((doc) {
      return BishopsMessageEntity.fromJson(doc.data());
    });
  }

  @override
  Stream<List<MessageEntity>> regionalMessages(String region, String locale) {
    return firestore
        .collection(path)
        .doc('groups')
        .collection('regions')
        .doc(region)
        .collection('locales')
        .doc(locale)
        .collection('messages')
        .orderBy('time', descending: true)
        .limit(10)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return MessageEntity.fromJson(doc.data());
      }).toList();
    });

//    return firestore
//        .collection(path)
//        .doc('groups')
//        .collection('regions')
//        .doc(region)
//        .collection('messages')
//        .orderBy('time', descending: true)
//        .limit(30)
//        .snapshots()
//        .map((snapshot) {
//      return snapshot.docs.map((doc) {
//        return MessageEntity.fromJson(doc.data());
//      }).toList();
//    });
  }

  @override
  Stream<List<MessageEntity>> localeMessages(String locale) {
    return firestore
        .collection(path)
        .doc('groups')
        .collection('locales')
        .doc(locale)
        .collection('messages')
        .orderBy('time', descending: true)
        .limit(30)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return MessageEntity.fromJson(doc.data());
      }).toList();
    });
  }

  Future<dynamic> messages1() async {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .snapshots();
  }

  Future<void> updateMessage(MessageEntity msg) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .doc(msg.id)
        .update(msg.toJson());
  }

  Future<void> updateRegionalMessage(MessageEntity msg, String region) {
    return firestore
        .collection(path)
        .doc('groups')
        .collection('regions')
        .doc(region)
        .collection('messages')
        .doc(msg.id)
        .update(msg.toJson());
  }

  Future<int> getTotalRosaryCount() {
    int total = 0;
    firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .snapshots()
        .listen((snapshot) {
      total =
          snapshot.docs.fold(0, (tot, doc) => tot + doc.data()['rosaryCount']);
    });

    return Future.value(total);
  }

  Future<int> getTotalIncompleteRosaryCount(int startTime) {
    int total = 1;
    firestore
        .collection(path)
        .doc('rosary')
        .collection('messages')
        .where("msgtype", isEqualTo: "rosary")
        .where("complete", isEqualTo: true)
//        .where("time", isGreaterThanOrEqualTo: startTime)
//        .orderBy('time')
        .snapshots()
        .listen((snapshot) {
      total =
          snapshot.docs.fold(0, (tot, doc) => tot + doc.data()['rosaryCount']);
    });

    return Future.value(total);
  }

  //
  ///
  /// Gospel Cards / Home Screen Sliders
  ///
//

  @override
  Stream<List<GospelCardEntity>> gospelCards(String uid, String locale) {
    return firestore
        .collection('mem')
        .doc('rosary')
        .collection('gospelcards')
        .orderBy('time')
        .where("locale", isEqualTo: locale)
        .where("sliderCard", isEqualTo: true)
        .limit(8)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return GospelCardEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Stream<List<GospelCardEntity>> onBoardingCards(String uid) {
    return firestore
        .collection('mem')
        .doc('rosary')
        .collection('gospelcards')
        .where("cardType", isEqualTo: "onboard")
        .orderBy('time')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return GospelCardEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Future<void> addGospelCard(GospelCardEntity gc) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('gospelcards')
          .doc(gc.id)
          .set(gc.toJson());
    } catch (e) {
      throw "Some issue in adding the Gosepl Card, Please notify the admin";
    }
  }

  @override
  Future<void> updateGospelCardComplete(String id) {
    firestore
        .collection('mem')
        .doc('rosary')
        .collection('gospelcards')
        .doc(id)
        .update({'isComplete': true});

    return Future.value();
  }

  @override
  Future<void> deleteGospelCard(String id) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('gospelcards')
          .doc(id)
          .delete();
    } catch (e) {
      throw "Some issue in deleting the message, Please notify the admin";
    }
  }

  @override
  Future<GospelCardEntity> getLatestGospelCard() {
    return firestore
        .collection('mem')
        .doc('rosary')
        .collection('gospelcards')
        .orderBy('time')
        .limit(1)
        .get()
        .then((doc) {
      return GospelCardEntity.fromJson(doc.docs.last.data());
    });
  }

  @override
  Stream<List<ActionEntity>> actions(String uid) {
    return firestore
        .collection('mem')
        .doc('rosary')
        .collection('actions')
        .orderBy('time')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return ActionEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Future<void> addAction(String uid, ActionEntity action) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('actions')
          .doc(action.id)
          .set(action.toJson());
    } catch (e) {
      throw "Some issue in adding the Action, Please notify the admin";
    }
  }

  @override
  Future<void> updateAction(String id) {
    firestore
        .collection(path)
        .doc('rosary')
        .collection('actions')
        .doc(id)
        .update({'notified': true});

    return Future.value();
  }

  @override
  Future<void> addUserAction(String uid, ActionEntity action) {
    try {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('users')
          .doc(uid)
          .collection('actions')
          .doc(action.id)
          .set(action.toJson());
    } catch (e) {
      throw "Some issue in adding the User Action, Please notify the admin";
    }
  }

  @override
  Future<void> updateUserAction(String uid, ActionEntity userAction) {
    firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .collection('actions')
        .doc(userAction.id)
        .set(userAction.toJson());

    return Future.value();
  }

  @override
  Future<QuerySnapshot> getMessages(int count, String msgType) {
    return firestore
        .collection(path)
        .doc("rosary")
        .collection('messages')
        .where("msgtype", isEqualTo: msgType)
        .orderBy('time', descending: true)
        .limit(count)
        .get();
  }

  //Return a stream of filtered messages
  @override
  Stream<QuerySnapshot> getFilteredMessages(int count, String msgType) {
    return firestore
        .collection(path)
        .doc("rosary")
        .collection('messages')
        .where("msgtype", isEqualTo: msgType)
        .orderBy('time', descending: true)
        .limit(count)
        .get()
        .asStream();
  }
}
