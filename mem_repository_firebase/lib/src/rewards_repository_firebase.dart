import 'dart:async';
import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mem_repository/mem_repository.dart';
import 'package:mem_repository_firebase/src/file_storage.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:flutter/widgets.dart';
import 'dart:convert';
import 'package:base_helpers/basehelpers.dart';

class RewardsRepositoryFireStore implements ReactiveRewardsRepository {
  static const String path = 'mem';
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FileStorage fileStorage;

  RewardsRepositoryFireStore({
    @required this.fileStorage,
  });

  @override
  Future<void> addStarsChannelEntry(StarsChannelEntity stars) {
    firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(stars.uid)
        .collection('stars')
        .doc(stars.id)
        .set(stars.toJson());

    if (stars.level > 0) {
      for (var badgeLevel = 1; badgeLevel <= stars.level; badgeLevel++) {
        String badgeID = stars.id + " - " + badgeLevel.toString();
        BadgeEntity badge = BadgeEntity(
            badgeID,
            stars.title + " - Level" + badgeLevel.toString(),
            stars.uid,
            DateTime.now().millisecondsSinceEpoch,
            badgeLevel,
            stars.action,
            stars.count,
            badgeLevel != stars.level ? true : false,
            "assets/" + stars.id + ".jpg");
        //Add a new badge
        firestore
            .collection(path)
            .doc('rosary')
            .collection('users')
            .doc(stars.uid)
            .collection('badges')
            .doc(badge.id)
            .set(badge.toJson());
      }
    }

    return Future.value();
  }

  @override
  Future<bool> starsChannelEntryExists(String uid, String starChannelID) async {
    DocumentSnapshot snapshot = await firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .collection('stars')
        .doc(starChannelID)
        .get();
    return snapshot.exists;
  }

  @override
  Future<void> addStars(int additionalCount, String uid, String starChannelID) {
    int level = 0;
    String badgeTitle = "";
    String badgeAction = "";

    FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .collection('stars')
        .doc(starChannelID)
        .get()
        .then((snapshot) {
      int existingStarsCount = snapshot.data()["count"];
      int existingLevel = snapshot.data()["level"];

      if (starChannelID == "rosary_warrior") {
        level = MEMStarLevels()
            .rosaryWarriorLevel(existingStarsCount + additionalCount);
        badgeTitle = "Rosary Warrior - Level " + level.toString();
        badgeAction = "ADD ROSARY";
      } else if (starChannelID == "good_samaritan") {
        level = MEMStarLevels()
            .goodSamaritanLevel(existingStarsCount + additionalCount);
        badgeTitle = "Good Samaritan - Level " + level.toString();
        badgeAction = "COLLECT ROSARY";
      }

      firestore
          .collection(path)
          .doc('rosary')
          .collection('users')
          .doc(uid)
          .collection('stars')
          .doc(starChannelID)
          .update(
              {'count': existingStarsCount + additionalCount, 'level': level});

      if (level > existingLevel) {
        String badgeID = starChannelID + " - " + level.toString();
        BadgeEntity badge = BadgeEntity(
            badgeID,
            badgeTitle,
            uid,
            DateTime.now().millisecondsSinceEpoch,
            level,
            badgeAction,
            existingStarsCount + additionalCount,
            false,
            "assets/" + starChannelID + ".jpg");
        //Add a new badge
        firestore
            .collection(path)
            .doc('rosary')
            .collection('users')
            .doc(uid)
            .collection('badges')
            .doc(badge.id)
            .set(badge.toJson());
      }
    });

    return Future.value();
  }

  @override
  Future<void> removeStars(int deletedCount, String uid, String starChannelID) {
    int level = 0;

    FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .collection('stars')
        .doc(starChannelID)
        .get()
        .then((snapshot) {
      int existingStarsCount = snapshot.data()["count"];
      int existingLevel = snapshot.data()["level"];

      if (starChannelID == "rosary_warrior")
        level = MEMStarLevels()
            .rosaryWarriorLevel(existingStarsCount - deletedCount);
      else if (starChannelID == "good_samaritan")
        level = MEMStarLevels()
            .goodSamaritanLevel(existingStarsCount - deletedCount);

      firestore
          .collection(path)
          .doc('rosary')
          .collection('users')
          .doc(uid)
          .collection('stars')
          .doc(starChannelID)
          .update({'count': existingStarsCount - deletedCount, 'level': level});

      if (level < existingLevel) {
        String badgeID = starChannelID + " - " + existingLevel.toString();

        //Add a new badge
        firestore
            .collection(path)
            .doc('rosary')
            .collection('users')
            .doc(uid)
            .collection('badges')
            .doc(badgeID)
            .delete();
      }
    });

    return Future.value();
  }

  @override
  Future<void> updateBadgeComplete(String uid, String id) {
    firestore
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .collection('badges')
        .doc(id)
        .update({'isComplete': true});

    return Future.value();
  }

  @override
  Stream<List<BadgeEntity>> badges(String uid) {
    return firestore
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .collection('badges')
        .orderBy('time')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return BadgeEntity.fromJson(doc.data());
      }).toList();
    });
  }
}
