import 'dart:async';
import 'package:mem_repository/mem_repository.dart';
import 'package:mem_entities/mem_entities.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';

// Simple email password sign in

class UserRepositoryFirestore implements ReactiveUserRepository {
  static const String path = 'mem';
  static FirebaseFirestore firestore = FirebaseFirestore.instance;

//  @override
//  Future<FirebaseUser> addUser(UserEntity userEntity) async {
//    FirebaseUser user = await auth.createUserWithEmailAndPassword(
//        email: userEntity.emailId, password: userEntity.password);
//
//    assert(user != null);
//    assert(await user.getIdToken() != null);
//    //Set the userid obtained from the login provider.
//    userEntity.id = user.uid;
//    userEntity.phoneNumber = user.phoneNumber;
//    userEntity.status = false;
//
//    await firestore
//        .collection(path)
//        .document('rosary')
//        .collection('users')
//        .document(user.uid)
//        .setData(userEntity.toJson());
//
//    return user;
//  }

//  Future<bool> isUserExist({String email, String Password}){
//
//    DocumentSnapshot snapshot =
//        await firestore.collection(path).document('rosary')
//        .collection('users')
//        .document(user.uid).get();
//
//    return snapshot.data["total_count"] as int;
//
//
//  }

//  @override
//  Future getUser(authData) {
//    switch (authData.provider) {
//      case 'password':
//        return authData.password.email;
//      case 'twitter':
//        return authData.twitter.username;
//      case 'facebook':
//        return authData.facebook.email;
//      case 'google':
//        return authData.google.email;
//      case 'github':
//        return authData.github.username;
//    }
//  }

  // var authData = FirebaseAuth.instance.

  @override
  Stream<List<String>> regions() {
    return firestore
        .collection(path)
        .doc('groups')
        .collection('regions')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return doc.data()["title"] as String;
      }).toList();
    });
  }

  @override
  Stream<List<String>> locales() {
    return firestore
        .collection(path)
        .doc('groups')
        .collection('locales')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return doc.data()["title"] as String;
      }).toList();
    });
  }

//  @override
//  Stream<List<String>> locales() {
//    return firestore
//        .collection(path)
//        .document('rosary')
//        .collection('notifications')
//        .snapshots()
//        .map((snapshot) {
//      return snapshot.documents
//          .map((doc) {
//            return doc.data["title"] as String;
//          })
//          .skipWhile((title) => title.compareTo("Global") > 0)
//          .toList();
//    });
//  }

  @override
  Future<void> addNewUser(UserEntity user) {
    firestore
        .collection(path)
        .doc('groups')
        .collection('regions')
        .doc(user.region)
        .collection('users')
        .doc(user.id)
        .set({"admin": false});

    firestore
        .collection(path)
        .doc('groups')
        .collection('locales')
        .doc(user.locale)
        .collection('users')
        .doc(user.id)
        .set({"admin": false});

    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(user.id)
        .set(user.toJson());
  }

  @override
  Future<void> deleteUser(List<String> idList) async {
    await Future.wait<void>(idList.map((id) {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('users')
          .doc(id)
          .delete();
    }));
  }

  @override
  Future addUser(UserEntity userEntity) async {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(userEntity.id)
        .set(userEntity.toJson());
  }

  @override
  Stream<List<UserEntity>> users() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .orderBy('name')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return UserEntity.fromJson(doc.data());
      }).toList();
    });
  }

  //Access Control List to manage privileges 1.9.823 onwards
  @override
  Stream<List<AclEntity>> acl() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('acl')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return AclEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Future<AclEntity> getUserAcl(String userId) async {
    DocumentSnapshot snapshot = await firestore
        .collection(path)
        .doc('rosary')
        .collection('acl')
        .doc(userId)
        .get();

    return AclEntity.fromJson(snapshot.data());

    ///todo: complete userEntity after fields are final
  }

  @override
  Stream<List<UserEntity>> usersRegionWise(String region) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .orderBy('userAddedOn')
        .where('region', isEqualTo: region)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return UserEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Future<void> updateProfilePhoto(UserEntity user) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(user.id)
        .update({"profilePhotoUrl": user.profilePhotoUrl});
  }

  @override
  Future<void> updateUser(UserEntity user) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(user.id)
        .update(user.toJson());
  }

  @override
  Future<UserEntity> getUser(String userId) async {
    DocumentSnapshot snapshot = await firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(userId)
        .get();

    return UserEntity.fromJson(snapshot.data());

    ///todo: complete userEntity after fields are final
//    return UserEntity(
//
//      id:snapshot.data['id'],
//      displayName:snapshot.data['id'],
//
//
//
//      name:snapshot.data['name'],);
  }

  @override
  Future<bool> userExists(String userId) async {
    DocumentSnapshot snapshot = await firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(userId)
        .get();

    return snapshot.exists;
  }

  @override
  Future<bool> userActive(String userId) async {
    DocumentSnapshot snapshot = await firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(userId)
        .get();

    return snapshot['status'] as bool;
  }

  @override
  Future<String> userRole(String userId) async {
    DocumentSnapshot snapshot = await firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(userId)
        .get();

    return snapshot['role'] as String;
  }

  @override
  Future<bool> isSuperAdmin(String userId) async {
    DocumentSnapshot snapshot = await firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(userId)
        .get();

    return snapshot['isSuperAdmin'] as bool;
  }

  @override
  Future<void> assignAdminRole(UserEntity user) {
    firestore
        .collection(path)
        .doc('groups')
        .collection('regions')
        .doc(user.region)
        .collection('users')
        .doc(user.id)
        .update({"admin": true});

    firestore
        .collection(path)
        .doc('rosary')
        .collection('acl')
        .doc(user.id)
        .set({
      "id": user.id,
      "name": user.name,
      "grantAdminRole": false,
      "revokeAdminRole": false,
      "grantSuperAdminRole": false,
      "revokeSuperAdminRole": false,
      "activateUser": true,
      "deactivateUser": true,
      "blockUser": false,
      "unBlockUser": false,
      "deleteUser": false,
      "regionalAdmin": false,
      "localeAdmin": false,
      "viewCounselling": false,
      "rosaryDelete": false,
      "messageDelete": true,
      "addVideoTestimony": false,
      "addBishopsMessage": false,
      "editAboutUsContent": false
    });

    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(user.id)
        .update({
      'role': "admin",
    });
  }

  @override
  Future<void> revokeAdminRole(UserEntity user) {
    firestore
        .collection(path)
        .doc('groups')
        .collection('regions')
        .doc(user.region)
        .collection('users')
        .doc(user.id)
        .update({"admin": false});

    firestore
        .collection(path)
        .doc('rosary')
        .collection('acl')
        .doc(user.id)
        .update({
      "grantAdminRole": false,
      "revokeAdminRole": false,
      "grantSuperAdminRole": false,
      "revokeSuperAdminRole": false,
      "activateUser": false,
      "deactivateUser": false,
      "blockUser": false,
      "unBlockUser": false,
      "deleteUser": false,
      "regionalAdmin": false,
      "localeAdmin": false,
      "viewCounselling": false,
      "rosaryDelete": false,
      "messageDelete": false,
      "addVideoTestimony": false,
      "addBishopsMessage": false,
      "editAboutUsContent": false
    });

    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(user.id)
        .update({
      'role': "user",
    });
  }

  @override
  Future<void> blockUser(String uid) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .update({
      'status': false,
      'isBlocked': true,
    });
  }

  @override
  Future<void> unBlockUser(String uid) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .update({
      'isBlocked': false,
    });
  }

  @override
  Future<void> activateUser(String uid) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .update({
      'status': true,
    });
  }

  @override
  Future<void> deActivateUser(String uid) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .update({
      'status': false,
    });
  }

  @override
  Future<void> deleteSingleUser(UserEntity user) {
    try {
      firestore
          .collection(path)
          .doc('groups')
          .collection('regions')
          .doc(user.region)
          .collection('users')
          .doc(user.id)
          .delete();

      return firestore
          .collection(path)
          .doc('rosary')
          .collection('users')
          .doc(user.id)
          .delete();
    } catch (e) {
      throw "Some issue in deleting the message, Please notify the admin";
    }
  }

  @override
  Future<void> updatePushNotificationToken(String token, String uid) {
    FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .get()
        .then((snapshot) {
      String existingToken = snapshot.data()["pushNotificationToken"];

      if (existingToken != token)
        firestore
            .collection(path)
            .doc('rosary')
            .collection('users')
            .doc(uid)
            .update({
          'pushNotificationToken': token,
        });
    });

    return Future.value();
  }

  @override
  Future<void> setPushNotificationToken(String token, String uid) {
    FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .get()
        .then((snapshot) {
      firestore
          .collection(path)
          .doc('rosary')
          .collection('users')
          .doc(uid)
          .set({
        'pushNotificationToken': token,
      });
    });

    return Future.value();
  }
}
