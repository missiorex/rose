import 'dart:async';
import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mem_repository/mem_repository.dart';
import 'package:mem_repository_firebase/src/file_storage.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:flutter/widgets.dart';
import 'dart:convert';

class RosaryRepositoryFireStore implements ReactiveRosaryRepository {
  static const String path = 'mem';
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FileStorage fileStorage;

  RosaryRepositoryFireStore({
    @required this.fileStorage,
  });

  @override
  Future<void> addNewRosary(RosaryEntity rosary) {
    debugPrint("Rosary from repository: " + rosary.toString());

    FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .get()
        .then((snapshot) {
      int existingTotalCount = snapshot.data()["total_count"];
      int existingTotalByUsers = snapshot.data()["totalByUsers"];
      int existingTotalExternal = snapshot.data()["totalExternal"];

      firestore
          .collection(path)
          .doc('rosary')
          .collection('rosary_count')
          .doc(rosary.id)
          .set({
        'id': rosary.id,
        'total_count':
            existingTotalCount + rosary.totalByUsers + rosary.totalExternal,
        'totalByUsers': existingTotalByUsers + rosary.totalByUsers,
        'totalExternal': existingTotalExternal + rosary.totalExternal,
        'time': rosary.time
      });
    });
    return Future.value();
  }

  // Modified version of addNewRosay to fix Rosary count issue.
  @override
  Future<void> addNewRosaryCount(RosaryEntity rosary) {
    debugPrint("Rosary Count from repository: " + rosary.toString());

    FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .get()
        .then((snapshot) {
      int existingTotalCount = snapshot.data()["total_count"];
      int existingTotalByUsers = snapshot.data()["totalByUsers"];
      int existingTotalExternal = snapshot.data()["totalExternal"];

      firestore
          .collection(path)
          .doc('rosary')
          .collection('rosary_count')
          .doc(rosary.id)
          .set({
        'id': rosary.id,
        'total_count':
            existingTotalCount + rosary.totalByUsers + rosary.totalExternal,
        'totalByUsers': existingTotalByUsers + rosary.totalByUsers,
        'totalExternal': existingTotalExternal + rosary.totalExternal,
        'time': rosary.time
      });
    });
    return Future.value();
  }

  @override
  Future<void> addRosaryListEntry(RosaryCountEntity rc) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(rc.uid)
        .collection('rosarylist')
        .doc(rc.id)
        .set(rc.toJson());
  }

  @override
  Future<void> updateRosaryListEntry(RosaryCountEntity rc) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(rc.uid)
        .collection('rosarylist')
        .doc(rc.id)
        .update(rc.toJson());
  }

  @override
  Future<bool> rosaryListEntryExists(RosaryCountEntity rc) async {
    DocumentSnapshot snapshot = await firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(rc.uid)
        .collection('rosarylist')
        .doc(rc.id)
        .get();

    return snapshot.exists;
  }

  @override
  Future<void> deleteRosaryListEntry(RosaryCountEntity rc) async {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(rc.uid)
        .collection('rosarylist')
        .doc(rc.id)
        .delete();
  }

  @override
  Stream<List<RosaryCountEntity>> rosaryCountList(String uid) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .collection('rosarylist')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return RosaryCountEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Future<void> deleteRosary(List<String> idList) async {
    await Future.wait<void>(idList.map((id) {
      return firestore
          .collection(path)
          .doc('rosary')
          .collection('rosary_count')
          .doc(id)
          .delete();
    }));
  }

  @override
  Stream<List<RosaryEntity>> rosaries() {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('rosary_count')
        .orderBy('time', descending: true)
        .limit(1)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return RosaryEntity.fromJson(doc.data());
      }).toList();
    });
  }

  // The rosary counts 1.9813 onwards
  @override
  Stream<List<CountSnapShotEntity>> latestRosaryCount() {
    return firestore
        .collection(path)
        .doc('rosary_count')
        .collection('count_snapshot')
        .orderBy('time', descending: true)
        .limit(1)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return CountSnapShotEntity.fromJson(doc.data());
      }).toList();
    });
  }

  @override
  Future<void> updateRosary(RosaryEntity msg) {
    return firestore
        .collection(path)
        .doc('rosary')
        .collection('rosary_count')
        .doc(msg.id)
        .update(msg.toJson());
  }

//  @override
//  Future<int> fetchRosaryOldCount() async {
//    DocumentSnapshot snapshot =
//        await Firestore.instance.collection('mem').doc('rosary').get();
//
//    return snapshot.data()["total_count"] as int;
//  }

  @override
  Future<int> fetchRosary() async {
    DocumentSnapshot snapshot =
        await FirebaseFirestore.instance.collection('mem').doc('rosary').get();

    return snapshot["total_count"] as int;
  }

  Future<RosaryEntity> getRosary() async {
    DocumentSnapshot snapshot =
        await FirebaseFirestore.instance.collection('mem').doc('rosary').get();

    return RosaryEntity(snapshot.id, 121212, snapshot["total_count"] as int,
        121212, DateTime.now().millisecondsSinceEpoch);
  }

  @override
  Future<void> updateRosaryCount(int countByUser, int countExternal) {
    FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .get()
        .then((snapshot) {
      int existingTotalCount = snapshot.data()["total_count"];
      int existingTotalByUsers = snapshot.data()["totalByUsers"];
      int existingTotalExternal = snapshot.data()["totalExternal"];

      firestore.collection(path).doc('rosary').set({
        'total_count': existingTotalCount + countByUser + countExternal,
        'totalByUsers': existingTotalByUsers + countByUser,
        'totalExternal': existingTotalExternal + countExternal
      });
    });

    return Future.value();
  }

  @override
  Future<void> updateRosaryCountUser(
      int countByUser, int countExternal, String uid) {
    FirebaseFirestore.instance
        .collection('mem')
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .get()
        .then((snapshot) {
      int existingTotalByUsers = snapshot.data()["rosariesOffered"];
      int existingTotalExternal = snapshot.data()["rosariesCollected"];

      firestore
          .collection(path)
          .doc('rosary')
          .collection('users')
          .doc(uid)
          .update({
        'rosariesOffered': existingTotalByUsers + countByUser,
        'rosariesCollected': existingTotalExternal + countExternal,
        'rosaryLastAddedOn': DateTime.now().millisecondsSinceEpoch,
      });
    });

    return Future.value();
  }

  @override
  Future<void> reduceRosaryCountUser(
      int countByUser, int countExternal, String uid) {
    FirebaseFirestore.instance
        .collection(path)
        .doc('rosary')
        .collection('users')
        .doc(uid)
        .get()
        .then((snapshot) {
      int existingTotalByUsers = snapshot.data()["rosariesOffered"];
      int existingTotalExternal = snapshot.data()["rosariesCollected"];

      firestore
          .collection(path)
          .doc('rosary')
          .collection('users')
          .doc(uid)
          .update({
        'rosariesOffered': (existingTotalByUsers - countByUser),
        'rosariesCollected': (existingTotalExternal - countExternal),
        'rosaryLastAddedOn': DateTime.now().millisecondsSinceEpoch,
      });
    });

    return Future.value();
  }
}
