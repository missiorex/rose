import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mem_entities/mem_entities.dart';

abstract class AuthRepository {
  Future<UserEntity> loginAnonymous();
  Future<dynamic> ensureLoggedInOnStartUp();
  Future<User> logIntoFirebase({
    String userName,
    String password,
  });
  Future<User> phoneLogin({
    String verificationId,
    String smsCode,
  });
  Future initUser();
  Future<User> addUser({
    String userName,
    String password,
  });
  Future<UserEntity> getCurrentUserDetails();
}
