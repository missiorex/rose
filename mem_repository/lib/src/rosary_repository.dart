// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:core';

import 'package:mem_entities/mem_entities.dart';

/// A class that Loads and Persists rosary counts. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as rosary_repository_web or rosary_repository_firebase.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class RosaryRepository {
  /// Loads rosaries first from File storage. If they don't exist or encounter an
  /// error, it attempts to load the Messages from a Server (Firebase/Web).
  Future<List<RosaryEntity>> loadRosaries();

  // Persists rosary counts to local disk and the Firebase/web
  Future saveRosaries(List<MessageEntity> rosaries);
}
