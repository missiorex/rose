// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:core';

import 'package:mem_entities/mem_entities.dart';

/// A class that Loads and Persists messages. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as message_repository_web or message_repository_firebase.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class MessageRepository {
  /// Loads msgs first from File storage. If they don't exist or encounter an
  /// error, it attempts to load the Messages from a Server (Firebase).
  Future<List<MessageEntity>> loadMessages();

  // Persists messages to local disk and the web
  Future saveMessages(List<MessageEntity> messages);
}
