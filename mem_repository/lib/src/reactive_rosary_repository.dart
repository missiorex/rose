// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:core';

import 'package:mem_entities/mem_entities.dart';

/// A data layer class works with reactive data sources, such as Firebase. This
/// class emits a Stream of MessageEntities. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as mem_repository_web / mem_repository_firebase.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Firebase.
abstract class ReactiveRosaryRepository {
  Future<void> addNewRosary(RosaryEntity rosary);

  Future<void> deleteRosary(List<String> idList);

  Stream<List<RosaryEntity>> rosaries();

  Stream<List<CountSnapShotEntity>> latestRosaryCount();

  Future<void> updateRosary(RosaryEntity rosary);

  Future<int> fetchRosary();

  Future<RosaryEntity> getRosary();

  Future<void> updateRosaryCount(int countByUser, int countExternal);

  Future<void> addRosaryListEntry(RosaryCountEntity rc);

  Future<void> updateRosaryListEntry(RosaryCountEntity rc);

  Stream<List<RosaryCountEntity>> rosaryCountList(String uid);

  Future<bool> rosaryListEntryExists(RosaryCountEntity rc);

  Future<void> deleteRosaryListEntry(RosaryCountEntity rc);

  Future<void> updateRosaryCountUser(
      int countByUser, int countExternal, String uid);

  Future<void> addNewRosaryCount(RosaryEntity rosary);

  Future<void> reduceRosaryCountUser(
      int countByUser, int countExternal, String uid);
}
