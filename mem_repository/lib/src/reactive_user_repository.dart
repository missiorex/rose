import 'dart:async';
import 'dart:core';

import 'package:mem_entities/mem_entities.dart';

/// A data layer class works with reactive data sources, such as Firebase. This
/// class emits a Stream of MessageEntities. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as mem_repository_web / mem_repository_firebase.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Firebase.
abstract class ReactiveUserRepository {
  Future<void> addNewUser(UserEntity user);

  Future<void> deleteUser(List<String> idList);

  Stream<List<UserEntity>> users();

  Stream<List<AclEntity>> acl();

  Stream<List<UserEntity>> usersRegionWise(String region);

  Future<void> updateUser(UserEntity user);

  Future<UserEntity> getUser(String uid);

  Future<bool> userExists(String uid);

  Future<bool> userActive(String uid);

  Future<String> userRole(String userId);

  Future<bool> isSuperAdmin(String userId);

  Stream<List<String>> regions();

  Future<void> updateProfilePhoto(UserEntity user);

  Future<void> assignAdminRole(UserEntity user);

  Future<void> revokeAdminRole(UserEntity user);

  Future<AclEntity> getUserAcl(String userId);

  Future<void> blockUser(String uid);

  Future<void> activateUser(String uid);

  Future<void> deleteSingleUser(UserEntity user);

  Future<void> unBlockUser(String uid);

  Future<void> deActivateUser(String uid);

  Future<void> updatePushNotificationToken(String token, String uid);

  Future<void> setPushNotificationToken(String token, String uid);

  Stream<List<String>> locales();
}
