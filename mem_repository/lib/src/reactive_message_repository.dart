// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:core';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mem_entities/mem_entities.dart';

/// A data layer class works with reactive data sources, such as Firebase. This
/// class emits a Stream of MessageEntities. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as mem_repository_web / mem_repository_firebase.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Firebase.
abstract class ReactiveMessageRepository {
  Future<void> addNewMessage(MessageEntity msg);

  Future<void> addRegionalMessage(MessageEntity msg, String region);

  Future<String> addRegionalLocaleMessage (
      MessageEntity msg, String region, String locale);

  Future<String> deleteRegionalLocaleMessage(
      String id, String region, String locale);

  Future<void> deleteMessage(List<String> idList);

  Stream<List<MessageEntity>> messages();

  Stream<List<MessageEntity>> messagesPageWise(
      int lastMsgTime, String msgType, String lastId);

  Stream<List<MessageEntity>> rosaryMessages();

  Future<Stream<List<MessageEntity>>> moreRosaryMessages(String lastId);

  Stream<List<MessageEntity>> requests();

  Stream<List<MessageEntity>> testimonies();

  Stream<List<MessageEntity>> guidanceMessages();

  Stream<List<MessageEntity>> regionalMessages(String region, String locale);

  Stream<List<MessageEntity>> localeMessages(String locale);

  Future<void> updateMessage(MessageEntity msg);

  Future<dynamic> messages1();

  Future<void> updateRegionalMessage(MessageEntity msg, String region);

  Future<void> deleteRegionalMessage(MessageEntity msg, String region);

  Future<void> addLocaleMessage(MessageEntity msg, String locale);

  Future<void> addBishopsMessage(BishopsMessageEntity msg);

  Future<void> addVideoTestimony(VideoTestimonyEntity msg);

  Future<void> addBenefactor(BenefactorEntity msg);

  Future<BishopsMessageEntity> getBishopsMessage();

  Stream<List<BishopsMessageEntity>> bishopsMessages();

  Stream<List<BenefactorEntity>> benefactorMessages();

  Stream<List<VideoTestimonyEntity>> videoTestimonies();

  Future<void> addNotification(NotificationEntity msg);

  Future<void> addAdminNotification(NotificationEntity msg);

  Future<void> deleteBishopsMessage(BishopsMessageEntity msg);

  Future<void> deleteBenefactor(BenefactorEntity msg);

  Future<void> deleteVideoTestimony(VideoTestimonyEntity msg);

  Stream<List<NotificationEntity>> adminNotifications(String topic);

  Future<void> deleteNotification(List<String> idList, String topic);

  Future<int> getTotalRosaryCount();

  Future<int> getTotalIncompleteRosaryCount(int startTime);

  Future<void> addGospelCard(GospelCardEntity gc);

  Future<void> deleteGospelCard(String id);

  Stream<List<GospelCardEntity>> gospelCards(String uid,String locale);

  Future<void> updateGospelCardComplete(String id);

  Stream<List<GospelCardEntity>> onBoardingCards(String uid);

  Future<GospelCardEntity> getLatestGospelCard();

  Future<void> addAction(String uid, ActionEntity action);

  Stream<List<ActionEntity>> actions(String uid);

  Future<void> updateAction(String id);

  Future<void> addUserAction(String uid, ActionEntity action);

  Future<void> updateUserAction(String uid, ActionEntity userAction);

  Future<QuerySnapshot> getMessages(int count,String msgType);

  Stream<QuerySnapshot> getFilteredMessages(int count, String msgType);
}
