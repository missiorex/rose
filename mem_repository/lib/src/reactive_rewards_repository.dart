// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:core';

import 'package:mem_entities/mem_entities.dart';

/// A data layer class works with reactive data sources, such as Firebase. This
/// class emits a Stream of MessageEntities. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as mem_repository_web / mem_repository_firebase.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Firebase.
abstract class ReactiveRewardsRepository {
  Future<void> addStarsChannelEntry(StarsChannelEntity stars);

  Future<bool> starsChannelEntryExists(String uid, String starChannelID);

  Future<void> addStars(int additionalCount, String uid, String starChannelID);
  Future<void> removeStars(int deletedCount, String uid, String starChannelID);
  Future<void> updateBadgeComplete(String uid, String id);
  Stream<List<BadgeEntity>> badges(String uid);
}
