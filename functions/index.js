const functions = require('firebase-functions');
let admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });



exports.sendFiveContinentsPrayer = functions.firestore
    .document('mem/rosary/notifications/five/messages/{documentID}')
    .onCreate((snap, context) => {
      
      const msg = snap.data();
      var payload;
      
       if(msg.topic!==""){

        payload = {

       	  topic: msg.topic,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};
	}
	else if(msg.token !== ""){

		payload = {

       	  token: msg.token,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};


	}


	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
    
  

    });




  exports.malayalamNotification = functions.firestore
    .document('mem/rosary/notifications/Malayalam/messages/{documentID}')
    .onCreate((snap, context) => {
      
      const msg = snap.data();
      var payload;
      

		payload = {

       	  topic: msg.topic,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
    
  

    });


    exports.englishNotification = functions.firestore
    .document('mem/rosary/notifications/English/messages/{documentID}')
    .onCreate((snap, context) => {
      
      const msg = snap.data();
      var payload;
      

		payload = {

       	  topic: msg.topic,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
    
  

    });

      // perform desired operations ...
    // });   

exports.newRegistrationNotification = functions.firestore
    .document('/mem/rosary/admin_notifications/new_user/messages/{documentID}')
    .onCreate((snap, context) => {
      
      
      const msg = snap.data();
      var payload;
      

		payload = {

       	  topic: msg.topic,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
  

    });


  exports.rosaryLimitsNotification = functions.firestore
    .document('/mem/rosary/admin_notifications/rosary_limits/messages/{documentID}')
    .onCreate((snap, context) => {
      
      
      const msg = snap.data();
      var payload;
      
      if(msg)

		payload = {

       	  topic: "new_user",
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
  

    });
    

 exports.deleteUser = functions.firestore
    .document('/mem/rosary/users/{userID}')
    .onDelete((snap, context) => {
      return admin.auth().deleteUser(snap.id)
          .then(() => console.log('Deleted user with ID:' + snap.id))
          .catch((error) => console.error('There was an error while deleting user:', error));
    });


 function loadUsers() {
    let dbRef = admin.database().ref('/users');
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            let users = [];
            for (var property in data) {
                users.push(data[property]);
            }
            resolve(users);
        }, (err) => {
            reject(err);
        });
    });
    return defer;
}