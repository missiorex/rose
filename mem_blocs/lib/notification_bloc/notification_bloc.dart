import 'dart:async';
import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:mem_models/mem_models.dart';
import 'package:mem_blocs/message_bloc/message_page.dart';
import 'package:mem_blocs/message_bloc/message_slice.dart';
import 'package:rxdart/rxdart.dart';
import 'package:mem_entities/entities/message_entity.dart';
import 'package:mem_repository/mem_repository.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'notification_validators.dart';

/// This component encapsulates the logic of fetching messages from
/// a database, page by page, according to position in an infinite list.
///
/// Only the data that are close to the current location are cached, the rest
/// are thrown away.
class NotificationBloc extends Object with NotificationValidators {
  final ReactiveMessageRepository messageRepository;
  final ReactiveUserRepository userRepository;

  List<MEMmessage> messages;

  final _msgTitleController = BehaviorSubject<String>();
  final _msgBodyController = BehaviorSubject<String>();
  final _topicController = BehaviorSubject<String>();

  NotificationBloc(this.messageRepository, this.userRepository) {}

  //Message CRUD
  // Add data to stream
  Stream<String> get msgTitle =>
      _msgTitleController.stream.transform(validateMsgTitle);

  Stream<String> get msgBody =>
      _msgBodyController.stream.transform(validateMsgBody);

  Stream<String> get topic => _topicController.stream;

  Stream<bool> get submitValidNotificationMessage =>
      Rx.combineLatest3(msgTitle, msgBody, topic, (e, p, t) => true);
//  Stream<bool> get submitValidRosaryMessage => Observable.combineLatest3(
//      msgTitle, msgBody, rosaryCount, (e, p, r) => true);

  // change data
  Function(String) get changeMsgTitle => _msgTitleController.sink.add;

  Function(String) get changeMsgBody => _msgBodyController.sink.add;

  Function(String) get changeTopic => _topicController.sink.add;

  void addNotification(MemUser userDetails) {
//    msg.type == MessageType.admin
//        ? MemNotification(flutterLocalNotificationsPlugin).showNotification(
//        NotificationMessage(
//            title: msg.title, body: msg.body, payload: "recite a rosary"))
//        : '';
    String validMsgTitle = _msgTitleController.value;

    final validMsgBody = _msgBodyController.value;

    final validTopic = _topicController.value;

    FCMNotification notification = FCMNotification(
        title: validMsgTitle ?? "",
        body: validMsgBody ?? "",
        topic: validTopic,
        author: userDetails.name,
        time: DateTime.now().add(Duration(minutes: 1)),
        authorID: userDetails.id ?? "",
        priority: "medium");

    messageRepository.addNotification(notification.toEntity());

    MEMmessage msg = MEMmessage(
        title: validMsgTitle ?? "",
        body: validMsgBody ?? "",
        type: "admin",
        author: userDetails.name,
        time: DateTime.now(),
        authorID: userDetails.id ?? "");

//    List<String> _regions;
//    userRepository.regions().listen((regions) {
//      _regions = regions.toList();
//      _regions.forEach((region) =>
//          messageRepository.addRegionalMessage(msg.toEntity(), region));
//    }).onError((error) {
//      debugPrint(
//          "Error in getting regions data from firebase" + error.toString());
//    });

    messageRepository.addLocaleMessage(msg.toEntity(), validTopic);
  }

  void updateMessage(String msgType) {
//    final validMsgTitle = _msgTitleController.value;
//    final validMsgBody = _msgBodyController.value;
//
//    messageRepository.updateMessage(msg.toEntity());
  }

  dispose() {
    _msgBodyController.close();

    _msgTitleController.close();

    _topicController.close();
  }
}

/// The equivalent of [NotificationProvider], but for [NotificationBloc].
class NotificationProvider extends InheritedWidget {
  final NotificationBloc notificationBloc;

  NotificationProvider({
    Key key,
    @required NotificationBloc notificationBloc,
    Widget child,
  })  : assert(notificationBloc != null),
        notificationBloc = notificationBloc,
        super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static NotificationBloc of(BuildContext context) =>
      (context.findAncestorWidgetOfExactType<NotificationProvider>())
          .notificationBloc;
}
