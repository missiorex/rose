import 'dart:async';

class NotificationValidators {
  final validateMsgTitle = StreamTransformer<String, String>.fromHandlers(
      handleData: (msgTitle, sink) {
    if (msgTitle.length < 50) {
      //For checking password & confirm password

      sink.add(msgTitle);
    } else {
      sink.addError('Maximum 50 Characters for the title, please !');
    }
  });

  final validateMsgBody = StreamTransformer<String, String>.fromHandlers(
      handleData: (msgBody, sink) {
    if (msgBody.length < 550) {
      //For checking password & confirm password

      sink.add(msgBody);
    } else {
      sink.addError('Maximum 550 Characters for the title, please !');
    }
  });
}
