import 'dart:async';

class RosaryValidators {
  final validateAuthor = StreamTransformer<String, String>.fromHandlers(
      handleData: (author, sink) {
    if (author.length < 21) {
      //For checking password & confirm password

      sink.add(author);
    } else {
      sink.addError('Maximum 20 Characters!');
    }
  });

  final validateRosaryCount = StreamTransformer<String, int>.fromHandlers(
      handleData: (rosaryCount, sink) {
    try {
      //Temporarily build
      //if (int.parse(rosaryCount) > 0 && int.parse(rosaryCount) <= 1000) {
      if (int.parse(rosaryCount) > 0 && int.parse(rosaryCount) <= 1000000) {
        //For checking password & confirm password

        sink.add(int.parse(rosaryCount));
      } else {
        sink.addError('Max 1000');
      }
    } catch (e) {
      sink.addError("Only Number!");
    }
  });
}
