import 'dart:async';
import 'package:mem_repository/mem_repository.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';
import 'package:mem_blocs/rosary_bloc/rosary_validators.dart';
import 'package:mem_models/mem_models.dart';
import 'package:base_helpers/basehelpers.dart';
import 'package:mem_entities/mem_entities.dart';

class RosaryBloc extends Object with RosaryValidators {
  //The Repository Object to store the rosary details to the database.
  final ReactiveMessageRepository messageRepository;
  final ReactiveRosaryRepository rosaryRepository;
  final ReactiveRewardsRepository rewardsRepository;
  // This is the internal state. It's mostly a helper object so that the code
  // in this class only deals with streams.
  //final _rosary = Rosary()

  // These are the internal objects whose streams /   sinks are provided
  // by this component. See below for what each means.
  //static int total_count_initial;
  final _rosaryTotalCount = BehaviorSubject<int>.seeded(1);
  final _rosaryTotalCountController = StreamController<int>();
  final _authorController = BehaviorSubject<String>();
  final _rosaryCountController = BehaviorSubject<String>();

  static bool isFirst = true;

  //An attempt to pipe the database stream
//  Stream<int> _totalCount = Stream.empty();
//  ReplaySubject<int> _query = ReplaySubject<int>();
//  Sink<int> get query => _query;
//  Stream<int> get totalCount => _totalCount;

  /// Because we're piping an output of one BLoC into an input of another,
  /// we need to hold the subscription object in order to cancel it later.
  StreamSubscription _subscription;

  RosaryBloc(
      this.messageRepository, this.rosaryRepository, this.rewardsRepository) {
    _rosaryTotalCountController.stream.listen(_handleAddition);
//    _totalCount = _query
//        .distinct()
//        .asyncMap(rosaryRepository.fetchRosary())
//        .asBroadcastStream();
  }

  /// This is the input of additions to the rosary count. Use this to signal
  /// to the component that user is adding rosary counts.
  Sink<int> get TotalRosaryAddition => _rosaryTotalCountController.sink;

  /// This stream has a new value whenever the count of items in the cart
  /// changes.
  ///
  /// We're using the `distinct()` transform so that only values that are
  /// in fact a change will be published by the stream.
  Stream<int> get rosaryTotalCount => _rosaryTotalCount.stream.distinct();
  Stream<String> get author =>
      _authorController.stream.transform(validateAuthor);
  Stream<int> get rosaryCount =>
      _rosaryCountController.stream.transform(validateRosaryCount);

  Stream<bool> get submitValidRosaryCount =>
      Rx.combineLatest2(author, rosaryCount, (e, p) => true);

  Stream<int> get submitValidRosaryListCount => rosaryCount;

  Function(String) get changeAuthor => _authorController.sink.add;
  Function(String) get changeRosaryCount => _rosaryCountController.sink.add;

  /// Take care of closing streams.
  void dispose() {
    _rosaryTotalCount.close();
    _rosaryTotalCountController.close();
    _authorController.close();
    _rosaryCountController.close();
//    _query.close();
  }

  /// Business logic for adding rosary counts. Adds new events to outputs
  /// as needed.
  void _handleAddition(int count) {
//    if (!isFirst) {
//      RosaryEntity rosary = RosaryEntity(Uuid().generateV4().toString(), 10000,
//          14500 + count, 12333, 12121212, 121212, DateTime.now());
//      rosaryRepository.addNewRosary(rosary);
//    }
    _rosaryTotalCount.add(count);
    isFirst = false;
  }

  RosaryCount addRosaryListEntry(MemUser userDetails) {
    final validAuthor = _authorController.value;
    final validRosaryCount = int.parse(_rosaryCountController.value);
    String validMsgTitle = "";

//    validRosaryCount > 1
//        ? validMsgTitle =
//            validAuthor + " - " + validRosaryCount.toString() + " Rosaries"
//        : validMsgTitle =
//            validAuthor + " - " + validRosaryCount.toString() + " Rosary";

    validMsgTitle = "Offered by " + validAuthor;

    String rose = String.fromCharCodes(Runes(' \u{1F339} '));

    final validMsgBody = rose + " Ave Maria " + rose;

    MEMmessage msg = MEMmessage(
        title: validMsgTitle,
        body: validMsgBody,
        type: "rosary",
        rosaryCount: 0,
        rosaryByOthers: validRosaryCount,
        author: userDetails.displayName ?? userDetails.name,
        authorID: userDetails.id,
        time: DateTime.now());

    RosaryCount rc = RosaryCount(
        totalCount: validRosaryCount,
        author: validAuthor,
        rosaryCount: validRosaryCount,
        uid: userDetails.id,
        time: DateTime.now());

    Rosary r = Rosary(
        totalCount: 0,
        totalByUsers: 0,
        totalExternal: validRosaryCount,
        time: DateTime.now());

    if (validRosaryCount > 50) {
      //Add Admin Notification, to notify new user registration.
      String NotificationMsgTitle =
          "Rosary Collected by " + userDetails.name + " exceeds 50 ";

      final NotificationMsgBody = userDetails.name +
          " has collected and offered in the name of " +
          validAuthor +
          validRosaryCount.toString() +
          " rosaries in a single offering. Please check.";
      final validTopic = "rosary_limits";

      FCMNotification notification = FCMNotification(
          title: NotificationMsgTitle ?? "",
          body: NotificationMsgBody ?? "",
          topic: validTopic,
          author: "",
          time: DateTime.now().add(Duration(minutes: 1)),
          authorID: "",
          priority: "medium");

      messageRepository.addAdminNotification(notification.toEntity());
    }

    rosaryRepository.addRosaryListEntry(rc.toEntity());
    rosaryRepository.updateRosaryCount(0, validRosaryCount);
    rosaryRepository.addNewRosary(r.toEntity());
    //rosaryRepository.updateRosaryCountUser(0, validRosaryCount, userDetails.id);
    messageRepository.addNewMessage(msg.toEntity());

    int total_stars = msg.rosaryByOthers + userDetails.rosariesCollected;

    // Update Rewards - Good Samaritan
    rewardsRepository
        .starsChannelEntryExists(userDetails.id, "good_samaritan")
        .then((channelExists) {
      if (channelExists)
        rewardsRepository.addStars(
            msg.rosaryByOthers, userDetails.id, "good_samaritan");
      else
        rewardsRepository.addStarsChannelEntry(StarsChannelEntity(
            "good_samaritan",
            "Good Samaritan",
            userDetails.id,
            DateTime.now().millisecondsSinceEpoch,
            "COLLECT ROSARY",
            total_stars,
            MEMStarLevels().goodSamaritanLevel(total_stars)));
    });

    _rosaryCountController.sink.add(0.toString());
    return rc;
  }

  void updateRosaryListEntry(RosaryCount rc, MemUser userDetails) {
    //final validAuthor = _authorController.value;
    final validRosaryCount = int.parse(_rosaryCountController.value);
    String validMsgTitle = "";

//    validRosaryCount > 1
//        ? validMsgTitle =
//            rc.author + " - " + validRosaryCount.toString() + " Rosaries"
//        : validMsgTitle =
//            rc.author + " - " + validRosaryCount.toString() + " Rosary";
    validMsgTitle = "Offered by " + rc.author;

    String rose = String.fromCharCodes(Runes(' \u{1F339} '));

    final validMsgBody = rose + " Ave Maria " + rose;

//    validRosaryCount > 1
//        ? validMsgTitle = validAuthor +
//            " OFFERS " +
//            validRosaryCount.toString() +
//            " ROSARIES TO THE IMMACULATE HEART OF MOTHER MARY"
//        : validMsgTitle = validAuthor +
//            " OFFERS A "
//            "ROSARY TO THE IMMACULATE HEART OF MOTHER MARY";

    MEMmessage msg = MEMmessage(
        title: validMsgTitle,
        body: validMsgBody,
        type: "rosary",
        rosaryCount: 0,
        rosaryByOthers: validRosaryCount,
        author: userDetails.displayName ?? userDetails.name,
        authorID: userDetails.id,
        time: DateTime.now());

    RosaryCount _rc = RosaryCount(
        id: rc.id,
        totalCount: validRosaryCount + rc.totalCount,
        author: rc.author,
        rosaryCount: validRosaryCount,
        uid: rc.uid,
        time: DateTime.now());

    Rosary r = Rosary(
        totalCount: 0,
        totalByUsers: 0,
        totalExternal: validRosaryCount,
        time: DateTime.now());

    if (validRosaryCount > 50) {
      //Add Admin Notification, to notify new user registration.
      String NotificationMsgTitle =
          "Rosary Collected by " + userDetails.name + " exceeds 50 ";

      final NotificationMsgBody = userDetails.name +
          " has collected and offered in the name of " +
          rc.author +
          validRosaryCount.toString() +
          " rosaries in a single offering. Please check.";
      final validTopic = "rosary_limits";

      FCMNotification notification = FCMNotification(
          title: NotificationMsgTitle ?? "",
          body: NotificationMsgBody ?? "",
          topic: validTopic,
          author: userDetails.name,
          time: DateTime.now().add(Duration(minutes: 1)),
          authorID: "",
          priority: "medium");

      messageRepository.addAdminNotification(notification.toEntity());
    }

    rosaryRepository.updateRosaryListEntry(_rc.toEntity());
    rosaryRepository.updateRosaryCount(0, validRosaryCount);
    rosaryRepository.addNewRosary(r.toEntity());
    //rosaryRepository.updateRosaryCountUser(0, validRosaryCount, rc.uid);
    messageRepository.addNewMessage(msg.toEntity());

    int total_stars = msg.rosaryByOthers + userDetails.rosariesCollected;

    // Update Rewards - Good Samaritan
    rewardsRepository
        .starsChannelEntryExists(userDetails.id, "good_samaritan")
        .then((channelExists) {
      if (channelExists)
        rewardsRepository.addStars(
            msg.rosaryByOthers, userDetails.id, "good_samaritan");
      else
        rewardsRepository.addStarsChannelEntry(StarsChannelEntity(
            "good_samaritan",
            "Good Samaritan",
            userDetails.id,
            DateTime.now().millisecondsSinceEpoch,
            "COLLECT ROSARY",
            total_stars,
            MEMStarLevels().goodSamaritanLevel(total_stars)));
    });

    _rosaryCountController.sink.add(0.toString());
  }

//  void removeMessage(Message msg) {
//    List<String> idList = new List<String>();
//    idList.add(msg.id);
//    messageRepository.deleteMessage(idList);
//  }

//  void updateRosary(Message msg) {
//    final validMsgTitle = _msgTitleController.value;
//    final validMsgBody = _msgBodyController.value;
//    final validRosaryCount = _rosaryCountController.value;
//
//    int rosaryCountDiff = msg.rosaryCount - int.parse(validRosaryCount);
//
//    msg.title = validMsgTitle;
//    msg.body = validMsgBody;
//    msg.rosaryCount = int.parse(validRosaryCount);
//    messageRepository.updateMessage(msg.toEntity());
//    rosaryRepository.updateRosaryCount(rosaryCountDiff);
//  }
}
