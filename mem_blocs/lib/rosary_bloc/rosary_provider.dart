import 'package:flutter/widgets.dart';
import 'package:mem_blocs/rosary_bloc/rosary_bloc.dart';
import 'package:mem_repository/mem_repository.dart';

/// This is an InheritedWidget that wraps around [RosaryBloc]. Think about this
/// as Scoped Model for that specific class.
///
/// This merely solves the "passing reference down the tree" problem for us.
/// You can solve this in other ways, like through dependency injection.
///
///todo Also note that this does not call [RosaryBloc.dispose]. If your app
///todo ever doesn't need to access this bloc, you should make sure it's
///todo disposed of properly.
///
class RosaryProvider extends InheritedWidget {
  final RosaryBloc rosaryBloc;

//  RosaryProvider({
//    Key key,
//    RosaryBloc rosaryBloc,
//    Widget child,
//  })  : rosaryBloc = rosaryBloc ?? RosaryBloc(),
//        super(key: key, child: child);

  RosaryProvider({Key key, @required this.rosaryBloc, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static RosaryBloc of(BuildContext context) =>
//      (context.inheritFromWidgetOfExactType(RosaryProvider) as RosaryProvider)
//          .rosaryBloc;
      (context.findAncestorWidgetOfExactType<RosaryProvider>()).rosaryBloc;
}
