import 'package:flutter/widgets.dart';
import 'package:mem_blocs/mem_blocs.dart';
import 'package:mem_repository/mem_repository.dart';

/// This is an InheritedWidget that wraps around [RosaryBloc]. Think about this
/// as Scoped Model for that specific class.
///
/// This merely solves the "passing reference down the tree" problem for us.
/// You can solve this in other ways, like through dependency injection.
///
///todo Also note that this does not call [RosaryBloc.dispose]. If your app
///todo ever doesn't need to access this bloc, you should make sure it's
///todo disposed of properly.
///
class UserProvider extends InheritedWidget {
  final UserBloc userBloc;

//  RosaryProvider({
//    Key key,
//    RosaryBloc rosaryBloc,
//    Widget child,
//  })  : rosaryBloc = rosaryBloc ?? RosaryBloc(),
//        super(key: key, child: child);

  UserProvider({Key key, @required this.userBloc, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static UserBloc of(BuildContext context) =>
      (context.findAncestorWidgetOfExactType<UserProvider>()).userBloc;
}
