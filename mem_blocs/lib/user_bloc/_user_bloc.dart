import 'dart:async';
import 'package:rxdart/subjects.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserBloc {
  User user;

  //The Repository Object to store the rosary details to the database.
  //final UserRepository userRepository;

  // This is the internal state. It's mostly a helper object so that the code
  // in this class only deals with streams.
  //final _rosary = Rosary()

  // These are the internal objects whose streams / sinks are provided
  // by this component. See below for what each means.
  //static int total_count_initial;
  final _userLogIn = BehaviorSubject<bool>.seeded(false);
  final _userController = StreamController<UserEntity>();
  final _userLoginController = StreamController<UserEntity>();

  /// Because we're piping an output of one BLoC into an input of another,
  /// we need to hold the subscription object in order to cancel it later.
  StreamSubscription _subscription;

  UserBloc() {
    _userController.stream.listen(_handleUserAddition);
    _userLoginController.stream.listen(_handleUserLogin);
  }

  /// This is the input of additions to the rosary count. Use this to signal
  /// to the component that user is adding rosary counts.
  Sink<UserEntity> get UserAddition => _userController.sink;
  Sink<UserEntity> get UserLogin => _userLoginController.sink;

  /// This stream has a new value whenever the count of items in the cart
  /// changes.
  ///
  /// We're using the `distinct()` transform so that only values that are
  /// in fact a change will be published by the stream.
  Stream<bool> get userLogIn => _userLogIn.stream.distinct();

  /// Take care of closing streams.
  void dispose() {
    _userLogIn.close();
    _userController.close();
    _userLoginController.close();
    UserLogin.close();
    UserAddition.close();
  }

  /// Business logic for adding rosary counts. Adds new events to outputs
  /// as needed.
  void _handleUserAddition(UserEntity user) {
//    if (!isFirst) {
//      RosaryEntity rosary = RosaryEntity(Uuid().generateV4().toString(), 10000,
//          14500 + count, 12333, 12121212, 121212, DateTime.now());
//      rosaryRepository.addNewRosary(rosary);
//    }
//    _rosaryCount.add(count);
//    isFirst = false;
  }

  void _handleUserLogin(UserEntity user) {
//    if (!isFirst) {
//      RosaryEntity rosary = RosaryEntity(Uuid().generateV4().toString(), 10000,
//          14500 + count, 12333, 12121212, 121212, DateTime.now());
//      rosaryRepository.addNewRosary(rosary);
//    }
//    _rosaryCount.add(count);
//    isFirst = false;
  }
}
