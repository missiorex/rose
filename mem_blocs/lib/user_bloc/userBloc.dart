import 'dart:async';
import 'validators.dart';
import 'package:rxdart/rxdart.dart';
import 'package:mem_repository/mem_repository.dart';
import 'package:meta/meta.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:mem_models/mem_models.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

//* Using a shortcut getter method on the class to create simpler and friendlier API for the class to provide access of a particular function on StreamController
//* Mixin can only be used on a class that extends from a base class, therefore, we are adding Bloc class that extends from the Object class
//NOTE: Or you can write "class Bloc extends Validators" since we don't really need to extend Bloc from a base class
class UserBloc extends Object with Validators {
  final ReactiveUserRepository userRepository;
  final ReactiveMessageRepository messageRepository;
  final AuthRepository authRepository;
  User firebaseUser;
  MemUser userDetails;
  UserState userState = UserState();

  //* "_" sets the instance variable to a private variable
  //NOTE: By default, streams are created as "single-subscription stream", but in this case and in most cases, we need to create "broadcast stream"
  //Note(con'd): because the email/password streams are consumed by the email/password fields as well as the combineLastest2 RxDart method
  //Note:(con'd): because we need to merge these two streams as one and get the lastest streams of both that are valid to enable the button state
  //Note:(con'd): Thus, below two streams are being consumed multiple times
  //* original single-subscription stream
  // final _emailController = StreamController<String>();
  // final _passwordController = StreamController<String>();

  //* Broadcast stream
  // final _emailController = StreamController<String>.broadcast();
  // final _passwordController = StreamController<String>.broadcast();

  //* Replacing above Dart StreamController with RxDart BehaviourSubject (which is a broadcast stream by default)
  //NOTE: We are leveraging the additional functionality from BehaviorSubject to go back in time and retrieve the lastest value of the streams for form submission
  //NOTE: Dart StreamController doesn't have such functionality
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  final _passwordConfirmController = BehaviorSubject<String>();
  final _phoneNumberController = BehaviorSubject<String>();
  final _regionController = BehaviorSubject<String>();
  final _titleController = BehaviorSubject<String>();
  final _localeController = BehaviorSubject<String>();

  //constructor
  UserBloc(this.userRepository, this.authRepository, this.messageRepository);

  // Add data to stream
  Stream<String> get title => _titleController.stream;
  Stream<String> get email => _emailController.stream.transform(validateEmail);
  Stream<String> get password =>
      _passwordController.stream.transform(validatePassword);
  Stream<String> get confirmPassword =>
      _passwordConfirmController.stream.transform(validatePwdMatch);
  Stream<String> get phoneNumber =>
      _phoneNumberController.stream.transform(validatePhoneNumber);
  Stream<String> get region => _regionController.stream;
  Stream<String> get locale => _localeController.stream;

  Stream<bool> get submitValidSignup => Rx.combineLatest6(
      email,
      password,
      confirmPassword,
      phoneNumber,
      region,
      locale,
      (e, p, c, n, r, l) => true);
  Stream<bool> get submitValidPhoneSignup =>
      Rx.combineLatest3(title, region, locale, (t, r, l) => true);

  Stream<bool> get submitValidPSignup =>
      Rx.combineLatest2(title, locale, (t, l) => true);

  Stream<bool> get submitValidSignIn =>
      Rx.combineLatest2(email, password, (e, p) => true);

  // change data
  Function(String) get changeTitle => _titleController.sink.add;
  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;
  Function(String) get changeConfirmPassword =>
      _passwordConfirmController.sink.add;
  Function(String) get changePhoneNumber => _phoneNumberController.sink.add;
  Function(String) get changeRegion => _regionController.sink.add;
  Function(String) get changeLocale => _localeController.sink.add;

  Future<User> signUp(MemUser userDetails) async {
    final validTitle = _titleController.value;
    final validEmail = _emailController.value;
    final validPassword = _passwordController.value;
    //final validConfirmPassword = _passwordConfirmController.value;
    final validPhoneNumber = _phoneNumberController.value;

    final validRegion = _regionController.value;
    final validLocale = _localeController.value;

    User firebaseUser;
    userDetails = userDetails;
    // Add new user to Firebase authentication

    await authRepository
        .addUser(userName: validEmail, password: validPassword)
        .then((user) {
      firebaseUser = user;
    });

    //If use added to firebase authentication, add the user details to database.
    firebaseUser != null
        ? userRepository.addNewUser(UserEntity(
            id: firebaseUser.uid,
            title: userDetails.title ?? "",
            //title: validTitle,
            emailId: validEmail ?? "",
            password: "",
            status: false,
            userAddedOn: DateTime.now().millisecondsSinceEpoch,
            displayName: userDetails.displayName ?? userDetails.name ?? "",
            photoUrl: "",
            name: userDetails.name ?? "",
            country: userDetails.country ?? "",
            countryCode: userDetails.countryCode ?? "",
            region: validRegion,
            profilePhotoUrl:
                "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a",
            gender: userDetails.gender ?? "",
            phoneNumber: validPhoneNumber ?? "",
            role: "user",
            rosaryLastAddedOn: DateTime.now().millisecondsSinceEpoch,
            rosariesOffered: 0,
            rosariesCollected: 0,
            birthday:
                userDetails.birthday ?? DateTime.now().millisecondsSinceEpoch,
            isBlocked: false,
            isSuperAdmin: false,
            registrationCalls: 0,
            pushNotificationToken: userDetails.pushNotificationToken ?? "",
            locale: validLocale ?? "",
          ))
        : null;

    print('Email is $validEmail, and password is $validPassword');

    //Add Admin Notification, to notify new user registration.
    String validMsgTitle = "New Registartion from " + validRegion;

    final validMsgBody = """
    Following user registration pending for your action: Please go to admin dashboard and activate.
    User Name: """ +
        userDetails.name +
        """ """ +
        """ Country:""" +
        userDetails.country +
        """ """ +
        """ Phone Number: $validPhoneNumber 
      
     """;

    final validTopic = "new_user";

    FCMNotification notification = FCMNotification(
        title: validMsgTitle ?? "",
        body: validMsgBody ?? "",
        topic: validTopic,
        author: "",
        time: DateTime.now().add(Duration(minutes: 1)),
        authorID: "",
        priority: "medium");

    messageRepository.addAdminNotification(notification.toEntity());

    return firebaseUser;
  }

  Future<User> pSignUp(MemUser userDetails, User firebaseUser) async {
    //final validPhoneNumber = _phoneNumberController.value;
    final validRegion = userDetails.region;
    final validLocale = _localeController.value;
    final validTitle = _titleController.value;
    //If use added to firebase authentication, add the user details to database.
    firebaseUser != null
        ? userRepository.addNewUser(UserEntity(
            id: firebaseUser.uid,
            emailId: "",
            password: "",
            status: false,
            userAddedOn: DateTime.now().millisecondsSinceEpoch,
            displayName: userDetails.displayName ?? userDetails.name ?? "",
            photoUrl: "",
            title: validTitle ?? "",
            name: userDetails.name ?? "",
            country: userDetails.country ?? "",
            countryCode: userDetails.countryCode ?? "",
            region: validRegion ?? "",
            profilePhotoUrl:
                "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a",
            gender: userDetails.gender ?? "",
            phoneNumber: firebaseUser.phoneNumber ?? "",
            role: "user",
            rosaryLastAddedOn: DateTime.now().millisecondsSinceEpoch,
            rosariesOffered: 0,
            rosariesCollected: 0,
            birthday:
                userDetails.birthday ?? DateTime.now().millisecondsSinceEpoch,
            isBlocked: false,
            isSuperAdmin: false,
            registrationCalls: 0,
            pushNotificationToken: userDetails.pushNotificationToken ?? "",
            locale: validLocale ?? "",
          ))
        : null;

    //Add Admin Notification, to notify new user registration.
    String validMsgTitle = "New Registartion from " + validRegion;

    final validMsgBody = """
    Following user registration pending for your action: Please go to admin dashboard and activate.
    User Name: """ +
        validTitle +
        " " +
        userDetails.name +
        """ """ +
        """ Country:""" +
        userDetails.country +
        """ """ +
        """ Phone Number: """ +
        firebaseUser.phoneNumber;

    final validTopic = "new_user";

    FCMNotification notification = FCMNotification(
        title: validMsgTitle ?? "",
        body: validMsgBody ?? "",
        topic: validTopic,
        author: "",
        time: DateTime.now().add(Duration(minutes: 1)),
        authorID: "",
        priority: "medium");

    messageRepository.addAdminNotification(notification.toEntity());

    return firebaseUser;
  }

  Future<User> phoneSignUp(MemUser userDetails, User firebaseUser) async {
    //final validPhoneNumber = _phoneNumberController.value;
    final validRegion = _regionController.value;
    final validLocale = _localeController.value;
    final validTitle = _titleController.value;
    //If use added to firebase authentication, add the user details to database.
    firebaseUser != null
        ? userRepository.addNewUser(UserEntity(
            id: firebaseUser.uid,
            emailId: "",
            password: "",
            status: false,
            userAddedOn: DateTime.now().millisecondsSinceEpoch,
            displayName: userDetails.displayName ?? userDetails.name ?? "",
            photoUrl: "",
            title: validTitle ?? "",
            name: userDetails.name ?? "",
            country: userDetails.country ?? "",
            countryCode: userDetails.countryCode ?? "",
            region: validRegion ?? "",
            profilePhotoUrl:
                "https://firebasestorage.googleapis.com/v0/b/memapp-4c4c5.appspot.com/o/profile%2Fplaceholder-face.png?alt=media&token=51256368-5163-40f3-b9f7-f7b08194cd8a",
            gender: userDetails.gender ?? "",
            phoneNumber: firebaseUser.phoneNumber ?? "",
            role: "user",
            rosaryLastAddedOn: DateTime.now().millisecondsSinceEpoch,
            rosariesOffered: 0,
            rosariesCollected: 0,
            birthday:
                userDetails.birthday ?? DateTime.now().millisecondsSinceEpoch,
            isBlocked: false,
            isSuperAdmin: false,
            registrationCalls: 0,
            pushNotificationToken: userDetails.pushNotificationToken ?? "",
            locale: validLocale ?? "",
          ))
        : null;

    //Add Admin Notification, to notify new user registration.
    String validMsgTitle = "New Registartion from " + validRegion;

    final validMsgBody = """
    Following user registration pending for your action: Please go to admin dashboard and activate.
    User Name: """ +
        validTitle +
        " " +
        userDetails.name +
        """ """ +
        """ Country:""" +
        userDetails.country +
        """ """ +
        """ Phone Number: """ +
        firebaseUser.phoneNumber;

    final validTopic = "new_user";

    FCMNotification notification = FCMNotification(
        title: validMsgTitle ?? "",
        body: validMsgBody ?? "",
        topic: validTopic,
        author: "",
        time: DateTime.now().add(Duration(minutes: 1)),
        authorID: "",
        priority: "medium");

    messageRepository.addAdminNotification(notification.toEntity());

    return firebaseUser;
  }

  Future<UserState> signIn() async {
    final validEmail = _emailController.value;
    final validPassword = _passwordController.value;
    User firebaseUser;

    ///todo implement the logic for signin.

    await authRepository
        .logIntoFirebase(userName: validEmail, password: validPassword)
        .then((user) async {
      if (user != null) userState.isAuthenticated = true;

      await userActive(user.uid).then((_isActive) {
        if (_isActive) userState.isActive = true;

        _isActive && user != null ? firebaseUser = user : firebaseUser = null;
      }).catchError((error) {
        _passwordController.addError("User Not active");
        userState.isActive = false;
        throw "User Not Active";
      });

      ///todo implement the logic for validation.
    }).catchError((onError) {
      _passwordController
          .addError("Username or password is wrong. Please try again");
      userState.isAuthenticated = false;
      throw "User Not Authenticated";
    });

    userState.user = firebaseUser;

    ///
    //print("Firebase User from bloc user : " + firebaseUser.toString());
    return userState;
  }

  Future<UserState> phoneSignIn(String verId, String smsCode) async {
    User firebaseUser;

    ///todo implement the logic for signin.
    ///
    ///

    await authRepository
        .phoneLogin(verificationId: verId, smsCode: smsCode)
        .then((user) async {
      if (user != null) userState.isAuthenticated = true;

      await userActive(user.uid).then((_isActive) {
        if (_isActive) userState.isActive = true;

        _isActive && user != null ? firebaseUser = user : firebaseUser = null;
      }).catchError((error) {
        userState.isActive = false;
        //throw Exception("User Not Active");
      });

      ///todo implement the logic for validation.s
    }).catchError((onError) {
      userState.isAuthenticated = false;

      /// Commenting this for inital release
      //throw Exception("User Not Authenticated");
    });

    userState.user = firebaseUser;

    ///
    print("Firebase User from bloc user : " + firebaseUser.toString());
    return userState;
  }

  ///todo if required convert this into a stream.
  Future<bool> userActive(String uid) async {
    bool _isActive;
    await userRepository.userActive(uid).then((isActive) {
      _isActive = isActive;
    });

    return _isActive;
  }

  Future<MemUser> userProfile(String uid) async {
    await userRepository.getUser(uid).then((user) {
      userDetails = MemUser.fromEntity(user);
    });

    return userDetails;
  }

  Future<MemUser> authorUser(String uid) async {
    MemUser _user;

    await userRepository.getUser(uid).then((user) {
      _user = MemUser.fromEntity(user);
    });

    return _user;
  }

  dispose() {
    _regionController.close();
    _localeController.close();
    _titleController.close();
    _emailController.close();
    _passwordController.close();
    _passwordConfirmController.close();
    _phoneNumberController.close();
  }
}

//Note: This creates a global instance of Bloc that's automatically exported and can be accessed anywhere in the app
//final bloc = Bloc();
