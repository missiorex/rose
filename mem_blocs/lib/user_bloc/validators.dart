import 'dart:async';

class Validators {
  static String _password;

  final validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    if (email.contains('@')) {
      sink.add(email);
    } else {
      sink.addError('Enter a valid email');
    }
  });

  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
    if (password.length > 5) {
      //For checking password & confirm password
      _password = password;
      sink.add(password);
    } else {
      sink.addError('Invalid password, please enter more than 5 characters');
    }
  });

  final validatePwdMatch = StreamTransformer<String, String>.fromHandlers(
      handleData: (confirmpassword, sink) {
    if (confirmpassword.length > 5) {
      sink.add(confirmpassword);
    } else {
      sink.addError('Invalid password, please enter more than 5 characters');
    }
    confirmpassword == _password
        ? sink.add(confirmpassword)
        : sink.addError('passwords dont match');
  });

  final validatePhoneNumber = StreamTransformer<String, String>.fromHandlers(
      handleData: (phoneNumber, sink) {
    if (isValidPhoneNumber(phoneNumber)) {
      sink.add(phoneNumber);
    } else {
      sink.addError('Invalid Phone Number');
    }
  });

  static bool isValidPhoneNumber(String input) {
    final RegExp regex = new RegExp(
        r'((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))');
    return regex.hasMatch(input);
  }
}
