import 'dart:async';

class MessageValidators {
  final validateMsgTitle = StreamTransformer<String, String>.fromHandlers(
      handleData: (msgTitle, sink) {
    if (msgTitle.length < 40) {
      //For checking password & confirm password

      sink.add(msgTitle);
    } else {
      sink.addError('Maximum 40 Characters for the title, please !');
    }
  });

  final validateAdminMsgBody = StreamTransformer<String, String>.fromHandlers(
      handleData: (adminMsgBody, sink) {
    if (adminMsgBody.length < 5001) {
      //For checking password & confirm password

      sink.add(adminMsgBody);
    } else {
      sink.addError('Maximum 5000 Characters for the title, please !');
    }
  });

  final validateMsgBody = StreamTransformer<String, String>.fromHandlers(
      handleData: (msgBody, sink) {
    if (msgBody.length < 2501) {
      //For checking password & confirm password

      sink.add(msgBody);
    } else {
      sink.addError('Maximum 2500 Characters , please !');
    }
  });

  final validateRosaryCount = StreamTransformer<String, int>.fromHandlers(
      handleData: (rosaryCount, sink) {
    try {
      //For a temp build to update 1 crore roaries.
//      if (int.parse(rosaryCount) > 0) {
      if (int.parse(rosaryCount) > 0 && int.parse(rosaryCount) <= 1000000) {
        //if (int.parse(rosaryCount) > 0 && int.parse(rosaryCount) <= 1000) {
        //For checking password & confirm password

        sink.add(int.parse(rosaryCount));
      } else {
        sink.addError('Max 1000');
      }
    } catch (e) {
      sink.addError("Only Number!");
    }
  });
}
