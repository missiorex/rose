import 'dart:collection';

import 'package:mem_models/mem_models.dart';

/// A page of message items fetched from network.
///
class MessagePage {
  final List<MEMmessage> _messages;

  final int startIndex;

  MessagePage(this._messages, this.startIndex);

  int get count => _messages.length;

  int get endIndex => startIndex + count - 1;

  UnmodifiableListView<MEMmessage> get messages =>
      UnmodifiableListView<MEMmessage>(_messages);

  @override
  String toString() => "_MessagePage($startIndex-$endIndex)";
}
