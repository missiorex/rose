import 'dart:async';
import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:mem_models/mem_models.dart';
import 'package:mem_blocs/message_bloc/message_page.dart';
import 'package:mem_blocs/message_bloc/message_slice.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';
import 'package:mem_entities/entities/message_entity.dart';
import 'package:mem_repository/mem_repository.dart';
import 'package:mem_entities/mem_entities.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'message_validators.dart';
import 'package:base_helpers/basehelpers.dart';

/// This component encapsulates the logic of fetching messages from
/// a database, page by page, according to position in an infinite list.
///
/// Only the data that are close to the current location are cached, the rest
/// are thrown away.
class MessageBloc extends Object with MessageValidators {
  final ReactiveMessageRepository messageRepository;
  final ReactiveRosaryRepository rosaryRepository;
  final ReactiveUserRepository userRepository;
  final ReactiveRewardsRepository rewardsRepository;

  List<MEMmessage> messages;

  static const _messagesPerPage = 10;

  /// We're using ReactiveX's [PublishSubject] here because we want to easily
  /// buffer the stream. See [MessageBloc] constructor.
  final _indexController = PublishSubject<int>();

  /// These are the pages stored in memory. For O(1) retrieval, we're storing
  /// them in a [Map]. The key value is [MessagePage.startIndex].
  final _pages = <int, MessagePage>{};

  /// A set of pages that are currently being fetched from the network.
  /// They are identified by their [MessagePage.startIndex].
  final _pagesBeingRequested = Set<int>();

  final _sliceSubject = BehaviorSubject<MessageSlice>();

  final _msgTitleController = BehaviorSubject<String>();
  final _msgBodyController = BehaviorSubject<String>();
  final _rosaryCountController = BehaviorSubject<String>();
  final _adminMsgBodyController = BehaviorSubject<String>();

  //messageStream;
  final _messageData = BehaviorSubject<QuerySnapshot>();

  MessageBloc(this.messageRepository, this.rosaryRepository,
      this.userRepository, this.rewardsRepository) {
    _indexController.stream
        // Don't try to update too frequently.
        .bufferTime(Duration(milliseconds: 500))
        // Don't update when there is no need.
        .where((batch) => batch.isNotEmpty)
        .listen(_handleIndexes);

    //_subscription = itemsStream.listen(_bloc.cartItems.add);
  }

  //Message CRUD
  // Add data to stream
  Stream<String> get msgTitle =>
      _msgTitleController.stream.transform(validateMsgTitle);
  Stream<String> get adminMsgBody =>
      _adminMsgBodyController.stream.transform(validateAdminMsgBody);

  Stream<String> get msgBody =>
      _msgBodyController.stream.transform(validateMsgBody);
  Stream<int> get rosaryCount =>
      _rosaryCountController.stream.transform(validateRosaryCount);

  //To listen documents data from StreamBuilder
  Stream<QuerySnapshot> get messageData => _messageData.stream;

  //Stream to fetch filtered messages
  Stream<QuerySnapshot> getFilteredMessages(int count, String msgType) {
    return messageRepository.getFilteredMessages(count, msgType);
  }

//  Stream<String> get confirmPassword =>
//      _passwordConfirmController.stream.transform(validatePwdMatch);

  Stream<bool> get submitValidAdminMessage =>
      Rx.combineLatest2(msgTitle, adminMsgBody, (e, p) => true);

  Stream<String> get submitValidMessage => msgBody;
//  Stream<bool> get submitValidRosaryMessage => Observable.combineLatest3(
//      msgTitle, msgBody, rosaryCount, (e, p, r) => true);
  Stream<int> get submitValidRosaryMessage => rosaryCount;
  // change data
  Function(String) get changeMsgTitle => _msgTitleController.sink.add;
  Function(String) get changeAdminMsgBody => _adminMsgBodyController.sink.add;
  Function(String) get changeRosaryCount => _rosaryCountController.sink.add;
  Function(String) get changeMsgBody => _msgBodyController.sink.add;

  /// An input of the indexes that the [ListView.builder]
  /// (or [GridView.builder]) is getting in its builder callbacks. Just push
  /// the index that you get in a [IndexedWidgetBuilder] down this [Sink].
  ///
  /// The component uses this input to figure out which pages it should
  /// be requesting from the network.
  Sink<int> get index => _indexController.sink;

  /// The currently available data, as a slice of the (potentially infinite)
  /// catalog.
  Stream<MessageSlice> get slice => _sliceSubject.stream;

  /// Outputs the [MessagePage.startIndex] given an arbitrary index of
  /// a product.
  int _getPageStartFromIndex(int index) =>
      (index ~/ _messagesPerPage) * _messagesPerPage;

  /// This will handle the incoming [indexes] (that were requested by
  /// a [IndexedWidgetBuilder]) and, if needed, will fetch missing data.
  void _handleIndexes(List<int> indexes) async {
    const maxInt = 0x7fffffff;
    final int minIndex = indexes.fold(maxInt, min);
    final int maxIndex = indexes.fold(-1, max);

    final minPageIndex = _getPageStartFromIndex(minIndex);
    final maxPageIndex = _getPageStartFromIndex(maxIndex);

    for (int i = minPageIndex; i <= maxPageIndex; i += _messagesPerPage) {
      if (_pages.containsKey(i)) continue;
      if (_pagesBeingRequested.contains(i)) continue;

      _pagesBeingRequested.add(i);
      await _requestPage(i).then((page) => _handleNewPage(page, i));
    }

    // Remove pages too far from current scroll position.
    _pages.removeWhere((pageIndex, _) =>
        pageIndex < minPageIndex - _messagesPerPage ||
        pageIndex > maxPageIndex + _messagesPerPage);
  }

  /// Handles arrival of a new [page] from the network. Will call
  /// [_sendNewSlice].
  void _handleNewPage(MessagePage page, int index) {
    _pages[index] = page;
    _pagesBeingRequested.remove(index);
    _sendNewSlice();
  }

  /// Fetches a page of products from a database. The [MessagePage.startIndex]
  /// of the returned value will be [index].
  Future<MessagePage> _requestPage(int index) async {
    // Simulate network delay.
    //List<Message> messages;

    //await Future.delayed(const Duration(milliseconds: 300));

    //await messageRepository.messages();

//    await messageRepository.messages().then((msgList) async {
//      debugPrint("Messages from blocs: " + msgList.toString());
//      msgList.listen((result) async {
//        messages = result.map(Message.fromEntity).toList();
//      });
//    });

    //List<Message> messages = [];

//    rosaryRepository.fetchRosary().then((rosary) {
//      Stream<List<MessageEntity>> msgList = messageRepository.messages();
//      msgList.listen((result) {
//        messages = result.map(Message.fromEntity).toList();
//      });
//    }).catchError((err) {
//      debugPrint("Error : rosaryerr" + err.toString());
//    });
//
//    debugPrint("message list length :" + messages.length.toString());
//    StreamController<MessageEntity> _messageController;
//    List<Message> messages = [];
//
//    _messageController = StreamController.broadcast();
//    load(_messageController);
//    await _messageController.stream.listen((msgEntity) async {
//      Message message = Message.fromEntity(msgEntity);
//      messages.add(message);
//    });

    //Stream<List<MessageEntity>> msgList = await messageRepository.messages();

    //debugPrint("Messages from blocs: " + messages.toString());
//    msgList.last.then((result) {
//      messages = result.map(Message.fromEntity).toList();
//    });

//     Create a list of random products. We seed the random generator with
//     index so that scrolling back to a position gives the same exact products.

    final random = Random(index);
    messages = List.generate(_messagesPerPage, (_) {
      final id = random.nextInt(0xffff).toString();
      final title = "Message " + id + ": Pray, Pray, Pray";
      final body =
          "Message from Mother Mary : Dear Children, I have called you here to my alcove to extend upon you glad tidings of joy. I give to you the call of the ages.";
      final type = "admin";
      final complete = false;
      final r_count = 1;
      final r_by_others = 23;
      final time = DateTime.now();
      return MEMmessage(
          id: id,
          title: title,
          body: body,
          complete: complete,
          type: type,
          rosaryCount: r_count,
          rosaryByOthers: r_by_others,
          time: time);
    });

    //List<Message> _messages = messages.sublist(0, _messagesPerPage);
    // messages.sublist(index--*_messagesPerPage,index*_messagesPerPage);
    return MessagePage(messages, index);
  }

  load(StreamController<MessageEntity> sc) async {
//    Stream<MessageEntity> msgStream = await messageRepository.messages();
//    assert(msgStream != null, "Message stream is still null");
//    debugPrint("Message Stream from load " + msgStream.toString());
//    msgStream.listen(
//        (message) => debugPrint("message from load" + message.body.toString()));
//
//    msgStream.pipe(sc);
  }

  /// Creates a [MessageSlice] from the current [_pages] and sends it
  /// down the [slice] stream.
  void _sendNewSlice() {
    final pages = _pages.values.toList(growable: false);

    final slice = MessageSlice(pages, true);

    _sliceSubject.add(slice);
  }

  Future<String> addMessage(String msgType, MemUser userDetails,
      String selectedRegion, String selectedLocale,
      {String msgId}) async {
//    msg.type == MessageType.admin
//        ? MemNotification(flutterLocalNotificationsPlugin).showNotification(
//        NotificationMessage(
//            title: msg.title, body: msg.body, payload: "recite a rosary"))
//        : '';
    String validMsgTitle = msgType != "admin" ? "" : _msgTitleController.value;

    var confirmation = "";

    final validMsgBody = msgType != "admin"
        ? _msgBodyController.value
        : _adminMsgBodyController.value;

    String _author = userDetails.role == "admin"
        ? "Admin"
        : userDetails.displayName ?? userDetails.name;

    MEMmessage msg = MEMmessage(
        id: msgId,
        title: validMsgTitle ?? "",
        body: validMsgBody ?? "",
        type: msgType,
        author: _author,
        // author: userDetails.displayName ?? userDetails.name,
        time: DateTime.now(),
        authorID: userDetails.id ?? "");

    messageRepository.addNewMessage(msg.toEntity());
    //Add only admin messages to regional channel
    if (msgType == "admin") {
      messageRepository.addRegionalMessage(msg.toEntity(), userDetails.region);
      confirmation = await messageRepository.addRegionalLocaleMessage(
          msg.toEntity(), selectedRegion, selectedLocale);
    }

    return Future.value(confirmation);
  }

  void addRosary(MemUser userDetails, String giftVerse) async {
    final validRosaryCount = _rosaryCountController.value;
    String validMsgTitle = "";

    validMsgTitle = "By " + userDetails.name;

    String rose = String.fromCharCodes(Runes(' \u{1F339} '));

    //final validMsgBody = rose + " Ave Maria " + rose;

    MEMmessage msg = MEMmessage(
        title: validMsgTitle ?? "",
        body: "",
        type: "rosary",
        rosaryCount: int.parse(validRosaryCount),
        author: userDetails.displayName ?? userDetails.name,
        authorID: userDetails.id ?? "",
        time: DateTime.now());

    Rosary r = Rosary(
        totalCount: 0,
        totalByUsers: int.parse(validRosaryCount),
        totalExternal: 0,
        time: DateTime.now());
    //debugPrint("Rosary from message Bloc: " + r.toString());

    if (int.parse(validRosaryCount) > 50) {
      //Add Admin Notification, to notify new user registration.
      String NotificationMsgTitle =
          "Rosary Offering by " + userDetails.name + " exceeds 50 ";

      final NotificationMsgBody = userDetails.name +
          " has offered " +
          validRosaryCount +
          " rosaries in a single offering. Please check.";
      final validTopic = "rosary_limits";

      FCMNotification notification = FCMNotification(
          title: NotificationMsgTitle ?? "",
          body: NotificationMsgBody ?? "",
          topic: validTopic,
          author: "",
          time: DateTime.now().add(Duration(minutes: 1)),
          authorID: "",
          priority: "medium");

      messageRepository.addAdminNotification(notification.toEntity());
    }
    try {
      await rosaryRepository.addNewRosary(r.toEntity());
      await messageRepository.addNewMessage(msg.toEntity());
      await rosaryRepository.updateRosaryCount(msg.rosaryCount, 0);

      int total_stars = msg.rosaryCount + userDetails.rosariesOffered;

      // Update Rewards - Rosary Warrior
      rewardsRepository
          .starsChannelEntryExists(userDetails.id, "rosary_warrior")
          .then((channelExists) {
        if (channelExists)
          rewardsRepository.addStars(
              msg.rosaryCount, userDetails.id, "rosary_warrior");
        else
          rewardsRepository.addStarsChannelEntry(StarsChannelEntity(
              "rosary_warrior",
              "Rosary Warrior",
              userDetails.id,
              DateTime.now().millisecondsSinceEpoch,
              "ADD ROSARY",
              total_stars,
              MEMStarLevels().rosaryWarriorLevel(total_stars)));
      });

//      await rosaryRepository.updateRosaryCountUser(
//          int.parse(validRosaryCount), 0, userDetails.id);
      // Reset the value.
      _rosaryCountController.sink.add(0.toString());
    } catch (e) {
      throw e;
    }
  }

  bool removeMessage(MEMmessage msg) {
    List<String> idList = new List<String>();
    idList.add(msg.id);
    try {
      messageRepository.deleteMessage(idList);
      return true;
    } catch (e) {
      return false;
    }
  }

  bool removeNotification(FCMNotification msg) {
    List<String> idList = new List<String>();
    idList.add(msg.id);
    try {
      messageRepository.deleteNotification(idList, msg.topic);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> removeRosaryMessage(MEMmessage msg, MemUser userDetails) async {
    List<String> idList = new List<String>();
    idList.add(msg.id);

    int rosaryCountByUser = msg.rosaryCount != 0 ? msg.rosaryCount : 0;
    int rosaryCountByOthers = msg.rosaryByOthers != 0 ? msg.rosaryByOthers : 0;

    Rosary r = Rosary(
        totalCount: 0,
        totalByUsers: rosaryCountByUser,
        totalExternal: rosaryCountByOthers,
        time: DateTime.now());

    try {
      await messageRepository.deleteMessage(idList);
      await rosaryRepository.addNewRosary(r.toEntity());
      await rosaryRepository.updateRosaryCount(
          rosaryCountByUser, rosaryCountByOthers);

//      await userRepository.userExists(msg.authorID).then((userExists) {
//        if (userExists) {
//          rosaryRepository.reduceRosaryCountUser(
//              rosaryCountByUser, rosaryCountByOthers, msg.authorID);
//        }
//      });

      if (msg.rosaryCount != 0) {
        int total_stars = userDetails.rosariesOffered - msg.rosaryCount;
        //Remove Rewards
        rewardsRepository
            .starsChannelEntryExists(userDetails.id, "rosary_warrior")
            .then((channelExists) {
          if (channelExists)
            rewardsRepository.removeStars(
                msg.rosaryCount, userDetails.id, "rosary_warrior");
          else
            rewardsRepository.addStarsChannelEntry(StarsChannelEntity(
                "rosary_warrior",
                "Rosary Warrior",
                userDetails.id,
                DateTime.now().millisecondsSinceEpoch,
                "ADD ROSARY",
                total_stars,
                MEMStarLevels().rosaryWarriorLevel(total_stars)));
        });
      } else if (msg.rosaryByOthers != 0) {
        int total_stars = userDetails.rosariesOffered - msg.rosaryByOthers;
        //Remove Rewards
        rewardsRepository
            .starsChannelEntryExists(userDetails.id, "good_samaritan")
            .then((channelExists) {
          if (channelExists)
            rewardsRepository.removeStars(
                msg.rosaryByOthers, userDetails.id, "good_samaritan");
          else
            rewardsRepository.addStarsChannelEntry(StarsChannelEntity(
                "good_samaritan",
                "Good Samaritan",
                userDetails.id,
                DateTime.now().millisecondsSinceEpoch,
                "COLLECT ROSARY",
                total_stars,
                MEMStarLevels().goodSamaritanLevel(total_stars)));
        });
      }

      return true;
    } catch (e) {
      throw e;
      return false;
    }
  }

  bool removeRegionalMessage(MEMmessage msg, MemUser userDetails) {
    if (userDetails.role == 'admin' || msg.authorID == userDetails.id) {
      messageRepository.deleteRegionalMessage(
          msg.toEntity(), userDetails.region);
      return true;
    } else
      return false;
  }

  Future<String> deleteRegionalLocaleMessage(
      String id, String selectedRegion, String selectedLocale) async {
    var confirmation = "";

    confirmation = await messageRepository.deleteRegionalLocaleMessage(
        id, selectedRegion, selectedLocale);
    return Future.value(confirmation);
  }

  void updateRosary(MEMmessage msg) {
    final validMsgTitle = _msgTitleController.value;
    final validMsgBody = _msgBodyController.value;
    final validRosaryCount = _rosaryCountController.value;

    int rosaryCountDiff = msg.rosaryCount - int.parse(validRosaryCount);

    msg.title = validMsgTitle;
    msg.body = validMsgBody;
    msg.rosaryCount = int.parse(validRosaryCount);
    messageRepository.updateMessage(msg.toEntity());
    rosaryRepository.updateRosaryCount(rosaryCountDiff, 0);
  }

  void updateMessage(String msgType) {
    final validMsgTitle = _msgTitleController.value;
    final validMsgBody = _msgBodyController.value;
    final validRosaryCount = _rosaryCountController.value;

    MEMmessage msg = MEMmessage(
        title: validMsgTitle,
        body: validMsgBody,
        type: msgType,
        rosaryCount: int.parse(validRosaryCount));

    messageRepository.updateMessage(msg.toEntity());
  }

  dispose() {
    _msgBodyController.close();
    _adminMsgBodyController.close();
    _msgTitleController.close();
    _rosaryCountController.close();
    _sliceSubject.close();
    _indexController.close();
  }
}

/// The equivalent of [MessageProvider], but for [MessageBloc].
class MessageProvider extends InheritedWidget {
  final MessageBloc messageBloc;

  MessageProvider({
    Key key,
    @required MessageBloc messageBloc,
    Widget child,
  })  : assert(messageBloc != null),
        messageBloc = messageBloc,
        super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static MessageBloc of(BuildContext context) =>
      (context.findAncestorWidgetOfExactType<MessageProvider>()).messageBloc;
}
