
const functions = require('firebase-functions');
let admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();


const settings = {timestampsInSnapshots: true};
db.settings(settings);


// Cut off time. Child nodes older than this will be deleted.
const CUT_OFF_TIME = 96 * 60 * 60 * 1000; // 96 Hours in milliseconds.

/**
 * This cloud schedule triggered function will check for child nodes that are older than the
 * cut-off time. Each child needs to have a `time` attribute.
 */
exports.deleteOldMalayalamChatMessages = functions.pubsub.schedule('every 12 hours').onRun(async(context) => {
    console.log('This will be run every 12 hours!');
 
  const mRef = admin.database().ref('chats/Malayalam/'); // reference to the Malayalam chat room
  const now = Date.now();
  const cutoff = now - CUT_OFF_TIME;
  const oldItemsQuery = mRef.orderByChild('time').endAt(cutoff);
  const snapshot = await oldItemsQuery.once('value');
  // create a map with all children that need to be removed
  const updates = {};
  snapshot.forEach(child => {
    updates[child.key] = null;
  });
  // execute all updates in one go and return the result to end the function
  return mRef.update(updates);
});

/**
 * This cloud schedule triggered function will check for child nodes that are older than the
 * cut-off time. Each child needs to have a `time` attribute.
 */
exports.deleteOldEnglishChatMessages = functions.pubsub.schedule('every 12 hours').onRun(async(context) => {
    console.log('This will be run every 12 hours!');
 
  const eRef = admin.database().ref('chats/English/'); // reference to the English chat room
  const now = Date.now();
  const cutoff = now - CUT_OFF_TIME;
  const oldItemsQuery = eRef.orderByChild('time').endAt(cutoff);
  const snapshot = await oldItemsQuery.once('value');
  // create a map with all children that need to be removed
  const updates = {};
  snapshot.forEach(child => {
    updates[child.key] = null;
  });
  // execute all updates in one go and return the result to end the function
  return eRef.update(updates);
});


/**
 * This database triggered function will check for child nodes that are older than the
 * cut-off time. Each child needs to have a `time` attribute.
 */
// exports.deleteOldItems = functions.database.ref('/chats/Malayalam/{pushId}').onWrite(async (change) => {
//   const ref = change.after.ref.parent; // reference to the parent
//   const now = Date.now();
//   const cutoff = now - CUT_OFF_TIME;
//   const oldItemsQuery = ref.orderByChild('time').endAt(cutoff);
//   const snapshot = await oldItemsQuery.once('value');
//   // create a map with all children that need to be removed
//   const updates = {};
//   snapshot.forEach(child => {
//     updates[child.key] = null;
//   });
//   // execute all updates in one go and return the result to end the function
//   return ref.update(updates);
// });

/**
 * This cloud schedule triggered function will check for child nodes that are younger than the
 * cut-off time. Each child needs to have a `time` attribute. Checks if any of them are failed rosary submission If yes retries.
 */
exports.retryFailedRosarySubmissions = functions.pubsub.schedule('every 3 minutes').onRun((context) => {
    console.log('This will be run every 3 minutes!');

    var _updateTime = Date.now();
    var messageRef = db.collection('mem').doc('rosary').collection("messages");
    
    
    var retryDate = new Date();
    retryDate.setDate(retryDate.getDate()-2);
    var retryTime = retryDate.getTime();

    var archiveDate = new Date();
    archiveDate.setDate(archiveDate.getDate()-90);
    var retryFrom = archiveDate.getTime();


  //return db.runTransaction(transaction => {

    var filteredMessageRef = messageRef.where("complete", "==", false).where("time","<=",retryTime.toString()).where("time",">",retryFrom.toString()).orderBy("time", "asc").limit(1);
    var console_message = "";
    
      
    return filteredMessageRef.get().then(function(querySnapshot) {


          querySnapshot.forEach(function(doc) {


                    const msg = doc.data();  
                    
                    var _selfCount = msg.rosaryCount;
                   
                    var _externalCount = msg.rosaryByOthers;
                    
                    
                    var _updatedAuthor = msg.author;
                    var _updatedAuthorID = msg.authorID;
                    
                    var _details = msg.title;

                    
                    var _isExternal = _externalCount!==0?true:false;

             
                    const _msgId = msg.id;


                    var _updateTime = Date.now();

                    var newTotalCount =0;

                    

                    var rosaryRef = db.collection('mem').doc('rosary_count');
                    var countSnapShoptsRef = db.collection('mem').doc('rosary_count').collection('count_snapshot');
                    var messageDocRef = db.collection('mem').doc('rosary').collection('messages').doc(_msgId);
                    var authorRef = db.collection('mem').doc('rosary').collection('users').doc(_updatedAuthorID);
                    var msgTime = parseInt(msg.time);

                    if(msg.msgtype === 'rosary' && !msg.complete){

                    
                    return db.runTransaction(transaction => {

                      transaction.get(authorRef).then(userDoc => {

                            
                        
                        if (!userDoc.exists) {
                          console.log('No such user!');
                          return false;
                        } else {
                          
                          const author = userDoc.data();

                          return authorRef.update({

                            rosariesOffered:Number(author.rosariesOffered) + Number(_selfCount),
                            rosariesCollected:Number(author.rosariesCollected) + Number(_externalCount),
                            rosaryLastAddedOn:_updateTime

                          });

                        }   


                      }).catch(err => {
                        console.log('Error getting user', err);
                      });



                      return transaction.get(rosaryRef).then(rosaryDoc => {
                        
                         const rosaryCounts = rosaryDoc.data(); 

                       
                        newTotalCount = Number(rosaryCounts.totalCount) + Number(_selfCount) + Number(_externalCount);

                        
                        var newRosarySelfCount = Number(rosaryCounts.rosarySelfCount) + Number(_selfCount) ;

                        
                        var newRosaryExternalCount = Number(rosaryCounts.rosaryExternalCount) + Number(_externalCount) ;

                          countSnapShoptsRef.doc().set({

                          totalCount: newTotalCount,
                          rosarySelfCount: newRosarySelfCount,
                          rosaryExternalCount:newRosaryExternalCount,
                          updatedBy:"retry",
                          updatedByID: _updatedAuthorID,
                          msgId:_msgId,
                          isExternal:_isExternal,
                          details:_details+" original author: "+ _updatedAuthor,
                          time:_updateTime
                        });
               
                        
                        return transaction.update(rosaryRef, {
                          totalCount: newTotalCount,
                          rosarySelfCount: newRosarySelfCount,
                          rosaryExternalCount:newRosaryExternalCount,

                        });


                      });


                  

                    }).then(() => {

                      
                      var updateMessageComplete = messageDocRef.update({complete: true});


                      return console.log('Rosary Count Updated to '+ newTotalCount);
                    }); 


                }

                else{

                  return Promise.resolve("Not a Rosary Message");
                }


                    
          })
          .catch(function(error) {
             
             return console.log("Error getting messages: ", error);

             
          });

          return console.log("Completed Rosary Resubmissions");

    }).catch(function(error) {
       
       return console.log("Error: ", error);
  
    }); 

 

});

//});






exports.updateToBeArchivedMessages =
functions.pubsub.schedule('every 11 hours').onRun((context) => {
    console.log('This will be run every 11 hours!');

    var _updateTime = Date.now();
    var messageRef = db.collection('mem').doc('rosary').collection("messages");
    var messageArchiveRef = db.collection('mem').doc('rosary').collection("message_archive");
    
    var archiveDate = new Date();
  	archiveDate.setDate(archiveDate.getDate()-120);
  	var archiveFrom = archiveDate.getTime();

	return db.runTransaction(transaction => {

		var filteredMessageRef = messageRef.where("time", "<", archiveFrom.toString()).orderBy("time", "asc").limit(450);
		var console_message = "";
	    
	    return transaction.get(filteredMessageRef).then(function(querySnapshot) {


	        querySnapshot.forEach(function(doc) {
	 

				var msgTime = parseInt(doc.data().time);

				if(doc.data().complete &&  msgTime < archiveFrom){
					transaction.update(messageRef.doc(doc.data().id), { complete: false });
				}


	        });

	        if(console_message !== "")
	        	return console.log(console_message);
	        else
	        	return console.log("Success!!");
	    })
	    .catch(function(error) {
	       
	       return console.log("Error getting messages: ", error);

	       
	    });

    }).then(() => {

		return console.log('Messages Ready to be Archived');

     }).catch(function(error) {
       
       return console.log("Error: ", error);

       
    });	

 

});

exports.scheduledArchiveMessagesDb =
functions.pubsub.schedule('every 12 hours').onRun((context) => {
    console.log('This will be run every 12 hours!');

    var _updateTime = Date.now();
    var messageRef = db.collection('mem').doc('rosary').collection("messages");
    var messageArchiveRef = db.collection('mem').doc('rosary').collection("message_archive");

    var archiveDate = new Date();
  	archiveDate.setDate(archiveDate.getDate()-120);
  	var archiveFrom = archiveDate.getTime();


	return db.runTransaction(transaction => {

		var filteredMessageRef = messageRef.where("time", "<", archiveFrom.toString()).orderBy("time", "asc").limit(100);
		var console_message = "";
	    
	    return transaction.get(filteredMessageRef).then(function(querySnapshot) {


	        querySnapshot.forEach(function(doc) {


				var msgTime = parseInt(doc.data().time);

				if(!doc.data().complete &&  msgTime < archiveFrom){
					 transaction.set(messageArchiveRef.doc(doc.data().id),doc.data());
	            	transaction.delete(messageRef.doc(doc.data().id));

	       		}
	       		else {

	       			console_message = "Some documents selected cant be moved, to avoid reducing rosary count";
	       		}
	         

	        });

	        if(console_message !== "")
	        	return console.log(console_message);
	        else
	        	return console.log("Success!!");
	    })
	    .catch(function(error) {
	       
	       return console.log("Error getting messages: ", error);

	       
	    });

}).then(() => {


    

        return console.log('100 messages archived');
      });	

});


exports.addRosary = functions.firestore
    .document('mem/rosary/messages/{documentID}')
    .onCreate((snap, context) => {



      const msg = snap.data();	
      // Get the count of rosary offered by a user
      var _selfCount = msg.rosaryCount;
      // Get the count of rosaries collected by a user
      var _externalCount = msg.rosaryByOthers;
      
      //Get the author name & id of new rosary addition
      var _updatedAuthor = msg.author;
      var _updatedAuthorID = msg.authorID;
      //Get extra details
      var _details = msg.title;

      //Whether the offering is external or by users
      var _isExternal = _externalCount!==0?true:false;

      //The id of the message that updated this.
      const _msgId = msg.id;

        //Update time
      var _updateTime = Date.now();

      var newTotalCount =0;

      // Get a reference to the rosary counts collection & the newly added message

      var rosaryRef = db.collection('mem').doc('rosary_count');
      var countSnapShoptsRef = db.collection('mem').doc('rosary_count').collection('count_snapshot');
   	  var messageRef = db.collection('mem').doc('rosary').collection('messages').doc(_msgId);
      var authorRef = db.collection('mem').doc('rosary').collection('users').doc(_updatedAuthorID);


      //To permanent fail submissions older than a day. 
      var msgDateThreshhold = new Date();
      msgDateThreshhold.setDate(msgDateThreshhold.getDate()-1);
      var msgTimeThreshhold = msgDateThreshhold.getTime(); 


      if(msg.msgtype === 'rosary'&& msg.time > msgTimeThreshhold){

      // Update aggregations in a transaction
      return db.runTransaction(transaction => {

      	transaction.get(authorRef).then(userDoc => {

      		   	      // Add Rosary Count of the Author.
          
			    if (!userDoc.exists) {
			      console.log('No such user!');
			      return false;
			    } else {
			      
			      const author = userDoc.data();

			      return authorRef.update({

			      	rosariesOffered:Number(author.rosariesOffered) + Number(_selfCount),
			      	rosariesCollected:Number(author.rosariesCollected) + Number(_externalCount),
			      	rosaryLastAddedOn:_updateTime

			      });

			    }	  


      	}).catch(err => {
			    console.log('Error getting user', err);
			  });



        return transaction.get(rosaryRef).then(rosaryDoc => {
          
           const rosaryCounts = rosaryDoc.data();	

          // Compute new total count
          newTotalCount = Number(rosaryCounts.totalCount) + Number(_selfCount) + Number(_externalCount);

          // Compute new total rosary count offered directly by users
          var newRosarySelfCount = Number(rosaryCounts.rosarySelfCount) + Number(_selfCount) ;

          // Compute new total rosary count collected by users
          var newRosaryExternalCount = Number(rosaryCounts.rosaryExternalCount) + Number(_externalCount) ;

            countSnapShoptsRef.doc().set({

          	totalCount: newTotalCount,
            rosarySelfCount: newRosarySelfCount,
            rosaryExternalCount:newRosaryExternalCount,
            updatedBy:_updatedAuthor,
            updatedByID: _updatedAuthorID,
            msgId:_msgId,
            isExternal:_isExternal,
            details:_details,
            time:_updateTime
          });
 
          // Update rosary count
          return transaction.update(rosaryRef, {
            totalCount: newTotalCount,
            rosarySelfCount: newRosarySelfCount,
            rosaryExternalCount:newRosaryExternalCount,

          });


        });


    

      }).then(() => {

      	// Updates server time, to fix the issue of client updatimg time wrongly.	
      	var updateMessageComplete = messageRef.update({complete: true,time:Date.now().toString(),});


        return console.log('Rosary Count Updated to '+ newTotalCount);
      });	


  }

  else{

  	return Promise.resolve("Not a Rosary Message");
  }


});



exports.deleteRosary = functions.firestore
    .document('mem/rosary/messages/{documentID}')
    .onDelete((snap, context) => {

      const msg = snap.data();	
      // Get the count of rosary offered by a user
      var _selfCount = msg.rosaryCount;
      // Get the count of rosaries collected by a user
      var _externalCount = msg.rosaryByOthers;
      
      //Get the author name & id of new rosary addition
      var _updatedAuthor = msg.author;
      var _updatedAuthorID = msg.authorID;
      //Get extra details
      var _details = msg.title;

      //Whether the offering is external or by users
      var _isExternal = _externalCount!==0?true:false;

      //The id of the message that updated this.
      var _msgId = msg.id;

      //Update time
      var _updateTime = Date.now();

      var newTotalCount =0;

      var rosaryCounts;


      // Get a reference to the rosary counts collection
      var rosaryRef = db.collection('mem').doc('rosary_count');
      var countSnapShoptsRef = db.collection('mem').doc('rosary_count').collection('count_snapshot');
      var authorRef = db.collection('mem').doc('rosary').collection('users').doc(_updatedAuthorID);

      if(msg.msgtype === 'rosary' && msg.complete){

      // Update aggregations in a transaction
      return db.runTransaction(transaction => {


      	    	transaction.get(authorRef).then(userDoc => {

      		   	      // Add Rosary Count of the Author.
          
			    if (!userDoc.exists) {
			      console.log('No such user!');
			      return false;
			    } else {
			      
			      const author = userDoc.data();

			      return authorRef.update({

			      	rosariesOffered:Number(author.rosariesOffered) - Number(_selfCount),
			      	rosariesCollected:Number(author.rosariesCollected) - Number(_externalCount),
			      	

			      });

			    }	  


      	}).catch(err => {
			    console.log('Error getting user', err);
			  });	



        return transaction.get(rosaryRef).then(rosaryDoc => {
          
          rosaryCounts = rosaryDoc.data();	

          // Compute new total count
          newTotalCount = Number(rosaryCounts.totalCount) - (Number(_selfCount) + Number(_externalCount));

          // Compute new total rosary count offered directly by users
          var newRosarySelfCount = Number(rosaryCounts.rosarySelfCount) - Number(_selfCount) ;

          // Compute new total rosary count collected by users
          var newRosaryExternalCount = Number(rosaryCounts.rosaryExternalCount) - Number(_externalCount) ;

          countSnapShoptsRef.doc().set({

          	totalCount: newTotalCount,
            rosarySelfCount: newRosarySelfCount,
            rosaryExternalCount:newRosaryExternalCount,
            updatedBy:_updatedAuthor,
            updatedByID: _updatedAuthorID,
            msgId:_msgId,
            isExternal:_isExternal,
            details:_details,
            time:_updateTime
          })
 
          // Update rosary count
          return transaction.update(rosaryRef, {
            totalCount: newTotalCount,
            rosarySelfCount: newRosarySelfCount,
            rosaryExternalCount:newRosaryExternalCount,

          });




        });
      }).then(() => {


      	var rosaryNotificationRef = db.collection('mem').doc('rosary').collection('admin_notifications').doc('rosary_limits').collection('messages').doc();

      	var deletedCount = Number(_selfCount) + Number(_externalCount);

  //     	// Add a message of type deleted
      	return setRosaryDeletedNotification = rosaryNotificationRef.set({
		  author: "Rosaries Deleted",
		  authorID: msg.authorID,
		  body:"It was offered by: " + msg.author + ".\n \nTotal count reduced from " + rosaryCounts.totalCount + " to " + newTotalCount.toString() ,
		  title:deletedCount.toString() + " Rosaries Deleted",
		  priority:"medium",
		  id:msg.id,
		  token:"",
		  time:Date.now().toString(),
		  topic:"rosary_limits"
		});


      });	


  }
   else return "Not an active Rosary Message";

});


exports.sendFiveContinentsPrayer = functions.firestore
    .document('mem/rosary/notifications/five/messages/{documentID}')
    .onCreate((snap, context) => {
      
      const msg = snap.data();
      var payload;
      
       if(msg.topic!==""){

        payload = {

       	  topic: msg.topic,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};
	}
	else if(msg.token !== ""){

		payload = {

       	  token: msg.token,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};


	}


	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
    
  

    });




  exports.malayalamNotification = functions.firestore
    .document('mem/rosary/notifications/Malayalam/messages/{documentID}')
    .onCreate((snap, context) => {
      
      const msg = snap.data();
      var payload;
      

		payload = {

       	  topic: msg.topic,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
    
  

    });


    exports.englishNotification = functions.firestore
    .document('mem/rosary/notifications/English/messages/{documentID}')
    .onCreate((snap, context) => {
      
      const msg = snap.data();
      var payload;
      

		payload = {

       	  topic: msg.topic,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
    
  

    });



    exports.rosaryReminderNotification = functions.firestore
    .document('mem/rosary/notifications/rosary_reminder/messages/{documentID}')
    .onCreate((snap, context) => {
      
      const msg = snap.data();
      var payload;
      

		payload = {
       	
       	  token: msg.token,

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		    
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
    
  

    });



 exports.userActiveNotification = functions.firestore
    .document('mem/rosary/notifications/user_active/messages/{documentID}')
    .onCreate((snap, context) => {
      
      const msg = snap.data();
      var payload;
      

		payload = {
       
          token: msg.token,

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },

		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
    
  

    });

      // perform desired operations ...
    // });   

exports.newRegistrationNotification = functions.firestore
    .document('/mem/rosary/admin_notifications/new_user/messages/{documentID}')
    .onCreate((snap, context) => {
      
      
      const msg = snap.data();
      var payload;
      

		payload = {

       	  topic: msg.topic,
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
  

    });


  exports.rosaryLimitsNotification = functions.firestore
    .document('/mem/rosary/admin_notifications/rosary_limits/messages/{documentID}')
    .onCreate((snap, context) => {
      
      
      const msg = snap.data();
      var payload;
      
      if(msg)

		payload = {

       	  topic: "rosary_limits",
       

		  notification: {
		    title: msg.title,
		    body: msg.body,

		  },
		  android: {
		    ttl: 3600 * 1000,
		    notification: {
		      color: '#ffffff',
		    },
		  },
		  apns: {
		    payload: {
		      aps: {
		        badge: 42,
		      },
		    },
		  },
		  
		};

	    return admin.messaging().send(payload)
		  .then((response) => {
		    // Response is a message ID string.
		    console.log('Successfully sent message:', response);
		    console.log(response);
		    return response;
		  })
		  .catch((error) => {
		    console.log('Error sending message:', error);
		  });
  

    });
    

 exports.deleteUser = functions.firestore
    .document('/mem/rosary/users/{userID}')
    .onDelete((snap, context) => {
      return admin.auth().deleteUser(snap.id)
          .then(() => console.log('Deleted user with ID:' + snap.id))
          .catch((error) => console.error('There was an error while deleting user:', error));
    });


 function loadUsers() { 
    let dbRef = admin.database().ref('/users');
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            let users = [];
            for (var property in data) {
                users.push(data[property]);
            }
            resolve(users);
        }, (err) => {
            reject(err);
        });
    });
    return defer;
  }